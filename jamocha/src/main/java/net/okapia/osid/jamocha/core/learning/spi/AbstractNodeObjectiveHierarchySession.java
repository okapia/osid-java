//
// AbstractNodeObjectiveHierarchySession.java
//
//     Defines an Objective hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.learning.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an objective hierarchy session for delivering a hierarchy
 *  of objectives using the ObjectiveNode interface.
 */

public abstract class AbstractNodeObjectiveHierarchySession
    extends net.okapia.osid.jamocha.learning.spi.AbstractObjectiveHierarchySession
    implements org.osid.learning.ObjectiveHierarchySession {

    private java.util.Collection<org.osid.learning.ObjectiveNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root objective <code> Ids </code> in this hierarchy.
     *
     *  @return the root objective <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootObjectiveIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.learning.objectivenode.ObjectiveNodeToIdList(this.roots));
    }


    /**
     *  Gets the root objectives in the objective hierarchy. A node
     *  with no parents is an orphan. While all objective <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root objectives 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveList getRootObjectives()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.learning.objectivenode.ObjectiveNodeToObjectiveList(new net.okapia.osid.jamocha.learning.objectivenode.ArrayObjectiveNodeList(this.roots)));
    }


    /**
     *  Adds a root objective node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootObjective(org.osid.learning.ObjectiveNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root objective nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootObjectives(java.util.Collection<org.osid.learning.ObjectiveNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root objective node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootObjective(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.learning.ObjectiveNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Objective </code> has any parents. 
     *
     *  @param  objectiveId an objective <code> Id </code> 
     *  @return <code> true </code> if the objective has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> objectiveId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> objectiveId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentObjectives(org.osid.id.Id objectiveId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getObjectiveNode(objectiveId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of an
     *  objective.
     *
     *  @param  id an <code> Id </code> 
     *  @param  objectiveId the <code> Id </code> of an objective 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> objectiveId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> objectiveId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> objectiveId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfObjective(org.osid.id.Id id, org.osid.id.Id objectiveId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.learning.ObjectiveNodeList parents = getObjectiveNode(objectiveId).getParentObjectiveNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextObjectiveNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given objective. 
     *
     *  @param  objectiveId an objective <code> Id </code> 
     *  @return the parent <code> Ids </code> of the objective 
     *  @throws org.osid.NotFoundException <code> objectiveId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> objectiveId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentObjectiveIds(org.osid.id.Id objectiveId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.learning.objective.ObjectiveToIdList(getParentObjectives(objectiveId)));
    }


    /**
     *  Gets the parents of the given objective. 
     *
     *  @param  objectiveId the <code> Id </code> to query 
     *  @return the parents of the objective 
     *  @throws org.osid.NotFoundException <code> objectiveId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> objectiveId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveList getParentObjectives(org.osid.id.Id objectiveId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.learning.objectivenode.ObjectiveNodeToObjectiveList(getObjectiveNode(objectiveId).getParentObjectiveNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of an
     *  objective.
     *
     *  @param  id an <code> Id </code> 
     *  @param  objectiveId the Id of an objective 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> objectiveId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> objectiveId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> objectiveId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfObjective(org.osid.id.Id id, org.osid.id.Id objectiveId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfObjective(id, objectiveId)) {
            return (true);
        }

        try (org.osid.learning.ObjectiveList parents = getParentObjectives(objectiveId)) {
            while (parents.hasNext()) {
                if (isAncestorOfObjective(id, parents.getNextObjective().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if an objective has any children. 
     *
     *  @param  objectiveId an objective <code> Id </code> 
     *  @return <code> true </code> if the <code> objectiveId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> objectiveId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> objectiveId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildObjectives(org.osid.id.Id objectiveId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getObjectiveNode(objectiveId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of an
     *  objective.
     *
     *  @param  id an <code> Id </code> 
     *  @param objectiveId the <code> Id </code> of an 
     *         objective
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> objectiveId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> objectiveId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> objectiveId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfObjective(org.osid.id.Id id, org.osid.id.Id objectiveId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfObjective(objectiveId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  objective.
     *
     *  @param  objectiveId the <code> Id </code> to query 
     *  @return the children of the objective 
     *  @throws org.osid.NotFoundException <code> objectiveId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> objectiveId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildObjectiveIds(org.osid.id.Id objectiveId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.learning.objective.ObjectiveToIdList(getChildObjectives(objectiveId)));
    }


    /**
     *  Gets the children of the given objective. 
     *
     *  @param  objectiveId the <code> Id </code> to query 
     *  @return the children of the objective 
     *  @throws org.osid.NotFoundException <code> objectiveId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> objectiveId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveList getChildObjectives(org.osid.id.Id objectiveId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.learning.objectivenode.ObjectiveNodeToObjectiveList(getObjectiveNode(objectiveId).getChildObjectiveNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of an
     *  objective.
     *
     *  @param  id an <code> Id </code> 
     *  @param objectiveId the <code> Id </code> of an 
     *         objective
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> objectiveId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> objectiveId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> objectiveId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfObjective(org.osid.id.Id id, org.osid.id.Id objectiveId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfObjective(objectiveId, id)) {
            return (true);
        }

        try (org.osid.learning.ObjectiveList children = getChildObjectives(objectiveId)) {
            while (children.hasNext()) {
                if (isDescendantOfObjective(id, children.getNextObjective().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  objective.
     *
     *  @param  objectiveId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified objective node 
     *  @throws org.osid.NotFoundException <code> objectiveId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> objectiveId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getObjectiveNodeIds(org.osid.id.Id objectiveId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.learning.objectivenode.ObjectiveNodeToNode(getObjectiveNode(objectiveId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given objective.
     *
     *  @param  objectiveId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified objective node 
     *  @throws org.osid.NotFoundException <code> objectiveId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> objectiveId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveNode getObjectiveNodes(org.osid.id.Id objectiveId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getObjectiveNode(objectiveId));
    }


    /**
     *  Closes this <code>ObjectiveHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets an objective node.
     *
     *  @param objectiveId the id of the objective node
     *  @throws org.osid.NotFoundException <code>objectiveId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>objectiveId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.learning.ObjectiveNode getObjectiveNode(org.osid.id.Id objectiveId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(objectiveId, "objective Id");
        for (org.osid.learning.ObjectiveNode objective : this.roots) {
            if (objective.getId().equals(objectiveId)) {
                return (objective);
            }

            org.osid.learning.ObjectiveNode r = findObjective(objective, objectiveId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(objectiveId + " is not found");
    }


    protected org.osid.learning.ObjectiveNode findObjective(org.osid.learning.ObjectiveNode node, 
                                                            org.osid.id.Id objectiveId) 
	throws org.osid.OperationFailedException {

        try (org.osid.learning.ObjectiveNodeList children = node.getChildObjectiveNodes()) {
            while (children.hasNext()) {
                org.osid.learning.ObjectiveNode objective = children.getNextObjectiveNode();
                if (objective.getId().equals(objectiveId)) {
                    return (objective);
                }
                
                objective = findObjective(objective, objectiveId);
                if (objective != null) {
                    return (objective);
                }
            }
        }

        return (null);
    }
}
