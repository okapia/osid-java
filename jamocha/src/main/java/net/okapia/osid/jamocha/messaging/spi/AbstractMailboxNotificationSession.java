//
// AbstractMailboxNotificationSession.java
//
//     A template for making MailboxNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.messaging.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Mailbox} objects. This session is intended
 *  for consumers needing to synchronize their state with this service
 *  without the use of polling. Notifications are cancelled when this
 *  session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Mailbox} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for mailbox entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractMailboxNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.messaging.MailboxNotificationSession {


    /**
     *  Tests if this user can register for {@code Mailbox}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForMailboxNotifications() {
        return (true);
    }


    /**
     *  Register for notifications of new mailboxes. {@code
     *  MailboxReceiver.newMailbox()} is invoked when a new {@code
     *  Mailbox} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewMailboxes()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that introduces a new ancestor of the specified
     *  mailbox. {@code MailboxReceiver.newAncestorMailbox()} is
     *  invoked when the specified mailbox node gets a new ancestor.
     *
     *  @param mailboxId the {@code Id} of the
     *         {@code Mailbox} node to monitor
     *  @throws org.osid.NullArgumentException {@code mailboxId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewMailboxAncestors(org.osid.id.Id mailboxId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that introduces a new descendant of the specified
     *  mailbox. {@code MailboxReceiver.newDescendantMailbox()} is
     *  invoked when the specified mailbox node gets a new descendant.
     *
     *  @param mailboxId the {@code Id} of the
     *         {@code Mailbox} node to monitor
     *  @throws org.osid.NullArgumentException {@code mailboxId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewMailboxDescendants(org.osid.id.Id mailboxId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated mailboxes. {@code
     *  MailboxReceiver.changedMailbox()} is invoked when a mailbox is
     *  changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedMailboxes()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated mailbox. {@code
     *  MailboxReceiver.changedMailbox()} is invoked when the
     *  specified mailbox is changed.
     *
     *  @param mailboxId the {@code Id} of the {@code Mailbox} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code mailboxId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedMailbox(org.osid.id.Id mailboxId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted mailboxes. {@code
     *  MailboxReceiver.deletedMailbox()} is invoked when a mailbox is
     *  deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedMailboxes()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted mailbox. {@code
     *  MailboxReceiver.deletedMailbox()} is invoked when the
     *  specified mailbox is deleted.
     *
     *  @param mailboxId the {@code Id} of the
     *          {@code Mailbox} to monitor
     *  @throws org.osid.NullArgumentException {@code mailboxId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedMailbox(org.osid.id.Id mailboxId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that removes an ancestor of the specified mailbox. {@code
     *  MailboxReceiver.deletedAncestor()} is invoked when the
     *  specified mailbox node loses an ancestor.
     *
     *  @param mailboxId the {@code Id} of the
     *         {@code Mailbox} node to monitor
     *  @throws org.osid.NullArgumentException {@code mailboxId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedMailboxAncestors(org.osid.id.Id mailboxId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that removes a descendant of the specified mailbox. {@code
     *  MailboxReceiver.deletedDescendant()} is invoked when the
     *  specified mailbox node loses a descendant.
     *
     *  @param mailboxId the {@code Id} of the
     *          {@code Mailbox} node to monitor
     *  @throws org.osid.NullArgumentException {@code mailboxId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedMailboxDescendants(org.osid.id.Id mailboxId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
