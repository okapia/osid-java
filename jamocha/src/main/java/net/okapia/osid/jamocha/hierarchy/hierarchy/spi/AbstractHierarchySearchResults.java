//
// AbstractHierarchySearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.hierarchy.hierarchy.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractHierarchySearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.hierarchy.HierarchySearchResults {

    private org.osid.hierarchy.HierarchyList hierarchies;
    private final org.osid.hierarchy.HierarchyQueryInspector inspector;
    private final java.util.Collection<org.osid.hierarchy.records.HierarchySearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractHierarchySearchResults.
     *
     *  @param hierarchies the result set
     *  @param hierarchyQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>hierarchies</code>
     *          or <code>hierarchyQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractHierarchySearchResults(org.osid.hierarchy.HierarchyList hierarchies,
                                            org.osid.hierarchy.HierarchyQueryInspector hierarchyQueryInspector) {
        nullarg(hierarchies, "hierarchies");
        nullarg(hierarchyQueryInspector, "hierarchy query inspectpr");

        this.hierarchies = hierarchies;
        this.inspector = hierarchyQueryInspector;

        return;
    }


    /**
     *  Gets the hierarchy list resulting from a search.
     *
     *  @return a hierarchy list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyList getHierarchies() {
        if (this.hierarchies == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.hierarchy.HierarchyList hierarchies = this.hierarchies;
        this.hierarchies = null;
	return (hierarchies);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.hierarchy.HierarchyQueryInspector getHierarchyQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  hierarchy search record <code> Type. </code> This method must
     *  be used to retrieve a hierarchy implementing the requested
     *  record.
     *
     *  @param hierarchySearchRecordType a hierarchy search 
     *         record type 
     *  @return the hierarchy search
     *  @throws org.osid.NullArgumentException
     *          <code>hierarchySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(hierarchySearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.hierarchy.records.HierarchySearchResultsRecord getHierarchySearchResultsRecord(org.osid.type.Type hierarchySearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.hierarchy.records.HierarchySearchResultsRecord record : this.records) {
            if (record.implementsRecordType(hierarchySearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(hierarchySearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record hierarchy search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addHierarchyRecord(org.osid.hierarchy.records.HierarchySearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "hierarchy record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
