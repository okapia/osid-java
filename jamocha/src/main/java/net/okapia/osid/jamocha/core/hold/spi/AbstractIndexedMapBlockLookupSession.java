//
// AbstractIndexedMapBlockLookupSession.java
//
//    A simple framework for providing a Block lookup service
//    backed by a fixed collection of blocks with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.hold.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Block lookup service backed by a
 *  fixed collection of blocks. The blocks are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some blocks may be compatible
 *  with more types than are indicated through these block
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Blocks</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapBlockLookupSession
    extends AbstractMapBlockLookupSession
    implements org.osid.hold.BlockLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.hold.Block> blocksByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.hold.Block>());
    private final MultiMap<org.osid.type.Type, org.osid.hold.Block> blocksByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.hold.Block>());


    /**
     *  Makes a <code>Block</code> available in this session.
     *
     *  @param  block a block
     *  @throws org.osid.NullArgumentException <code>block<code> is
     *          <code>null</code>
     */

    @Override
    protected void putBlock(org.osid.hold.Block block) {
        super.putBlock(block);

        this.blocksByGenus.put(block.getGenusType(), block);
        
        try (org.osid.type.TypeList types = block.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.blocksByRecord.put(types.getNextType(), block);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a block from this session.
     *
     *  @param blockId the <code>Id</code> of the block
     *  @throws org.osid.NullArgumentException <code>blockId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeBlock(org.osid.id.Id blockId) {
        org.osid.hold.Block block;
        try {
            block = getBlock(blockId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.blocksByGenus.remove(block.getGenusType());

        try (org.osid.type.TypeList types = block.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.blocksByRecord.remove(types.getNextType(), block);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeBlock(blockId);
        return;
    }


    /**
     *  Gets a <code>BlockList</code> corresponding to the given
     *  block genus <code>Type</code> which does not include
     *  blocks of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known blocks or an error results. Otherwise,
     *  the returned list may contain only those blocks that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  blockGenusType a block genus type 
     *  @return the returned <code>Block</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>blockGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.BlockList getBlocksByGenusType(org.osid.type.Type blockGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.hold.block.ArrayBlockList(this.blocksByGenus.get(blockGenusType)));
    }


    /**
     *  Gets a <code>BlockList</code> containing the given
     *  block record <code>Type</code>. In plenary mode, the
     *  returned list contains all known blocks or an error
     *  results. Otherwise, the returned list may contain only those
     *  blocks that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  blockRecordType a block record type 
     *  @return the returned <code>block</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>blockRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.BlockList getBlocksByRecordType(org.osid.type.Type blockRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.hold.block.ArrayBlockList(this.blocksByRecord.get(blockRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.blocksByGenus.clear();
        this.blocksByRecord.clear();

        super.close();

        return;
    }
}
