//
// AbstractAssemblyOsidGovernatorQuery.java
//
//     An OsidGovernatorQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An OsidGovernatorQuery that stores terms.
 */

public abstract class AbstractAssemblyOsidGovernatorQuery
    extends AbstractAssemblySourceableOsidObjectQuery
    implements org.osid.OsidGovernatorQuery,
               org.osid.OsidGovernatorQueryInspector,
               org.osid.OsidGovernatorSearchOrder {

    private final AssemblyOsidOperableQuery query;

    
    /** 
     *  Constructs a new
     *  <code>AbstractAssemblyOsidGovernatorQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyOsidGovernatorQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        this.query = new AssemblyOsidOperableQuery(assembler);
        return;
    }


    /**
     *  Matches active operables.
     *
     *  @param match <code> true </code> to match active objects,
     *          <code> false </code> to match inactive objects
     */

    @OSID @Override
    public void matchActive(boolean match) {
        this.query.matchActive(match);
        return;
    }


    /**
     *  Clears all match active terms. 
     */

    @OSID @Override
    public void clearActiveTerms() {
        this.query.clearActiveTerms();
        return;
    }


    /**
     *  Gets the active query terms. 
     *
     *  @return the active terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getActiveTerms() {
        return (this.query.getActiveTerms());
    }


    /**
     *  Specifies a preference for ordering the result set by the
     *  active flag.
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByActive(org.osid.SearchOrderStyle style) {
        this.query.orderByActive(style);
        return;
    }


    /**
     *  Gets the column name for the active field.
     *
     *  @return the column name
     */

    protected String getActiveColumn() {
        return (this.query.getActiveColumn());
    }


    /**
     *  Matches enabled operables.
     *
     *  @param match <code> true </code> to match enabled objects,
     *          <code> false </code> to match inenabled objects
     */

    @OSID @Override
    public void matchEnabled(boolean match) {
        this.query.matchEnabled(match);
        return;
    }


    /**
     *  Clears all match enabled terms. 
     */

    @OSID @Override
    public void clearEnabledTerms() {
        this.query.clearEnabledTerms();
        return;
    }


    /**
     *  Gets the enabled query terms. 
     *
     *  @return the enabled terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getEnabledTerms() {
        return (this.query.getEnabledTerms());
    }


    /**
     *  Specifies a preference for ordering the result set by the
     *  enabled flag.
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByEnabled(org.osid.SearchOrderStyle style) {
        this.query.orderByEnabled(style);
        return;
    }


    /**
     *  Gets the column name for the enabled field.
     *
     *  @return the column name
     */

    protected String getEnabledColumn() {
        return (this.query.getEnabledColumn());
    }
    

    /**
     *  Matches disabled operables.
     *
     *  @param match <code> true </code> to match disabled objects,
     *          <code> false </code> to match indisabled objects
     */

    @OSID @Override
    public void matchDisabled(boolean match) {
        this.query.matchDisabled(match);
        return;
    }


    /**
     *  Clears all match disabled terms. 
     */

    @OSID @Override
    public void clearDisabledTerms() {
        this.query.clearDisabledTerms();
        return;
    }


    /**
     *  Gets the disabled query terms. 
     *
     *  @return the disabled terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getDisabledTerms() {
        return (this.query.getDisabledTerms());
    }


    /**
     *  Specifies a preference for ordering the result set by the
     *  disabled flag.
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDisabled(org.osid.SearchOrderStyle style) {
        this.query.orderByDisabled(style);
        return;
    }


    /**
     *  Gets the column name for the disabled field.
     *
     *  @return the column name
     */

    protected String getDisabledColumn() {
        return (this.query.getDisabledColumn());
    }


    /**
     *  Matches operational operables.
     *
     *  @param match <code> true </code> to match operational objects,
     *          <code> false </code> to match inoperational objects
     */

    @OSID @Override
    public void matchOperational(boolean match) {
        this.query.matchOperational(match);
        return;
    }


    /**
     *  Clears all match operational terms. 
     */

    @OSID @Override
    public void clearOperationalTerms() {
        this.query.clearOperationalTerms();
        return;
    }


    /**
     *  Gets the operational query terms. 
     *
     *  @return the operational terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getOperationalTerms() {
        return (this.query.getOperationalTerms());
    }


    /**
     *  Specifies a preference for ordering the result set by the
     *  operational flag.
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByOperational(org.osid.SearchOrderStyle style) {
        this.query.orderByOperational(style);
        return;
    }


    /**
     *  Gets the column name for the operational field.
     *
     *  @return the column name
     */

    protected String getOperationalColumn() {
        return (this.query.getOperationalColumn());
    }


    /**
     *  An OsidOperableQuery that stores terms.
     */
    
    protected class AssemblyOsidOperableQuery
        extends AbstractAssemblyOsidOperableQuery
        implements org.osid.OsidOperableQuery,
                   org.osid.OsidOperableQueryInspector,
                   org.osid.OsidOperableSearchOrder {

    
        /** 
         *  Constructs a new <code>AssemblyOsidOperableQuery</code>.
         *
         *  @param assembler the query assembler
         *  @throws org.osid.NullArgumentException <code>assembler</code>
         *          is <code>null</code>
         */

        protected AssemblyOsidOperableQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
            super(assembler);
            return;
        }

        /**
         *  Gets the column name for the active field.
         *
         *  @return the column name
         */

        @Override        
        protected String getActiveColumn() {
            return (super.getActiveColumn());
        }


        /**
         *  Gets the column name for the enabled field.
         *
         *  @return the column name
         */
        
        @Override
        protected String getEnabledColumn() {
            return (super.getEnabledColumn());
        }
    

        /**
         *  Gets the column name for the disabled field.
         *
         *  @return the column name
         */
        
        @Override
        protected String getDisabledColumn() {
            return (super.getDisabledColumn());
        }
    }
}
