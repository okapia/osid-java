//
// AbstractIssueQuery.java
//
//     A template for making an Issue Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.tracking.issue.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for issues.
 */

public abstract class AbstractIssueQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.tracking.IssueQuery {

    private final java.util.Collection<org.osid.tracking.records.IssueQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the queue <code> Id </code> for this query. 
     *
     *  @param  queueId the queue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> queueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchQueueId(org.osid.id.Id queueId, boolean match) {
        return;
    }


    /**
     *  Clears the queue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearQueueIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> QueueQuery </code> is available. 
     *
     *  @return <code> true </code> if a queue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a queue. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the queue query 
     *  @throws org.osid.UnimplementedException <code> supportsQueueQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueQuery getQueueQuery() {
        throw new org.osid.UnimplementedException("supportsQueueQuery() is false");
    }


    /**
     *  Clears the queue query terms. 
     */

    @OSID @Override
    public void clearQueueTerms() {
        return;
    }


    /**
     *  Sets the customer <code> Id </code> for this query. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCustomerId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the customer <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCustomerIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a customer query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCustomerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a customer. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsCustomerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getCustomerQuery() {
        throw new org.osid.UnimplementedException("supportsCustomerQuery() is false");
    }


    /**
     *  Clears the customer query terms. 
     */

    @OSID @Override
    public void clearCustomerTerms() {
        return;
    }


    /**
     *  Sets the topic <code> Id </code> for this query. 
     *
     *  @param  topicId the topic <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> topicId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchTopicId(org.osid.id.Id topicId, boolean match) {
        return;
    }


    /**
     *  Clears the topic <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearTopicIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> TopicQuery </code> is available. 
     *
     *  @return <code> true </code> if a topic query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTopicQuery() {
        return (false);
    }


    /**
     *  Gets the query for a topic. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the topic query 
     *  @throws org.osid.UnimplementedException <code> supportsTopicQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectQuery getTopicQuery() {
        throw new org.osid.UnimplementedException("supportsTopicQuery() is false");
    }


    /**
     *  Matches issues that have any topic. 
     *
     *  @param  match <code> true </code> to match issues with any topic, 
     *          <code> false </code> to match issues with no topic 
     */

    @OSID @Override
    public void matchAnyTopic(boolean match) {
        return;
    }


    /**
     *  Clears the topic query terms. 
     */

    @OSID @Override
    public void clearTopicTerms() {
        return;
    }


    /**
     *  Sets the subtask <code> Id </code> for this query. 
     *
     *  @param  issueId the issue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> issueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSubtaskId(org.osid.id.Id issueId, boolean match) {
        return;
    }


    /**
     *  Clears the subtask issue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSubtaskIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> IssueQuery </code> is available. 
     *
     *  @return <code> true </code> if a subtask issue query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubtaskQuery() {
        return (false);
    }


    /**
     *  Gets the query for a subtask issue. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the subtask issue query 
     *  @throws org.osid.UnimplementedException <code> supportsSubtaskQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueQuery getSubtaskQuery() {
        throw new org.osid.UnimplementedException("supportsSubtaskQuery() is false");
    }


    /**
     *  Matches issues that have any subtask. 
     *
     *  @param  match <code> true </code> to match issues with any subtasks, 
     *          <code> false </code> to match issues on no subtasks 
     */

    @OSID @Override
    public void matchAnySubtask(boolean match) {
        return;
    }


    /**
     *  Clears the subtask query terms. 
     */

    @OSID @Override
    public void clearSubtaskTerms() {
        return;
    }


    /**
     *  Sets the master issue <code> Id </code> for this query. 
     *
     *  @param  issueId the issue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> issueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchMasterIssueId(org.osid.id.Id issueId, boolean match) {
        return;
    }


    /**
     *  Clears the master issue issue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearMasterIssueIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> IssueQuery </code> is available. 
     *
     *  @return <code> true </code> if a master issue query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMasterIssueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a master issue. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the master issue query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMasterIssueQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueQuery getMasterIssueQuery() {
        throw new org.osid.UnimplementedException("supportsMasterIssueQuery() is false");
    }


    /**
     *  Matches issues that are subtasks. 
     *
     *  @param  match <code> true </code> to match issues with any master 
     *          issue, <code> false </code> to match issues on no master 
     *          issues 
     */

    @OSID @Override
    public void matchAnyMasterIssue(boolean match) {
        return;
    }


    /**
     *  Clears the linked issue query terms. 
     */

    @OSID @Override
    public void clearMasterIssueTerms() {
        return;
    }


    /**
     *  Sets the linked issue <code> Id </code> for this query. 
     *
     *  @param  issueId the issue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> issueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDuplicateIssueId(org.osid.id.Id issueId, boolean match) {
        return;
    }


    /**
     *  Clears the duplicate issue issue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDuplicateIssueIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> IssueQuery </code> is available. 
     *
     *  @return <code> true </code> if a duplicate issue query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDuplicateIssueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a duplicate issue. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the duplicate issue query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDuplicateIssueQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueQuery getDuplicateIssueQuery() {
        throw new org.osid.UnimplementedException("supportsDuplicateIssueQuery() is false");
    }


    /**
     *  Matches issues that are duplicated by any other issue. 
     *
     *  @param  match <code> true </code> to match duplicate issues, <code> 
     *          false </code> to match unlinked issues 
     */

    @OSID @Override
    public void matchAnyDuplicateIssue(boolean match) {
        return;
    }


    /**
     *  Clears the duplicate issue query terms. 
     */

    @OSID @Override
    public void clearDuplicateIssueTerms() {
        return;
    }


    /**
     *  Sets the branched issue <code> Id </code> for this query. 
     *
     *  @param  issueId the issue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> issueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBranchedIssueId(org.osid.id.Id issueId, boolean match) {
        return;
    }


    /**
     *  Clears the branched issue issue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearBranchedIssueIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> IssueQuery </code> is available. 
     *
     *  @return <code> true </code> if a branched issue query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBranchedIssueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a branched issue. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the branched issue query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBranchedIssueQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueQuery getBranchedIssueQuery() {
        throw new org.osid.UnimplementedException("supportsBranchedIssueQuery() is false");
    }


    /**
     *  Matches issues that branched to any other issue. 
     *
     *  @param  match <code> true </code> to match issues with branches, 
     *          <code> false </code> to match issues with no branches 
     */

    @OSID @Override
    public void matchAnyBranchedIssue(boolean match) {
        return;
    }


    /**
     *  Clears the branched issue query terms. 
     */

    @OSID @Override
    public void clearBranchedIssueTerms() {
        return;
    }


    /**
     *  Sets the root of the branch issue <code> Id </code> for this query. 
     *
     *  @param  issueId the issue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> issueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRootIssueId(org.osid.id.Id issueId, boolean match) {
        return;
    }


    /**
     *  Clears the root issue issue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRootIssueIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> IssueQuery </code> is available. 
     *
     *  @return <code> true </code> if a root issue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRootIssueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a root of the branch issue. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the root issue query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRootIssueQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueQuery getRootIssueQuery() {
        throw new org.osid.UnimplementedException("supportsRootIssueQuery() is false");
    }


    /**
     *  Matches issue branches with a root. 
     *
     *  @param  match <code> true </code> to match branches with a root, 
     *          <code> false </code> to match issues that are not a branch of 
     *          another issue 
     */

    @OSID @Override
    public void matchAnyRootIssue(boolean match) {
        return;
    }


    /**
     *  Clears the root issue query terms. 
     */

    @OSID @Override
    public void clearRootIssueTerms() {
        return;
    }


    /**
     *  Matches issues with the given priority type. 
     *
     *  @param  priorityType the priority <code> Type </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> priorityType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPriorityType(org.osid.type.Type priorityType, 
                                  boolean match) {
        return;
    }


    /**
     *  Clears the priority type query terms. 
     */

    @OSID @Override
    public void clearPriorityTypeTerms() {
        return;
    }


    /**
     *  Sets the creator <code> Id </code> for this query. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCreatorId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the creator <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCreatorIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreatorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a creator. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsCreatorQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getCreatorQuery() {
        throw new org.osid.UnimplementedException("supportsCreatorQuery() is false");
    }


    /**
     *  Clears the creator query terms. 
     */

    @OSID @Override
    public void clearCreatorTerms() {
        return;
    }


    /**
     *  Sets the creating agent <code> Id </code> for this query. 
     *
     *  @param  agentId the agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCreatingAgentId(org.osid.id.Id agentId, boolean match) {
        return;
    }


    /**
     *  Clears the creator <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCreatingAgentIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreatingAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for a creator. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCreatingAgentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getCreatingAgentQuery() {
        throw new org.osid.UnimplementedException("supportsCreatingAgentQuery() is false");
    }


    /**
     *  Clears the creating agent query terms. 
     */

    @OSID @Override
    public void clearCreatingAgentTerms() {
        return;
    }


    /**
     *  Matches issues created within the given date range inclusive. 
     *
     *  @param  from the range start 
     *  @param  to the range end 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchCreatedDate(org.osid.calendaring.DateTime from, 
                                 org.osid.calendaring.DateTime to, 
                                 boolean match) {
        return;
    }


    /**
     *  Clears the created date query terms. 
     */

    @OSID @Override
    public void clearCreatedDateTerms() {
        return;
    }


    /**
     *  Sets the reopener <code> Id </code> for this query. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchReopenerId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the reopener <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearReopenerIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReopenerQuery() {
        return (false);
    }


    /**
     *  Gets the query for the resource who reopened this issue. Multiple 
     *  retrievals produce a nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsReopenerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getReopenerQuery() {
        throw new org.osid.UnimplementedException("supportsReopenerQuery() is false");
    }


    /**
     *  Clears the reopener query terms. 
     */

    @OSID @Override
    public void clearReopenerTerms() {
        return;
    }


    /**
     *  Sets the reopening agent <code> Id </code> for this query. 
     *
     *  @param  agentId the agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchReopeningAgentId(org.osid.id.Id agentId, boolean match) {
        return;
    }


    /**
     *  Clears the reopening agent <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearReopeningAgentIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReopeningAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for the agent who reopened this issue. Multiple 
     *  retrievals produce a nested <code> OR </code> term. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsReopeningAgentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getReopeningAgentQuery() {
        throw new org.osid.UnimplementedException("supportsReopeningAgentQuery() is false");
    }


    /**
     *  Clears the reopner query terms. 
     */

    @OSID @Override
    public void clearReopeningAgentTerms() {
        return;
    }


    /**
     *  Matches issues reopened within the given date range inclusive. 
     *
     *  @param  from the range start 
     *  @param  to the range end 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchReopenedDate(org.osid.calendaring.DateTime from, 
                                  org.osid.calendaring.DateTime to, 
                                  boolean match) {
        return;
    }


    /**
     *  Matches issues that have any reopened date. 
     *
     *  @param  match <code> true </code> to match issues with any reopened 
     *          date, <code> false </code> to match issues with no reopened 
     *          date 
     */

    @OSID @Override
    public void matchAnyReopenedDate(boolean match) {
        return;
    }


    /**
     *  Clears the reopened date query terms. 
     */

    @OSID @Override
    public void clearReopenedDateTerms() {
        return;
    }


    /**
     *  Matches issue due dates within the given date range inclusive. 
     *
     *  @param  from the range start 
     *  @param  to the range end 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDueDate(org.osid.calendaring.DateTime from, 
                             org.osid.calendaring.DateTime to, boolean match) {
        return;
    }


    /**
     *  Matches issues that have any due date. 
     *
     *  @param  match <code> true </code> to match issues with any due date, 
     *          <code> false </code> to match issues with no due date 
     */

    @OSID @Override
    public void matchAnyDueDate(boolean match) {
        return;
    }


    /**
     *  Clears the due date query terms. 
     */

    @OSID @Override
    public void clearDueDateTerms() {
        return;
    }


    /**
     *  Matches issues that are pending a customer response. 
     *
     *  @param  match <code> true </code> to match pending issues, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public void matchPending(boolean match) {
        return;
    }


    /**
     *  Clears the pending query terms. 
     */

    @OSID @Override
    public void clearPendingTerms() {
        return;
    }


    /**
     *  Sets the blocking issue <code> Id </code> for this query. 
     *
     *  @param  issueId the issue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> issueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBlockingIssueId(org.osid.id.Id issueId, boolean match) {
        return;
    }


    /**
     *  Clears the blocking issue issue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearBlockingIssueIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> IssueQuery </code> is available. 
     *
     *  @return <code> true </code> if a blocking issue query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlockingIssueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a blocking issue. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the blocking issue query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlockingIssueQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueQuery getBlockingIssueQuery() {
        throw new org.osid.UnimplementedException("supportsBlockingIssueQuery() is false");
    }


    /**
     *  Matches any blocking issue. 
     *
     *  @param  match <code> true </code> to match blocking issues, <code> 
     *          false </code> to match issues not blocking 
     */

    @OSID @Override
    public void matchAnyBlockingIssue(boolean match) {
        return;
    }


    /**
     *  Clears the blocking issue query terms. 
     */

    @OSID @Override
    public void clearBlockingIssueTerms() {
        return;
    }


    /**
     *  Sets the blocked issue <code> Id </code> for this query. 
     *
     *  @param  issueId the issue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> issueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBlockedIssueId(org.osid.id.Id issueId, boolean match) {
        return;
    }


    /**
     *  Clears the blocked issue issue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearBlockedIssueIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> IssueQuery </code> is available. 
     *
     *  @return <code> true </code> if a blocked issue query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlockedIssueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a blocked issue. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the blocking issue query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlockedIssueQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueQuery getBlockedIssueQuery() {
        throw new org.osid.UnimplementedException("supportsBlockedIssueQuery() is false");
    }


    /**
     *  Matches any issue blocked. 
     *
     *  @param  match <code> true </code> to match issues blocked, <code> 
     *          false </code> to match issues that are not blocked 
     */

    @OSID @Override
    public void matchAnyBlockedIssue(boolean match) {
        return;
    }


    /**
     *  Clears the blocked issue query terms. 
     */

    @OSID @Override
    public void clearBlockedIssueTerms() {
        return;
    }


    /**
     *  Sets the resolver <code> Id </code> for this query. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResolverId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the resolver <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearResolverIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResolverQuery() {
        return (false);
    }


    /**
     *  Gets the query for the resource who resolved this issue. Multiple 
     *  retrievals produce a nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsResolverQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResolverQuery() {
        throw new org.osid.UnimplementedException("supportsResolverQuery() is false");
    }


    /**
     *  Clears the resolver query terms. 
     */

    @OSID @Override
    public void clearResolverTerms() {
        return;
    }


    /**
     *  Sets the resolving agent <code> Id </code> for this query. 
     *
     *  @param  agentId the agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResolvingAgentId(org.osid.id.Id agentId, boolean match) {
        return;
    }


    /**
     *  Clears the resolving agent <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearResolvingAgentIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResolvingAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for the agent who resolved this issue. Multiple 
     *  retrievals produce a nested <code> OR </code> term. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResolvingAgentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getResolvingAgentQuery() {
        throw new org.osid.UnimplementedException("supportsResolvingAgentQuery() is false");
    }


    /**
     *  Clears the resolving query terms. 
     */

    @OSID @Override
    public void clearResolvingAgentTerms() {
        return;
    }


    /**
     *  Matches issue resolved dates within the given date range inclusive. 
     *
     *  @param  from the range start 
     *  @param  to the range end 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchResolvedDate(org.osid.calendaring.DateTime from, 
                                  org.osid.calendaring.DateTime to, 
                                  boolean match) {
        return;
    }


    /**
     *  Matches any issue that has been resolved. 
     *
     *  @param  match <code> true </code> to match resolved issues, <code> 
     *          false </code> to match unresolved issues 
     */

    @OSID @Override
    public void matchAnyResolvedDate(boolean match) {
        return;
    }


    /**
     *  Clears the resolved date query terms. 
     */

    @OSID @Override
    public void clearResolvedDateTerms() {
        return;
    }


    /**
     *  Matches issues with the given resolution type. 
     *
     *  @param  resolutionType the resolution <code> Type </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resolutionType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchResolutionType(org.osid.type.Type resolutionType, 
                                    boolean match) {
        return;
    }


    /**
     *  Clears the resolution type query terms. 
     */

    @OSID @Override
    public void clearResolutionTypeTerms() {
        return;
    }


    /**
     *  Sets the closer resource <code> Id </code> for this query. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCloserId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the closer resource <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCloserIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCloserQuery() {
        return (false);
    }


    /**
     *  Gets the query for the agent who closed this issue. Multiple 
     *  retrievals produce a nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsCloserQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getCloserQuery() {
        throw new org.osid.UnimplementedException("supportsCloserQuery() is false");
    }


    /**
     *  Clears the closer query terms. 
     */

    @OSID @Override
    public void clearCloserTerms() {
        return;
    }


    /**
     *  Sets the closing agent <code> Id </code> for this query. 
     *
     *  @param  agentId the agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchClosingAgentId(org.osid.id.Id agentId, boolean match) {
        return;
    }


    /**
     *  Clears the closing agent <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearClosingAgentIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available. 
     *
     *  @return <code> true </code> if a closing agent query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsClosingAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for the agent who closed this issue. Multiple 
     *  retrievals produce a nested <code> OR </code> term. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsClosingAgentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getClosingAgentQuery() {
        throw new org.osid.UnimplementedException("supportsClosingAgentQuery() is false");
    }


    /**
     *  Clears the closing agent query terms. 
     */

    @OSID @Override
    public void clearClosingAgentTerms() {
        return;
    }


    /**
     *  Matches issue closed dates within the given date range inclusive. 
     *
     *  @param  from the range start 
     *  @param  to the range end 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchClosedDate(org.osid.calendaring.DateTime from, 
                                org.osid.calendaring.DateTime to, 
                                boolean match) {
        return;
    }


    /**
     *  Matches any issue that has been closed. 
     *
     *  @param  match <code> true </code> to match closed issues, <code> false 
     *          </code> to match unclosed issues 
     */

    @OSID @Override
    public void matchAnyClosedDate(boolean match) {
        return;
    }


    /**
     *  Clears the closed date query terms. 
     */

    @OSID @Override
    public void clearClosedDateTerms() {
        return;
    }


    /**
     *  Sets the currently assigned resource <code> Id </code> for this query. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAssignedResourceId(org.osid.id.Id resourceId, 
                                        boolean match) {
        return;
    }


    /**
     *  Clears the currently assigned resource <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAssignedResourceIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if an assigned resource query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssignedResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for the current assigned resource. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssignedResourceQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getAssignedResourceQuery() {
        throw new org.osid.UnimplementedException("supportsAssignedResourceQuery() is false");
    }


    /**
     *  Matches any issue that has any current assigned resource. 
     *
     *  @param  match <code> true </code> to match issues with any currently 
     *          assigned resource, <code> false </code> to match issues with 
     *          no currently assigned resource 
     */

    @OSID @Override
    public void matchAnyAssignedResource(boolean match) {
        return;
    }


    /**
     *  Clears the current assigned resource query terms. 
     */

    @OSID @Override
    public void clearAssignedResourceTerms() {
        return;
    }


    /**
     *  Sets the resource <code> Id </code> for this query to match any 
     *  resource ever assigned to an issue. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResourceId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the assigned resource <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearResourceIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if an assigned resource query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for the assigned resource. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResourceQuery() {
        throw new org.osid.UnimplementedException("supportsResourceQuery() is false");
    }


    /**
     *  Matches any issue that has any assigned resource. 
     *
     *  @param  match <code> true </code> to match issues with any resource 
     *          ever assigned, <code> false </code> to match issues that never 
     *          had an assigned resource 
     */

    @OSID @Override
    public void matchAnyResource(boolean match) {
        return;
    }


    /**
     *  Clears the resource query terms. 
     */

    @OSID @Override
    public void clearResourceTerms() {
        return;
    }


    /**
     *  Sets the log entry <code> Id </code> for this query to match issues 
     *  along the given log entry. 
     *
     *  @param  logEntryId the log entry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> logEntryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLogEntryId(org.osid.id.Id logEntryId, boolean match) {
        return;
    }


    /**
     *  Clears the log entry <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearLogEntryIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> LogEntryQuery </code> is available. 
     *
     *  @return <code> true </code> if a log entry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a log entry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the log entry query 
     *  @throws org.osid.UnimplementedException <code> supportsLogEntryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.LogEntryQuery getLogEntryQuery() {
        throw new org.osid.UnimplementedException("supportsLogEntryQuery() is false");
    }


    /**
     *  Matches issues that are used on any log entry. 
     *
     *  @param  match <code> true </code> to match issues with any log entry, 
     *          <code> false </code> to match issues with no log entries 
     */

    @OSID @Override
    public void matchAnyLogEntry(boolean match) {
        return;
    }


    /**
     *  Clears the log entry query terms. 
     */

    @OSID @Override
    public void clearLogEntryTerms() {
        return;
    }


    /**
     *  Sets the front office <code> Id </code> for this query to match issues 
     *  assigned to front offices. 
     *
     *  @param  frontOfficeId the front office <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchFrontOfficeId(org.osid.id.Id frontOfficeId, boolean match) {
        return;
    }


    /**
     *  Clears the front office <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearFrontOfficeIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> FrontOfficeQuery </code> is available. 
     *
     *  @return <code> true </code> if a front office query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFrontOfficeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a front office. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the front office query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFrontOfficeQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeQuery getFrontOfficeQuery() {
        throw new org.osid.UnimplementedException("supportsFrontOfficeQuery() is false");
    }


    /**
     *  Clears the front office query terms. 
     */

    @OSID @Override
    public void clearFrontOfficeTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given issue query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an issue implementing the requested record.
     *
     *  @param issueRecordType an issue record type
     *  @return the issue query record
     *  @throws org.osid.NullArgumentException
     *          <code>issueRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(issueRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.tracking.records.IssueQueryRecord getIssueQueryRecord(org.osid.type.Type issueRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.tracking.records.IssueQueryRecord record : this.records) {
            if (record.implementsRecordType(issueRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(issueRecordType + " is not supported");
    }


    /**
     *  Adds a record to this issue query. 
     *
     *  @param issueQueryRecord issue query record
     *  @param issueRecordType issue record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addIssueQueryRecord(org.osid.tracking.records.IssueQueryRecord issueQueryRecord, 
                                          org.osid.type.Type issueRecordType) {

        addRecordType(issueRecordType);
        nullarg(issueQueryRecord, "issue query record");
        this.records.add(issueQueryRecord);        
        return;
    }
}
