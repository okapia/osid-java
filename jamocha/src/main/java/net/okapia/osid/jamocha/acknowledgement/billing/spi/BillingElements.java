//
// BillingElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.acknowledgement.billing.spi;

/**
 *  Ids for object elements for use in forms and queries.
 */

public class BillingElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the BillingElement Id.
     *
     *  @return the billing element Id
     */

    public static org.osid.id.Id getBillingEntityId() {
        return (makeEntityId("osid.acknowledgement.Billing"));
    }


    /**
     *  Gets the CreditId element Id.
     *
     *  @return the CreditId element Id
     */

    public static org.osid.id.Id getCreditId() {
        return (makeQueryElementId("osid.acknowledgement.billing.CreditId"));
    }


    /**
     *  Gets the Credit element Id.
     *
     *  @return the Credit element Id
     */

    public static org.osid.id.Id getCredit() {
        return (makeQueryElementId("osid.acknowledgement.billing.Credit"));
    }


    /**
     *  Gets the AncestorBillingId element Id.
     *
     *  @return the AncestorBillingId element Id
     */

    public static org.osid.id.Id getAncestorBillingId() {
        return (makeQueryElementId("osid.acknowledgement.billing.AncestorBillingId"));
    }


    /**
     *  Gets the AncestorBilling element Id.
     *
     *  @return the AncestorBilling element Id
     */

    public static org.osid.id.Id getAncestorBilling() {
        return (makeQueryElementId("osid.acknowledgement.billing.AncestorBilling"));
    }


    /**
     *  Gets the DescendantBillingId element Id.
     *
     *  @return the DescendantBillingId element Id
     */

    public static org.osid.id.Id getDescendantBillingId() {
        return (makeQueryElementId("osid.acknowledgement.billing.DescendantBillingId"));
    }


    /**
     *  Gets the DescendantBilling element Id.
     *
     *  @return the DescendantBilling element Id
     */

    public static org.osid.id.Id getDescendantBilling() {
        return (makeQueryElementId("osid.acknowledgement.billing.DescendantBilling"));
    }
}
