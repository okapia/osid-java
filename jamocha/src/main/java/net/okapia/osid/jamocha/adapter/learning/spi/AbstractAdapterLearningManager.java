//
// AbstractLearningManager.java
//
//     An adapter for a LearningManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.learning.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a LearningManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterLearningManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.learning.LearningManager>
    implements org.osid.learning.LearningManager {


    /**
     *  Constructs a new {@code AbstractAdapterLearningManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterLearningManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterLearningManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterLearningManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if an objective lookup service is supported. An objective lookup 
     *  service defines methods to access objectives. 
     *
     *  @return true if objective lookup is supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveLookup() {
        return (getAdapteeManager().supportsObjectiveLookup());
    }


    /**
     *  Tests if an objective query service is supported. 
     *
     *  @return <code> true </code> if objective query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveQuery() {
        return (getAdapteeManager().supportsObjectiveQuery());
    }


    /**
     *  Tests if an objective search service is supported. 
     *
     *  @return <code> true </code> if objective search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveSearch() {
        return (getAdapteeManager().supportsObjectiveSearch());
    }


    /**
     *  Tests if an objective administrative service is supported. 
     *
     *  @return <code> true </code> if objective admin is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveAdmin() {
        return (getAdapteeManager().supportsObjectiveAdmin());
    }


    /**
     *  Tests if objective notification is supported. Messages may be sent 
     *  when objectives are created, modified, or deleted. 
     *
     *  @return <code> true </code> if objective notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveNotification() {
        return (getAdapteeManager().supportsObjectiveNotification());
    }


    /**
     *  Tests if an objective hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if an objective hierarchy traversal is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveHierarchy() {
        return (getAdapteeManager().supportsObjectiveHierarchy());
    }


    /**
     *  Tests if an objective hierarchy design is supported. 
     *
     *  @return <code> true </code> if an objective hierarchy design is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveHierarchyDesign() {
        return (getAdapteeManager().supportsObjectiveHierarchyDesign());
    }


    /**
     *  Tests if an objective sequencing design is supported. 
     *
     *  @return <code> true </code> if objective sequencing is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveSequencing() {
        return (getAdapteeManager().supportsObjectiveSequencing());
    }


    /**
     *  Tests if an objective to objective bank lookup session is available. 
     *
     *  @return <code> true </code> if objective objective bank lookup session 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveObjectiveBank() {
        return (getAdapteeManager().supportsObjectiveObjectiveBank());
    }


    /**
     *  Tests if an objective to objective bank assignment session is 
     *  available. 
     *
     *  @return <code> true </code> if objective objective bank assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveObjectiveBankAssignment() {
        return (getAdapteeManager().supportsObjectiveObjectiveBankAssignment());
    }


    /**
     *  Tests if an objective smart objective bank cataloging service is 
     *  supported. 
     *
     *  @return <code> true </code> if objective smart objective banks are 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveSmartObjectiveBank() {
        return (getAdapteeManager().supportsObjectiveSmartObjectiveBank());
    }


    /**
     *  Tests if an objective requisite service is supported. 
     *
     *  @return <code> true </code> if objective requisite service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveRequisite() {
        return (getAdapteeManager().supportsObjectiveRequisite());
    }


    /**
     *  Tests if an objective requisite assignment service is supported. 
     *
     *  @return <code> true </code> if objective requisite assignment service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveRequisiteAssignment() {
        return (getAdapteeManager().supportsObjectiveRequisiteAssignment());
    }


    /**
     *  Tests if an activity lookup service is supported. 
     *
     *  @return <code> true </code> if activity lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityLookup() {
        return (getAdapteeManager().supportsActivityLookup());
    }


    /**
     *  Tests if an activity query service is supported. 
     *
     *  @return <code> true </code> if activity query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityQuery() {
        return (getAdapteeManager().supportsActivityQuery());
    }


    /**
     *  Tests if an activity search service is supported. 
     *
     *  @return <code> true </code> if activity search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivitySearch() {
        return (getAdapteeManager().supportsActivitySearch());
    }


    /**
     *  Tests if an activity administrative service is supported. 
     *
     *  @return <code> true </code> if activity admin is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityAdmin() {
        return (getAdapteeManager().supportsActivityAdmin());
    }


    /**
     *  Tests if activity notification is supported. Messages may be sent when 
     *  activities are created, modified, or deleted. 
     *
     *  @return <code> true </code> if activity notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityNotification() {
        return (getAdapteeManager().supportsActivityNotification());
    }


    /**
     *  Tests if an activity to objective bank lookup session is available. 
     *
     *  @return <code> true </code> if activity objective bank lookup session 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityObjectiveBank() {
        return (getAdapteeManager().supportsActivityObjectiveBank());
    }


    /**
     *  Tests if an activity to objective bank assignment session is 
     *  available. 
     *
     *  @return <code> true </code> if activity objective bank assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityObjectiveBankAssignment() {
        return (getAdapteeManager().supportsActivityObjectiveBankAssignment());
    }


    /**
     *  Tests if an activity smart objective bank cataloging service is 
     *  supported. 
     *
     *  @return <code> true </code> if activity smart objective banks are 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivitySmartObjectiveBank() {
        return (getAdapteeManager().supportsActivitySmartObjectiveBank());
    }


    /**
     *  Tests if looking up proficiencies is supported. 
     *
     *  @return <code> true </code> if proficiency lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProficiencyLookup() {
        return (getAdapteeManager().supportsProficiencyLookup());
    }


    /**
     *  Tests if querying proficiencies is supported. 
     *
     *  @return <code> true </code> if proficiency query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProficiencyQuery() {
        return (getAdapteeManager().supportsProficiencyQuery());
    }


    /**
     *  Tests if searching proficiencies is supported. 
     *
     *  @return <code> true </code> if proficiency search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProficiencySearch() {
        return (getAdapteeManager().supportsProficiencySearch());
    }


    /**
     *  Tests if proficiency <code> </code> administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if proficiency administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProficiencyAdmin() {
        return (getAdapteeManager().supportsProficiencyAdmin());
    }


    /**
     *  Tests if a proficiency <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if proficiency notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProficiencyNotification() {
        return (getAdapteeManager().supportsProficiencyNotification());
    }


    /**
     *  Tests if a proficiency objective bank mapping lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a proficiency objective bank lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProficiencyObjectiveBank() {
        return (getAdapteeManager().supportsProficiencyObjectiveBank());
    }


    /**
     *  Tests if a proficiency objective bank mapping service is supported. 
     *
     *  @return <code> true </code> if proficiency to objective bank mapping 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProficiencyObjectiveBankAssignment() {
        return (getAdapteeManager().supportsProficiencyObjectiveBankAssignment());
    }


    /**
     *  Tests if a proficiency smart objective bank cataloging service is 
     *  supported. 
     *
     *  @return <code> true </code> if proficiency smart objective banks are 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProficiencySmartObjectiveBank() {
        return (getAdapteeManager().supportsProficiencySmartObjectiveBank());
    }


    /**
     *  Tests if a learning path service is supported for the authenticated 
     *  agent. 
     *
     *  @return <code> true </code> if learning path is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMyLearningPath() {
        return (getAdapteeManager().supportsMyLearningPath());
    }


    /**
     *  Tests if a learning path service is supported. 
     *
     *  @return <code> true </code> if learning path is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLearningPath() {
        return (getAdapteeManager().supportsLearningPath());
    }


    /**
     *  Tests if an objective bank lookup service is supported. 
     *
     *  @return <code> true </code> if objective bank lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveBankLookup() {
        return (getAdapteeManager().supportsObjectiveBankLookup());
    }


    /**
     *  Tests if an objective bank query service is supported. 
     *
     *  @return <code> true </code> if objective bank query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveBankQuery() {
        return (getAdapteeManager().supportsObjectiveBankQuery());
    }


    /**
     *  Tests if an objective bank search service is supported. 
     *
     *  @return <code> true </code> if objective bank search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveBankSearch() {
        return (getAdapteeManager().supportsObjectiveBankSearch());
    }


    /**
     *  Tests if an objective bank administrative service is supported. 
     *
     *  @return <code> true </code> if objective bank admin is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveBankAdmin() {
        return (getAdapteeManager().supportsObjectiveBankAdmin());
    }


    /**
     *  Tests if objective bank notification is supported. Messages may be 
     *  sent when objective banks are created, modified, or deleted. 
     *
     *  @return <code> true </code> if objective bank notification is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveBankNotification() {
        return (getAdapteeManager().supportsObjectiveBankNotification());
    }


    /**
     *  Tests if an objective bank hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if an objective bank hierarchy traversal 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveBankHierarchy() {
        return (getAdapteeManager().supportsObjectiveBankHierarchy());
    }


    /**
     *  Tests if objective bank hierarchy design is supported. 
     *
     *  @return <code> true </code> if an objective bank hierarchy design is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveBankHierarchyDesign() {
        return (getAdapteeManager().supportsObjectiveBankHierarchyDesign());
    }


    /**
     *  Tests if a learning batch service is supported. 
     *
     *  @return <code> true </code> if a learning batch service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLearningBatch() {
        return (getAdapteeManager().supportsLearningBatch());
    }


    /**
     *  Gets the supported <code> Objective </code> record types. 
     *
     *  @return a list containing the supported <code> Objective </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getObjectiveRecordTypes() {
        return (getAdapteeManager().getObjectiveRecordTypes());
    }


    /**
     *  Tests if the given <code> Objective </code> record type is supported. 
     *
     *  @param  objectiveRecordType a <code> Type </code> indicating an <code> 
     *          Objective </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> objectiveRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsObjectiveRecordType(org.osid.type.Type objectiveRecordType) {
        return (getAdapteeManager().supportsObjectiveRecordType(objectiveRecordType));
    }


    /**
     *  Gets the supported <code> Objective </code> search record types. 
     *
     *  @return a list containing the supported <code> Objective </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getObjectiveSearchRecordTypes() {
        return (getAdapteeManager().getObjectiveSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Objective </code> search record type is 
     *  supported. 
     *
     *  @param  objectiveSearchRecordType a <code> Type </code> indicating an 
     *          <code> Objective </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          objectiveSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsObjectiveSearchRecordType(org.osid.type.Type objectiveSearchRecordType) {
        return (getAdapteeManager().supportsObjectiveSearchRecordType(objectiveSearchRecordType));
    }


    /**
     *  Gets the supported <code> Activity </code> record types. 
     *
     *  @return a list containing the supported <code> Activity </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getActivityRecordTypes() {
        return (getAdapteeManager().getActivityRecordTypes());
    }


    /**
     *  Tests if the given <code> Activity </code> record type is supported. 
     *
     *  @param  activityRecordType a <code> Type </code> indicating a <code> 
     *          Activity </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> activityRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsActivityRecordType(org.osid.type.Type activityRecordType) {
        return (getAdapteeManager().supportsActivityRecordType(activityRecordType));
    }


    /**
     *  Gets the supported <code> Activity </code> search record types. 
     *
     *  @return a list containing the supported <code> Activity </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getActivitySearchRecordTypes() {
        return (getAdapteeManager().getActivitySearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Activity </code> search record type is 
     *  supported. 
     *
     *  @param  activitySearchRecordType a <code> Type </code> indicating a 
     *          <code> Activity </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> activitySearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsActivitySearchRecordType(org.osid.type.Type activitySearchRecordType) {
        return (getAdapteeManager().supportsActivitySearchRecordType(activitySearchRecordType));
    }


    /**
     *  Gets the supported <code> Proficiency </code> record types. 
     *
     *  @return a list containing the supported <code> Proficiency </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProficiencyRecordTypes() {
        return (getAdapteeManager().getProficiencyRecordTypes());
    }


    /**
     *  Tests if the given <code> Proficiency </code> record type is 
     *  supported. 
     *
     *  @param  proficiencyRecordType a <code> Type </code> indicating a 
     *          <code> Proficiency </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> proficiencyRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProficiencyRecordType(org.osid.type.Type proficiencyRecordType) {
        return (getAdapteeManager().supportsProficiencyRecordType(proficiencyRecordType));
    }


    /**
     *  Gets the supported <code> Proficiency </code> search types. 
     *
     *  @return a list containing the supported <code> Proficiency </code> 
     *          search types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProficiencySearchRecordTypes() {
        return (getAdapteeManager().getProficiencySearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Proficiency </code> search type is 
     *  supported. 
     *
     *  @param  proficiencySearchRecordType a <code> Type </code> indicating a 
     *          <code> Proficiency </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          proficiencySearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProficiencySearchRecordType(org.osid.type.Type proficiencySearchRecordType) {
        return (getAdapteeManager().supportsProficiencySearchRecordType(proficiencySearchRecordType));
    }


    /**
     *  Gets the supported <code> ObjectiveBank </code> record types. 
     *
     *  @return a list containing the supported <code> ObjectiveBank </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getObjectiveBankRecordTypes() {
        return (getAdapteeManager().getObjectiveBankRecordTypes());
    }


    /**
     *  Tests if the given <code> ObjectiveBank </code> record type is 
     *  supported. 
     *
     *  @param  objectiveBankRecordType a <code> Type </code> indicating an 
     *          <code> ObjectiveBank </code> type 
     *  @return <code> true </code> if the given objective bank record <code> 
     *          Type </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> objectiveBankRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsObjectiveBankRecordType(org.osid.type.Type objectiveBankRecordType) {
        return (getAdapteeManager().supportsObjectiveBankRecordType(objectiveBankRecordType));
    }


    /**
     *  Gets the supported objective bank search record types. 
     *
     *  @return a list containing the supported <code> ObjectiveBank </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getObjectiveBankSearchRecordTypes() {
        return (getAdapteeManager().getObjectiveBankSearchRecordTypes());
    }


    /**
     *  Tests if the given objective bank search record type is supported. 
     *
     *  @param  objectiveBankSearchRecordType a <code> Type </code> indicating 
     *          an <code> ObjectiveBank </code> search record type 
     *  @return <code> true </code> if the given search record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          objectiveBankSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsObjectiveBankSearchRecordType(org.osid.type.Type objectiveBankSearchRecordType) {
        return (getAdapteeManager().supportsObjectiveBankSearchRecordType(objectiveBankSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the objective 
     *  lookup service. 
     *
     *  @return an <code> ObjectiveLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveLookupSession getObjectiveLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getObjectiveLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the objective 
     *  lookup service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the objective bank 
     *  @return <code> an ObjectiveLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveLookupSession getObjectiveLookupSessionForObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getObjectiveLookupSessionForObjectiveBank(objectiveBankId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the objective 
     *  query service. 
     *
     *  @return an <code> ObjectiveQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQuerySession getObjectiveQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getObjectiveQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the objective 
     *  query service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the objective bank 
     *  @return <code> an ObjectiveQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQuerySession getObjectiveQuerySessionForObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getObjectiveQuerySessionForObjectiveBank(objectiveBankId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the objective 
     *  search service. 
     *
     *  @return an <code> ObjectiveSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveSearchSession getObjectiveSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getObjectiveSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the objective 
     *  search service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the objective bank 
     *  @return <code> an ObjectiveSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveSearchSession getObjectiveSearchSessionForObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getObjectiveSearchSessionForObjectiveBank(objectiveBankId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the objective 
     *  administration service. 
     *
     *  @return an <code> ObjectiveAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveAdminSession getObjectiveAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getObjectiveAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the objective 
     *  admin service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the objective bank 
     *  @return <code> an ObjectiveAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveAdminSession getObjectiveAdminSessionForObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getObjectiveAdminSessionForObjectiveBank(objectiveBankId));
    }


    /**
     *  Gets the notification session for notifications pertaining to 
     *  objective changes. 
     *
     *  @param  objectiveReceiver the objective receiver 
     *  @return an <code> ObjectiveNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> objectiveReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveNotificationSession getObjectiveNotificationSession(org.osid.learning.ObjectiveReceiver objectiveReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getObjectiveNotificationSession(objectiveReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the objective 
     *  notification service for the given objective bank. 
     *
     *  @param  objectiveReceiver the objective receiver 
     *  @param  objectiveBankId the <code> Id </code> of the objective bank 
     *  @return <code> an ObjectiveNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> objectiveReceiver 
     *          </code> or <code> objectiveBankId </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveNotificationSession getObjectiveNotificationSessionForObjectiveBank(org.osid.learning.ObjectiveReceiver objectiveReceiver, 
                                                                                                          org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getObjectiveNotificationSessionForObjectiveBank(objectiveReceiver, objectiveBankId));
    }


    /**
     *  Gets the session for traversing objective hierarchies. 
     *
     *  @return an <code> ObjectiveHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveHierarchySession getObjectiveHierarchySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getObjectiveHierarchySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the objective 
     *  hierarchy traversal service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the objective bank 
     *  @return an <code> ObjectiveHierarchySession </code> 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveHierarchy() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveHierarchySession getObjectiveHierarchySessionForObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getObjectiveHierarchySessionForObjectiveBank(objectiveBankId));
    }


    /**
     *  Gets the session for designing objective hierarchies. 
     *
     *  @return an <code> ObjectiveHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveHierarchyDesignSession getObjectiveHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getObjectiveHierarchyDesignSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the objective 
     *  hierarchy design service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the objective bank 
     *  @return an <code> ObjectiveHierarchyDesignSession </code> 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveHierarchyDesign() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveHierarchyDesignSession getObjectiveHierarchyDesignSessionForObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getObjectiveHierarchyDesignSessionForObjectiveBank(objectiveBankId));
    }


    /**
     *  Gets the session for sequencing objectives. 
     *
     *  @return an <code> ObjectiveSequencingSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveSequencing() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveSequencingSession getObjectiveSequencingSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getObjectiveSequencingSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the objective 
     *  sequencing service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the objective bank 
     *  @return an <code> ObjectiveSequencingSession </code> 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveSequencing() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveSequencingSession getObjectiveSequencingSessionForObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getObjectiveSequencingSessionForObjectiveBank(objectiveBankId));
    }


    /**
     *  Gets the session for retrieving objective to objective bank mappings. 
     *
     *  @return an <code> ObjectiveObjectiveBankSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveObjectiveBank() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveObjectiveBankSession getObjectiveObjectiveBankSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getObjectiveObjectiveBankSession());
    }


    /**
     *  Gets the session for assigning objective to objective bank mappings. 
     *
     *  @return an <code> ObjectiveObjectiveBankAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveObjectiveBankAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveObjectiveBankAssignmentSession getObjectiveObjectiveBankAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getObjectiveObjectiveBankAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage dynamic objective banks 
     *  of objectives. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the <code> 
     *          ObjectiveBank </code> 
     *  @return an <code> ObjectiveSmartObjectiveBankSession </code> 
     *  @throws org.osid.NotFoundException no objective bank found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveSmartObjectiveBank() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveSmartObjectiveBankSession getObjectiveSmartObjectiveBankSession(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getObjectiveSmartObjectiveBankSession(objectiveBankId));
    }


    /**
     *  Gets the session for examining objective requisites. 
     *
     *  @return an <code> ObjectiveRequisiteSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveRequisite() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveRequisiteSession getObjectiveRequisiteSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getObjectiveRequisiteSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the objective 
     *  sequencing service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the objective bank 
     *  @return an <code> ObjectiveRequisiteSession </code> 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveRequisite() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveRequisiteSession getObjectiveRequisiteSessionForObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getObjectiveRequisiteSessionForObjectiveBank(objectiveBankId));
    }


    /**
     *  Gets the session for managing objective requisites. 
     *
     *  @return an <code> ObjectiveRequisiteAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveRequisiteAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveRequisiteAssignmentSession getObjectiveRequisiteAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getObjectiveRequisiteAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the objective 
     *  sequencing service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the objective bank 
     *  @return an <code> ObjectiveRequisiteAssignmentSession </code> 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveRequisiteAssignment() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveRequisiteAssignmentSession getObjectiveRequisiteAssignmentSessionForObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getObjectiveRequisiteAssignmentSessionForObjectiveBank(objectiveBankId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  lookup service. 
     *
     *  @return an <code> ActivityLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivityLookupSession getActivityLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  lookup service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the objective bank 
     *  @return an <code> ActivityLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivityLookupSession getActivityLookupSessionForObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityLookupSessionForObjectiveBank(objectiveBankId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity query 
     *  service. 
     *
     *  @return a <code> ActivityQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsActivityQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivityQuerySession getActivityQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity query 
     *  service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the objective bank 
     *  @return an <code> ActivityQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsActivityQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivityQuerySession getActivityQuerySessionForObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityQuerySessionForObjectiveBank(objectiveBankId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  search service. 
     *
     *  @return a <code> ActivitySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivitySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivitySearchSession getActivitySearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivitySearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  search service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the objective bank 
     *  @return an <code> ActivitySearchSession </code> 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivitySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivitySearchSession getActivitySearchSessionForObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivitySearchSessionForObjectiveBank(objectiveBankId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  administration service. 
     *
     *  @return a <code> ActivityAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsActivityAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivityAdminSession getActivityAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity admin 
     *  service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the objective bank 
     *  @return an <code> ActivityAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsActivityAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivityAdminSession getActivityAdminSessionForObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityAdminSessionForObjectiveBank(objectiveBankId));
    }


    /**
     *  Gets the notification session for notifications pertaining to activity 
     *  changes. 
     *
     *  @param  activityReceiver the activity receiver 
     *  @return an <code> ActivityNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> activityReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivityNotificationSession getActivityNotificationSession(org.osid.learning.ActivityReceiver activityReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityNotificationSession(activityReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  notification service for the given objective bank. 
     *
     *  @param  activityReceiver the activity receiver 
     *  @param  objectiveBankId the <code> Id </code> of the objective bank 
     *  @return <code> an ActivityNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> activityReceiver </code> 
     *          or <code> objectiveBankId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivityNotificationSession getActivityNotificationSessionForObjectiveBank(org.osid.learning.ActivityReceiver activityReceiver, 
                                                                                                        org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityNotificationSessionForObjectiveBank(activityReceiver, objectiveBankId));
    }


    /**
     *  Gets the session for retrieving activity to objective bank mappings. 
     *
     *  @return an <code> ActivityObjectiveBankSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityObjectiveBank() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivityObjectiveBankSession getActivityObjectiveBankSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityObjectiveBankSession());
    }


    /**
     *  Gets the session for assigning activity to objective bank mappings. 
     *
     *  @return an <code> ActivityObjectiveBankAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityObjectiveBankAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivityObjectiveBankAssignmentSession getActivityObjectiveBankAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityObjectiveBankAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage dynamic objective banks 
     *  of activities. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the <code> 
     *          ObjectiveBank </code> 
     *  @return an <code> ActivitySmartObjectiveBankSession </code> 
     *  @throws org.osid.NotFoundException no objective bank found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivitySmartObjectiveBank() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivitySmartObjectiveBankSession getActivitySmartObjectiveBankSession(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivitySmartObjectiveBankSession(objectiveBankId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the proficiency 
     *  lookup service. 
     *
     *  @return a <code> ProficiencyLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProficiencyLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyLookupSession getProficiencyLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProficiencyLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the proficiency 
     *  lookup service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the obective bank 
     *  @return a <code> ProficiencyLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> ObjectiveBank </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProficiencyLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyLookupSession getProficiencyLookupSessionForObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProficiencyLookupSessionForObjectiveBank(objectiveBankId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the proficiency 
     *  query service. 
     *
     *  @return a <code> ProficiencyQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProficiencyQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyQuerySession getProficiencyQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProficiencyQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the proficiency 
     *  query service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the obective bank 
     *  @return a <code> ProficiencyQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> ObjectiveBank </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProficiencyQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyQuerySession getProficiencyQuerySessionForObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProficiencyQuerySessionForObjectiveBank(objectiveBankId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the proficiency 
     *  search service. 
     *
     *  @return a <code> ProficiencySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProficiencySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ProficiencySearchSession getProficiencySearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProficiencySearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the proficiency 
     *  search service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the <code> 
     *          ObjectiveBank </code> 
     *  @return a <code> ProficiencySearchSession </code> 
     *  @throws org.osid.NotFoundException no objective bank found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProficiencySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ProficiencySearchSession getProficiencySearchSessionForObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProficiencySearchSessionForObjectiveBank(objectiveBankId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the proficiency 
     *  administration service. 
     *
     *  @return a <code> ProficiencyAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProficiencyAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyAdminSession getProficiencyAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProficiencyAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the proficiency 
     *  administration service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the <code> 
     *          ObjectiveBank </code> 
     *  @return a <code> ProficiencyAdminSession </code> 
     *  @throws org.osid.NotFoundException no objective bank found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProficiencyAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyAdminSession getProficiencyAdminSessionForObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProficiencyAdminSessionForObjectiveBank(objectiveBankId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the proficiency 
     *  notification service. 
     *
     *  @param  proficiencyReceiver the notification callback 
     *  @return a <code> ProficiencyNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proficiencyReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProficiencyNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyNotificationSession getProficiencyNotificationSession(org.osid.learning.ProficiencyReceiver proficiencyReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProficiencyNotificationSession(proficiencyReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the proficiency 
     *  notification service for the given objective bank. 
     *
     *  @param  proficiencyReceiver the notification callback 
     *  @param  objectiveBankId the <code> Id </code> of the <code> 
     *          ObjectiveBank </code> 
     *  @return a <code> ProficiencyNotificationSession </code> 
     *  @throws org.osid.NotFoundException no objective bank found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proficiencyReceiver 
     *          </code> or <code> objectiveBankId </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProficiencyNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyNotificationSession getProficiencyNotificationSessionForObjectiveBank(org.osid.learning.ProficiencyReceiver proficiencyReceiver, 
                                                                                                              org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProficiencyNotificationSessionForObjectiveBank(proficiencyReceiver, objectiveBankId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup proficiency/objective 
     *  bank mappings. 
     *
     *  @return a <code> ProficiencyObjectiveBankSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProficiencyObjectiveBank() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyObjectiveBankSession getProficiencyObjectiveBankSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProficiencyObjectiveBankSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  proficiencys to objective banks. 
     *
     *  @return a <code> ProficiencyObjectiveBankAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProficiencyObjectiveBankAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyObjectiveBankAssignmentSession getProficiencyObjectiveBankAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProficiencyObjectiveBankAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage dynamic objective banks 
     *  of objectives. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the <code> 
     *          ObjectiveBank </code> 
     *  @return a <code> ProficiencySmartObjectiveBankSession </code> 
     *  @throws org.osid.NotFoundException no objective bank found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProficiencySmartObjectiveBank() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.learning.ProficiencySmartObjectiveBankSession getProficiencySmartObjectiveBankSession(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProficiencySmartObjectiveBankSession(objectiveBankId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my learning 
     *  path service. 
     *
     *  @return a <code> MyLearningPathSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMyLearningPath() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.MyLearningPathSession getMyLearningPathSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMyLearningPathSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my learning 
     *  path service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the <code> 
     *          ObjectiveBank </code> 
     *  @return a <code> MyLearningPathSession </code> 
     *  @throws org.osid.NotFoundException no objective bank found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMyLearningPath() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.MyLearningPathSession getMyLearningPathSessionForObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getMyLearningPathSessionForObjectiveBank(objectiveBankId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the learning path 
     *  service. 
     *
     *  @return a <code> LearningPathSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLearningPath() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.LearningPathSession getLearningPathSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLearningPathSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the learning path 
     *  service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the <code> 
     *          ObjectiveBank </code> 
     *  @return a <code> LearningPathSession </code> 
     *  @throws org.osid.NotFoundException no objective bank found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportyLearningPath() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.LearningPathSession getLearningPathSessionForObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getLearningPathSessionForObjectiveBank(objectiveBankId));
    }


    /**
     *  Gets the OsidSession associated with the objective bank lookup 
     *  service. 
     *
     *  @return an <code> ObjectiveBankLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveBankLookup() is false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankLookupSession getObjectiveBankLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getObjectiveBankLookupSession());
    }


    /**
     *  Gets the OsidSession associated with the objective bank query service. 
     *
     *  @return an <code> ObjectiveBankQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveBankQuery() is false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankQuerySession getObjectiveBankQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getObjectiveBankQuerySession());
    }


    /**
     *  Gets the OsidSession associated with the objective bank search 
     *  service. 
     *
     *  @return an <code> ObjectiveBankSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveBankSearch() is false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankSearchSession getObjectiveBankSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getObjectiveBankSearchSession());
    }


    /**
     *  Gets the OsidSession associated with the objective bank administration 
     *  service. 
     *
     *  @return an <code> ObjectiveBankAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveBankAdmin() is false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankAdminSession getObjectiveBankAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getObjectiveBankAdminSession());
    }


    /**
     *  Gets the notification session for notifications pertaining to 
     *  objective bank service changes. 
     *
     *  @param  objectiveBankReceiver the objective bank receiver 
     *  @return an <code> ObjectiveBankNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> objectiveBankReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveBankNotification() is false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankNotificationSession getObjectiveBankNotificationSession(org.osid.learning.ObjectiveBankReceiver objectiveBankReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getObjectiveBankNotificationSession(objectiveBankReceiver));
    }


    /**
     *  Gets the session traversing objective bank hierarchies. 
     *
     *  @return an <code> ObjectiveBankHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveBankHierarchy() is false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankHierarchySession getObjectiveBankHierarchySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getObjectiveBankHierarchySession());
    }


    /**
     *  Gets the session designing objective bank hierarchies. 
     *
     *  @return an <code> ObjectiveBankHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveBankHierarchyDesign() is false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankHierarchyDesignSession getObjectiveBankHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getObjectiveBankHierarchyDesignSession());
    }


    /**
     *  Gets a <code> LearningBatchManager. </code> 
     *
     *  @return a <code> LearningBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLearningBatch() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.learning.batch.LearningBatchManager getLearningBatchManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLearningBatchManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
