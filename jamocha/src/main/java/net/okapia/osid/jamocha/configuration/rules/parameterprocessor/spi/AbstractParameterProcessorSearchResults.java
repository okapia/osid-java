//
// AbstractParameterProcessorSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.configuration.rules.parameterprocessor.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractParameterProcessorSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.configuration.rules.ParameterProcessorSearchResults {

    private org.osid.configuration.rules.ParameterProcessorList parameterProcessors;
    private final org.osid.configuration.rules.ParameterProcessorQueryInspector inspector;
    private final java.util.Collection<org.osid.configuration.rules.records.ParameterProcessorSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractParameterProcessorSearchResults.
     *
     *  @param parameterProcessors the result set
     *  @param parameterProcessorQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>parameterProcessors</code>
     *          or <code>parameterProcessorQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractParameterProcessorSearchResults(org.osid.configuration.rules.ParameterProcessorList parameterProcessors,
                                            org.osid.configuration.rules.ParameterProcessorQueryInspector parameterProcessorQueryInspector) {
        nullarg(parameterProcessors, "parameter processors");
        nullarg(parameterProcessorQueryInspector, "parameter processor query inspectpr");

        this.parameterProcessors = parameterProcessors;
        this.inspector = parameterProcessorQueryInspector;

        return;
    }


    /**
     *  Gets the parameter processor list resulting from a search.
     *
     *  @return a parameter processor list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorList getParameterProcessors() {
        if (this.parameterProcessors == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.configuration.rules.ParameterProcessorList parameterProcessors = this.parameterProcessors;
        this.parameterProcessors = null;
	return (parameterProcessors);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.configuration.rules.ParameterProcessorQueryInspector getParameterProcessorQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  parameter processor search record <code> Type. </code> This method must
     *  be used to retrieve a parameterProcessor implementing the requested
     *  record.
     *
     *  @param parameterProcessorSearchRecordType a parameterProcessor search 
     *         record type 
     *  @return the parameter processor search
     *  @throws org.osid.NullArgumentException
     *          <code>parameterProcessorSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(parameterProcessorSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.configuration.rules.records.ParameterProcessorSearchResultsRecord getParameterProcessorSearchResultsRecord(org.osid.type.Type parameterProcessorSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.configuration.rules.records.ParameterProcessorSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(parameterProcessorSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(parameterProcessorSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record parameter processor search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addParameterProcessorRecord(org.osid.configuration.rules.records.ParameterProcessorSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "parameter processor record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
