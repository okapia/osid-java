//
// AbstractIndexedMapInputEnablerLookupSession.java
//
//    A simple framework for providing an InputEnabler lookup service
//    backed by a fixed collection of input enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an InputEnabler lookup service backed by a
 *  fixed collection of input enablers. The input enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some input enablers may be compatible
 *  with more types than are indicated through these input enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>InputEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapInputEnablerLookupSession
    extends AbstractMapInputEnablerLookupSession
    implements org.osid.control.rules.InputEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.control.rules.InputEnabler> inputEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.control.rules.InputEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.control.rules.InputEnabler> inputEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.control.rules.InputEnabler>());


    /**
     *  Makes an <code>InputEnabler</code> available in this session.
     *
     *  @param  inputEnabler an input enabler
     *  @throws org.osid.NullArgumentException <code>inputEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putInputEnabler(org.osid.control.rules.InputEnabler inputEnabler) {
        super.putInputEnabler(inputEnabler);

        this.inputEnablersByGenus.put(inputEnabler.getGenusType(), inputEnabler);
        
        try (org.osid.type.TypeList types = inputEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.inputEnablersByRecord.put(types.getNextType(), inputEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }

    
    /**
     *  Makes an array of input enablers available in this session.
     *
     *  @param  inputEnablers an array of input enablers
     *  @throws org.osid.NullArgumentException <code>inputEnablers<code>
     *          is <code>null</code>
     */

    @Override
    protected void putInputEnablers(org.osid.control.rules.InputEnabler[] inputEnablers) {
        for (org.osid.control.rules.InputEnabler inputEnabler : inputEnablers) {
            putInputEnabler(inputEnabler);
        }

        return;
    }


    /**
     *  Makes a collection of input enablers available in this session.
     *
     *  @param  inputEnablers a collection of input enablers
     *  @throws org.osid.NullArgumentException <code>inputEnablers<code>
     *          is <code>null</code>
     */

    @Override
    protected void putInputEnablers(java.util.Collection<? extends org.osid.control.rules.InputEnabler> inputEnablers) {
        for (org.osid.control.rules.InputEnabler inputEnabler : inputEnablers) {
            putInputEnabler(inputEnabler);
        }

        return;
    }


    /**
     *  Removes an input enabler from this session.
     *
     *  @param inputEnablerId the <code>Id</code> of the input enabler
     *  @throws org.osid.NullArgumentException <code>inputEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeInputEnabler(org.osid.id.Id inputEnablerId) {
        org.osid.control.rules.InputEnabler inputEnabler;
        try {
            inputEnabler = getInputEnabler(inputEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.inputEnablersByGenus.remove(inputEnabler.getGenusType());

        try (org.osid.type.TypeList types = inputEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.inputEnablersByRecord.remove(types.getNextType(), inputEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeInputEnabler(inputEnablerId);
        return;
    }


    /**
     *  Gets an <code>InputEnablerList</code> corresponding to the given
     *  input enabler genus <code>Type</code> which does not include
     *  input enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known input enablers or an error results. Otherwise,
     *  the returned list may contain only those input enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  inputEnablerGenusType an input enabler genus type 
     *  @return the returned <code>InputEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>inputEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerList getInputEnablersByGenusType(org.osid.type.Type inputEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.control.rules.inputenabler.ArrayInputEnablerList(this.inputEnablersByGenus.get(inputEnablerGenusType)));
    }


    /**
     *  Gets an <code>InputEnablerList</code> containing the given
     *  input enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known input enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  input enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  inputEnablerRecordType an input enabler record type 
     *  @return the returned <code>inputEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>inputEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerList getInputEnablersByRecordType(org.osid.type.Type inputEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.control.rules.inputenabler.ArrayInputEnablerList(this.inputEnablersByRecord.get(inputEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.inputEnablersByGenus.clear();
        this.inputEnablersByRecord.clear();

        super.close();

        return;
    }
}
