//
// AbstractImmutablePath.java
//
//     Wraps a mutable Path to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.topology.path.path.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Path</code> to hide modifiers. This
 *  wrapper provides an immutized Path from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying path whose state changes are visible.
 */

public abstract class AbstractImmutablePath
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.topology.path.Path {

    private final org.osid.topology.path.Path path;


    /**
     *  Constructs a new <code>AbstractImmutablePath</code>.
     *
     *  @param path the path to immutablize
     *  @throws org.osid.NullArgumentException <code>path</code>
     *          is <code>null</code>
     */

    protected AbstractImmutablePath(org.osid.topology.path.Path path) {
        super(path);
        this.path = path;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the starting node of this path. 
     *
     *  @return the starting node <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getStartingNodeId() {
        return (this.path.getStartingNodeId());
    }


    /**
     *  Gets the starting node of this path. 
     *
     *  @return the starting node 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.topology.Node getStartingNode()
        throws org.osid.OperationFailedException {

        return (this.path.getStartingNode());
    }


    /**
     *  Gets the <code> Id </code> of the ending node of this path. 
     *
     *  @return the ending node <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getEndingNodeId() {
        return (this.path.getEndingNodeId());
    }


    /**
     *  Gets the ending node of this path. 
     *
     *  @return the ending node 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.topology.Node getEndingNode()
        throws org.osid.OperationFailedException {

        return (this.path.getEndingNode());
    }


    /**
     *  Tests if all edges exist between the start and end nodes. A path may 
     *  be complete and inactive. 
     *
     *  @return <code> true </code> if the path is complete, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isComplete() {
        return (this.path.isComplete());
    }


    /**
     *  Test if the starting node and ending node are equal. 
     *
     *  @return <code> true </code> if the path is closed, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isClosed() {
        return (this.path.isClosed());
    }


    /**
     *  Gets the total number of nodes of this path. 
     *
     *  @return the total path hops 
     */

    @OSID @Override
    public long getHops() {
        return (this.path.getHops());
    }


    /**
     *  Gets the total distance of this path. 
     *
     *  @return the total path distance 
     */

    @OSID @Override
    public java.math.BigDecimal getDistance() {
        return (this.path.getDistance());
    }


    /**
     *  Gets the total cost of this path. 
     *
     *  @return the total path cost 
     */

    @OSID @Override
    public java.math.BigDecimal getCost() {
        return (this.path.getCost());
    }


    /**
     *  Gets the edge <code> Ids </code> of this path. 
     *
     *  @return the edge <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getEdgeIds() {
        return (this.path.getEdgeIds());
    }


    /**
     *  Gets the edges of this path. 
     *
     *  @return the edges 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdges()
        throws org.osid.OperationFailedException {

        return (this.path.getEdges());
    }


    /**
     *  Gets the path record corresponding to the given <code> Path </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> pathRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(pathRecordType) </code> is <code> true </code> . 
     *
     *  @param  pathRecordType the type of path record to retrieve 
     *  @return the path record 
     *  @throws org.osid.NullArgumentException <code> pathRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(pathRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.records.PathRecord getPathRecord(org.osid.type.Type pathRecordType)
        throws org.osid.OperationFailedException {

        return (this.path.getPathRecord(pathRecordType));
    }
}

