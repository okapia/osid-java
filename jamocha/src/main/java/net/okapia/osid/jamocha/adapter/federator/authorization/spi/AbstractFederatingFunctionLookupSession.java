//
// AbstractFederatingFunctionLookupSession.java
//
//     An abstract federating adapter for a FunctionLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.authorization.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  FunctionLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingFunctionLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.authorization.FunctionLookupSession>
    implements org.osid.authorization.FunctionLookupSession {

    private boolean parallel = false;
    private org.osid.authorization.Vault vault = new net.okapia.osid.jamocha.nil.authorization.vault.UnknownVault();


    /**
     *  Constructs a new <code>AbstractFederatingFunctionLookupSession</code>.
     */

    protected AbstractFederatingFunctionLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.authorization.FunctionLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Vault/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Vault Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getVaultId() {
        return (this.vault.getId());
    }


    /**
     *  Gets the <code>Vault</code> associated with this 
     *  session.
     *
     *  @return the <code>Vault</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.Vault getVault()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.vault);
    }


    /**
     *  Sets the <code>Vault</code>.
     *
     *  @param  vault the vault for this session
     *  @throws org.osid.NullArgumentException <code>vault</code>
     *          is <code>null</code>
     */

    protected void setVault(org.osid.authorization.Vault vault) {
        nullarg(vault, "vault");
        this.vault = vault;
        return;
    }


    /**
     *  Tests if this user can perform <code>Function</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupFunctions() {
        for (org.osid.authorization.FunctionLookupSession session : getSessions()) {
            if (session.canLookupFunctions()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Function</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeFunctionView() {
        for (org.osid.authorization.FunctionLookupSession session : getSessions()) {
            session.useComparativeFunctionView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Function</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryFunctionView() {
        for (org.osid.authorization.FunctionLookupSession session : getSessions()) {
            session.usePlenaryFunctionView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include functions in vaults which are children
     *  of this vault in the vault hierarchy.
     */

    @OSID @Override
    public void useFederatedVaultView() {
        for (org.osid.authorization.FunctionLookupSession session : getSessions()) {
            session.useFederatedVaultView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this vault only.
     */

    @OSID @Override
    public void useIsolatedVaultView() {
        for (org.osid.authorization.FunctionLookupSession session : getSessions()) {
            session.useIsolatedVaultView();
        }

        return;
    }


    /**
     *  Only active functions are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveFunctionView() {
        for (org.osid.authorization.FunctionLookupSession session : getSessions()) {
            session.useActiveFunctionView();
        }

        return;
    }


    /**
     *  Active and inactive functions are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusFunctionView() {
        for (org.osid.authorization.FunctionLookupSession session : getSessions()) {
            session.useAnyStatusFunctionView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>Function</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Function</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Function</code> and
     *  retained for compatibility.
     *
     *  In active mode, functions are returned that are currently
     *  active. In any status mode, active and inactive functions
     *  are returned.
     *
     *  @param  functionId <code>Id</code> of the
     *          <code>Function</code>
     *  @return the function
     *  @throws org.osid.NotFoundException <code>functionId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>functionId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.Function getFunction(org.osid.id.Id functionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.authorization.FunctionLookupSession session : getSessions()) {
            try {
                return (session.getFunction(functionId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(functionId + " not found");
    }


    /**
     *  Gets a <code>FunctionList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  functions specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Functions</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, functions are returned that are currently
     *  active. In any status mode, active and inactive functions
     *  are returned.
     *
     *  @param  functionIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Function</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>functionIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.FunctionList getFunctionsByIds(org.osid.id.IdList functionIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.authorization.function.MutableFunctionList ret = new net.okapia.osid.jamocha.authorization.function.MutableFunctionList();

        try (org.osid.id.IdList ids = functionIds) {
            while (ids.hasNext()) {
                ret.addFunction(getFunction(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>FunctionList</code> corresponding to the given
     *  function genus <code>Type</code> which does not include
     *  functions of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  functions or an error results. Otherwise, the returned list
     *  may contain only those functions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, functions are returned that are currently
     *  active. In any status mode, active and inactive functions
     *  are returned.
     *
     *  @param  functionGenusType a function genus type 
     *  @return the returned <code>Function</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>functionGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.FunctionList getFunctionsByGenusType(org.osid.type.Type functionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authorization.function.FederatingFunctionList ret = getFunctionList();

        for (org.osid.authorization.FunctionLookupSession session : getSessions()) {
            ret.addFunctionList(session.getFunctionsByGenusType(functionGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>FunctionList</code> corresponding to the given
     *  function genus <code>Type</code> and include any additional
     *  functions with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  functions or an error results. Otherwise, the returned list
     *  may contain only those functions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, functions are returned that are currently
     *  active. In any status mode, active and inactive functions
     *  are returned.
     *
     *  @param  functionGenusType a function genus type 
     *  @return the returned <code>Function</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>functionGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.FunctionList getFunctionsByParentGenusType(org.osid.type.Type functionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authorization.function.FederatingFunctionList ret = getFunctionList();

        for (org.osid.authorization.FunctionLookupSession session : getSessions()) {
            ret.addFunctionList(session.getFunctionsByParentGenusType(functionGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>FunctionList</code> containing the given
     *  function record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  functions or an error results. Otherwise, the returned list
     *  may contain only those functions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, functions are returned that are currently
     *  active. In any status mode, active and inactive functions
     *  are returned.
     *
     *  @param  functionRecordType a function record type 
     *  @return the returned <code>Function</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>functionRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.FunctionList getFunctionsByRecordType(org.osid.type.Type functionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authorization.function.FederatingFunctionList ret = getFunctionList();

        for (org.osid.authorization.FunctionLookupSession session : getSessions()) {
            ret.addFunctionList(session.getFunctionsByRecordType(functionRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Functions</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  functions or an error results. Otherwise, the returned list
     *  may contain only those functions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, functions are returned that are currently
     *  active. In any status mode, active and inactive functions
     *  are returned.
     *
     *  @return a list of <code>Functions</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.FunctionList getFunctions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authorization.function.FederatingFunctionList ret = getFunctionList();

        for (org.osid.authorization.FunctionLookupSession session : getSessions()) {
            ret.addFunctionList(session.getFunctions());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.authorization.function.FederatingFunctionList getFunctionList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.authorization.function.ParallelFunctionList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.authorization.function.CompositeFunctionList());
        }
    }
}
