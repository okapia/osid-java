//
// AbstractMutableDemographic.java
//
//     Defines a mutable Demographic.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.resource.demographic.demographic.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Demographic</code>.
 */

public abstract class AbstractMutableDemographic
    extends net.okapia.osid.jamocha.resource.demographic.demographic.spi.AbstractDemographic
    implements org.osid.resource.demographic.Demographic,
               net.okapia.osid.jamocha.builder.resource.demographic.demographic.DemographicMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this demographic. 
     *
     *  @param record demographic record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addDemographicRecord(org.osid.resource.demographic.records.DemographicRecord record, org.osid.type.Type recordType) {
        super.addDemographicRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Enables this demographic. Enabling an operable overrides any
     *  enabling rule that may exist.
     *  
     *  @param enabled <code>true</code> if enabled, <code>false<code>
     *         otherwise
     */
    
    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        return;
    }


    /**
     *  Disables this demographic. Disabling an operable overrides any
     *  enabling rule that may exist.
     *
     *  @param disabled <code> true </code> if this object is
     *         disabled, <code> false </code> otherwise
     */
    
    @Override
    public void setDisabled(boolean disabled) {
        super.setDisabled(disabled);
        return;
    }


    /**
     *  Sets the operational flag.
     *
     *  @param operational <code>true</code>if operational,
     *         <code>false</code> if not operational
     */
    
    @Override
    public void setOperational(boolean operational) {
        super.setOperational(operational);
        return;
    }


    /**
     *  Sets the display name for this demographic.
     *
     *  @param displayName the name for this demographic
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this demographic.
     *
     *  @param description the description of this demographic
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException <code>genusType</code>
     *          is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the rule.
     *
     *  @param rule the rule
     *  @throws org.osid.NullArgumentException <code>rule</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setRule(org.osid.rules.Rule rule) {
        super.setRule(rule);
        return;
    }


    /**
     *  Adds an included demographic.
     *
     *  @param demographic an included demographic
     *  @throws org.osid.NullArgumentException
     *          <code>demographic</code> is <code>null</code>
     */

    @Override
    public void addIncludedDemographic(org.osid.resource.demographic.Demographic demographic) {
        super.addIncludedDemographic(demographic);
        return;
    }


    /**
     *  Sets all the included demographics.
     *
     *  @param demographics a collection of included demographics
     *  @throws org.osid.NullArgumentException
     *          <code>demographics</code> is <code>null</code>
     */

    @Override
    public void setIncludedDemographics(java.util.Collection<org.osid.resource.demographic.Demographic> demographics) {
        super.setIncludedDemographics(demographics);
        return;
    }


    /**
     *  Adds an included intersecting demographic.
     *
     *  @param demographic an included intersecting demographic
     *  @throws org.osid.NullArgumentException
     *          <code>demographic</code> is <code>null</code>
     */

    @Override
    public void addIncludedIntersectingDemographic(org.osid.resource.demographic.Demographic demographic) {
        super.addIncludedIntersectingDemographic(demographic);
        return;
    }


    /**
     *  Sets all the included intersecting demographics.
     *
     *  @param demographics a collection of included intersecting demographics
     *  @throws org.osid.NullArgumentException
     *          <code>demographics</code> is <code>null</code>
     */

    @Override
    public void setIncludedIntersectingDemographics(java.util.Collection<org.osid.resource.demographic.Demographic> demographics) {
        super.setIncludedIntersectingDemographics(demographics);
        return;
    }


    /**
     *  Adds an included exclusive demographic.
     *
     *  @param demographic an included exclusive demographic
     *  @throws org.osid.NullArgumentException
     *          <code>demographic</code> is <code>null</code>
     */

    @Override
    public void addIncludedExclusiveDemographic(org.osid.resource.demographic.Demographic demographic) {
        super.addIncludedExclusiveDemographic(demographic);
        return;
    }


    /**
     *  Sets all the included exclusive demographics.
     *
     *  @param demographics a collection of included exclusive demographics
     *  @throws org.osid.NullArgumentException
     *          <code>demographics</code> is <code>null</code>
     */

    @Override
    public void setIncludedExclusiveDemographics(java.util.Collection<org.osid.resource.demographic.Demographic> demographics) {
        super.setIncludedExclusiveDemographics(demographics);
        return;
    }


    /**
     *  Adds an excluded demographic.
     *
     *  @param demographic an excluded demographic
     *  @throws org.osid.NullArgumentException
     *          <code>demographic</code> is <code>null</code>
     */

    @Override
    public void addExcludedDemographic(org.osid.resource.demographic.Demographic demographic) {
        super.addExcludedDemographic(demographic);
        return;
    }


    /**
     *  Sets all the excluded demographics.
     *
     *  @param demographics a collection of excluded demographics
     *  @throws org.osid.NullArgumentException
     *          <code>demographics</code> is <code>null</code>
     */

    @Override
    public void setExcludedDemographics(java.util.Collection<org.osid.resource.demographic.Demographic> demographics) {
        super.setExcludedDemographics(demographics);
        return;
    }


    /**
     *  Adds an included resource.
     *
     *  @param resource an included resource
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    @Override
    public void addIncludedResource(org.osid.resource.Resource resource) {
        super.addIncludedResource(resource);
        return;
    }


    /**
     *  Sets all the included resources.
     *
     *  @param resources a collection of included resources
     *  @throws org.osid.NullArgumentException <code>resources</code>
     *          is <code>null</code>
     */

    @Override
    public void setIncludedResources(java.util.Collection<org.osid.resource.Resource> resources) {
        super.setIncludedResources(resources);
        return;
    }


    /**
     *  Adds an excluded resource.
     *
     *  @param resource an excluded resource
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    @Override
    public void addExcludedResource(org.osid.resource.Resource resource) {
        super.addExcludedResource(resource);
        return;
    }


    /**
     *  Sets all the excluded resources.
     *
     *  @param resources a collection of excluded resources
     *  @throws org.osid.NullArgumentException <code>resources</code>
     *          is <code>null</code>
     */

    @Override
    public void setExcludedResources(java.util.Collection<org.osid.resource.Resource> resources) {
        super.setExcludedResources(resources);
        return;
    }
}

