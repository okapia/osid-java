//
// MutableIndexedMapQualifierLookupSession
//
//    Implements a Qualifier lookup service backed by a collection of
//    qualifiers indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authorization;


/**
 *  Implements a Qualifier lookup service backed by a collection of
 *  qualifiers. The qualifiers are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some qualifiers may be compatible
 *  with more types than are indicated through these qualifier
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of qualifiers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapQualifierLookupSession
    extends net.okapia.osid.jamocha.core.authorization.spi.AbstractIndexedMapQualifierLookupSession
    implements org.osid.authorization.QualifierLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapQualifierLookupSession} with no qualifiers.
     *
     *  @param vault the vault
     *  @throws org.osid.NullArgumentException {@code vault}
     *          is {@code null}
     */

      public MutableIndexedMapQualifierLookupSession(org.osid.authorization.Vault vault) {
        setVault(vault);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapQualifierLookupSession} with a
     *  single qualifier.
     *  
     *  @param vault the vault
     *  @param  qualifier a single qualifier
     *  @throws org.osid.NullArgumentException {@code vault} or
     *          {@code qualifier} is {@code null}
     */

    public MutableIndexedMapQualifierLookupSession(org.osid.authorization.Vault vault,
                                                  org.osid.authorization.Qualifier qualifier) {
        this(vault);
        putQualifier(qualifier);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapQualifierLookupSession} using an
     *  array of qualifiers.
     *
     *  @param vault the vault
     *  @param  qualifiers an array of qualifiers
     *  @throws org.osid.NullArgumentException {@code vault} or
     *          {@code qualifiers} is {@code null}
     */

    public MutableIndexedMapQualifierLookupSession(org.osid.authorization.Vault vault,
                                                  org.osid.authorization.Qualifier[] qualifiers) {
        this(vault);
        putQualifiers(qualifiers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapQualifierLookupSession} using a
     *  collection of qualifiers.
     *
     *  @param vault the vault
     *  @param  qualifiers a collection of qualifiers
     *  @throws org.osid.NullArgumentException {@code vault} or
     *          {@code qualifiers} is {@code null}
     */

    public MutableIndexedMapQualifierLookupSession(org.osid.authorization.Vault vault,
                                                  java.util.Collection<? extends org.osid.authorization.Qualifier> qualifiers) {

        this(vault);
        putQualifiers(qualifiers);
        return;
    }
    

    /**
     *  Makes a {@code Qualifier} available in this session.
     *
     *  @param  qualifier a qualifier
     *  @throws org.osid.NullArgumentException {@code qualifier{@code  is
     *          {@code null}
     */

    @Override
    public void putQualifier(org.osid.authorization.Qualifier qualifier) {
        super.putQualifier(qualifier);
        return;
    }


    /**
     *  Makes an array of qualifiers available in this session.
     *
     *  @param  qualifiers an array of qualifiers
     *  @throws org.osid.NullArgumentException {@code qualifiers{@code 
     *          is {@code null}
     */

    @Override
    public void putQualifiers(org.osid.authorization.Qualifier[] qualifiers) {
        super.putQualifiers(qualifiers);
        return;
    }


    /**
     *  Makes collection of qualifiers available in this session.
     *
     *  @param  qualifiers a collection of qualifiers
     *  @throws org.osid.NullArgumentException {@code qualifier{@code  is
     *          {@code null}
     */

    @Override
    public void putQualifiers(java.util.Collection<? extends org.osid.authorization.Qualifier> qualifiers) {
        super.putQualifiers(qualifiers);
        return;
    }


    /**
     *  Removes a Qualifier from this session.
     *
     *  @param qualifierId the {@code Id} of the qualifier
     *  @throws org.osid.NullArgumentException {@code qualifierId{@code  is
     *          {@code null}
     */

    @Override
    public void removeQualifier(org.osid.id.Id qualifierId) {
        super.removeQualifier(qualifierId);
        return;
    }    
}
