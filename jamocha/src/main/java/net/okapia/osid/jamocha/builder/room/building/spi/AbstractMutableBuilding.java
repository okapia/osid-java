//
// AbstractMutableBuilding.java
//
//     Defines a mutable Building.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.room.building.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Building</code>.
 */

public abstract class AbstractMutableBuilding
    extends net.okapia.osid.jamocha.room.building.spi.AbstractBuilding
    implements org.osid.room.Building,
               net.okapia.osid.jamocha.builder.room.building.BuildingMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this building. 
     *
     *  @param record building record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addBuildingRecord(org.osid.room.records.BuildingRecord record, org.osid.type.Type recordType) {
        super.addBuildingRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the start date for when this building is effective.
     *
     *  @param date the start date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setStartDate(org.osid.calendaring.DateTime date) {
        super.setStartDate(date);
        return;
    }


    /**
     *  Sets the end date for when this building ceases to be
     *  effective.
     *
     *  @param date the end date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setEndDate(org.osid.calendaring.DateTime date) {
        super.setEndDate(date);
        return;
    }


    /**
     *  Sets the display name for this building.
     *
     *  @param displayName the name for this building
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this building.
     *
     *  @param description the description of this building
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the address.
     *
     *  @param address an address
     *  @throws org.osid.NullArgumentException
     *          <code>address</code> is <code>null</code>
     */

    @Override
    public void setAddress(org.osid.contact.Address address) {
        super.setAddress(address);
        return;
    }


    /**
     *  Sets the official name.
     *
     *  @param officialName an official name
     *  @throws org.osid.NullArgumentException
     *          <code>officialName</code> is <code>null</code>
     */

    @Override
    public void setOfficialName(org.osid.locale.DisplayText officialName) {
        super.setOfficialName(officialName);
        return;
    }


    /**
     *  Sets the number.
     *
     *  @param number a number
     *  @throws org.osid.NullArgumentException
     *          <code>number</code> is <code>null</code>
     */

    @Override
    public void setNumber(String number) {
        super.setNumber(number);
        return;
    }


    /**
     *  Sets the enclosing building.
     *
     *  @param building an enclosing building
     *  @throws org.osid.NullArgumentException <code>building</code>
     *          is <code>null</code>
     */

    @Override
    public void setEnclosingBuilding(org.osid.room.Building building) {
        super.setEnclosingBuilding(building);
        return;
    }


    /**
     *  Adds a subdivision.
     *
     *  @param subdivision a subdivision
     *  @throws org.osid.NullArgumentException
     *          <code>subdivision</code> is <code>null</code>
     */

    @Override
    public void addSubdivision(org.osid.room.Building subdivision) {
        super.addSubdivision(subdivision);
        return;
    }


    /**
     *  Sets all the subdivisions.
     *
     *  @param subdivisions a collection of subdivisions
     *  @throws org.osid.NullArgumentException
     *          <code>subdivisions</code> is <code>null</code>
     */

    @Override
    public void setSubdivisions(java.util.Collection<org.osid.room.Building> subdivisions) {
        super.setSubdivisions(subdivisions);
        return;
    }


    /**
     *  Sets the gross area.
     *
     *  @param area a gross area
     *  @throws org.osid.NullArgumentException <code>area</code> is
     *          <code>null</code>
     */

    @Override
    public void setGrossArea(java.math.BigDecimal area) {
        super.setGrossArea(area);
        return;
    }
}

