//
// AbstractProgramSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.program.program.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractProgramSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.course.program.ProgramSearchResults {

    private org.osid.course.program.ProgramList programs;
    private final org.osid.course.program.ProgramQueryInspector inspector;
    private final java.util.Collection<org.osid.course.program.records.ProgramSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractProgramSearchResults.
     *
     *  @param programs the result set
     *  @param programQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>programs</code>
     *          or <code>programQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractProgramSearchResults(org.osid.course.program.ProgramList programs,
                                            org.osid.course.program.ProgramQueryInspector programQueryInspector) {
        nullarg(programs, "programs");
        nullarg(programQueryInspector, "program query inspectpr");

        this.programs = programs;
        this.inspector = programQueryInspector;

        return;
    }


    /**
     *  Gets the program list resulting from a search.
     *
     *  @return a program list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.course.program.ProgramList getPrograms() {
        if (this.programs == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.course.program.ProgramList programs = this.programs;
        this.programs = null;
	return (programs);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.course.program.ProgramQueryInspector getProgramQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  program search record <code> Type. </code> This method must
     *  be used to retrieve a program implementing the requested
     *  record.
     *
     *  @param programSearchRecordType a program search 
     *         record type 
     *  @return the program search
     *  @throws org.osid.NullArgumentException
     *          <code>programSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(programSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.program.records.ProgramSearchResultsRecord getProgramSearchResultsRecord(org.osid.type.Type programSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.course.program.records.ProgramSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(programSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(programSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record program search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addProgramRecord(org.osid.course.program.records.ProgramSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "program record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
