//
// AbstractOfficeSearch.java
//
//     A template for making an Office Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.office.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing office searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractOfficeSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.workflow.OfficeSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.workflow.records.OfficeSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.workflow.OfficeSearchOrder officeSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of offices. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  officeIds list of offices
     *  @throws org.osid.NullArgumentException
     *          <code>officeIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongOffices(org.osid.id.IdList officeIds) {
        while (officeIds.hasNext()) {
            try {
                this.ids.add(officeIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongOffices</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of office Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getOfficeIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  officeSearchOrder office search order 
     *  @throws org.osid.NullArgumentException
     *          <code>officeSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>officeSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderOfficeResults(org.osid.workflow.OfficeSearchOrder officeSearchOrder) {
	this.officeSearchOrder = officeSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.workflow.OfficeSearchOrder getOfficeSearchOrder() {
	return (this.officeSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given office search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an office implementing the requested record.
     *
     *  @param officeSearchRecordType an office search record
     *         type
     *  @return the office search record
     *  @throws org.osid.NullArgumentException
     *          <code>officeSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(officeSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.records.OfficeSearchRecord getOfficeSearchRecord(org.osid.type.Type officeSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.workflow.records.OfficeSearchRecord record : this.records) {
            if (record.implementsRecordType(officeSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(officeSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this office search. 
     *
     *  @param officeSearchRecord office search record
     *  @param officeSearchRecordType office search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addOfficeSearchRecord(org.osid.workflow.records.OfficeSearchRecord officeSearchRecord, 
                                           org.osid.type.Type officeSearchRecordType) {

        addRecordType(officeSearchRecordType);
        this.records.add(officeSearchRecord);        
        return;
    }
}
