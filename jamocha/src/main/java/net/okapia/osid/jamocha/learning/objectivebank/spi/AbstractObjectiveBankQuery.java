//
// AbstractObjectiveBankQuery.java
//
//     A template for making an ObjectiveBank Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.learning.objectivebank.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for objective banks.
 */

public abstract class AbstractObjectiveBankQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.learning.ObjectiveBankQuery {

    private final java.util.Collection<org.osid.learning.records.ObjectiveBankQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the objective <code> Id </code> for this query. 
     *
     *  @param  objectiveId an objective <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> objectiveId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchObjectiveId(org.osid.id.Id objectiveId, boolean match) {
        return;
    }


    /**
     *  Clears the objective <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearObjectiveIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> ObjectiveQuery </code> is available. 
     *
     *  @return <code> true </code> if an objective query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveQuery() {
        return (false);
    }


    /**
     *  Gets the query for an objective. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the objective query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQuery getObjectiveQuery() {
        throw new org.osid.UnimplementedException("supportsObjectiveQuery() is false");
    }


    /**
     *  Matches an objective bank that has any objective assigned. 
     *
     *  @param  match <code> true </code> to match objective banks with any 
     *          objective, <code> false </code> to match objective banks with 
     *          no objectives 
     */

    @OSID @Override
    public void matchAnyObjective(boolean match) {
        return;
    }


    /**
     *  Clears the objective terms. 
     */

    @OSID @Override
    public void clearObjectiveTerms() {
        return;
    }


    /**
     *  Sets the activity <code> Id </code> for this query. 
     *
     *  @param  activityId an activity <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> activityId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchActivityId(org.osid.id.Id activityId, boolean match) {
        return;
    }


    /**
     *  Clears the activity <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearActivityIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ActivityQuery </code> is available for querying 
     *  activities. 
     *
     *  @return <code> true </code> if an activity query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityQuery() {
        return (false);
    }


    /**
     *  Gets the query for an activity. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the activity query 
     *  @throws org.osid.UnimplementedException <code> supportsActivityQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivityQuery getActivityQuery() {
        throw new org.osid.UnimplementedException("supportsActivityQuery() is false");
    }


    /**
     *  Matches an objective bank that has any activity assigned. 
     *
     *  @param  match <code> true </code> to match objective banks with any 
     *          activity, <code> false </code> to match objective banks with 
     *          no activities 
     */

    @OSID @Override
    public void matchAnyActivity(boolean match) {
        return;
    }


    /**
     *  Clears the activity terms. 
     */

    @OSID @Override
    public void clearActivityTerms() {
        return;
    }


    /**
     *  Sets the objective bank <code> Id </code> for this query to match 
     *  objective banks that have the specified objective bank as an ancestor. 
     *
     *  @param  objectiveBankId an objective bank <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorObjectiveBankId(org.osid.id.Id objectiveBankId, 
                                             boolean match) {
        return;
    }


    /**
     *  Clears the ancestor objective bank <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorObjectiveBankIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ObjectiveBankQuery </code> is available for querying 
     *  ancestor objective banks. 
     *
     *  @return <code> true </code> if an objective bank query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorObjectiveBankQuery() {
        return (false);
    }


    /**
     *  Gets the query for an objective bank. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the objective bank query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorObjectiveBankQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankQuery getAncestorObjectiveBankQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorObjectiveBankQuery() is false");
    }


    /**
     *  Matches an objective bank that has any ancestor. 
     *
     *  @param  match <code> true </code> to match objective banks with any 
     *          ancestor, <code> false </code> to match root objective banks 
     */

    @OSID @Override
    public void matchAnyAncestorObjectiveBank(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor objective bank terms. 
     */

    @OSID @Override
    public void clearAncestorObjectiveBankTerms() {
        return;
    }


    /**
     *  Sets the objective bank <code> Id </code> for this query to match 
     *  objective banks that have the specified objective bank as a 
     *  descendant. 
     *
     *  @param  objectiveBankId an objective bank <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantObjectiveBankId(org.osid.id.Id objectiveBankId, 
                                               boolean match) {
        return;
    }


    /**
     *  Clears the descendant objective bank <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantObjectiveBankIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ObjectiveBankQuery </code> is available for querying 
     *  descendant objective banks. 
     *
     *  @return <code> true </code> if an objective bank query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantObjectiveBankQuery() {
        return (false);
    }


    /**
     *  Gets the query for an objective bank. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the objective bank query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantObjectiveBankQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankQuery getDescendantObjectiveBankQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantObjectiveBankQuery() is false");
    }


    /**
     *  Matches an objective bank that has any descendant. 
     *
     *  @param  match <code> true </code> to match objective banks with any 
     *          descendant, <code> false </code> to match leaf objective banks 
     */

    @OSID @Override
    public void matchAnyDescendantObjectiveBank(boolean match) {
        return;
    }


    /**
     *  Clears the descendant objective bank terms. 
     */

    @OSID @Override
    public void clearDescendantObjectiveBankTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given objective bank query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an objective bank implementing the requested record.
     *
     *  @param objectiveBankRecordType an objective bank record type
     *  @return the objective bank query record
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveBankRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(objectiveBankRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.learning.records.ObjectiveBankQueryRecord getObjectiveBankQueryRecord(org.osid.type.Type objectiveBankRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.learning.records.ObjectiveBankQueryRecord record : this.records) {
            if (record.implementsRecordType(objectiveBankRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(objectiveBankRecordType + " is not supported");
    }


    /**
     *  Adds a record to this objective bank query. 
     *
     *  @param objectiveBankQueryRecord objective bank query record
     *  @param objectiveBankRecordType objectiveBank record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addObjectiveBankQueryRecord(org.osid.learning.records.ObjectiveBankQueryRecord objectiveBankQueryRecord, 
                                          org.osid.type.Type objectiveBankRecordType) {

        addRecordType(objectiveBankRecordType);
        nullarg(objectiveBankQueryRecord, "objective bank query record");
        this.records.add(objectiveBankQueryRecord);        
        return;
    }
}
