//
// MutableMapLessonLookupSession
//
//    Implements a Lesson lookup service backed by a collection of
//    lessons that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.plan;


/**
 *  Implements a Lesson lookup service backed by a collection of
 *  lessons. The lessons are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of lessons can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapLessonLookupSession
    extends net.okapia.osid.jamocha.core.course.plan.spi.AbstractMapLessonLookupSession
    implements org.osid.course.plan.LessonLookupSession {


    /**
     *  Constructs a new {@code MutableMapLessonLookupSession}
     *  with no lessons.
     *
     *  @param courseCatalog the course catalog
     *  @throws org.osid.NullArgumentException {@code courseCatalog} is
     *          {@code null}
     */

      public MutableMapLessonLookupSession(org.osid.course.CourseCatalog courseCatalog) {
        setCourseCatalog(courseCatalog);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapLessonLookupSession} with a
     *  single lesson.
     *
     *  @param courseCatalog the course catalog  
     *  @param lesson a lesson
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code lesson} is {@code null}
     */

    public MutableMapLessonLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                           org.osid.course.plan.Lesson lesson) {
        this(courseCatalog);
        putLesson(lesson);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapLessonLookupSession}
     *  using an array of lessons.
     *
     *  @param courseCatalog the course catalog
     *  @param lessons an array of lessons
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code lessons} is {@code null}
     */

    public MutableMapLessonLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                           org.osid.course.plan.Lesson[] lessons) {
        this(courseCatalog);
        putLessons(lessons);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapLessonLookupSession}
     *  using a collection of lessons.
     *
     *  @param courseCatalog the course catalog
     *  @param lessons a collection of lessons
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code lessons} is {@code null}
     */

    public MutableMapLessonLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                           java.util.Collection<? extends org.osid.course.plan.Lesson> lessons) {

        this(courseCatalog);
        putLessons(lessons);
        return;
    }

    
    /**
     *  Makes a {@code Lesson} available in this session.
     *
     *  @param lesson a lesson
     *  @throws org.osid.NullArgumentException {@code lesson{@code  is
     *          {@code null}
     */

    @Override
    public void putLesson(org.osid.course.plan.Lesson lesson) {
        super.putLesson(lesson);
        return;
    }


    /**
     *  Makes an array of lessons available in this session.
     *
     *  @param lessons an array of lessons
     *  @throws org.osid.NullArgumentException {@code lessons{@code 
     *          is {@code null}
     */

    @Override
    public void putLessons(org.osid.course.plan.Lesson[] lessons) {
        super.putLessons(lessons);
        return;
    }


    /**
     *  Makes collection of lessons available in this session.
     *
     *  @param lessons a collection of lessons
     *  @throws org.osid.NullArgumentException {@code lessons{@code  is
     *          {@code null}
     */

    @Override
    public void putLessons(java.util.Collection<? extends org.osid.course.plan.Lesson> lessons) {
        super.putLessons(lessons);
        return;
    }


    /**
     *  Removes a Lesson from this session.
     *
     *  @param lessonId the {@code Id} of the lesson
     *  @throws org.osid.NullArgumentException {@code lessonId{@code 
     *          is {@code null}
     */

    @Override
    public void removeLesson(org.osid.id.Id lessonId) {
        super.removeLesson(lessonId);
        return;
    }    
}
