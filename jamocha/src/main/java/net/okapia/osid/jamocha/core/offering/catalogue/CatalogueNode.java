//
// CatalogueNode.java
//
//     Defines a Catalogue node within an in code hierarchy.
//
//
// Tom Coppeto
// Okapia
// 8 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.offering;


/**
 *  A class for managing a hierarchy of catalogue nodes in core.
 */

public final class CatalogueNode
    extends net.okapia.osid.jamocha.core.offering.spi.AbstractCatalogueNode
    implements org.osid.offering.CatalogueNode {


    /**
     *  Constructs a new <code>CatalogueNode</code> from a single
     *  catalogue.
     *
     *  @param catalogue the catalogue
     *  @throws org.osid.NullArgumentException <code>catalogue</code> is 
     *          <code>null</code>.
     */

    public CatalogueNode(org.osid.offering.Catalogue catalogue) {
        super(catalogue);
        return;
    }


    /**
     *  Constructs a new <code>CatalogueNode</code>.
     *
     *  @param catalogue the catalogue
     *  @param root <code>true</code> if this node is a root, 
     *         <code>false</code> otherwise
     *  @param leaf <code>true</code> if this node is a leaf, 
     *         <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>catalogue</code>
     *          is <code>null</code>.
     */

    public CatalogueNode(org.osid.offering.Catalogue catalogue, boolean root, boolean leaf) {
        super(catalogue, root, leaf);
        return;
    }


    /**
     *  Adds a parent to this catalogue.
     *
     *  @param node the parent to add
     *  @throws org.osid.IllegalStateException this is a root
     *  @throws org.osid.NullArgumentException <code>node</code>
     *          is <code>null</code>
     */

    @Override
    public void addParent(org.osid.offering.CatalogueNode node) {
        super.addParent(node);
        return;
    }


    /**
     *  Adds a parent to this catalogue.
     *
     *  @param catalogue the catalogue to add as a parent
     *  @throws org.osid.NullArgumentException <code>catalogue</code>
     *          is <code>null</code>
     */

    public void addParent(org.osid.offering.Catalogue catalogue) {
        addParent(new CatalogueNode(catalogue));
        return;
    }


    /**
     *  Adds a child to this catalogue.
     *
     *  @param node the child node to add
     *  @throws org.osid.NullArgumentException <code>node</code>
     *          is <code>null</code>
     */

    @Override
    public void addChild(org.osid.offering.CatalogueNode node) {
        super.addChild(node);
        return;
    }


    /**
     *  Adds a child to this catalogue.
     *
     *  @param catalogue the catalogue to add as a child
     *  @throws org.osid.NullArgumentException <code>catalogue</code>
     *          is <code>null</code>
     */

    public void addChild(org.osid.offering.Catalogue catalogue) {
        addChild(new CatalogueNode(catalogue));
        return;
    }
}
