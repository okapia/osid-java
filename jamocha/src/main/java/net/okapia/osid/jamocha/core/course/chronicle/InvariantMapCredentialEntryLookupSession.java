//
// InvariantMapCredentialEntryLookupSession
//
//    Implements a CredentialEntry lookup service backed by a fixed collection of
//    credentialEntries.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.chronicle;


/**
 *  Implements a CredentialEntry lookup service backed by a fixed
 *  collection of credential entries. The credential entries are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapCredentialEntryLookupSession
    extends net.okapia.osid.jamocha.core.course.chronicle.spi.AbstractMapCredentialEntryLookupSession
    implements org.osid.course.chronicle.CredentialEntryLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapCredentialEntryLookupSession</code> with no
     *  credential entries.
     *  
     *  @param courseCatalog the course catalog
     *  @throws org.osid.NullArgumnetException {@code courseCatalog} is
     *          {@code null}
     */

    public InvariantMapCredentialEntryLookupSession(org.osid.course.CourseCatalog courseCatalog) {
        setCourseCatalog(courseCatalog);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapCredentialEntryLookupSession</code> with a single
     *  credential entry.
     *  
     *  @param courseCatalog the course catalog
     *  @param credentialEntry a single credential entry
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code credentialEntry} is <code>null</code>
     */

      public InvariantMapCredentialEntryLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                               org.osid.course.chronicle.CredentialEntry credentialEntry) {
        this(courseCatalog);
        putCredentialEntry(credentialEntry);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapCredentialEntryLookupSession</code> using an array
     *  of credential entries.
     *  
     *  @param courseCatalog the course catalog
     *  @param credentialEntries an array of credential entries
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code credentialEntries} is <code>null</code>
     */

      public InvariantMapCredentialEntryLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                               org.osid.course.chronicle.CredentialEntry[] credentialEntries) {
        this(courseCatalog);
        putCredentialEntries(credentialEntries);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapCredentialEntryLookupSession</code> using a
     *  collection of credential entries.
     *
     *  @param courseCatalog the course catalog
     *  @param credentialEntries a collection of credential entries
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code credentialEntries} is <code>null</code>
     */

      public InvariantMapCredentialEntryLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                               java.util.Collection<? extends org.osid.course.chronicle.CredentialEntry> credentialEntries) {
        this(courseCatalog);
        putCredentialEntries(credentialEntries);
        return;
    }
}
