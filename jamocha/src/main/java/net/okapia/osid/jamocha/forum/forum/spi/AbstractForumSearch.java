//
// AbstractForumSearch.java
//
//     A template for making a Forum Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.forum.forum.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing forum searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractForumSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.forum.ForumSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.forum.records.ForumSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.forum.ForumSearchOrder forumSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of forums. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  forumIds list of forums
     *  @throws org.osid.NullArgumentException
     *          <code>forumIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongForums(org.osid.id.IdList forumIds) {
        while (forumIds.hasNext()) {
            try {
                this.ids.add(forumIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongForums</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of forum Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getForumIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  forumSearchOrder forum search order 
     *  @throws org.osid.NullArgumentException
     *          <code>forumSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>forumSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderForumResults(org.osid.forum.ForumSearchOrder forumSearchOrder) {
	this.forumSearchOrder = forumSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.forum.ForumSearchOrder getForumSearchOrder() {
	return (this.forumSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given forum search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a forum implementing the requested record.
     *
     *  @param forumSearchRecordType a forum search record
     *         type
     *  @return the forum search record
     *  @throws org.osid.NullArgumentException
     *          <code>forumSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(forumSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.forum.records.ForumSearchRecord getForumSearchRecord(org.osid.type.Type forumSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.forum.records.ForumSearchRecord record : this.records) {
            if (record.implementsRecordType(forumSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(forumSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this forum search. 
     *
     *  @param forumSearchRecord forum search record
     *  @param forumSearchRecordType forum search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addForumSearchRecord(org.osid.forum.records.ForumSearchRecord forumSearchRecord, 
                                           org.osid.type.Type forumSearchRecordType) {

        addRecordType(forumSearchRecordType);
        this.records.add(forumSearchRecord);        
        return;
    }
}
