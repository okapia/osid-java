//
// AbstractAvailabilityQuery.java
//
//     A template for making an Availability Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.availability.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for availabilities.
 */

public abstract class AbstractAvailabilityQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.resourcing.AvailabilityQuery {

    private final java.util.Collection<org.osid.resourcing.records.AvailabilityQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the resource <code> Id </code> for this query. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResourceId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the resource <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearResourceIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResourceQuery() {
        throw new org.osid.UnimplementedException("supportsResourceQuery() is false");
    }


    /**
     *  Clears the resource query terms. 
     */

    @OSID @Override
    public void clearResourceTerms() {
        return;
    }


    /**
     *  Sets the job <code> Id </code> for this query. 
     *
     *  @param  jobId the job <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> jobId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchJobId(org.osid.id.Id jobId, boolean match) {
        return;
    }


    /**
     *  Clears the job <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearJobIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> JobQuery </code> is available. 
     *
     *  @return <code> true </code> if a job query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobQuery() {
        return (false);
    }


    /**
     *  Gets the query for a job. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the job query 
     *  @throws org.osid.UnimplementedException <code> supportsJobQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobQuery getJobQuery() {
        throw new org.osid.UnimplementedException("supportsJobQuery() is false");
    }


    /**
     *  Clears the job query terms. 
     */

    @OSID @Override
    public void clearJobTerms() {
        return;
    }


    /**
     *  Sets the competency <code> Id </code> for this query. 
     *
     *  @param  competencyId the competency <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> competencyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCompetencyId(org.osid.id.Id competencyId, boolean match) {
        return;
    }


    /**
     *  Clears the competency <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCompetencyIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CompetencyQuery </code> is available. 
     *
     *  @return <code> true </code> if a competency query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompetencyQuery() {
        return (false);
    }


    /**
     *  Gets the query for a competency. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the competency query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencyQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyQuery getCompetencyQuery() {
        throw new org.osid.UnimplementedException("supportsCompetencyQuery() is false");
    }


    /**
     *  Clears the competency query terms. 
     */

    @OSID @Override
    public void clearCompetencyTerms() {
        return;
    }


    /**
     *  Matches percentages within the given range inclusive. 
     *
     *  @param  low start range 
     *  @param  high end range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchPercentage(long low, long high, boolean match) {
        return;
    }


    /**
     *  Clears the percentage query terms. 
     */

    @OSID @Override
    public void clearPercentageTerms() {
        return;
    }


    /**
     *  Sets the foundry <code> Id </code> for this query to match 
     *  availabilities assigned to foundries. 
     *
     *  @param  foundryId the foundry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchFoundryId(org.osid.id.Id foundryId, boolean match) {
        return;
    }


    /**
     *  Clears the foundry <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearFoundryIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> FoundryQuery </code> is available. 
     *
     *  @return <code> true </code> if a foundry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFoundryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a foundry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the foundry query 
     *  @throws org.osid.UnimplementedException <code> supportsFoundryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQuery getFoundryQuery() {
        throw new org.osid.UnimplementedException("supportsFoundryQuery() is false");
    }


    /**
     *  Clears the foundry query terms. 
     */

    @OSID @Override
    public void clearFoundryTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given availability query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an availability implementing the requested record.
     *
     *  @param availabilityRecordType an availability record type
     *  @return the availability query record
     *  @throws org.osid.NullArgumentException
     *          <code>availabilityRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(availabilityRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.AvailabilityQueryRecord getAvailabilityQueryRecord(org.osid.type.Type availabilityRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.records.AvailabilityQueryRecord record : this.records) {
            if (record.implementsRecordType(availabilityRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(availabilityRecordType + " is not supported");
    }


    /**
     *  Adds a record to this availability query. 
     *
     *  @param availabilityQueryRecord availability query record
     *  @param availabilityRecordType availability record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAvailabilityQueryRecord(org.osid.resourcing.records.AvailabilityQueryRecord availabilityQueryRecord, 
                                          org.osid.type.Type availabilityRecordType) {

        addRecordType(availabilityRecordType);
        nullarg(availabilityQueryRecord, "availability query record");
        this.records.add(availabilityQueryRecord);        
        return;
    }
}
