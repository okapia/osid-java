//
// AbstractImmutableActionGroup.java
//
//     Wraps a mutable ActionGroup to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.control.actiongroup.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>ActionGroup</code> to hide modifiers. This
 *  wrapper provides an immutized ActionGroup from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying actionGroup whose state changes are visible.
 */

public abstract class AbstractImmutableActionGroup
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.control.ActionGroup {

    private final org.osid.control.ActionGroup actionGroup;


    /**
     *  Constructs a new <code>AbstractImmutableActionGroup</code>.
     *
     *  @param actionGroup the action group to immutablize
     *  @throws org.osid.NullArgumentException <code>actionGroup</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableActionGroup(org.osid.control.ActionGroup actionGroup) {
        super(actionGroup);
        this.actionGroup = actionGroup;
        return;
    }


    /**
     *  Gets the action <code> Ids. </code> 
     *
     *  @return the action <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getActionIds() {
        return (this.actionGroup.getActionIds());
    }


    /**
     *  Gets the actions. 
     *
     *  @return the actions 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.control.ActionList getActions()
        throws org.osid.OperationFailedException {

        return (this.actionGroup.getActions());
    }


    /**
     *  Gets the action group record corresponding to the given <code> 
     *  ActionGroup </code> record <code> Type. </code> This method is used to 
     *  retrieve an object implementing the requested record. The <code> 
     *  actionGroupRecordType </code> may be the <code> Type </code> returned 
     *  in <code> getRecordTypes() </code> or any of its parents in a <code> 
     *  Type </code> hierarchy where <code> 
     *  hasRecordType(actionGroupRecordType) </code> is <code> true </code> . 
     *
     *  @param  actionGroupRecordType the type of action group record to 
     *          retrieve 
     *  @return the action group record 
     *  @throws org.osid.NullArgumentException <code> actionGroupRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(actionGroupRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.records.ActionGroupRecord getActionGroupRecord(org.osid.type.Type actionGroupRecordType)
        throws org.osid.OperationFailedException {

        return (this.actionGroup.getActionGroupRecord(actionGroupRecordType));
    }
}

