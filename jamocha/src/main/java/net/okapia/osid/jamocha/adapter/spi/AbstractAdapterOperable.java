//
// AbstractAdapterOperable.java
//
//     Defines an Operable wrapper.
//
//
// Tom Coppeto
// Okapia
// 20 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an Operable wrapper.
 */

public abstract class AbstractAdapterOperable
    implements org.osid.Operable {

    private final org.osid.Operable operable;


    /**
     *  Creates a new <code>AbstractAdapterOperable</code>.
     *
     *  @param operable the <code>Operable</code> for this object
     *  @throws org.osid.NullArgumentException <code>operable</code>
     *          is <code>null</code>
     */

    protected AbstractAdapterOperable(org.osid.Operable operable) {
        this.operable = operable;
        return;
    }


    /**
     *  Tests if this object is active. <code> isActive() </code> is
     *  <code> true </code> if <code> isEnabled() </code> and <code>
     *  isOperational() </code> are <code> true </code> and <code>
     *  isDisabled() </code> is <code> false. </code>
     *
     *  @return <code> true </code> if this object is active, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isActive() {
        return (this.operable.isActive());
    }

    
    /**
     *  Tests if this object is administravely
     *  enabled. Administratively enabling overrides any enabling rule
     *  which may exist. If this method returns <code> true </code>
     *  then <code> isDisabled() </code> must return <code>
     *  false. </code>
     *
     *  @return <code> true </code> if this object is enabled, <code>
     *          false </code> is the active status is determined by
     *          other rules
     */

    @OSID @Override
    public boolean isEnabled() {
        return (this.operable.isEnabled());
    }


    /**
     *  Tests if this object is administravely
     *  disabled. Administratively disabling overrides any disabling
     *  rule which may exist. If this method returns <code> true
     *  </code> then <code> isDisabled() </code> must return <code>
     *  false. </code>
     *
     *  @return <code> true </code> if this object is disabled, <code> false 
     *          </code> is the active status is determined by other rules 
     */

    @OSID @Override
    public boolean isDisabled() {
        return (this.operable.isDisabled());
    }


    /**
     *  Tests if this object is operational in that all rules
     *  pertaining to this operation except for an administrative
     *  disable are <code> true.  </code>
     *
     *  @return <code> true </code> if this object is operational, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean isOperational() {
        return (this.operable.isOperational());
    }
}

