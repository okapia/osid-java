//
// AbstractQueryAddressLookupSession.java
//
//    An AddressQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.contact.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An AddressQuerySession adapter.
 */

public abstract class AbstractAdapterAddressQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.contact.AddressQuerySession {

    private final org.osid.contact.AddressQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterAddressQuerySession.
     *
     *  @param session the underlying address query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterAddressQuerySession(org.osid.contact.AddressQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeAddressBook</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeAddressBook Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAddressBookId() {
        return (this.session.getAddressBookId());
    }


    /**
     *  Gets the {@codeAddressBook</code> associated with this 
     *  session.
     *
     *  @return the {@codeAddressBook</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressBook getAddressBook()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getAddressBook());
    }


    /**
     *  Tests if this user can perform {@codeAddress</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchAddresses() {
        return (this.session.canSearchAddresses());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include addresses in address books which are children
     *  of this address book in the address book hierarchy.
     */

    @OSID @Override
    public void useFederatedAddressBookView() {
        this.session.useFederatedAddressBookView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this address book only.
     */
    
    @OSID @Override
    public void useIsolatedAddressBookView() {
        this.session.useIsolatedAddressBookView();
        return;
    }
    
      
    /**
     *  Gets an address query. The returned query will not have an
     *  extension query.
     *
     *  @return the address query 
     */
      
    @OSID @Override
    public org.osid.contact.AddressQuery getAddressQuery() {
        return (this.session.getAddressQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  addressQuery the address query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code addressQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code addressQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.contact.AddressList getAddressesByQuery(org.osid.contact.AddressQuery addressQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getAddressesByQuery(addressQuery));
    }
}
