//
// AbstractSupersedingEventEnablerSearchOdrer.java
//
//     Defines a SupersedingEventEnablerSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.rules.supersedingeventenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code SupersedingEventEnablerSearchOrder}.
 */

public abstract class AbstractSupersedingEventEnablerSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerSearchOrder
    implements org.osid.calendaring.rules.SupersedingEventEnablerSearchOrder {

    private final java.util.Collection<org.osid.calendaring.rules.records.SupersedingEventEnablerSearchOrderRecord> records = new java.util.LinkedHashSet<>();



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  supersedingEventEnablerRecordType a superseding event enabler record type 
     *  @return {@code true} if the supersedingEventEnablerRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code supersedingEventEnablerRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type supersedingEventEnablerRecordType) {
        for (org.osid.calendaring.rules.records.SupersedingEventEnablerSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(supersedingEventEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  supersedingEventEnablerRecordType the superseding event enabler record type 
     *  @return the superseding event enabler search order record
     *  @throws org.osid.NullArgumentException
     *          {@code supersedingEventEnablerRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(supersedingEventEnablerRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.calendaring.rules.records.SupersedingEventEnablerSearchOrderRecord getSupersedingEventEnablerSearchOrderRecord(org.osid.type.Type supersedingEventEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.rules.records.SupersedingEventEnablerSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(supersedingEventEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(supersedingEventEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this superseding event enabler. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param supersedingEventEnablerRecord the superseding event enabler search odrer record
     *  @param supersedingEventEnablerRecordType superseding event enabler record type
     *  @throws org.osid.NullArgumentException
     *          {@code supersedingEventEnablerRecord} or
     *          {@code supersedingEventEnablerRecordTypesupersedingEventEnabler} is
     *          {@code null}
     */
            
    protected void addSupersedingEventEnablerRecord(org.osid.calendaring.rules.records.SupersedingEventEnablerSearchOrderRecord supersedingEventEnablerSearchOrderRecord, 
                                     org.osid.type.Type supersedingEventEnablerRecordType) {

        addRecordType(supersedingEventEnablerRecordType);
        this.records.add(supersedingEventEnablerSearchOrderRecord);
        
        return;
    }
}
