//
// ProcessElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.process.process.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ProcessElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the ProcessElement Id.
     *
     *  @return the process element Id
     */

    public static org.osid.id.Id getProcessEntityId() {
        return (makeEntityId("osid.process.Process"));
    }


    /**
     *  Gets the StateId element Id.
     *
     *  @return the StateId element Id
     */

    public static org.osid.id.Id getStateId() {
        return (makeQueryElementId("osid.process.process.StateId"));
    }


    /**
     *  Gets the State element Id.
     *
     *  @return the State element Id
     */

    public static org.osid.id.Id getState() {
        return (makeQueryElementId("osid.process.process.State"));
    }


    /**
     *  Gets the AncestorProcessId element Id.
     *
     *  @return the AncestorProcessId element Id
     */

    public static org.osid.id.Id getAncestorProcessId() {
        return (makeQueryElementId("osid.process.process.AncestorProcessId"));
    }


    /**
     *  Gets the AncestorProcess element Id.
     *
     *  @return the AncestorProcess element Id
     */

    public static org.osid.id.Id getAncestorProcess() {
        return (makeQueryElementId("osid.process.process.AncestorProcess"));
    }


    /**
     *  Gets the DescendantProcessId element Id.
     *
     *  @return the DescendantProcessId element Id
     */

    public static org.osid.id.Id getDescendantProcessId() {
        return (makeQueryElementId("osid.process.process.DescendantProcessId"));
    }


    /**
     *  Gets the DescendantProcess element Id.
     *
     *  @return the DescendantProcess element Id
     */

    public static org.osid.id.Id getDescendantProcess() {
        return (makeQueryElementId("osid.process.process.DescendantProcess"));
    }
}
