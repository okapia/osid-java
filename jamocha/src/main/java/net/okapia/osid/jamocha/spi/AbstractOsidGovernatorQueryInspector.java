//
// AbstractOsidGovernatorQueryInspector.java
//
//     Defines an OsidGovernatorQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 1 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an OsidGovernatorQueryInspector to extend. 
 */

public abstract class AbstractOsidGovernatorQueryInspector
    extends AbstractSourceableOsidObjectQueryInspector
    implements org.osid.OsidGovernatorQueryInspector {

    private final OsidOperableQueryInspector inspector = new OsidOperableQueryInspector();


    /**
     *  Gets the active query terms.
     *
     *  @return the active terms
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getActiveTerms() {
        return (this.inspector.getActiveTerms());
    }


    /**
     *  Gets the enabled query terms.
     *
     *  @return the enabled terms
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getEnabledTerms() {
        return (this.inspector.getEnabledTerms());
    }


    /**
     *  Gets the disabled query terms.
     *
     *  @return the disabled terms
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getDisabledTerms() {
        return (this.inspector.getDisabledTerms());
    }


    /**
     *  Gets the operational query terms.
     *
     *  @return the operational terms
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getOperationalTerms() {
        return (this.inspector.getOperationalTerms());
    }


    protected class OsidOperableQueryInspector
        extends AbstractOsidOperableQueryInspector
        implements org.osid.OsidOperableQueryInspector {
    }
}

