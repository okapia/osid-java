//
// AbstractAdapterRequisiteLookupSession.java
//
//    A Requisite lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.course.requisite.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Requisite lookup session adapter.
 */

public abstract class AbstractAdapterRequisiteLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.course.requisite.RequisiteLookupSession {

    private final org.osid.course.requisite.RequisiteLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterRequisiteLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterRequisiteLookupSession(org.osid.course.requisite.RequisiteLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code CourseCatalog/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code CourseCatalog Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.session.getCourseCatalogId());
    }


    /**
     *  Gets the {@code CourseCatalog} associated with this session.
     *
     *  @return the {@code CourseCatalog} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCourseCatalog());
    }


    /**
     *  Tests if this user can perform {@code Requisite} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupRequisites() {
        return (this.session.canLookupRequisites());
    }


    /**
     *  A complete view of the {@code Requisite} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRequisiteView() {
        this.session.useComparativeRequisiteView();
        return;
    }


    /**
     *  A complete view of the {@code Requisite} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRequisiteView() {
        this.session.usePlenaryRequisiteView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include requisites in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.session.useFederatedCourseCatalogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.session.useIsolatedCourseCatalogView();
        return;
    }
    

    /**
     *  Only active requisites are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveRequisiteView() {
        this.session.useActiveRequisiteView();
        return;
    }


    /**
     *  Active and inactive requisites are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusRequisiteView() {
        this.session.useAnyStatusRequisiteView();
        return;
    }
    

    /**
     *  The returns from the lookup methods omit sequestered
     *  requisites.
     */

    @OSID @Override
    public void useSequesteredRequisiteView() {
        this.session.useSequesteredRequisiteView();
        return;
    }


    /**
     *  All requisites are returned including sequestered requisites.
     */

    @OSID @Override
    public void useUnsequesteredRequisiteView() {
        this.session.useUnsequesteredRequisiteView();
        return;
    }

     
    /**
     *  Gets the {@code Requisite} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Requisite} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Requisite} and
     *  retained for compatibility.
     *
     *  In active mode, requisites are returned that are currently
     *  active. In any status mode, active and inactive requisites
     *  are returned.
     *
     *  @param requisiteId {@code Id} of the {@code Requisite}
     *  @return the requisite
     *  @throws org.osid.NotFoundException {@code requisiteId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code requisiteId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.Requisite getRequisite(org.osid.id.Id requisiteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRequisite(requisiteId));
    }


    /**
     *  Gets a {@code RequisiteList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  requisites specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Requisites} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, requisites are returned that are currently
     *  active. In any status mode, active and inactive requisites
     *  are returned.
     *
     *  @param  requisiteIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Requisite} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code requisiteIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getRequisitesByIds(org.osid.id.IdList requisiteIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRequisitesByIds(requisiteIds));
    }


    /**
     *  Gets a {@code RequisiteList} corresponding to the given
     *  requisite genus {@code Type} which does not include
     *  requisites of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  requisites or an error results. Otherwise, the returned list
     *  may contain only those requisites that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, requisites are returned that are currently
     *  active. In any status mode, active and inactive requisites
     *  are returned.
     *
     *  @param  requisiteGenusType a requisite genus type 
     *  @return the returned {@code Requisite} list
     *  @throws org.osid.NullArgumentException
     *          {@code requisiteGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getRequisitesByGenusType(org.osid.type.Type requisiteGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRequisitesByGenusType(requisiteGenusType));
    }


    /**
     *  Gets a {@code RequisiteList} corresponding to the given
     *  requisite genus {@code Type} and include any additional
     *  requisites with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  requisites or an error results. Otherwise, the returned list
     *  may contain only those requisites that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, requisites are returned that are currently
     *  active. In any status mode, active and inactive requisites
     *  are returned.
     *
     *  @param  requisiteGenusType a requisite genus type 
     *  @return the returned {@code Requisite} list
     *  @throws org.osid.NullArgumentException
     *          {@code requisiteGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getRequisitesByParentGenusType(org.osid.type.Type requisiteGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRequisitesByParentGenusType(requisiteGenusType));
    }


    /**
     *  Gets a {@code RequisiteList} containing the given
     *  requisite record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  requisites or an error results. Otherwise, the returned list
     *  may contain only those requisites that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, requisites are returned that are currently
     *  active. In any status mode, active and inactive requisites
     *  are returned.
     *
     *  @param  requisiteRecordType a requisite record type 
     *  @return the returned {@code Requisite} list
     *  @throws org.osid.NullArgumentException
     *          {@code requisiteRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getRequisitesByRecordType(org.osid.type.Type requisiteRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRequisitesByRecordType(requisiteRecordType));
    }



    /**
     *  Gets a {@code RequisiteList} immediately containing the given 
     *  requisite option. 
     *  
     *  In plenary mode, the returned list contains all known requisites or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  requisites that are accessible through this session. 
     *  
     *  In active mode, requisites are returned that are currently active. In 
     *  any status mode, active and inactive requisites are returned. 
     *  
     *  In sequestered mode, no sequestered requisites are returned. In 
     *  unsequestered mode, all requisites are returned. 
     *
     *  @param  requisiteOptionId a requisite option {@code Id} 
     *  @return the returned {@code RequisiteList} 
     *  @throws org.osid.NullArgumentException {@code
     *         requisiteOptionId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getRequisitesForRequisiteOption(org.osid.id.Id requisiteOptionId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getRequisitesForRequisiteOption(requisiteOptionId));
    }


    /**
     *  Gets all {@code Requisites}. 
     *
     *  In plenary mode, the returned list contains all known
     *  requisites or an error results. Otherwise, the returned list
     *  may contain only those requisites that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, requisites are returned that are currently
     *  active. In any status mode, active and inactive requisites
     *  are returned.
     *
     *  @return a list of {@code Requisites} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getRequisites()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRequisites());
    }


    /**
     *  Gets the {@code CourseRequirement} specified by its {@code
     *  Id}.
     *  
     *  {@code} In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned {@code
     *  CourseRequirement} may have a different {@code Id} than
     *  requested, such as the case where a duplicate {@code Id} was
     *  assigned to a {@code CourseRequirement} and retained for
     *  compatibility.
     *  
     *  In active mode, course requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  course requirements are returned.
     *
     *  @param  courseRequirementId the {@code Id} of the {@code 
     *          CourseRequirement} to retrieve 
     *  @return the returned {@code CourseRequirement} 
     *  @throws org.osid.NotFoundException no {@code CourseRequirement} 
     *          found with the given {@code Id} 
     *  @throws org.osid.NullArgumentException {@code
     *         courseRequirementId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.CourseRequirement getCourseRequirement(org.osid.id.Id courseRequirementId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCourseRequirement(courseRequirementId));
    }


    /**
     *  Gets a {@code CourseRequirementList} corresponding to the given 
     *  {@code IdList.} 
     *  
     *  {@code} In plenary mode, the returned list contains all of the 
     *  course requirements specified in the {@code Id} list, in the 
     *  order of the list, including duplicates, or an error results if an 
     *  {@code Id} in the supplied list is not found or inaccessible. 
     *  Otherwise, inaccessible {@code CourseRequirements} may be 
     *  omitted from the list and may present the elements in any order 
     *  including returning a unique set. 
     *  
     *  In active mode, course requirements are returned that are currently 
     *  active. In any status mode, active and inactive course requirements 
     *  are returned. 
     *
     *  @param  courseRequirementIds the list of {@code Ids} to 
     *          retrieve 
     *  @return the returned {@code CourseRequirementList} list 
     *  @throws org.osid.NotFoundException an {@code Id was} not found 
     *  @throws org.osid.NullArgumentException {@code
     *         courseRequirementIds} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.CourseRequirementList getCourseRequirementsByIds(org.osid.id.IdList courseRequirementIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCourseRequirementsByIds(courseRequirementIds));
    }


    /**
     *  Gets a {@code CourseRequirementList} corresponding to the
     *  given course requirement genus {@code Type} which does not
     *  include course requirements of types derived from the
     *  specified {@code Type.}
     *  
     *  In plenary mode, the returned list contains all known course
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those course requirements that are accessible
     *  through this session.
     *  
     *  In active mode, course requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  course requirements are returned.
     *
     *  @param  courseRequirementGenusType a course requirement genus type 
     *  @return the returned {@code CourseRequirementList} 
     *  @throws org.osid.NullArgumentException {@code 
     *          courseRequirementGenusType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.CourseRequirementList getCourseRequirementsByGenusType(org.osid.type.Type courseRequirementGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.getCourseRequirementsByGenusType(courseRequirementGenusType));
    }

    
    /**
     *  Gets a {@code CourseRequirementList} corresponding to the
     *  given course requirement genus {@code Type} and include any
     *  additional course requirements with genus types derived from
     *  the specified {@code Type}.
     *  
     *  In plenary mode, the returned list contains all known course
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those course requirements that are accessible
     *  through this session.
     *  
     *  In active mode, course requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  course requirements are returned.
     *
     *  @param  courseRequirementGenusType a course requirements genus type 
     *  @return the returned {@code CourseRequirementList} 
     *  @throws org.osid.NullArgumentException {@code 
     *          courseRequirementGenusType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.CourseRequirementList getCourseRequirementsByParentGenusType(org.osid.type.Type courseRequirementGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.getCourseRequirementsByParentGenusType(courseRequirementGenusType));
    }


    /**
     *  Gets a {@code CourseRequirementList} containing the given
     *  course requirement record {@code Type.}
     *  
     *  In plenary mode, the returned list contains all known course
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those course requirements that are accessible
     *  through this session.
     *  
     *  In active mode, course requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  course requirements are returned.
     *
     *  @param  courseRequirementRecordType a course requirement record type 
     *  @return the returned {@code CourseRequirementList} 
     *  @throws org.osid.NullArgumentException {@code 
     *          courseRequirementRecordType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.CourseRequirementList getCourseRequirementsByRecordType(org.osid.type.Type courseRequirementRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.getCourseRequirementsByRecordType(courseRequirementRecordType));
    }

    
    /**
     *  Gets a {@code CourseRequirementList} containing the given
     *  course.
     *  
     *  In plenary mode, the returned list contains all known course
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those course requirements that are accessible
     *  through this session.
     *  
     *  In active mode, course requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  course requirements are returned.
     *
     *  @param  courseId a course {@code Id} 
     *  @return the returned {@code CourseRequirementList} 
     *  @throws org.osid.NullArgumentException {@code courseId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.CourseRequirementList getCourseRequirementsByCourse(org.osid.id.Id courseId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCourseRequirementsByCourse(courseId));
    }

    
    /**
     *  Gets a {@code CourseRequirementList} with the given {@code
     *  Requisite}.
     *  
     *  In plenary mode, the returned list contains all known course
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those course requirements that are accessible
     *  through this session.
     *  
     *  In active mode, course requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  course requirements are returned.
     *
     *  @param  requisiteId a requisite {@code Id} 
     *  @return the returned {@code CourseRequirementList} 
     *  @throws org.osid.NullArgumentException {@code requisiteId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.CourseRequirementList getCourseRequirementsByAltRequisite(org.osid.id.Id requisiteId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCourseRequirementsByAltRequisite(requisiteId));
    }


    /**
     *  Gets a {@code RequisiteList} immediately containing the given
     *  course requirement.
     *  
     *  In plenary mode, the returned list contains all known
     *  requisites or an error results. Otherwise, the returned list
     *  may contain only those requisites that are accessible through
     *  this session.
     *  
     *  In active mode, course requirements are processed and
     *  requisites are returned that are currently active. In any
     *  status mode, active and inactive requisites are returned.
     *
     *  @param  courseRequirementId a course requirement {@code Id} 
     *  @return the returned {@code RequisiteList} 
     *  @throws org.osid.NullArgumentException {@code
     *         courseRequirementId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getRequisitesForCourseRequirement(org.osid.id.Id courseRequirementId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRequisitesForCourseRequirement(courseRequirementId));
    }    


    /**
     *  Gets all {@code CourseRequirements.} 
     *  
     *  In plenary mode, the returned list contains all known course 
     *  requirements or an error results. Otherwise, the returned list may 
     *  contain only those course requirements that are accessible through this 
     *  session. 
     *  
     *  In active mode, course requirements are returned that are currently 
     *  active. In any status mode, active and inactive course requirements are 
     *  returned. 
     *
     *  @return a list of {@code CourseRequirements} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.CourseRequirementList getCourseRequirements()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCourseRequirements());
    }


    /**
     *  Gets the {@code ProgramRequirement} specified by its {@code
     *  Id}.
     *  
     *  {@code} In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned {@code
     *  ProgramRequirement} may have a different {@code Id} than
     *  requested, such as the case where a duplicate {@code Id} was
     *  assigned to a {@code ProgramRequirement} and retained for
     *  compatibility.
     *  
     *  In active mode, program requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  program requirements are returned.
     *
     *  @param  programRequirementId the {@code Id} of the {@code 
     *          ProgramRequirement} to retrieve 
     *  @return the returned {@code ProgramRequirement} 
     *  @throws org.osid.NotFoundException no {@code ProgramRequirement} 
     *          found with the given {@code Id} 
     *  @throws org.osid.NullArgumentException {@code
     *         programRequirementId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.ProgramRequirement getProgramRequirement(org.osid.id.Id programRequirementId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProgramRequirement(programRequirementId));
    }


    /**
     *  Gets a {@code ProgramRequirementList} corresponding to the given 
     *  {@code IdList.} 
     *  
     *  {@code} In plenary mode, the returned list contains all of the 
     *  program requirements specified in the {@code Id} list, in the 
     *  order of the list, including duplicates, or an error results if an 
     *  {@code Id} in the supplied list is not found or inaccessible. 
     *  Otherwise, inaccessible {@code ProgramRequirements} may be 
     *  omitted from the list and may present the elements in any order 
     *  including returning a unique set. 
     *  
     *  In active mode, program requirements are returned that are currently 
     *  active. In any status mode, active and inactive program requirements 
     *  are returned. 
     *
     *  @param  programRequirementIds the list of {@code Ids} to 
     *          retrieve 
     *  @return the returned {@code ProgramRequirementList} list 
     *  @throws org.osid.NotFoundException an {@code Id was} not found 
     *  @throws org.osid.NullArgumentException {@code
     *         programRequirementIds} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.ProgramRequirementList getProgramRequirementsByIds(org.osid.id.IdList programRequirementIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProgramRequirementsByIds(programRequirementIds));
    }


    /**
     *  Gets a {@code ProgramRequirementList} corresponding to the
     *  given program requirement genus {@code Type} which does not
     *  include program requirements of types derived from the
     *  specified {@code Type.}
     *  
     *  In plenary mode, the returned list contains all known program
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those program requirements that are accessible
     *  through this session.
     *  
     *  In active mode, program requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  program requirements are returned.
     *
     *  @param  programRequirementGenusType a program requirement genus type 
     *  @return the returned {@code ProgramRequirementList} 
     *  @throws org.osid.NullArgumentException {@code 
     *          programRequirementGenusType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.ProgramRequirementList getProgramRequirementsByGenusType(org.osid.type.Type programRequirementGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.getProgramRequirementsByGenusType(programRequirementGenusType));
    }

    
    /**
     *  Gets a {@code ProgramRequirementList} corresponding to the
     *  given program requirement genus {@code Type} and include any
     *  additional program requirements with genus types derived from
     *  the specified {@code Type}.
     *  
     *  In plenary mode, the returned list contains all known program
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those program requirements that are accessible
     *  through this session.
     *  
     *  In active mode, program requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  program requirements are returned.
     *
     *  @param  programRequirementGenusType a program requirements genus type 
     *  @return the returned {@code ProgramRequirementList} 
     *  @throws org.osid.NullArgumentException {@code 
     *          programRequirementGenusType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.ProgramRequirementList getProgramRequirementsByParentGenusType(org.osid.type.Type programRequirementGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.getProgramRequirementsByParentGenusType(programRequirementGenusType));
    }


    /**
     *  Gets a {@code ProgramRequirementList} containing the given
     *  program requirement record {@code Type.}
     *  
     *  In plenary mode, the returned list contains all known program
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those program requirements that are accessible
     *  through this session.
     *  
     *  In active mode, program requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  program requirements are returned.
     *
     *  @param  programRequirementRecordType a program requirement record type 
     *  @return the returned {@code ProgramRequirementList} 
     *  @throws org.osid.NullArgumentException {@code 
     *          programRequirementRecordType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.ProgramRequirementList getProgramRequirementsByRecordType(org.osid.type.Type programRequirementRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.getProgramRequirementsByRecordType(programRequirementRecordType));
    }

    
    /**
     *  Gets a {@code ProgramRequirementList} containing the given
     *  program.
     *  
     *  In plenary mode, the returned list contains all known program
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those program requirements that are accessible
     *  through this session.
     *  
     *  In active mode, program requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  program requirements are returned.
     *
     *  @param  programId a program {@code Id} 
     *  @return the returned {@code ProgramRequirementList} 
     *  @throws org.osid.NullArgumentException {@code programId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.ProgramRequirementList getProgramRequirementsByProgram(org.osid.id.Id programId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProgramRequirementsByProgram(programId));
    }

    
    /**
     *  Gets a {@code ProgramRequirementList} with the given {@code
     *  Requisite}.
     *  
     *  In plenary mode, the returned list contains all known program
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those program requirements that are accessible
     *  through this session.
     *  
     *  In active mode, program requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  program requirements are returned.
     *
     *  @param  requisiteId a requisite {@code Id} 
     *  @return the returned {@code ProgramRequirementList} 
     *  @throws org.osid.NullArgumentException {@code requisiteId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.ProgramRequirementList getProgramRequirementsByAltRequisite(org.osid.id.Id requisiteId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProgramRequirementsByAltRequisite(requisiteId));
    }


    /**
     *  Gets a {@code RequisiteList} immediately containing the given
     *  program requirement.
     *  
     *  In plenary mode, the returned list contains all known
     *  requisites or an error results. Otherwise, the returned list
     *  may contain only those requisites that are accessible through
     *  this session.
     *  
     *  In active mode, program requirements are processed and
     *  requisites are returned that are currently active. In any
     *  status mode, active and inactive requisites are returned.
     *
     *  @param  programRequirementId a program requirement {@code Id} 
     *  @return the returned {@code RequisiteList} 
     *  @throws org.osid.NullArgumentException {@code
     *         programRequirementId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getRequisitesForProgramRequirement(org.osid.id.Id programRequirementId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRequisitesForProgramRequirement(programRequirementId));
    }


    /**
     *  Gets all {@code ProgramRequirements.} 
     *  
     *  In plenary mode, the returned list contains all known program 
     *  requirements or an error results. Otherwise, the returned list may 
     *  contain only those program requirements that are accessible through this 
     *  session. 
     *  
     *  In active mode, program requirements are returned that are currently 
     *  active. In any status mode, active and inactive program requirements are 
     *  returned. 
     *
     *  @return a list of {@code ProgramRequirements} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.ProgramRequirementList getProgramRequirements()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProgramRequirements());
    }


    /**
     *  Gets the {@code CredentialRequirement} specified by its {@code
     *  Id}.
     *  
     *  {@code} In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned {@code
     *  CredentialRequirement} may have a different {@code Id} than
     *  requested, such as the case where a duplicate {@code Id} was
     *  assigned to a {@code CredentialRequirement} and retained for
     *  compatibility.
     *  
     *  In active mode, credential requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  credential requirements are returned.
     *
     *  @param  credentialRequirementId the {@code Id} of the {@code 
     *          CredentialRequirement} to retrieve 
     *  @return the returned {@code CredentialRequirement} 
     *  @throws org.osid.NotFoundException no {@code CredentialRequirement} 
     *          found with the given {@code Id} 
     *  @throws org.osid.NullArgumentException {@code
     *         credentialRequirementId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.CredentialRequirement getCredentialRequirement(org.osid.id.Id credentialRequirementId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCredentialRequirement(credentialRequirementId));
    }


    /**
     *  Gets a {@code CredentialRequirementList} corresponding to the given 
     *  {@code IdList.} 
     *  
     *  {@code} In plenary mode, the returned list contains all of the 
     *  credential requirements specified in the {@code Id} list, in the 
     *  order of the list, including duplicates, or an error results if an 
     *  {@code Id} in the supplied list is not found or inaccessible. 
     *  Otherwise, inaccessible {@code CredentialRequirements} may be 
     *  omitted from the list and may present the elements in any order 
     *  including returning a unique set. 
     *  
     *  In active mode, credential requirements are returned that are currently 
     *  active. In any status mode, active and inactive credential requirements 
     *  are returned. 
     *
     *  @param  credentialRequirementIds the list of {@code Ids} to 
     *          retrieve 
     *  @return the returned {@code CredentialRequirementList} list 
     *  @throws org.osid.NotFoundException an {@code Id was} not found 
     *  @throws org.osid.NullArgumentException {@code
     *         credentialRequirementIds} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.CredentialRequirementList getCredentialRequirementsByIds(org.osid.id.IdList credentialRequirementIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCredentialRequirementsByIds(credentialRequirementIds));
    }


    /**
     *  Gets a {@code CredentialRequirementList} corresponding to the
     *  given credential requirement genus {@code Type} which does not
     *  include credential requirements of types derived from the
     *  specified {@code Type.}
     *  
     *  In plenary mode, the returned list contains all known credential
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those credential requirements that are accessible
     *  through this session.
     *  
     *  In active mode, credential requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  credential requirements are returned.
     *
     *  @param  credentialRequirementGenusType a credential requirement genus type 
     *  @return the returned {@code CredentialRequirementList} 
     *  @throws org.osid.NullArgumentException {@code 
     *          credentialRequirementGenusType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.CredentialRequirementList getCredentialRequirementsByGenusType(org.osid.type.Type credentialRequirementGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.getCredentialRequirementsByGenusType(credentialRequirementGenusType));
    }

    
    /**
     *  Gets a {@code CredentialRequirementList} corresponding to the
     *  given credential requirement genus {@code Type} and include any
     *  additional credential requirements with genus types derived from
     *  the specified {@code Type}.
     *  
     *  In plenary mode, the returned list contains all known credential
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those credential requirements that are accessible
     *  through this session.
     *  
     *  In active mode, credential requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  credential requirements are returned.
     *
     *  @param  credentialRequirementGenusType a credential requirements genus type 
     *  @return the returned {@code CredentialRequirementList} 
     *  @throws org.osid.NullArgumentException {@code 
     *          credentialRequirementGenusType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.CredentialRequirementList getCredentialRequirementsByParentGenusType(org.osid.type.Type credentialRequirementGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.getCredentialRequirementsByParentGenusType(credentialRequirementGenusType));
    }


    /**
     *  Gets a {@code CredentialRequirementList} containing the given
     *  credential requirement record {@code Type.}
     *  
     *  In plenary mode, the returned list contains all known credential
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those credential requirements that are accessible
     *  through this session.
     *  
     *  In active mode, credential requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  credential requirements are returned.
     *
     *  @param  credentialRequirementRecordType a credential requirement record type 
     *  @return the returned {@code CredentialRequirementList} 
     *  @throws org.osid.NullArgumentException {@code 
     *          credentialRequirementRecordType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.CredentialRequirementList getCredentialRequirementsByRecordType(org.osid.type.Type credentialRequirementRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.getCredentialRequirementsByRecordType(credentialRequirementRecordType));
    }

    
    /**
     *  Gets a {@code CredentialRequirementList} containing the given
     *  credential.
     *  
     *  In plenary mode, the returned list contains all known credential
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those credential requirements that are accessible
     *  through this session.
     *  
     *  In active mode, credential requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  credential requirements are returned.
     *
     *  @param  credentialId a credential {@code Id} 
     *  @return the returned {@code CredentialRequirementList} 
     *  @throws org.osid.NullArgumentException {@code credentialId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.CredentialRequirementList getCredentialRequirementsByCredential(org.osid.id.Id credentialId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCredentialRequirementsByCredential(credentialId));
    }

    
    /**
     *  Gets a {@code CredentialRequirementList} with the given {@code
     *  Requisite}.
     *  
     *  In plenary mode, the returned list contains all known credential
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those credential requirements that are accessible
     *  through this session.
     *  
     *  In active mode, credential requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  credential requirements are returned.
     *
     *  @param  requisiteId a requisite {@code Id} 
     *  @return the returned {@code CredentialRequirementList} 
     *  @throws org.osid.NullArgumentException {@code requisiteId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.CredentialRequirementList getCredentialRequirementsByAltRequisite(org.osid.id.Id requisiteId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCredentialRequirementsByAltRequisite(requisiteId));
    }


    /**
     *  Gets a {@code RequisiteList} immediately containing the given
     *  credential requirement.
     *  
     *  In plenary mode, the returned list contains all known
     *  requisites or an error results. Otherwise, the returned list
     *  may contain only those requisites that are accessible through
     *  this session.
     *  
     *  In active mode, credential requirements are processed and
     *  requisites are returned that are currently active. In any
     *  status mode, active and inactive requisites are returned.
     *
     *  @param  credentialRequirementId a credential requirement {@code Id} 
     *  @return the returned {@code RequisiteList} 
     *  @throws org.osid.NullArgumentException {@code
     *         credentialRequirementId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getRequisitesForCredentialRequirement(org.osid.id.Id credentialRequirementId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRequisitesForCredentialRequirement(credentialRequirementId));
    }    


    /**
     *  Gets all {@code CredentialRequirements.} 
     *  
     *  In plenary mode, the returned list contains all known credential 
     *  requirements or an error results. Otherwise, the returned list may 
     *  contain only those credential requirements that are accessible through this 
     *  session. 
     *  
     *  In active mode, credential requirements are returned that are currently 
     *  active. In any status mode, active and inactive credential requirements are 
     *  returned. 
     *
     *  @return a list of {@code CredentialRequirements} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.CredentialRequirementList getCredentialRequirements()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCredentialRequirements());
    }


    /**
     *  Gets the {@code LearningObjectiveRequirement} specified by its
     *  {@code Id}.
     *  
     *  {@code} In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned {@code
     *  LearningObjectiveRequirement} may have a different {@code Id}
     *  than requested, such as the case where a duplicate {@code Id}
     *  was assigned to a {@code LearningObjectiveRequirement} and
     *  retained for compatibility.
     *  
     *  In active mode, learning objective requirements are returned
     *  that are currently active. In any status mode, active and
     *  inactive learning objective requirements are returned.
     *
     *  @param  learningObjectiveRequirementId the {@code Id} of the {@code 
     *          LearningObjectiveRequirement} to retrieve 
     *  @return the returned {@code LearningObjectiveRequirement} 
     *  @throws org.osid.NotFoundException no {@code LearningObjectiveRequirement} 
     *          found with the given {@code Id} 
     *  @throws org.osid.NullArgumentException {@code
     *         learningObjectiveRequirementId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.LearningObjectiveRequirement getLearningObjectiveRequirement(org.osid.id.Id learningObjectiveRequirementId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLearningObjectiveRequirement(learningObjectiveRequirementId));
    }


    /**
     *  Gets a {@code LearningObjectiveRequirementList} corresponding
     *  to the given {@code IdList.}
     *  
     *  {@code} In plenary mode, the returned list contains all of the
     *  learning objective requirements specified in the {@code Id}
     *  list, in the order of the list, including duplicates, or an
     *  error results if an {@code Id} in the supplied list is not
     *  found or inaccessible.  Otherwise, inaccessible {@code
     *  LearningObjectiveRequirements} may be omitted from the list
     *  and may present the elements in any order including returning
     *  a unique set.
     *  
     *  In active mode, learning objective requirements are returned
     *  that are currently active. In any status mode, active and
     *  inactive learning objective requirements are returned.
     *
     *  @param  learningObjectiveRequirementIds the list of {@code Ids} to 
     *          retrieve 
     *  @return the returned {@code LearningObjectiveRequirementList} list 
     *  @throws org.osid.NotFoundException an {@code Id was} not found 
     *  @throws org.osid.NullArgumentException {@code
     *         learningObjectiveRequirementIds} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.LearningObjectiveRequirementList getLearningObjectiveRequirementsByIds(org.osid.id.IdList learningObjectiveRequirementIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLearningObjectiveRequirementsByIds(learningObjectiveRequirementIds));
    }


    /**
     *  Gets a {@code LearningObjectiveRequirementList} corresponding
     *  to the given learning objective requirement genus {@code Type}
     *  which does not include learning objective requirements of
     *  types derived from the specified {@code Type.}
     *  
     *  In plenary mode, the returned list contains all known learning
     *  objective requirements or an error results. Otherwise, the
     *  returned list may contain only those learning objective
     *  requirements that are accessible through this session.
     *  
     *  In active mode, learning objective requirements are returned
     *  that are currently active. In any status mode, active and
     *  inactive learning objective requirements are returned.
     *
     *  @param  learningObjectiveRequirementGenusType a learning objective requirement genus type 
     *  @return the returned {@code LearningObjectiveRequirementList} 
     *  @throws org.osid.NullArgumentException {@code 
     *          learningObjectiveRequirementGenusType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.LearningObjectiveRequirementList getLearningObjectiveRequirementsByGenusType(org.osid.type.Type learningObjectiveRequirementGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.getLearningObjectiveRequirementsByGenusType(learningObjectiveRequirementGenusType));
    }

    
    /**
     *  Gets a {@code LearningObjectiveRequirementList} corresponding
     *  to the given learning objective requirement genus {@code Type}
     *  and include any additional learning objective requirements
     *  with genus types derived from the specified {@code Type}.
     *  
     *  In plenary mode, the returned list contains all known learning
     *  objective requirements or an error results. Otherwise, the
     *  returned list may contain only those learning objective
     *  requirements that are accessible through this session.
     *  
     *  In active mode, learning objective requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  learning objective requirements are returned.
     *
     *  @param  learningObjectiveRequirementGenusType a learning objective requirements genus type 
     *  @return the returned {@code LearningObjectiveRequirementList} 
     *  @throws org.osid.NullArgumentException {@code 
     *          learningObjectiveRequirementGenusType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.LearningObjectiveRequirementList getLearningObjectiveRequirementsByParentGenusType(org.osid.type.Type learningObjectiveRequirementGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.getLearningObjectiveRequirementsByParentGenusType(learningObjectiveRequirementGenusType));
    }


    /**
     *  Gets a {@code LearningObjectiveRequirementList} containing the given
     *  learning objective requirement record {@code Type.}
     *  
     *  In plenary mode, the returned list contains all known learning objective
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those learning objective requirements that are accessible
     *  through this session.
     *  
     *  In active mode, learning objective requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  learning objective requirements are returned.
     *
     *  @param  learningObjectiveRequirementRecordType a learning objective requirement record type 
     *  @return the returned {@code LearningObjectiveRequirementList} 
     *  @throws org.osid.NullArgumentException {@code 
     *          learningObjectiveRequirementRecordType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.LearningObjectiveRequirementList getLearningObjectiveRequirementsByRecordType(org.osid.type.Type learningObjectiveRequirementRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.getLearningObjectiveRequirementsByRecordType(learningObjectiveRequirementRecordType));
    }

    
    /**
     *  Gets a {@code LearningObjectiveRequirementList} containing the
     *  given learning objective.
     *  
     *  In plenary mode, the returned list contains all known learning
     *  objective requirements or an error results. Otherwise, the
     *  returned list may contain only those learning objective
     *  requirements that are accessible through this session.
     *  
     *  In active mode, learning objective requirements are returned
     *  that are currently active. In any status mode, active and
     *  inactive learning objective requirements are returned.
     *
     *  @param  learning objectiveId a learning objective {@code Id} 
     *  @return the returned {@code LearningObjectiveRequirementList} 
     *  @throws org.osid.NullArgumentException {@code learning objectiveId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.LearningObjectiveRequirementList getLearningObjectiveRequirementsByObjective(org.osid.id.Id learningObjectiveId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLearningObjectiveRequirementsByObjective(learningObjectiveId));
    }

    
    /**
     *  Gets a {@code LearningObjectiveRequirementList} with the given
     *  {@code Requisite}.
     *  
     *  In plenary mode, the returned list contains all known learning
     *  objective requirements or an error results. Otherwise, the
     *  returned list may contain only those learning objective
     *  requirements that are accessible through this session.
     *  
     *  In active mode, learning objective requirements are returned
     *  that are currently active. In any status mode, active and
     *  inactive learning objective requirements are returned.
     *
     *  @param  requisiteId a requisite {@code Id} 
     *  @return the returned {@code LearningObjectiveRequirementList} 
     *  @throws org.osid.NullArgumentException {@code requisiteId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.LearningObjectiveRequirementList getLearningObjectiveRequirementsByAltRequisite(org.osid.id.Id requisiteId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLearningObjectiveRequirementsByAltRequisite(requisiteId));
    }


    /**
     *  Gets a {@code RequisiteList} immediately containing the given
     *  learning objective requirement.
     *  
     *  In plenary mode, the returned list contains all known
     *  requisites or an error results. Otherwise, the returned list
     *  may contain only those requisites that are accessible through
     *  this session.
     *  
     *  In active mode, learning objective requirements are processed and
     *  requisites are returned that are currently active. In any
     *  status mode, active and inactive requisites are returned.
     *
     *  @param  learningObjectiveRequirementId a learning objective requirement {@code Id} 
     *  @return the returned {@code RequisiteList} 
     *  @throws org.osid.NullArgumentException {@code
     *         learningObjectiveRequirementId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getRequisitesForLearningObjectiveRequirement(org.osid.id.Id learningObjectiveRequirementId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRequisitesForLearningObjectiveRequirement(learningObjectiveRequirementId));
    }    


    /**
     *  Gets all {@code LearningObjectiveRequirements.} 
     *  
     *  In plenary mode, the returned list contains all known learning
     *  objective requirements or an error results. Otherwise, the
     *  returned list may contain only those learning objective
     *  requirements that are accessible through this session.
     *  
     *  In active mode, learning objective requirements are returned
     *  that are currently active. In any status mode, active and
     *  inactive learning objective requirements are returned.
     *
     *  @return a list of {@code LearningObjectiveRequirements} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.LearningObjectiveRequirementList getLearningObjectiveRequirements()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLearningObjectiveRequirements());
    }


    /**
     *  Gets the {@code AssessmentRequirement} specified by its {@code
     *  Id}.
     *  
     *  {@code} In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned {@code
     *  AssessmentRequirement} may have a different {@code Id} than
     *  requested, such as the case where a duplicate {@code Id} was
     *  assigned to a {@code AssessmentRequirement} and retained for
     *  compatibility.
     *  
     *  In active mode, assessment requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  assessment requirements are returned.
     *
     *  @param  assessmentRequirementId the {@code Id} of the {@code 
     *          AssessmentRequirement} to retrieve 
     *  @return the returned {@code AssessmentRequirement} 
     *  @throws org.osid.NotFoundException no {@code AssessmentRequirement} 
     *          found with the given {@code Id} 
     *  @throws org.osid.NullArgumentException {@code
     *         assessmentRequirementId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.AssessmentRequirement getAssessmentRequirement(org.osid.id.Id assessmentRequirementId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentRequirement(assessmentRequirementId));
    }


    /**
     *  Gets a {@code AssessmentRequirementList} corresponding to the given 
     *  {@code IdList.} 
     *  
     *  {@code} In plenary mode, the returned list contains all of the 
     *  assessment requirements specified in the {@code Id} list, in the 
     *  order of the list, including duplicates, or an error results if an 
     *  {@code Id} in the supplied list is not found or inaccessible. 
     *  Otherwise, inaccessible {@code AssessmentRequirements} may be 
     *  omitted from the list and may present the elements in any order 
     *  including returning a unique set. 
     *  
     *  In active mode, assessment requirements are returned that are currently 
     *  active. In any status mode, active and inactive assessment requirements 
     *  are returned. 
     *
     *  @param  assessmentRequirementIds the list of {@code Ids} to 
     *          retrieve 
     *  @return the returned {@code AssessmentRequirementList} list 
     *  @throws org.osid.NotFoundException an {@code Id was} not found 
     *  @throws org.osid.NullArgumentException {@code
     *         assessmentRequirementIds} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.AssessmentRequirementList getAssessmentRequirementsByIds(org.osid.id.IdList assessmentRequirementIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentRequirementsByIds(assessmentRequirementIds));
    }


    /**
     *  Gets a {@code AssessmentRequirementList} corresponding to the
     *  given assessment requirement genus {@code Type} which does not
     *  include assessment requirements of types derived from the
     *  specified {@code Type.}
     *  
     *  In plenary mode, the returned list contains all known assessment
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those assessment requirements that are accessible
     *  through this session.
     *  
     *  In active mode, assessment requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  assessment requirements are returned.
     *
     *  @param  assessmentRequirementGenusType an assessment requirement genus type 
     *  @return the returned {@code AssessmentRequirementList} 
     *  @throws org.osid.NullArgumentException {@code 
     *          assessmentRequirementGenusType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.AssessmentRequirementList getAssessmentRequirementsByGenusType(org.osid.type.Type assessmentRequirementGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.getAssessmentRequirementsByGenusType(assessmentRequirementGenusType));
    }

    
    /**
     *  Gets a {@code AssessmentRequirementList} corresponding to the
     *  given assessment requirement genus {@code Type} and include any
     *  additional assessment requirements with genus types derived from
     *  the specified {@code Type}.
     *  
     *  In plenary mode, the returned list contains all known assessment
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those assessment requirements that are accessible
     *  through this session.
     *  
     *  In active mode, assessment requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  assessment requirements are returned.
     *
     *  @param  assessmentRequirementGenusType a nassessment requirements genus type 
     *  @return the returned {@code AssessmentRequirementList} 
     *  @throws org.osid.NullArgumentException {@code 
     *          assessmentRequirementGenusType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.AssessmentRequirementList getAssessmentRequirementsByParentGenusType(org.osid.type.Type assessmentRequirementGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.getAssessmentRequirementsByParentGenusType(assessmentRequirementGenusType));
    }


    /**
     *  Gets a {@code AssessmentRequirementList} containing the given
     *  assessment requirement record {@code Type.}
     *  
     *  In plenary mode, the returned list contains all known assessment
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those assessment requirements that are accessible
     *  through this session.
     *  
     *  In active mode, assessment requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  assessment requirements are returned.
     *
     *  @param  assessmentRequirementRecordType an assessment requirement record type 
     *  @return the returned {@code AssessmentRequirementList} 
     *  @throws org.osid.NullArgumentException {@code 
     *          assessmentRequirementRecordType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.AssessmentRequirementList getAssessmentRequirementsByRecordType(org.osid.type.Type assessmentRequirementRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.getAssessmentRequirementsByRecordType(assessmentRequirementRecordType));
    }

    
    /**
     *  Gets a {@code AssessmentRequirementList} containing the given
     *  assessment.
     *  
     *  In plenary mode, the returned list contains all known assessment
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those assessment requirements that are accessible
     *  through this session.
     *  
     *  In active mode, assessment requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  assessment requirements are returned.
     *
     *  @param  assessmentId an assessment {@code Id} 
     *  @return the returned {@code AssessmentRequirementList} 
     *  @throws org.osid.NullArgumentException {@code assessmentId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.AssessmentRequirementList getAssessmentRequirementsByAssessment(org.osid.id.Id assessmentId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentRequirementsByAssessment(assessmentId));
    }

    
    /**
     *  Gets a {@code AssessmentRequirementList} with the given {@code
     *  Requisite}.
     *  
     *  In plenary mode, the returned list contains all known assessment
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those assessment requirements that are accessible
     *  through this session.
     *  
     *  In active mode, assessment requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  assessment requirements are returned.
     *
     *  @param  requisiteId a requisite {@code Id} 
     *  @return the returned {@code AssessmentRequirementList} 
     *  @throws org.osid.NullArgumentException {@code requisiteId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.AssessmentRequirementList getAssessmentRequirementsByAltRequisite(org.osid.id.Id requisiteId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentRequirementsByAltRequisite(requisiteId));
    }


    /**
     *  Gets a {@code RequisiteList} immediately containing the given
     *  assessment requirement.
     *  
     *  In plenary mode, the returned list contains all known
     *  requisites or an error results. Otherwise, the returned list
     *  may contain only those requisites that are accessible through
     *  this session.
     *  
     *  In active mode, assessment requirements are processed and
     *  requisites are returned that are currently active. In any
     *  status mode, active and inactive requisites are returned.
     *
     *  @param  assessmentRequirementId an assessment requirement {@code Id} 
     *  @return the returned {@code RequisiteList} 
     *  @throws org.osid.NullArgumentException {@code
     *         assessmentRequirementId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getRequisitesForAssessmentRequirement(org.osid.id.Id assessmentRequirementId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRequisitesForAssessmentRequirement(assessmentRequirementId));
    }


    /**
     *  Gets all {@code AssessmentRequirements.} 
     *  
     *  In plenary mode, the returned list contains all known assessment 
     *  requirements or an error results. Otherwise, the returned list may 
     *  contain only those assessment requirements that are accessible through this 
     *  session. 
     *  
     *  In active mode, assessment requirements are returned that are currently 
     *  active. In any status mode, active and inactive assessment requirements are 
     *  returned. 
     *
     *  @return a list of {@code AssessmentRequirements} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.AssessmentRequirementList getAssessmentRequirements()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentRequirements());
    }


    /**
     *  Gets the {@code AwardRequirement} specified by its {@code
     *  Id}.
     *  
     *  {@code} In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned {@code
     *  AwardRequirement} may have a different {@code Id} than
     *  requested, such as the case where a duplicate {@code Id} was
     *  assigned to a {@code AwardRequirement} and retained for
     *  compatibility.
     *  
     *  In active mode, award requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  award requirements are returned.
     *
     *  @param  awardRequirementId the {@code Id} of the {@code 
     *          AwardRequirement} to retrieve 
     *  @return the returned {@code AwardRequirement} 
     *  @throws org.osid.NotFoundException no {@code AwardRequirement} 
     *          found with the given {@code Id} 
     *  @throws org.osid.NullArgumentException {@code
     *         awardRequirementId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.AwardRequirement getAwardRequirement(org.osid.id.Id awardRequirementId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAwardRequirement(awardRequirementId));
    }


    /**
     *  Gets a {@code AwardRequirementList} corresponding to the given 
     *  {@code IdList.} 
     *  
     *  {@code} In plenary mode, the returned list contains all of the 
     *  award requirements specified in the {@code Id} list, in the 
     *  order of the list, including duplicates, or an error results if an 
     *  {@code Id} in the supplied list is not found or inaccessible. 
     *  Otherwise, inaccessible {@code AwardRequirements} may be 
     *  omitted from the list and may present the elements in any order 
     *  including returning a unique set. 
     *  
     *  In active mode, award requirements are returned that are currently 
     *  active. In any status mode, active and inactive award requirements 
     *  are returned. 
     *
     *  @param  awardRequirementIds the list of {@code Ids} to 
     *          retrieve 
     *  @return the returned {@code AwardRequirementList} list 
     *  @throws org.osid.NotFoundException an {@code Id was} not found 
     *  @throws org.osid.NullArgumentException {@code
     *         awardRequirementIds} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.AwardRequirementList getAwardRequirementsByIds(org.osid.id.IdList awardRequirementIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAwardRequirementsByIds(awardRequirementIds));
    }


    /**
     *  Gets a {@code AwardRequirementList} corresponding to the
     *  given award requirement genus {@code Type} which does not
     *  include award requirements of types derived from the
     *  specified {@code Type.}
     *  
     *  In plenary mode, the returned list contains all known award
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those award requirements that are accessible
     *  through this session.
     *  
     *  In active mode, award requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  award requirements are returned.
     *
     *  @param  awardRequirementGenusType an award requirement genus type 
     *  @return the returned {@code AwardRequirementList} 
     *  @throws org.osid.NullArgumentException {@code 
     *          awardRequirementGenusType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.AwardRequirementList getAwardRequirementsByGenusType(org.osid.type.Type awardRequirementGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.getAwardRequirementsByGenusType(awardRequirementGenusType));
    }

    
    /**
     *  Gets a {@code AwardRequirementList} corresponding to the
     *  given award requirement genus {@code Type} and include any
     *  additional award requirements with genus types derived from
     *  the specified {@code Type}.
     *  
     *  In plenary mode, the returned list contains all known award
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those award requirements that are accessible
     *  through this session.
     *  
     *  In active mode, award requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  award requirements are returned.
     *
     *  @param  awardRequirementGenusType a naward requirements genus type 
     *  @return the returned {@code AwardRequirementList} 
     *  @throws org.osid.NullArgumentException {@code 
     *          awardRequirementGenusType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.AwardRequirementList getAwardRequirementsByParentGenusType(org.osid.type.Type awardRequirementGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.getAwardRequirementsByParentGenusType(awardRequirementGenusType));
    }


    /**
     *  Gets a {@code AwardRequirementList} containing the given
     *  award requirement record {@code Type.}
     *  
     *  In plenary mode, the returned list contains all known award
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those award requirements that are accessible
     *  through this session.
     *  
     *  In active mode, award requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  award requirements are returned.
     *
     *  @param  awardRequirementRecordType an award requirement record type 
     *  @return the returned {@code AwardRequirementList} 
     *  @throws org.osid.NullArgumentException {@code 
     *          awardRequirementRecordType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.AwardRequirementList getAwardRequirementsByRecordType(org.osid.type.Type awardRequirementRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.getAwardRequirementsByRecordType(awardRequirementRecordType));
    }

    
    /**
     *  Gets a {@code AwardRequirementList} containing the given
     *  award.
     *  
     *  In plenary mode, the returned list contains all known award
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those award requirements that are accessible
     *  through this session.
     *  
     *  In active mode, award requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  award requirements are returned.
     *
     *  @param  awardId an award {@code Id} 
     *  @return the returned {@code AwardRequirementList} 
     *  @throws org.osid.NullArgumentException {@code awardId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.AwardRequirementList getAwardRequirementsByAward(org.osid.id.Id awardId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAwardRequirementsByAward(awardId));
    }

    
    /**
     *  Gets a {@code AwardRequirementList} with the given {@code
     *  Requisite}.
     *  
     *  In plenary mode, the returned list contains all known award
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those award requirements that are accessible
     *  through this session.
     *  
     *  In active mode, award requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  award requirements are returned.
     *
     *  @param  requisiteId a requisite {@code Id} 
     *  @return the returned {@code AwardRequirementList} 
     *  @throws org.osid.NullArgumentException {@code requisiteId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.AwardRequirementList getAwardRequirementsByAltRequisite(org.osid.id.Id requisiteId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAwardRequirementsByAltRequisite(requisiteId));
    }


    /**
     *  Gets a {@code RequisiteList} immediately containing the given
     *  award requirement.
     *  
     *  In plenary mode, the returned list contains all known
     *  requisites or an error results. Otherwise, the returned list
     *  may contain only those requisites that are accessible through
     *  this session.
     *  
     *  In active mode, award requirements are processed and
     *  requisites are returned that are currently active. In any
     *  status mode, active and inactive requisites are returned.
     *
     *  @param  awardRequirementId an award requirement {@code Id} 
     *  @return the returned {@code RequisiteList} 
     *  @throws org.osid.NullArgumentException {@code
     *         awardRequirementId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getRequisitesForAwardRequirement(org.osid.id.Id awardRequirementId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRequisitesForAwardRequirement(awardRequirementId));
    }
    

    /**
     *  Gets all {@code AwardRequirements.} 
     *  
     *  In plenary mode, the returned list contains all known award 
     *  requirements or an error results. Otherwise, the returned list may 
     *  contain only those award requirements that are accessible through this 
     *  session. 
     *  
     *  In active mode, award requirements are returned that are currently 
     *  active. In any status mode, active and inactive award requirements are 
     *  returned. 
     *
     *  @return a list of {@code AwardRequirements} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.AwardRequirementList getAwardRequirements()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAwardRequirements());
    }
}
