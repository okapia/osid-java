//
// InvariantMapActionGroupLookupSession
//
//    Implements an ActionGroup lookup service backed by a fixed collection of
//    actionGroups.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control;


/**
 *  Implements an ActionGroup lookup service backed by a fixed
 *  collection of action groups. The action groups are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapActionGroupLookupSession
    extends net.okapia.osid.jamocha.core.control.spi.AbstractMapActionGroupLookupSession
    implements org.osid.control.ActionGroupLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapActionGroupLookupSession</code> with no
     *  action groups.
     *  
     *  @param system the system
     *  @throws org.osid.NullArgumnetException {@code system} is
     *          {@code null}
     */

    public InvariantMapActionGroupLookupSession(org.osid.control.System system) {
        setSystem(system);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapActionGroupLookupSession</code> with a single
     *  action group.
     *  
     *  @param system the system
     *  @param actionGroup an single action group
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code actionGroup} is <code>null</code>
     */

      public InvariantMapActionGroupLookupSession(org.osid.control.System system,
                                               org.osid.control.ActionGroup actionGroup) {
        this(system);
        putActionGroup(actionGroup);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapActionGroupLookupSession</code> using an array
     *  of action groups.
     *  
     *  @param system the system
     *  @param actionGroups an array of action groups
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code actionGroups} is <code>null</code>
     */

      public InvariantMapActionGroupLookupSession(org.osid.control.System system,
                                               org.osid.control.ActionGroup[] actionGroups) {
        this(system);
        putActionGroups(actionGroups);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapActionGroupLookupSession</code> using a
     *  collection of action groups.
     *
     *  @param system the system
     *  @param actionGroups a collection of action groups
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code actionGroups} is <code>null</code>
     */

      public InvariantMapActionGroupLookupSession(org.osid.control.System system,
                                               java.util.Collection<? extends org.osid.control.ActionGroup> actionGroups) {
        this(system);
        putActionGroups(actionGroups);
        return;
    }
}
