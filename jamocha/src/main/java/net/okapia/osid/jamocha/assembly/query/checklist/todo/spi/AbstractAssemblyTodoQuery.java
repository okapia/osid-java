//
// AbstractAssemblyTodoQuery.java
//
//     A TodoQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.checklist.todo.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A TodoQuery that stores terms.
 */

public abstract class AbstractAssemblyTodoQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyTemporalOsidObjectQuery
    implements org.osid.checklist.TodoQuery,
               org.osid.checklist.TodoQueryInspector,
               org.osid.checklist.TodoSearchOrder {

    private final java.util.Collection<org.osid.checklist.records.TodoQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.checklist.records.TodoQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.checklist.records.TodoSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();

    private final AssemblyOsidContainableQuery query;


    /** 
     *  Constructs a new <code>AbstractAssemblyTodoQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyTodoQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        this.query = new AssemblyOsidContainableQuery(assembler);
        return;
    }
    

    /**
     *  Match containables that are sequestered. 
     *
     *  @param  match <code> true </code> to match any sequestered 
     *          containables, <code> false </code> to match non-sequestered 
     *          containables 
     */

    @OSID @Override
    public void matchSequestered(boolean match) {
        this.query.matchSequestered(match);
        return;
    }


    /**
     *  Clears the sequestered query terms. 
     */

    @OSID @Override
    public void clearSequesteredTerms() {
        this.query.clearSequesteredTerms();
        return;
    }

    
    /**
     *  Gets the sequestered query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getSequesteredTerms() {
        return (this.query.getSequesteredTerms());
    }

    
    /**
     *  Specifies a preference for ordering the result set by the sequestered 
     *  flag. 
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySequestered(org.osid.SearchOrderStyle style) {
        this.query.orderBySequestered(style);
        return;
    }


    /**
     *  Gets the column name for the sequestered field.
     *
     *  @return the column name
     */

    protected String getSequesteredColumn() {
        return (this.query.getSequesteredColumn());
    }


    /**
     *  Matches completed todos. 
     *
     *  @param  match <code> true </code> for to match completed todos, <code> 
     *          false </code> to match incomplete todos 
     */

    @OSID @Override
    public void matchComplete(boolean match) {
        getAssembler().addBooleanTerm(getCompleteColumn(), match);
        return;
    }


    /**
     *  Clears the complete terms. 
     */

    @OSID @Override
    public void clearCompleteTerms() {
        getAssembler().clearTerms(getCompleteColumn());
        return;
    }


    /**
     *  Gets the complete terms. 
     *
     *  @return the completed query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getCompleteTerms() {
        return (getAssembler().getBooleanTerms(getCompleteColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the completed 
     *  status. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByComplete(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCompleteColumn(), style);
        return;
    }


    /**
     *  Gets the Complete column name.
     *
     * @return the column name
     */

    protected String getCompleteColumn() {
        return ("complete");
    }


    /**
     *  Matches todos of the given priority. 
     *
     *  @param  priorityType a priority type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> priorityType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPriority(org.osid.type.Type priorityType, boolean match) {
        getAssembler().addTypeTerm(getPriorityColumn(), priorityType, match);
        return;
    }


    /**
     *  Matches todos with any priority set. 
     *
     *  @param  match <code> true </code> for to match todos with any 
     *          priority, <code> false </code> to match todos with no priority 
     */

    @OSID @Override
    public void matchAnyPriority(boolean match) {
        getAssembler().addTypeWildcardTerm(getPriorityColumn(), match);
        return;
    }


    /**
     *  Clears the priority terms. 
     */

    @OSID @Override
    public void clearPriorityTerms() {
        getAssembler().clearTerms(getPriorityColumn());
        return;
    }


    /**
     *  Gets the priority terms. 
     *
     *  @return the priority query terms 
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getPriorityTerms() {
        return (getAssembler().getTypeTerms(getPriorityColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the priority. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPriority(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getPriorityColumn(), style);
        return;
    }


    /**
     *  Gets the Priority column name.
     *
     * @return the column name
     */

    protected String getPriorityColumn() {
        return ("priority");
    }


    /**
     *  Matches todos of the given priority or greater. 
     *
     *  @param  priorityType a priority type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> priorityType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchMinimumPriority(org.osid.type.Type priorityType, 
                                     boolean match) {
        getAssembler().addTypeTerm(getMinimumPriorityColumn(), priorityType, match);
        return;
    }


    /**
     *  Clears the minimum priority terms. 
     */

    @OSID @Override
    public void clearMinimumPriorityTerms() {
        getAssembler().clearTerms(getMinimumPriorityColumn());
        return;
    }


    /**
     *  Gets the minimum priority terms. 
     *
     *  @return the priority query terms 
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getMinimumPriorityTerms() {
        return (getAssembler().getTypeTerms(getMinimumPriorityColumn()));
    }


    /**
     *  Gets the MinimumPriority column name.
     *
     * @return the column name
     */

    protected String getMinimumPriorityColumn() {
        return ("minimum_priority");
    }


    /**
     *  Matches todos with dues dates within the given date range inclusive. 
     *
     *  @param  from starting date 
     *  @param  to ending date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDueDate(org.osid.calendaring.DateTime from, 
                             org.osid.calendaring.DateTime to, boolean match) {
        getAssembler().addDateTimeRangeTerm(getDueDateColumn(), from, to, match);
        return;
    }


    /**
     *  Matches todos with any due date. 
     *
     *  @param  match <code> true </code> for to match todos with any due 
     *          date, <code> false </code> to match todos with no due date 
     */

    @OSID @Override
    public void matchAnyDueDate(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getDueDateColumn(), match);
        return;
    }


    /**
     *  Clears the due date terms. 
     */

    @OSID @Override
    public void clearDueDateTerms() {
        getAssembler().clearTerms(getDueDateColumn());
        return;
    }


    /**
     *  Gets the due date terms. 
     *
     *  @return the date query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getDueDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getDueDateColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the due date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDueDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDueDateColumn(), style);
        return;
    }


    /**
     *  Gets the DueDate column name.
     *
     * @return the column name
     */

    protected String getDueDateColumn() {
        return ("due_date");
    }


    /**
     *  Matches a depednency todo. 
     *
     *  @param  todoId a todo <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> todoId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchDependencyId(org.osid.id.Id todoId, boolean match) {
        getAssembler().addIdTerm(getDependencyIdColumn(), todoId, match);
        return;
    }


    /**
     *  Clears the todo <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDependencyIdTerms() {
        getAssembler().clearTerms(getDependencyIdColumn());
        return;
    }


    /**
     *  Gets the dependency <code> Id </code> terms. 
     *
     *  @return the todo query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDependencyIdTerms() {
        return (getAssembler().getIdTerms(getDependencyIdColumn()));
    }


    /**
     *  Gets the DependencyId column name.
     *
     * @return the column name
     */

    protected String getDependencyIdColumn() {
        return ("dependency_id");
    }


    /**
     *  Tests if a <code> TodoQuery </code> is available. 
     *
     *  @return <code> true </code> if a todo query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDependencyQuery() {
        return (false);
    }


    /**
     *  Gets the query for a todo query. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the checklist query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDependencyQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoQuery getDependencyQuery() {
        throw new org.osid.UnimplementedException("supportsDependencyQuery() is false");
    }


    /**
     *  Matches tdos with any dependencies. 
     *
     *  @param  match <code> true </code> for to matchc todos with any 
     *          dependency, <code> false </code> to matchtodos with no 
     *          dependencies 
     */

    @OSID @Override
    public void matchAnyDependency(boolean match) {
        getAssembler().addIdWildcardTerm(getDependencyColumn(), match);
        return;
    }


    /**
     *  Clears the dependency terms. 
     */

    @OSID @Override
    public void clearDependencyTerms() {
        getAssembler().clearTerms(getDependencyColumn());
        return;
    }


    /**
     *  Gets the dependency terms. 
     *
     *  @return the todo query terms 
     */

    @OSID @Override
    public org.osid.checklist.TodoQueryInspector[] getDependencyTerms() {
        return (new org.osid.checklist.TodoQueryInspector[0]);
    }


    /**
     *  Gets the Dependency column name.
     *
     * @return the column name
     */

    protected String getDependencyColumn() {
        return ("dependency");
    }


    /**
     *  Sets the todo <code> Id </code> for this query to match todos that 
     *  have the specified todo as an ancestor. 
     *
     *  @param  todoId a todo <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> todoId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchAncestorTodoId(org.osid.id.Id todoId, boolean match) {
        getAssembler().addIdTerm(getAncestorTodoIdColumn(), todoId, match);
        return;
    }


    /**
     *  Clears the ancestor todo <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorTodoIdTerms() {
        getAssembler().clearTerms(getAncestorTodoIdColumn());
        return;
    }


    /**
     *  Gets the ancestor todo <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorTodoIdTerms() {
        return (getAssembler().getIdTerms(getAncestorTodoIdColumn()));
    }


    /**
     *  Gets the AncestorTodoId column name.
     *
     * @return the column name
     */

    protected String getAncestorTodoIdColumn() {
        return ("ancestor_todo_id");
    }


    /**
     *  Tests if a <code> TodoQuery </code> is available. 
     *
     *  @return <code> true </code> if a todo query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorTodoQuery() {
        return (false);
    }


    /**
     *  Gets the query for a todo. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the todo query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorTodoQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoQuery getAncestorTodoQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorTodoQuery() is false");
    }


    /**
     *  Matches todos that have any ancestor. 
     *
     *  @param  match <code> true </code> to match todos with any ancestor, 
     *          <code> false </code> to match root todos 
     */

    @OSID @Override
    public void matchAnyAncestorTodo(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorTodoColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor todo query terms. 
     */

    @OSID @Override
    public void clearAncestorTodoTerms() {
        getAssembler().clearTerms(getAncestorTodoColumn());
        return;
    }


    /**
     *  Gets the ancestor todo query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.checklist.TodoQueryInspector[] getAncestorTodoTerms() {
        return (new org.osid.checklist.TodoQueryInspector[0]);
    }


    /**
     *  Gets the AncestorTodo column name.
     *
     * @return the column name
     */

    protected String getAncestorTodoColumn() {
        return ("ancestor_todo");
    }


    /**
     *  Sets the todo <code> Id </code> for this query to match todos that 
     *  have the specified todo as a descendant. 
     *
     *  @param  todoId a todo <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> todoId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchDescendantTodoId(org.osid.id.Id todoId, boolean match) {
        getAssembler().addIdTerm(getDescendantTodoIdColumn(), todoId, match);
        return;
    }


    /**
     *  Clears the descendant todo <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantTodoIdTerms() {
        getAssembler().clearTerms(getDescendantTodoIdColumn());
        return;
    }


    /**
     *  Gets the descendant todo <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantTodoIdTerms() {
        return (getAssembler().getIdTerms(getDescendantTodoIdColumn()));
    }


    /**
     *  Gets the DescendantTodoId column name.
     *
     * @return the column name
     */

    protected String getDescendantTodoIdColumn() {
        return ("descendant_todo_id");
    }


    /**
     *  Tests if a <code> TodoQuery </code> is available. 
     *
     *  @return <code> true </code> if a todo query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantTodoQuery() {
        return (false);
    }


    /**
     *  Gets the query for a todo. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the todo query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantTodoQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoQuery getDescendantTodoQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantTodoQuery() is false");
    }


    /**
     *  Matches todos that have any descendant. 
     *
     *  @param  match <code> true </code> to match todos with any descendant, 
     *          <code> false </code> to match leaf todos 
     */

    @OSID @Override
    public void matchAnyDescendantTodo(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantTodoColumn(), match);
        return;
    }


    /**
     *  Clears the descendant todo query terms. 
     */

    @OSID @Override
    public void clearDescendantTodoTerms() {
        getAssembler().clearTerms(getDescendantTodoColumn());
        return;
    }


    /**
     *  Gets the descendant todo query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.checklist.TodoQueryInspector[] getDescendantTodoTerms() {
        return (new org.osid.checklist.TodoQueryInspector[0]);
    }


    /**
     *  Gets the DescendantTodo column name.
     *
     * @return the column name
     */

    protected String getDescendantTodoColumn() {
        return ("descendant_todo");
    }


    /**
     *  Sets the checklist <code> Id </code> for this query to match todos 
     *  assigned to checklists. 
     *
     *  @param  checklistId a checklist <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> checklistId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchChecklistId(org.osid.id.Id checklistId, boolean match) {
        getAssembler().addIdTerm(getChecklistIdColumn(), checklistId, match);
        return;
    }


    /**
     *  Clears the checklist <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearChecklistIdTerms() {
        getAssembler().clearTerms(getChecklistIdColumn());
        return;
    }


    /**
     *  Gets the checklist <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getChecklistIdTerms() {
        return (getAssembler().getIdTerms(getChecklistIdColumn()));
    }


    /**
     *  Gets the ChecklistId column name.
     *
     * @return the column name
     */

    protected String getChecklistIdColumn() {
        return ("checklist_id");
    }


    /**
     *  Tests if a <code> ChecklistQuery </code> is available. 
     *
     *  @return <code> true </code> if a checklist query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChecklistQuery() {
        return (false);
    }


    /**
     *  Gets the query for a checklist query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the checklist query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChecklistQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistQuery getChecklistQuery() {
        throw new org.osid.UnimplementedException("supportsChecklistQuery() is false");
    }


    /**
     *  Clears the checklist terms. 
     */

    @OSID @Override
    public void clearChecklistTerms() {
        getAssembler().clearTerms(getChecklistColumn());
        return;
    }


    /**
     *  Gets the checklist query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistQueryInspector[] getChecklistTerms() {
        return (new org.osid.checklist.ChecklistQueryInspector[0]);
    }


    /**
     *  Gets the Checklist column name.
     *
     * @return the column name
     */

    protected String getChecklistColumn() {
        return ("checklist");
    }


    /**
     *  Tests if this todo supports the given record
     *  <code>Type</code>.
     *
     *  @param  todoRecordType a todo record type 
     *  @return <code>true</code> if the todoRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>todoRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type todoRecordType) {
        for (org.osid.checklist.records.TodoQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(todoRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  todoRecordType the todo record type 
     *  @return the todo query record 
     *  @throws org.osid.NullArgumentException
     *          <code>todoRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(todoRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.checklist.records.TodoQueryRecord getTodoQueryRecord(org.osid.type.Type todoRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.checklist.records.TodoQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(todoRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(todoRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  todoRecordType the todo record type 
     *  @return the todo query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>todoRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(todoRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.checklist.records.TodoQueryInspectorRecord getTodoQueryInspectorRecord(org.osid.type.Type todoRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.checklist.records.TodoQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(todoRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(todoRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param todoRecordType the todo record type
     *  @return the todo search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>todoRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(todoRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.checklist.records.TodoSearchOrderRecord getTodoSearchOrderRecord(org.osid.type.Type todoRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.checklist.records.TodoSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(todoRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(todoRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this todo. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param todoQueryRecord the todo query record
     *  @param todoQueryInspectorRecord the todo query inspector
     *         record
     *  @param todoSearchOrderRecord the todo search order record
     *  @param todoRecordType todo record type
     *  @throws org.osid.NullArgumentException
     *          <code>todoQueryRecord</code>,
     *          <code>todoQueryInspectorRecord</code>,
     *          <code>todoSearchOrderRecord</code> or
     *          <code>todoRecordTypetodo</code> is
     *          <code>null</code>
     */
            
    protected void addTodoRecords(org.osid.checklist.records.TodoQueryRecord todoQueryRecord, 
                                      org.osid.checklist.records.TodoQueryInspectorRecord todoQueryInspectorRecord, 
                                      org.osid.checklist.records.TodoSearchOrderRecord todoSearchOrderRecord, 
                                      org.osid.type.Type todoRecordType) {

        addRecordType(todoRecordType);

        nullarg(todoQueryRecord, "todo query record");
        nullarg(todoQueryInspectorRecord, "todo query inspector record");
        nullarg(todoSearchOrderRecord, "todo search odrer record");

        this.queryRecords.add(todoQueryRecord);
        this.queryInspectorRecords.add(todoQueryInspectorRecord);
        this.searchOrderRecords.add(todoSearchOrderRecord);
        
        return;
    }


    protected class AssemblyOsidContainableQuery
        extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidContainableQuery
        implements org.osid.OsidContainableQuery,
                   org.osid.OsidContainableQueryInspector,
                   org.osid.OsidContainableSearchOrder {
        
        
        /** 
         *  Constructs a new <code>AssemblyOsidContainableQuery</code>.
         *
         *  @param assembler the query assembler
         *  @throws org.osid.NullArgumentException <code>assembler</code>
         *          is <code>null</code>
         */
        
        protected AssemblyOsidContainableQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
            super(assembler);
            return;
        }


        /**
         *  Gets the column name for the sequestered field.
         *
         *  @return the column name
         */
        
        protected String getSequesteredColumn() {
            return (super.getSequesteredColumn());
        }
    }    
}
