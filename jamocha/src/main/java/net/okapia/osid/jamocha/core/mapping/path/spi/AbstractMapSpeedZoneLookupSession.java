//
// AbstractMapSpeedZoneLookupSession
//
//    A simple framework for providing a SpeedZone lookup service
//    backed by a fixed collection of speed zones.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.path.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a SpeedZone lookup service backed by a
 *  fixed collection of speed zones. The speed zones are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>SpeedZones</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapSpeedZoneLookupSession
    extends net.okapia.osid.jamocha.mapping.path.spi.AbstractSpeedZoneLookupSession
    implements org.osid.mapping.path.SpeedZoneLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.mapping.path.SpeedZone> speedZones = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.mapping.path.SpeedZone>());


    /**
     *  Makes a <code>SpeedZone</code> available in this session.
     *
     *  @param  speedZone a speed zone
     *  @throws org.osid.NullArgumentException <code>speedZone<code>
     *          is <code>null</code>
     */

    protected void putSpeedZone(org.osid.mapping.path.SpeedZone speedZone) {
        this.speedZones.put(speedZone.getId(), speedZone);
        return;
    }


    /**
     *  Makes an array of speed zones available in this session.
     *
     *  @param  speedZones an array of speed zones
     *  @throws org.osid.NullArgumentException <code>speedZones<code>
     *          is <code>null</code>
     */

    protected void putSpeedZones(org.osid.mapping.path.SpeedZone[] speedZones) {
        putSpeedZones(java.util.Arrays.asList(speedZones));
        return;
    }


    /**
     *  Makes a collection of speed zones available in this session.
     *
     *  @param  speedZones a collection of speed zones
     *  @throws org.osid.NullArgumentException <code>speedZones<code>
     *          is <code>null</code>
     */

    protected void putSpeedZones(java.util.Collection<? extends org.osid.mapping.path.SpeedZone> speedZones) {
        for (org.osid.mapping.path.SpeedZone speedZone : speedZones) {
            this.speedZones.put(speedZone.getId(), speedZone);
        }

        return;
    }


    /**
     *  Removes a SpeedZone from this session.
     *
     *  @param  speedZoneId the <code>Id</code> of the speed zone
     *  @throws org.osid.NullArgumentException <code>speedZoneId<code> is
     *          <code>null</code>
     */

    protected void removeSpeedZone(org.osid.id.Id speedZoneId) {
        this.speedZones.remove(speedZoneId);
        return;
    }


    /**
     *  Gets the <code>SpeedZone</code> specified by its <code>Id</code>.
     *
     *  @param  speedZoneId <code>Id</code> of the <code>SpeedZone</code>
     *  @return the speedZone
     *  @throws org.osid.NotFoundException <code>speedZoneId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>speedZoneId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZone getSpeedZone(org.osid.id.Id speedZoneId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.mapping.path.SpeedZone speedZone = this.speedZones.get(speedZoneId);
        if (speedZone == null) {
            throw new org.osid.NotFoundException("speedZone not found: " + speedZoneId);
        }

        return (speedZone);
    }


    /**
     *  Gets all <code>SpeedZones</code>. In plenary mode, the returned
     *  list contains all known speedZones or an error
     *  results. Otherwise, the returned list may contain only those
     *  speedZones that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>SpeedZones</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneList getSpeedZones()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.mapping.path.speedzone.ArraySpeedZoneList(this.speedZones.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.speedZones.clear();
        super.close();
        return;
    }
}
