//
// AbstractQueryCommentLookupSession.java
//
//    A CommentQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.commenting.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A CommentQuerySession adapter.
 */

public abstract class AbstractAdapterCommentQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.commenting.CommentQuerySession {

    private final org.osid.commenting.CommentQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterCommentQuerySession.
     *
     *  @param session the underlying comment query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterCommentQuerySession(org.osid.commenting.CommentQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeBook</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeBook Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBookId() {
        return (this.session.getBookId());
    }


    /**
     *  Gets the {@codeBook</code> associated with this 
     *  session.
     *
     *  @return the {@codeBook</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.Book getBook()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getBook());
    }


    /**
     *  Tests if this user can perform {@codeComment</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchComments() {
        return (this.session.canSearchComments());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include comments in books which are children
     *  of this book in the book hierarchy.
     */

    @OSID @Override
    public void useFederatedBookView() {
        this.session.useFederatedBookView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this book only.
     */
    
    @OSID @Override
    public void useIsolatedBookView() {
        this.session.useIsolatedBookView();
        return;
    }
    
      
    /**
     *  Gets a comment query. The returned query will not have an
     *  extension query.
     *
     *  @return the comment query 
     */
      
    @OSID @Override
    public org.osid.commenting.CommentQuery getCommentQuery() {
        return (this.session.getCommentQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  commentQuery the comment query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code commentQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code commentQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsByQuery(org.osid.commenting.CommentQuery commentQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getCommentsByQuery(commentQuery));
    }
}
