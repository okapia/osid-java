//
// AbstractQueryOfferingConstrainerEnablerLookupSession.java
//
//    An inline adapter that maps an OfferingConstrainerEnablerLookupSession to
//    an OfferingConstrainerEnablerQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.offering.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an OfferingConstrainerEnablerLookupSession to
 *  an OfferingConstrainerEnablerQuerySession.
 */

public abstract class AbstractQueryOfferingConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.offering.rules.spi.AbstractOfferingConstrainerEnablerLookupSession
    implements org.osid.offering.rules.OfferingConstrainerEnablerLookupSession {

      private boolean activeonly    = false;
      private boolean effectiveonly = false;

    private final org.osid.offering.rules.OfferingConstrainerEnablerQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryOfferingConstrainerEnablerLookupSession.
     *
     *  @param querySession the underlying offering constrainer enabler query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryOfferingConstrainerEnablerLookupSession(org.osid.offering.rules.OfferingConstrainerEnablerQuerySession querySession) {
        nullarg(querySession, "offering constrainer enabler query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Catalogue</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Catalogue Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCatalogueId() {
        return (this.session.getCatalogueId());
    }


    /**
     *  Gets the <code>Catalogue</code> associated with this 
     *  session.
     *
     *  @return the <code>Catalogue</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Catalogue getCatalogue()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCatalogue());
    }


    /**
     *  Tests if this user can perform <code>OfferingConstrainerEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupOfferingConstrainerEnablers() {
        return (this.session.canSearchOfferingConstrainerEnablers());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include offering constrainer enablers in catalogues which are children
     *  of this catalogue in the catalogue hierarchy.
     */

    @OSID @Override
    public void useFederatedCatalogueView() {
        this.session.useFederatedCatalogueView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this catalogue only.
     */

    @OSID @Override
    public void useIsolatedCatalogueView() {
        this.session.useIsolatedCatalogueView();
        return;
    }
    

    /**
     *  Only active offering constrainer enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveOfferingConstrainerEnablerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive offering constrainer enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusOfferingConstrainerEnablerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>OfferingConstrainerEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>OfferingConstrainerEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>OfferingConstrainerEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, offering constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive offering constrainer enablers
     *  are returned.
     *
     *  @param  offeringConstrainerEnablerId <code>Id</code> of the
     *          <code>OfferingConstrainerEnabler</code>
     *  @return the offering constrainer enabler
     *  @throws org.osid.NotFoundException <code>offeringConstrainerEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>offeringConstrainerEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnabler getOfferingConstrainerEnabler(org.osid.id.Id offeringConstrainerEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.rules.OfferingConstrainerEnablerQuery query = getQuery();
        query.matchId(offeringConstrainerEnablerId, true);
        org.osid.offering.rules.OfferingConstrainerEnablerList offeringConstrainerEnablers = this.session.getOfferingConstrainerEnablersByQuery(query);
        if (offeringConstrainerEnablers.hasNext()) {
            return (offeringConstrainerEnablers.getNextOfferingConstrainerEnabler());
        } 
        
        throw new org.osid.NotFoundException(offeringConstrainerEnablerId + " not found");
    }


    /**
     *  Gets an <code>OfferingConstrainerEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  offeringConstrainerEnablers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>OfferingConstrainerEnablers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, offering constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive offering constrainer enablers
     *  are returned.
     *
     *  @param  offeringConstrainerEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>OfferingConstrainerEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>offeringConstrainerEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerList getOfferingConstrainerEnablersByIds(org.osid.id.IdList offeringConstrainerEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.rules.OfferingConstrainerEnablerQuery query = getQuery();

        try (org.osid.id.IdList ids = offeringConstrainerEnablerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getOfferingConstrainerEnablersByQuery(query));
    }


    /**
     *  Gets an <code>OfferingConstrainerEnablerList</code> corresponding to the given
     *  offering constrainer enabler genus <code>Type</code> which does not include
     *  offering constrainer enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  offering constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those offering constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, offering constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive offering constrainer enablers
     *  are returned.
     *
     *  @param  offeringConstrainerEnablerGenusType an offeringConstrainerEnabler genus type 
     *  @return the returned <code>OfferingConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>offeringConstrainerEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerList getOfferingConstrainerEnablersByGenusType(org.osid.type.Type offeringConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.rules.OfferingConstrainerEnablerQuery query = getQuery();
        query.matchGenusType(offeringConstrainerEnablerGenusType, true);
        return (this.session.getOfferingConstrainerEnablersByQuery(query));
    }


    /**
     *  Gets an <code>OfferingConstrainerEnablerList</code> corresponding to the given
     *  offering constrainer enabler genus <code>Type</code> and include any additional
     *  offering constrainer enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  offering constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those offering constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, offering constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive offering constrainer enablers
     *  are returned.
     *
     *  @param  offeringConstrainerEnablerGenusType an offeringConstrainerEnabler genus type 
     *  @return the returned <code>OfferingConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>offeringConstrainerEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerList getOfferingConstrainerEnablersByParentGenusType(org.osid.type.Type offeringConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.rules.OfferingConstrainerEnablerQuery query = getQuery();
        query.matchParentGenusType(offeringConstrainerEnablerGenusType, true);
        return (this.session.getOfferingConstrainerEnablersByQuery(query));
    }


    /**
     *  Gets an <code>OfferingConstrainerEnablerList</code> containing the given
     *  offering constrainer enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  offering constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those offering constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, offering constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive offering constrainer enablers
     *  are returned.
     *
     *  @param  offeringConstrainerEnablerRecordType an offeringConstrainerEnabler record type 
     *  @return the returned <code>OfferingConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>offeringConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerList getOfferingConstrainerEnablersByRecordType(org.osid.type.Type offeringConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.rules.OfferingConstrainerEnablerQuery query = getQuery();
        query.matchRecordType(offeringConstrainerEnablerRecordType, true);
        return (this.session.getOfferingConstrainerEnablersByQuery(query));
    }


    /**
     *  Gets an <code>OfferingConstrainerEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  offering constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those offering constrainer enablers that are accessible
     *  through this session.
     *  
     *  In active mode, offering constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive offering constrainer enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>OfferingConstrainerEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerList getOfferingConstrainerEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.rules.OfferingConstrainerEnablerQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getOfferingConstrainerEnablersByQuery(query));
    }
        
    
    /**
     *  Gets all <code>OfferingConstrainerEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  offering constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those offering constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, offering constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive offering constrainer enablers
     *  are returned.
     *
     *  @return a list of <code>OfferingConstrainerEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerList getOfferingConstrainerEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {


        org.osid.offering.rules.OfferingConstrainerEnablerQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getOfferingConstrainerEnablersByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.offering.rules.OfferingConstrainerEnablerQuery getQuery() {
        org.osid.offering.rules.OfferingConstrainerEnablerQuery query = this.session.getOfferingConstrainerEnablerQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
