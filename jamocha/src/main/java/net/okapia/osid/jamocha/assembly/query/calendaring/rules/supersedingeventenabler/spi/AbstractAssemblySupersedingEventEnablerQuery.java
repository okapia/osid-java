//
// AbstractAssemblySupersedingEventEnablerQuery.java
//
//     A SupersedingEventEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.calendaring.rules.supersedingeventenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A SupersedingEventEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblySupersedingEventEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.calendaring.rules.SupersedingEventEnablerQuery,
               org.osid.calendaring.rules.SupersedingEventEnablerQueryInspector,
               org.osid.calendaring.rules.SupersedingEventEnablerSearchOrder {

    private final java.util.Collection<org.osid.calendaring.rules.records.SupersedingEventEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.rules.records.SupersedingEventEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.rules.records.SupersedingEventEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblySupersedingEventEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblySupersedingEventEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to the superseding event. 
     *
     *  @param  supersedingEventId the superseding event <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> supersedingEventId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledSupersedingEventId(org.osid.id.Id supersedingEventId, 
                                             boolean match) {
        getAssembler().addIdTerm(getRuledSupersedingEventIdColumn(), supersedingEventId, match);
        return;
    }


    /**
     *  Clears the superseding event <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledSupersedingEventIdTerms() {
        getAssembler().clearTerms(getRuledSupersedingEventIdColumn());
        return;
    }


    /**
     *  Gets the superseding event <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledSupersedingEventIdTerms() {
        return (getAssembler().getIdTerms(getRuledSupersedingEventIdColumn()));
    }


    /**
     *  Gets the RuledSupersedingEventId column name.
     *
     * @return the column name
     */

    protected String getRuledSupersedingEventIdColumn() {
        return ("ruled_superseding_event_id");
    }


    /**
     *  Tests if an <code> SupersedingEventQuery </code> is available. 
     *
     *  @return <code> true </code> if a superseding event query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledSupersedingEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for a supsreding event. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the superseding event query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledSupersedingEventQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventQuery getRuledSupersedingEventQuery() {
        throw new org.osid.UnimplementedException("supportsRuledSupersedingEventQuery() is false");
    }


    /**
     *  Matches enablers mapped to any supsreding event. 
     *
     *  @param  match <code> true </code> for enablers mapped to any 
     *          supsreding event, <code> false </code> to match enablers 
     *          mapped to no supsreding events 
     */

    @OSID @Override
    public void matchAnyRuledSupersedingEvent(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledSupersedingEventColumn(), match);
        return;
    }


    /**
     *  Clears the calendar query terms. 
     */

    @OSID @Override
    public void clearRuledSupersedingEventTerms() {
        getAssembler().clearTerms(getRuledSupersedingEventColumn());
        return;
    }


    /**
     *  Gets the superseding event query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventQueryInspector[] getRuledSupersedingEventTerms() {
        return (new org.osid.calendaring.SupersedingEventQueryInspector[0]);
    }


    /**
     *  Gets the RuledSupersedingEvent column name.
     *
     * @return the column name
     */

    protected String getRuledSupersedingEventColumn() {
        return ("ruled_superseding_event");
    }


    /**
     *  Matches enablers mapped to the calendar. 
     *
     *  @param  calendarId the calendar <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCalendarId(org.osid.id.Id calendarId, boolean match) {
        getAssembler().addIdTerm(getCalendarIdColumn(), calendarId, match);
        return;
    }


    /**
     *  Clears the calendar <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCalendarIdTerms() {
        getAssembler().clearTerms(getCalendarIdColumn());
        return;
    }


    /**
     *  Gets the calendar <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCalendarIdTerms() {
        return (getAssembler().getIdTerms(getCalendarIdColumn()));
    }


    /**
     *  Gets the CalendarId column name.
     *
     * @return the column name
     */

    protected String getCalendarIdColumn() {
        return ("calendar_id");
    }


    /**
     *  Tests if a <code> CalendarQuery </code> is available. 
     *
     *  @return <code> true </code> if a calendar query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarQuery() {
        return (false);
    }


    /**
     *  Gets the query for a calendar. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the calendar query 
     *  @throws org.osid.UnimplementedException <code> supportsCalendarQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQuery getCalendarQuery() {
        throw new org.osid.UnimplementedException("supportsCalendarQuery() is false");
    }


    /**
     *  Clears the calendar query terms. 
     */

    @OSID @Override
    public void clearCalendarTerms() {
        getAssembler().clearTerms(getCalendarColumn());
        return;
    }


    /**
     *  Gets the calendar query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQueryInspector[] getCalendarTerms() {
        return (new org.osid.calendaring.CalendarQueryInspector[0]);
    }


    /**
     *  Gets the Calendar column name.
     *
     * @return the column name
     */

    protected String getCalendarColumn() {
        return ("calendar");
    }


    /**
     *  Tests if this supersedingEventEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  supersedingEventEnablerRecordType a superseding event enabler record type 
     *  @return <code>true</code> if the supersedingEventEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type supersedingEventEnablerRecordType) {
        for (org.osid.calendaring.rules.records.SupersedingEventEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(supersedingEventEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  supersedingEventEnablerRecordType the superseding event enabler record type 
     *  @return the superseding event enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(supersedingEventEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.rules.records.SupersedingEventEnablerQueryRecord getSupersedingEventEnablerQueryRecord(org.osid.type.Type supersedingEventEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.rules.records.SupersedingEventEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(supersedingEventEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(supersedingEventEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  supersedingEventEnablerRecordType the superseding event enabler record type 
     *  @return the superseding event enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(supersedingEventEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.rules.records.SupersedingEventEnablerQueryInspectorRecord getSupersedingEventEnablerQueryInspectorRecord(org.osid.type.Type supersedingEventEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.rules.records.SupersedingEventEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(supersedingEventEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(supersedingEventEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param supersedingEventEnablerRecordType the superseding event enabler record type
     *  @return the superseding event enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(supersedingEventEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.rules.records.SupersedingEventEnablerSearchOrderRecord getSupersedingEventEnablerSearchOrderRecord(org.osid.type.Type supersedingEventEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.rules.records.SupersedingEventEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(supersedingEventEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(supersedingEventEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this superseding event enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param supersedingEventEnablerQueryRecord the superseding event enabler query record
     *  @param supersedingEventEnablerQueryInspectorRecord the superseding event enabler query inspector
     *         record
     *  @param supersedingEventEnablerSearchOrderRecord the superseding event enabler search order record
     *  @param supersedingEventEnablerRecordType superseding event enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventEnablerQueryRecord</code>,
     *          <code>supersedingEventEnablerQueryInspectorRecord</code>,
     *          <code>supersedingEventEnablerSearchOrderRecord</code> or
     *          <code>supersedingEventEnablerRecordTypesupersedingEventEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addSupersedingEventEnablerRecords(org.osid.calendaring.rules.records.SupersedingEventEnablerQueryRecord supersedingEventEnablerQueryRecord, 
                                      org.osid.calendaring.rules.records.SupersedingEventEnablerQueryInspectorRecord supersedingEventEnablerQueryInspectorRecord, 
                                      org.osid.calendaring.rules.records.SupersedingEventEnablerSearchOrderRecord supersedingEventEnablerSearchOrderRecord, 
                                      org.osid.type.Type supersedingEventEnablerRecordType) {

        addRecordType(supersedingEventEnablerRecordType);

        nullarg(supersedingEventEnablerQueryRecord, "superseding event enabler query record");
        nullarg(supersedingEventEnablerQueryInspectorRecord, "superseding event enabler query inspector record");
        nullarg(supersedingEventEnablerSearchOrderRecord, "superseding event enabler search odrer record");

        this.queryRecords.add(supersedingEventEnablerQueryRecord);
        this.queryInspectorRecords.add(supersedingEventEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(supersedingEventEnablerSearchOrderRecord);
        
        return;
    }
}
