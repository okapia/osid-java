//
// AbstractQuerySpeedZoneEnablerLookupSession.java
//
//    An inline adapter that maps a SpeedZoneEnablerLookupSession to
//    a SpeedZoneEnablerQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.mapping.path.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a SpeedZoneEnablerLookupSession to
 *  a SpeedZoneEnablerQuerySession.
 */

public abstract class AbstractQuerySpeedZoneEnablerLookupSession
    extends net.okapia.osid.jamocha.mapping.path.rules.spi.AbstractSpeedZoneEnablerLookupSession
    implements org.osid.mapping.path.rules.SpeedZoneEnablerLookupSession {

      private boolean activeonly    = false;
      private boolean effectiveonly = false;

    private final org.osid.mapping.path.rules.SpeedZoneEnablerQuerySession session;
    

    /**
     *  Constructs a new AbstractQuerySpeedZoneEnablerLookupSession.
     *
     *  @param querySession the underlying speed zone enabler query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQuerySpeedZoneEnablerLookupSession(org.osid.mapping.path.rules.SpeedZoneEnablerQuerySession querySession) {
        nullarg(querySession, "speed zone enabler query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Map</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Map Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getMapId() {
        return (this.session.getMapId());
    }


    /**
     *  Gets the <code>Map</code> associated with this 
     *  session.
     *
     *  @return the <code>Map</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.Map getMap()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getMap());
    }


    /**
     *  Tests if this user can perform <code>SpeedZoneEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupSpeedZoneEnablers() {
        return (this.session.canSearchSpeedZoneEnablers());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include speed zone enablers in maps which are children
     *  of this map in the map hierarchy.
     */

    @OSID @Override
    public void useFederatedMapView() {
        this.session.useFederatedMapView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this map only.
     */

    @OSID @Override
    public void useIsolatedMapView() {
        this.session.useIsolatedMapView();
        return;
    }
    

    /**
     *  Only active speed zone enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveSpeedZoneEnablerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive speed zone enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusSpeedZoneEnablerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>SpeedZoneEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>SpeedZoneEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>SpeedZoneEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, speed zone enablers are returned that are currently
     *  active. In any status mode, active and inactive speed zone enablers
     *  are returned.
     *
     *  @param  speedZoneEnablerId <code>Id</code> of the
     *          <code>SpeedZoneEnabler</code>
     *  @return the speed zone enabler
     *  @throws org.osid.NotFoundException <code>speedZoneEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>speedZoneEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnabler getSpeedZoneEnabler(org.osid.id.Id speedZoneEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.mapping.path.rules.SpeedZoneEnablerQuery query = getQuery();
        query.matchId(speedZoneEnablerId, true);
        org.osid.mapping.path.rules.SpeedZoneEnablerList speedZoneEnablers = this.session.getSpeedZoneEnablersByQuery(query);
        if (speedZoneEnablers.hasNext()) {
            return (speedZoneEnablers.getNextSpeedZoneEnabler());
        } 
        
        throw new org.osid.NotFoundException(speedZoneEnablerId + " not found");
    }


    /**
     *  Gets a <code>SpeedZoneEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  speedZoneEnablers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>SpeedZoneEnablers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, speed zone enablers are returned that are currently
     *  active. In any status mode, active and inactive speed zone enablers
     *  are returned.
     *
     *  @param  speedZoneEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>SpeedZoneEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>speedZoneEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerList getSpeedZoneEnablersByIds(org.osid.id.IdList speedZoneEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.mapping.path.rules.SpeedZoneEnablerQuery query = getQuery();

        try (org.osid.id.IdList ids = speedZoneEnablerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getSpeedZoneEnablersByQuery(query));
    }


    /**
     *  Gets a <code>SpeedZoneEnablerList</code> corresponding to the given
     *  speed zone enabler genus <code>Type</code> which does not include
     *  speed zone enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  speed zone enablers or an error results. Otherwise, the returned list
     *  may contain only those speed zone enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, speed zone enablers are returned that are currently
     *  active. In any status mode, active and inactive speed zone enablers
     *  are returned.
     *
     *  @param  speedZoneEnablerGenusType a speedZoneEnabler genus type 
     *  @return the returned <code>SpeedZoneEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>speedZoneEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerList getSpeedZoneEnablersByGenusType(org.osid.type.Type speedZoneEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.mapping.path.rules.SpeedZoneEnablerQuery query = getQuery();
        query.matchGenusType(speedZoneEnablerGenusType, true);
        return (this.session.getSpeedZoneEnablersByQuery(query));
    }


    /**
     *  Gets a <code>SpeedZoneEnablerList</code> corresponding to the given
     *  speed zone enabler genus <code>Type</code> and include any additional
     *  speed zone enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  speed zone enablers or an error results. Otherwise, the returned list
     *  may contain only those speed zone enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, speed zone enablers are returned that are currently
     *  active. In any status mode, active and inactive speed zone enablers
     *  are returned.
     *
     *  @param  speedZoneEnablerGenusType a speedZoneEnabler genus type 
     *  @return the returned <code>SpeedZoneEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>speedZoneEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerList getSpeedZoneEnablersByParentGenusType(org.osid.type.Type speedZoneEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.mapping.path.rules.SpeedZoneEnablerQuery query = getQuery();
        query.matchParentGenusType(speedZoneEnablerGenusType, true);
        return (this.session.getSpeedZoneEnablersByQuery(query));
    }


    /**
     *  Gets a <code>SpeedZoneEnablerList</code> containing the given
     *  speed zone enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  speed zone enablers or an error results. Otherwise, the returned list
     *  may contain only those speed zone enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, speed zone enablers are returned that are currently
     *  active. In any status mode, active and inactive speed zone enablers
     *  are returned.
     *
     *  @param  speedZoneEnablerRecordType a speedZoneEnabler record type 
     *  @return the returned <code>SpeedZoneEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>speedZoneEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerList getSpeedZoneEnablersByRecordType(org.osid.type.Type speedZoneEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.mapping.path.rules.SpeedZoneEnablerQuery query = getQuery();
        query.matchRecordType(speedZoneEnablerRecordType, true);
        return (this.session.getSpeedZoneEnablersByQuery(query));
    }


    /**
     *  Gets a <code>SpeedZoneEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  speed zone enablers or an error results. Otherwise, the returned list
     *  may contain only those speed zone enablers that are accessible
     *  through this session.
     *  
     *  In active mode, speed zone enablers are returned that are currently
     *  active. In any status mode, active and inactive speed zone enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>SpeedZoneEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerList getSpeedZoneEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.mapping.path.rules.SpeedZoneEnablerQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getSpeedZoneEnablersByQuery(query));
    }
        
    
    /**
     *  Gets all <code>SpeedZoneEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  speed zone enablers or an error results. Otherwise, the returned list
     *  may contain only those speed zone enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, speed zone enablers are returned that are currently
     *  active. In any status mode, active and inactive speed zone enablers
     *  are returned.
     *
     *  @return a list of <code>SpeedZoneEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerList getSpeedZoneEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {


        org.osid.mapping.path.rules.SpeedZoneEnablerQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getSpeedZoneEnablersByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.mapping.path.rules.SpeedZoneEnablerQuery getQuery() {
        org.osid.mapping.path.rules.SpeedZoneEnablerQuery query = this.session.getSpeedZoneEnablerQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
