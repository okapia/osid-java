//
// AbstractAssemblyStepQuery.java
//
//     A StepQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.workflow.step.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A StepQuery that stores terms.
 */

public abstract class AbstractAssemblyStepQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidGovernatorQuery
    implements org.osid.workflow.StepQuery,
               org.osid.workflow.StepQueryInspector,
               org.osid.workflow.StepSearchOrder {

    private final java.util.Collection<org.osid.workflow.records.StepQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.workflow.records.StepQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.workflow.records.StepSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyStepQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyStepQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the process <code> Id </code> for this query. 
     *
     *  @param  processId the process <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> processId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProcessId(org.osid.id.Id processId, boolean match) {
        getAssembler().addIdTerm(getProcessIdColumn(), processId, match);
        return;
    }


    /**
     *  Clears the process <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearProcessIdTerms() {
        getAssembler().clearTerms(getProcessIdColumn());
        return;
    }


    /**
     *  Gets the process <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProcessIdTerms() {
        return (getAssembler().getIdTerms(getProcessIdColumn()));
    }


    /**
     *  Orders the results by process. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByProcess(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getProcessColumn(), style);
        return;
    }


    /**
     *  Gets the ProcessId column name.
     *
     * @return the column name
     */

    protected String getProcessIdColumn() {
        return ("process_id");
    }


    /**
     *  Tests if a <code> ProcessQuery </code> is available. 
     *
     *  @return <code> true </code> if a process query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a process. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the process query 
     *  @throws org.osid.UnimplementedException <code> supportsProcessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessQuery getProcessQuery() {
        throw new org.osid.UnimplementedException("supportsProcessQuery() is false");
    }


    /**
     *  Clears the process query terms. 
     */

    @OSID @Override
    public void clearProcessTerms() {
        getAssembler().clearTerms(getProcessColumn());
        return;
    }


    /**
     *  Gets the process query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.workflow.ProcessQueryInspector[] getProcessTerms() {
        return (new org.osid.workflow.ProcessQueryInspector[0]);
    }


    /**
     *  Tests if a process search order is available. 
     *
     *  @return <code> true </code> if a process search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessSearchOrder() {
        return (false);
    }


    /**
     *  Gets the process search order. 
     *
     *  @return the process search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsProcessSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessSearchOrder getProcessSearchOrder() {
        throw new org.osid.UnimplementedException("supportsProcessSearchOrder() is false");
    }


    /**
     *  Gets the Process column name.
     *
     * @return the column name
     */

    protected String getProcessColumn() {
        return ("process");
    }


    /**
     *  Sets the resource <code> Id </code> for this query. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResourceId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getResourceIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the resource <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearResourceIdTerms() {
        getAssembler().clearTerms(getResourceIdColumn());
        return;
    }


    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResourceIdTerms() {
        return (getAssembler().getIdTerms(getResourceIdColumn()));
    }


    /**
     *  Gets the ResourceId column name.
     *
     * @return the column name
     */

    protected String getResourceIdColumn() {
        return ("resource_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResourceQuery() {
        throw new org.osid.UnimplementedException("supportsResourceQuery() is false");
    }


    /**
     *  Matches steps that have any resources. 
     *
     *  @param  match <code> true </code> to match steps with any resources, 
     *          <code> false </code> to match steps with no resources 
     */

    @OSID @Override
    public void matchAnyResource(boolean match) {
        getAssembler().addIdWildcardTerm(getResourceColumn(), match);
        return;
    }


    /**
     *  Clears the resource query terms. 
     */

    @OSID @Override
    public void clearResourceTerms() {
        getAssembler().clearTerms(getResourceColumn());
        return;
    }


    /**
     *  Gets the resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the Resource column name.
     *
     * @return the column name
     */

    protected String getResourceColumn() {
        return ("resource");
    }


    /**
     *  Matches initial steps. 
     *
     *  @param  match <code> true </code> to match initial steps, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public void matchInitial(boolean match) {
        getAssembler().addBooleanTerm(getInitialColumn(), match);
        return;
    }


    /**
     *  Clears the initial step terms. 
     */

    @OSID @Override
    public void clearInitialTerms() {
        getAssembler().clearTerms(getInitialColumn());
        return;
    }


    /**
     *  Gets the initial step query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getInitialTerms() {
        return (getAssembler().getBooleanTerms(getInitialColumn()));
    }


    /**
     *  Orders the results by initial steps. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByInitial(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getInitialColumn(), style);
        return;
    }


    /**
     *  Gets the Initial column name.
     *
     * @return the column name
     */

    protected String getInitialColumn() {
        return ("initial");
    }


    /**
     *  Matches terminal steps. 
     *
     *  @param  match <code> true </code> to match terminal steps, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public void matchTerminal(boolean match) {
        getAssembler().addBooleanTerm(getTerminalColumn(), match);
        return;
    }


    /**
     *  Clears the terminal step terms. 
     */

    @OSID @Override
    public void clearTerminalTerms() {
        getAssembler().clearTerms(getTerminalColumn());
        return;
    }


    /**
     *  Gets the terminal step query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getTerminalTerms() {
        return (getAssembler().getBooleanTerms(getTerminalColumn()));
    }


    /**
     *  Orders the results by terminal steps. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTerminal(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTerminalColumn(), style);
        return;
    }


    /**
     *  Gets the Terminal column name.
     *
     * @return the column name
     */

    protected String getTerminalColumn() {
        return ("terminal");
    }


    /**
     *  Sets the input state <code> Id </code> for this query. 
     *
     *  @param  stateId the state <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stateId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchInputStateId(org.osid.id.Id stateId, boolean match) {
        getAssembler().addIdTerm(getInputStateIdColumn(), stateId, match);
        return;
    }


    /**
     *  Clears the input state <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearInputStateIdTerms() {
        getAssembler().clearTerms(getInputStateIdColumn());
        return;
    }


    /**
     *  Gets the input state <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getInputStateIdTerms() {
        return (getAssembler().getIdTerms(getInputStateIdColumn()));
    }


    /**
     *  Gets the InputStateId column name.
     *
     * @return the column name
     */

    protected String getInputStateIdColumn() {
        return ("input_state_id");
    }


    /**
     *  Tests if a <code> StateQuery </code> is available. 
     *
     *  @return <code> true </code> if a state query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputStateQuery() {
        return (false);
    }


    /**
     *  Gets the query for an input state. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the state query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputStateQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateQuery getInputStateQuery() {
        throw new org.osid.UnimplementedException("supportsInputStateQuery() is false");
    }


    /**
     *  Matches steps that have any input states. 
     *
     *  @param  match <code> true </code> to match steps with any input 
     *          states, <code> false </code> to match steps with no input 
     *          states 
     */

    @OSID @Override
    public void matchAnyInputState(boolean match) {
        getAssembler().addIdWildcardTerm(getInputStateColumn(), match);
        return;
    }


    /**
     *  Clears the input state terms. 
     */

    @OSID @Override
    public void clearInputStateTerms() {
        getAssembler().clearTerms(getInputStateColumn());
        return;
    }


    /**
     *  Gets the input state query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.process.StateQueryInspector[] getInputStateTerms() {
        return (new org.osid.process.StateQueryInspector[0]);
    }


    /**
     *  Gets the InputState column name.
     *
     * @return the column name
     */

    protected String getInputStateColumn() {
        return ("input_state");
    }


    /**
     *  Sets the next state <code> Id </code> for this query. 
     *
     *  @param  stateId the state <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stateId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchNextStateId(org.osid.id.Id stateId, boolean match) {
        getAssembler().addIdTerm(getNextStateIdColumn(), stateId, match);
        return;
    }


    /**
     *  Clears the next state <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearNextStateIdTerms() {
        getAssembler().clearTerms(getNextStateIdColumn());
        return;
    }


    /**
     *  Gets the next state <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getNextStateIdTerms() {
        return (getAssembler().getIdTerms(getNextStateIdColumn()));
    }


    /**
     *  Orders the results by next state. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByNextState(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getNextStateColumn(), style);
        return;
    }


    /**
     *  Gets the NextStateId column name.
     *
     * @return the column name
     */

    protected String getNextStateIdColumn() {
        return ("next_state_id");
    }


    /**
     *  Tests if a <code> StateQuery </code> is available. 
     *
     *  @return <code> true </code> if a state query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsNextStateQuery() {
        return (false);
    }


    /**
     *  Gets the query for a state. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the state query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsNextStateQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateQuery getNextStateQuery() {
        throw new org.osid.UnimplementedException("supportsNextStateQuery() is false");
    }


    /**
     *  Matches steps that have any next state, 
     *
     *  @param  match <code> true </code> to match steps with any next state, 
     *          <code> false </code> to match steps with no next state 
     */

    @OSID @Override
    public void matchAnyNextState(boolean match) {
        getAssembler().addIdWildcardTerm(getNextStateColumn(), match);
        return;
    }


    /**
     *  Clears the state terms. 
     */

    @OSID @Override
    public void clearNextStateTerms() {
        getAssembler().clearTerms(getNextStateColumn());
        return;
    }


    /**
     *  Gets the next state query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.process.StateQueryInspector[] getNextStateTerms() {
        return (new org.osid.process.StateQueryInspector[0]);
    }


    /**
     *  Tests if a next state search order is available. 
     *
     *  @return <code> true </code> if a state search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsNextStateSearchOrder() {
        return (false);
    }


    /**
     *  Gets the next state search order. 
     *
     *  @return the state search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsNextStateSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateSearchOrder getNextStateSearchOrder() {
        throw new org.osid.UnimplementedException("supportsNextStateSearchOrder() is false");
    }


    /**
     *  Gets the NextState column name.
     *
     * @return the column name
     */

    protected String getNextStateColumn() {
        return ("next_state");
    }


    /**
     *  Sets the work <code> Id </code> for this query. 
     *
     *  @param  workId the work <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> workId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchWorkId(org.osid.id.Id workId, boolean match) {
        getAssembler().addIdTerm(getWorkIdColumn(), workId, match);
        return;
    }


    /**
     *  Clears the work <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearWorkIdTerms() {
        getAssembler().clearTerms(getWorkIdColumn());
        return;
    }


    /**
     *  Gets the work <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getWorkIdTerms() {
        return (getAssembler().getIdTerms(getWorkIdColumn()));
    }


    /**
     *  Gets the WorkId column name.
     *
     * @return the column name
     */

    protected String getWorkIdColumn() {
        return ("work_id");
    }


    /**
     *  Tests if a <code> WorkQuery </code> is available. 
     *
     *  @return <code> true </code> if a work query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkQuery() {
        return (false);
    }


    /**
     *  Gets the query for a work. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the work query 
     *  @throws org.osid.UnimplementedException <code> supportsWorkQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkQuery getWorkQuery() {
        throw new org.osid.UnimplementedException("supportsWorkQuery() is false");
    }


    /**
     *  Matches steps that have any work. 
     *
     *  @param  match <code> true </code> to match steps with any work, <code> 
     *          false </code> to match steps with no work 
     */

    @OSID @Override
    public void matchAnyWork(boolean match) {
        getAssembler().addIdWildcardTerm(getWorkColumn(), match);
        return;
    }


    /**
     *  Clears the work query terms. 
     */

    @OSID @Override
    public void clearWorkTerms() {
        getAssembler().clearTerms(getWorkColumn());
        return;
    }


    /**
     *  Gets the work query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.workflow.WorkQueryInspector[] getWorkTerms() {
        return (new org.osid.workflow.WorkQueryInspector[0]);
    }


    /**
     *  Gets the Work column name.
     *
     * @return the column name
     */

    protected String getWorkColumn() {
        return ("work");
    }


    /**
     *  Sets the office <code> Id </code> for this query to match steps 
     *  assigned to offices. 
     *
     *  @param  officeId the office <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOfficeId(org.osid.id.Id officeId, boolean match) {
        getAssembler().addIdTerm(getOfficeIdColumn(), officeId, match);
        return;
    }


    /**
     *  Clears the office <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearOfficeIdTerms() {
        getAssembler().clearTerms(getOfficeIdColumn());
        return;
    }


    /**
     *  Gets the office <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOfficeIdTerms() {
        return (getAssembler().getIdTerms(getOfficeIdColumn()));
    }


    /**
     *  Gets the OfficeId column name.
     *
     * @return the column name
     */

    protected String getOfficeIdColumn() {
        return ("office_id");
    }


    /**
     *  Tests if a <code> OfficeQuery </code> is available. 
     *
     *  @return <code> true </code> if a office query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfficeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a office. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the office query 
     *  @throws org.osid.UnimplementedException <code> supportsOfficeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.OfficeQuery getOfficeQuery() {
        throw new org.osid.UnimplementedException("supportsOfficeQuery() is false");
    }


    /**
     *  Clears the office query terms. 
     */

    @OSID @Override
    public void clearOfficeTerms() {
        getAssembler().clearTerms(getOfficeColumn());
        return;
    }


    /**
     *  Gets the office query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.workflow.OfficeQueryInspector[] getOfficeTerms() {
        return (new org.osid.workflow.OfficeQueryInspector[0]);
    }


    /**
     *  Gets the Office column name.
     *
     * @return the column name
     */

    protected String getOfficeColumn() {
        return ("office");
    }


    /**
     *  Tests if this step supports the given record
     *  <code>Type</code>.
     *
     *  @param  stepRecordType a step record type 
     *  @return <code>true</code> if the stepRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>stepRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type stepRecordType) {
        for (org.osid.workflow.records.StepQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(stepRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  stepRecordType the step record type 
     *  @return the step query record 
     *  @throws org.osid.NullArgumentException
     *          <code>stepRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(stepRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.records.StepQueryRecord getStepQueryRecord(org.osid.type.Type stepRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.workflow.records.StepQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(stepRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(stepRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  stepRecordType the step record type 
     *  @return the step query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>stepRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(stepRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.records.StepQueryInspectorRecord getStepQueryInspectorRecord(org.osid.type.Type stepRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.workflow.records.StepQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(stepRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(stepRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param stepRecordType the step record type
     *  @return the step search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>stepRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(stepRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.records.StepSearchOrderRecord getStepSearchOrderRecord(org.osid.type.Type stepRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.workflow.records.StepSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(stepRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(stepRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this step. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param stepQueryRecord the step query record
     *  @param stepQueryInspectorRecord the step query inspector
     *         record
     *  @param stepSearchOrderRecord the step search order record
     *  @param stepRecordType step record type
     *  @throws org.osid.NullArgumentException
     *          <code>stepQueryRecord</code>,
     *          <code>stepQueryInspectorRecord</code>,
     *          <code>stepSearchOrderRecord</code> or
     *          <code>stepRecordTypestep</code> is
     *          <code>null</code>
     */
            
    protected void addStepRecords(org.osid.workflow.records.StepQueryRecord stepQueryRecord, 
                                      org.osid.workflow.records.StepQueryInspectorRecord stepQueryInspectorRecord, 
                                      org.osid.workflow.records.StepSearchOrderRecord stepSearchOrderRecord, 
                                      org.osid.type.Type stepRecordType) {

        addRecordType(stepRecordType);

        nullarg(stepQueryRecord, "step query record");
        nullarg(stepQueryInspectorRecord, "step query inspector record");
        nullarg(stepSearchOrderRecord, "step search odrer record");

        this.queryRecords.add(stepQueryRecord);
        this.queryInspectorRecords.add(stepQueryInspectorRecord);
        this.searchOrderRecords.add(stepSearchOrderRecord);
        
        return;
    }
}
