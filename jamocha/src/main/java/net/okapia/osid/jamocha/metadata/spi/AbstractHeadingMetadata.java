//
// AbstractHeadingMetadata.java
//
//     Defines a heading Metadata.
//
//
// Tom Coppeto
// Okapia
// 15 March 2013
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metadata.spi;

import org.osid.binding.java.annotation.OSID;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeHashMap;
import net.okapia.osid.torrefacto.collect.TypeSet;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a heading Metadata.
 */

public abstract class AbstractHeadingMetadata
    extends AbstractMetadata
    implements org.osid.Metadata {

    private final TypeHashMap<Long> dimensions = new TypeHashMap<>();
    private final Types types = new TypeSet();

    private final TypeHashMap<org.osid.mapping.Heading> minimums = new TypeHashMap<>();
    private final TypeHashMap<org.osid.mapping.Heading> maximums = new TypeHashMap<>();

    private final java.util.Collection<org.osid.mapping.Heading> set = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.mapping.Heading> defvals  = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.mapping.Heading> existing = new java.util.LinkedHashSet<>();


    /**
     *  Constructs a new {@code AbstractHeadingMetadata}.
     *
     *  @param elementId the Id of the element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    protected AbstractHeadingMetadata(org.osid.id.Id elementId) {
        super(org.osid.Syntax.HEADING, elementId);
        return;
    }


    /**
     *  Constructs a new {@code AbstractHeadingMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @param isLinked {@code true} if the element is linked to
     *         another element, {@code false} otherwise
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    protected AbstractHeadingMetadata(org.osid.id.Id elementId, boolean isArray, boolean isLinked) {
        super(org.osid.Syntax.HEADING, elementId, isArray, isLinked);
        return;
    }

    
    /**
     *  Gets the set of acceptable heading types. 
     *
     *  @return a set of heading types or an empty array if not restricted 
     *  @throws org.osid.IllegalStateException syntax is not a
     *          <code>DATEHEADING</code> or <code>HEADING</code>
     */

    @OSID @Override
    public org.osid.type.Type[] getHeadingTypes() {
        return (this.types.toArray());
    }


    /**
     *  Tests if the given heading type is supported. 
     *
     *  @param  headingType a heading Type 
     *  @return <code> true </code> if the type is supported, <code> false 
     *          </code> otherwise 
     *  @throws org.osid.IllegalStateException syntax is not a
     *          <code>DATEHEADING</code> or <code>HEADING</code>
     *  @throws org.osid.NullArgumentException <code> headingType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean supportsHeadingType(org.osid.type.Type headingType) {
        return (this.types.contains(headingType));
    }


    /**
     *  Gets the number of axes for a given supported heading type. 
     *
     *  @param  headingType a heading Type 
     *  @return the number of axes 
     *  @throws org.osid.IllegalStateException syntax is not a <code> 
     *          HEADING </code> 
     *  @throws org.osid.NullArgumentException <code> headingType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsHeadingType(headingType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public long getAxesForHeadingType(org.osid.type.Type headingType) {
        if (this.dimensions.containsKey(headingType)) {
            return (this.dimensions.get(headingType));
        } else {
            throw new org.osid.UnsupportedException(headingType + " not supported");
        }
    }


    /**
     *  Gets the minimum heading values given supported heading type. 
     *
     *  @param  headingType a heading Type 
     *  @return the minimum heading values 
     *  @throws org.osid.IllegalStateException syntax is not a <code> 
     *          HEADING </code> 
     *  @throws org.osid.NullArgumentException <code> headingType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsHeadingType(headingType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public java.math.BigDecimal[] getMinimumHeadingValues(org.osid.type.Type headingType) {
        if (this.dimensions.containsKey(headingType)) {
            return (this.minimums.get(headingType).getValues());
        } else {
            throw new org.osid.UnsupportedException(headingType + " not supported");
        }
    }


    /**
     *  Gets the maximum heading values given supported heading type. 
     *
     *  @param  headingType a heading Type 
     *  @return the maximum heading values 
     *  @throws org.osid.IllegalStateException syntax is not a <code> 
     *          HEADING </code> 
     *  @throws org.osid.NullArgumentException <code> headingType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsHeadingType(headingType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public java.math.BigDecimal[] getMaximumHeadingValues(org.osid.type.Type headingType) {
        if (this.dimensions.containsKey(headingType)) {
            return (this.maximums.get(headingType).getValues());
        } else {
            throw new org.osid.UnsupportedException(headingType + " not supported");
        }
    }    


    /**
     *  Add support for a heading type.
     *
     *  @param headingType the type of heading
     *  @param dimensions the number of dimensions for the type
     *  @throws org.osid.InvalidArgumentException {@code dimensions}
     *          is negative
     *  @throws org.osid.NullArgumentException {@code headingType} is
     *          {@code null}
     */

    protected void addHeadingType(org.osid.type.Type headingType, long dimensions) {
        nullarg(headingType, "heading type");
        cardinalarg(dimensions, "dimensions");

        this.dimensions.put(headingType, dimensions);

        return;
    }


    /**
     *  Sets the heading range.
     *
     *  @param min the minimum value
     *  @param max the maximum value
     *  @throws org.osid.InvalidArgumentException {@code min} is
     *          greater than {@code max} or, {@code min} or {@code
     *          max} is negative
     *  @throws org.osid.UnsupportedException {@code} types not supported
     */

    protected void setHeadingRange(org.osid.mapping.Heading min, org.osid.mapping.Heading max) {
        nullarg(min, "min");
        nullarg(max, "max");

        if (!min.getHeadingType().equals(max.getHeadingType())) {
            throw new org.osid.InvalidArgumentException("heading types differ");
        }

        if (min.getDimensions() != max.getDimensions()) {
            throw new org.osid.InvalidArgumentException("heading dimensions differ");
        }

        if (!supportsHeadingType(min.getHeadingType())) {
            throw new org.osid.UnsupportedException("heading type not supported");
        }

        if (!supportsHeadingType(max.getHeadingType())) {
            throw new org.osid.UnsupportedException("heading type not supported");
        }

        for (int i = 0; i < min.getDimensions(); i++) {
            if (min.getValues()[i].compareTo(max.getValues()[i]) > 0) {
                throw new org.osid.InvalidArgumentException("min is greater than max");
            }
        }

        return;
    }


    /**
     *  Gets the set of acceptable heading values. 
     *
     *  @return a set of headings or an empty array if not restricted 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          HEADING </code>
     */

    @OSID @Override
    public org.osid.mapping.Heading[] getHeadingSet() {
        return (this.set.toArray(new org.osid.mapping.Heading[this.set.size()]));
    }

    
    /**
     *  Sets the heading set.
     *
     *  @param values a collection of accepted heading values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setHeadingSet(java.util.Collection<org.osid.mapping.Heading> values) {
        this.set.clear();
        addToHeadingSet(values);
        return;
    }


    /**
     *  Adds a collection of values to the heading set.
     *
     *  @param values a collection of accepted heading values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addToHeadingSet(java.util.Collection<org.osid.mapping.Heading> values) {
        nullarg(values, "heading set");
        this.set.addAll(values);
        return;
    }


    /**
     *  Adds a value to the heading set.
     *
     *  @param value a heading value
     *  @throws org.osid.InvalidArgumentException value is negative
     */

    protected void addToHeadingSet(org.osid.mapping.Heading value) {
        nullarg(value, "heading value");
        this.set.add(value);
        return;
    }


    /**
     *  Removes a value from the heading set.
     *
     *  @param value a heading value
     *  @throws org.osid.InvalidArgumentException value is negative
     */

    protected void removeFromHeadingSet(org.osid.mapping.Heading value) {
        nullarg(value, "heading value");
        this.set.remove(value);
        return;
    }


    /**
     *  Clears the heading set.
     */

    protected void clearHeadingSet() {
        this.set.clear();
        return;
    }


    /**
     *  Gets the default heading values. These are the values used
     *  if the element value is not provided or is cleared. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the default heading values 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          HEADING </code> or <code> isRequired() </code> is
     *          <code> true </code>
     */

    @OSID @Override
    public org.osid.mapping.Heading[] getDefaultHeadingValues() {
        return (this.defvals.toArray(new org.osid.mapping.Heading[this.defvals.size()]));
    }


    /**
     *  Sets the default heading set.
     *
     *  @param values a collection of default heading values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setDefaultHeadingValues(java.util.Collection<org.osid.mapping.Heading> values) {
        clearDefaultHeadingValues();
        addDefaultHeadingValues(values);
        return;
    }


    /**
     *  Adds a collection of default heading values.
     *
     *  @param values a collection of default heading values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addDefaultHeadingValues(java.util.Collection<org.osid.mapping.Heading> values) {
        nullarg(values, "default heading values");
        this.defvals.addAll(values);
        return;
    }


    /**
     *  Adds a default heading value.
     *
     *  @param value a heading value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void addDefaultHeadingValue(org.osid.mapping.Heading value) {
        nullarg(value, "default heading value");
        this.defvals.add(value);
        return;
    }


    /**
     *  Removes a default heading value.
     *
     *  @param value a heading value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void removeDefaultHeadingValue(org.osid.mapping.Heading value) {
        nullarg(value, "default heading value");
        this.defvals.remove(value);
        return;
    }


    /**
     *  Clears the default heading values.
     */

    protected void clearDefaultHeadingValues() {
        this.defvals.clear();
        return;
    }


    /**
     *  Gets the existing heading values. If <code> hasValue()
     *  </code> and <code> isRequired() </code> are <code> false,
     *  </code> then these values are the default values. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the existing heading values 
     *  @throws org.osid.IllegalStateException syntax is not a <code> 
     *          HEADING </code> or <code> isValueKnown() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.mapping.Heading[] getExistingHeadingValues() {
        return (this.existing.toArray(new org.osid.mapping.Heading[this.existing.size()]));
    }


    /**
     *  Sets the existing heading set.
     *
     *  @param values a collection of existing heading values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setExistingHeadingValues(java.util.Collection<org.osid.mapping.Heading> values) {
        clearExistingHeadingValues();
        addExistingHeadingValues(values);
        return;
    }


    /**
     *  Adds a collection of existing heading values.
     *
     *  @param values a collection of existing heading values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addExistingHeadingValues(java.util.Collection<org.osid.mapping.Heading> values) {
        nullarg(values, "existing heading values");

        this.existing.addAll(values);
        setValueKnown(true);

        return;
    }


    /**
     *  Adds a existing heading value.
     *
     *  @param value a heading value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void addExistingHeadingValue(org.osid.mapping.Heading value) {
        nullarg(value, "existing heading value");

        this.existing.add(value);
        setValueKnown(true);

        return;
    }


    /**
     *  Removes a existing heading value.
     *
     *  @param value a heading value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void removeExistingHeadingValue(org.osid.mapping.Heading value) {
        nullarg(value, "existing heading value");
        this.existing.remove(value);
        return;
    }


    /**
     *  Clears the existing heading values.
     */

    protected void clearExistingHeadingValues() {
        this.existing.clear();
        setValueKnown(false);
        return;
    }    
}