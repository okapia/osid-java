//
// AbstractAdapterAssessmentTakenLookupSession.java
//
//    An AssessmentTaken lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.assessment.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An AssessmentTaken lookup session adapter.
 */

public abstract class AbstractAdapterAssessmentTakenLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.assessment.AssessmentTakenLookupSession {

    private final org.osid.assessment.AssessmentTakenLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterAssessmentTakenLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterAssessmentTakenLookupSession(org.osid.assessment.AssessmentTakenLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Bank/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Bank Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBankId() {
        return (this.session.getBankId());
    }


    /**
     *  Gets the {@code Bank} associated with this session.
     *
     *  @return the {@code Bank} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.Bank getBank()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getBank());
    }


    /**
     *  Tests if this user can perform {@code AssessmentTaken} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupAssessmentsTaken() {
        return (this.session.canLookupAssessmentsTaken());
    }


    /**
     *  A complete view of the {@code AssessmentTaken} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAssessmentTakenView() {
        this.session.useComparativeAssessmentTakenView();
        return;
    }


    /**
     *  A complete view of the {@code AssessmentTaken} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAssessmentTakenView() {
        this.session.usePlenaryAssessmentTakenView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include assessments taken in banks which are children
     *  of this bank in the bank hierarchy.
     */

    @OSID @Override
    public void useFederatedBankView() {
        this.session.useFederatedBankView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this bank only.
     */

    @OSID @Override
    public void useIsolatedBankView() {
        this.session.useIsolatedBankView();
        return;
    }
    
     
    /**
     *  Gets the {@code AssessmentTaken} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code AssessmentTaken} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code AssessmentTaken} and
     *  retained for compatibility.
     *
     *  @param assessmentTakenId {@code Id} of the {@code AssessmentTaken}
     *  @return the assessment taken
     *  @throws org.osid.NotFoundException {@code assessmentTakenId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code assessmentTakenId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTaken getAssessmentTaken(org.osid.id.Id assessmentTakenId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentTaken(assessmentTakenId));
    }


    /**
     *  Gets an {@code AssessmentTakenList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  assessmentsTaken specified in the {@code Id} list, in the
     *  order of the list, including duplicates, or an error results
     *  if an {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code AssessmentsTaken}
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  assessmentTakenIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code AssessmentTaken} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code assessmentTakenIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenByIds(org.osid.id.IdList assessmentTakenIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentsTakenByIds(assessmentTakenIds));
    }


    /**
     *  Gets an {@code AssessmentTakenList} corresponding to the given
     *  assessment taken genus {@code Type} which does not include
     *  assessments taken of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  assessments taken or an error results. Otherwise, the returned list
     *  may contain only those assessments taken that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  assessmentTakenGenusType an assessmentTaken genus type 
     *  @return the returned {@code AssessmentTaken} list
     *  @throws org.osid.NullArgumentException
     *          {@code assessmentTakenGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenByGenusType(org.osid.type.Type assessmentTakenGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentsTakenByGenusType(assessmentTakenGenusType));
    }


    /**
     *  Gets an {@code AssessmentTakenList} corresponding to the given
     *  assessment taken genus {@code Type} and include any additional
     *  assessments taken with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  assessments taken or an error results. Otherwise, the returned
     *  list may contain only those assessments taken that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  assessmentTakenGenusType an assessmentTaken genus type 
     *  @return the returned {@code AssessmentTaken} list
     *  @throws org.osid.NullArgumentException
     *          {@code assessmentTakenGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenByParentGenusType(org.osid.type.Type assessmentTakenGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentsTakenByParentGenusType(assessmentTakenGenusType));
    }


    /**
     *  Gets an {@code AssessmentTakenList} containing the given
     *  assessment taken record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  assessments taken or an error results. Otherwise, the returned list
     *  may contain only those assessments taken that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  assessmentTakenRecordType an assessmentTaken record type 
     *  @return the returned {@code AssessmentTaken} list
     *  @throws org.osid.NullArgumentException
     *          {@code assessmentTakenRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenByRecordType(org.osid.type.Type assessmentTakenRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentsTakenByRecordType(assessmentTakenRecordType));
    }


    /**
     *  Gets an {@code AssessmentTakenList} started in the
     *  given date range inclusive. In plenary mode, the returned list
     *  contains all known assessments taken or an error
     *  results. Otherwise, the returned list may contain only those
     *  assessments taken that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  @param  from start date 
     *  @param  to end date 
     *  @return the returned {@code AssessmentTaken} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code from} or {@code 
     *          to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenByDate(org.osid.calendaring.DateTime from, 
                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentsTakenByDate(from, to));
    }


    /**
     *  Gets an {@code AssessmentTakenList} for the given
     *  resource. In plenary mode, the returned list contains all
     *  known assessments taken or an error results. Otherwise, the
     *  returned list may contain only those assessments taken that
     *  are accessible through this session.
     *
     *  @param  resourceId {@code Id} of a {@code Resource} 
     *  @return the returned {@code AssessmentTaken} list 
     *  @throws org.osid.NullArgumentException {@code resourceId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenForTaker(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentsTakenForTaker(resourceId));
    }


    /**
     *  Gets an {@code AssessmentTakenList} started in the
     *  given date range inclusive for the given resource. In plenary
     *  mode, the returned list contains all known assessments taken
     *  or an error results.  Otherwise, the returned list may contain
     *  only those assessments taken that are accessible through this
     *  session.
     *
     *  @param  resourceId {@code Id} of a {@code Resource} 
     *  @param  from start date 
     *  @param  to end date 
     *  @return the returned {@code AssessmentTaken} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code resourceId, from} 
     *          or {@code to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenByDateForTaker(org.osid.id.Id resourceId, 
                                                                                     org.osid.calendaring.DateTime from, 
                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentsTakenByDateForTaker(resourceId, from, to));
    }


    /**
     *  Gets an {@code AssessmentTakenList} for the given
     *  assessment.  In plenary mode, the returned list contains all
     *  known assessments taken or an error results. Otherwise, the
     *  returned list may contain only those assessments taken that
     *  are accessible through this session.
     *
     *  @param  assessmentId {@code Id} of an {@code Assessment} 
     *  @return the returned {@code AssessmentTaken} list 
     *  @throws org.osid.NullArgumentException {@code assessmentId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenForAssessment(org.osid.id.Id assessmentId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentsTakenForAssessment(assessmentId));
    }


    /**
     *  Gets an {@code AssessmentTakenList} started in the given date 
     *  range inclusive for the given assessment. In plenary mode, the 
     *  returned list contains all known assessments taken or an error 
     *  results. Otherwise, the returned list may contain only those 
     *  assessments taken that are accessible through this session. 
     *
     *  @param  assessmentId {@code Id} of an {@code Assessment} 
     *  @param  from start date 
     *  @param  to end date 
     *  @return the returned {@code AssessmentTaken} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code assessmentId, from 
     *         } or {@code to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenByDateForAssessment(org.osid.id.Id assessmentId, 
                                                                                          org.osid.calendaring.DateTime from, 
                                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentsTakenByDateForAssessment(assessmentId, from, to));
    }


    /**
     *  Gets an {@code AssessmentTakenList} for the given
     *  resource and assessment. In plenary mode, the returned list
     *  contains all known assessments taken or an error
     *  results. Otherwise, the returned list may contain only those
     *  assessments taken that are accessible through this session.
     *
     *  @param  resourceId {@code Id} of a {@code Resource} 
     *  @param  assessmentId {@code Id} of an {@code Assessment} 
     *  @return the returned {@code AssessmentTaken} list 
     *  @throws org.osid.NullArgumentException {@code resourceId} or 
     *          {@code assessmentId} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenForTakerAndAssessment(org.osid.id.Id resourceId, 
                                                                                            org.osid.id.Id assessmentId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getAssessmentsTakenForTakerAndAssessment(resourceId, assessmentId));
    }


    /**
     *  Gets an {@code AssessmentTakenList} started in the given date 
     *  range inclusive for the given resource and assessment. In plenary 
     *  mode, the returned list contains all known assessments taken or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  assessments taken that are accessible through this session. 
     *
     *  @param  resourceId {@code Id} of a {@code Resource} 
     *  @param  assessmentId {@code Id} of an {@code Assessment} 
     *  @param  from start date 
     *  @param  to end date 
     *  @return the returned {@code AssessmentTaken} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code resourceId, 
     *          assessmentId, from} or {@code to} is {@code null 
     *         } 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenByDateForTakerAndAssessment(org.osid.id.Id resourceId, 
                                                                                                  org.osid.id.Id assessmentId, 
                                                                                                  org.osid.calendaring.DateTime from, 
                                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentsTakenByDateForTakerAndAssessment(resourceId, assessmentId, from, to));
    }

    
    /**
     *  Gets an {@code AssessmentTakenList} by the given assessment 
     *  offered. In plenary mode, the returned list contains all known 
     *  assessments taken or an error results. Otherwise, the returned list 
     *  may contain only those assessments taken that are accessible through 
     *  this session. 
     *
     *  @param  assessmentOfferedId {@code Id} of an {@code 
     *          AssessmentOffered} 
     *  @return the returned {@code AssessmentTaken} list 
     *  @throws org.osid.NullArgumentException {@code
     *          assessmentOfferedId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenForAssessmentOffered(org.osid.id.Id assessmentOfferedId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentsTakenForAssessmentOffered(assessmentOfferedId));
    }


    /**
     *  Gets an {@code AssessmentTakenList} started in the given date
     *  range inclusive for the given assessment offered. In plenary
     *  mode, the returned list contains all known assessments taken
     *  or an error results. Otherwise, the returned list may contain
     *  only those assessments taken that are accessible through this
     *  session.
     *
     *  @param  assessmentOfferedId {@code Id} of an {@code 
     *          AssessmentOffered} 
     *  @param  from start date 
     *  @param  to end date 
     *  @return the returned {@code AssessmentTaken} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code
     *          assessmentOfferedId, from}, or {@code to} is {@code
     *          null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenByDateForAssessmentOffered(org.osid.id.Id assessmentOfferedId, 
                                                                                                 org.osid.calendaring.DateTime from, 
                                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getAssessmentsTakenByDateForAssessmentOffered(assessmentOfferedId, from, to));
    }


    /**
     *  Gets an {@code AssessmentTakenList} for the given resource and
     *  assessment offered. In plenary mode, the returned list
     *  contains all known assessments taken or an error
     *  results. Otherwise, the returned list may contain only those
     *  assessments taken that are accessible through this session.
     *
     *  @param  resourceId {@code Id} of a {@code Resource} 
     *  @param  assessmentOfferedId {@code Id} of an {@code 
     *          AssessmentOffered} 
     *  @return the returned {@code AssessmentTaken} list 
     *  @throws org.osid.NullArgumentException {@code resourceId} or 
     *          {@code assessmenOfferedtId} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenForTakerAndAssessmentOffered(org.osid.id.Id resourceId, 
                                                                                                   org.osid.id.Id assessmentOfferedId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getAssessmentsTakenForTakerAndAssessmentOffered(resourceId, assessmentOfferedId));
    }

    
    /**
     *  Gets an {@code AssessmentTakenList} started in the
     *  given date range inclusive for the given resource and
     *  assessment offered. In plenary mode, the returned list
     *  contains all known assessments taken or an error
     *  results. Otherwise, the returned list may contain only those
     *  assessments taken that are accessible through this session.
     *
     *  @param  resourceId {@code Id} of a {@code Resource} 
     *  @param  assessmentOfferedId {@code Id} of an {@code 
     *          AssessmentOffered} 
     *  @param  from start date 
     *  @param  to end date 
     *  @return the returned {@code AssessmentTaken} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code resourceId,
     *          assessmentOfferedId, from}, or {@code to} is {@code
     *          null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenByDateForTakerAndAssessmentOffered(org.osid.id.Id resourceId, 
                                                                                                         org.osid.id.Id assessmentOfferedId, 
                                                                                                         org.osid.calendaring.DateTime from, 
                                                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getAssessmentsTakenByDateForTakerAndAssessmentOffered(resourceId, assessmentOfferedId, from, to));
    }


    /**
     *  Gets all {@code AssessmentsTaken}. 
     *
     *  In plenary mode, the returned list contains all known
     *  assessments taken or an error results. Otherwise, the returned
     *  list may contain only those assessments taken that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @return a list of {@code AssessmentsTaken} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTaken()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentsTaken());
    }
}
