//
// AbstractRoom.java
//
//     Defines a Room.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.room.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Room</code>.
 */

public abstract class AbstractRoom
    extends net.okapia.osid.jamocha.spi.AbstractTemporalOsidObject
    implements org.osid.room.Room {

    private org.osid.room.Building building;
    private org.osid.room.Floor floor;
    private org.osid.room.Room enclosingRoom;
    private final java.util.Collection<org.osid.room.Room> subdivisions = new java.util.LinkedHashSet<>();
    private org.osid.locale.DisplayText designatedName;
    private String roomNumber;
    private String code;
    private java.math.BigDecimal area;
    private long occupancyLimit;
    private final java.util.Collection<org.osid.resource.Resource> resources = new java.util.LinkedHashSet<>();

    private final java.util.Collection<org.osid.room.records.RoomRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the building. 
     *
     *  @return the building <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getBuildingId() {
        return (this.building.getId());
    }


    /**
     *  Gets the building. 
     *
     *  @return the building 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.room.Building getBuilding()
        throws org.osid.OperationFailedException {

        return (this.building);
    }


    /**
     *  Sets the building.
     *
     *  @param building a building
     *  @throws org.osid.NullArgumentException
     *          <code>building</code> is <code>null</code>
     */

    protected void setBuilding(org.osid.room.Building building) {
        nullarg(building, "building");
        this.building = building;
        return;
    }


    /**
     *  Gets the floor <code> Id. </code> 
     *
     *  @return the floor <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getFloorId() {
        return (this.floor.getId());
    }


    /**
     *  Gets the floor. 
     *
     *  @return the floor 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.room.Floor getFloor()
        throws org.osid.OperationFailedException {

        return (this.floor);
    }


    /**
     *  Sets the floor.
     *
     *  @param floor a floor
     *  @throws org.osid.NullArgumentException
     *          <code>floor</code> is <code>null</code>
     */

    protected void setFloor(org.osid.room.Floor floor) {
        nullarg(floor, "floor");
        this.floor = floor;
        return;
    }


    /**
     *  Tests if this room is a subdivision of another room. 
     *
     *  @return <code> true </code> if this room is a subdivisiaion, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean isSubdivision() {
        return (this.enclosingRoom != null);
    }


    /**
     *  Gets the enclosing room <code> Id </code> if this room is a 
     *  subdivision. 
     *
     *  @return the enclosing room <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isSubdivision() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getEnclosingRoomId() {
        if (!isSubdivision()) {
            throw new org.osid.IllegalStateException("isSubdivision() is false");
        }

        return (this.enclosingRoom.getId());
    }


    /**
     *  Gets the enclosing room if this room is a subdivision. 
     *
     *  @return the enlcosing room 
     *  @throws org.osid.IllegalStateException <code> isSubdivision() </code> 
     *          is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.room.Room getEnclosingRoom()
        throws org.osid.OperationFailedException {

        if (!isSubdivision()) {
            throw new org.osid.IllegalStateException("isSubdivision() is false");
        }

        return (this.enclosingRoom);
    }


    /**
     *  Sets the enclosing room.
     *
     *  @param room an enclosing room
     *  @throws org.osid.NullArgumentException <code>room</code> is
     *          <code>null</code>
     */

    protected void setEnclosingRoom(org.osid.room.Room room) {
        nullarg(room, "enclosing room");
        this.enclosingRoom = room;
        return;
    }


    /**
     *  Gets the subdivision room <code> Ids </code> if this room is 
     *  subdivided. 
     *
     *  @return the subdivision room <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getSubdivisionIds() {
        try {
            org.osid.room.RoomList subdivisions = getSubdivisions();
            return (new net.okapia.osid.jamocha.adapter.converter.room.room.RoomToIdList(subdivisions));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the subdivision rooms if this room is subdivided. 
     *
     *  @return the subdivision rooms 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.room.RoomList getSubdivisions()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.room.room.ArrayRoomList(this.subdivisions));
    }


    /**
     *  Adds a subdivision.
     *
     *  @param subdivision a subdivision
     *  @throws org.osid.NullArgumentException
     *          <code>subdivision</code> is <code>null</code>
     */

    protected void addSubdivision(org.osid.room.Room subdivision) {
        nullarg(subdivision, "subdivision");
        this.subdivisions.add(subdivision);
        return;
    }


    /**
     *  Sets all the subdivisions.
     *
     *  @param subdivisions a collection of subdivisions
     *  @throws org.osid.NullArgumentException
     *          <code>subdivisions</code> is <code>null</code>
     */

    protected void setSubdivisions(java.util.Collection<org.osid.room.Room> subdivisions) {
        nullarg(subdivisions, "subdivisions");
        this.subdivisions.clear();
        this.subdivisions.addAll(subdivisions);
        return;
    }


    /**
     *  Gets the designated or formal name of the room (e.g. Twenty Chimneys). 
     *  The display name may be mapped to this designated name, room number, 
     *  or some other room label. 
     *
     *  @return the room number 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getDesignatedName() {
        return (this.designatedName);
    }


    /**
     *  Sets the designated name.
     *
     *  @param name a designated name
     *  @throws org.osid.NullArgumentException
     *          <code>name</code> is <code>null</code>
     */

    protected void setDesignatedName(org.osid.locale.DisplayText name) {
        nullarg(name, "designated name");
        this.designatedName = name;
        return;
    }


    /**
     *  Gets the complete room number including the building and floor (e.g. 
     *  W20-306). 
     *
     *  @return the room number 
     */

    @OSID @Override
    public String getRoomNumber() {
        return (this.roomNumber);
    }


    /**
     *  Sets the room number.
     *
     *  @param number a room number
     *  @throws org.osid.NullArgumentException <code>number</code> is
     *          <code>null</code>
     */

    protected void setRoomNumber(String number) {
        nullarg(number, "number");
        this.roomNumber = number;
        return;
    }


    /**
     *  Gets the room number within a floor (e.g. 06). 
     *
     *  @return the room code 
     */

    @OSID @Override
    public String getCode() {
        return (this.code);
    }


    /**
     *  Sets the code.
     *
     *  @param code a code
     *  @throws org.osid.NullArgumentException
     *          <code>code</code> is <code>null</code>
     */

    protected void setCode(String code) {
        nullarg(code, "room code");
        this.code = code;
        return;
    }


    /**
     *  Tests if an area is available. 
     *
     *  @return <code> true </code> if an area is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasArea() {
        return (this.area != null);
    }


    /**
     *  Gets the area of this room in square feet. 
     *
     *  @return the area in square feet 
     *  @throws org.osid.IllegalStateException <code> hasArea() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getArea() {
        if (!hasArea()) {
            throw new org.osid.IllegalStateException("hasArea() is false");
        }

        return (this.area);
    }


    /**
     *  Sets the area.
     *
     *  @param area an area
     *  @throws org.osid.InvalidArgumentException <code>area</code> is
     *          negative
     *  @throws org.osid.NullArgumentException <code>area</code> is
     *          <code>null</code>
     */

    protected void setArea(java.math.BigDecimal area) {
        cardinalarg(area, "area");
        this.area = area;
        return;
    }


    /**
     *  Gets the limit on occupancy of this room. 
     *
     *  @return the occupancy limit 
     */

    @OSID @Override
    public long getOccupancyLimit() {
        return (this.occupancyLimit);
    }


    /**
     *  Sets the occupancy limit.
     *
     *  @param limit an occupancy limit
     *  @throws org.osid.InvalidArgumentException <code>limit</code>
     *          is negative
     */

    protected void setOccupancyLimit(long limit) {
        cardinalarg(limit, "occupancy limit");
        this.occupancyLimit = limit;
        return;
    }


    /**
     *  Gets the <code> Ids </code> of any resources in this room such as 
     *  projectors, white boards, or other classes of equiptment. 
     *
     *  @return the resource <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getResourceIds() {
        try {
            org.osid.resource.ResourceList resources = getResources();
            return (new net.okapia.osid.jamocha.adapter.converter.resource.resource.ResourceToIdList(resources));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets a list of resources in this room such as projectors, white 
     *  boards, or other classes of equiptment. These classes of equipment are 
     *  expressed as resources although they may be managed at the individual 
     *  item level in the Inventory OSID. These resources are to facilitate 
     *  searches among equipped rooms. 
     *
     *  @return a list of resources 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getResources()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.resource.resource.ArrayResourceList(this.resources));
    }


    /**
     *  Adds a resource.
     *
     *  @param resource a resource
     *  @throws org.osid.NullArgumentException
     *          <code>resource</code> is <code>null</code>
     */

    protected void addResource(org.osid.resource.Resource resource) {
        nullarg(resource, "resource");
        this.resources.add(resource);
        return;
    }


    /**
     *  Sets all the resources.
     *
     *  @param resources a collection of resources
     *  @throws org.osid.NullArgumentException
     *          <code>resources</code> is <code>null</code>
     */

    protected void setResources(java.util.Collection<org.osid.resource.Resource> resources) {
        nullarg(resources, "resources");
        this.resources.clear();
        this.resources.addAll(resources);
        return;
    }


    /**
     *  Tests if this room supports the given record
     *  <code>Type</code>.
     *
     *  @param  roomRecordType a room record type 
     *  @return <code>true</code> if the roomRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>roomRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type roomRecordType) {
        for (org.osid.room.records.RoomRecord record : this.records) {
            if (record.implementsRecordType(roomRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>Room</code>
     *  record <code>Type</code>.
     *
     *  @param  roomRecordType the room record type 
     *  @return the room record 
     *  @throws org.osid.NullArgumentException
     *          <code>roomRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(roomRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.room.records.RoomRecord getRoomRecord(org.osid.type.Type roomRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.records.RoomRecord record : this.records) {
            if (record.implementsRecordType(roomRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(roomRecordType + " is not supported");
    }


    /**
     *  Adds a record to this room. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param roomRecord the room record
     *  @param roomRecordType room record type
     *  @throws org.osid.NullArgumentException
     *          <code>roomRecord</code> or
     *          <code>roomRecordTyperoom</code> is
     *          <code>null</code>
     */
            
    protected void addRoomRecord(org.osid.room.records.RoomRecord roomRecord, 
                                 org.osid.type.Type roomRecordType) {

        nullarg(roomRecord, "room record");
        addRecordType(roomRecordType);
        this.records.add(roomRecord);
        
        return;
    }
}
