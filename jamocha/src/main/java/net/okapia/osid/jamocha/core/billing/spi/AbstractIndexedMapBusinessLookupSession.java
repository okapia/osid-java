//
// AbstractIndexedMapBusinessLookupSession.java
//
//    A simple framework for providing a Business lookup service
//    backed by a fixed collection of businesses with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.billing.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Business lookup service backed by a
 *  fixed collection of businesses. The businesses are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some businesses may be compatible
 *  with more types than are indicated through these business
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Businesses</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapBusinessLookupSession
    extends AbstractMapBusinessLookupSession
    implements org.osid.billing.BusinessLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.billing.Business> businessesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.billing.Business>());
    private final MultiMap<org.osid.type.Type, org.osid.billing.Business> businessesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.billing.Business>());


    /**
     *  Makes a <code>Business</code> available in this session.
     *
     *  @param  business a business
     *  @throws org.osid.NullArgumentException <code>business<code> is
     *          <code>null</code>
     */

    @Override
    protected void putBusiness(org.osid.billing.Business business) {
        super.putBusiness(business);

        this.businessesByGenus.put(business.getGenusType(), business);
        
        try (org.osid.type.TypeList types = business.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.businessesByRecord.put(types.getNextType(), business);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a business from this session.
     *
     *  @param businessId the <code>Id</code> of the business
     *  @throws org.osid.NullArgumentException <code>businessId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeBusiness(org.osid.id.Id businessId) {
        org.osid.billing.Business business;
        try {
            business = getBusiness(businessId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.businessesByGenus.remove(business.getGenusType());

        try (org.osid.type.TypeList types = business.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.businessesByRecord.remove(types.getNextType(), business);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeBusiness(businessId);
        return;
    }


    /**
     *  Gets a <code>BusinessList</code> corresponding to the given
     *  business genus <code>Type</code> which does not include
     *  businesses of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known businesses or an error results. Otherwise,
     *  the returned list may contain only those businesses that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  businessGenusType a business genus type 
     *  @return the returned <code>Business</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>businessGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.BusinessList getBusinessesByGenusType(org.osid.type.Type businessGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.billing.business.ArrayBusinessList(this.businessesByGenus.get(businessGenusType)));
    }


    /**
     *  Gets a <code>BusinessList</code> containing the given
     *  business record <code>Type</code>. In plenary mode, the
     *  returned list contains all known businesses or an error
     *  results. Otherwise, the returned list may contain only those
     *  businesses that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  businessRecordType a business record type 
     *  @return the returned <code>business</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>businessRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.BusinessList getBusinessesByRecordType(org.osid.type.Type businessRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.billing.business.ArrayBusinessList(this.businessesByRecord.get(businessRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.businessesByGenus.clear();
        this.businessesByRecord.clear();

        super.close();

        return;
    }
}
