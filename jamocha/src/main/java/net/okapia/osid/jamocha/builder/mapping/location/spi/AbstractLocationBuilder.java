//
// AbstractLocation.java
//
//     Defines a Location builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.mapping.location.spi;


/**
 *  Defines a <code>Location</code> builder.
 */

public abstract class AbstractLocationBuilder<T extends AbstractLocationBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.mapping.location.LocationMiter location;


    /**
     *  Constructs a new <code>AbstractLocationBuilder</code>.
     *
     *  @param location the location to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractLocationBuilder(net.okapia.osid.jamocha.builder.mapping.location.LocationMiter location) {
        super(location);
        this.location = location;
        return;
    }


    /**
     *  Builds the location.
     *
     *  @return the new location
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.mapping.Location build() {
        (new net.okapia.osid.jamocha.builder.validator.mapping.location.LocationValidator(getValidations())).validate(this.location);
        return (new net.okapia.osid.jamocha.builder.mapping.location.ImmutableLocation(this.location));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the location miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.mapping.location.LocationMiter getMiter() {
        return (this.location);
    }


    /**
     *  Sets the spatial unit.
     *
     *  @param spatialUnit a spatial unit
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>spatialUnit</code> is <code>null</code>
     */

    public T spatialUnit(org.osid.mapping.SpatialUnit spatialUnit) {
        getMiter().setSpatialUnit(spatialUnit);
        return (self());
    }


    /**
     *  Adds a Location record.
     *
     *  @param record a location record
     *  @param recordType the type of location record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.mapping.records.LocationRecord record, org.osid.type.Type recordType) {
        getMiter().addLocationRecord(record, recordType);
        return (self());
    }
}       


