//
// AbstractGradeSystem.java
//
//     Defines a GradeSystem builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.grading.gradesystem.spi;


/**
 *  Defines a <code>GradeSystem</code> builder.
 */

public abstract class AbstractGradeSystemBuilder<T extends AbstractGradeSystemBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.grading.gradesystem.GradeSystemMiter gradeSystem;


    /**
     *  Constructs a new <code>AbstractGradeSystemBuilder</code>.
     *
     *  @param gradeSystem the grade system to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractGradeSystemBuilder(net.okapia.osid.jamocha.builder.grading.gradesystem.GradeSystemMiter gradeSystem) {
        super(gradeSystem);
        this.gradeSystem = gradeSystem;
        return;
    }


    /**
     *  Builds the grade system.
     *
     *  @return the new grade system
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.grading.GradeSystem build() {
        (new net.okapia.osid.jamocha.builder.validator.grading.gradesystem.GradeSystemValidator(getValidations())).validate(this.gradeSystem);
        return (new net.okapia.osid.jamocha.builder.grading.gradesystem.ImmutableGradeSystem(this.gradeSystem));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the grade system miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.grading.gradesystem.GradeSystemMiter getMiter() {
        return (this.gradeSystem);
    }


    /**
     *  Adds a grade.
     *
     *  @param grade a grade
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>grade</code> is <code>null</code>
     */

    public T grade(org.osid.grading.Grade grade) {
        getMiter().addGrade(grade);
        return (self());
    }


    /**
     *  Sets all the grades.
     *
     *  @param grades a collection of grades
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>grades</code> is <code>null</code>
     */

    public T grades(java.util.Collection<org.osid.grading.Grade> grades) {
        getMiter().setGrades(grades);
        return (self());
    }


    /**
     *  Sets the lowest numeric score.
     *
     *  @param score a lowest numeric score
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>score</code> is
     *          <code>null</code>
     */

    public T lowestNumericScore(java.math.BigDecimal score) {
        getMiter().setLowestNumericScore(score);
        return (self());
    }


    /**
     *  Sets the numeric score increment.
     *
     *  @param increment a numeric score increment
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>increment</code>
     *          is <code>null</code>
     */

    public T numericScoreIncrement(java.math.BigDecimal increment) {
        getMiter().setNumericScoreIncrement(increment);
        return (self());
    }


    /**
     *  Sets the highest numeric score.
     *
     *  @param score a highest numeric score
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>score</code> is
     *          <code>null</code>
     */

    public T highestNumericScore(java.math.BigDecimal score) {
        getMiter().setHighestNumericScore(score);
        return (self());
    }


    /**
     *  Adds a GradeSystem record.
     *
     *  @param record a grade system record
     *  @param recordType the type of grade system record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.grading.records.GradeSystemRecord record, org.osid.type.Type recordType) {
        getMiter().addGradeSystemRecord(record, recordType);
        return (self());
    }
}       


