//
// AbstractEntryLookupSession.java
//
//    A starter implementation framework for providing an Entry lookup
//    service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.dictionary.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an Entry
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getEntries(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractEntryLookupSession
    extends AbstractEntryRetrievalSession
    implements org.osid.dictionary.EntryLookupSession {

    private boolean pedantic = false;
    

    /**
     *  A complete view of the <code>Entry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeEntryView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Entry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryEntryView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Gets the <code> Dictionary </code> entry associated with the given key 
     *  and types. The <code> keyType </code> indicates the key object type 
     *  and the <code> valueType </code> indicates the value object return. 
     *
     *  @param  key the key of the entry to retrieve 
     *  @param  keyType the key type of the entry to retrieve 
     *  @param  valueType the value type of the entry to retrieve 
     *  @return the returned <code> object </code> 
     *  @throws org.osid.InvalidArgumentException <code> key </code> is not of 
     *          <code> keyType </code> 
     *  @throws org.osid.NotFoundException no entry found 
     *  @throws org.osid.NullArgumentException <code> key, keyType </code> or 
     *          <code> valueType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public java.lang.Object retrieveEntry(java.lang.Object key, 
                                          org.osid.type.Type keyType, 
                                          org.osid.type.Type valueType)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.dictionary.entry.EntryFilterList(new ValueTypeFilter(valueType), getEntriesByKeyAndKeyType(key, keyType)));
    }        


    /**
     *  Gets the <code>Entry</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Entry</code> may have a different <code>Id</code> than
     *  requested, such as the case where a duplicate <code>Id</code>
     *  was assigned to a <code>Entry</code> and retained for
     *  compatibility.
     *
     *  @param entryId <code>Id</code> of the <code>Entry</code>
     *  @return the entry
     *  @throws org.osid.NotFoundException <code>entryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>entryId</code> is
     *          <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.Entry getEntry(org.osid.id.Id entryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.dictionary.EntryList entries = getEntries()) {
            while (entries.hasNext()) {
                org.osid.dictionary.Entry entry = entries.getNextEntry();
                if (entry.getId().equals(entryId)) {
                    return (entry);
                }
            }
        } 

        throw new org.osid.NotFoundException(entryId + " not found");
    }


    /**
     *  Gets an <code>EntryList</code> corresponding to the given
     *  <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the entries
     *  specified in the <code>Id</code> list, in the order of the
     *  list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Entries</code> may
     *  be omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getEntries()</code>.
     *
     *  @param  entryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Entry</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>entryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.EntryList getEntriesByIds(org.osid.id.IdList entryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.dictionary.Entry> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = entryIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getEntry(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("entry " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.dictionary.entry.LinkedEntryList(ret));
    }


    /**
     *  Gets an <code>EntryList</code> corresponding to the given
     *  entry genus <code>Type</code> which does not include
     *  entries of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  entries or an error results. Otherwise, the returned list
     *  may contain only those entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getEntries()</code>.
     *
     *  @param  entryGenusType an entry genus type 
     *  @return the returned <code>Entry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>entryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.EntryList getEntriesByGenusType(org.osid.type.Type entryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.dictionary.entry.EntryGenusFilterList(getEntries(), entryGenusType));
    }


    /**
     *  Gets an <code>EntryList</code> corresponding to the given
     *  entry genus <code>Type</code> and include any additional
     *  entries with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  entries or an error results. Otherwise, the returned list
     *  may contain only those entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getEntries()</code>.
     *
     *  @param  entryGenusType an entry genus type 
     *  @return the returned <code>Entry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>entryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.EntryList getEntriesByParentGenusType(org.osid.type.Type entryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getEntriesByGenusType(entryGenusType));
    }


    /**
     *  Gets an <code>EntryList</code> containing the given
     *  entry record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  entries or an error results. Otherwise, the returned list
     *  may contain only those entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getEntries()</code>.
     *
     *  @param  entryRecordType an entry record type 
     *  @return the returned <code>Entry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>entryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.EntryList getEntriesByRecordType(org.osid.type.Type entryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.dictionary.entry.EntryRecordFilterList(getEntries(), entryRecordType));
    }

    
    /**
     *  Gets all the <code> Dictionary </code> entries matching the
     *  given key <code> Type. </code> In plenary mode, the returned
     *  list contains all of the entries specified in the Id list, in
     *  the order of the list, including duplicates, or an error
     *  results if an Id in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible Entry elements may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  keyType the type of the key to match 
     *  @return the list of entries matching <code> keyType </code> 
     *  @throws org.osid.NullArgumentException <code> keyType </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.dictionary.EntryList getEntriesByKeyType(org.osid.type.Type keyType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.dictionary.entry.EntryFilterList(new KeyTypeFilter(keyType), getEntries()));
    }

    
    /**
     *  Gets all the <code> Dictionary </code> entries matching the
     *  given key and value <code> Type. </code> In plenary mode, the
     *  returned list contains all of the entries specified in the Id
     *  list, in the order of the list, including duplicates, or an
     *  error results if an Id in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible Entry elements may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  keyType the type of the key to match 
     *  @param  valueType the type of the value to match 
     *  @return the list of entries matching <code> keyType </code> 
     *  @throws org.osid.NullArgumentException <code> keyType </code> or 
     *          <code> valueType </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.dictionary.EntryList getEntriesByKeyTypeAndValueType(org.osid.type.Type keyType, 
                                                                         org.osid.type.Type valueType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.dictionary.entry.EntryFilterList(new ValueTypeFilter(valueType), getEntriesByKeyType(keyType)));
    }


    /**
     *  Gets all the <code> Dictionary </code> entries matching the
     *  given key and key <code> Type. I </code> n plenary mode, the
     *  returned list contains all of the entries specified in the Id
     *  list, in the order of the list, including duplicates, or an
     *  error results if an Id in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible Entry elements may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  key the key to match 
     *  @param  keyType the type of the value to match 
     *  @return the list of entries matching <code> keyType </code> 
     *  @throws org.osid.NullArgumentException <code> key </code> or <code> 
     *          keyType </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
    
    @OSID @Override
    public org.osid.dictionary.EntryList getEntriesByKeyAndKeyType(java.lang.Object key, 
                                                                   org.osid.type.Type keyType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.dictionary.entry.EntryFilterList(new KeyFilter(key), getEntriesByKeyType(keyType)));
    }        


    /**
     *  Gets all <code>Entries</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  entries or an error results. Otherwise, the returned list
     *  may contain only those entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Entries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.dictionary.EntryList getEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the entry list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of entries
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.dictionary.EntryList filterEntriesOnViews(org.osid.dictionary.EntryList list)
        throws org.osid.OperationFailedException {

        return (list);
    }


    public static class KeyTypeFilter
        implements net.okapia.osid.jamocha.inline.filter.dictionary.entry.EntryFilter {

        private final org.osid.type.Type keyType;

        
        /**
         *  Constructs a new <code>KeyTypeFilterList</code>.
         *
         *  @param keyType the key type to filter
         *  @throws org.osid.NullArgumentException
         *          <code>keyType</code> is <code>null</code>
         */

        public KeyTypeFilter(org.osid.type.Type keyType) {
            nullarg(keyType, "key type");
            this.keyType = keyType;
            return;
        }


        /**
         *  Used by the EntryFilterList to filter the entry list
         *  based on key type.
         *
         *  @param entry the entry
         *  @return <code>true</code> to pass the entry,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.dictionary.Entry entry) {
            return (entry.getKeyType().equals(this.keyType));
        }
    }    


    public static class KeyFilter
        implements net.okapia.osid.jamocha.inline.filter.dictionary.entry.EntryFilter {

        private final Object key;


        /**
         *  Constructs a new <code>KeyFilterList</code>.
         *
         *  @param key the key to filter
         *  @throws org.osid.NullArgumentException
         *          <code>key</code> is <code>null</code>
         */

        public KeyFilter(Object key) {
            nullarg(key, "key ");
            this.key = key;
            return;
        }


        /**
         *  Used by the EntryFilterList to filter the entry list
         *  based on key.
         *
         *  @param entry the entry
         *  @return <code>true</code> to pass the entry,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.dictionary.Entry entry) {
            return (entry.getKey().equals(this.key));
        }
    }    


    public static class ValueTypeFilter
        implements net.okapia.osid.jamocha.inline.filter.dictionary.entry.EntryFilter {

        private final org.osid.type.Type valueType;

        
        /**
         *  Constructs a new <code>ValueTypeFilterList</code>.
         *
         *  @param valueType the value type to filter
         *  @throws org.osid.NullArgumentException
         *          <code>valueType</code> is <code>null</code>
         */

        public ValueTypeFilter(org.osid.type.Type valueType) {
            nullarg(valueType, "value type");
            this.valueType = valueType;
            return;
        }


        /**
         *  Used by the EntryFilterList to filter the entry list
         *  based on value type.
         *
         *  @param entry the entry
         *  @return <code>true</code> to pass the entry,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.dictionary.Entry entry) {
            return (entry.getValueType().equals(this.valueType));
        }
    }    
}
