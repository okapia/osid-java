//
// InvariantMapAuctionLookupSession
//
//    Implements an Auction lookup service backed by a fixed collection of
//    auctions.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.bidding;


/**
 *  Implements an Auction lookup service backed by a fixed
 *  collection of auctions. The auctions are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapAuctionLookupSession
    extends net.okapia.osid.jamocha.core.bidding.spi.AbstractMapAuctionLookupSession
    implements org.osid.bidding.AuctionLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapAuctionLookupSession</code> with no
     *  auctions.
     *  
     *  @param auctionHouse the auction house
     *  @throws org.osid.NullArgumnetException {@code auctionHouse} is
     *          {@code null}
     */

    public InvariantMapAuctionLookupSession(org.osid.bidding.AuctionHouse auctionHouse) {
        setAuctionHouse(auctionHouse);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAuctionLookupSession</code> with a single
     *  auction.
     *  
     *  @param auctionHouse the auction house
     *  @param auction an single auction
     *  @throws org.osid.NullArgumentException {@code auctionHouse} or
     *          {@code auction} is <code>null</code>
     */

      public InvariantMapAuctionLookupSession(org.osid.bidding.AuctionHouse auctionHouse,
                                               org.osid.bidding.Auction auction) {
        this(auctionHouse);
        putAuction(auction);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAuctionLookupSession</code> using an array
     *  of auctions.
     *  
     *  @param auctionHouse the auction house
     *  @param auctions an array of auctions
     *  @throws org.osid.NullArgumentException {@code auctionHouse} or
     *          {@code auctions} is <code>null</code>
     */

      public InvariantMapAuctionLookupSession(org.osid.bidding.AuctionHouse auctionHouse,
                                               org.osid.bidding.Auction[] auctions) {
        this(auctionHouse);
        putAuctions(auctions);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAuctionLookupSession</code> using a
     *  collection of auctions.
     *
     *  @param auctionHouse the auction house
     *  @param auctions a collection of auctions
     *  @throws org.osid.NullArgumentException {@code auctionHouse} or
     *          {@code auctions} is <code>null</code>
     */

      public InvariantMapAuctionLookupSession(org.osid.bidding.AuctionHouse auctionHouse,
                                               java.util.Collection<? extends org.osid.bidding.Auction> auctions) {
        this(auctionHouse);
        putAuctions(auctions);
        return;
    }
}
