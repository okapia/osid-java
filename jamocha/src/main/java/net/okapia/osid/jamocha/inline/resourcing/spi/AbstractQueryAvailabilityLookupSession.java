//
// AbstractQueryAvailabilityLookupSession.java
//
//    An inline adapter that maps an AvailabilityLookupSession to
//    an AvailabilityQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.resourcing.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an AvailabilityLookupSession to
 *  an AvailabilityQuerySession.
 */

public abstract class AbstractQueryAvailabilityLookupSession
    extends net.okapia.osid.jamocha.resourcing.spi.AbstractAvailabilityLookupSession
    implements org.osid.resourcing.AvailabilityLookupSession {

    private boolean effectiveonly = false;
    private final org.osid.resourcing.AvailabilityQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryAvailabilityLookupSession.
     *
     *  @param querySession the underlying availability query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryAvailabilityLookupSession(org.osid.resourcing.AvailabilityQuerySession querySession) {
        nullarg(querySession, "availability query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Foundry</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Foundry Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFoundryId() {
        return (this.session.getFoundryId());
    }


    /**
     *  Gets the <code>Foundry</code> associated with this 
     *  session.
     *
     *  @return the <code>Foundry</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Foundry getFoundry()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getFoundry());
    }


    /**
     *  Tests if this user can perform <code>Availability</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAvailabilities() {
        return (this.session.canSearchAvailabilities());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include availabilities in foundries which are children
     *  of this foundry in the foundry hierarchy.
     */

    @OSID @Override
    public void useFederatedFoundryView() {
        this.session.useFederatedFoundryView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this foundry only.
     */

    @OSID @Override
    public void useIsolatedFoundryView() {
        this.session.useIsolatedFoundryView();
        return;
    }
    

    /**
     *  Only availabilities whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveAvailabilityView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All availabilities of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveAvailabilityView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Availability</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Availability</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Availability</code> and
     *  retained for compatibility.
     *
     *  In effective mode, availabilities are returned that are currently
     *  effective.  In any effective mode, effective availabilities and
     *  those currently expired are returned.
     *
     *  @param  availabilityId <code>Id</code> of the
     *          <code>Availability</code>
     *  @return the availability
     *  @throws org.osid.NotFoundException <code>availabilityId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>availabilityId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Availability getAvailability(org.osid.id.Id availabilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.AvailabilityQuery query = getQuery();
        query.matchId(availabilityId, true);
        org.osid.resourcing.AvailabilityList availabilities = this.session.getAvailabilitiesByQuery(query);
        if (availabilities.hasNext()) {
            return (availabilities.getNextAvailability());
        } 
        
        throw new org.osid.NotFoundException(availabilityId + " not found");
    }


    /**
     *  Gets an <code>AvailabilityList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  availabilities specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Availabilities</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, availabilities are returned that are currently effective.
     *  In any effective mode, effective availabilities and those currently expired
     *  are returned.
     *
     *  @param  availabilityIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Availability</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>availabilityIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilitiesByIds(org.osid.id.IdList availabilityIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.AvailabilityQuery query = getQuery();

        try (org.osid.id.IdList ids = availabilityIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getAvailabilitiesByQuery(query));
    }


    /**
     *  Gets an <code>AvailabilityList</code> corresponding to the given
     *  availability genus <code>Type</code> which does not include
     *  availabilities of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned list
     *  may contain only those availabilities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, availabilities are returned that are currently effective.
     *  In any effective mode, effective availabilities and those currently expired
     *  are returned.
     *
     *  @param  availabilityGenusType an availability genus type 
     *  @return the returned <code>Availability</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>availabilityGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilitiesByGenusType(org.osid.type.Type availabilityGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.AvailabilityQuery query = getQuery();
        query.matchGenusType(availabilityGenusType, true);
        return (this.session.getAvailabilitiesByQuery(query));
    }


    /**
     *  Gets an <code>AvailabilityList</code> corresponding to the given
     *  availability genus <code>Type</code> and include any additional
     *  availabilities with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned list
     *  may contain only those availabilities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, availabilities are returned that are currently
     *  effective.  In any effective mode, effective availabilities and
     *  those currently expired are returned.
     *
     *  @param  availabilityGenusType an availability genus type 
     *  @return the returned <code>Availability</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>availabilityGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilitiesByParentGenusType(org.osid.type.Type availabilityGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.AvailabilityQuery query = getQuery();
        query.matchParentGenusType(availabilityGenusType, true);
        return (this.session.getAvailabilitiesByQuery(query));
    }


    /**
     *  Gets an <code>AvailabilityList</code> containing the given
     *  availability record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned list
     *  may contain only those availabilities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, availabilities are returned that are currently
     *  effective.  In any effective mode, effective availabilities and
     *  those currently expired are returned.
     *
     *  @param  availabilityRecordType an availability record type 
     *  @return the returned <code>Availability</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>availabilityRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilitiesByRecordType(org.osid.type.Type availabilityRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.AvailabilityQuery query = getQuery();
        query.matchRecordType(availabilityRecordType, true);
        return (this.session.getAvailabilitiesByQuery(query));
    }


    /**
     *  Gets an <code>AvailabilityList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned list
     *  may contain only those availabilities that are accessible
     *  through this session.
     *  
     *  In effective mode, availabilities are returned that are currently
     *  effective.  In any effective mode, effective availabilities and
     *  those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Availability</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilitiesOnDate(org.osid.calendaring.DateTime from, 
                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.AvailabilityQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getAvailabilitiesByQuery(query));
    }
        

    /**
     *  Gets a list of availabilities corresponding to a resource
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned list
     *  may contain only those availabilities that are accessible
     *  through this session.
     *
     *  In effective mode, availabilities are returned that are
     *  currently effective.  In any effective mode, effective
     *  availabilities and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>AvailabilityList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.resourcing.AvailabilityList getAvailabilitiesForResource(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.resourcing.AvailabilityQuery query = getQuery();
        query.matchResourceId(resourceId, true);
        return (this.session.getAvailabilitiesByQuery(query));
    }


    /**
     *  Gets a list of availabilities corresponding to a resource
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned list
     *  may contain only those availabilities that are accessible
     *  through this session.
     *
     *  In effective mode, availabilities are returned that are
     *  currently effective.  In any effective mode, effective
     *  availabilities and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>AvailabilityList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilitiesForResourceOnDate(org.osid.id.Id resourceId,
                                                                                   org.osid.calendaring.DateTime from,
                                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.AvailabilityQuery query = getQuery();
        query.matchResourceId(resourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getAvailabilitiesByQuery(query));
    }


    /**
     *  Gets a list of availabilities corresponding to a job
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned list
     *  may contain only those availabilities that are accessible
     *  through this session.
     *
     *  In effective mode, availabilities are returned that are
     *  currently effective.  In any effective mode, effective
     *  availabilities and those currently expired are returned.
     *
     *  @param  jobId the <code>Id</code> of the job
     *  @return the returned <code>AvailabilityList</code>
     *  @throws org.osid.NullArgumentException <code>jobId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.resourcing.AvailabilityList getAvailabilitiesForJob(org.osid.id.Id jobId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.resourcing.AvailabilityQuery query = getQuery();
        query.matchJobId(jobId, true);
        return (this.session.getAvailabilitiesByQuery(query));
    }


    /**
     *  Gets a list of availabilities corresponding to a job
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned
     *  list may contain only those availabilities that are accessible
     *  through this session.
     *
     *  In effective mode, availabilities are returned that are
     *  currently effective.  In any effective mode, effective
     *  availabilities and those currently expired are returned.
     *
     *  @param  jobId the <code>Id</code> of the job
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>AvailabilityList</code>
     *  @throws org.osid.NullArgumentException <code>jobId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilitiesForJobOnDate(org.osid.id.Id jobId,
                                                                              org.osid.calendaring.DateTime from,
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.AvailabilityQuery query = getQuery();
        query.matchJobId(jobId, true);
        query.matchDate(from, to, true);
        return (this.session.getAvailabilitiesByQuery(query));
    }


    /**
     *  Gets a list of availabilities corresponding to resource and
     *  job <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned
     *  list may contain only those availabilities that are accessible
     *  through this session.
     *
     *  In effective mode, availabilities are returned that are
     *  currently effective.  In any effective mode, effective
     *  availabilities and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  jobId the <code>Id</code> of the job
     *  @return the returned <code>AvailabilityList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>jobId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilitiesForResourceAndJob(org.osid.id.Id resourceId,
                                                                                   org.osid.id.Id jobId)
        throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.resourcing.AvailabilityQuery query = getQuery();
        query.matchResourceId(resourceId, true);
        query.matchJobId(jobId, true);
        return (this.session.getAvailabilitiesByQuery(query));
    }


    /**
     *  Gets a list of availabilities corresponding to resource and
     *  job <code>Ids</code> and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned
     *  list may contain only those availabilities that are accessible
     *  through this session.
     *
     *  In effective mode, availabilities are returned that are
     *  currently effective.  In any effective mode, effective
     *  availabilities and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  jobId the <code>Id</code> of the job
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>AvailabilityList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>, <code>jobId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilitiesForResourceAndJobOnDate(org.osid.id.Id resourceId,
                                                                                         org.osid.id.Id jobId,
                                                                                         org.osid.calendaring.DateTime from,
                                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.resourcing.AvailabilityQuery query = getQuery();
        query.matchResourceId(resourceId, true);
        query.matchJobId(jobId, true);
        query.matchDate(from, to, true);
        return (this.session.getAvailabilitiesByQuery(query));
    }

    
    /**
     *  Gets all <code>Availabilities</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned list
     *  may contain only those availabilities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, availabilities are returned that are currently
     *  effective.  In any effective mode, effective availabilities and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Availabilities</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilities()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.AvailabilityQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getAvailabilitiesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.resourcing.AvailabilityQuery getQuery() {
        org.osid.resourcing.AvailabilityQuery query = this.session.getAvailabilityQuery();
        
        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }

        return (query);
    }
}
