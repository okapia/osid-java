//
// AbstractSignal.java
//
//     Defines a Signal.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.path.signal.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Signal</code>.
 */

public abstract class AbstractSignal
    extends net.okapia.osid.jamocha.spi.AbstractOsidRule
    implements org.osid.mapping.path.Signal {

    private org.osid.mapping.path.Path path;
    private org.osid.mapping.Coordinate coordinate;
    private final java.util.Collection<org.osid.process.State> states = new java.util.LinkedHashSet<>();

    private final java.util.Collection<org.osid.mapping.path.records.SignalRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the path <code> Id. </code> 
     *
     *  @return the <code> Id </code> of the path 
     */

    @OSID @Override
    public org.osid.id.Id getPathId() {
        return (this.path.getId());
    }


    /**
     *  Gets the path. 
     *
     *  @return the path 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.mapping.path.Path getPath()
        throws org.osid.OperationFailedException {

        return (this.path);
    }


    /**
     *  Sets the path.
     *
     *  @param path a path
     *  @throws org.osid.NullArgumentException
     *          <code>path</code> is <code>null</code>
     */

    protected void setPath(org.osid.mapping.path.Path path) {
        nullarg(path, "path");
        this.path = path;
        return;
    }


    /**
     *  Gets the coordinate of the signal on the path. 
     *
     *  @return the coordinate 
     */

    @OSID @Override
    public org.osid.mapping.Coordinate getCoordinate() {
        return (this.coordinate);
    }


    /**
     *  Sets the coordinate.
     *
     *  @param coordinate a coordinate
     *  @throws org.osid.NullArgumentException
     *          <code>coordinate</code> is <code>null</code>
     */

    protected void setCoordinate(org.osid.mapping.Coordinate coordinate) {
        nullarg(coordinate, "coordinate");
        this.coordinate = coordinate;
        return;
    }


    /**
     *  Gets the valid state <code> Ids </code> of this signal. 
     *
     *  @return the <code> Ids </code> of the states 
     */

    @OSID @Override
    public org.osid.id.IdList getStateIds() {
        try {
            org.osid.process.StateList states = getStates();
            return (new net.okapia.osid.jamocha.adapter.converter.process.state.StateToIdList(states));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the states of this signal. 
     *
     *  @return the states 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.process.StateList getStates()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.process.state.ArrayStateList(this.states));
    }


    /**
     *  Adds a state.
     *
     *  @param state a state
     *  @throws org.osid.NullArgumentException
     *          <code>state</code> is <code>null</code>
     */

    protected void addState(org.osid.process.State state) {
        nullarg(state, "state");
        this.states.add(state);
        return;
    }


    /**
     *  Sets all the states.
     *
     *  @param states a collection of states
     *  @throws org.osid.NullArgumentException
     *          <code>states</code> is <code>null</code>
     */

    protected void setStates(java.util.Collection<org.osid.process.State> states) {
        nullarg(states, "states");
        this.states.clear();
        this.states.addAll(states);
        return;
    }


    /**
     *  Tests if this signal supports the given record
     *  <code>Type</code>.
     *
     *  @param  signalRecordType a signal record type 
     *  @return <code>true</code> if the signalRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>signalRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type signalRecordType) {
        for (org.osid.mapping.path.records.SignalRecord record : this.records) {
            if (record.implementsRecordType(signalRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>Signal</code>
     *  record <code>Type</code>.
     *
     *  @param  signalRecordType the signal record type 
     *  @return the signal record 
     *  @throws org.osid.NullArgumentException
     *          <code>signalRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(signalRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.records.SignalRecord getSignalRecord(org.osid.type.Type signalRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.path.records.SignalRecord record : this.records) {
            if (record.implementsRecordType(signalRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(signalRecordType + " is not supported");
    }


    /**
     *  Adds a record to this signal. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param signalRecord the signal record
     *  @param signalRecordType signal record type
     *  @throws org.osid.NullArgumentException
     *          <code>signalRecord</code> or
     *          <code>signalRecordTypesignal</code> is
     *          <code>null</code>
     */
            
    protected void addSignalRecord(org.osid.mapping.path.records.SignalRecord signalRecord, 
                                   org.osid.type.Type signalRecordType) {

        nullarg(signalRecord, "signal record");
        addRecordType(signalRecordType);
        this.records.add(signalRecord);
        
        return;
    }
}
