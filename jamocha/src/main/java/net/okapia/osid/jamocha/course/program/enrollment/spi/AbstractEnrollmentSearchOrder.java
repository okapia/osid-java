//
// AbstractEnrollmentSearchOdrer.java
//
//     Defines an EnrollmentSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.program.enrollment.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an {@code EnrollmentSearchOrder}.
 */

public abstract class AbstractEnrollmentSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipSearchOrder
    implements org.osid.course.program.EnrollmentSearchOrder {

    private final java.util.Collection<org.osid.course.program.records.EnrollmentSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by program 
     *  offering. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByProgramOffering(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a program offering search order is available. 
     *
     *  @return <code> true </code> if a program offering order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramOfferingSearchOrder() {
        return (false);
    }


    /**
     *  Gets the program offering search order. 
     *
     *  @return the program offering search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingSearchOrder getProgramOfferingSearchOrder() {
        throw new org.osid.UnimplementedException("supportsProgramOfferingSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by student. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStudent(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a student search order is available. 
     *
     *  @return <code> true </code> if a student order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStudentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the student search order. 
     *
     *  @return the student search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStudentSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getStudentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsStudentSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  enrollmentRecordType an enrollment record type 
     *  @return {@code true} if the enrollmentRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code enrollmentRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type enrollmentRecordType) {
        for (org.osid.course.program.records.EnrollmentSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(enrollmentRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  enrollmentRecordType the enrollment record type 
     *  @return the enrollment search order record
     *  @throws org.osid.NullArgumentException
     *          {@code enrollmentRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(enrollmentRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.course.program.records.EnrollmentSearchOrderRecord getEnrollmentSearchOrderRecord(org.osid.type.Type enrollmentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.program.records.EnrollmentSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(enrollmentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(enrollmentRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this enrollment. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param enrollmentRecord the enrollment search odrer record
     *  @param enrollmentRecordType enrollment record type
     *  @throws org.osid.NullArgumentException
     *          {@code enrollmentRecord} or
     *          {@code enrollmentRecordTypeenrollment} is
     *          {@code null}
     */
            
    protected void addEnrollmentRecord(org.osid.course.program.records.EnrollmentSearchOrderRecord enrollmentSearchOrderRecord, 
                                     org.osid.type.Type enrollmentRecordType) {

        addRecordType(enrollmentRecordType);
        this.records.add(enrollmentSearchOrderRecord);
        
        return;
    }
}
