//
// MutableMapSpeedZoneLookupSession
//
//    Implements a SpeedZone lookup service backed by a collection of
//    speedZones that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.path;


/**
 *  Implements a SpeedZone lookup service backed by a collection of
 *  speed zones. The speed zones are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of speed zones can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapSpeedZoneLookupSession
    extends net.okapia.osid.jamocha.core.mapping.path.spi.AbstractMapSpeedZoneLookupSession
    implements org.osid.mapping.path.SpeedZoneLookupSession {


    /**
     *  Constructs a new {@code MutableMapSpeedZoneLookupSession}
     *  with no speed zones.
     *
     *  @param map the map
     *  @throws org.osid.NullArgumentException {@code map} is
     *          {@code null}
     */

      public MutableMapSpeedZoneLookupSession(org.osid.mapping.Map map) {
        setMap(map);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapSpeedZoneLookupSession} with a
     *  single speedZone.
     *
     *  @param map the map  
     *  @param speedZone a speed zone
     *  @throws org.osid.NullArgumentException {@code map} or
     *          {@code speedZone} is {@code null}
     */

    public MutableMapSpeedZoneLookupSession(org.osid.mapping.Map map,
                                           org.osid.mapping.path.SpeedZone speedZone) {
        this(map);
        putSpeedZone(speedZone);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapSpeedZoneLookupSession}
     *  using an array of speed zones.
     *
     *  @param map the map
     *  @param speedZones an array of speed zones
     *  @throws org.osid.NullArgumentException {@code map} or
     *          {@code speedZones} is {@code null}
     */

    public MutableMapSpeedZoneLookupSession(org.osid.mapping.Map map,
                                           org.osid.mapping.path.SpeedZone[] speedZones) {
        this(map);
        putSpeedZones(speedZones);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapSpeedZoneLookupSession}
     *  using a collection of speed zones.
     *
     *  @param map the map
     *  @param speedZones a collection of speed zones
     *  @throws org.osid.NullArgumentException {@code map} or
     *          {@code speedZones} is {@code null}
     */

    public MutableMapSpeedZoneLookupSession(org.osid.mapping.Map map,
                                           java.util.Collection<? extends org.osid.mapping.path.SpeedZone> speedZones) {

        this(map);
        putSpeedZones(speedZones);
        return;
    }

    
    /**
     *  Makes a {@code SpeedZone} available in this session.
     *
     *  @param speedZone a speed zone
     *  @throws org.osid.NullArgumentException {@code speedZone{@code  is
     *          {@code null}
     */

    @Override
    public void putSpeedZone(org.osid.mapping.path.SpeedZone speedZone) {
        super.putSpeedZone(speedZone);
        return;
    }


    /**
     *  Makes an array of speed zones available in this session.
     *
     *  @param speedZones an array of speed zones
     *  @throws org.osid.NullArgumentException {@code speedZones{@code 
     *          is {@code null}
     */

    @Override
    public void putSpeedZones(org.osid.mapping.path.SpeedZone[] speedZones) {
        super.putSpeedZones(speedZones);
        return;
    }


    /**
     *  Makes collection of speed zones available in this session.
     *
     *  @param speedZones a collection of speed zones
     *  @throws org.osid.NullArgumentException {@code speedZones{@code  is
     *          {@code null}
     */

    @Override
    public void putSpeedZones(java.util.Collection<? extends org.osid.mapping.path.SpeedZone> speedZones) {
        super.putSpeedZones(speedZones);
        return;
    }


    /**
     *  Removes a SpeedZone from this session.
     *
     *  @param speedZoneId the {@code Id} of the speed zone
     *  @throws org.osid.NullArgumentException {@code speedZoneId{@code 
     *          is {@code null}
     */

    @Override
    public void removeSpeedZone(org.osid.id.Id speedZoneId) {
        super.removeSpeedZone(speedZoneId);
        return;
    }    
}
