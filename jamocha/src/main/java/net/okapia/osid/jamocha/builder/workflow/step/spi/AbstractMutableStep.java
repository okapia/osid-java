//
// AbstractMutableStep.java
//
//     Defines a mutable Step.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.workflow.step.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Step</code>.
 */

public abstract class AbstractMutableStep
    extends net.okapia.osid.jamocha.workflow.step.spi.AbstractStep
    implements org.osid.workflow.Step,
               net.okapia.osid.jamocha.builder.workflow.step.StepMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this step. 
     *
     *  @param record step record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addStepRecord(org.osid.workflow.records.StepRecord record, org.osid.type.Type recordType) {
        super.addStepRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the provider for this step.
     *
     *  @param provider the new provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    @Override
    public void setProvider(net.okapia.osid.provider.Provider provider) {
        super.setProvider(provider);
        return;
    }


    /**
     *  Sets the provider of this step
     *
     *  @param provider the new provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    @Override
    public void setProvider(org.osid.resource.Resource provider) {
        super.setProvider(provider);
        return;
    }


    /**
     *  Adds an asset for the provider branding.
     *
     *  @param asset an asset to add
     *  @throws org.osid.NullArgumentException <code>asset</code>
     *          is <code>null</code>
     */

    @Override
    public void addAssetToBranding(org.osid.repository.Asset asset) {
        super.addAssetToBranding(asset);
        return;
    }


    /**
     *  Adds assets for the provider branding.
     *
     *  @param assets an array of assets to add
     *  @throws org.osid.NullArgumentException <code>assets</code>
     *          is <code>null</code>
     */

    @Override
    public void setBranding(java.util.Collection<org.osid.repository.Asset> assets) {
        super.setBranding(assets);
        return;
    }


    /**
     *  Sets the license.
     *
     *  @param license the license
     *  @throws org.osid.NullArgumentException <code>license</code>
     *          is <code>null</code>
     */

    @Override
    public void setLicense(org.osid.locale.DisplayText license) {
        super.setLicense(license);
        return;
    }


    /**
     *  Enables this step. Enabling an operable overrides any
     *  enabling rule that may exist.
     *  
     *  @param enabled <code>true</code> if enabled, <code>false<code>
     *         otherwise
     */
    
    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        return;
    }


    /**
     *  Disables this step. Disabling an operable overrides any
     *  enabling rule that may exist.
     *
     *  @param disabled <code> true </code> if this object is
     *         disabled, <code> false </code> otherwise
     */
    
    @Override
    public void setDisabled(boolean disabled) {
        super.setDisabled(disabled);
        return;
    }


    /**
     *  Sets the operational flag.
     *
     *  @param operational <code>true</code>if operational,
     *         <code>false</code> if not operational
     */
    
    @Override
    public void setOperational(boolean operational) {
        super.setOperational(operational);
        return;
    }


    /**
     *  Sets the display name for this step.
     *
     *  @param displayName the name for this step
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this step.
     *
     *  @param description the description of this step
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException <code>genusType</code>
     *          is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the process.
     *
     *  @param process a process
     *  @throws org.osid.NullArgumentException <code>process</code> is
     *          <code>null</code>
     */

    @Override
    public void setProcess(org.osid.workflow.Process process) {
        super.setProcess(process);
        return;
    }


    /**
     *  Adds a resource.
     *
     *  @param resource a resource
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    @Override
    public void addResource(org.osid.resource.Resource resource) {
        super.addResource(resource);
        return;
    }


    /**
     *  Sets all the resources.
     *
     *  @param resources a collection of resources
     *  @throws org.osid.NullArgumentException <code>resources</code>
     *          is <code>null</code>
     */

    @Override
    public void setResources(java.util.Collection<org.osid.resource.Resource> resources) {
        super.setResources(resources);
        return;
    }


    /**
     *  Adds an input state.
     *
     *  @param state an input state
     *  @throws org.osid.NullArgumentException <code>state</code> is
     *          <code>null</code>
     */

    @Override
    public void addInputState(org.osid.process.State state) {
        super.addInputState(state);
        return;
    }


    /**
     *  Sets all the input states.
     *
     *  @param states a collection of input states
     *  @throws org.osid.NullArgumentException <code>states</code> is
     *          <code>null</code>
     */

    public void setInputStates(java.util.Collection<org.osid.process.State> states) {
        super.setInputStates(states);
        return;
    }


    /**
     *  Sets the next state.
     *
     *  @param state a next state
     *  @throws org.osid.NullArgumentException <code>state</code> is
     *          <code>null</code>
     */

    public void setNextState(org.osid.process.State state) {
        super.setNextState(state);
        return;
    }
}

