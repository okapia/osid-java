//
// MutableMapProxyLeaseLookupSession
//
//    Implements a Lease lookup service backed by a collection of
//    leases that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.room.squatting;


/**
 *  Implements a Lease lookup service backed by a collection of
 *  leases. The leases are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of leases can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyLeaseLookupSession
    extends net.okapia.osid.jamocha.core.room.squatting.spi.AbstractMapLeaseLookupSession
    implements org.osid.room.squatting.LeaseLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyLeaseLookupSession}
     *  with no leases.
     *
     *  @param campus the campus
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code campus} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyLeaseLookupSession(org.osid.room.Campus campus,
                                                  org.osid.proxy.Proxy proxy) {
        setCampus(campus);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyLeaseLookupSession} with a
     *  single lease.
     *
     *  @param campus the campus
     *  @param lease a lease
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code campus},
     *          {@code lease}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyLeaseLookupSession(org.osid.room.Campus campus,
                                                org.osid.room.squatting.Lease lease, org.osid.proxy.Proxy proxy) {
        this(campus, proxy);
        putLease(lease);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyLeaseLookupSession} using an
     *  array of leases.
     *
     *  @param campus the campus
     *  @param leases an array of leases
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code campus},
     *          {@code leases}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyLeaseLookupSession(org.osid.room.Campus campus,
                                                org.osid.room.squatting.Lease[] leases, org.osid.proxy.Proxy proxy) {
        this(campus, proxy);
        putLeases(leases);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyLeaseLookupSession} using a
     *  collection of leases.
     *
     *  @param campus the campus
     *  @param leases a collection of leases
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code campus},
     *          {@code leases}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyLeaseLookupSession(org.osid.room.Campus campus,
                                                java.util.Collection<? extends org.osid.room.squatting.Lease> leases,
                                                org.osid.proxy.Proxy proxy) {
   
        this(campus, proxy);
        setSessionProxy(proxy);
        putLeases(leases);
        return;
    }

    
    /**
     *  Makes a {@code Lease} available in this session.
     *
     *  @param lease an lease
     *  @throws org.osid.NullArgumentException {@code lease{@code 
     *          is {@code null}
     */

    @Override
    public void putLease(org.osid.room.squatting.Lease lease) {
        super.putLease(lease);
        return;
    }


    /**
     *  Makes an array of leases available in this session.
     *
     *  @param leases an array of leases
     *  @throws org.osid.NullArgumentException {@code leases{@code 
     *          is {@code null}
     */

    @Override
    public void putLeases(org.osid.room.squatting.Lease[] leases) {
        super.putLeases(leases);
        return;
    }


    /**
     *  Makes collection of leases available in this session.
     *
     *  @param leases
     *  @throws org.osid.NullArgumentException {@code lease{@code 
     *          is {@code null}
     */

    @Override
    public void putLeases(java.util.Collection<? extends org.osid.room.squatting.Lease> leases) {
        super.putLeases(leases);
        return;
    }


    /**
     *  Removes a Lease from this session.
     *
     *  @param leaseId the {@code Id} of the lease
     *  @throws org.osid.NullArgumentException {@code leaseId{@code  is
     *          {@code null}
     */

    @Override
    public void removeLease(org.osid.id.Id leaseId) {
        super.removeLease(leaseId);
        return;
    }    
}
