//
// AbstractGradebookColumnQuery.java
//
//     A template for making a GradebookColumn Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.gradebookcolumn.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for gradebook columns.
 */

public abstract class AbstractGradebookColumnQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.grading.GradebookColumnQuery {

    private final java.util.Collection<org.osid.grading.records.GradebookColumnQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the grade system <code> Id </code> for this query. 
     *
     *  @param  gradeSystemId a grade system <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeSystemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGradeSystemId(org.osid.id.Id gradeSystemId, boolean match) {
        return;
    }


    /**
     *  Clears the grade system <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradeSystemIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradeSystemQuery </code> is available for querying 
     *  grade systems. 
     *
     *  @return <code> true </code> if a grade system query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSystemQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade system. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the grade system query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQuery getGradeSystemQuery() {
        throw new org.osid.UnimplementedException("supportsGradeSystemQuery() is false");
    }


    /**
     *  Matches gradebook columns with any grade system assigned. 
     *
     *  @param  match <code> true </code> to match columns with any grade 
     *          system, <code> false </code> to match columns with no grade 
     *          system 
     */

    @OSID @Override
    public void matchAnyGradeSystem(boolean match) {
        return;
    }


    /**
     *  Clears the grade system terms. 
     */

    @OSID @Override
    public void clearGradeSystemTerms() {
        return;
    }


    /**
     *  Sets the grade entry <code> Id </code> for this query. 
     *
     *  @param  gradeEntryId a grade entry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeEntryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGradeEntryId(org.osid.id.Id gradeEntryId, boolean match) {
        return;
    }


    /**
     *  Clears the grade entry <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradeEntryIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradeEntryQuery </code> is available for querying 
     *  grade entries. 
     *
     *  @return <code> true </code> if a grade entry query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade entry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the grade entry query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryQuery getGradeEntryQuery() {
        throw new org.osid.UnimplementedException("supportsGradeEntryQuery() is false");
    }


    /**
     *  Matches gradebook columns with any grade entry assigned. 
     *
     *  @param  match <code> true </code> to match columns with any grade 
     *          entry, <code> false </code> to match columns with no grade 
     *          entries 
     */

    @OSID @Override
    public void matchAnyGradeEntry(boolean match) {
        return;
    }


    /**
     *  Clears the grade entry terms. 
     */

    @OSID @Override
    public void clearGradeEntryTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradebookColumnSummaryQuery </code> is available for 
     *  querying grade systems. 
     *
     *  @return <code> true </code> if a gradebook column summary query 
     *          interface is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookColumnSummaryQuery() {
        return (false);
    }


    /**
     *  Gets the query interface for a gradebook column summary. Multiple 
     *  retrievals produce a nested <code> OR </code> term. 
     *
     *  @return the gradebook column summary query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnSummaryQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnSummaryQuery getGradebookColumnSummaryQuery() {
        throw new org.osid.UnimplementedException("supportsGradebookColumnSummaryQuery() is false");
    }


    /**
     *  Clears the gradebook column summary terms. 
     */

    @OSID @Override
    public void clearGradebookColumnSummaryTerms() {
        return;
    }


    /**
     *  Sets the gradebook <code> Id </code> for this query. 
     *
     *  @param  gradebookId a gradebook <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGradebookId(org.osid.id.Id gradebookId, boolean match) {
        return;
    }


    /**
     *  Clears the gradebook <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradebookIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradebookQuery </code> is available for querying 
     *  grade systems. 
     *
     *  @return <code> true </code> if a gradebook query interface is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookQuery() {
        return (false);
    }


    /**
     *  Gets the query interface for a gradebook. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the gradebook query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookQuery getGradebookQuery() {
        throw new org.osid.UnimplementedException("supportsGradebookQuery() is false");
    }


    /**
     *  Clears the gradebook terms. 
     */

    @OSID @Override
    public void clearGradebookTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given gradebook column query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a gradebook column implementing the requested record.
     *
     *  @param gradebookColumnRecordType a gradebook column record type
     *  @return the gradebook column query record
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumnRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(gradebookColumnRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.records.GradebookColumnQueryRecord getGradebookColumnQueryRecord(org.osid.type.Type gradebookColumnRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.grading.records.GradebookColumnQueryRecord record : this.records) {
            if (record.implementsRecordType(gradebookColumnRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradebookColumnRecordType + " is not supported");
    }


    /**
     *  Adds a record to this gradebook column query. 
     *
     *  @param gradebookColumnQueryRecord gradebook column query record
     *  @param gradebookColumnRecordType gradebookColumn record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addGradebookColumnQueryRecord(org.osid.grading.records.GradebookColumnQueryRecord gradebookColumnQueryRecord, 
                                          org.osid.type.Type gradebookColumnRecordType) {

        addRecordType(gradebookColumnRecordType);
        nullarg(gradebookColumnQueryRecord, "gradebook column query record");
        this.records.add(gradebookColumnQueryRecord);        
        return;
    }
}
