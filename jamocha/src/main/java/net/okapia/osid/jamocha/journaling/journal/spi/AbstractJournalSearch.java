//
// AbstractJournalSearch.java
//
//     A template for making a Journal Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.journaling.journal.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing journal searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractJournalSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.journaling.JournalSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.journaling.records.JournalSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.journaling.JournalSearchOrder journalSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of journals. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  journalIds list of journals
     *  @throws org.osid.NullArgumentException
     *          <code>journalIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongJournals(org.osid.id.IdList journalIds) {
        while (journalIds.hasNext()) {
            try {
                this.ids.add(journalIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongJournals</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of journal Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getJournalIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  journalSearchOrder journal search order 
     *  @throws org.osid.NullArgumentException
     *          <code>journalSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>journalSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderJournalResults(org.osid.journaling.JournalSearchOrder journalSearchOrder) {
	this.journalSearchOrder = journalSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.journaling.JournalSearchOrder getJournalSearchOrder() {
	return (this.journalSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given journal search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a journal implementing the requested record.
     *
     *  @param journalSearchRecordType a journal search record
     *         type
     *  @return the journal search record
     *  @throws org.osid.NullArgumentException
     *          <code>journalSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(journalSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.journaling.records.JournalSearchRecord getJournalSearchRecord(org.osid.type.Type journalSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.journaling.records.JournalSearchRecord record : this.records) {
            if (record.implementsRecordType(journalSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(journalSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this journal search. 
     *
     *  @param journalSearchRecord journal search record
     *  @param journalSearchRecordType journal search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addJournalSearchRecord(org.osid.journaling.records.JournalSearchRecord journalSearchRecord, 
                                           org.osid.type.Type journalSearchRecordType) {

        addRecordType(journalSearchRecordType);
        this.records.add(journalSearchRecord);        
        return;
    }
}
