//
// MutableMapProxyProgramOfferingLookupSession
//
//    Implements a ProgramOffering lookup service backed by a collection of
//    programOfferings that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.program;


/**
 *  Implements a ProgramOffering lookup service backed by a collection of
 *  programOfferings. The programOfferings are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of program offerings can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyProgramOfferingLookupSession
    extends net.okapia.osid.jamocha.core.course.program.spi.AbstractMapProgramOfferingLookupSession
    implements org.osid.course.program.ProgramOfferingLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyProgramOfferingLookupSession}
     *  with no program offerings.
     *
     *  @param courseCatalog the course catalog
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyProgramOfferingLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.proxy.Proxy proxy) {
        setCourseCatalog(courseCatalog);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyProgramOfferingLookupSession} with a
     *  single program offering.
     *
     *  @param courseCatalog the course catalog
     *  @param programOffering a program offering
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code programOffering}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyProgramOfferingLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                org.osid.course.program.ProgramOffering programOffering, org.osid.proxy.Proxy proxy) {
        this(courseCatalog, proxy);
        putProgramOffering(programOffering);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyProgramOfferingLookupSession} using an
     *  array of program offerings.
     *
     *  @param courseCatalog the course catalog
     *  @param programOfferings an array of program offerings
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code programOfferings}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyProgramOfferingLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                org.osid.course.program.ProgramOffering[] programOfferings, org.osid.proxy.Proxy proxy) {
        this(courseCatalog, proxy);
        putProgramOfferings(programOfferings);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyProgramOfferingLookupSession} using a
     *  collection of program offerings.
     *
     *  @param courseCatalog the course catalog
     *  @param programOfferings a collection of program offerings
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code programOfferings}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyProgramOfferingLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                java.util.Collection<? extends org.osid.course.program.ProgramOffering> programOfferings,
                                                org.osid.proxy.Proxy proxy) {
   
        this(courseCatalog, proxy);
        setSessionProxy(proxy);
        putProgramOfferings(programOfferings);
        return;
    }

    
    /**
     *  Makes a {@code ProgramOffering} available in this session.
     *
     *  @param programOffering an program offering
     *  @throws org.osid.NullArgumentException {@code programOffering{@code 
     *          is {@code null}
     */

    @Override
    public void putProgramOffering(org.osid.course.program.ProgramOffering programOffering) {
        super.putProgramOffering(programOffering);
        return;
    }


    /**
     *  Makes an array of programOfferings available in this session.
     *
     *  @param programOfferings an array of program offerings
     *  @throws org.osid.NullArgumentException {@code programOfferings{@code 
     *          is {@code null}
     */

    @Override
    public void putProgramOfferings(org.osid.course.program.ProgramOffering[] programOfferings) {
        super.putProgramOfferings(programOfferings);
        return;
    }


    /**
     *  Makes collection of program offerings available in this session.
     *
     *  @param programOfferings
     *  @throws org.osid.NullArgumentException {@code programOffering{@code 
     *          is {@code null}
     */

    @Override
    public void putProgramOfferings(java.util.Collection<? extends org.osid.course.program.ProgramOffering> programOfferings) {
        super.putProgramOfferings(programOfferings);
        return;
    }


    /**
     *  Removes a ProgramOffering from this session.
     *
     *  @param programOfferingId the {@code Id} of the program offering
     *  @throws org.osid.NullArgumentException {@code programOfferingId{@code  is
     *          {@code null}
     */

    @Override
    public void removeProgramOffering(org.osid.id.Id programOfferingId) {
        super.removeProgramOffering(programOfferingId);
        return;
    }    
}
