//
// AbstractMapRenovationLookupSession
//
//    A simple framework for providing a Renovation lookup service
//    backed by a fixed collection of renovations.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.room.construction.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Renovation lookup service backed by a
 *  fixed collection of renovations. The renovations are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Renovations</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapRenovationLookupSession
    extends net.okapia.osid.jamocha.room.construction.spi.AbstractRenovationLookupSession
    implements org.osid.room.construction.RenovationLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.room.construction.Renovation> renovations = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.room.construction.Renovation>());


    /**
     *  Makes a <code>Renovation</code> available in this session.
     *
     *  @param  renovation a renovation
     *  @throws org.osid.NullArgumentException <code>renovation<code>
     *          is <code>null</code>
     */

    protected void putRenovation(org.osid.room.construction.Renovation renovation) {
        this.renovations.put(renovation.getId(), renovation);
        return;
    }


    /**
     *  Makes an array of renovations available in this session.
     *
     *  @param  renovations an array of renovations
     *  @throws org.osid.NullArgumentException <code>renovations<code>
     *          is <code>null</code>
     */

    protected void putRenovations(org.osid.room.construction.Renovation[] renovations) {
        putRenovations(java.util.Arrays.asList(renovations));
        return;
    }


    /**
     *  Makes a collection of renovations available in this session.
     *
     *  @param  renovations a collection of renovations
     *  @throws org.osid.NullArgumentException <code>renovations<code>
     *          is <code>null</code>
     */

    protected void putRenovations(java.util.Collection<? extends org.osid.room.construction.Renovation> renovations) {
        for (org.osid.room.construction.Renovation renovation : renovations) {
            this.renovations.put(renovation.getId(), renovation);
        }

        return;
    }


    /**
     *  Removes a Renovation from this session.
     *
     *  @param  renovationId the <code>Id</code> of the renovation
     *  @throws org.osid.NullArgumentException <code>renovationId<code> is
     *          <code>null</code>
     */

    protected void removeRenovation(org.osid.id.Id renovationId) {
        this.renovations.remove(renovationId);
        return;
    }


    /**
     *  Gets the <code>Renovation</code> specified by its <code>Id</code>.
     *
     *  @param  renovationId <code>Id</code> of the <code>Renovation</code>
     *  @return the renovation
     *  @throws org.osid.NotFoundException <code>renovationId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>renovationId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.Renovation getRenovation(org.osid.id.Id renovationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.room.construction.Renovation renovation = this.renovations.get(renovationId);
        if (renovation == null) {
            throw new org.osid.NotFoundException("renovation not found: " + renovationId);
        }

        return (renovation);
    }


    /**
     *  Gets all <code>Renovations</code>. In plenary mode, the returned
     *  list contains all known renovations or an error
     *  results. Otherwise, the returned list may contain only those
     *  renovations that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Renovations</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.room.construction.renovation.ArrayRenovationList(this.renovations.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.renovations.clear();
        super.close();
        return;
    }
}
