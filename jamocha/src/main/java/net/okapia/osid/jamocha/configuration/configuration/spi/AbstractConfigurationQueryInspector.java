//
// AbstractConfigurationQueryInspector.java
//
//     A template for making a ConfigurationQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.configuration.configuration.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for configurations.
 */

public abstract class AbstractConfigurationQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.configuration.ConfigurationQueryInspector {

    private final java.util.Collection<org.osid.configuration.records.ConfigurationQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the registry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getRegistryTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the parameter <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getParameterIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the parameter query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.configuration.ParameterQueryInspector[] getParameterTerms() {
        return (new org.osid.configuration.ParameterQueryInspector[0]);
    }


    /**
     *  Gets the ancestor configuration <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorConfigurationIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor configuration query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationQueryInspector[] getAncestorConfigurationTerms() {
        return (new org.osid.configuration.ConfigurationQueryInspector[0]);
    }


    /**
     *  Gets the descendant configuration <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantConfigurationIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant configuration query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationQueryInspector[] getDescendantConfigurationTerms() {
        return (new org.osid.configuration.ConfigurationQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given configuration query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a configuration implementing the requested record.
     *
     *  @param configurationRecordType a configuration record type
     *  @return the configuration query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>configurationRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(configurationRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.configuration.records.ConfigurationQueryInspectorRecord getConfigurationQueryInspectorRecord(org.osid.type.Type configurationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.configuration.records.ConfigurationQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(configurationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(configurationRecordType + " is not supported");
    }


    /**
     *  Adds a record to this configuration query. 
     *
     *  @param configurationQueryInspectorRecord configuration query inspector
     *         record
     *  @param configurationRecordType configuration record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addConfigurationQueryInspectorRecord(org.osid.configuration.records.ConfigurationQueryInspectorRecord configurationQueryInspectorRecord, 
                                                   org.osid.type.Type configurationRecordType) {

        addRecordType(configurationRecordType);
        nullarg(configurationRecordType, "configuration record type");
        this.records.add(configurationQueryInspectorRecord);        
        return;
    }
}
