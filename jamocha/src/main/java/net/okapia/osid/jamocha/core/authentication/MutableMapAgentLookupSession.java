//
// MutableMapAgentLookupSession
//
//    Implements an Agent lookup service backed by a collection of
//    agents that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authentication;


/**
 *  Implements an Agent lookup service backed by a collection of
 *  agents. The agents are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of agents can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapAgentLookupSession
    extends net.okapia.osid.jamocha.core.authentication.spi.AbstractMapAgentLookupSession
    implements org.osid.authentication.AgentLookupSession {


    /**
     *  Constructs a new {@code MutableMapAgentLookupSession}
     *  with no agents.
     *
     *  @param agency the agency
     *  @throws org.osid.NullArgumentException {@code agency} is
     *          {@code null}
     */

      public MutableMapAgentLookupSession(org.osid.authentication.Agency agency) {
        setAgency(agency);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapAgentLookupSession} with a
     *  single agent.
     *
     *  @param agency the agency  
     *  @param agent an agent
     *  @throws org.osid.NullArgumentException {@code agency} or
     *          {@code agent} is {@code null}
     */

    public MutableMapAgentLookupSession(org.osid.authentication.Agency agency,
                                           org.osid.authentication.Agent agent) {
        this(agency);
        putAgent(agent);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapAgentLookupSession}
     *  using an array of agents.
     *
     *  @param agency the agency
     *  @param agents an array of agents
     *  @throws org.osid.NullArgumentException {@code agency} or
     *          {@code agents} is {@code null}
     */

    public MutableMapAgentLookupSession(org.osid.authentication.Agency agency,
                                           org.osid.authentication.Agent[] agents) {
        this(agency);
        putAgents(agents);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapAgentLookupSession}
     *  using a collection of agents.
     *
     *  @param agency the agency
     *  @param agents a collection of agents
     *  @throws org.osid.NullArgumentException {@code agency} or
     *          {@code agents} is {@code null}
     */

    public MutableMapAgentLookupSession(org.osid.authentication.Agency agency,
                                           java.util.Collection<? extends org.osid.authentication.Agent> agents) {

        this(agency);
        putAgents(agents);
        return;
    }

    
    /**
     *  Makes an {@code Agent} available in this session.
     *
     *  @param agent an agent
     *  @throws org.osid.NullArgumentException {@code agent{@code  is
     *          {@code null}
     */

    @Override
    public void putAgent(org.osid.authentication.Agent agent) {
        super.putAgent(agent);
        return;
    }


    /**
     *  Makes an array of agents available in this session.
     *
     *  @param agents an array of agents
     *  @throws org.osid.NullArgumentException {@code agents{@code 
     *          is {@code null}
     */

    @Override
    public void putAgents(org.osid.authentication.Agent[] agents) {
        super.putAgents(agents);
        return;
    }


    /**
     *  Makes collection of agents available in this session.
     *
     *  @param agents a collection of agents
     *  @throws org.osid.NullArgumentException {@code agents{@code  is
     *          {@code null}
     */

    @Override
    public void putAgents(java.util.Collection<? extends org.osid.authentication.Agent> agents) {
        super.putAgents(agents);
        return;
    }


    /**
     *  Removes an Agent from this session.
     *
     *  @param agentId the {@code Id} of the agent
     *  @throws org.osid.NullArgumentException {@code agentId{@code 
     *          is {@code null}
     */

    @Override
    public void removeAgent(org.osid.id.Id agentId) {
        super.removeAgent(agentId);
        return;
    }    
}
