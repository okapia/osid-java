//
// AbstractNodeOfficeHierarchySession.java
//
//     Defines an Office hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.workflow.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an office hierarchy session for delivering a hierarchy
 *  of offices using the OfficeNode interface.
 */

public abstract class AbstractNodeOfficeHierarchySession
    extends net.okapia.osid.jamocha.workflow.spi.AbstractOfficeHierarchySession
    implements org.osid.workflow.OfficeHierarchySession {

    private java.util.Collection<org.osid.workflow.OfficeNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root office <code> Ids </code> in this hierarchy.
     *
     *  @return the root office <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootOfficeIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.workflow.officenode.OfficeNodeToIdList(this.roots));
    }


    /**
     *  Gets the root offices in the office hierarchy. A node
     *  with no parents is an orphan. While all office <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root offices 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.OfficeList getRootOffices()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.workflow.officenode.OfficeNodeToOfficeList(new net.okapia.osid.jamocha.workflow.officenode.ArrayOfficeNodeList(this.roots)));
    }


    /**
     *  Adds a root office node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootOffice(org.osid.workflow.OfficeNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root office nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootOffices(java.util.Collection<org.osid.workflow.OfficeNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root office node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootOffice(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.workflow.OfficeNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Office </code> has any parents. 
     *
     *  @param  officeId an office <code> Id </code> 
     *  @return <code> true </code> if the office has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> officeId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> officeId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentOffices(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getOfficeNode(officeId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of an
     *  office.
     *
     *  @param  id an <code> Id </code> 
     *  @param  officeId the <code> Id </code> of an office 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> officeId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> officeId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> officeId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfOffice(org.osid.id.Id id, org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.workflow.OfficeNodeList parents = getOfficeNode(officeId).getParentOfficeNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextOfficeNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given office. 
     *
     *  @param  officeId an office <code> Id </code> 
     *  @return the parent <code> Ids </code> of the office 
     *  @throws org.osid.NotFoundException <code> officeId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> officeId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentOfficeIds(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.workflow.office.OfficeToIdList(getParentOffices(officeId)));
    }


    /**
     *  Gets the parents of the given office. 
     *
     *  @param  officeId the <code> Id </code> to query 
     *  @return the parents of the office 
     *  @throws org.osid.NotFoundException <code> officeId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> officeId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.OfficeList getParentOffices(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.workflow.officenode.OfficeNodeToOfficeList(getOfficeNode(officeId).getParentOfficeNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of an
     *  office.
     *
     *  @param  id an <code> Id </code> 
     *  @param  officeId the Id of an office 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> officeId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> officeId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> officeId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfOffice(org.osid.id.Id id, org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfOffice(id, officeId)) {
            return (true);
        }

        try (org.osid.workflow.OfficeList parents = getParentOffices(officeId)) {
            while (parents.hasNext()) {
                if (isAncestorOfOffice(id, parents.getNextOffice().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if an office has any children. 
     *
     *  @param  officeId an office <code> Id </code> 
     *  @return <code> true </code> if the <code> officeId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> officeId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> officeId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildOffices(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getOfficeNode(officeId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of an
     *  office.
     *
     *  @param  id an <code> Id </code> 
     *  @param officeId the <code> Id </code> of an 
     *         office
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> officeId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> officeId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> officeId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfOffice(org.osid.id.Id id, org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfOffice(officeId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  office.
     *
     *  @param  officeId the <code> Id </code> to query 
     *  @return the children of the office 
     *  @throws org.osid.NotFoundException <code> officeId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> officeId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildOfficeIds(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.workflow.office.OfficeToIdList(getChildOffices(officeId)));
    }


    /**
     *  Gets the children of the given office. 
     *
     *  @param  officeId the <code> Id </code> to query 
     *  @return the children of the office 
     *  @throws org.osid.NotFoundException <code> officeId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> officeId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.OfficeList getChildOffices(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.workflow.officenode.OfficeNodeToOfficeList(getOfficeNode(officeId).getChildOfficeNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of an
     *  office.
     *
     *  @param  id an <code> Id </code> 
     *  @param officeId the <code> Id </code> of an 
     *         office
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> officeId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> officeId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> officeId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfOffice(org.osid.id.Id id, org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfOffice(officeId, id)) {
            return (true);
        }

        try (org.osid.workflow.OfficeList children = getChildOffices(officeId)) {
            while (children.hasNext()) {
                if (isDescendantOfOffice(id, children.getNextOffice().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  office.
     *
     *  @param  officeId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified office node 
     *  @throws org.osid.NotFoundException <code> officeId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> officeId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getOfficeNodeIds(org.osid.id.Id officeId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.workflow.officenode.OfficeNodeToNode(getOfficeNode(officeId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given office.
     *
     *  @param  officeId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified office node 
     *  @throws org.osid.NotFoundException <code> officeId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> officeId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.OfficeNode getOfficeNodes(org.osid.id.Id officeId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getOfficeNode(officeId));
    }


    /**
     *  Closes this <code>OfficeHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets an office node.
     *
     *  @param officeId the id of the office node
     *  @throws org.osid.NotFoundException <code>officeId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>officeId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.workflow.OfficeNode getOfficeNode(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(officeId, "office Id");
        for (org.osid.workflow.OfficeNode office : this.roots) {
            if (office.getId().equals(officeId)) {
                return (office);
            }

            org.osid.workflow.OfficeNode r = findOffice(office, officeId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(officeId + " is not found");
    }


    protected org.osid.workflow.OfficeNode findOffice(org.osid.workflow.OfficeNode node, 
                                                      org.osid.id.Id officeId) 
        throws org.osid.OperationFailedException {

        try (org.osid.workflow.OfficeNodeList children = node.getChildOfficeNodes()) {
            while (children.hasNext()) {
                org.osid.workflow.OfficeNode office = children.getNextOfficeNode();
                if (office.getId().equals(officeId)) {
                    return (office);
                }
                
                office = findOffice(office, officeId);
                if (office != null) {
                    return (office);
                }
            }
        }

        return (null);
    }
}
