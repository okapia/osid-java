//
// ReplyElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.forum.reply.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ReplyElements
    extends net.okapia.osid.jamocha.spi.ContainableOsidObjectElements {


    /**
     *  Gets the ReplyElement Id.
     *
     *  @return the reply element Id
     */

    public static org.osid.id.Id getReplyEntityId() {
        return (makeEntityId("osid.forum.Reply"));
    }


    /**
     *  Gets the PostId element Id.
     *
     *  @return the PostId element Id
     */

    public static org.osid.id.Id getPostId() {
        return (makeElementId("osid.forum.reply.PostId"));
    }


    /**
     *  Gets the Post element Id.
     *
     *  @return the Post element Id
     */

    public static org.osid.id.Id getPost() {
        return (makeElementId("osid.forum.reply.Post"));
    }


    /**
     *  Gets the ReplyIds element Id.
     *
     *  @return the ReplyIds element Id
     */

    public static org.osid.id.Id getReplyIds() {
        return (makeElementId("osid.forum.reply.ReplyIds"));
    }


    /**
     *  Gets the Replies element Id.
     *
     *  @return the Replies element Id
     */

    public static org.osid.id.Id getReplies() {
        return (makeElementId("osid.forum.reply.Replies"));
    }


    /**
     *  Gets the Timestamp element Id.
     *
     *  @return the Timestamp element Id
     */

    public static org.osid.id.Id getTimestamp() {
        return (makeElementId("osid.forum.reply.Timestamp"));
    }


    /**
     *  Gets the PosterId element Id.
     *
     *  @return the PosterId element Id
     */

    public static org.osid.id.Id getPosterId() {
        return (makeElementId("osid.forum.reply.PosterId"));
    }


    /**
     *  Gets the Poster element Id.
     *
     *  @return the Poster element Id
     */

    public static org.osid.id.Id getPoster() {
        return (makeElementId("osid.forum.reply.Poster"));
    }


    /**
     *  Gets the PostingAgentId element Id.
     *
     *  @return the PostingAgentId element Id
     */

    public static org.osid.id.Id getPostingAgentId() {
        return (makeElementId("osid.forum.reply.PostingAgentId"));
    }


    /**
     *  Gets the PostingAgent element Id.
     *
     *  @return the PostingAgent element Id
     */

    public static org.osid.id.Id getPostingAgent() {
        return (makeElementId("osid.forum.reply.PostingAgent"));
    }


    /**
     *  Gets the SubjectLine element Id.
     *
     *  @return the SubjectLine element Id
     */

    public static org.osid.id.Id getSubjectLine() {
        return (makeElementId("osid.forum.reply.SubjectLine"));
    }


    /**
     *  Gets the Text element Id.
     *
     *  @return the Text element Id
     */

    public static org.osid.id.Id getText() {
        return (makeElementId("osid.forum.reply.Text"));
    }


    /**
     *  Gets the ContainingReplyId element Id.
     *
     *  @return the ContainingReplyId element Id
     */

    public static org.osid.id.Id getContainingReplyId() {
        return (makeQueryElementId("osid.forum.reply.ContainingReplyId"));
    }


    /**
     *  Gets the ContainingReply element Id.
     *
     *  @return the ContainingReply element Id
     */

    public static org.osid.id.Id getContainingReply() {
        return (makeQueryElementId("osid.forum.reply.ContainingReply"));
    }


    /**
     *  Gets the ContainedReplyId element Id.
     *
     *  @return the ContainedReplyId element Id
     */

    public static org.osid.id.Id getContainedReplyId() {
        return (makeQueryElementId("osid.forum.reply.ContainedReplyId"));
    }


    /**
     *  Gets the ContainedReply element Id.
     *
     *  @return the ContainedReply element Id
     */

    public static org.osid.id.Id getContainedReply() {
        return (makeQueryElementId("osid.forum.reply.ContainedReply"));
    }


    /**
     *  Gets the ForumId element Id.
     *
     *  @return the ForumId element Id
     */

    public static org.osid.id.Id getForumId() {
        return (makeQueryElementId("osid.forum.reply.ForumId"));
    }


    /**
     *  Gets the Forum element Id.
     *
     *  @return the Forum element Id
     */

    public static org.osid.id.Id getForum() {
        return (makeQueryElementId("osid.forum.reply.Forum"));
    }
}
