//
// AbstractMeteringManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metering.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractMeteringManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.metering.MeteringManager,
               org.osid.metering.MeteringProxyManager {

    private final Types statisticRecordTypes               = new TypeRefSet();
    private final Types statisticSearchRecordTypes         = new TypeRefSet();

    private final Types meterRecordTypes                   = new TypeRefSet();
    private final Types meterSearchRecordTypes             = new TypeRefSet();

    private final Types utilityRecordTypes                 = new TypeRefSet();
    private final Types utilitySearchRecordTypes           = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractMeteringManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractMeteringManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if visible federation is supported. 
     *
     *  @return <code> true </code> if visible federation is supproted, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if reading meters is supported. 
     *
     *  @return <code> true </code> if reading meters is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMeterReading() {
        return (false);
    }


    /**
     *  Tests if statistical lookup is supported. 
     *
     *  @return <code> true </code> if statistical lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStatisticLookup() {
        return (false);
    }


    /**
     *  Tests if statistical query is supported. 
     *
     *  @return <code> true </code> if statistical query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStatisticQuery() {
        return (false);
    }


    /**
     *  Tests if statistical search is supported. 
     *
     *  @return <code> true </code> if statistical search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStatisticSearch() {
        return (false);
    }


    /**
     *  Tests if statistical notification is supported. 
     *
     *  @return <code> true </code> if statistical notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStatisticNotification() {
        return (false);
    }


    /**
     *  Tests if statistical smart utilitiy is supported. 
     *
     *  @return <code> true </code> if statistical smart utility is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStatisticSmartUtility() {
        return (false);
    }


    /**
     *  Tests if looking up meters is supported. 
     *
     *  @return <code> true </code> if looking up meters is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMeterLookup() {
        return (false);
    }


    /**
     *  Tests if querying meters is supported. 
     *
     *  @return <code> true </code> if querying meters is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMeterQuery() {
        return (false);
    }


    /**
     *  Tests if searching meters is supported. 
     *
     *  @return <code> true </code> if searching meters is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMeterSearch() {
        return (false);
    }


    /**
     *  Tests if meter notification is supported,. 
     *
     *  @return <code> true </code> if meter notification is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMeterNotification() {
        return (false);
    }


    /**
     *  Tests if looking up meter utlity mappings is supported,. 
     *
     *  @return <code> true </code> if utility meter mapping is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMeterUtility() {
        return (false);
    }


    /**
     *  Tests if managing meter utlity mappings is supported,. 
     *
     *  @return <code> true </code> if assigning utility meter mappings is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMeterUtilityAssignment() {
        return (false);
    }


    /**
     *  Tests if managing meter smart utlity service is supported,. 
     *
     *  @return <code> true </code> if a meter smart utility service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSmartMeterUtility() {
        return (false);
    }


    /**
     *  Tests for the availability of a utility lookup service. 
     *
     *  @return <code> true </code> if utility lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsUtilityLookup() {
        return (false);
    }


    /**
     *  Tests for the availability of a utility query service. 
     *
     *  @return <code> true </code> if utility query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsUtilityQuery() {
        return (false);
    }


    /**
     *  Tests if searching for utilities is available. 
     *
     *  @return <code> true </code> if utility search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsUtilitySearch() {
        return (false);
    }


    /**
     *  Tests for the availability of a utility administrative service for 
     *  creating and deleting utilities. 
     *
     *  @return <code> true </code> if utility administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsUtilityAdmin() {
        return (false);
    }


    /**
     *  Tests for the availability of a utility notification service. 
     *
     *  @return <code> true </code> if utility notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsUtilityNotification() {
        return (false);
    }


    /**
     *  Tests for the availability of a utility hierarchy traversal service. 
     *
     *  @return <code> true </code> if utility hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsUtilityHierarchy() {
        return (false);
    }


    /**
     *  Tests for the availability of a utility hierarchy design service. 
     *
     *  @return <code> true </code> if utility hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsUtilityHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests for the availability of a meteringbatch service. 
     *
     *  @return <code> true </code> if meteringbatch service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMeteringBatch() {
        return (false);
    }


    /**
     *  Tests if a metering threshold service is supported. 
     *
     *  @return <code> true </code> if a metering threshold service is
     *          supported, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean supportsMeteringThreshold() {
        return (false);
    }


    /**
     *  Gets the supported <code> Statistic </code> record types. 
     *
     *  @return a list containing the supported statistic record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getStatisticRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.statisticRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Statistic </code> record type is supported. 
     *
     *  @param  statisticRecordType a <code> Type </code> indicating a <code> 
     *          Statistic </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> statisticRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsStatisticRecordType(org.osid.type.Type statisticRecordType) {
        return (this.statisticRecordTypes.contains(statisticRecordType));
    }


    /**
     *  Adds support for a statistic record type.
     *
     *  @param statisticRecordType a statistic record type
     *  @throws org.osid.NullArgumentException
     *  <code>statisticRecordType</code> is <code>null</code>
     */

    protected void addStatisticRecordType(org.osid.type.Type statisticRecordType) {
        this.statisticRecordTypes.add(statisticRecordType);
        return;
    }


    /**
     *  Removes support for a statistic record type.
     *
     *  @param statisticRecordType a statistic record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>statisticRecordType</code> is <code>null</code>
     */

    protected void removeStatisticRecordType(org.osid.type.Type statisticRecordType) {
        this.statisticRecordTypes.remove(statisticRecordType);
        return;
    }


    /**
     *  Gets the supported statistic search record types. 
     *
     *  @return a list containing the supported statistic search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getStatisticSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.statisticSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given statistic search record type is supported. 
     *
     *  @param  statisticSearchRecordType a <code> Type </code> indicating a 
     *          statistic record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          statisticSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsStatisticSearchRecordType(org.osid.type.Type statisticSearchRecordType) {
        return (this.statisticSearchRecordTypes.contains(statisticSearchRecordType));
    }


    /**
     *  Adds support for a statistic search record type.
     *
     *  @param statisticSearchRecordType a statistic search record type
     *  @throws org.osid.NullArgumentException
     *  <code>statisticSearchRecordType</code> is <code>null</code>
     */

    protected void addStatisticSearchRecordType(org.osid.type.Type statisticSearchRecordType) {
        this.statisticSearchRecordTypes.add(statisticSearchRecordType);
        return;
    }


    /**
     *  Removes support for a statistic search record type.
     *
     *  @param statisticSearchRecordType a statistic search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>statisticSearchRecordType</code> is <code>null</code>
     */

    protected void removeStatisticSearchRecordType(org.osid.type.Type statisticSearchRecordType) {
        this.statisticSearchRecordTypes.remove(statisticSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Meter </code> record types. 
     *
     *  @return a list containing the supported meter record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getMeterRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.meterRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Meter </code> record type is supported. 
     *
     *  @param  meterRecordType a <code> Type </code> indicating a <code> 
     *          Meter </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> meterRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsMeterRecordType(org.osid.type.Type meterRecordType) {
        return (this.meterRecordTypes.contains(meterRecordType));
    }


    /**
     *  Adds support for a meter record type.
     *
     *  @param meterRecordType a meter record type
     *  @throws org.osid.NullArgumentException
     *  <code>meterRecordType</code> is <code>null</code>
     */

    protected void addMeterRecordType(org.osid.type.Type meterRecordType) {
        this.meterRecordTypes.add(meterRecordType);
        return;
    }


    /**
     *  Removes support for a meter record type.
     *
     *  @param meterRecordType a meter record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>meterRecordType</code> is <code>null</code>
     */

    protected void removeMeterRecordType(org.osid.type.Type meterRecordType) {
        this.meterRecordTypes.remove(meterRecordType);
        return;
    }


    /**
     *  Gets the supported meter search record types. 
     *
     *  @return a list containing the supported meter search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getMeterSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.meterSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given meter search record type is supported. 
     *
     *  @param  meterSearchRecordType a <code> Type </code> indicating a meter 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> meterSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsMeterSearchRecordType(org.osid.type.Type meterSearchRecordType) {
        return (this.meterSearchRecordTypes.contains(meterSearchRecordType));
    }


    /**
     *  Adds support for a meter search record type.
     *
     *  @param meterSearchRecordType a meter search record type
     *  @throws org.osid.NullArgumentException
     *  <code>meterSearchRecordType</code> is <code>null</code>
     */

    protected void addMeterSearchRecordType(org.osid.type.Type meterSearchRecordType) {
        this.meterSearchRecordTypes.add(meterSearchRecordType);
        return;
    }


    /**
     *  Removes support for a meter search record type.
     *
     *  @param meterSearchRecordType a meter search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>meterSearchRecordType</code> is <code>null</code>
     */

    protected void removeMeterSearchRecordType(org.osid.type.Type meterSearchRecordType) {
        this.meterSearchRecordTypes.remove(meterSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Utility </code> record types. 
     *
     *  @return a list containing the supported utility record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getUtilityRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.utilityRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Utility </code> record type is supported. 
     *
     *  @param  utilityRecordType a <code> Type </code> indicating a <code> 
     *          Utility </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> utilityRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsUtilityRecordType(org.osid.type.Type utilityRecordType) {
        return (this.utilityRecordTypes.contains(utilityRecordType));
    }


    /**
     *  Adds support for an utility record type.
     *
     *  @param utilityRecordType an utility record type
     *  @throws org.osid.NullArgumentException
     *  <code>utilityRecordType</code> is <code>null</code>
     */

    protected void addUtilityRecordType(org.osid.type.Type utilityRecordType) {
        this.utilityRecordTypes.add(utilityRecordType);
        return;
    }


    /**
     *  Removes support for an utility record type.
     *
     *  @param utilityRecordType an utility record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>utilityRecordType</code> is <code>null</code>
     */

    protected void removeUtilityRecordType(org.osid.type.Type utilityRecordType) {
        this.utilityRecordTypes.remove(utilityRecordType);
        return;
    }


    /**
     *  Gets the supported utility search record types. 
     *
     *  @return a list containing the supported utility search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getUtilitySearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.utilitySearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given utility search record type is supported. 
     *
     *  @param  utilitySearchRecordType a <code> Type </code> indicating a 
     *          utility record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> utilitySearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsUtilitySearchRecordType(org.osid.type.Type utilitySearchRecordType) {
        return (this.utilitySearchRecordTypes.contains(utilitySearchRecordType));
    }


    /**
     *  Adds support for an utility search record type.
     *
     *  @param utilitySearchRecordType an utility search record type
     *  @throws org.osid.NullArgumentException
     *  <code>utilitySearchRecordType</code> is <code>null</code>
     */

    protected void addUtilitySearchRecordType(org.osid.type.Type utilitySearchRecordType) {
        this.utilitySearchRecordTypes.add(utilitySearchRecordType);
        return;
    }


    /**
     *  Removes support for an utility search record type.
     *
     *  @param utilitySearchRecordType an utility search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>utilitySearchRecordType</code> is <code>null</code>
     */

    protected void removeUtilitySearchRecordType(org.osid.type.Type utilitySearchRecordType) {
        this.utilitySearchRecordTypes.remove(utilitySearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the meter reading 
     *  service. 
     *
     *  @return a <code> MeterReadingSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMeterReading() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterReadingSession getMeterReadingSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringManager.getMeterReadingSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the meter reading 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MeterReadingSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMeterreading() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterReadingSession getMeterReadingSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringProxyManager.getMeterReadingSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the meter reading 
     *  service for the given utility. 
     *
     *  @param  utilityId the <code> Id </code> of the <code> Utility </code> 
     *  @return a <code> MeterReadingSession </code> 
     *  @throws org.osid.NotFoundException no <code> Utility </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> utilityId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMeterReading() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterReadingSession getMeterReadingSessionForUtility(org.osid.id.Id utilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringManager.getMeterReadingSessionForUtility not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the meter reading 
     *  service for the given utility. 
     *
     *  @param  utilityId the <code> Id </code> of the <code> Utility </code> 
     *  @param  proxy a proxy 
     *  @return a <code> MeterReadingSession </code> 
     *  @throws org.osid.NotFoundException no <code> Utility </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> utilityId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMeterReading() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterReadingSession getMeterReadingSessionForUtility(org.osid.id.Id utilityId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringProxyManager.getMeterReadingSessionForUtility not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the statistic 
     *  lookup service. 
     *
     *  @return a <code> StatisticLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStatisticLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.StatisticLookupSession getStatisticLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringManager.getStatisticLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the statistic 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MeterStatisticsSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStatisticLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.StatisticLookupSession getStatisticLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringProxyManager.getStatisticLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the statistic 
     *  lookup service for the given utility. 
     *
     *  @param  utilityId the <code> Id </code> of the <code> Utility </code> 
     *  @return a <code> StatisticLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Utility </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> utilityId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStatisticLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.StatisticLookupSession getStatisticLookupSessionForUtility(org.osid.id.Id utilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringManager.getStatisticLookupSessionForUtility not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the statistic 
     *  lookup service for the given utility. 
     *
     *  @param  utilityId the <code> Id </code> of the <code> Utility </code> 
     *  @param  proxy a proxy 
     *  @return a <code> MeterStatisticsSession </code> 
     *  @throws org.osid.NotFoundException no <code> Utility </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> utilityId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStatisticLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.StatisticLookupSession getStatisticLookupSessionForUtility(org.osid.id.Id utilityId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringProxyManager.getStatisticLookupSessionForUtility not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the statistic 
     *  query service. 
     *
     *  @return a <code> StatisticQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStatisticQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.StatisticQuerySession getStatisticQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringManager.getStatisticQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the statistic 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StatisticQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStatisticQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.StatisticQuerySession getStatisticQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringProxyManager.getStatisticQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the statistic 
     *  query service for the given utility. 
     *
     *  @param  utilityId the <code> Id </code> of the <code> Utility </code> 
     *  @return a <code> StatisticQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Utility </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> utilityId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStatisticQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.StatisticQuerySession getStatisticQuerySessionForUtility(org.osid.id.Id utilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringManager.getStatisticQuerySessionForUtility not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the statistic 
     *  query service for the given utility. 
     *
     *  @param  utilityId the <code> Id </code> of the <code> Utility </code> 
     *  @param  proxy a proxy 
     *  @return a <code> StatisticQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Utility </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> utilityId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStatisticQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.StatisticQuerySession getStatisticQuerySessionForUtility(org.osid.id.Id utilityId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringProxyManager.getStatisticQuerySessionForUtility not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the statistic 
     *  search service. 
     *
     *  @return a <code> StatisticSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStatisticSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.StatisticSearchSession getStatisticSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringManager.getStatisticSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the statistic 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StatisticSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStatisticSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.StatisticSearchSession getStatisticSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringProxyManager.getStatisticSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the statistic 
     *  search service for the given utility. 
     *
     *  @param  utilityId the <code> Id </code> of the <code> Utility </code> 
     *  @return a <code> StatisticSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Utility </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> utilityId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStatisticSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.StatisticSearchSession getStatisticSearchSessionForUtility(org.osid.id.Id utilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringManager.getStatisticSearchSessionForUtility not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the statistic 
     *  search service for the given utility. 
     *
     *  @param  utilityId the <code> Id </code> of the <code> Utility </code> 
     *  @param  proxy a proxy 
     *  @return a <code> StatisticSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Utility </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> utilityId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStatisticSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.StatisticSearchSession getStatisticSearchSessionForUtility(org.osid.id.Id utilityId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringProxyManager.getStatisticSearchSessionForUtility not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the statistic 
     *  notification service. 
     *
     *  @param  statisticReceiver the receiver 
     *  @return a <code> StatisticNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> statisticReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStatisticNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterNotificationSession getStatisticNotificationSession(org.osid.metering.StatisticReceiver statisticReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringManager.getStatisticNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the statistic 
     *  notification service. 
     *
     *  @param  statisticReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> StatisticNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> statisticReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStatisticNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.metering.StatisticNotificationSession getStatisticNotificationSession(org.osid.metering.StatisticReceiver statisticReceiver, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringProxyManager.getStatisticNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the statistic 
     *  notification service for the given utility. 
     *
     *  @param  statisticReceiver the receiver 
     *  @param  utilityId the <code> Id </code> of the <code> Utility </code> 
     *  @return a <code> StatisticNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Utility </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> statisticReceiver 
     *          </code> or <code> utilityId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStatisticNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterNotificationSession getStatisticNotificationSessionForUtility(org.osid.metering.StatisticReceiver statisticReceiver, 
                                                                                                org.osid.id.Id utilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringManager.getStatisticNotificationSessionForUtility not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the statistic 
     *  notification service for the given utility. 
     *
     *  @param  statisticReceiver the receiver 
     *  @param  utilityId the <code> Id </code> of the <code> Utility </code> 
     *  @param  proxy a proxy 
     *  @return a <code> StatisticNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Utility </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> statisticReceiver, 
     *          utilityId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStatisticNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.StatisticNotificationSession getStatisticNotificationSessionForUtility(org.osid.metering.StatisticReceiver statisticReceiver, 
                                                                                                    org.osid.id.Id utilityId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringProxyManager.getStatisticNotificationSessionForUtility not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the statistic 
     *  smart utility session. 
     *
     *  @param  utilityId the <code> Id </code> of the <code> Utility </code> 
     *  @return a <code> StatisticSmartUtilitySession </code> 
     *  @throws org.osid.NotFoundException no <code> Utility </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> utilityId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStatisticSmartUtility() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.StatisticSmartUtilitySession getStatisticSmartUtilitySession(org.osid.id.Id utilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringManager.getStatisticSmartUtilitySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the statistic 
     *  smart utility session. 
     *
     *  @param  utilityId the <code> Id </code> of the <code> Utility </code> 
     *  @param  proxy a proxy 
     *  @return a <code> StatisticSmartUtilitySession </code> 
     *  @throws org.osid.NotFoundException no <code> Utility </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> utilityId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStatisticSmartUtility() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.StatisticSmartUtilitySession getStatisticSmartUtilitySession(org.osid.id.Id utilityId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringProxyManager.getStatisticSmartUtilitySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the meter lookup 
     *  service. 
     *
     *  @return a <code> MeterLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMeterLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterLookupSession getMeterLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringManager.getMeterLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the meter lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MeterLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMeterLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterLookupSession getMeterLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringProxyManager.getMeterLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the meter lookup 
     *  service for the given utility. 
     *
     *  @param  utilityId the <code> Id </code> of the <code> Utility </code> 
     *  @return a <code> MeterLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Utility </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> utilityId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMeterLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterLookupSession getMeterLookupSessionForUtility(org.osid.id.Id utilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringManager.getMeterLookupSessionForUtility not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the utility 
     *  reading service for the given utility. 
     *
     *  @param  utilityId the <code> Id </code> of the <code> Utility </code> 
     *  @param  proxy a proxy 
     *  @return a <code> MeterLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Utility </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> utilityId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMeterLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterLookupSession getMeterLookupSessionForUtility(org.osid.id.Id utilityId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringProxyManager.getMeterLookupSessionForUtility not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the meter query 
     *  service. 
     *
     *  @return a <code> MeterQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMeterQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterQuerySession getMeterQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringManager.getMeterQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the meter query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MeterQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMeterQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterQuerySession getMeterQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringProxyManager.getMeterQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the meter query 
     *  service for the given utility. 
     *
     *  @param  utilityId the <code> Id </code> of the <code> Utility </code> 
     *  @return a <code> MeterQuerySession </code> 
     *  @throws org.osid.NotFoundException no Utility found by the given Id 
     *  @throws org.osid.NullArgumentException <code> utilityId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMeterQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterQuerySession getMeterQuerySessionForUtility(org.osid.id.Id utilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringManager.getMeterQuerySessionForUtility not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the utility query 
     *  service for the given utility. 
     *
     *  @param  utilityId the <code> Id </code> of the <code> Utility </code> 
     *  @param  proxy a proxy 
     *  @return a <code> MeterQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Utility </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> utilityId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMeterQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterQuerySession getMeterQuerySessionForUtility(org.osid.id.Id utilityId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringProxyManager.getMeterQuerySessionForUtility not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the metering 
     *  search service. 
     *
     *  @return a <code> MeterSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMeterSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterSearchSession getMeterSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringManager.getMeterSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the meter search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MeterSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMeterSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterSearchSession getMeterSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringProxyManager.getMeterSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the meter search 
     *  service for the given utility. 
     *
     *  @param  utilityId the <code> Id </code> of the <code> Utility </code> 
     *  @return a <code> MeterSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Utility </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> utilityId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMeterSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterSearchSession getMeterSearchSessionForUtility(org.osid.id.Id utilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringManager.getMeterSearchSessionForUtility not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the meter search 
     *  service for the given utility. 
     *
     *  @param  utilityId the <code> Id </code> of the <code> Utility </code> 
     *  @param  proxy a proxy 
     *  @return a <code> MeterSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Utility </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> utilityId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMeterSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterSearchSession getMeterSearchSessionForUtility(org.osid.id.Id utilityId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringProxyManager.getMeterSearchSessionForUtility not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the meter 
     *  administrative service. 
     *
     *  @return a <code> MeterAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMeterAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterAdminSession getMeterAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringManager.getMeterAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the meter 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MeterAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMeterAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterAdminSession getMeterAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringProxyManager.getMeterAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the meter 
     *  administrative service for the given utility. 
     *
     *  @param  utilityId the <code> Id </code> of the <code> Utility </code> 
     *  @return a <code> MeterAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Utility </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> utilityId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMeterAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterAdminSession getMeterAdminSessionForUtility(org.osid.id.Id utilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringManager.getMeterAdminSessionForUtility not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the meter 
     *  administrative service for the given utility. 
     *
     *  @param  utilityId the <code> Id </code> of the <code> Utility </code> 
     *  @param  proxy a proxy 
     *  @return a <code> MeterAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Utility </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> utilityId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMeterAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterAdminSession getMeterAdminSessionForUtility(org.osid.id.Id utilityId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringProxyManager.getMeterAdminSessionForUtility not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the meter 
     *  notification service. 
     *
     *  @param  meterReceiver the receiver 
     *  @return a <code> MeterNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> meterReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMeterNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterNotificationSession getMeterNotificationSession(org.osid.metering.MeterReceiver meterReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringManager.getMeterNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the meter 
     *  notification service. 
     *
     *  @param  meterReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> MeterNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> meterReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMeterNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterNotificationSession getMeterNotificationSession(org.osid.metering.MeterReceiver meterReceiver, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringProxyManager.getMeterNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the meter 
     *  notification service for the given utility. 
     *
     *  @param  meterReceiver the receiver 
     *  @param  utilityId the <code> Id </code> of the <code> Utility </code> 
     *  @return a <code> MeterNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Utility </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> meterReceiver </code> or 
     *          <code> utilityId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMeterNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterNotificationSession getMeterNotificationSessionForUtility(org.osid.metering.MeterReceiver meterReceiver, 
                                                                                            org.osid.id.Id utilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringManager.getMeterNotificationSessionForUtility not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the meter 
     *  notification service for the given utility. 
     *
     *  @param  meterReceiver the receiver 
     *  @param  utilityId the <code> Id </code> of the <code> Utility </code> 
     *  @param  proxy a proxy 
     *  @return a <code> MeterNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Utility </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> meterReceiver, utilityId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMeterNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterNotificationSession getMeterNotificationSessionForUtility(org.osid.metering.MeterReceiver meterReceiver, 
                                                                                            org.osid.id.Id utilityId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringProxyManager.getMeterNotificationSessionForUtility not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with looking up mapping 
     *  of meters and utilities. 
     *
     *  @return a <code> MeterUtilitySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMeterUtility() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterUtilitySession getMeterUtilitySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringManager.getMeterUtilitySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with looking up mapping 
     *  of meters and utilities. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MeterUtilitySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMeterUtility() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterUtilitySession getMeterUtilitySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringProxyManager.getMeterUtilitySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with managing the 
     *  mapping of meters and utilities. 
     *
     *  @return a <code> MeterUtilityAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMeterUtilityAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterUtilityAssignmentSession getMeterUtilityAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringManager.getMeterUtilityAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with managing the 
     *  mapping of meters and utilities. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MeterUtilityAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMeterUtilityAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterUtilityAssignmentSession getMeterUtilityAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringProxyManager.getMeterUtilityAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the meter smart 
     *  utility session. 
     *
     *  @param  utilityId the <code> Id </code> of the <code> Utility </code> 
     *  @return a <code> MeterSmartUtilitySession </code> 
     *  @throws org.osid.NotFoundException no <code> Utility </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> utilityId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMeterSmartUtility() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterSmartUtilitySession getMeterSmartUtilitySession(org.osid.id.Id utilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringManager.getMeterSmartUtilitySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the meter smart 
     *  utility session. 
     *
     *  @param  utilityId the <code> Id </code> of the <code> Utility </code> 
     *  @param  proxy a proxy 
     *  @return a <code> MeterSmartUtilitySession </code> 
     *  @throws org.osid.NotFoundException no <code> Utility </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> utilityId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMeterSmartUtility() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterSmartUtilitySession getMeterSmartUtilitySession(org.osid.id.Id utilityId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringProxyManager.getMeterSmartUtilitySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the utility lookup 
     *  service. 
     *
     *  @return a <code> UtilityLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsUtilityLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.UtilityLookupSession getUtilityLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringManager.getUtilityLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the utility lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> UtilityLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsUtilityLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.UtilityLookupSession getUtilityLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringProxyManager.getUtilityLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the utility query 
     *  service. 
     *
     *  @return a <code> UtilityQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsUtilityQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.UtilityQuerySession getUtilityQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringManager.getUtilityQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the utility query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> UtilityQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsUtilityQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.UtilityQuerySession getUtilityQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringProxyManager.getUtilityQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the utility search 
     *  service. 
     *
     *  @return a <code> UtilitySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsUtilitySearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.UtilitySearchSession getUtilitySearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringManager.getUtilitySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the utility search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> UtilitySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsUtilitySearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.UtilitySearchSession getUtilitySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringProxyManager.getUtilitySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the utility 
     *  administrative service. 
     *
     *  @return a <code> UtilityAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsUtilityAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.UtilityAdminSession getUtilityAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringManager.getUtilityAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the utility 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> UtilityAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsUtilityAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.UtilityAdminSession getUtilityAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringProxyManager.getUtilityAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the utility 
     *  notification service. 
     *
     *  @param  utilityReceiver the receiver 
     *  @return a <code> UtilityNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> utilityReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsUtilityNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.UtilityNotificationSession getUtilityNotificationSession(org.osid.metering.UtilityReceiver utilityReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringManager.getUtilityNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the utility 
     *  notification service. 
     *
     *  @param  utilityReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> UtilityNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> utilityReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsUtilityNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.UtilityNotificationSession getUtilityNotificationSession(org.osid.metering.UtilityReceiver utilityReceiver, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringProxyManager.getUtilityNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the utility 
     *  hierarchy service. 
     *
     *  @return a <code> UtilityHierarchySession </code> for utilities 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsUtilityHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.UtilityHierarchySession getUtilityHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringManager.getUtilityHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the utility 
     *  hierarchy service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> UtilityHierarchySession </code> for utilities 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsUtilityHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.UtilityHierarchySession getUtilityHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringProxyManager.getUtilityHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the utility 
     *  hierarchy design service. 
     *
     *  @return a <code> HierarchyDesignSession </code> for utilities 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsUtilityHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.metering.UtilityHierarchyDesignSession getUtilityHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringManager.getUtilityHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the utility 
     *  hierarchy design service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HierarchyDesignSession </code> for utilities 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsUtilityHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.metering.UtilityHierarchyDesignSession getUtilityHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringProxyManager.getUtilityHierarchyDesignSession not implemented");
    }


    /**
     *  Gets a <code> MeteringBatchManager. </code> 
     *
     *  @return a <code> MeteringBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMeteringBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.batch.MeteringBatchManager getMeteringBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringManager.getMeteringBatchManager not implemented");
    }


    /**
     *  Gets a <code> MeteringBatchProxyManager. </code> 
     *
     *  @return a <code> MeteringBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMeteringBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.batch.MeteringBatchProxyManager getMeteringBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringProxyManager.getMeteringBatchProxyManager not implemented");
    }


    /**
     *  Gets a <code> MeteringThresholdManager. </code> 
     *
     *  @return a <code> MeteringThresholdManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMeteringThreshold() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.threshold.MeteringThresholdManager getMeteringThresholdManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringManager.getMeteringThresholdManager not implemented");
    }


    /**
     *  Gets a <code> MeteringThresholdProxyManager. </code> 
     *
     *  @return a <code> MeteringThresholdProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMeteringThreshold() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.threshold.MeteringThresholdProxyManager getMeteringThresholdProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.metering.MeteringProxyManager.getMeteringThresholdProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.statisticRecordTypes.clear();
        this.statisticRecordTypes.clear();

        this.statisticSearchRecordTypes.clear();
        this.statisticSearchRecordTypes.clear();

        this.meterRecordTypes.clear();
        this.meterRecordTypes.clear();

        this.meterSearchRecordTypes.clear();
        this.meterSearchRecordTypes.clear();

        this.utilityRecordTypes.clear();
        this.utilityRecordTypes.clear();

        this.utilitySearchRecordTypes.clear();
        this.utilitySearchRecordTypes.clear();

        return;
    }
}
