//
// AbstractWarehouseLookupSession.java
//
//    A starter implementation framework for providing a Warehouse
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A starter implementation framework for providing a Warehouse
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getWarehouses(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractWarehouseLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.inventory.WarehouseLookupSession {

    private boolean pedantic = false;
    private org.osid.inventory.Warehouse warehouse = new net.okapia.osid.jamocha.nil.inventory.warehouse.UnknownWarehouse();
    

    /**
     *  Tests if this user can perform <code>Warehouse</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupWarehouses() {
        return (true);
    }


    /**
     *  A complete view of the <code>Warehouse</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeWarehouseView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Warehouse</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryWarehouseView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }

     
    /**
     *  Gets the <code>Warehouse</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Warehouse</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Warehouse</code> and
     *  retained for compatibility.
     *
     *  @param  warehouseId <code>Id</code> of the
     *          <code>Warehouse</code>
     *  @return the warehouse
     *  @throws org.osid.NotFoundException <code>warehouseId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>warehouseId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Warehouse getWarehouse(org.osid.id.Id warehouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.inventory.WarehouseList warehouses = getWarehouses()) {
            while (warehouses.hasNext()) {
                org.osid.inventory.Warehouse warehouse = warehouses.getNextWarehouse();
                if (warehouse.getId().equals(warehouseId)) {
                    return (warehouse);
                }
            }
        } 

        throw new org.osid.NotFoundException(warehouseId + " not found");
    }


    /**
     *  Gets a <code>WarehouseList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  warehouses specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Warehouses</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getWarehouses()</code>.
     *
     *  @param  warehouseIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Warehouse</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>warehouseIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseList getWarehousesByIds(org.osid.id.IdList warehouseIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.inventory.Warehouse> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = warehouseIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getWarehouse(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("warehouse " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.inventory.warehouse.LinkedWarehouseList(ret));
    }


    /**
     *  Gets a <code>WarehouseList</code> corresponding to the given
     *  warehouse genus <code>Type</code> which does not include
     *  warehouses of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  warehouses or an error results. Otherwise, the returned list
     *  may contain only those warehouses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getWarehouses()</code>.
     *
     *  @param  warehouseGenusType a warehouse genus type 
     *  @return the returned <code>Warehouse</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>warehouseGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseList getWarehousesByGenusType(org.osid.type.Type warehouseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.inventory.warehouse.WarehouseGenusFilterList(getWarehouses(), warehouseGenusType));
    }


    /**
     *  Gets a <code>WarehouseList</code> corresponding to the given
     *  warehouse genus <code>Type</code> and include any additional
     *  warehouses with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  warehouses or an error results. Otherwise, the returned list
     *  may contain only those warehouses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getWarehouses()</code>.
     *
     *  @param  warehouseGenusType a warehouse genus type 
     *  @return the returned <code>Warehouse</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>warehouseGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseList getWarehousesByParentGenusType(org.osid.type.Type warehouseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getWarehousesByGenusType(warehouseGenusType));
    }


    /**
     *  Gets a <code>WarehouseList</code> containing the given
     *  warehouse record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  warehouses or an error results. Otherwise, the returned list
     *  may contain only those warehouses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getWarehouses()</code>.
     *
     *  @param  warehouseRecordType a warehouse record type 
     *  @return the returned <code>Warehouse</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>warehouseRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseList getWarehousesByRecordType(org.osid.type.Type warehouseRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.inventory.warehouse.WarehouseRecordFilterList(getWarehouses(), warehouseRecordType));
    }


    /**
     *  Gets a <code>WarehouseList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known warehouses or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  warehouses that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Warehouse</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseList getWarehousesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.inventory.warehouse.WarehouseProviderFilterList(getWarehouses(), resourceId));
    }


    /**
     *  Gets all <code>Warehouses</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  warehouses or an error results. Otherwise, the returned list
     *  may contain only those warehouses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Warehouses</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.inventory.WarehouseList getWarehouses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the warehouse list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of warehouses
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.inventory.WarehouseList filterWarehousesOnViews(org.osid.inventory.WarehouseList list)
        throws org.osid.OperationFailedException {

        return (list);
    }
}
