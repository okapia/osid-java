//
// AbstractMapJournalLookupSession
//
//    A simple framework for providing a Journal lookup service
//    backed by a fixed collection of journals.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.journaling.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Journal lookup service backed by a
 *  fixed collection of journals. The journals are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Journals</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapJournalLookupSession
    extends net.okapia.osid.jamocha.journaling.spi.AbstractJournalLookupSession
    implements org.osid.journaling.JournalLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.journaling.Journal> journals = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.journaling.Journal>());


    /**
     *  Makes a <code>Journal</code> available in this session.
     *
     *  @param  journal a journal
     *  @throws org.osid.NullArgumentException <code>journal<code>
     *          is <code>null</code>
     */

    protected void putJournal(org.osid.journaling.Journal journal) {
        this.journals.put(journal.getId(), journal);
        return;
    }


    /**
     *  Makes an array of journals available in this session.
     *
     *  @param  journals an array of journals
     *  @throws org.osid.NullArgumentException <code>journals<code>
     *          is <code>null</code>
     */

    protected void putJournals(org.osid.journaling.Journal[] journals) {
        putJournals(java.util.Arrays.asList(journals));
        return;
    }


    /**
     *  Makes a collection of journals available in this session.
     *
     *  @param  journals a collection of journals
     *  @throws org.osid.NullArgumentException <code>journals<code>
     *          is <code>null</code>
     */

    protected void putJournals(java.util.Collection<? extends org.osid.journaling.Journal> journals) {
        for (org.osid.journaling.Journal journal : journals) {
            this.journals.put(journal.getId(), journal);
        }

        return;
    }


    /**
     *  Removes a Journal from this session.
     *
     *  @param  journalId the <code>Id</code> of the journal
     *  @throws org.osid.NullArgumentException <code>journalId<code> is
     *          <code>null</code>
     */

    protected void removeJournal(org.osid.id.Id journalId) {
        this.journals.remove(journalId);
        return;
    }


    /**
     *  Gets the <code>Journal</code> specified by its <code>Id</code>.
     *
     *  @param  journalId <code>Id</code> of the <code>Journal</code>
     *  @return the journal
     *  @throws org.osid.NotFoundException <code>journalId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>journalId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.Journal getJournal(org.osid.id.Id journalId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.journaling.Journal journal = this.journals.get(journalId);
        if (journal == null) {
            throw new org.osid.NotFoundException("journal not found: " + journalId);
        }

        return (journal);
    }


    /**
     *  Gets all <code>Journals</code>. In plenary mode, the returned
     *  list contains all known journals or an error
     *  results. Otherwise, the returned list may contain only those
     *  journals that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Journals</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalList getJournals()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.journaling.journal.ArrayJournalList(this.journals.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.journals.clear();
        super.close();
        return;
    }
}
