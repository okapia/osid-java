//
// AbstractNodeAntimatroidHierarchySession.java
//
//     Defines an Antimatroid hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.sequencing.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an antimatroid hierarchy session for delivering a hierarchy
 *  of antimatroids using the AntimatroidNode interface.
 */

public abstract class AbstractNodeAntimatroidHierarchySession
    extends net.okapia.osid.jamocha.sequencing.spi.AbstractAntimatroidHierarchySession
    implements org.osid.sequencing.AntimatroidHierarchySession {

    private java.util.Collection<org.osid.sequencing.AntimatroidNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root antimatroid <code> Ids </code> in this hierarchy.
     *
     *  @return the root antimatroid <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootAntimatroidIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.sequencing.antimatroidnode.AntimatroidNodeToIdList(this.roots));
    }


    /**
     *  Gets the root antimatroids in the antimatroid hierarchy. A node
     *  with no parents is an orphan. While all antimatroid <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root antimatroids 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidList getRootAntimatroids()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.sequencing.antimatroidnode.AntimatroidNodeToAntimatroidList(new net.okapia.osid.jamocha.sequencing.antimatroidnode.ArrayAntimatroidNodeList(this.roots)));
    }


    /**
     *  Adds a root antimatroid node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootAntimatroid(org.osid.sequencing.AntimatroidNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root antimatroid nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootAntimatroids(java.util.Collection<org.osid.sequencing.AntimatroidNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root antimatroid node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootAntimatroid(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.sequencing.AntimatroidNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Antimatroid </code> has any parents. 
     *
     *  @param  antimatroidId an antimatroid <code> Id </code> 
     *  @return <code> true </code> if the antimatroid has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> antimatroidId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> antimatroidId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentAntimatroids(org.osid.id.Id antimatroidId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getAntimatroidNode(antimatroidId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of an
     *  antimatroid.
     *
     *  @param  id an <code> Id </code> 
     *  @param  antimatroidId the <code> Id </code> of an antimatroid 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> antimatroidId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> antimatroidId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> antimatroidId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfAntimatroid(org.osid.id.Id id, org.osid.id.Id antimatroidId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.sequencing.AntimatroidNodeList parents = getAntimatroidNode(antimatroidId).getParentAntimatroidNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextAntimatroidNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given antimatroid. 
     *
     *  @param  antimatroidId an antimatroid <code> Id </code> 
     *  @return the parent <code> Ids </code> of the antimatroid 
     *  @throws org.osid.NotFoundException <code> antimatroidId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> antimatroidId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentAntimatroidIds(org.osid.id.Id antimatroidId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.sequencing.antimatroid.AntimatroidToIdList(getParentAntimatroids(antimatroidId)));
    }


    /**
     *  Gets the parents of the given antimatroid. 
     *
     *  @param  antimatroidId the <code> Id </code> to query 
     *  @return the parents of the antimatroid 
     *  @throws org.osid.NotFoundException <code> antimatroidId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> antimatroidId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidList getParentAntimatroids(org.osid.id.Id antimatroidId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.sequencing.antimatroidnode.AntimatroidNodeToAntimatroidList(getAntimatroidNode(antimatroidId).getParentAntimatroidNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of an
     *  antimatroid.
     *
     *  @param  id an <code> Id </code> 
     *  @param  antimatroidId the Id of an antimatroid 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> antimatroidId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> antimatroidId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> antimatroidId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfAntimatroid(org.osid.id.Id id, org.osid.id.Id antimatroidId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfAntimatroid(id, antimatroidId)) {
            return (true);
        }

        try (org.osid.sequencing.AntimatroidList parents = getParentAntimatroids(antimatroidId)) {
            while (parents.hasNext()) {
                if (isAncestorOfAntimatroid(id, parents.getNextAntimatroid().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if an antimatroid has any children. 
     *
     *  @param  antimatroidId an antimatroid <code> Id </code> 
     *  @return <code> true </code> if the <code> antimatroidId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> antimatroidId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> antimatroidId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildAntimatroids(org.osid.id.Id antimatroidId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getAntimatroidNode(antimatroidId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of an
     *  antimatroid.
     *
     *  @param  id an <code> Id </code> 
     *  @param antimatroidId the <code> Id </code> of an 
     *         antimatroid
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> antimatroidId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> antimatroidId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> antimatroidId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfAntimatroid(org.osid.id.Id id, org.osid.id.Id antimatroidId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfAntimatroid(antimatroidId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  antimatroid.
     *
     *  @param  antimatroidId the <code> Id </code> to query 
     *  @return the children of the antimatroid 
     *  @throws org.osid.NotFoundException <code> antimatroidId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> antimatroidId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildAntimatroidIds(org.osid.id.Id antimatroidId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.sequencing.antimatroid.AntimatroidToIdList(getChildAntimatroids(antimatroidId)));
    }


    /**
     *  Gets the children of the given antimatroid. 
     *
     *  @param  antimatroidId the <code> Id </code> to query 
     *  @return the children of the antimatroid 
     *  @throws org.osid.NotFoundException <code> antimatroidId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> antimatroidId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidList getChildAntimatroids(org.osid.id.Id antimatroidId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.sequencing.antimatroidnode.AntimatroidNodeToAntimatroidList(getAntimatroidNode(antimatroidId).getChildAntimatroidNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of an
     *  antimatroid.
     *
     *  @param  id an <code> Id </code> 
     *  @param antimatroidId the <code> Id </code> of an 
     *         antimatroid
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> antimatroidId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> antimatroidId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> antimatroidId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfAntimatroid(org.osid.id.Id id, org.osid.id.Id antimatroidId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfAntimatroid(antimatroidId, id)) {
            return (true);
        }

        try (org.osid.sequencing.AntimatroidList children = getChildAntimatroids(antimatroidId)) {
            while (children.hasNext()) {
                if (isDescendantOfAntimatroid(id, children.getNextAntimatroid().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  antimatroid.
     *
     *  @param  antimatroidId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified antimatroid node 
     *  @throws org.osid.NotFoundException <code> antimatroidId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> antimatroidId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getAntimatroidNodeIds(org.osid.id.Id antimatroidId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.sequencing.antimatroidnode.AntimatroidNodeToNode(getAntimatroidNode(antimatroidId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given antimatroid.
     *
     *  @param  antimatroidId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified antimatroid node 
     *  @throws org.osid.NotFoundException <code> antimatroidId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> antimatroidId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidNode getAntimatroidNodes(org.osid.id.Id antimatroidId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getAntimatroidNode(antimatroidId));
    }


    /**
     *  Closes this <code>AntimatroidHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets an antimatroid node.
     *
     *  @param antimatroidId the id of the antimatroid node
     *  @throws org.osid.NotFoundException <code>antimatroidId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>antimatroidId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.sequencing.AntimatroidNode getAntimatroidNode(org.osid.id.Id antimatroidId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(antimatroidId, "antimatroid Id");
        for (org.osid.sequencing.AntimatroidNode antimatroid : this.roots) {
            if (antimatroid.getId().equals(antimatroidId)) {
                return (antimatroid);
            }

            org.osid.sequencing.AntimatroidNode r = findAntimatroid(antimatroid, antimatroidId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(antimatroidId + " is not found");
    }


    protected org.osid.sequencing.AntimatroidNode findAntimatroid(org.osid.sequencing.AntimatroidNode node, 
                                                                  org.osid.id.Id antimatroidId) 
        throws org.osid.OperationFailedException {

        try (org.osid.sequencing.AntimatroidNodeList children = node.getChildAntimatroidNodes()) {
            while (children.hasNext()) {
                org.osid.sequencing.AntimatroidNode antimatroid = children.getNextAntimatroidNode();
                if (antimatroid.getId().equals(antimatroidId)) {
                    return (antimatroid);
                }
                
                antimatroid = findAntimatroid(antimatroid, antimatroidId);
                if (antimatroid != null) {
                    return (antimatroid);
                }
            }
        }

        return (null);
    }
}
