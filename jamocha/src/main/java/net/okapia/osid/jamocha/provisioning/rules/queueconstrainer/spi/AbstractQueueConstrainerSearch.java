//
// AbstractQueueConstrainerSearch.java
//
//     A template for making a QueueConstrainer Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.queueconstrainer.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing queue constrainer searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractQueueConstrainerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.provisioning.rules.QueueConstrainerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.provisioning.rules.records.QueueConstrainerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.provisioning.rules.QueueConstrainerSearchOrder queueConstrainerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of queue constrainers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  queueConstrainerIds list of queue constrainers
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongQueueConstrainers(org.osid.id.IdList queueConstrainerIds) {
        while (queueConstrainerIds.hasNext()) {
            try {
                this.ids.add(queueConstrainerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongQueueConstrainers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of queue constrainer Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getQueueConstrainerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  queueConstrainerSearchOrder queue constrainer search order 
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>queueConstrainerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderQueueConstrainerResults(org.osid.provisioning.rules.QueueConstrainerSearchOrder queueConstrainerSearchOrder) {
	this.queueConstrainerSearchOrder = queueConstrainerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.provisioning.rules.QueueConstrainerSearchOrder getQueueConstrainerSearchOrder() {
	return (this.queueConstrainerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given queue constrainer search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a queue constrainer implementing the requested record.
     *
     *  @param queueConstrainerSearchRecordType a queue constrainer search record
     *         type
     *  @return the queue constrainer search record
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(queueConstrainerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.QueueConstrainerSearchRecord getQueueConstrainerSearchRecord(org.osid.type.Type queueConstrainerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.provisioning.rules.records.QueueConstrainerSearchRecord record : this.records) {
            if (record.implementsRecordType(queueConstrainerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(queueConstrainerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this queue constrainer search. 
     *
     *  @param queueConstrainerSearchRecord queue constrainer search record
     *  @param queueConstrainerSearchRecordType queueConstrainer search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addQueueConstrainerSearchRecord(org.osid.provisioning.rules.records.QueueConstrainerSearchRecord queueConstrainerSearchRecord, 
                                           org.osid.type.Type queueConstrainerSearchRecordType) {

        addRecordType(queueConstrainerSearchRecordType);
        this.records.add(queueConstrainerSearchRecord);        
        return;
    }
}
