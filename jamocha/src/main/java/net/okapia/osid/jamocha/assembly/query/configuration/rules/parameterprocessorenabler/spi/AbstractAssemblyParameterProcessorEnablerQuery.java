//
// AbstractAssemblyParameterProcessorEnablerQuery.java
//
//     A ParameterProcessorEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.configuration.rules.parameterprocessorenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ParameterProcessorEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblyParameterProcessorEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.configuration.rules.ParameterProcessorEnablerQuery,
               org.osid.configuration.rules.ParameterProcessorEnablerQueryInspector,
               org.osid.configuration.rules.ParameterProcessorEnablerSearchOrder {

    private final java.util.Collection<org.osid.configuration.rules.records.ParameterProcessorEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.configuration.rules.records.ParameterProcessorEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.configuration.rules.records.ParameterProcessorEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyParameterProcessorEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyParameterProcessorEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to the parameter processor. 
     *
     *  @param  parameterProcessorId the parameter processor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> parameterProcessorId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledParameterProcessorId(org.osid.id.Id parameterProcessorId, 
                                               boolean match) {
        getAssembler().addIdTerm(getRuledParameterProcessorIdColumn(), parameterProcessorId, match);
        return;
    }


    /**
     *  Clears the parameter processor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledParameterProcessorIdTerms() {
        getAssembler().clearTerms(getRuledParameterProcessorIdColumn());
        return;
    }


    /**
     *  Gets the parameter processor <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledParameterProcessorIdTerms() {
        return (getAssembler().getIdTerms(getRuledParameterProcessorIdColumn()));
    }


    /**
     *  Gets the RuledParameterProcessorId column name.
     *
     * @return the column name
     */

    protected String getRuledParameterProcessorIdColumn() {
        return ("ruled_parameter_processor_id");
    }


    /**
     *  Tests if a <code> ParameterProcessorQuery </code> is available. 
     *
     *  @return <code> true </code> if a parameter processor query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledParameterProcessorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a parameter processor. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the parameter processor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledParameterProcessorQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorQuery getRuledParameterProcessorQuery() {
        throw new org.osid.UnimplementedException("supportsRuledParameterProcessorQuery() is false");
    }


    /**
     *  Matches enablers mapped to any parameter processor. 
     *
     *  @param  match <code> true </code> for enablers mapped to any parameter 
     *          processor, <code> false </code> to match enablers mapped to no 
     *          parameter processors 
     */

    @OSID @Override
    public void matchAnyRuledParameterProcessor(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledParameterProcessorColumn(), match);
        return;
    }


    /**
     *  Clears the parameter processor query terms. 
     */

    @OSID @Override
    public void clearRuledParameterProcessorTerms() {
        getAssembler().clearTerms(getRuledParameterProcessorColumn());
        return;
    }


    /**
     *  Gets the parameter processor query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorQueryInspector[] getRuledParameterProcessorTerms() {
        return (new org.osid.configuration.rules.ParameterProcessorQueryInspector[0]);
    }


    /**
     *  Gets the RuledParameterProcessor column name.
     *
     * @return the column name
     */

    protected String getRuledParameterProcessorColumn() {
        return ("ruled_parameter_processor");
    }


    /**
     *  Matches enablers mapped to the configuration. 
     *
     *  @param  configurationId the configuration <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchConfigurationId(org.osid.id.Id configurationId, 
                                     boolean match) {
        getAssembler().addIdTerm(getConfigurationIdColumn(), configurationId, match);
        return;
    }


    /**
     *  Clears the configuration <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearConfigurationIdTerms() {
        getAssembler().clearTerms(getConfigurationIdColumn());
        return;
    }


    /**
     *  Gets the configuration <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getConfigurationIdTerms() {
        return (getAssembler().getIdTerms(getConfigurationIdColumn()));
    }


    /**
     *  Gets the ConfigurationId column name.
     *
     * @return the column name
     */

    protected String getConfigurationIdColumn() {
        return ("configuration_id");
    }


    /**
     *  Tests if a <code> ConfigurationQuery </code> is available. 
     *
     *  @return <code> true </code> if a configuration query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConfigurationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a configuration. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the configuration query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConfigurationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationQuery getConfigurationQuery() {
        throw new org.osid.UnimplementedException("supportsConfigurationQuery() is false");
    }


    /**
     *  Clears the configuration query terms. 
     */

    @OSID @Override
    public void clearConfigurationTerms() {
        getAssembler().clearTerms(getConfigurationColumn());
        return;
    }


    /**
     *  Gets the configuration query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationQueryInspector[] getConfigurationTerms() {
        return (new org.osid.configuration.ConfigurationQueryInspector[0]);
    }


    /**
     *  Gets the Configuration column name.
     *
     * @return the column name
     */

    protected String getConfigurationColumn() {
        return ("configuration");
    }


    /**
     *  Tests if this parameterProcessorEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  parameterProcessorEnablerRecordType a parameter processor enabler record type 
     *  @return <code>true</code> if the parameterProcessorEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>parameterProcessorEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type parameterProcessorEnablerRecordType) {
        for (org.osid.configuration.rules.records.ParameterProcessorEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(parameterProcessorEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  parameterProcessorEnablerRecordType the parameter processor enabler record type 
     *  @return the parameter processor enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>parameterProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(parameterProcessorEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.configuration.rules.records.ParameterProcessorEnablerQueryRecord getParameterProcessorEnablerQueryRecord(org.osid.type.Type parameterProcessorEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.configuration.rules.records.ParameterProcessorEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(parameterProcessorEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(parameterProcessorEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  parameterProcessorEnablerRecordType the parameter processor enabler record type 
     *  @return the parameter processor enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>parameterProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(parameterProcessorEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.configuration.rules.records.ParameterProcessorEnablerQueryInspectorRecord getParameterProcessorEnablerQueryInspectorRecord(org.osid.type.Type parameterProcessorEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.configuration.rules.records.ParameterProcessorEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(parameterProcessorEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(parameterProcessorEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param parameterProcessorEnablerRecordType the parameter processor enabler record type
     *  @return the parameter processor enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>parameterProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(parameterProcessorEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.configuration.rules.records.ParameterProcessorEnablerSearchOrderRecord getParameterProcessorEnablerSearchOrderRecord(org.osid.type.Type parameterProcessorEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.configuration.rules.records.ParameterProcessorEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(parameterProcessorEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(parameterProcessorEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this parameter processor enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param parameterProcessorEnablerQueryRecord the parameter processor enabler query record
     *  @param parameterProcessorEnablerQueryInspectorRecord the parameter processor enabler query inspector
     *         record
     *  @param parameterProcessorEnablerSearchOrderRecord the parameter processor enabler search order record
     *  @param parameterProcessorEnablerRecordType parameter processor enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>parameterProcessorEnablerQueryRecord</code>,
     *          <code>parameterProcessorEnablerQueryInspectorRecord</code>,
     *          <code>parameterProcessorEnablerSearchOrderRecord</code> or
     *          <code>parameterProcessorEnablerRecordTypeparameterProcessorEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addParameterProcessorEnablerRecords(org.osid.configuration.rules.records.ParameterProcessorEnablerQueryRecord parameterProcessorEnablerQueryRecord, 
                                      org.osid.configuration.rules.records.ParameterProcessorEnablerQueryInspectorRecord parameterProcessorEnablerQueryInspectorRecord, 
                                      org.osid.configuration.rules.records.ParameterProcessorEnablerSearchOrderRecord parameterProcessorEnablerSearchOrderRecord, 
                                      org.osid.type.Type parameterProcessorEnablerRecordType) {

        addRecordType(parameterProcessorEnablerRecordType);

        nullarg(parameterProcessorEnablerQueryRecord, "parameter processor enabler query record");
        nullarg(parameterProcessorEnablerQueryInspectorRecord, "parameter processor enabler query inspector record");
        nullarg(parameterProcessorEnablerSearchOrderRecord, "parameter processor enabler search odrer record");

        this.queryRecords.add(parameterProcessorEnablerQueryRecord);
        this.queryInspectorRecords.add(parameterProcessorEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(parameterProcessorEnablerSearchOrderRecord);
        
        return;
    }
}
