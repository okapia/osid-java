//
// AbstractInstallationBatchManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.installation.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractInstallationBatchManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.installation.batch.InstallationBatchManager,
               org.osid.installation.batch.InstallationBatchProxyManager {


    /**
     *  Constructs a new
     *  <code>AbstractInstallationBatchManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractInstallationBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if bulk administration of packages is available. 
     *
     *  @return <code> true </code> if a package bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPackageBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of depots is available. 
     *
     *  @return <code> true </code> if a depot bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDepotBatchAdmin() {
        return (false);
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk package 
     *  administration service. 
     *
     *  @return a <code> PackageBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPackageBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.batch.PackageBatchAdminSession getPackageBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.batch.InstallationBatchManager.getPackageBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk package 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PackageBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPackageBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.batch.PackageBatchAdminSession getPackageBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.batch.InstallationBatchProxyManager.getPackageBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk package 
     *  administration service for the given depot. 
     *
     *  @param  depotId the <code> Id </code> of the <code> Depot </code> 
     *  @return a <code> PackageBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Depot </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> depotId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPackageBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.batch.PackageBatchAdminSession getPackageBatchAdminSessionForDepot(org.osid.id.Id depotId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.batch.InstallationBatchManager.getPackageBatchAdminSessionForDepot not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk package 
     *  administration service for the given depot. 
     *
     *  @param  depotId the <code> Id </code> of the <code> Depot </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PackageBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Depot </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> depotId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPackageBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.batch.PackageBatchAdminSession getPackageBatchAdminSessionForDepot(org.osid.id.Id depotId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.batch.InstallationBatchProxyManager.getPackageBatchAdminSessionForDepot not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk depot 
     *  administration service. 
     *
     *  @return a <code> DepotBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDepotBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.batch.DepotBatchAdminSession getDepotBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.batch.InstallationBatchManager.getDepotBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk depot 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DepotBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDepotBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.batch.DepotBatchAdminSession getDepotBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.batch.InstallationBatchProxyManager.getDepotBatchAdminSession not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        return;
    }
}
