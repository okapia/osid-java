//
// AbstractMutableLocale.java
//
//     Defines a mutable Locale.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.locale.locale.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Locale</code>.
 */

public abstract class AbstractMutableLocale
    extends net.okapia.osid.jamocha.locale.locale.spi.AbstractLocale
    implements org.osid.locale.Locale,
               net.okapia.osid.jamocha.builder.locale.locale.LocaleMiter {


    /**
     *  Sets the language <code>Type</code>.
     *
     *  @param type the language type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    @Override
    public void setLanguageType(org.osid.type.Type type) {
        super.setLanguageType(type);
        return;
    }


    /**
     *  Sets the script <code>Type</code>.
     *
     *  @param type the script type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    @Override
    public void setScriptType(org.osid.type.Type type) {
        super.setScriptType(type);
        return;
    }


    /**
     *  Sets the calendar <code>Type</code>.
     *
     *  @param type the calendar type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    @Override
    public void setCalendarType(org.osid.type.Type type) {
        super.setCalendarType(type);
        return;
    }


    /**
     *  Sets the time <code>Type</code>.
     *
     *  @param type the time type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    @Override
    public void setTimeType(org.osid.type.Type type) {
        super.setTimeType(type);
        return;
    }


    /**
     *  Sets the currency <code>Type</code>.
     *
     *  @param type the currency type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    @Override
    public void setCurrencyType(org.osid.type.Type type) {
        super.setCurrencyType(type);
        return;
    }


    /**
     *  Sets the unit system <code>Type</code>.
     *
     *  @param type the unit system type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    @Override
    public void setUnitSystemType(org.osid.type.Type type) {
        super.setUnitSystemType(type);
        return;
    }


    /**
     *  Sets the numeric format <code>Type</code>.
     *
     *  @param type the numeric format type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    @Override
    public void setNumericFormatType(org.osid.type.Type type) {
        super.setNumericFormatType(type);
        return;
    }


    /**
     *  Sets the calendar format <code>Type</code>.
     *
     *  @param type the calendar format type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    @Override
    public void setCalendarFormatType(org.osid.type.Type type) {
        super.setCalendarFormatType(type);
        return;
    }


    /**
     *  Sets the time format <code>Type</code>.
     *
     *  @param type the time format type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    @Override
    public void setTimeFormatType(org.osid.type.Type type) {
        super.setTimeFormatType(type);
        return;
    }


    /**
     *  Sets the currency format <code>Type</code>.
     *
     *  @param type the currency format type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    @Override
    public void setCurrencyFormatType(org.osid.type.Type type) {
        super.setCurrencyFormatType(type);
        return;
    }


    /**
     *  Sets the coordinate format <code>Type</code>.
     *
     *  @param type the coordinate format type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    @Override
    public void setCoordinateFormatType(org.osid.type.Type type) {
        super.setCoordinateFormatType(type);
        return;
    }
}

