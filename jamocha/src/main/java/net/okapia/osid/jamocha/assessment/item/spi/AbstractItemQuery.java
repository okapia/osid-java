//
// AbstractItemQuery.java
//
//     A template for making an Item Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.item.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for items.
 */

public abstract class AbstractItemQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.assessment.ItemQuery {

    private final java.util.Collection<org.osid.assessment.records.ItemQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the learning objective <code> Id </code> for this query. 
     *
     *  @param  objectiveId a learning objective <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> objectiveId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLearningObjectiveId(org.osid.id.Id objectiveId, 
                                         boolean match) {
        return;
    }


    /**
     *  Clears all learning objective <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearLearningObjectiveIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> ObjectiveQuery </code> is available. 
     *
     *  @return <code> true </code> if a learning objective query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLearningObjectiveQuery() {
        return (false);
    }


    /**
     *  Gets the query for a learning objective. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the learning objective query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLearningObjectiveQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQuery getLearningObjectiveQuery() {
        throw new org.osid.UnimplementedException("supportsLearningObjectiveQuery() is false");
    }


    /**
     *  Matches an item with any objective. 
     *
     *  @param  match <code> true </code> to match items with any learning 
     *          objective, <code> false </code> to match items with no 
     *          learning objectives 
     */

    @OSID @Override
    public void matchAnyLearningObjective(boolean match) {
        return;
    }


    /**
     *  Clears all learning objective terms. 
     */

    @OSID @Override
    public void clearLearningObjectiveTerms() {
        return;
    }


    /**
     *  Sets the question <code> Id </code> for this query. 
     *
     *  @param  questionId a question <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> questionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchQuestionId(org.osid.id.Id questionId, boolean match) {
        return;
    }


    /**
     *  Clears all question <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearQuestionIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> QuestionQuery </code> is available. 
     *
     *  @return <code> true </code> if a question query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQuestionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a question. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the question query 
     *  @throws org.osid.UnimplementedException <code> supportsQuestionQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.QuestionQuery getQuestionQuery() {
        throw new org.osid.UnimplementedException("supportsQuestionQuery() is false");
    }


    /**
     *  Matches an item with any question. 
     *
     *  @param  match <code> true </code> to match items with any question, 
     *          <code> false </code> to match items with no questions 
     */

    @OSID @Override
    public void matchAnyQuestion(boolean match) {
        return;
    }


    /**
     *  Clears all question terms. 
     */

    @OSID @Override
    public void clearQuestionTerms() {
        return;
    }


    /**
     *  Sets the answer <code> Id </code> for this query. 
     *
     *  @param  answerId an answer <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> answerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAnswerId(org.osid.id.Id answerId, boolean match) {
        return;
    }


    /**
     *  Clears all answer <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAnswerIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AnswerQuery </code> is available. 
     *
     *  @return <code> true </code> if an answer query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAnswerQuery() {
        return (false);
    }


    /**
     *  Gets the query for an answer. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the answer query 
     *  @throws org.osid.UnimplementedException <code> supportsAnswerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AnswerQuery getAnswerQuery() {
        throw new org.osid.UnimplementedException("supportsAnswerQuery() is false");
    }


    /**
     *  Matches an item with any answer. 
     *
     *  @param  match <code> true </code> to match items with any answer, 
     *          <code> false </code> to match items with no answers 
     */

    @OSID @Override
    public void matchAnyAnswer(boolean match) {
        return;
    }


    /**
     *  Clears all answer terms. 
     */

    @OSID @Override
    public void clearAnswerTerms() {
        return;
    }


    /**
     *  Sets the assessment <code> Id </code> for this query. 
     *
     *  @param  assessmentId an assessment <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAssessmentId(org.osid.id.Id assessmentId, boolean match) {
        return;
    }


    /**
     *  Clears all assessment <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAssessmentIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AssessmentQuery </code> is available. 
     *
     *  @return <code> true </code> if an assessment query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an assessment. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the assessment query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentQuery getAssessmentQuery() {
        throw new org.osid.UnimplementedException("supportsAssessmentQuery() is false");
    }


    /**
     *  Matches an item with any assessment. 
     *
     *  @param  match <code> true </code> to match items with any assessment, 
     *          <code> false </code> to match items with no assessments 
     */

    @OSID @Override
    public void matchAnyAssessment(boolean match) {
        return;
    }


    /**
     *  Clears all assessment terms. 
     */

    @OSID @Override
    public void clearAssessmentTerms() {
        return;
    }


    /**
     *  Sets the bank <code> Id </code> for this query. 
     *
     *  @param  bankId a bank <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchBankId(org.osid.id.Id bankId, boolean match) {
        return;
    }


    /**
     *  Clears all bank <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBankIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BankQuery </code> is available. 
     *
     *  @return <code> true </code> if a bank query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBankQuery() {
        return (false);
    }


    /**
     *  Gets the query for a bank. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the bank query 
     *  @throws org.osid.UnimplementedException <code> supportsBankQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.BankQuery getBankQuery() {
        throw new org.osid.UnimplementedException("supportsBankQuery() is false");
    }


    /**
     *  Clears all bank terms. 
     */

    @OSID @Override
    public void clearBankTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given item query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an item implementing the requested record.
     *
     *  @param itemRecordType an item record type
     *  @return the item query record
     *  @throws org.osid.NullArgumentException
     *          <code>itemRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(itemRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.ItemQueryRecord getItemQueryRecord(org.osid.type.Type itemRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.records.ItemQueryRecord record : this.records) {
            if (record.implementsRecordType(itemRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(itemRecordType + " is not supported");
    }


    /**
     *  Adds a record to this item query. 
     *
     *  @param itemQueryRecord item query record
     *  @param itemRecordType item record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addItemQueryRecord(org.osid.assessment.records.ItemQueryRecord itemQueryRecord, 
                                          org.osid.type.Type itemRecordType) {

        addRecordType(itemRecordType);
        nullarg(itemQueryRecord, "item query record");
        this.records.add(itemQueryRecord);        
        return;
    }
}
