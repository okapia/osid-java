//
// AbstractOsidObjectQueryInspector.java
//
//     Defines an OsidObjectQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 1 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.query.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a simple OsidQueryInspector to extend. 
 */

public abstract class AbstractOsidObjectQueryInspector
    extends AbstractOsidIdentifiableQueryInspector
    implements org.osid.OsidObjectQueryInspector {
    
    private final java.util.Collection<org.osid.search.terms.StringTerm> displayNameTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.search.terms.StringTerm> descriptionTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.search.terms.TypeTerm> genusTypeTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.search.terms.TypeTerm> parentGenusTypeTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.search.terms.IdTerm> subjectIdTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.search.terms.IdTerm> stateIdTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.search.terms.IdTerm> commentIdTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.search.terms.IdTerm> journalEntryIdTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.search.terms.IdTerm> creditIdTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.search.terms.IdTerm> relationshipIdTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.search.terms.IdTerm> relationshipPeerIdTerms = new java.util.LinkedHashSet<>();

    private final java.util.Collection<org.osid.ontology.SubjectQueryInspector> subjectTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.ontology.RelevancyQueryInspector> relevancyTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.process.StateQueryInspector> stateTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.commenting.CommentQueryInspector> commentTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.journaling.JournalEntryQueryInspector> journalEntryTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.metering.StatisticQueryInspector> statisticTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.acknowledgement.CreditQueryInspector> creditTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.relationship.RelationshipQueryInspector> relationshipTerms = new java.util.LinkedHashSet<>();

    private final BrowsableQueryInspector browsable = new BrowsableQueryInspector();


    /**
     *  Gets the display name query terms.
     *
     *  @return the display name terms
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getDisplayNameTerms() {
        return (this.displayNameTerms.toArray(new org.osid.search.terms.StringTerm[this.displayNameTerms.size()]));
    }

    
    /**
     *  Adds a display name term.
     *
     *  @param term a display name term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addDisplayNameTerm(org.osid.search.terms.StringTerm term) {
        nullarg(term, "display name term");
        this.displayNameTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of display name terms.
     *
     *  @param terms a collection of display name terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addDisplayNameTerms(java.util.Collection<org.osid.search.terms.StringTerm> terms) {
        nullarg(terms, "display name terms");
        this.displayNameTerms.addAll(terms);
        return;
    }


    /**
     *  Adds a display name term.
     *
     *  @param displayName the display name
     *  @param stringMatchType a string match type
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>id</code> or
     *          <code>stringMatchType</code> is <code>null</code>
     */

    protected void addDisplayNameTerm(String displayName, org.osid.type.Type stringMatchType, boolean match) {
        this.displayNameTerms.add(new net.okapia.osid.primordium.terms.StringTerm(displayName, 
                                                                                  stringMatchType, 
                                                                                  match));
        return;
    }


    /**
     *  Gets the description query terms.
     *
     *  @return the description terms
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getDescriptionTerms() {
        return (this.descriptionTerms.toArray(new org.osid.search.terms.StringTerm[this.descriptionTerms.size()]));
    }

    
    /**
     *  Adds a description term.
     *
     *  @param term a description term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addDescriptionTerm(org.osid.search.terms.StringTerm term) {
        nullarg(term, "description term");
        this.descriptionTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of description terms.
     *
     *  @param terms a collection of description terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addDescriptionTerms(java.util.Collection<org.osid.search.terms.StringTerm> terms) {
        nullarg(terms, "description terms");
        this.descriptionTerms.addAll(terms);
        return;
    }


    /**
     *  Adds a description term.
     *
     *  @param description the description
     *  @param stringMatchType a string match type
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>id</code> or
     *          <code>stringMatchType</code> is <code>null</code>
     */

    protected void addDescriptionTerm(String description, org.osid.type.Type stringMatchType, boolean match) {
        this.descriptionTerms.add(new net.okapia.osid.primordium.terms.StringTerm(description, 
                                                                                  stringMatchType, 
                                                                                  match));
        return;
    }


    /**
     *  Gets the genus type query terms.
     *
     *  @return the genus type terms
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getGenusTypeTerms() {
        return (this.genusTypeTerms.toArray(new org.osid.search.terms.TypeTerm[this.genusTypeTerms.size()]));
    }

    
    /**
     *  Adds a genus type term.
     *
     *  @param term a genus type term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addGenusTypeTerm(org.osid.search.terms.TypeTerm term) {
        nullarg(term, "type term");
        this.genusTypeTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of genus type terms.
     *
     *  @param terms a collection of type terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addGenusTypeTerms(java.util.Collection<org.osid.search.terms.TypeTerm> terms) {
        nullarg(terms, "type terms");
        this.genusTypeTerms.addAll(terms);
        return;
    }


    /**
     *  Adds a gennus type term.
     *
     *  @param type the genus type
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    protected void addGenusTypeTerm(org.osid.type.Type type, boolean match) {
        this.genusTypeTerms.add(new net.okapia.osid.primordium.terms.TypeTerm(type, match));
        return;
    }


    /**
     *  Gets the parent genus type query terms.
     *
     *  @return the parent genus type terms
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getParentGenusTypeTerms() {
        return (this.parentGenusTypeTerms.toArray(new org.osid.search.terms.TypeTerm[this.parentGenusTypeTerms.size()]));
    }

    
    /**
     *  Adds a parent genus type term.
     *
     *  @param term a parent genus type term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addParentGenusTypeTerm(org.osid.search.terms.TypeTerm term) {
        nullarg(term, "type term");
        this.parentGenusTypeTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of parent genus type terms.
     *
     *  @param terms a collection of type terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addParentGenusTypeTerms(java.util.Collection<org.osid.search.terms.TypeTerm> terms) {
        nullarg(terms, "type terms");
        this.parentGenusTypeTerms.addAll(terms);
        return;
    }


    /**
     *  Adds a parent genus type term.
     *
     *  @param type the type
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    protected void addParentGenusTypeTerm(org.osid.type.Type type, boolean match) {
        this.parentGenusTypeTerms.add(new net.okapia.osid.primordium.terms.TypeTerm(type, match));
        return;
    }


    /**
     *  Gets the subject <code>Id</code> query terms.
     *
     *  @return the subject <code>Id</code> terms
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSubjectIdTerms() {
        return (this.subjectIdTerms.toArray(new org.osid.search.terms.IdTerm[this.subjectIdTerms.size()]));
    }

    
    /**
     *  Adds a subject <code>Id</code> term.
     *
     *  @param term a subject <code>Id</code> term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addSubjectIdTerm(org.osid.search.terms.IdTerm term) {
        nullarg(term, "subject Id term");
        this.subjectIdTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of subject <code>Id</code> terms.
     *
     *  @param terms a collection of subject <code>Id</code> terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addSubjectIdTerms(java.util.Collection<org.osid.search.terms.IdTerm> terms) {
        nullarg(terms, "subject Id terms");
        this.subjectIdTerms.addAll(terms);
        return;
    }


    /**
     *  Adds a subject <code>Id</code> term.
     *
     *  @param subjectId the subject <code>Id</code>
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>subjectId</code>
     *          or is <code>null</code>
     */

    protected void addSubjectIdTerm(org.osid.id.Id subjectId, boolean match) {
        this.subjectIdTerms.add(new net.okapia.osid.primordium.terms.IdTerm(subjectId, match));
        return;
    }


    /**
     *  Gets the subject query terms.
     *
     *  @return the subject terms
     */

    @OSID @Override
    public org.osid.ontology.SubjectQueryInspector[] getSubjectTerms() {
        return (this.subjectTerms.toArray(new org.osid.ontology.SubjectQueryInspector[this.subjectTerms.size()]));
    }

    
    /**
     *  Adds a subject term.
     *
     *  @param term a subject term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addSubjectTerm(org.osid.ontology.SubjectQueryInspector term) {
        nullarg(term, "subject term");
        this.subjectTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of subject terms.
     *
     *  @param terms a collection of subject terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addSubjectTerms(java.util.Collection<org.osid.ontology.SubjectQueryInspector> terms) {
        nullarg(terms, "subject terms");
        this.subjectTerms.addAll(terms);
        return;
    }


    /**
     *  Gets the relevancy query terms.
     *
     *  @return the relevancy terms
     */

    @OSID @Override
    public org.osid.ontology.RelevancyQueryInspector[] getSubjectRelevancyTerms() {
        return (this.relevancyTerms.toArray(new org.osid.ontology.RelevancyQueryInspector[this.relevancyTerms.size()]));
    }

    
    /**
     *  Adds a relevancy term.
     *
     *  @param term a relevancy term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addRelevancyTerm(org.osid.ontology.RelevancyQueryInspector term) {
        nullarg(term, "relevancy term");
        this.relevancyTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of relevancy terms.
     *
     *  @param terms a collection of relevancy terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addRelevancyTerms(java.util.Collection<org.osid.ontology.RelevancyQueryInspector> terms) {
        nullarg(terms, "relevancy terms");
        this.relevancyTerms.addAll(terms);
        return;
    }


    /**
     *  Gets the state <code>Id</code> query terms.
     *
     *  @return the state <code>Id</code> terms
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStateIdTerms() {
        return (this.stateIdTerms.toArray(new org.osid.search.terms.IdTerm[this.stateIdTerms.size()]));
    }

    
    /**
     *  Adds a state <code>Id</code> term.
     *
     *  @param term a state <code>Id</code> term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addStateIdTerm(org.osid.search.terms.IdTerm term) {
        nullarg(term, "state Id term");
        this.stateIdTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of state <code>Id</code> terms.
     *
     *  @param terms a collection of state <code>Id</code> terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addStateIdTerms(java.util.Collection<org.osid.search.terms.IdTerm> terms) {
        nullarg(terms, "state Id terms");
        this.stateIdTerms.addAll(terms);
        return;
    }


    /**
     *  Adds a state <code>Id</code> term.
     *
     *  @param stateId the state <code>Id</code>
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>stateId</code>
     *          or is <code>null</code>
     */

    protected void addStateIdTerm(org.osid.id.Id stateId, boolean match) {
        this.stateIdTerms.add(new net.okapia.osid.primordium.terms.IdTerm(stateId, match));
        return;
    }


    /**
     *  Gets the state query terms.
     *
     *  @return the state terms
     */

    @OSID @Override
    public org.osid.process.StateQueryInspector[] getStateTerms() {
        return (this.stateTerms.toArray(new org.osid.process.StateQueryInspector[this.stateTerms.size()]));
    }

    
    /**
     *  Adds a state term.
     *
     *  @param term a state term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addStateTerm(org.osid.process.StateQueryInspector term) {
        nullarg(term, "state term");
        this.stateTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of state terms.
     *
     *  @param terms a collection of state terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addStateTerms(java.util.Collection<org.osid.process.StateQueryInspector> terms) {
        nullarg(terms, "state terms");
        this.stateTerms.addAll(terms);
        return;
    }


    /**
     *  Gets the comment <code>Id</code> query terms.
     *
     *  @return the comment <code>Id</code> terms
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCommentIdTerms() {
        return (this.commentIdTerms.toArray(new org.osid.search.terms.IdTerm[this.commentIdTerms.size()]));
    }

    
    /**
     *  Adds a comment <code>Id</code> term.
     *
     *  @param term a comment <code>Id</code> term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addCommentIdTerm(org.osid.search.terms.IdTerm term) {
        nullarg(term, "comment Id term");
        this.commentIdTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of comment <code>Id</code> terms.
     *
     *  @param terms a collection of comment <code>Id</code> terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addCommentIdTerms(java.util.Collection<org.osid.search.terms.IdTerm> terms) {
        nullarg(terms, "comment Id terms");
        this.commentIdTerms.addAll(terms);
        return;
    }


    /**
     *  Adds a comment <code>Id</code> term.
     *
     *  @param commentId the comment <code>Id</code>
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>commentId</code>
     *          or is <code>null</code>
     */

    protected void addCommentIdTerm(org.osid.id.Id commentId, boolean match) {
        this.commentIdTerms.add(new net.okapia.osid.primordium.terms.IdTerm(commentId, match));
        return;
    }


    /**
     *  Gets the comment query terms.
     *
     *  @return the comment terms
     */

    @OSID @Override
    public org.osid.commenting.CommentQueryInspector[] getCommentTerms() {
        return (this.commentTerms.toArray(new org.osid.commenting.CommentQueryInspector[this.commentTerms.size()]));
    }

    
    /**
     *  Adds a comment term.
     *
     *  @param term a comment term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addCommentTerm(org.osid.commenting.CommentQueryInspector term) {
        nullarg(term, "comment term");
        this.commentTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of comment terms.
     *
     *  @param terms a collection of comment terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addCommentTerms(java.util.Collection<org.osid.commenting.CommentQueryInspector> terms) {
        nullarg(terms, "comment terms");
        this.commentTerms.addAll(terms);
        return;
    }


    /**
     *  Gets the journal entry <code>Id</code> query terms.
     *
     *  @return the journal entry <code>Id</code> terms
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getJournalEntryIdTerms() {
        return (this.journalEntryIdTerms.toArray(new org.osid.search.terms.IdTerm[this.journalEntryIdTerms.size()]));
    }

    
    /**
     *  Adds a journal entry <code>Id</code> term.
     *
     *  @param term a journal entry <code>Id</code> term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addJournalEntryIdTerm(org.osid.search.terms.IdTerm term) {
        nullarg(term, "journal entry Id term");
        this.journalEntryIdTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of journal entry <code>Id</code> terms.
     *
     *  @param terms a collection of journal entry <code>Id</code> terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addJournalEntryIdTerms(java.util.Collection<org.osid.search.terms.IdTerm> terms) {
        nullarg(terms, "journal entry Id terms");
        this.journalEntryIdTerms.addAll(terms);
        return;
    }


    /**
     *  Adds a journal entry <code>Id</code> term.
     *
     *  @param journalEntryId the journal entry <code>Id</code>
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>journalEntryId</code>
     *          or is <code>null</code>
     */

    protected void addJournalEntryIdTerm(org.osid.id.Id journalEntryId, boolean match) {
        this.journalEntryIdTerms.add(new net.okapia.osid.primordium.terms.IdTerm(journalEntryId, match));
        return;
    }


    /**
     *  Gets the journal entry query terms.
     *
     *  @return the journal entry terms
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryQueryInspector[] getJournalEntryTerms() {
        return (this.journalEntryTerms.toArray(new org.osid.journaling.JournalEntryQueryInspector[this.journalEntryTerms.size()]));
    }

    
    /**
     *  Adds a journal entry term.
     *
     *  @param term a journal entry term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addJournalEntryTerm(org.osid.journaling.JournalEntryQueryInspector term) {
        nullarg(term, "journal entry term");
        this.journalEntryTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of journal entry terms.
     *
     *  @param terms a collection of journal entry terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addJournalEntryTerms(java.util.Collection<org.osid.journaling.JournalEntryQueryInspector> terms) {
        nullarg(terms, "journal entry terms");
        this.journalEntryTerms.addAll(terms);
        return;
    }
    

    /**
     *  Gets the statistic query terms.
     *
     *  @return the statistic terms
     */

    @OSID @Override
    public org.osid.metering.StatisticQueryInspector[] getStatisticTerms() {
        return (this.statisticTerms.toArray(new org.osid.metering.StatisticQueryInspector[this.statisticTerms.size()]));
    }

    
    /**
     *  Adds a statistic term.
     *
     *  @param term a statistic term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addStatisticTerm(org.osid.metering.StatisticQueryInspector term) {
        nullarg(term, "statistic term");
        this.statisticTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of statistic terms.
     *
     *  @param terms a collection of statistic terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addStatisticTerms(java.util.Collection<org.osid.metering.StatisticQueryInspector> terms) {
        nullarg(terms, "statistic terms");
        this.statisticTerms.addAll(terms);
        return;
    }


    /**
     *  Gets the credit <code>Id</code> query terms.
     *
     *  @return the credit <code>Id</code> terms
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCreditIdTerms() {
        return (this.creditIdTerms.toArray(new org.osid.search.terms.IdTerm[this.creditIdTerms.size()]));
    }

    
    /**
     *  Adds a credit <code>Id</code> term.
     *
     *  @param term a credit <code>Id</code> term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addCreditIdTerm(org.osid.search.terms.IdTerm term) {
        nullarg(term, "credit Id term");
        this.creditIdTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of credit <code>Id</code> terms.
     *
     *  @param terms a collection of credit <code>Id</code> terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addCreditIdTerms(java.util.Collection<org.osid.search.terms.IdTerm> terms) {
        nullarg(terms, "credit Id terms");
        this.creditIdTerms.addAll(terms);
        return;
    }


    /**
     *  Adds a credit <code>Id</code> term.
     *
     *  @param creditId the credit <code>Id</code>
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>creditId</code>
     *          or is <code>null</code>
     */

    protected void addCreditIdTerm(org.osid.id.Id creditId, boolean match) {
        this.creditIdTerms.add(new net.okapia.osid.primordium.terms.IdTerm(creditId, match));
        return;
    }


    /**
     *  Gets the credit query terms.
     *
     *  @return the credit terms
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditQueryInspector[] getCreditTerms() {
        return (this.creditTerms.toArray(new org.osid.acknowledgement.CreditQueryInspector[this.creditTerms.size()]));
    }

    
    /**
     *  Adds a credit term.
     *
     *  @param term a credit term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addCreditTerm(org.osid.acknowledgement.CreditQueryInspector term) {
        nullarg(term, "credit term");
        this.creditTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of credit terms.
     *
     *  @param terms a collection of credit terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addCreditTerms(java.util.Collection<org.osid.acknowledgement.CreditQueryInspector> terms) {
        nullarg(terms, "credit terms");
        this.creditTerms.addAll(terms);
        return;
    }


    /**
     *  Gets the relationship <code>Id</code> query terms.
     *
     *  @return the relationship <code>Id</code> terms
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRelationshipIdTerms() {
        return (this.relationshipIdTerms.toArray(new org.osid.search.terms.IdTerm[this.relationshipIdTerms.size()]));
    }

    
    /**
     *  Adds a relationship <code>Id</code> term.
     *
     *  @param term a relationship <code>Id</code> term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addRelationshipIdTerm(org.osid.search.terms.IdTerm term) {
        nullarg(term, "relationship Id term");
        this.relationshipIdTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of relationship <code>Id</code> terms.
     *
     *  @param terms a collection of relationship <code>Id</code> terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addRelationshipIdTerms(java.util.Collection<org.osid.search.terms.IdTerm> terms) {
        nullarg(terms, "relationship Id terms");
        this.relationshipIdTerms.addAll(terms);
        return;
    }


    /**
     *  Adds a relationship <code>Id</code> term.
     *
     *  @param relationshipId the relationship <code>Id</code>
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>relationshipId</code>
     *          or is <code>null</code>
     */

    protected void addRelationshipIdTerm(org.osid.id.Id relationshipId, boolean match) {
        this.relationshipIdTerms.add(new net.okapia.osid.primordium.terms.IdTerm(relationshipId, match));
        return;
    }


    /**
     *  Gets the relationship query terms.
     *
     *  @return the relationship terms
     */

    @OSID @Override
    public org.osid.relationship.RelationshipQueryInspector[] getRelationshipTerms() {
        return (this.relationshipTerms.toArray(new org.osid.relationship.RelationshipQueryInspector[this.relationshipTerms.size()]));
    }

    
    /**
     *  Adds a relationship term.
     *
     *  @param term a relationship term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addRelationshipTerm(org.osid.relationship.RelationshipQueryInspector term) {
        nullarg(term, "relationship term");
        this.relationshipTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of relationship terms.
     *
     *  @param terms a collection of relationship terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addRelationshipTerms(java.util.Collection<org.osid.relationship.RelationshipQueryInspector> terms) {
        nullarg(terms, "relationship terms");
        this.relationshipTerms.addAll(terms);
        return;
    }


    /**
     *  Gets the relationship peer <code>Id</code> query terms.
     *
     *  @return the relationship peer <code>Id</code> terms
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRelationshipPeerIdTerms() {
        return (this.relationshipPeerIdTerms.toArray(new org.osid.search.terms.IdTerm[this.relationshipPeerIdTerms.size()]));
    }

    
    /**
     *  Adds a relationship peer <code>Id</code> term.
     *
     *  @param term a relationship peer <code>Id</code> term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addRelationshipPeerIdTerm(org.osid.search.terms.IdTerm term) {
        nullarg(term, "relationship peer Id term");
        this.relationshipPeerIdTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of relationship peer <code>Id</code> terms.
     *
     *  @param terms a collection of relationship peer <code>Id</code> terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addRelationshipPeerIdTerms(java.util.Collection<org.osid.search.terms.IdTerm> terms) {
        nullarg(terms, "relationship peer Id terms");
        this.relationshipPeerIdTerms.addAll(terms);
        return;
    }


    /**
     *  Adds a relationship peer <code>Id</code> term.
     *
     *  @param peerId the relationship peer <code>Id</code>
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>peerId</code>
     *          or is <code>null</code>
     */

    protected void addRelationshipPeerIdTerm(org.osid.id.Id peerId, boolean match) {
        this.relationshipPeerIdTerms.add(new net.okapia.osid.primordium.terms.IdTerm(peerId, match));
        return;
    }


    /**
     *  Gets the record type query terms.
     *
     *  @return the record type terms
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getRecordTypeTerms() {
        return (this.browsable.getRecordTypeTerms());
    }

    
    /**
     *  Adds a record type term.
     *
     *  @param term a record type term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addRecordTypeTerm(org.osid.search.terms.TypeTerm term) {
        this.browsable.addRecordTypeTerm(term);
        return;
    }


    /**
     *  Adds a collection of record type terms.
     *
     *  @param terms a collection of type terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addRecordTypeTerms(java.util.Collection<org.osid.search.terms.TypeTerm> terms) {
        this.browsable.addRecordTypeTerms(terms);
        return;
    }


    /**
     *  Adds a record type term.
     *
     *  @param type the type
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    protected void addRecordTypeTerm(org.osid.type.Type type, boolean match) {
        this.browsable.addRecordTypeTerm(type, match);
        return;
    }


    /**
     *  Gets the record types available in this object.
     *
     *  @return the record types
     */

    @OSID @Override
    public org.osid.type.TypeList getRecordTypes() {
        return (this.browsable.getRecordTypes());
    }


    /**
     *  Tests if this object supports the given record <code>
     *  Type. </code>
     *
     *  @param  recordType a type 
     *  @return <code>true</code> if <code>recordType</code> is
     *          supported, <code> false </code> otherwise
     *  @throws org.osid.NullArgumentException <code> recordType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type recordType) {
        return (this.browsable.hasRecordType(recordType));
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    protected void addRecordType(org.osid.type.Type recordType) {
        this.browsable.addRecordType(recordType);
        return;
    }


    protected class BrowsableQueryInspector
        extends AbstractOsidBrowsableQueryInspector
        implements org.osid.OsidBrowsableQueryInspector,
                   org.osid.OsidExtensibleQueryInspector {

    
        /**
         *  Adds a record type term.
         *
         *  @param term a record type term
         *  @throws org.osid.NullArgumentException <code>term</code> is 
         *          <code>null</code>
         */
        
        @Override
        protected void addRecordTypeTerm(org.osid.search.terms.TypeTerm term) {
            super.addRecordTypeTerm(term);
            return;
        }


        /**
         *  Adds a collection of record type terms.
         *
         *  @param terms a collection of type terms
         *  @throws org.osid.NullArgumentException <code>terms</code> is 
         *          <code>null</code>
         */
        
        @Override
        protected void addRecordTypeTerms(java.util.Collection<org.osid.search.terms.TypeTerm> terms) {
            super.addRecordTypeTerms(terms);
            return;
        }


        /**
         *  Adds a record type term.
         *
         *  @param type the type
         *  @param match <code>true</code> for a positive match,
         *         <code>false</code> for a negative match
         *  @throws org.osid.NullArgumentException <code>type</code>
         *          is <code>null</code>
         */
        
        @Override
        protected void addRecordTypeTerm(org.osid.type.Type type, boolean match) {
            super.addRecordTypeTerm(type, match);
            return;
        }


        /**
         *  Adds a record type.
         *
         *  @param recordType
         *  @throws org.osid.NullArgumentException <code>recordType</code>
         *          is <code>null</code>
         */
        
        @Override
        protected void addRecordType(org.osid.type.Type recordType) {
            super.addRecordType(recordType);
            return;
        }
    }
}
