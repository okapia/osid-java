//
// AbstractAssemblyOsidIdentifiableQuery.java
//
//     An OsidQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An OsidQuery that stores terms.
 */

public abstract class AbstractAssemblyOsidIdentifiableQuery
    extends AbstractAssemblyOsidQuery
    implements org.osid.OsidIdentifiableQuery,
               org.osid.OsidIdentifiableQueryInspector,
               org.osid.OsidIdentifiableSearchOrder {

    
    /** 
     *  Constructs a new <code>AbstractAssemblyOsidIdentifiableQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyOsidIdentifiableQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Adds an <code> Id </code> to match. Multiple <code> Ids
     *  </code> can be added to perform a boolean <code> OR </code>
     *  among them.
     *
     *  @param  id <code> Id </code> to match
     *  @param  match <code> true </code> for a positive match, <code> false
     *          </code> for a negative match
     *  @throws org.osid.NullArgumentException <code> id </code> is <code>
     *          null </code>
     */

    @OSID @Override
    public void matchId(org.osid.id.Id id, boolean match) {
        getAssembler().addIdTerm(getIdColumn(), id, match);
        return;
    }


    /**
     *  Clears all <code> Id </code> terms.
     */

    public void clearIdTerms() {
        getAssembler().clearTerms(getIdColumn());
        return;
    }

    
    /**
     *  Gets the Id query terms. 
     *
     *  @return the Id terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getIdTerms() {
        return (getAssembler().getIdTerms(getIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the Id.
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderById(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getIdColumn(), style);
        return;
    }


    /**
     *  Gets the column name for the Id field.
     *
     *  @return the column name
     */

    protected String getIdColumn() {
        return ("id");
    }
}
