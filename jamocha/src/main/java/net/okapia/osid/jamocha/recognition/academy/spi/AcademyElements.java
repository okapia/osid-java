//
// AcademyElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recognition.academy.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class AcademyElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the AcademyElement Id.
     *
     *  @return the academy element Id
     */

    public static org.osid.id.Id getAcademyEntityId() {
        return (makeEntityId("osid.recognition.Academy"));
    }


    /**
     *  Gets the ConferralId element Id.
     *
     *  @return the ConferralId element Id
     */

    public static org.osid.id.Id getConferralId() {
        return (makeQueryElementId("osid.recognition.academy.ConferralId"));
    }


    /**
     *  Gets the Conferral element Id.
     *
     *  @return the Conferral element Id
     */

    public static org.osid.id.Id getConferral() {
        return (makeQueryElementId("osid.recognition.academy.Conferral"));
    }


    /**
     *  Gets the AwardId element Id.
     *
     *  @return the AwardId element Id
     */

    public static org.osid.id.Id getAwardId() {
        return (makeQueryElementId("osid.recognition.academy.AwardId"));
    }


    /**
     *  Gets the Award element Id.
     *
     *  @return the Award element Id
     */

    public static org.osid.id.Id getAward() {
        return (makeQueryElementId("osid.recognition.academy.Award"));
    }


    /**
     *  Gets the ConvocationId element Id.
     *
     *  @return the ConvocationId element Id
     */

    public static org.osid.id.Id getConvocationId() {
        return (makeQueryElementId("osid.recognition.academy.ConvocationId"));
    }


    /**
     *  Gets the Convocation element Id.
     *
     *  @return the Convocation element Id
     */

    public static org.osid.id.Id getConvocation() {
        return (makeQueryElementId("osid.recognition.academy.Convocation"));
    }


    /**
     *  Gets the AncestorAcademyId element Id.
     *
     *  @return the AncestorAcademyId element Id
     */

    public static org.osid.id.Id getAncestorAcademyId() {
        return (makeQueryElementId("osid.recognition.academy.AncestorAcademyId"));
    }


    /**
     *  Gets the AncestorAcademy element Id.
     *
     *  @return the AncestorAcademy element Id
     */

    public static org.osid.id.Id getAncestorAcademy() {
        return (makeQueryElementId("osid.recognition.academy.AncestorAcademy"));
    }


    /**
     *  Gets the DescendantAcademyId element Id.
     *
     *  @return the DescendantAcademyId element Id
     */

    public static org.osid.id.Id getDescendantAcademyId() {
        return (makeQueryElementId("osid.recognition.academy.DescendantAcademyId"));
    }


    /**
     *  Gets the DescendantAcademy element Id.
     *
     *  @return the DescendantAcademy element Id
     */

    public static org.osid.id.Id getDescendantAcademy() {
        return (makeQueryElementId("osid.recognition.academy.DescendantAcademy"));
    }
}
