//
// MutableMapFiscalPeriodLookupSession
//
//    Implements a FiscalPeriod lookup service backed by a collection of
//    fiscalPeriods that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.financials;


/**
 *  Implements a FiscalPeriod lookup service backed by a collection of
 *  fiscal periods. The fiscal periods are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of fiscal periods can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapFiscalPeriodLookupSession
    extends net.okapia.osid.jamocha.core.financials.spi.AbstractMapFiscalPeriodLookupSession
    implements org.osid.financials.FiscalPeriodLookupSession {


    /**
     *  Constructs a new {@code MutableMapFiscalPeriodLookupSession}
     *  with no fiscal periods.
     *
     *  @param business the business
     *  @throws org.osid.NullArgumentException {@code business} is
     *          {@code null}
     */

      public MutableMapFiscalPeriodLookupSession(org.osid.financials.Business business) {
        setBusiness(business);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapFiscalPeriodLookupSession} with a
     *  single fiscalPeriod.
     *
     *  @param business the business  
     *  @param fiscalPeriod a fiscal period
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code fiscalPeriod} is {@code null}
     */

    public MutableMapFiscalPeriodLookupSession(org.osid.financials.Business business,
                                           org.osid.financials.FiscalPeriod fiscalPeriod) {
        this(business);
        putFiscalPeriod(fiscalPeriod);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapFiscalPeriodLookupSession}
     *  using an array of fiscal periods.
     *
     *  @param business the business
     *  @param fiscalPeriods an array of fiscal periods
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code fiscalPeriods} is {@code null}
     */

    public MutableMapFiscalPeriodLookupSession(org.osid.financials.Business business,
                                           org.osid.financials.FiscalPeriod[] fiscalPeriods) {
        this(business);
        putFiscalPeriods(fiscalPeriods);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapFiscalPeriodLookupSession}
     *  using a collection of fiscal periods.
     *
     *  @param business the business
     *  @param fiscalPeriods a collection of fiscal periods
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code fiscalPeriods} is {@code null}
     */

    public MutableMapFiscalPeriodLookupSession(org.osid.financials.Business business,
                                           java.util.Collection<? extends org.osid.financials.FiscalPeriod> fiscalPeriods) {

        this(business);
        putFiscalPeriods(fiscalPeriods);
        return;
    }

    
    /**
     *  Makes a {@code FiscalPeriod} available in this session.
     *
     *  @param fiscalPeriod a fiscal period
     *  @throws org.osid.NullArgumentException {@code fiscalPeriod{@code  is
     *          {@code null}
     */

    @Override
    public void putFiscalPeriod(org.osid.financials.FiscalPeriod fiscalPeriod) {
        super.putFiscalPeriod(fiscalPeriod);
        return;
    }


    /**
     *  Makes an array of fiscal periods available in this session.
     *
     *  @param fiscalPeriods an array of fiscal periods
     *  @throws org.osid.NullArgumentException {@code fiscalPeriods{@code 
     *          is {@code null}
     */

    @Override
    public void putFiscalPeriods(org.osid.financials.FiscalPeriod[] fiscalPeriods) {
        super.putFiscalPeriods(fiscalPeriods);
        return;
    }


    /**
     *  Makes collection of fiscal periods available in this session.
     *
     *  @param fiscalPeriods a collection of fiscal periods
     *  @throws org.osid.NullArgumentException {@code fiscalPeriods{@code  is
     *          {@code null}
     */

    @Override
    public void putFiscalPeriods(java.util.Collection<? extends org.osid.financials.FiscalPeriod> fiscalPeriods) {
        super.putFiscalPeriods(fiscalPeriods);
        return;
    }


    /**
     *  Removes a FiscalPeriod from this session.
     *
     *  @param fiscalPeriodId the {@code Id} of the fiscal period
     *  @throws org.osid.NullArgumentException {@code fiscalPeriodId{@code 
     *          is {@code null}
     */

    @Override
    public void removeFiscalPeriod(org.osid.id.Id fiscalPeriodId) {
        super.removeFiscalPeriod(fiscalPeriodId);
        return;
    }    
}
