//
// AbstractResponse.java
//
//     Defines a Response.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inquiry.response.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Response</code>.
 */

public abstract class AbstractResponse
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.inquiry.Response {

    private org.osid.inquiry.Inquiry inquiry;
    private org.osid.resource.Resource responder;
    private org.osid.authentication.Agent respondingAgent;
    private boolean affirmative;

    private final java.util.Collection<org.osid.inquiry.records.ResponseRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the inquiry <code> Id. </code> 
     *
     *  @return the inquiry <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getInquiryId() {
        return (this.inquiry.getId());
    }


    /**
     *  Gets the inquiry. 
     *
     *  @return the inquiry 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.inquiry.Inquiry getInquiry()
        throws org.osid.OperationFailedException {

        return (this.inquiry);
    }


    /**
     *  Sets the inquiry.
     *
     *  @param inquiry an inquiry
     *  @throws org.osid.NullArgumentException <code>inquiry</code> is
     *          <code>null</code>
     */

    protected void setInquiry(org.osid.inquiry.Inquiry inquiry) {
        nullarg(inquiry, "inquiry");
        this.inquiry = inquiry;
        return;
    }


    /**
     *  Gets the responder resource <code> Id. </code> 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResponderId() {
        return (this.responder.getId());
    }


    /**
     *  Gets the responder resource. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResponder()
        throws org.osid.OperationFailedException {

        return (this.responder);
    }


    /**
     *  Sets the responder.
     *
     *  @param responder a responder
     *  @throws org.osid.NullArgumentException <code>responder</code>
     *          is <code>null</code>
     */

    protected void setResponder(org.osid.resource.Resource responder) {
        nullarg(responder, "responder resource");
        this.responder = responder;
        return;
    }


    /**
     *  Gets the responding agent <code> Id. </code> 
     *
     *  @return the agent <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getRespondingAgentId() {
        return (this.respondingAgent.getId());
    }


    /**
     *  Gets the responding agent. 
     *
     *  @return the agent 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getRespondingAgent()
        throws org.osid.OperationFailedException {

        return (this.respondingAgent);
    }


    /**
     *  Sets the responding agent.
     *
     *  @param respondingAgent a responding agent
     *  @throws org.osid.NullArgumentException
     *          <code>respondingAgent</code> is <code>null</code>
     */

    protected void setRespondingAgent(org.osid.authentication.Agent respondingAgent) {
        nullarg(respondingAgent, "responding agent");
        this.respondingAgent = respondingAgent;
        return;
    }


    /**
     *  Tests if this response was affirmative. 
     *
     *  @return <code> true </code> if affirmative, <code> false
     *          </code> if negative
     */

    @OSID @Override
    public boolean isAffirmative() {
        return (this.affirmative);
    }


    /**
     *  Sets the affirmative flag.
     *
     *  @param affirmative {@code true} if a positive response, {@code
     *         false} if a negative response
     */

    protected void setAffirmative(boolean affirmative) {
        this.affirmative = affirmative;
        return;
    }


    /**
     *  Tests if this response supports the given record
     *  <code>Type</code>.
     *
     *  @param  responseRecordType a response record type 
     *  @return <code>true</code> if the responseRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>responseRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type responseRecordType) {
        for (org.osid.inquiry.records.ResponseRecord record : this.records) {
            if (record.implementsRecordType(responseRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  responseRecordType the response record type 
     *  @return the response record 
     *  @throws org.osid.NullArgumentException
     *          <code>responseRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(responseRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inquiry.records.ResponseRecord getResponseRecord(org.osid.type.Type responseRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inquiry.records.ResponseRecord record : this.records) {
            if (record.implementsRecordType(responseRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(responseRecordType + " is not supported");
    }


    /**
     *  Adds a record to this response. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param responseRecord the response record
     *  @param responseRecordType response record type
     *  @throws org.osid.NullArgumentException
     *          <code>responseRecord</code> or
     *          <code>responseRecordTyperesponse</code> is
     *          <code>null</code>
     */
            
    protected void addResponseRecord(org.osid.inquiry.records.ResponseRecord responseRecord, 
                                     org.osid.type.Type responseRecordType) {

        addRecordType(responseRecordType);
        this.records.add(responseRecord);
        
        return;
    }
}
