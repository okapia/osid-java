//
// InstructionMiter.java
//
//     Defines an Instruction miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.rules.check.instruction;


/**
 *  Defines an <code>Instruction</code> miter for use with the builders.
 */

public interface InstructionMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidEnablerMiter,
            net.okapia.osid.jamocha.builder.spi.OsidRelationshipMiter,
            org.osid.rules.check.Instruction {


    /**
     *  Sets the agenda.
     *
     *  @param agenda an agenda
     *  @throws org.osid.NullArgumentException <code>agenda</code> is
     *          <code>null</code>
     */

    public void setAgenda(org.osid.rules.check.Agenda agenda);


    /**
     *  Sets the check.
     *
     *  @param check a check
     *  @throws org.osid.NullArgumentException <code>check</code> is
     *          <code>null</code>
     */

    public void setCheck(org.osid.rules.check.Check check);


    /**
     *  Sets the message.
     *
     *  @param message a message
     *  @throws org.osid.NullArgumentException <code>message</code> is
     *          <code>null</code>
     */

    public void setMessage(org.osid.locale.DisplayText message);


    /**
     *  Adds an Instruction record.
     *
     *  @param record an instruction record
     *  @param recordType the type of instruction record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addInstructionRecord(org.osid.rules.check.records.InstructionRecord record, org.osid.type.Type recordType);
}       


