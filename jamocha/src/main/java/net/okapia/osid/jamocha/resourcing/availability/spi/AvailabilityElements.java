//
// AvailabilityElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.availability.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class AvailabilityElements
    extends net.okapia.osid.jamocha.spi.OsidRelationshipElements {


    /**
     *  Gets the AvailabilityElement Id.
     *
     *  @return the availability element Id
     */

    public static org.osid.id.Id getAvailabilityEntityId() {
        return (makeEntityId("osid.resourcing.Availability"));
    }


    /**
     *  Gets the ResourceId element Id.
     *
     *  @return the ResourceId element Id
     */

    public static org.osid.id.Id getResourceId() {
        return (makeElementId("osid.resourcing.availability.ResourceId"));
    }


    /**
     *  Gets the Resource element Id.
     *
     *  @return the Resource element Id
     */

    public static org.osid.id.Id getResource() {
        return (makeElementId("osid.resourcing.availability.Resource"));
    }


    /**
     *  Gets the JobId element Id.
     *
     *  @return the JobId element Id
     */

    public static org.osid.id.Id getJobId() {
        return (makeElementId("osid.resourcing.availability.JobId"));
    }


    /**
     *  Gets the Job element Id.
     *
     *  @return the Job element Id
     */

    public static org.osid.id.Id getJob() {
        return (makeElementId("osid.resourcing.availability.Job"));
    }


    /**
     *  Gets the CompetencyId element Id.
     *
     *  @return the CompetencyId element Id
     */

    public static org.osid.id.Id getCompetencyId() {
        return (makeElementId("osid.resourcing.availability.CompetencyId"));
    }


    /**
     *  Gets the Competency element Id.
     *
     *  @return the Competency element Id
     */

    public static org.osid.id.Id getCompetency() {
        return (makeElementId("osid.resourcing.availability.Competency"));
    }


    /**
     *  Gets the Percentage element Id.
     *
     *  @return the Percentage element Id
     */

    public static org.osid.id.Id getPercentage() {
        return (makeElementId("osid.resourcing.availability.Percentage"));
    }


    /**
     *  Gets the FoundryId element Id.
     *
     *  @return the FoundryId element Id
     */

    public static org.osid.id.Id getFoundryId() {
        return (makeQueryElementId("osid.resourcing.availability.FoundryId"));
    }


    /**
     *  Gets the Foundry element Id.
     *
     *  @return the Foundry element Id
     */

    public static org.osid.id.Id getFoundry() {
        return (makeQueryElementId("osid.resourcing.availability.Foundry"));
    }
}
