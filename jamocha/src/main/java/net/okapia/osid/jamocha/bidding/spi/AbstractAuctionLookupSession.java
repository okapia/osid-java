//
// AbstractAuctionLookupSession.java
//
//    A starter implementation framework for providing an Auction
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an Auction
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getAuctions(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractAuctionLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.bidding.AuctionLookupSession {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean federated     = false;
    private org.osid.bidding.AuctionHouse auctionHouse = new net.okapia.osid.jamocha.nil.bidding.auctionhouse.UnknownAuctionHouse();
    

    /**
     *  Gets the <code>AuctionHouse/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>AuctionHouse Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAuctionHouseId() {
        return (this.auctionHouse.getId());
    }


    /**
     *  Gets the <code>AuctionHouse</code> associated with this 
     *  session.
     *
     *  @return the <code>AuctionHouse</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouse getAuctionHouse()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.auctionHouse);
    }


    /**
     *  Sets the <code>AuctionHouse</code>.
     *
     *  @param  auctionHouse the auction house for this session
     *  @throws org.osid.NullArgumentException <code>auctionHouse</code>
     *          is <code>null</code>
     */

    protected void setAuctionHouse(org.osid.bidding.AuctionHouse auctionHouse) {
        nullarg(auctionHouse, "auction house");
        this.auctionHouse = auctionHouse;
        return;
    }

    /**
     *  Tests if this user can perform <code>Auction</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAuctions() {
        return (true);
    }


    /**
     *  A complete view of the <code>Auction</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAuctionView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Auction</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAuctionView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include auctions in auction houses which are
     *  children of this auction house in the auction house hierarchy.
     */

    @OSID @Override
    public void useFederatedAuctionHouseView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this auction house only.
     */

    @OSID @Override
    public void useIsolatedAuctionHouseView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active auctions are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveAuctionView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive auctions are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusAuctionView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>Auction</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Auction</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Auction</code> and
     *  retained for compatibility.
     *
     *  In active mode, auctions are returned that are currently
     *  active. In any status mode, active and inactive auctions
     *  are returned.
     *
     *  @param  auctionId <code>Id</code> of the
     *          <code>Auction</code>
     *  @return the auction
     *  @throws org.osid.NotFoundException <code>auctionId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>auctionId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.Auction getAuction(org.osid.id.Id auctionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.bidding.AuctionList auctions = getAuctions()) {
            while (auctions.hasNext()) {
                org.osid.bidding.Auction auction = auctions.getNextAuction();
                if (auction.getId().equals(auctionId)) {
                    return (auction);
                }
            }
        } 

        throw new org.osid.NotFoundException(auctionId + " not found");
    }


    /**
     *  Gets a <code>AuctionList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  auctions specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Auctions</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, auctions are returned that are currently
     *  active. In any status mode, active and inactive auctions
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getAuctions()</code>.
     *
     *  @param  auctionIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Auction</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>auctionIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionList getAuctionsByIds(org.osid.id.IdList auctionIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.bidding.Auction> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = auctionIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getAuction(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("auction " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.bidding.auction.LinkedAuctionList(ret));
    }


    /**
     *  Gets a <code>AuctionList</code> corresponding to the given
     *  auction genus <code>Type</code> which does not include
     *  auctions of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  auctions or an error results. Otherwise, the returned list
     *  may contain only those auctions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, auctions are returned that are currently
     *  active. In any status mode, active and inactive auctions
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getAuctions()</code>.
     *
     *  @param  auctionGenusType an auction genus type 
     *  @return the returned <code>Auction</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionList getAuctionsByGenusType(org.osid.type.Type auctionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.bidding.auction.AuctionGenusFilterList(getAuctions(), auctionGenusType));
    }


    /**
     *  Gets a <code>AuctionList</code> corresponding to the given
     *  auction genus <code>Type</code> and include any additional
     *  auctions with genus types derived from the specified
     *  <code>Type</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  auctions or an error results. Otherwise, the returned list
     *  may contain only those auctions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, auctions are returned that are currently
     *  active. In any status mode, active and inactive auctions
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAuctions()</code>.
     *
     *  @param  auctionGenusType an auction genus type 
     *  @return the returned <code>Auction</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionList getAuctionsByParentGenusType(org.osid.type.Type auctionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getAuctionsByGenusType(auctionGenusType));
    }


    /**
     *  Gets a <code>AuctionList</code> containing the given
     *  auction record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  auctions or an error results. Otherwise, the returned list
     *  may contain only those auctions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, auctions are returned that are currently
     *  active. In any status mode, active and inactive auctions
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAuctions()</code>.
     *
     *  @param  auctionRecordType an auction record type 
     *  @return the returned <code>Auction</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionList getAuctionsByRecordType(org.osid.type.Type auctionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.bidding.auction.AuctionRecordFilterList(getAuctions(), auctionRecordType));
    }


    /**
     *  Gets an <code>AuctionList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known auctions
     *  or an error results. Otherwise, the returned list may contain
     *  only those auctions that are accessible through this session.
     *
     *  In active mode, auctions are returned that are currently
     *  active. In any status mode, active and inactive auctions
     *  are returned.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Auction</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.bidding.AuctionList getAuctionsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.bidding.auction.AuctionProviderFilterList(getAuctions(), resourceId));
    }


    /**
     *  Gets an <code> AuctionList </code> effective during the entire given
     *  date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known auctions or an
     *  error results. Otherwise, the returned list may contain only those
     *  auctions that are accessible through this session.
     *
     *  In active mode, auctions are returned that are currently active. In
     *  any status mode, active and inactive auctions are returned.
     *
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code> Auction </code> list
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> from </code> or <code>
     *          to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.bidding.AuctionList getAuctionsOnDate(org.osid.calendaring.DateTime from,
                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.bidding.auction.TemporalAuctionFilterList(getAuctions(), from, to));
    }


    /**
     *  Gets a list of auctions for an item.
     *
     *  In plenary mode, the returned list contains all known auctions
     *  or an error results. Otherwise, the returned list may contain
     *  only those auctions that are accessible through this session.
     *
     *  In active mode, auctions are returned that are currently
     *  active. In any status mode, active and inactive auctions are
     *  returned.
     *
     *  @param  itemId a resource <code> Id </code>
     *  @return the returned <code> Auction </code> list
     *  @throws org.osid.NullArgumentException <code> itemId </code> is <code>
     *          null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.bidding.AuctionList getAuctionsByItem(org.osid.id.Id itemId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.bidding.auction.AuctionFilterList(new ItemFilter(itemId), getAuctions()));
    }


    /**
     *  Gets a list of auctions for an item genus type.
     *
     *  In plenary mode, the returned list contains all known auctions
     *  or an error results. Otherwise, the returned list may contain
     *  only those auctions that are accessible through this session.
     *
     *  In active mode, auctions are returned that are currently
     *  active. In any status mode, active and inactive auctions are
     *  returned.
     *
     *  @param  itemGenusType an item genus type
     *  @return the returned <code> Auction </code> list
     *  @throws org.osid.NullArgumentException <code> itemGenusType </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.bidding.AuctionList getAuctionsByItemGenusType(org.osid.type.Type itemGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.bidding.Auction> ret = new java.util.ArrayList<>();

        try (org.osid.bidding.AuctionList auctions = getAuctions()) {
            while (auctions.hasNext()) {
                org.osid.bidding.Auction auction = auctions.getNextAuction();
                org.osid.resource.Resource item = auction.getItem();
                if (item.isOfGenusType(itemGenusType)) {
                    ret.add(auction);
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.bidding.auction.LinkedAuctionList(ret));
    }


    /**
     *  Gets an <code> AuctionList </code> for an item genus type and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *
     *  In plenary mode, the returned list contains all known auctions or an
     *  error results. Otherwise, the returned list may contain only those
     *  auctions that are accessible through this session.
     *
     *  In active mode, auctions are returned that are currently active. In
     *  any status mode, active and inactive auctions are returned.
     *
     *  @param  itemGenusType an item genus type
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code> Auction </code> list
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> itemGenusType, from,
     *          </code> or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.bidding.AuctionList getAuctionsByItemGenusTypeOnDate(org.osid.type.Type itemGenusType,
                                                                         org.osid.calendaring.DateTime from,
                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.bidding.auction.TemporalAuctionFilterList(getAuctionsByItemGenusType(itemGenusType), from, to));
    }


    /**
     *  Gets all <code>Auctions</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  auctions or an error results. Otherwise, the returned list
     *  may contain only those auctions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, auctions are returned that are currently
     *  active. In any status mode, active and inactive auctions
     *  are returned.
     *
     *  @return a list of <code>Auctions</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.bidding.AuctionList getAuctions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the auction list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of auctions
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.bidding.AuctionList filterAuctionsOnViews(org.osid.bidding.AuctionList list)
        throws org.osid.OperationFailedException {
            
        org.osid.bidding.AuctionList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.bidding.auction.ActiveAuctionFilterList(ret);
        }

        return (ret);
    }


    public static class ItemFilter
        implements net.okapia.osid.jamocha.inline.filter.bidding.auction.AuctionFilter {
        
        private final org.osid.id.Id itemId;
        
        
        /**
         *  Constructs a new <code>ItemFilter</code>.
         *
         *  @param itemId the item to filter
         *  @throws org.osid.NullArgumentException
         *          <code>itemId</code> is <code>null</code>
         */
        
        public ItemFilter(org.osid.id.Id itemId) {
            nullarg(itemId, "item Id");
            this.itemId = itemId;
            return;
        }
        

        /**
         *  Used by the AtcionFilterList to filter the 
         *  auction list based on item.
         *
         *  @param auction the auction
         *  @return <code>true</code> to pass the auction,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.bidding.Auction auction) {
            return (auction.getItemId().equals(this.itemId));
        }
    }    
}
