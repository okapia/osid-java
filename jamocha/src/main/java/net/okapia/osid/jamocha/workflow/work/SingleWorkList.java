//
// SingleWorkList.java
//
//     Implements a WorkList based on a single Work.
//
//
// Tom Coppeto
// OnTapSolutions
// 29 June 2008
//
//
// Copyright (c) 2008, 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.work;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a ObjectList based on a single Work.
 */

public final class SingleWorkList
    extends net.okapia.osid.jamocha.workflow.work.spi.AbstractWorkList
    implements org.osid.workflow.WorkList {

    private final org.osid.workflow.Work work;
    private boolean read = false;


    /**
     *  Creates a new <code>SingleWorkList</code>.
     *
     *  @param work a work
     *  @throws org.osid.NullArgumentException <code>work</code>
     *          is <code>null</code>
     */

    public SingleWorkList(org.osid.workflow.Work work) {
        nullarg(work, "work");
        this.work = work;
        return;
    }


    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code>true</code> if more elements are available in
     *          this list, <code>false</code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (this.read) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code>Work</code> in this list. 
     *
     *  @return the next <code>Work</code> in this list. The
     *          <code> hasNext() </code> method should be used to test
     *          that a next <code>Work</code> is available before
     *          calling this method.
     *  @throws org.osid.IllegalStateException no more elements
     *          available in this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.workflow.Work getNextWork()
        throws org.osid.OperationFailedException {

        if (hasNext()) {
            this.read = true;
            return (this.work);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in work list");
        }
    }
}
