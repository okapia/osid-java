//
// AbstractQueryAuditEnablerLookupSession.java
//
//    An AuditEnablerQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.inquiry.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An AuditEnablerQuerySession adapter.
 */

public abstract class AbstractAdapterAuditEnablerQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.inquiry.rules.AuditEnablerQuerySession {

    private final org.osid.inquiry.rules.AuditEnablerQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterAuditEnablerQuerySession.
     *
     *  @param session the underlying audit enabler query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterAuditEnablerQuerySession(org.osid.inquiry.rules.AuditEnablerQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeInquest</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeInquest Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getInquestId() {
        return (this.session.getInquestId());
    }


    /**
     *  Gets the {@codeInquest</code> associated with this 
     *  session.
     *
     *  @return the {@codeInquest</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.Inquest getInquest()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getInquest());
    }


    /**
     *  Tests if this user can perform {@codeAuditEnabler</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchAuditEnablers() {
        return (this.session.canSearchAuditEnablers());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include audit enablers in inquests which are children
     *  of this inquest in the inquest hierarchy.
     */

    @OSID @Override
    public void useFederatedInquestView() {
        this.session.useFederatedInquestView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this inquest only.
     */
    
    @OSID @Override
    public void useIsolatedInquestView() {
        this.session.useIsolatedInquestView();
        return;
    }
    
      
    /**
     *  Gets an audit enabler query. The returned query will not have an
     *  extension query.
     *
     *  @return the audit enabler query 
     */
      
    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerQuery getAuditEnablerQuery() {
        return (this.session.getAuditEnablerQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  auditEnablerQuery the audit enabler query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code auditEnablerQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code auditEnablerQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerList getAuditEnablersByQuery(org.osid.inquiry.rules.AuditEnablerQuery auditEnablerQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getAuditEnablersByQuery(auditEnablerQuery));
    }
}
