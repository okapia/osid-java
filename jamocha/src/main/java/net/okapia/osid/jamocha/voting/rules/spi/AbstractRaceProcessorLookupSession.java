//
// AbstractRaceProcessorLookupSession.java
//
//    A starter implementation framework for providing a RaceProcessor
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a RaceProcessor
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getRaceProcessors(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractRaceProcessorLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.voting.rules.RaceProcessorLookupSession {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean federated     = false;
    private org.osid.voting.Polls polls = new net.okapia.osid.jamocha.nil.voting.polls.UnknownPolls();
    

    /**
     *  Gets the <code>Polls/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Polls Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getPollsId() {
        return (this.polls.getId());
    }


    /**
     *  Gets the <code>Polls</code> associated with this 
     *  session.
     *
     *  @return the <code>Polls</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Polls getPolls()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.polls);
    }


    /**
     *  Sets the <code>Polls</code>.
     *
     *  @param  polls the polls for this session
     *  @throws org.osid.NullArgumentException <code>polls</code>
     *          is <code>null</code>
     */

    protected void setPolls(org.osid.voting.Polls polls) {
        nullarg(polls, "polls");
        this.polls = polls;
        return;
    }


    /**
     *  Tests if this user can perform <code>RaceProcessor</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRaceProcessors() {
        return (true);
    }


    /**
     *  A complete view of the <code>RaceProcessor</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRaceProcessorView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>RaceProcessor</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRaceProcessorView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include race processors in pollses which are children
     *  of this polls in the polls hierarchy.
     */

    @OSID @Override
    public void useFederatedPollsView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this polls only.
     */

    @OSID @Override
    public void useIsolatedPollsView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active race processors are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveRaceProcessorView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive race processors are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusRaceProcessorView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>RaceProcessor</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>RaceProcessor</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>RaceProcessor</code> and
     *  retained for compatibility.
     *
     *  In active mode, race processors are returned that are currently
     *  active. In any status mode, active and inactive race processors
     *  are returned.
     *
     *  @param  raceProcessorId <code>Id</code> of the
     *          <code>RaceProcessor</code>
     *  @return the race processor
     *  @throws org.osid.NotFoundException <code>raceProcessorId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>raceProcessorId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessor getRaceProcessor(org.osid.id.Id raceProcessorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.voting.rules.RaceProcessorList raceProcessors = getRaceProcessors()) {
            while (raceProcessors.hasNext()) {
                org.osid.voting.rules.RaceProcessor raceProcessor = raceProcessors.getNextRaceProcessor();
                if (raceProcessor.getId().equals(raceProcessorId)) {
                    return (raceProcessor);
                }
            }
        } 

        throw new org.osid.NotFoundException(raceProcessorId + " not found");
    }


    /**
     *  Gets a <code>RaceProcessorList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  raceProcessors specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>RaceProcessors</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, race processors are returned that are currently
     *  active. In any status mode, active and inactive race processors
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getRaceProcessors()</code>.
     *
     *  @param  raceProcessorIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>RaceProcessor</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorList getRaceProcessorsByIds(org.osid.id.IdList raceProcessorIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.voting.rules.RaceProcessor> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = raceProcessorIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getRaceProcessor(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("race processor " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.voting.rules.raceprocessor.LinkedRaceProcessorList(ret));
    }


    /**
     *  Gets a <code>RaceProcessorList</code> corresponding to the given
     *  race processor genus <code>Type</code> which does not include
     *  race processors of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  race processors or an error results. Otherwise, the returned list
     *  may contain only those race processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, race processors are returned that are currently
     *  active. In any status mode, active and inactive race processors
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getRaceProcessors()</code>.
     *
     *  @param  raceProcessorGenusType a raceProcessor genus type 
     *  @return the returned <code>RaceProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorList getRaceProcessorsByGenusType(org.osid.type.Type raceProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.voting.rules.raceprocessor.RaceProcessorGenusFilterList(getRaceProcessors(), raceProcessorGenusType));
    }


    /**
     *  Gets a <code>RaceProcessorList</code> corresponding to the given
     *  race processor genus <code>Type</code> and include any additional
     *  race processors with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  race processors or an error results. Otherwise, the returned list
     *  may contain only those race processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, race processors are returned that are currently
     *  active. In any status mode, active and inactive race processors
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getRaceProcessors()</code>.
     *
     *  @param  raceProcessorGenusType a raceProcessor genus type 
     *  @return the returned <code>RaceProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorList getRaceProcessorsByParentGenusType(org.osid.type.Type raceProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getRaceProcessorsByGenusType(raceProcessorGenusType));
    }


    /**
     *  Gets a <code>RaceProcessorList</code> containing the given
     *  race processor record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  race processors or an error results. Otherwise, the returned list
     *  may contain only those race processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, race processors are returned that are currently
     *  active. In any status mode, active and inactive race processors
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getRaceProcessors()</code>.
     *
     *  @param  raceProcessorRecordType a raceProcessor record type 
     *  @return the returned <code>RaceProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorList getRaceProcessorsByRecordType(org.osid.type.Type raceProcessorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.voting.rules.raceprocessor.RaceProcessorRecordFilterList(getRaceProcessors(), raceProcessorRecordType));
    }


    /**
     *  Gets all <code>RaceProcessors</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  race processors or an error results. Otherwise, the returned list
     *  may contain only those race processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, race processors are returned that are currently
     *  active. In any status mode, active and inactive race processors
     *  are returned.
     *
     *  @return a list of <code>RaceProcessors</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.voting.rules.RaceProcessorList getRaceProcessors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the race processor list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of race processors
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.voting.rules.RaceProcessorList filterRaceProcessorsOnViews(org.osid.voting.rules.RaceProcessorList list)
        throws org.osid.OperationFailedException {

        org.osid.voting.rules.RaceProcessorList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.voting.rules.raceprocessor.ActiveRaceProcessorFilterList(ret);
        }

        return (ret);
    }
}
