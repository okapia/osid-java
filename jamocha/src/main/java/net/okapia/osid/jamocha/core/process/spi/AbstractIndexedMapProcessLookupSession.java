//
// AbstractIndexedMapProcessLookupSession.java
//
//    A simple framework for providing a Process lookup service
//    backed by a fixed collection of processes with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.process.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Process lookup service backed by a
 *  fixed collection of processes. The processes are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some processes may be compatible
 *  with more types than are indicated through these process
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Processes</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapProcessLookupSession
    extends AbstractMapProcessLookupSession
    implements org.osid.process.ProcessLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.process.Process> processesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.process.Process>());
    private final MultiMap<org.osid.type.Type, org.osid.process.Process> processesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.process.Process>());


    /**
     *  Makes a <code>Process</code> available in this session.
     *
     *  @param  process a process
     *  @throws org.osid.NullArgumentException <code>process<code> is
     *          <code>null</code>
     */

    @Override
    protected void putProcess(org.osid.process.Process process) {
        super.putProcess(process);

        this.processesByGenus.put(process.getGenusType(), process);
        
        try (org.osid.type.TypeList types = process.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.processesByRecord.put(types.getNextType(), process);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a process from this session.
     *
     *  @param processId the <code>Id</code> of the process
     *  @throws org.osid.NullArgumentException <code>processId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeProcess(org.osid.id.Id processId) {
        org.osid.process.Process process;
        try {
            process = getProcess(processId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.processesByGenus.remove(process.getGenusType());

        try (org.osid.type.TypeList types = process.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.processesByRecord.remove(types.getNextType(), process);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeProcess(processId);
        return;
    }


    /**
     *  Gets a <code>ProcessList</code> corresponding to the given
     *  process genus <code>Type</code> which does not include
     *  processes of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known processes or an error results. Otherwise,
     *  the returned list may contain only those processes that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  processGenusType a process genus type 
     *  @return the returned <code>Process</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>processGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.ProcessList getProcessesByGenusType(org.osid.type.Type processGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.process.process.ArrayProcessList(this.processesByGenus.get(processGenusType)));
    }


    /**
     *  Gets a <code>ProcessList</code> containing the given
     *  process record <code>Type</code>. In plenary mode, the
     *  returned list contains all known processes or an error
     *  results. Otherwise, the returned list may contain only those
     *  processes that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  processRecordType a process record type 
     *  @return the returned <code>process</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>processRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.ProcessList getProcessesByRecordType(org.osid.type.Type processRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.process.process.ArrayProcessList(this.processesByRecord.get(processRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.processesByGenus.clear();
        this.processesByRecord.clear();

        super.close();

        return;
    }
}
