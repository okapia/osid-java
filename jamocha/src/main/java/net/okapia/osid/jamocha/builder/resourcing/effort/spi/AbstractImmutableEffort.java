//
// AbstractImmutableEffort.java
//
//     Wraps a mutable Effort to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.resourcing.effort.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Effort</code> to hide modifiers. This
 *  wrapper provides an immutized Effort from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying effort whose state changes are visible.
 */

public abstract class AbstractImmutableEffort
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.resourcing.Effort {

    private final org.osid.resourcing.Effort effort;


    /**
     *  Constructs a new <code>AbstractImmutableEffort</code>.
     *
     *  @param effort the effort to immutablize
     *  @throws org.osid.NullArgumentException <code>effort</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableEffort(org.osid.resourcing.Effort effort) {
        super(effort);
        this.effort = effort;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the resource. 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResourceId() {
        return (this.effort.getResourceId());
    }


    /**
     *  Gets the resource. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource()
        throws org.osid.OperationFailedException {

        return (this.effort.getResource());
    }


    /**
     *  Gets the <code> Id </code> of the commission. 
     *
     *  @return the commission <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCommissionId() {
        return (this.effort.getCommissionId());
    }


    /**
     *  Gets the commission. 
     *
     *  @return the commission 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resourcing.Commission getCommission()
        throws org.osid.OperationFailedException {

        return (this.effort.getCommission());
    }


    /**
     *  Gets the time spent on this commission. 
     *
     *  @return the time spent 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getTimeSpent() {
        return (this.effort.getTimeSpent());
    }


    /**
     *  Gets the effort record corresponding to the given <code> Effort 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> effortRecordType 
     *  </code> may be the <code> Type </code> returned in <code> 
     *  getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(effortRecordType) </code> 
     *  is <code> true </code> . 
     *
     *  @param  effortRecordType the type of effort record to retrieve 
     *  @return the effort record 
     *  @throws org.osid.NullArgumentException <code> effortRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(effortRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.records.EffortRecord getEffortRecord(org.osid.type.Type effortRecordType)
        throws org.osid.OperationFailedException {

        return (this.effort.getEffortRecord(effortRecordType));
    }
}

