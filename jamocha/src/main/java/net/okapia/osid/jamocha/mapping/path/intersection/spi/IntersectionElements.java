//
// IntersectionElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.path.intersection.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class IntersectionElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the IntersectionElement Id.
     *
     *  @return the intersection element Id
     */

    public static org.osid.id.Id getIntersectionEntityId() {
        return (makeEntityId("osid.mapping.path.Intersection"));
    }


    /**
     *  Gets the Coordinate element Id.
     *
     *  @return the Coordinate element Id
     */

    public static org.osid.id.Id getCoordinate() {
        return (makeElementId("osid.mapping.path.intersection.Coordinate"));
    }


    /**
     *  Gets the ContainingSpatialUnit element Id.
     *
     *  @return the ContainingSpatialUnit element Id
     */

    public static org.osid.id.Id getContainingSpatialUnit() {
        return (makeQueryElementId("osid.mapping.path.obstacle.ContainingSpatialUnit"));
    }


    /**
     *  Gets the PathIds element Id.
     *
     *  @return the PathIds element Id
     */

    public static org.osid.id.Id getPathIds() {
        return (makeElementId("osid.mapping.path.intersection.PathIds"));
    }


    /**
     *  Gets the Paths element Id.
     *
     *  @return the Paths element Id
     */

    public static org.osid.id.Id getPaths() {
        return (makeElementId("osid.mapping.path.intersection.Paths"));
    }


    /**
     *  Gets the Rotary element Id.
     *
     *  @return the Rotary element Id
     */

    public static org.osid.id.Id getRotary() {
        return (makeElementId("osid.mapping.path.intersection.Rotary"));
    }


    /**
     *  Gets the Fork element Id.
     *
     *  @return the Fork element Id
     */

    public static org.osid.id.Id getFork() {
        return (makeElementId("osid.mapping.path.intersection.Fork"));
    }


    /**
     *  Gets the MapId element Id.
     *
     *  @return the MapId element Id
     */

    public static org.osid.id.Id getMapId() {
        return (makeQueryElementId("osid.mapping.path.intersection.MapId"));
    }


    /**
     *  Gets the Map element Id.
     *
     *  @return the Map element Id
     */

    public static org.osid.id.Id getMap() {
        return (makeQueryElementId("osid.mapping.path.intersection.Map"));
    }
}
