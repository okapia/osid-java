//
// AbstractBidSearch.java
//
//     A template for making a Bid Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.bid.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing bid searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractBidSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.bidding.BidSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.bidding.records.BidSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.bidding.BidSearchOrder bidSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of bids. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  bidIds list of bids
     *  @throws org.osid.NullArgumentException
     *          <code>bidIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongBids(org.osid.id.IdList bidIds) {
        while (bidIds.hasNext()) {
            try {
                this.ids.add(bidIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongBids</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of bid Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getBidIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  bidSearchOrder bid search order 
     *  @throws org.osid.NullArgumentException
     *          <code>bidSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>bidSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderBidResults(org.osid.bidding.BidSearchOrder bidSearchOrder) {
	this.bidSearchOrder = bidSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.bidding.BidSearchOrder getBidSearchOrder() {
	return (this.bidSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given bid search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a bid implementing the requested record.
     *
     *  @param bidSearchRecordType a bid search record
     *         type
     *  @return the bid search record
     *  @throws org.osid.NullArgumentException
     *          <code>bidSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(bidSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.records.BidSearchRecord getBidSearchRecord(org.osid.type.Type bidSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.bidding.records.BidSearchRecord record : this.records) {
            if (record.implementsRecordType(bidSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(bidSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this bid search. 
     *
     *  @param bidSearchRecord bid search record
     *  @param bidSearchRecordType bid search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBidSearchRecord(org.osid.bidding.records.BidSearchRecord bidSearchRecord, 
                                           org.osid.type.Type bidSearchRecordType) {

        addRecordType(bidSearchRecordType);
        this.records.add(bidSearchRecord);        
        return;
    }
}
