//
// AbstractNodeStockHierarchySession.java
//
//     Defines a Stock hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inventory.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a stock hierarchy session for delivering a hierarchy
 *  of stocks using the StockNode interface.
 */

public abstract class AbstractNodeStockHierarchySession
    extends net.okapia.osid.jamocha.inventory.spi.AbstractStockHierarchySession
    implements org.osid.inventory.StockHierarchySession {

    private java.util.Collection<org.osid.inventory.StockNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root stock <code> Ids </code> in this hierarchy.
     *
     *  @return the root stock <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootStockIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.inventory.stocknode.StockNodeToIdList(this.roots));
    }


    /**
     *  Gets the root stocks in the stock hierarchy. A node
     *  with no parents is an orphan. While all stock <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root stocks 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.StockList getRootStocks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.inventory.stocknode.StockNodeToStockList(new net.okapia.osid.jamocha.inventory.stocknode.ArrayStockNodeList(this.roots)));
    }


    /**
     *  Adds a root stock node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootStock(org.osid.inventory.StockNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root stock nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootStocks(java.util.Collection<org.osid.inventory.StockNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root stock node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootStock(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.inventory.StockNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Stock </code> has any parents. 
     *
     *  @param  stockId a stock <code> Id </code> 
     *  @return <code> true </code> if the stock has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> stockId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> stockId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentStocks(org.osid.id.Id stockId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getStockNode(stockId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  stock.
     *
     *  @param  id an <code> Id </code> 
     *  @param  stockId the <code> Id </code> of a stock 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> stockId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> stockId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> stockId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfStock(org.osid.id.Id id, org.osid.id.Id stockId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.inventory.StockNodeList parents = getStockNode(stockId).getParentStockNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextStockNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given stock. 
     *
     *  @param  stockId a stock <code> Id </code> 
     *  @return the parent <code> Ids </code> of the stock 
     *  @throws org.osid.NotFoundException <code> stockId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> stockId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentStockIds(org.osid.id.Id stockId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.inventory.stock.StockToIdList(getParentStocks(stockId)));
    }


    /**
     *  Gets the parents of the given stock. 
     *
     *  @param  stockId the <code> Id </code> to query 
     *  @return the parents of the stock 
     *  @throws org.osid.NotFoundException <code> stockId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> stockId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.StockList getParentStocks(org.osid.id.Id stockId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.inventory.stocknode.StockNodeToStockList(getStockNode(stockId).getParentStockNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  stock.
     *
     *  @param  id an <code> Id </code> 
     *  @param  stockId the Id of a stock 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> stockId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> stockId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> stockId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfStock(org.osid.id.Id id, org.osid.id.Id stockId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfStock(id, stockId)) {
            return (true);
        }

        try (org.osid.inventory.StockList parents = getParentStocks(stockId)) {
            while (parents.hasNext()) {
                if (isAncestorOfStock(id, parents.getNextStock().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a stock has any children. 
     *
     *  @param  stockId a stock <code> Id </code> 
     *  @return <code> true </code> if the <code> stockId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> stockId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> stockId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildStocks(org.osid.id.Id stockId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getStockNode(stockId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  stock.
     *
     *  @param  id an <code> Id </code> 
     *  @param stockId the <code> Id </code> of a 
     *         stock
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> stockId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> stockId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> stockId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfStock(org.osid.id.Id id, org.osid.id.Id stockId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfStock(stockId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  stock.
     *
     *  @param  stockId the <code> Id </code> to query 
     *  @return the children of the stock 
     *  @throws org.osid.NotFoundException <code> stockId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> stockId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildStockIds(org.osid.id.Id stockId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.inventory.stock.StockToIdList(getChildStocks(stockId)));
    }


    /**
     *  Gets the children of the given stock. 
     *
     *  @param  stockId the <code> Id </code> to query 
     *  @return the children of the stock 
     *  @throws org.osid.NotFoundException <code> stockId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> stockId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.StockList getChildStocks(org.osid.id.Id stockId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.inventory.stocknode.StockNodeToStockList(getStockNode(stockId).getChildStockNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  stock.
     *
     *  @param  id an <code> Id </code> 
     *  @param stockId the <code> Id </code> of a 
     *         stock
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> stockId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> stockId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> stockId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfStock(org.osid.id.Id id, org.osid.id.Id stockId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfStock(stockId, id)) {
            return (true);
        }

        try (org.osid.inventory.StockList children = getChildStocks(stockId)) {
            while (children.hasNext()) {
                if (isDescendantOfStock(id, children.getNextStock().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  stock.
     *
     *  @param  stockId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified stock node 
     *  @throws org.osid.NotFoundException <code> stockId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> stockId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getStockNodeIds(org.osid.id.Id stockId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.inventory.stocknode.StockNodeToNode(getStockNode(stockId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given stock.
     *
     *  @param  stockId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified stock node 
     *  @throws org.osid.NotFoundException <code> stockId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> stockId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.StockNode getStockNodes(org.osid.id.Id stockId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getStockNode(stockId));
    }


    /**
     *  Closes this <code>StockHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a stock node.
     *
     *  @param stockId the id of the stock node
     *  @throws org.osid.NotFoundException <code>stockId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>stockId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.inventory.StockNode getStockNode(org.osid.id.Id stockId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(stockId, "stock Id");
        for (org.osid.inventory.StockNode stock : this.roots) {
            if (stock.getId().equals(stockId)) {
                return (stock);
            }

            org.osid.inventory.StockNode r = findStock(stock, stockId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(stockId + " is not found");
    }


    protected org.osid.inventory.StockNode findStock(org.osid.inventory.StockNode node, 
                                                     org.osid.id.Id stockId) 
	throws org.osid.OperationFailedException {

        try (org.osid.inventory.StockNodeList children = node.getChildStockNodes()) {
            while (children.hasNext()) {
                org.osid.inventory.StockNode stock = children.getNextStockNode();
                if (stock.getId().equals(stockId)) {
                    return (stock);
                }
                
                stock = findStock(stock, stockId);
                if (stock != null) {
                    return (stock);
                }
            }
        }

        return (null);
    }
}
