//
// AbstractIndexedMapRequisiteLookupSession.java
//
//    A simple framework for providing a Requisite lookup service
//    backed by a fixed collection of requisites with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.requisite.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Requisite lookup service backed by a
 *  fixed collection of requisites. The requisites are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some requisites may be compatible
 *  with more types than are indicated through these requisite
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Requisites</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapRequisiteLookupSession
    extends AbstractMapRequisiteLookupSession
    implements org.osid.course.requisite.RequisiteLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.course.requisite.Requisite> requisitesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.requisite.Requisite>());
    private final MultiMap<org.osid.type.Type, org.osid.course.requisite.Requisite> requisitesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.requisite.Requisite>());


    /**
     *  Makes a <code>Requisite</code> available in this session.
     *
     *  @param  requisite a requisite
     *  @throws org.osid.NullArgumentException <code>requisite<code> is
     *          <code>null</code>
     */

    @Override
    protected void putRequisite(org.osid.course.requisite.Requisite requisite) {
        super.putRequisite(requisite);

        this.requisitesByGenus.put(requisite.getGenusType(), requisite);
        
        try (org.osid.type.TypeList types = requisite.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.requisitesByRecord.put(types.getNextType(), requisite);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a requisite from this session.
     *
     *  @param requisiteId the <code>Id</code> of the requisite
     *  @throws org.osid.NullArgumentException <code>requisiteId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeRequisite(org.osid.id.Id requisiteId) {
        org.osid.course.requisite.Requisite requisite;
        try {
            requisite = getRequisite(requisiteId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.requisitesByGenus.remove(requisite.getGenusType());

        try (org.osid.type.TypeList types = requisite.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.requisitesByRecord.remove(types.getNextType(), requisite);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeRequisite(requisiteId);
        return;
    }


    /**
     *  Gets a <code>RequisiteList</code> corresponding to the given
     *  requisite genus <code>Type</code> which does not include
     *  requisites of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known requisites or an error results. Otherwise,
     *  the returned list may contain only those requisites that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  requisiteGenusType a requisite genus type 
     *  @return the returned <code>Requisite</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>requisiteGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getRequisitesByGenusType(org.osid.type.Type requisiteGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.requisite.requisite.ArrayRequisiteList(this.requisitesByGenus.get(requisiteGenusType)));
    }


    /**
     *  Gets a <code>RequisiteList</code> containing the given
     *  requisite record <code>Type</code>. In plenary mode, the
     *  returned list contains all known requisites or an error
     *  results. Otherwise, the returned list may contain only those
     *  requisites that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  requisiteRecordType a requisite record type 
     *  @return the returned <code>requisite</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>requisiteRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getRequisitesByRecordType(org.osid.type.Type requisiteRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.requisite.requisite.ArrayRequisiteList(this.requisitesByRecord.get(requisiteRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.requisitesByGenus.clear();
        this.requisitesByRecord.clear();

        super.close();

        return;
    }
}
