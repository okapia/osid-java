//
// AbstractContactEnablerSearch.java
//
//     A template for making a ContactEnabler Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.contact.rules.contactenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing contact enabler searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractContactEnablerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.contact.rules.ContactEnablerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.contact.rules.records.ContactEnablerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.contact.rules.ContactEnablerSearchOrder contactEnablerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of contact enablers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  contactEnablerIds list of contact enablers
     *  @throws org.osid.NullArgumentException
     *          <code>contactEnablerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongContactEnablers(org.osid.id.IdList contactEnablerIds) {
        while (contactEnablerIds.hasNext()) {
            try {
                this.ids.add(contactEnablerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongContactEnablers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of contact enabler Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getContactEnablerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  contactEnablerSearchOrder contact enabler search order 
     *  @throws org.osid.NullArgumentException
     *          <code>contactEnablerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>contactEnablerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderContactEnablerResults(org.osid.contact.rules.ContactEnablerSearchOrder contactEnablerSearchOrder) {
	this.contactEnablerSearchOrder = contactEnablerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.contact.rules.ContactEnablerSearchOrder getContactEnablerSearchOrder() {
	return (this.contactEnablerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given contact enabler search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a contact enabler implementing the requested record.
     *
     *  @param contactEnablerSearchRecordType a contact enabler search record
     *         type
     *  @return the contact enabler search record
     *  @throws org.osid.NullArgumentException
     *          <code>contactEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(contactEnablerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.contact.rules.records.ContactEnablerSearchRecord getContactEnablerSearchRecord(org.osid.type.Type contactEnablerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.contact.rules.records.ContactEnablerSearchRecord record : this.records) {
            if (record.implementsRecordType(contactEnablerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(contactEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this contact enabler search. 
     *
     *  @param contactEnablerSearchRecord contact enabler search record
     *  @param contactEnablerSearchRecordType contactEnabler search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addContactEnablerSearchRecord(org.osid.contact.rules.records.ContactEnablerSearchRecord contactEnablerSearchRecord, 
                                           org.osid.type.Type contactEnablerSearchRecordType) {

        addRecordType(contactEnablerSearchRecordType);
        this.records.add(contactEnablerSearchRecord);        
        return;
    }
}
