//
// MutableMapHierarchyLookupSession
//
//    Implements a Hierarchy lookup service backed by a collection of
//    hierarchies that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.hierarchy;


/**
 *  Implements a Hierarchy lookup service backed by a collection of
 *  hierarchies. The hierarchies are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of hierarchies can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapHierarchyLookupSession
    extends net.okapia.osid.jamocha.core.hierarchy.spi.AbstractMapHierarchyLookupSession
    implements org.osid.hierarchy.HierarchyLookupSession {


    /**
     *  Constructs a new {@code MutableMapHierarchyLookupSession}
     *  with no hierarchies.
     */

    public MutableMapHierarchyLookupSession() {
        return;
    }


    /**
     *  Constructs a new {@code MutableMapHierarchyLookupSession} with a
     *  single hierarchy.
     *  
     *  @param hierarchy a hierarchy
     *  @throws org.osid.NullArgumentException {@code hierarchy}
     *          is {@code null}
     */

    public MutableMapHierarchyLookupSession(org.osid.hierarchy.Hierarchy hierarchy) {
        putHierarchy(hierarchy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapHierarchyLookupSession}
     *  using an array of hierarchies.
     *
     *  @param hierarchies an array of hierarchies
     *  @throws org.osid.NullArgumentException {@code hierarchies}
     *          is {@code null}
     */

    public MutableMapHierarchyLookupSession(org.osid.hierarchy.Hierarchy[] hierarchies) {
        putHierarchies(hierarchies);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapHierarchyLookupSession}
     *  using a collection of hierarchies.
     *
     *  @param hierarchies a collection of hierarchies
     *  @throws org.osid.NullArgumentException {@code hierarchies}
     *          is {@code null}
     */

    public MutableMapHierarchyLookupSession(java.util.Collection<? extends org.osid.hierarchy.Hierarchy> hierarchies) {
        putHierarchies(hierarchies);
        return;
    }

    
    /**
     *  Makes a {@code Hierarchy} available in this session.
     *
     *  @param hierarchy a hierarchy
     *  @throws org.osid.NullArgumentException {@code hierarchy{@code  is
     *          {@code null}
     */

    @Override
    public void putHierarchy(org.osid.hierarchy.Hierarchy hierarchy) {
        super.putHierarchy(hierarchy);
        return;
    }


    /**
     *  Makes an array of hierarchies available in this session.
     *
     *  @param hierarchies an array of hierarchies
     *  @throws org.osid.NullArgumentException {@code hierarchies{@code 
     *          is {@code null}
     */

    @Override
    public void putHierarchies(org.osid.hierarchy.Hierarchy[] hierarchies) {
        super.putHierarchies(hierarchies);
        return;
    }


    /**
     *  Makes collection of hierarchies available in this session.
     *
     *  @param hierarchies a collection of hierarchies
     *  @throws org.osid.NullArgumentException {@code hierarchies{@code  is
     *          {@code null}
     */

    @Override
    public void putHierarchies(java.util.Collection<? extends org.osid.hierarchy.Hierarchy> hierarchies) {
        super.putHierarchies(hierarchies);
        return;
    }


    /**
     *  Removes a Hierarchy from this session.
     *
     *  @param hierarchyId the {@code Id} of the hierarchy
     *  @throws org.osid.NullArgumentException {@code hierarchyId{@code 
     *          is {@code null}
     */

    @Override
    public void removeHierarchy(org.osid.id.Id hierarchyId) {
        super.removeHierarchy(hierarchyId);
        return;
    }    
}
