//
// AbstractAssemblyOfficeQuery.java
//
//     An OfficeQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.workflow.office.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An OfficeQuery that stores terms.
 */

public abstract class AbstractAssemblyOfficeQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.workflow.OfficeQuery,
               org.osid.workflow.OfficeQueryInspector,
               org.osid.workflow.OfficeSearchOrder {

    private final java.util.Collection<org.osid.workflow.records.OfficeQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.workflow.records.OfficeQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.workflow.records.OfficeSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyOfficeQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyOfficeQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the step <code> Id </code> for this query to match offices 
     *  containing process. 
     *
     *  @param  processId the process <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> processId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProcessId(org.osid.id.Id processId, boolean match) {
        getAssembler().addIdTerm(getProcessIdColumn(), processId, match);
        return;
    }


    /**
     *  Clears the process query terms. 
     */

    @OSID @Override
    public void clearProcessIdTerms() {
        getAssembler().clearTerms(getProcessIdColumn());
        return;
    }


    /**
     *  Gets the process <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProcessIdTerms() {
        return (getAssembler().getIdTerms(getProcessIdColumn()));
    }


    /**
     *  Gets the ProcessId column name.
     *
     * @return the column name
     */

    protected String getProcessIdColumn() {
        return ("process_id");
    }


    /**
     *  Tests if a <code> ProcessQuery </code> is available. 
     *
     *  @return <code> true </code> if a process query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a process. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the process query 
     *  @throws org.osid.UnimplementedException <code> supportsProcessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessQuery getProcessQuery() {
        throw new org.osid.UnimplementedException("supportsProcessQuery() is false");
    }


    /**
     *  Matches offices that have any process. 
     *
     *  @param  match <code> true </code> to match offices with any process, 
     *          <code> false </code> to match offices with no process 
     */

    @OSID @Override
    public void matchAnyProcess(boolean match) {
        getAssembler().addIdWildcardTerm(getProcessColumn(), match);
        return;
    }


    /**
     *  Clears the process query terms. 
     */

    @OSID @Override
    public void clearProcessTerms() {
        getAssembler().clearTerms(getProcessColumn());
        return;
    }


    /**
     *  Gets the process query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.workflow.ProcessQueryInspector[] getProcessTerms() {
        return (new org.osid.workflow.ProcessQueryInspector[0]);
    }


    /**
     *  Gets the Process column name.
     *
     * @return the column name
     */

    protected String getProcessColumn() {
        return ("process");
    }


    /**
     *  Sets the step <code> Id </code> for this query. 
     *
     *  @param  stepId the step <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stepId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchStepId(org.osid.id.Id stepId, boolean match) {
        getAssembler().addIdTerm(getStepIdColumn(), stepId, match);
        return;
    }


    /**
     *  Clears the step <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearStepIdTerms() {
        getAssembler().clearTerms(getStepIdColumn());
        return;
    }


    /**
     *  Gets the step <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStepIdTerms() {
        return (getAssembler().getIdTerms(getStepIdColumn()));
    }


    /**
     *  Gets the StepId column name.
     *
     * @return the column name
     */

    protected String getStepIdColumn() {
        return ("step_id");
    }


    /**
     *  Tests if an <code> StepQuery </code> is available. 
     *
     *  @return <code> true </code> if an step query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepQuery() {
        return (false);
    }


    /**
     *  Gets the query for an step. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the step query 
     *  @throws org.osid.UnimplementedException <code> supportsStepQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepQuery getStepQuery() {
        throw new org.osid.UnimplementedException("supportsStepQuery() is false");
    }


    /**
     *  Matches offices with any step. 
     *
     *  @param  match <code> true </code> to match offices with any step, 
     *          <code> false </code> to match offices with no step 
     */

    @OSID @Override
    public void matchAnyStep(boolean match) {
        getAssembler().addIdWildcardTerm(getStepColumn(), match);
        return;
    }


    /**
     *  Clears the step query terms. 
     */

    @OSID @Override
    public void clearStepTerms() {
        getAssembler().clearTerms(getStepColumn());
        return;
    }


    /**
     *  Gets the step query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.workflow.StepQueryInspector[] getStepTerms() {
        return (new org.osid.workflow.StepQueryInspector[0]);
    }


    /**
     *  Gets the Step column name.
     *
     * @return the column name
     */

    protected String getStepColumn() {
        return ("step");
    }


    /**
     *  Sets the work <code> Id </code> for this query. 
     *
     *  @param  workId the work <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> workId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchWorkId(org.osid.id.Id workId, boolean match) {
        getAssembler().addIdTerm(getWorkIdColumn(), workId, match);
        return;
    }


    /**
     *  Clears the work <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearWorkIdTerms() {
        getAssembler().clearTerms(getWorkIdColumn());
        return;
    }


    /**
     *  Gets the work <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getWorkIdTerms() {
        return (getAssembler().getIdTerms(getWorkIdColumn()));
    }


    /**
     *  Gets the WorkId column name.
     *
     * @return the column name
     */

    protected String getWorkIdColumn() {
        return ("work_id");
    }


    /**
     *  Tests if a <code> WorkQuery </code> is available. 
     *
     *  @return <code> true </code> if a work query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkQuery() {
        return (false);
    }


    /**
     *  Gets the query for a work. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the work query 
     *  @throws org.osid.UnimplementedException <code> supportsWorkQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkQuery getWorkQuery() {
        throw new org.osid.UnimplementedException("supportsWorkQuery() is false");
    }


    /**
     *  Matches offices that have any work. 
     *
     *  @param  match <code> true </code> to match offices with any work, 
     *          <code> false </code> to match offices with no process 
     */

    @OSID @Override
    public void matchAnyWork(boolean match) {
        getAssembler().addIdWildcardTerm(getWorkColumn(), match);
        return;
    }


    /**
     *  Clears the work query terms. 
     */

    @OSID @Override
    public void clearWorkTerms() {
        getAssembler().clearTerms(getWorkColumn());
        return;
    }


    /**
     *  Gets the work query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.workflow.WorkQueryInspector[] getWorkTerms() {
        return (new org.osid.workflow.WorkQueryInspector[0]);
    }


    /**
     *  Gets the Work column name.
     *
     * @return the column name
     */

    protected String getWorkColumn() {
        return ("work");
    }


    /**
     *  Sets the office <code> Id </code> for this query to match offices that 
     *  have the specified office as an ancestor. 
     *
     *  @param  officeId a office <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorOfficeId(org.osid.id.Id officeId, boolean match) {
        getAssembler().addIdTerm(getAncestorOfficeIdColumn(), officeId, match);
        return;
    }


    /**
     *  Clears the ancestor office <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorOfficeIdTerms() {
        getAssembler().clearTerms(getAncestorOfficeIdColumn());
        return;
    }


    /**
     *  Gets the ancestor office <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorOfficeIdTerms() {
        return (getAssembler().getIdTerms(getAncestorOfficeIdColumn()));
    }


    /**
     *  Gets the AncestorOfficeId column name.
     *
     * @return the column name
     */

    protected String getAncestorOfficeIdColumn() {
        return ("ancestor_office_id");
    }


    /**
     *  Tests if a <code> OfficeQuery </code> is available. 
     *
     *  @return <code> true </code> if a office query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorOfficeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a office/ Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the office query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorOfficeQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.OfficeQuery getAncestorOfficeQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorOfficeQuery() is false");
    }


    /**
     *  Matches offices with any ancestor. 
     *
     *  @param  match <code> true </code> to match offices with any ancestor, 
     *          <code> false </code> to match root offices 
     */

    @OSID @Override
    public void matchAnyAncestorOffice(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorOfficeColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor office query terms. 
     */

    @OSID @Override
    public void clearAncestorOfficeTerms() {
        getAssembler().clearTerms(getAncestorOfficeColumn());
        return;
    }


    /**
     *  Gets the ancestor office query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.workflow.OfficeQueryInspector[] getAncestorOfficeTerms() {
        return (new org.osid.workflow.OfficeQueryInspector[0]);
    }


    /**
     *  Gets the AncestorOffice column name.
     *
     * @return the column name
     */

    protected String getAncestorOfficeColumn() {
        return ("ancestor_office");
    }


    /**
     *  Sets the office <code> Id </code> for this query to match offices that 
     *  have the specified office as a descendant. 
     *
     *  @param  officeId a office <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantOfficeId(org.osid.id.Id officeId, boolean match) {
        getAssembler().addIdTerm(getDescendantOfficeIdColumn(), officeId, match);
        return;
    }


    /**
     *  Clears the descendant office <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantOfficeIdTerms() {
        getAssembler().clearTerms(getDescendantOfficeIdColumn());
        return;
    }


    /**
     *  Gets the descendant office <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantOfficeIdTerms() {
        return (getAssembler().getIdTerms(getDescendantOfficeIdColumn()));
    }


    /**
     *  Gets the DescendantOfficeId column name.
     *
     * @return the column name
     */

    protected String getDescendantOfficeIdColumn() {
        return ("descendant_office_id");
    }


    /**
     *  Tests if a <code> OfficeQuery </code> is available. 
     *
     *  @return <code> true </code> if a office query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantOfficeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a office/ Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the office query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantOfficeQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.OfficeQuery getDescendantOfficeQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantOfficeQuery() is false");
    }


    /**
     *  Matches offices with any descendant. 
     *
     *  @param  match <code> true </code> to match offices with any 
     *          descendant, <code> false </code> to match leaf offices 
     */

    @OSID @Override
    public void matchAnyDescendantOffice(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantOfficeColumn(), match);
        return;
    }


    /**
     *  Clears the descendant office query terms. 
     */

    @OSID @Override
    public void clearDescendantOfficeTerms() {
        getAssembler().clearTerms(getDescendantOfficeColumn());
        return;
    }


    /**
     *  Gets the descendant office query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.workflow.OfficeQueryInspector[] getDescendantOfficeTerms() {
        return (new org.osid.workflow.OfficeQueryInspector[0]);
    }


    /**
     *  Gets the DescendantOffice column name.
     *
     * @return the column name
     */

    protected String getDescendantOfficeColumn() {
        return ("descendant_office");
    }


    /**
     *  Tests if this office supports the given record
     *  <code>Type</code>.
     *
     *  @param  officeRecordType an office record type 
     *  @return <code>true</code> if the officeRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>officeRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type officeRecordType) {
        for (org.osid.workflow.records.OfficeQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(officeRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  officeRecordType the office record type 
     *  @return the office query record 
     *  @throws org.osid.NullArgumentException
     *          <code>officeRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(officeRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.records.OfficeQueryRecord getOfficeQueryRecord(org.osid.type.Type officeRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.workflow.records.OfficeQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(officeRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(officeRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  officeRecordType the office record type 
     *  @return the office query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>officeRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(officeRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.records.OfficeQueryInspectorRecord getOfficeQueryInspectorRecord(org.osid.type.Type officeRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.workflow.records.OfficeQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(officeRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(officeRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param officeRecordType the office record type
     *  @return the office search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>officeRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(officeRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.records.OfficeSearchOrderRecord getOfficeSearchOrderRecord(org.osid.type.Type officeRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.workflow.records.OfficeSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(officeRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(officeRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this office. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param officeQueryRecord the office query record
     *  @param officeQueryInspectorRecord the office query inspector
     *         record
     *  @param officeSearchOrderRecord the office search order record
     *  @param officeRecordType office record type
     *  @throws org.osid.NullArgumentException
     *          <code>officeQueryRecord</code>,
     *          <code>officeQueryInspectorRecord</code>,
     *          <code>officeSearchOrderRecord</code> or
     *          <code>officeRecordTypeoffice</code> is
     *          <code>null</code>
     */
            
    protected void addOfficeRecords(org.osid.workflow.records.OfficeQueryRecord officeQueryRecord, 
                                      org.osid.workflow.records.OfficeQueryInspectorRecord officeQueryInspectorRecord, 
                                      org.osid.workflow.records.OfficeSearchOrderRecord officeSearchOrderRecord, 
                                      org.osid.type.Type officeRecordType) {

        addRecordType(officeRecordType);

        nullarg(officeQueryRecord, "office query record");
        nullarg(officeQueryInspectorRecord, "office query inspector record");
        nullarg(officeSearchOrderRecord, "office search odrer record");

        this.queryRecords.add(officeQueryRecord);
        this.queryInspectorRecords.add(officeQueryInspectorRecord);
        this.searchOrderRecords.add(officeSearchOrderRecord);
        
        return;
    }
}
