//
// AbstractAssemblyOsidSourceableQuery.java
//
//     An OsidSourceableQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An OsidSourceableQuery that stores terms.
 */

public abstract class AbstractAssemblyOsidSourceableQuery
    extends AbstractAssemblyOsidQuery
    implements org.osid.OsidSourceableQuery,
               org.osid.OsidSourceableQueryInspector,
               org.osid.OsidSourceableSearchOrder {

    
    /** 
     *  Constructs a new
     *  <code>AbstractAssemblyOsidSourceableQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyOsidSourceableQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Match the <code> Id </code> of the provider resource. 
     *
     *  @param  resourceId <code> Id </code> to match 
     *  @param match <code> true </code> if for a positive match,
     *          <code> false </code> for a negative match
     *  @throws org.osid.NullArgumentException <code> resourceId
     *          </code> is <code> null </code>
     */

    @OSID @Override
    public void matchProviderId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getProviderIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears all provider <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearProviderIdTerms() {
        getAssembler().clearTerms(getProviderIdColumn());
        return;
    }


    /**
     *  Gets the provider <code> Id </code> query terms. 
     *
     *  @return the provider <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProviderIdTerms() {
        return (getAssembler().getIdTerms(getProviderIdColumn()));
    }


    /**
     *  Gets the column name for the provider Id field.
     *
     *  @return the column name
     */

    protected String getProviderIdColumn() {
        return ("provider_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> for the provider is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProviderQuery() {
        return (false);
    }


    /**
     *  Gets the query for the provider. Each retrieval performs a boolean 
     *  <code> OR. </code> 
     *
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @return the provider query 
     *  @throws org.osid.UnimplementedException <code> supportsProviderQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getProviderQuery(boolean match) {
        throw new org.osid.UnimplementedException("supportsProviderQuery() is false");
    }


    /**
     *  Match sourceables with a provider value. 
     *
     *  @param  match <code> true </code> to match sourceables with any 
     *          provider, <code> false </code> to match sourceables with no 
     *          providers 
     */

    @OSID @Override
    public void matchAnyProvider(boolean match) {
        getAssembler().addIdWildcardTerm(getProviderIdColumn(), match);
        return;
    }


    /**
     *  Clears all provider terms. 
     */

    @OSID @Override
    public void clearProviderTerms() {
        getAssembler().clearTerms(getProviderColumn());
        return;
    }


    /**
     *  Gets the provider query terms. 
     *
     *  @return the provider terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getProviderTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Specifies a preference for ordering the results by provider. The 
     *  element of the provider to order is not specified but may be managed 
     *  through the provider ordering interface. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByProvider(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getProviderColumn(), style);
        return;
    }


    /**
     *  Tests if a <code> ProviderSearchOrder </code> interface is available. 
     *
     *  @return <code> true </code> if a provider search order interface is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProviderSearchOrder() {
        return (false);
    }

    
    /**
     *  Gets the search order interface for a provider. 
     *
     *  @return the provider search order interface 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProviderSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getProviderSearchOrder() {
        throw new org.osid.UnimplementedException("supportsProviderSearchOrder() is false");
    }


    /**
     *  Gets the column name for the providerfield.
     *
     *  @return the column name
     */

    protected String getProviderColumn() {
        return ("provider");
    }


    /**
     *  Match the <code> Id </code> of an asset used for branding. 
     *
     *  @param  assetId <code> Id </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assetId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBrandingId(org.osid.id.Id assetId, boolean match) {
        getAssembler().addIdTerm(getBrandingIdColumn(), assetId, match);
        return;
    }


    /**
     *  Clears all asset <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBrandingIdTerms() {
        getAssembler().clearTerms(getBrandingIdColumn());
        return;
    }


    /**
     *  Gets the asset <code> Id </code> query terms. 
     *
     *  @return the asset <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBrandingIdTerms() {
        return (getAssembler().getIdTerms(getBrandingIdColumn()));
    }


    /**
     *  Gets the column name for the branding Id field.
     *
     *  @return the column name
     */

    protected String getBrandingIdColumn() {
        return ("branding_id");
    }


    /**
     *  Tests if an <code> AssetQuery </code> for the branding is available. 
     *
     *  @return <code> true </code> if a asset query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrandingQuery() {
        return (false);
    }


    /**
     *  Gets the query for an asset. Each retrieval performs a boolean <code> 
     *  OR. </code> 
     *
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @return the asset query 
     *  @throws org.osid.UnimplementedException <code> supportsBrandingQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetQuery getBrandingQuery(boolean match) {
        throw new org.osid.UnimplementedException("supportsBrandingQuery() is false");
    }


    /**
     *  Match sourceables with any branding. 
     *
     *  @param  match <code> true </code> to match any asset, <code> false 
     *          </code> to match no assets 
     */

    @OSID @Override
    public void matchAnyBranding(boolean match) {
        getAssembler().addIdWildcardTerm(getBrandingColumn(), match);
        return;
    }


    /**
     *  Clears all branding terms. 
     */

    @OSID @Override
    public void clearBrandingTerms() {
        getAssembler().clearTerms(getBrandingColumn());
        return;
    }


    /**
     *  Gets the asset query terms. 
     *
     *  @return the branding terms 
     */

    @OSID @Override
    public org.osid.repository.AssetQueryInspector[] getBrandingTerms() {
        return (new org.osid.repository.AssetQueryInspector[0]);
    }


    /**
     *  Gets the column name for the branding field.
     *
     *  @return the column name
     */

    protected String getBrandingColumn() {
        return ("branding");
    }


    /**
     *  Adds a license to match. Multiple license matches can be added to 
     *  perform a boolean <code> OR </code> among them. 
     *
     *  @param  license a string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> license </code> is 
     *          not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> license </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchLicense(String license, 
                             org.osid.type.Type stringMatchType, boolean match) {
        getAssembler().addStringTerm(getLicenseColumn(), license, stringMatchType, match);
        return;
    }


    /**
     *  Matches any object with a license. 
     *
     *  @param  match <code> true </code> to match any license, <code> false 
     *          </code> to match objects with no license 
     */

    @OSID @Override
    public void matchAnyLicense(boolean match) {
        getAssembler().addStringWildcardTerm(getLicenseColumn(), match);
        return;
    }

    
    /**
     *  Clears all license terms. 
     */

    @OSID @Override
    public void clearLicenseTerms() {
        getAssembler().clearTerms(getLicenseColumn());
        return;
    }


    /**
     *  Gets the license query terms. 
     *
     *  @return the license terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getLicenseTerms() {
        return (getAssembler().getStringTerms(getLicenseColumn()));
    }
    

    /**
     *  Gets the column name for the license field.
     *
     *  @return the column name
     */

    protected String getLicenseColumn() {
        return ("license");
    }
}
