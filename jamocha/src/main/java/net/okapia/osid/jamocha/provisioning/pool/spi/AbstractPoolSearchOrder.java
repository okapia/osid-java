//
// AbstractPoolSearchOdrer.java
//
//     Defines a PoolSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.pool.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code PoolSearchOrder}.
 */

public abstract class AbstractPoolSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidGovernatorSearchOrder
    implements org.osid.provisioning.PoolSearchOrder {

    private final java.util.Collection<org.osid.provisioning.records.PoolSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Orders the results by broker. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBroker(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a broker search order is available. 
     *
     *  @return <code> true </code> if a broker search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerSearchOrder() {
        return (false);
    }


    /**
     *  Gets the broker search order. 
     *
     *  @return the broker search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsBrokerSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerSearchOrder getBrokerSearchOrder() {
        throw new org.osid.UnimplementedException("supportsBrokerSearchOrder() is false");
    }


    /**
     *  Orders the results by pool size. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySize(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  poolRecordType a pool record type 
     *  @return {@code true} if the poolRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code poolRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type poolRecordType) {
        for (org.osid.provisioning.records.PoolSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(poolRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  poolRecordType the pool record type 
     *  @return the pool search order record
     *  @throws org.osid.NullArgumentException
     *          {@code poolRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(poolRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.provisioning.records.PoolSearchOrderRecord getPoolSearchOrderRecord(org.osid.type.Type poolRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.PoolSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(poolRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(poolRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this pool. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param poolRecord the pool search odrer record
     *  @param poolRecordType pool record type
     *  @throws org.osid.NullArgumentException
     *          {@code poolRecord} or
     *          {@code poolRecordTypepool} is
     *          {@code null}
     */
            
    protected void addPoolRecord(org.osid.provisioning.records.PoolSearchOrderRecord poolSearchOrderRecord, 
                                     org.osid.type.Type poolRecordType) {

        addRecordType(poolRecordType);
        this.records.add(poolSearchOrderRecord);
        
        return;
    }
}
