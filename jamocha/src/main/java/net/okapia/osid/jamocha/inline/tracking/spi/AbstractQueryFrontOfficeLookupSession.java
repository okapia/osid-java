//
// AbstractQueryFrontOfficeLookupSession.java
//
//    An inline adapter that maps a FrontOfficeLookupSession to
//    a FrontOfficeQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.tracking.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a FrontOfficeLookupSession to
 *  a FrontOfficeQuerySession.
 */

public abstract class AbstractQueryFrontOfficeLookupSession
    extends net.okapia.osid.jamocha.tracking.spi.AbstractFrontOfficeLookupSession
    implements org.osid.tracking.FrontOfficeLookupSession {

    private final org.osid.tracking.FrontOfficeQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryFrontOfficeLookupSession.
     *
     *  @param querySession the underlying front office query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryFrontOfficeLookupSession(org.osid.tracking.FrontOfficeQuerySession querySession) {
        nullarg(querySession, "front office query session");
        this.session = querySession;
        return;
    }



    /**
     *  Tests if this user can perform <code>FrontOffice</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupFrontOffices() {
        return (this.session.canSearchFrontOffices());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }

     
    /**
     *  Gets the <code>FrontOffice</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>FrontOffice</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>FrontOffice</code> and
     *  retained for compatibility.
     *
     *  @param  frontOfficeId <code>Id</code> of the
     *          <code>FrontOffice</code>
     *  @return the front office
     *  @throws org.osid.NotFoundException <code>frontOfficeId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>frontOfficeId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOffice getFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.tracking.FrontOfficeQuery query = getQuery();
        query.matchId(frontOfficeId, true);
        org.osid.tracking.FrontOfficeList frontOffices = this.session.getFrontOfficesByQuery(query);
        if (frontOffices.hasNext()) {
            return (frontOffices.getNextFrontOffice());
        } 
        
        throw new org.osid.NotFoundException(frontOfficeId + " not found");
    }


    /**
     *  Gets a <code>FrontOfficeList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  frontOffices specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>FrontOffices</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  frontOfficeIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>FrontOffice</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>frontOfficeIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeList getFrontOfficesByIds(org.osid.id.IdList frontOfficeIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.tracking.FrontOfficeQuery query = getQuery();

        try (org.osid.id.IdList ids = frontOfficeIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getFrontOfficesByQuery(query));
    }


    /**
     *  Gets a <code>FrontOfficeList</code> corresponding to the given
     *  front office genus <code>Type</code> which does not include
     *  front offices of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known front
     *  offices or an error results. Otherwise, the returned list may
     *  contain only those front offices that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  frontOfficeGenusType a frontOffice genus type 
     *  @return the returned <code>FrontOffice</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>frontOfficeGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeList getFrontOfficesByGenusType(org.osid.type.Type frontOfficeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.tracking.FrontOfficeQuery query = getQuery();
        query.matchGenusType(frontOfficeGenusType, true);
        return (this.session.getFrontOfficesByQuery(query));
    }


    /**
     *  Gets a <code>FrontOfficeList</code> corresponding to the given
     *  front office genus <code>Type</code> and include any
     *  additional front offices with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known front
     *  offices or an error results. Otherwise, the returned list may
     *  contain only those front offices that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  frontOfficeGenusType a frontOffice genus type 
     *  @return the returned <code>FrontOffice</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>frontOfficeGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeList getFrontOfficesByParentGenusType(org.osid.type.Type frontOfficeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.tracking.FrontOfficeQuery query = getQuery();
        query.matchParentGenusType(frontOfficeGenusType, true);
        return (this.session.getFrontOfficesByQuery(query));
    }


    /**
     *  Gets a <code>FrontOfficeList</code> containing the given
     *  front office record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known front
     *  offices or an error results. Otherwise, the returned list may
     *  contain only those front offices that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  frontOfficeRecordType a frontOffice record type 
     *  @return the returned <code>FrontOffice</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>frontOfficeRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeList getFrontOfficesByRecordType(org.osid.type.Type frontOfficeRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.tracking.FrontOfficeQuery query = getQuery();
        query.matchRecordType(frontOfficeRecordType, true);
        return (this.session.getFrontOfficesByQuery(query));
    }


    /**
     *  Gets a <code>FrontOfficeList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known front offices or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  front offices that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>FrontOffice</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeList getFrontOfficesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.tracking.FrontOfficeQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getFrontOfficesByQuery(query));        
    }

    
    /**
     *  Gets all <code>FrontOffices</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  front offices or an error results. Otherwise, the returned list
     *  may contain only those front offices that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>FrontOffices</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeList getFrontOffices()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.tracking.FrontOfficeQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getFrontOfficesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.tracking.FrontOfficeQuery getQuery() {
        org.osid.tracking.FrontOfficeQuery query = this.session.getFrontOfficeQuery();
        return (query);
    }
}
