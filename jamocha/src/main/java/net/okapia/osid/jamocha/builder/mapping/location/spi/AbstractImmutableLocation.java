//
// AbstractImmutableLocation.java
//
//     Wraps a mutable Location to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.mapping.location.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Location</code> to hide modifiers. This
 *  wrapper provides an immutized Location from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying location whose state changes are visible.
 */

public abstract class AbstractImmutableLocation
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.mapping.Location {

    private final org.osid.mapping.Location location;


    /**
     *  Constructs a new <code>AbstractImmutableLocation</code>.
     *
     *  @param location the location to immutablize
     *  @throws org.osid.NullArgumentException <code>location</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableLocation(org.osid.mapping.Location location) {
        super(location);
        this.location = location;
        return;
    }


    /**
     *  Tests if a spatial unit is available for this location. 
     *
     *  @return <code> true </code> if a spatial unit is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasSpatialUnit() {
        return (this.location.hasSpatialUnit());
    }


    /**
     *  Gets the spatial unit corresponding to this location. 
     *
     *  @return the spatial unit for this location 
     *  @throws org.osid.IllegalStateException <code> hasSpatialUnit() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.SpatialUnit getSpatialUnit() {
        return (this.location.getSpatialUnit());
    }


    /**
     *  Gets the location record corresponding to the given <code> Location 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record.. The <code> 
     *  locationRecordType </code> may be the <code> Type </code> returned in 
     *  <code> getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(locationRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  locationRecordType the type of location record to retrieve 
     *  @return the location record 
     *  @throws org.osid.NullArgumentException <code> locationRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(locationRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.records.LocationRecord getLocationRecord(org.osid.type.Type locationRecordType)
        throws org.osid.OperationFailedException {

        return (this.location.getLocationRecord(locationRecordType));
    }
}

