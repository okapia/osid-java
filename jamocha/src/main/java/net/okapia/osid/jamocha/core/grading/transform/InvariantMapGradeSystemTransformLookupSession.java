//
// InvariantMapGradeSystemTransformLookupSession
//
//    Implements a GradeSystemTransform lookup service backed by a fixed collection of
//    gradeSystemTransforms.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.grading.transform;


/**
 *  Implements a GradeSystemTransform lookup service backed by a fixed
 *  collection of grade system transforms. The grade system transforms are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapGradeSystemTransformLookupSession
    extends net.okapia.osid.jamocha.core.grading.transform.spi.AbstractMapGradeSystemTransformLookupSession
    implements org.osid.grading.transform.GradeSystemTransformLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapGradeSystemTransformLookupSession</code> with no
     *  grade system transforms.
     *  
     *  @param gradebook the gradebook
     *  @throws org.osid.NullArgumnetException {@code gradebook} is
     *          {@code null}
     */

    public InvariantMapGradeSystemTransformLookupSession(org.osid.grading.Gradebook gradebook) {
        setGradebook(gradebook);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapGradeSystemTransformLookupSession</code> with a single
     *  grade system transform.
     *  
     *  @param gradebook the gradebook
     *  @param gradeSystemTransform a single grade system transform
     *  @throws org.osid.NullArgumentException {@code gradebook} or
     *          {@code gradeSystemTransform} is <code>null</code>
     */

      public InvariantMapGradeSystemTransformLookupSession(org.osid.grading.Gradebook gradebook,
                                               org.osid.grading.transform.GradeSystemTransform gradeSystemTransform) {
        this(gradebook);
        putGradeSystemTransform(gradeSystemTransform);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapGradeSystemTransformLookupSession</code> using an array
     *  of grade system transforms.
     *  
     *  @param gradebook the gradebook
     *  @param gradeSystemTransforms an array of grade system transforms
     *  @throws org.osid.NullArgumentException {@code gradebook} or
     *          {@code gradeSystemTransforms} is <code>null</code>
     */

      public InvariantMapGradeSystemTransformLookupSession(org.osid.grading.Gradebook gradebook,
                                               org.osid.grading.transform.GradeSystemTransform[] gradeSystemTransforms) {
        this(gradebook);
        putGradeSystemTransforms(gradeSystemTransforms);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapGradeSystemTransformLookupSession</code> using a
     *  collection of grade system transforms.
     *
     *  @param gradebook the gradebook
     *  @param gradeSystemTransforms a collection of grade system transforms
     *  @throws org.osid.NullArgumentException {@code gradebook} or
     *          {@code gradeSystemTransforms} is <code>null</code>
     */

      public InvariantMapGradeSystemTransformLookupSession(org.osid.grading.Gradebook gradebook,
                                               java.util.Collection<? extends org.osid.grading.transform.GradeSystemTransform> gradeSystemTransforms) {
        this(gradebook);
        putGradeSystemTransforms(gradeSystemTransforms);
        return;
    }
}
