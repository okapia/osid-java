//
// AbstractURLAssetContent
//
//     AssetContent retrieved by a URL.
//
//
// Tom Coppeto
// Okpaia
// 29 July 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.accouter.repository.asset.spi;


/**
 *  AssetContent where the content is retrieved by A URL. The access
 *  to the URL will be lazy and retrieved only when data is accessed.
 */

public abstract class AbstractURLAssetContent
    extends AbstractAssetContent
    implements org.osid.repository.AssetContent {

    private org.osid.type.Type mime;
    private boolean populated = false;
    private final java.net.URL url;


    /**
     *  Constructs a new <code>AbstractURLAssetContent</code>. The Id
     *  of the content will be the url.
     *
     *  @param asset the Asset to which this content belongs
     *  @param url url to the asset
     *  @throws org.osid.InvalidArgumentException <code>url</code> is invalid
     *  @throws org.osid.NotFoundException <code>url</code> not found
     *  @throws org.osid.NullArgumentException <code>null</code> argument
     *          provided
     */

    protected AbstractURLAssetContent(org.osid.repository.Asset asset, String url)
	throws org.osid.NotFoundException,
	       org.osid.OperationFailedException {

	this(asset, makeURL(url));
	return;
    }


    /**
     *  Constructs a new <code>AbstractURLAssetContent</code>. The ID
     *  will be the URL.
     *
     *  @param asset the Asset to which this content belongs
     *  @param url url to the asset
     *  @throws org.osid.InvalidArgumentException <code>url</code> is invalid
     *  @throws org.osid.NotFoundException <code>url</code> not found
     *  @throws org.osid.NullArgumentException <code>null</code> argument
     *          provided
     */

    protected AbstractURLAssetContent(org.osid.repository.Asset asset, java.net.URL url)
	throws org.osid.NotFoundException,
	       org.osid.OperationFailedException {

	setId(net.okapia.osid.primordium.id.URLId.valueOf(url));
	setAsset(asset);
	
	this.url = url;
	setDescription("Asset content at " + url + ".");

	return;
    }


    /**
     *  Tests if a data length is available. 
     *
     *  @return <code> true </code> if a length is available for this content, 
     *          <code> false </code> otherwise. 
     */

    public boolean hasDataLength() {
	try {
	    populateData();
	} catch (Exception e) {}

	return (super.hasDataLength());
    }


    /**
     *  Gets the asset content data. 
     *
     *  @return the length of the content data 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    public org.osid.transport.DataInputStream getData()
        throws org.osid.OperationFailedException {
	
	try {
	    return (new net.okapia.osid.jamocha.accouter.transport.DataInputStream(this.url.openConnection().getInputStream()));
	} catch (Exception e) {
	    throw new org.osid.OperationFailedException(e);
	}
    }


    /**
     *  Access the URL and populates the length and type.
     *
     *  @throws org.osid.NotFoundException image not found
     *  @throws org.osid.OperationFailedException
     */

    private void populateData()
	throws org.osid.NotFoundException,
	       org.osid.OperationFailedException {

	if (this.populated) {
	    return;
	}

	try {
	    java.net.URLConnection conn = this.url.openConnection();
	    if (conn instanceof java.net.HttpURLConnection) {
		if (((java.net.HttpURLConnection) conn).getResponseCode() == java.net.HttpURLConnection.HTTP_NOT_FOUND) {
		    throw new org.osid.NotFoundException(this.url + " not found");
		} 
		
		if (((java.net.HttpURLConnection) conn).getResponseCode() == java.net.HttpURLConnection.HTTP_OK) {
		    throw new org.osid.OperationFailedException(((java.net.HttpURLConnection) conn).getResponseMessage());
		} 
	    }

	    setDataLength(conn.getContentLength());
	    String type = conn.getContentType();
	    // get mime type

	    processFile(conn);
	} catch (Exception e) {
	    throw new org.osid.OperationFailedException(e);
	}

	this.populated = true;
	return;
    }


    /**
     *  A hook for processing the URL connection to examine the file.
     */

    protected void processFile(java.net.URLConnection connection)
	throws org.osid.OperationFailedException {
	
	return;
    }
    

    protected static java.net.URL makeURL(String urlString) {
	checker.nullarg(urlString, "url");

	java.net.URL url = Thread.currentThread().getContextClassLoader().getResource(urlString);

	if (url == null) {
	    try {
		url = new java.net.URL(urlString);
	    } catch (Exception e) {
		throw new org.osid.InvalidArgumentException("cannot parse " + url, e);
	    }
	}

	return (url);
    }
}

