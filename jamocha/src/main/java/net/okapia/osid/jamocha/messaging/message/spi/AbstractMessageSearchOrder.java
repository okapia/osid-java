//
// AbstractMessageSearchOdrer.java
//
//     Defines a MessageSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.messaging.message.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code MessageSearchOrder}.
 */

public abstract class AbstractMessageSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectSearchOrder
    implements org.osid.messaging.MessageSearchOrder {

    private final java.util.Collection<org.osid.messaging.records.MessageSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering messages by subject line. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySubjectLine(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering messages by text. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByText(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering messages by sent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySent(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering messages by the sent time. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySentTime(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering messages by sender. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySender(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a resource order is available. 
     *
     *  @return <code> true </code> if a resource order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSenderSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSenderSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getSenderSearchOrder() {
        throw new org.osid.UnimplementedException("supportsSenderSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering messages by sending agent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySendingAgent(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an agent order is available. 
     *
     *  @return <code> true </code> if an agent order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSendingAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the sending agent order. 
     *
     *  @return the agent search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSendingAgentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getSendingAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsSendingAgentSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering messages by the sent time. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByReceivedTime(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering messages by the delivery time. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDeliveryTime(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering messages by receipt. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByReceipt(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a receipt order is available. 
     *
     *  @return <code> true </code> if a receipt order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReceiptSearchOrder() {
        return (false);
    }


    /**
     *  Gets the receipt order. 
     *
     *  @return the receipt search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsReceiptSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptSearchOrder getReceiptSearchOrder() {
        throw new org.osid.UnimplementedException("supportsReceiptSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  messageRecordType a message record type 
     *  @return {@code true} if the messageRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code messageRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type messageRecordType) {
        for (org.osid.messaging.records.MessageSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(messageRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  messageRecordType the message record type 
     *  @return the message search order record
     *  @throws org.osid.NullArgumentException
     *          {@code messageRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(messageRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.messaging.records.MessageSearchOrderRecord getMessageSearchOrderRecord(org.osid.type.Type messageRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.messaging.records.MessageSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(messageRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(messageRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this message. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param messageRecord the message search odrer record
     *  @param messageRecordType message record type
     *  @throws org.osid.NullArgumentException
     *          {@code messageRecord} or
     *          {@code messageRecordTypemessage} is
     *          {@code null}
     */
            
    protected void addMessageRecord(org.osid.messaging.records.MessageSearchOrderRecord messageSearchOrderRecord, 
                                     org.osid.type.Type messageRecordType) {

        addRecordType(messageRecordType);
        this.records.add(messageSearchOrderRecord);
        
        return;
    }
}
