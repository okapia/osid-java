//
// AbstractIndexedMapPoolProcessorEnablerLookupSession.java
//
//    A simple framework for providing a PoolProcessorEnabler lookup service
//    backed by a fixed collection of pool processor enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a PoolProcessorEnabler lookup service backed by a
 *  fixed collection of pool processor enablers. The pool processor enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some pool processor enablers may be compatible
 *  with more types than are indicated through these pool processor enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>PoolProcessorEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapPoolProcessorEnablerLookupSession
    extends AbstractMapPoolProcessorEnablerLookupSession
    implements org.osid.provisioning.rules.PoolProcessorEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.provisioning.rules.PoolProcessorEnabler> poolProcessorEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.provisioning.rules.PoolProcessorEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.provisioning.rules.PoolProcessorEnabler> poolProcessorEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.provisioning.rules.PoolProcessorEnabler>());


    /**
     *  Makes a <code>PoolProcessorEnabler</code> available in this session.
     *
     *  @param  poolProcessorEnabler a pool processor enabler
     *  @throws org.osid.NullArgumentException <code>poolProcessorEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putPoolProcessorEnabler(org.osid.provisioning.rules.PoolProcessorEnabler poolProcessorEnabler) {
        super.putPoolProcessorEnabler(poolProcessorEnabler);

        this.poolProcessorEnablersByGenus.put(poolProcessorEnabler.getGenusType(), poolProcessorEnabler);
        
        try (org.osid.type.TypeList types = poolProcessorEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.poolProcessorEnablersByRecord.put(types.getNextType(), poolProcessorEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a pool processor enabler from this session.
     *
     *  @param poolProcessorEnablerId the <code>Id</code> of the pool processor enabler
     *  @throws org.osid.NullArgumentException <code>poolProcessorEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removePoolProcessorEnabler(org.osid.id.Id poolProcessorEnablerId) {
        org.osid.provisioning.rules.PoolProcessorEnabler poolProcessorEnabler;
        try {
            poolProcessorEnabler = getPoolProcessorEnabler(poolProcessorEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.poolProcessorEnablersByGenus.remove(poolProcessorEnabler.getGenusType());

        try (org.osid.type.TypeList types = poolProcessorEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.poolProcessorEnablersByRecord.remove(types.getNextType(), poolProcessorEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removePoolProcessorEnabler(poolProcessorEnablerId);
        return;
    }


    /**
     *  Gets a <code>PoolProcessorEnablerList</code> corresponding to the given
     *  pool processor enabler genus <code>Type</code> which does not include
     *  pool processor enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known pool processor enablers or an error results. Otherwise,
     *  the returned list may contain only those pool processor enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  poolProcessorEnablerGenusType a pool processor enabler genus type 
     *  @return the returned <code>PoolProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>poolProcessorEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorEnablerList getPoolProcessorEnablersByGenusType(org.osid.type.Type poolProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.rules.poolprocessorenabler.ArrayPoolProcessorEnablerList(this.poolProcessorEnablersByGenus.get(poolProcessorEnablerGenusType)));
    }


    /**
     *  Gets a <code>PoolProcessorEnablerList</code> containing the given
     *  pool processor enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known pool processor enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  pool processor enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  poolProcessorEnablerRecordType a pool processor enabler record type 
     *  @return the returned <code>poolProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>poolProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorEnablerList getPoolProcessorEnablersByRecordType(org.osid.type.Type poolProcessorEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.rules.poolprocessorenabler.ArrayPoolProcessorEnablerList(this.poolProcessorEnablersByRecord.get(poolProcessorEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.poolProcessorEnablersByGenus.clear();
        this.poolProcessorEnablersByRecord.clear();

        super.close();

        return;
    }
}
