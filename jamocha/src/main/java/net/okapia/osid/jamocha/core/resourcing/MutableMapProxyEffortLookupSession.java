//
// MutableMapProxyEffortLookupSession
//
//    Implements an Effort lookup service backed by a collection of
//    efforts that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing;


/**
 *  Implements an Effort lookup service backed by a collection of
 *  efforts. The efforts are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of efforts can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyEffortLookupSession
    extends net.okapia.osid.jamocha.core.resourcing.spi.AbstractMapEffortLookupSession
    implements org.osid.resourcing.EffortLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyEffortLookupSession}
     *  with no efforts.
     *
     *  @param foundry the foundry
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyEffortLookupSession(org.osid.resourcing.Foundry foundry,
                                                  org.osid.proxy.Proxy proxy) {
        setFoundry(foundry);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyEffortLookupSession} with a
     *  single effort.
     *
     *  @param foundry the foundry
     *  @param effort an effort
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry},
     *          {@code effort}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyEffortLookupSession(org.osid.resourcing.Foundry foundry,
                                                org.osid.resourcing.Effort effort, org.osid.proxy.Proxy proxy) {
        this(foundry, proxy);
        putEffort(effort);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyEffortLookupSession} using an
     *  array of efforts.
     *
     *  @param foundry the foundry
     *  @param efforts an array of efforts
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry},
     *          {@code efforts}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyEffortLookupSession(org.osid.resourcing.Foundry foundry,
                                                org.osid.resourcing.Effort[] efforts, org.osid.proxy.Proxy proxy) {
        this(foundry, proxy);
        putEfforts(efforts);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyEffortLookupSession} using a
     *  collection of efforts.
     *
     *  @param foundry the foundry
     *  @param efforts a collection of efforts
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry},
     *          {@code efforts}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyEffortLookupSession(org.osid.resourcing.Foundry foundry,
                                                java.util.Collection<? extends org.osid.resourcing.Effort> efforts,
                                                org.osid.proxy.Proxy proxy) {
   
        this(foundry, proxy);
        setSessionProxy(proxy);
        putEfforts(efforts);
        return;
    }

    
    /**
     *  Makes a {@code Effort} available in this session.
     *
     *  @param effort an effort
     *  @throws org.osid.NullArgumentException {@code effort{@code 
     *          is {@code null}
     */

    @Override
    public void putEffort(org.osid.resourcing.Effort effort) {
        super.putEffort(effort);
        return;
    }


    /**
     *  Makes an array of efforts available in this session.
     *
     *  @param efforts an array of efforts
     *  @throws org.osid.NullArgumentException {@code efforts{@code 
     *          is {@code null}
     */

    @Override
    public void putEfforts(org.osid.resourcing.Effort[] efforts) {
        super.putEfforts(efforts);
        return;
    }


    /**
     *  Makes collection of efforts available in this session.
     *
     *  @param efforts
     *  @throws org.osid.NullArgumentException {@code effort{@code 
     *          is {@code null}
     */

    @Override
    public void putEfforts(java.util.Collection<? extends org.osid.resourcing.Effort> efforts) {
        super.putEfforts(efforts);
        return;
    }


    /**
     *  Removes a Effort from this session.
     *
     *  @param effortId the {@code Id} of the effort
     *  @throws org.osid.NullArgumentException {@code effortId{@code  is
     *          {@code null}
     */

    @Override
    public void removeEffort(org.osid.id.Id effortId) {
        super.removeEffort(effortId);
        return;
    }    
}
