//
// OfferingMiter.java
//
//     Defines an Offering miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.offering.offering;


/**
 *  Defines an <code>Offering</code> miter for use with the builders.
 */

public interface OfferingMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRelationshipMiter,
            org.osid.offering.Offering {


    /**
     *  Sets the canonical unit.
     *
     *  @param canonicalUnit a canonical unit
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnit</code> is <code>null</code>
     */

    public void setCanonicalUnit(org.osid.offering.CanonicalUnit canonicalUnit);


    /**
     *  Sets the time period.
     *
     *  @param timePeriod a time period
     *  @throws org.osid.NullArgumentException
     *          <code>timePeriod</code> is <code>null</code>
     */

    public void setTimePeriod(org.osid.calendaring.TimePeriod timePeriod);


    /**
     *  Sets the title.
     *
     *  @param title a title
     *  @throws org.osid.NullArgumentException
     *          <code>title</code> is <code>null</code>
     */

    public void setTitle(org.osid.locale.DisplayText title);


    /**
     *  Sets the code.
     *
     *  @param code a code
     *  @throws org.osid.NullArgumentException
     *          <code>code</code> is <code>null</code>
     */

    public void setCode(String code);


    /**
     *  Adds a result option.
     *
     *  @param resultOption a result option
     *  @throws org.osid.NullArgumentException
     *          <code>resultOption</code> is <code>null</code>
     */

    public void addResultOption(org.osid.grading.GradeSystem resultOption);


    /**
     *  Sets all the result options.
     *
     *  @param resultOptions a collection of result options
     *  @throws org.osid.NullArgumentException
     *          <code>resultOptions</code> is <code>null</code>
     */

    public void setResultOptions(java.util.Collection<org.osid.grading.GradeSystem> resultOptions);


    /**
     *  Adds a sponsor.
     *
     *  @param sponsor a sponsor
     *  @throws org.osid.NullArgumentException
     *          <code>sponsor</code> is <code>null</code>
     */

    public void addSponsor(org.osid.resource.Resource sponsor);


    /**
     *  Sets all the sponsors.
     *
     *  @param sponsors a collection of sponsors
     *  @throws org.osid.NullArgumentException
     *          <code>sponsors</code> is <code>null</code>
     */

    public void setSponsors(java.util.Collection<org.osid.resource.Resource> sponsors);


    /**
     *  Adds a schedule.
     *
     *  @param schedule a schedule
     *  @throws org.osid.NullArgumentException
     *          <code>schedule</code> is <code>null</code>
     */

    public void addSchedule(org.osid.calendaring.Schedule schedule);


    /**
     *  Sets all the schedules.
     *
     *  @param schedules a collection of schedules
     *  @throws org.osid.NullArgumentException
     *          <code>schedules</code> is <code>null</code>
     */

    public void setSchedules(java.util.Collection<org.osid.calendaring.Schedule> schedules);


    /**
     *  Adds an Offering record.
     *
     *  @param record an offering record
     *  @param recordType the type of offering record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addOfferingRecord(org.osid.offering.records.OfferingRecord record, org.osid.type.Type recordType);
}       


