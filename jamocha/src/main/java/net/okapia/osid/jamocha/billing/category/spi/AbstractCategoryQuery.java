//
// AbstractCategoryQuery.java
//
//     A template for making a Category Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.category.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for categories.
 */

public abstract class AbstractCategoryQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.billing.CategoryQuery {

    private final java.util.Collection<org.osid.billing.records.CategoryQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the entry <code> Id </code> for this query to match categories 
     *  that have a related entry. 
     *
     *  @param  entryId an entry <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> entryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEntryId(org.osid.id.Id entryId, boolean match) {
        return;
    }


    /**
     *  Clears the entry <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearEntryIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> EntryQuery </code> is available. 
     *
     *  @return <code> true </code> if an entry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for an entry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the entry query 
     *  @throws org.osid.UnimplementedException <code> supportsEntryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.EntryQuery getEntryQuery() {
        throw new org.osid.UnimplementedException("supportsEntryQuery() is false");
    }


    /**
     *  Matches any related entry. 
     *
     *  @param  match <code> true </code> to match categories with any entry, 
     *          <code> false </code> to match categories with no entries 
     */

    @OSID @Override
    public void matchAnyEntry(boolean match) {
        return;
    }


    /**
     *  Clears the entry terms. 
     */

    @OSID @Override
    public void clearEntryTerms() {
        return;
    }


    /**
     *  Sets the business <code> Id </code> for this query to match categories 
     *  assigned to businesses. 
     *
     *  @param  businessId the business <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBusinessId(org.osid.id.Id businessId, boolean match) {
        return;
    }


    /**
     *  Clears the business <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBusinessIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BusinessQuery </code> is available. 
     *
     *  @return <code> true </code> if a business query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a business. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the business query 
     *  @throws org.osid.UnimplementedException <code> supportsBusinessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.BusinessQuery getBusinessQuery() {
        throw new org.osid.UnimplementedException("supportsBusinessQuery() is false");
    }


    /**
     *  Clears the business terms. 
     */

    @OSID @Override
    public void clearBusinessTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given category query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a category implementing the requested record.
     *
     *  @param categoryRecordType a category record type
     *  @return the category query record
     *  @throws org.osid.NullArgumentException
     *          <code>categoryRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(categoryRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.records.CategoryQueryRecord getCategoryQueryRecord(org.osid.type.Type categoryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.records.CategoryQueryRecord record : this.records) {
            if (record.implementsRecordType(categoryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(categoryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this category query. 
     *
     *  @param categoryQueryRecord category query record
     *  @param categoryRecordType category record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCategoryQueryRecord(org.osid.billing.records.CategoryQueryRecord categoryQueryRecord, 
                                          org.osid.type.Type categoryRecordType) {

        addRecordType(categoryRecordType);
        nullarg(categoryQueryRecord, "category query record");
        this.records.add(categoryQueryRecord);        
        return;
    }
}
