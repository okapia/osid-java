//
// InvariantMapCourseLookupSession
//
//    Implements a Course lookup service backed by a fixed collection of
//    courses.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course;


/**
 *  Implements a Course lookup service backed by a fixed
 *  collection of courses. The courses are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapCourseLookupSession
    extends net.okapia.osid.jamocha.core.course.spi.AbstractMapCourseLookupSession
    implements org.osid.course.CourseLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapCourseLookupSession</code> with no
     *  courses.
     *  
     *  @param courseCatalog the course catalog
     *  @throws org.osid.NullArgumnetException {@code courseCatalog} is
     *          {@code null}
     */

    public InvariantMapCourseLookupSession(org.osid.course.CourseCatalog courseCatalog) {
        setCourseCatalog(courseCatalog);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapCourseLookupSession</code> with a single
     *  course.
     *  
     *  @param courseCatalog the course catalog
     *  @param course a single course
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code course} is <code>null</code>
     */

      public InvariantMapCourseLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                               org.osid.course.Course course) {
        this(courseCatalog);
        putCourse(course);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapCourseLookupSession</code> using an array
     *  of courses.
     *  
     *  @param courseCatalog the course catalog
     *  @param courses an array of courses
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code courses} is <code>null</code>
     */

      public InvariantMapCourseLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                               org.osid.course.Course[] courses) {
        this(courseCatalog);
        putCourses(courses);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapCourseLookupSession</code> using a
     *  collection of courses.
     *
     *  @param courseCatalog the course catalog
     *  @param courses a collection of courses
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code courses} is <code>null</code>
     */

      public InvariantMapCourseLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                               java.util.Collection<? extends org.osid.course.Course> courses) {
        this(courseCatalog);
        putCourses(courses);
        return;
    }
}
