//
// AbstractAdapterCanonicalUnitEnablerLookupSession.java
//
//    A CanonicalUnitEnabler lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.offering.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A CanonicalUnitEnabler lookup session adapter.
 */

public abstract class AbstractAdapterCanonicalUnitEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.offering.rules.CanonicalUnitEnablerLookupSession {

    private final org.osid.offering.rules.CanonicalUnitEnablerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterCanonicalUnitEnablerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterCanonicalUnitEnablerLookupSession(org.osid.offering.rules.CanonicalUnitEnablerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Catalogue/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Catalogue Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCatalogueId() {
        return (this.session.getCatalogueId());
    }


    /**
     *  Gets the {@code Catalogue} associated with this session.
     *
     *  @return the {@code Catalogue} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Catalogue getCatalogue()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCatalogue());
    }


    /**
     *  Tests if this user can perform {@code CanonicalUnitEnabler} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupCanonicalUnitEnablers() {
        return (this.session.canLookupCanonicalUnitEnablers());
    }


    /**
     *  A complete view of the {@code CanonicalUnitEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCanonicalUnitEnablerView() {
        this.session.useComparativeCanonicalUnitEnablerView();
        return;
    }


    /**
     *  A complete view of the {@code CanonicalUnitEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCanonicalUnitEnablerView() {
        this.session.usePlenaryCanonicalUnitEnablerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include canonical unit enablers in catalogues which are children
     *  of this catalogue in the catalogue hierarchy.
     */

    @OSID @Override
    public void useFederatedCatalogueView() {
        this.session.useFederatedCatalogueView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this catalogue only.
     */

    @OSID @Override
    public void useIsolatedCatalogueView() {
        this.session.useIsolatedCatalogueView();
        return;
    }
    

    /**
     *  Only active canonical unit enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveCanonicalUnitEnablerView() {
        this.session.useActiveCanonicalUnitEnablerView();
        return;
    }


    /**
     *  Active and inactive canonical unit enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusCanonicalUnitEnablerView() {
        this.session.useAnyStatusCanonicalUnitEnablerView();
        return;
    }
    
     
    /**
     *  Gets the {@code CanonicalUnitEnabler} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code CanonicalUnitEnabler} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code CanonicalUnitEnabler} and
     *  retained for compatibility.
     *
     *  In active mode, canonical unit enablers are returned that are currently
     *  active. In any status mode, active and inactive canonical unit enablers
     *  are returned.
     *
     *  @param canonicalUnitEnablerId {@code Id} of the {@code CanonicalUnitEnabler}
     *  @return the canonical unit enabler
     *  @throws org.osid.NotFoundException {@code canonicalUnitEnablerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code canonicalUnitEnablerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnabler getCanonicalUnitEnabler(org.osid.id.Id canonicalUnitEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCanonicalUnitEnabler(canonicalUnitEnablerId));
    }


    /**
     *  Gets a {@code CanonicalUnitEnablerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  canonicalUnitEnablers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code CanonicalUnitEnablers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, canonical unit enablers are returned that are currently
     *  active. In any status mode, active and inactive canonical unit enablers
     *  are returned.
     *
     *  @param  canonicalUnitEnablerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code CanonicalUnitEnabler} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code canonicalUnitEnablerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerList getCanonicalUnitEnablersByIds(org.osid.id.IdList canonicalUnitEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCanonicalUnitEnablersByIds(canonicalUnitEnablerIds));
    }


    /**
     *  Gets a {@code CanonicalUnitEnablerList} corresponding to the given
     *  canonical unit enabler genus {@code Type} which does not include
     *  canonical unit enablers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  canonical unit enablers or an error results. Otherwise, the returned list
     *  may contain only those canonical unit enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, canonical unit enablers are returned that are currently
     *  active. In any status mode, active and inactive canonical unit enablers
     *  are returned.
     *
     *  @param  canonicalUnitEnablerGenusType a canonicalUnitEnabler genus type 
     *  @return the returned {@code CanonicalUnitEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code canonicalUnitEnablerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerList getCanonicalUnitEnablersByGenusType(org.osid.type.Type canonicalUnitEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCanonicalUnitEnablersByGenusType(canonicalUnitEnablerGenusType));
    }


    /**
     *  Gets a {@code CanonicalUnitEnablerList} corresponding to the given
     *  canonical unit enabler genus {@code Type} and include any additional
     *  canonical unit enablers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  canonical unit enablers or an error results. Otherwise, the returned list
     *  may contain only those canonical unit enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, canonical unit enablers are returned that are currently
     *  active. In any status mode, active and inactive canonical unit enablers
     *  are returned.
     *
     *  @param  canonicalUnitEnablerGenusType a canonicalUnitEnabler genus type 
     *  @return the returned {@code CanonicalUnitEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code canonicalUnitEnablerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerList getCanonicalUnitEnablersByParentGenusType(org.osid.type.Type canonicalUnitEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCanonicalUnitEnablersByParentGenusType(canonicalUnitEnablerGenusType));
    }


    /**
     *  Gets a {@code CanonicalUnitEnablerList} containing the given
     *  canonical unit enabler record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  canonical unit enablers or an error results. Otherwise, the returned list
     *  may contain only those canonical unit enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, canonical unit enablers are returned that are currently
     *  active. In any status mode, active and inactive canonical unit enablers
     *  are returned.
     *
     *  @param  canonicalUnitEnablerRecordType a canonicalUnitEnabler record type 
     *  @return the returned {@code CanonicalUnitEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code canonicalUnitEnablerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerList getCanonicalUnitEnablersByRecordType(org.osid.type.Type canonicalUnitEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCanonicalUnitEnablersByRecordType(canonicalUnitEnablerRecordType));
    }


    /**
     *  Gets a {@code CanonicalUnitEnablerList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  canonical unit enablers or an error results. Otherwise, the returned list
     *  may contain only those canonical unit enablers that are accessible
     *  through this session.
     *  
     *  In active mode, canonical unit enablers are returned that are currently
     *  active. In any status mode, active and inactive canonical unit enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code CanonicalUnitEnabler} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerList getCanonicalUnitEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCanonicalUnitEnablersOnDate(from, to));
    }
        

    /**
     *  Gets a {@code CanonicalUnitEnablerList } which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  canonical unit enablers or an error results. Otherwise, the returned list
     *  may contain only those canonical unit enablers that are accessible
     *  through this session.
     *
     *  In active mode, canonical unit enablers are returned that are currently
     *  active. In any status mode, active and inactive canonical unit enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned {@code CanonicalUnitEnabler} list
     *  @throws org.osid.InvalidArgumentException {@code from} is
     *          greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code agent},
     *          {@code from}, or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerList getCanonicalUnitEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (this.session.getCanonicalUnitEnablersOnDateWithAgent(agentId, from, to));
    }


    /**
     *  Gets all {@code CanonicalUnitEnablers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  canonical unit enablers or an error results. Otherwise, the returned list
     *  may contain only those canonical unit enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, canonical unit enablers are returned that are currently
     *  active. In any status mode, active and inactive canonical unit enablers
     *  are returned.
     *
     *  @return a list of {@code CanonicalUnitEnablers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerList getCanonicalUnitEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCanonicalUnitEnablers());
    }
}
