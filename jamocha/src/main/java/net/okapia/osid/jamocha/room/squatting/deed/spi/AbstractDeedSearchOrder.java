//
// AbstractDeedSearchOdrer.java
//
//     Defines a DeedSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.squatting.deed.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code DeedSearchOrder}.
 */

public abstract class AbstractDeedSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipSearchOrder
    implements org.osid.room.squatting.DeedSearchOrder {

    private final java.util.Collection<org.osid.room.squatting.records.DeedSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by the building. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBuilding(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a building search order is available. 
     *
     *  @return <code> true </code> if a building search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBuildingSearchOrder() {
        return (false);
    }


    /**
     *  Gets the building search order. 
     *
     *  @return the building search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBuildingSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingSearchOrder getBuildingSearchOrder() {
        throw new org.osid.UnimplementedException("supportsBuildingSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the owner. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByOwner(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an owner resource search order is available. 
     *
     *  @return <code> true </code> if an owner search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOwnerSearchOrder() {
        return (false);
    }


    /**
     *  Gets the owner resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOwnerSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getOwnerSearchOrder() {
        throw new org.osid.UnimplementedException("supportsOwnerSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  deedRecordType a deed record type 
     *  @return {@code true} if the deedRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code deedRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type deedRecordType) {
        for (org.osid.room.squatting.records.DeedSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(deedRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  deedRecordType the deed record type 
     *  @return the deed search order record
     *  @throws org.osid.NullArgumentException
     *          {@code deedRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(deedRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.room.squatting.records.DeedSearchOrderRecord getDeedSearchOrderRecord(org.osid.type.Type deedRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.squatting.records.DeedSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(deedRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(deedRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this deed. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param deedRecord the deed search odrer record
     *  @param deedRecordType deed record type
     *  @throws org.osid.NullArgumentException
     *          {@code deedRecord} or
     *          {@code deedRecordTypedeed} is
     *          {@code null}
     */
            
    protected void addDeedRecord(org.osid.room.squatting.records.DeedSearchOrderRecord deedSearchOrderRecord, 
                                     org.osid.type.Type deedRecordType) {

        addRecordType(deedRecordType);
        this.records.add(deedSearchOrderRecord);
        
        return;
    }
}
