//
// AbstractReceiptQuery.java
//
//     A template for making a Receipt Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.messaging.receipt.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for receipts.
 */

public abstract class AbstractReceiptQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.messaging.ReceiptQuery {

    private final java.util.Collection<org.osid.messaging.records.ReceiptQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the message <code> Id </code> for this query. 
     *
     *  @param  messageId a message <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> messageId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchMessageId(org.osid.id.Id messageId, boolean match) {
        return;
    }


    /**
     *  Clears the message <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearMessageIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> MessageQuery </code> is available. 
     *
     *  @return <code> true </code> if a message query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMessageQuery() {
        return (false);
    }


    /**
     *  Gets the query for a message. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the message query 
     *  @throws org.osid.UnimplementedException <code> supportsMessageQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageQuery getMessageQuery() {
        throw new org.osid.UnimplementedException("supportsMessageQuery() is false");
    }


    /**
     *  Clears the message terms. 
     */

    @OSID @Override
    public void clearMessageTerms() {
        return;
    }


    /**
     *  Matches messages whose received time is between the supplied range 
     *  inclusive. 
     *
     *  @param  startTime start time 
     *  @param  endTime end time 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> startTime </code> is 
     *          greater than <code> endTime </code> 
     *  @throws org.osid.NullArgumentException <code> startTime </code> or 
     *          <code> endTime </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchReceivedTime(org.osid.calendaring.DateTime startTime, 
                                  org.osid.calendaring.DateTime endTime, 
                                  boolean match) {
        return;
    }


    /**
     *  Clears the received time terms. 
     */

    @OSID @Override
    public void clearReceivedTimeTerms() {
        return;
    }


    /**
     *  Matches the endpoint recipient of the message. 
     *
     *  @param  agentId <code> Id </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchReceivingAgentId(org.osid.id.Id agentId, boolean match) {
        return;
    }


    /**
     *  Clears the receiving agent <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearReceivingAgentIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available for querying 
     *  receiving agents. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReceivingAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for the receiving agent. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsReceivingQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getReceivingAgentQuery() {
        throw new org.osid.UnimplementedException("supportsReceivingAgentQuery() is false");
    }


    /**
     *  Clears the receiving agent terms. 
     */

    @OSID @Override
    public void clearReceivingAgentTerms() {
        return;
    }


    /**
     *  Matches any recipient of the message. 
     *
     *  @param  resourceId <code> Id </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRecipientId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the recipient <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRecipientIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available for querying 
     *  recipients. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipientQuery() {
        return (false);
    }


    /**
     *  Gets the query for a recipient resource. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecipientQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getRecipientQuery() {
        throw new org.osid.UnimplementedException("supportsRecipientQuery() is false");
    }


    /**
     *  Clears the recipient terms. 
     */

    @OSID @Override
    public void clearRecipientTerms() {
        return;
    }


    /**
     *  Sets the mailbox <code> Id </code> for this query. 
     *
     *  @param  mailboxId the mailbox <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> mailboxId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchMailboxId(org.osid.id.Id mailboxId, boolean match) {
        return;
    }


    /**
     *  Clears the mailbox <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearMailboxIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> MailboxQuery </code> is available. 
     *
     *  @return <code> true </code> if a mailbox query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMailboxQuery() {
        return (false);
    }


    /**
     *  Gets the query for a mailbox. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the mailbox query 
     *  @throws org.osid.UnimplementedException <code> supportsMailboxQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MailboxQuery getMailboxQuery() {
        throw new org.osid.UnimplementedException("supportsMailboxQuery() is false");
    }


    /**
     *  Clears the mailbox terms. 
     */

    @OSID @Override
    public void clearMailboxTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given receipt query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a receipt implementing the requested record.
     *
     *  @param receiptRecordType a receipt record type
     *  @return the receipt query record
     *  @throws org.osid.NullArgumentException
     *          <code>receiptRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(receiptRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.messaging.records.ReceiptQueryRecord getReceiptQueryRecord(org.osid.type.Type receiptRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.messaging.records.ReceiptQueryRecord record : this.records) {
            if (record.implementsRecordType(receiptRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(receiptRecordType + " is not supported");
    }


    /**
     *  Adds a record to this receipt query. 
     *
     *  @param receiptQueryRecord receipt query record
     *  @param receiptRecordType receipt record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addReceiptQueryRecord(org.osid.messaging.records.ReceiptQueryRecord receiptQueryRecord, 
                                          org.osid.type.Type receiptRecordType) {

        addRecordType(receiptRecordType);
        nullarg(receiptQueryRecord, "receipt query record");
        this.records.add(receiptQueryRecord);        
        return;
    }
}
