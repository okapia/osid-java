//
// InvariantMapProxyCreditLookupSession
//
//    Implements a Credit lookup service backed by a fixed
//    collection of credits. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.acknowledgement;


/**
 *  Implements a Credit lookup service backed by a fixed
 *  collection of credits. The credits are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyCreditLookupSession
    extends net.okapia.osid.jamocha.core.acknowledgement.spi.AbstractMapCreditLookupSession
    implements org.osid.acknowledgement.CreditLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyCreditLookupSession} with no
     *  credits.
     *
     *  @param billing the billing
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code billing} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyCreditLookupSession(org.osid.acknowledgement.Billing billing,
                                                  org.osid.proxy.Proxy proxy) {
        setBilling(billing);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyCreditLookupSession} with a single
     *  credit.
     *
     *  @param billing the billing
     *  @param credit a single credit
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code billing},
     *          {@code credit} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyCreditLookupSession(org.osid.acknowledgement.Billing billing,
                                                  org.osid.acknowledgement.Credit credit, org.osid.proxy.Proxy proxy) {

        this(billing, proxy);
        putCredit(credit);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyCreditLookupSession} using
     *  an array of credits.
     *
     *  @param billing the billing
     *  @param credits an array of credits
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code billing},
     *          {@code credits} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyCreditLookupSession(org.osid.acknowledgement.Billing billing,
                                                  org.osid.acknowledgement.Credit[] credits, org.osid.proxy.Proxy proxy) {

        this(billing, proxy);
        putCredits(credits);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyCreditLookupSession} using a
     *  collection of credits.
     *
     *  @param billing the billing
     *  @param credits a collection of credits
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code billing},
     *          {@code credits} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyCreditLookupSession(org.osid.acknowledgement.Billing billing,
                                                  java.util.Collection<? extends org.osid.acknowledgement.Credit> credits,
                                                  org.osid.proxy.Proxy proxy) {

        this(billing, proxy);
        putCredits(credits);
        return;
    }
}
