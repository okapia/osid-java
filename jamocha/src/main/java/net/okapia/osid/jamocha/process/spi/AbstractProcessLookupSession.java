//
// AbstractProcessLookupSession.java
//
//    A starter implementation framework for providing a Process
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.process.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A starter implementation framework for providing a Process
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getProcesses(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractProcessLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.process.ProcessLookupSession {

    private boolean pedantic = false;
    private org.osid.process.Process process = new net.okapia.osid.jamocha.nil.process.process.UnknownProcess();
    


    /**
     *  Tests if this user can perform <code>Process</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupProcesses() {
        return (true);
    }


    /**
     *  A complete view of the <code>Process</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeProcessView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Process</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryProcessView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }

     
    /**
     *  Gets the <code>Process</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Process</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Process</code> and
     *  retained for compatibility.
     *
     *  @param  processId <code>Id</code> of the
     *          <code>Process</code>
     *  @return the process
     *  @throws org.osid.NotFoundException <code>processId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>processId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.Process getProcess(org.osid.id.Id processId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.process.ProcessList processes = getProcesses()) {
            while (processes.hasNext()) {
                org.osid.process.Process process = processes.getNextProcess();
                if (process.getId().equals(processId)) {
                    return (process);
                }
            }
        } 

        throw new org.osid.NotFoundException(processId + " not found");
    }


    /**
     *  Gets a <code>ProcessList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  processes specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Processes</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getProcesses()</code>.
     *
     *  @param  processIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Process</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>processIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.ProcessList getProcessesByIds(org.osid.id.IdList processIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.process.Process> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = processIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getProcess(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("process " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.process.process.LinkedProcessList(ret));
    }


    /**
     *  Gets a <code>ProcessList</code> corresponding to the given
     *  process genus <code>Type</code> which does not include
     *  processes of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  processes or an error results. Otherwise, the returned list
     *  may contain only those processes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getProcesses()</code>.
     *
     *  @param  processGenusType a process genus type 
     *  @return the returned <code>Process</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>processGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.ProcessList getProcessesByGenusType(org.osid.type.Type processGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.process.process.ProcessGenusFilterList(getProcesses(), processGenusType));
    }


    /**
     *  Gets a <code>ProcessList</code> corresponding to the given
     *  process genus <code>Type</code> and include any additional
     *  processes with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  processes or an error results. Otherwise, the returned list
     *  may contain only those processes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getProcesses()</code>.
     *
     *  @param  processGenusType a process genus type 
     *  @return the returned <code>Process</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>processGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.ProcessList getProcessesByParentGenusType(org.osid.type.Type processGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getProcessesByGenusType(processGenusType));
    }


    /**
     *  Gets a <code>ProcessList</code> containing the given
     *  process record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  processes or an error results. Otherwise, the returned list
     *  may contain only those processes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getProcesses()</code>.
     *
     *  @param  processRecordType a process record type 
     *  @return the returned <code>Process</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>processRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.ProcessList getProcessesByRecordType(org.osid.type.Type processRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.process.process.ProcessRecordFilterList(getProcesses(), processRecordType));
    }


    /**
     *  Gets a <code>ProcessList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known processes or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  processes that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Process</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.process.ProcessList getProcessesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.process.process.ProcessProviderFilterList(getProcesses(), resourceId));
    }


    /**
     *  Gets all <code>Processes</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  processes or an error results. Otherwise, the returned list
     *  may contain only those processes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Processes</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.process.ProcessList getProcesses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the process list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of processes
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.process.ProcessList filterProcessesOnViews(org.osid.process.ProcessList list)
        throws org.osid.OperationFailedException {

        return (list);
    }
}
