//
// AbstractMapGradebookColumnLookupSession
//
//    A simple framework for providing a GradebookColumn lookup service
//    backed by a fixed collection of gradebook columns.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.grading.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a GradebookColumn lookup service backed by a
 *  fixed collection of gradebook columns. The gradebook columns are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>GradebookColumns</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapGradebookColumnLookupSession
    extends net.okapia.osid.jamocha.grading.spi.AbstractGradebookColumnLookupSession
    implements org.osid.grading.GradebookColumnLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.grading.GradebookColumn> gradebookColumns = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.grading.GradebookColumn>());


    /**
     *  Makes a <code>GradebookColumn</code> available in this session.
     *
     *  @param  gradebookColumn a gradebook column
     *  @throws org.osid.NullArgumentException <code>gradebookColumn<code>
     *          is <code>null</code>
     */

    protected void putGradebookColumn(org.osid.grading.GradebookColumn gradebookColumn) {
        this.gradebookColumns.put(gradebookColumn.getId(), gradebookColumn);
        return;
    }


    /**
     *  Makes an array of gradebook columns available in this session.
     *
     *  @param  gradebookColumns an array of gradebook columns
     *  @throws org.osid.NullArgumentException <code>gradebookColumns<code>
     *          is <code>null</code>
     */

    protected void putGradebookColumns(org.osid.grading.GradebookColumn[] gradebookColumns) {
        putGradebookColumns(java.util.Arrays.asList(gradebookColumns));
        return;
    }


    /**
     *  Makes a collection of gradebook columns available in this session.
     *
     *  @param  gradebookColumns a collection of gradebook columns
     *  @throws org.osid.NullArgumentException <code>gradebookColumns<code>
     *          is <code>null</code>
     */

    protected void putGradebookColumns(java.util.Collection<? extends org.osid.grading.GradebookColumn> gradebookColumns) {
        for (org.osid.grading.GradebookColumn gradebookColumn : gradebookColumns) {
            this.gradebookColumns.put(gradebookColumn.getId(), gradebookColumn);
        }

        return;
    }


    /**
     *  Removes a GradebookColumn from this session.
     *
     *  @param  gradebookColumnId the <code>Id</code> of the gradebook column
     *  @throws org.osid.NullArgumentException <code>gradebookColumnId<code> is
     *          <code>null</code>
     */

    protected void removeGradebookColumn(org.osid.id.Id gradebookColumnId) {
        this.gradebookColumns.remove(gradebookColumnId);
        return;
    }


    /**
     *  Gets the <code>GradebookColumn</code> specified by its <code>Id</code>.
     *
     *  @param  gradebookColumnId <code>Id</code> of the <code>GradebookColumn</code>
     *  @return the gradebookColumn
     *  @throws org.osid.NotFoundException <code>gradebookColumnId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>gradebookColumnId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumn getGradebookColumn(org.osid.id.Id gradebookColumnId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.grading.GradebookColumn gradebookColumn = this.gradebookColumns.get(gradebookColumnId);
        if (gradebookColumn == null) {
            throw new org.osid.NotFoundException("gradebookColumn not found: " + gradebookColumnId);
        }

        return (gradebookColumn);
    }


    /**
     *  Gets all <code>GradebookColumns</code>. In plenary mode, the returned
     *  list contains all known gradebookColumns or an error
     *  results. Otherwise, the returned list may contain only those
     *  gradebookColumns that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>GradebookColumns</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnList getGradebookColumns()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.grading.gradebookcolumn.ArrayGradebookColumnList(this.gradebookColumns.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.gradebookColumns.clear();
        super.close();
        return;
    }
}
