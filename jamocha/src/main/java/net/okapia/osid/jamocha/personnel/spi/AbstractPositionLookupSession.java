//
// AbstractPositionLookupSession.java
//
//    A starter implementation framework for providing a Position
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.personnel.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Position
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getPositions(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractPositionLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.personnel.PositionLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.personnel.Realm realm = new net.okapia.osid.jamocha.nil.personnel.realm.UnknownRealm();
    

    /**
     *  Gets the <code>Realm/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Realm Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getRealmId() {
        return (this.realm.getId());
    }


    /**
     *  Gets the <code>Realm</code> associated with this 
     *  session.
     *
     *  @return the <code>Realm</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.Realm getRealm()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.realm);
    }


    /**
     *  Sets the <code>Realm</code>.
     *
     *  @param  realm the realm for this session
     *  @throws org.osid.NullArgumentException <code>realm</code>
     *          is <code>null</code>
     */

    protected void setRealm(org.osid.personnel.Realm realm) {
        nullarg(realm, "realm");
        this.realm = realm;
        return;
    }


    /**
     *  Tests if this user can perform <code>Position</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPositions() {
        return (true);
    }


    /**
     *  A complete view of the <code>Position</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePositionView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Position</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPositionView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include positions in realms which are children
     *  of this realm in the realm hierarchy.
     */

    @OSID @Override
    public void useFederatedRealmView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this realm only.
     */

    @OSID @Override
    public void useIsolatedRealmView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only effective positions are returned by methods in this
     *  session.
     */
     
    @OSID @Override
    public void useEffectivePositionView() {
        this.effectiveonly = true;
        return;
    }


    /**
     *  Effective and inEffective positions are returned by methods in
     *  this session.
     */
    
    @OSID @Override
    public void useAnyEffectivePositionView() {
       this.effectiveonly = false;
       return;
    }


    /**
     *  Tests if an effective or any effecive view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */
    
    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }
    
     
    /**
     *  Gets the <code>Position</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Position</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Position</code> and
     *  retained for compatibility.
     *
     *  In active mode, positions are returned that are currently
     *  active. In any status mode, active and inactive positions
     *  are returned.
     *
     *  @param  positionId <code>Id</code> of the
     *          <code>Position</code>
     *  @return the position
     *  @throws org.osid.NotFoundException <code>positionId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>positionId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.Position getPosition(org.osid.id.Id positionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.personnel.PositionList positions = getPositions()) {
            while (positions.hasNext()) {
                org.osid.personnel.Position position = positions.getNextPosition();
                if (position.getId().equals(positionId)) {
                    return (position);
                }
            }
        } 

        throw new org.osid.NotFoundException(positionId + " not found");
    }


    /**
     *  Gets a <code>PositionList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  positions specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Positions</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, positions are returned that are currently
     *  effective.  In any effective mode, effective positions and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getPositions()</code>.
     *
     *  @param  positionIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Position</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>positionIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.PositionList getPositionsByIds(org.osid.id.IdList positionIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.personnel.Position> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = positionIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getPosition(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("position " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.personnel.position.LinkedPositionList(ret));
    }


    /**
     *  Gets a <code>PositionList</code> corresponding to the given
     *  position genus <code>Type</code> which does not include
     *  positions of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  positions or an error results. Otherwise, the returned list
     *  may contain only those positions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, positions are returned that are currently
     *  effective.  In any effective mode, effective positions and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getPositions()</code>.
     *
     *  @param  positionGenusType a position genus type 
     *  @return the returned <code>Position</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>positionGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.PositionList getPositionsByGenusType(org.osid.type.Type positionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.personnel.position.PositionGenusFilterList(getPositions(), positionGenusType));
    }


    /**
     *  Gets a <code>PositionList</code> corresponding to the given
     *  position genus <code>Type</code> and include any additional
     *  positions with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  positions or an error results. Otherwise, the returned list
     *  may contain only those positions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, positions are returned that are currently
     *  effective.  In any effective mode, effective positions and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getPositions()</code>.
     *
     *  @param  positionGenusType a position genus type 
     *  @return the returned <code>Position</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>positionGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.PositionList getPositionsByParentGenusType(org.osid.type.Type positionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getPositionsByGenusType(positionGenusType));
    }


    /**
     *  Gets a <code>PositionList</code> containing the given
     *  position record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  positions or an error results. Otherwise, the returned list
     *  may contain only those positions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, positions are returned that are currently
     *  effective.  In any effective mode, effective positions and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getPositions()</code>.
     *
     *  @param  positionRecordType a position record type 
     *  @return the returned <code>Position</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>positionRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.PositionList getPositionsByRecordType(org.osid.type.Type positionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.personnel.position.PositionRecordFilterList(getPositions(), positionRecordType));
    }


    /**
     *  Gets a <code>PositionList</code> effective during the entire
     *  given date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  positions or an error results. Otherwise, the returned list
     *  may contain only those positions that are accessible through
     *  this session.
     *  
     *  In effective mode, positions are returned that are currently
     *  effective.  In any effective mode, effective positions and
     *  those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>PositionList</code> 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.personnel.PositionList getPositionsOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.personnel.position.TemporalPositionFilterList(getPositions(), from, to));
    }        


    /**
     *  Gets a <code>PositionList</code> for the given organization.
     *  
     *  In plenary mode, the returned list contains all known
     *  positions or an error results. Otherwise, the returned list
     *  may contain only those positions that are accessible through
     *  this session.
     *  
     *  In effective mode, positions are returned that are currently
     *  effective.  In any effective mode, effective positions and
     *  those currently expired are returned.
     *
     *  @param  organizationId an organization <code>Id</code> 
     *  @return the returned <code>PositionList</code> 
     *  @throws org.osid.NullArgumentException
     *          <code>organizationId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.personnel.PositionList getPositionsForOrganization(org.osid.id.Id organizationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.personnel.position.PositionFilterList(new OrganizationFilter(organizationId), getPositions()));
    }


    /**
     *  Gets a <code>PositionList</code> for the given organization
     *  and effective during the entire given date range inclusive but
     *  not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  positions or an error results. Otherwise, the returned list
     *  may contain only those positions that are accessible through
     *  this session.
     *  
     *  In effective mode, positions are returned that are currently
     *  effective.  In any effective mode, effective positions and
     *  those currently expired are returned.
     *
     *  @param  organizationId an organization <code>Id</code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>PositionList</code> 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>organizationId</code>, <code>from</code>, or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.personnel.PositionList getPositionsForOrganizationOnDate(org.osid.id.Id organizationId, 
                                                                             org.osid.calendaring.DateTime from, 
                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.personnel.position.TemporalPositionFilterList(getPositionsForOrganization(organizationId), from, to));
    }


    /**
     *  Gets all <code>Positions</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  positions or an error results. Otherwise, the returned list
     *  may contain only those positions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, positions are returned that are currently
     *  effective.  In any effective mode, effective positions and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Positions</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.personnel.PositionList getPositions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the position list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of positions
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.personnel.PositionList filterPositionsOnViews(org.osid.personnel.PositionList list)
        throws org.osid.OperationFailedException {

        org.osid.personnel.PositionList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.personnel.position.EffectivePositionFilterList(ret);
        }

        return (ret);
    }


    public static class OrganizationFilter
        implements net.okapia.osid.jamocha.inline.filter.personnel.position.PositionFilter {
         
        private final org.osid.id.Id organizationId;
         
         
        /**
         *  Constructs a new <code>OrganizationFilter</code>.
         *
         *  @param organizationId the organization to filter
         *  @throws org.osid.NullArgumentException
         *          <code>organizationId</code> is <code>null</code>
         */
        
        public OrganizationFilter(org.osid.id.Id organizationId) {
            nullarg(organizationId, "organization Id");
            this.organizationId = organizationId;
            return;
        }

         
        /**
         *  Used by the PositionFilterList to filter the 
         *  position list based on organization.
         *
         *  @param position the position
         *  @return <code>true</code> to pass the position,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.personnel.Position position) {
            return (position.getOrganizationId().equals(this.organizationId));
        }
    }
}
