//
// AbstractPathNotificationSession.java
//
//     A template for making PathNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.topology.path.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Path} objects. This session is intended for
 *  consumers needing to synchronize their state with this service
 *  without the use of polling. Notifications are cancelled when this
 *  session is closed.
 *  
 *  Notifications are triggered with changes to the {@code Path}
 *  object itself. Adding and removing entries result in notifications
 *  available from the notification session for path entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractPathNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.topology.path.PathNotificationSession {

    private boolean federated = false;
    private org.osid.topology.Graph graph = new net.okapia.osid.jamocha.nil.topology.graph.UnknownGraph();


    /**
     *  Gets the {@code Graph/code> {@code Id} associated with this
     *  session.
     *
     *  @return the {@code Graph Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getGraphId() {
        return (this.graph.getId());
    }

    
    /**
     *  Gets the {@code Graph} associated with this session.
     *
     *  @return the {@code Graph} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.Graph getGraph()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.graph);
    }


    /**
     *  Sets the {@code Graph}.
     *
     *  @param graph the graph for this session
     *  @throws org.osid.NullArgumentException {@code graph}
     *          is {@code null}
     */

    protected void setGraph(org.osid.topology.Graph graph) {
        nullarg(graph, "graph");
        this.graph = graph;
        return;
    }


    /**
     *  Tests if this user can register for {@code Path}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForPathNotifications() {
        return (true);
    }


    /**
     *  Reliable notifications are desired. In reliable mode, notifications 
     *  are to be acknowledged using <code> acknowledgePathNotification() 
     *  </code>. 
     */

    @OSID @Override
    public void reliablePathNotifications() {
        return;
    }


    /**
     *  Unreliable notifications are desired. In unreliable mode, 
     *  notifications do not need to be acknowledged. 
     */

    @OSID @Override
    public void unreliablePathNotifications() {
        return;
    }


    /**
     *  Acknowledge a path notification. 
     *
     *  @param  notificationId the <code> Id </code> of the notification 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void acknowledgePathNotification(org.osid.id.Id notificationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include edges in paths which are children of this
     *  graph in the graph hierarchy.
     */

    @OSID @Override
    public void useFederatedGraphView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this graph only.
     */

    @OSID @Override
    public void useIsolatedGraphView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return {@codetrue</code> if federated view,
     *          {@codefalse</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new paths. {@code
     *  PathReceiver.newPath()} is invoked when a new {@code Path} is
     *  created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewPaths()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new paths of the given genus
     *  type.  {@code PathReceiver.newPathConnection()} is invoked
     *  when a new {@code Path} is connected to the specified
     *  location.
     *
     *  @param  pathGenusType a path genus type 
     *  @throws org.osid.NullArgumentException {@code pathGenusType}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForNewPathsByGenusType(org.osid.type.Type pathGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new paths for the given starting
     *  node {@code Id}. {@code PathReceiver.newPath()} is invoked
     *  when a new {@code Path} is created.
     *
     *  @param  startingNodeId the {@code Id} of the starting node to monitor
     *  @throws org.osid.NullArgumentException {@code startingNodeId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewPathsForStartingNode(org.osid.id.Id startingNodeId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /** 
     *  Register for notifications of new paths for the given ending
     *  node {@code Id}. {@code PathReceiver.newPath()} is invoked
     *  when a new {@code Path} is created.
     *
     *  @param  endingNodeId the {@code Id} of the ending node to monitor
     *  @throws org.osid.NullArgumentException {@code endingNodeId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewPathsForEndingNode(org.osid.id.Id endingNodeId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated paths. {@code
     *  PathReceiver.changedPath()} is invoked when a path is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedPaths()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated paths of the given genus
     *  type.  {@code PathReceiver.changedPath()} is invoked when a
     *  path in this graph is changed.
     *
     *  @param  pathGenusType a path genus type 
     *  @throws org.osid.NullArgumentException {@code pathGenusType}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedPathsByGenusType(org.osid.type.Type pathGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of updated paths for the given
     *  starting node {@code Id}. {@code PathReceiver.changedPath()}
     *  is invoked when a {@code Path} in this graph is changed.
     *
     *  @param  startingNodeId the {@code Id} of the starting node to monitor
     *  @throws org.osid.NullArgumentException {@code startingNodeId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedPathsForStartingNode(org.osid.id.Id startingNodeId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated paths for the given
     *  ending node {@code Id}. {@code PathReceiver.changedPath()} is
     *  invoked when a {@code Path} in this graph is changed.
     *
     *  @param  endingNodeId the {@code Id} of the ending node to monitor
     *  @throws org.osid.NullArgumentException {@code endingNodeId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedPathsForEndingNode(org.osid.id.Id endingNodeId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated path. {@code
     *  PathReceiver.changedPath()} is invoked when the specified path
     *  is changed.
     *
     *  @param pathId the {@code Id} of the {@code Path} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code pathId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedPath(org.osid.id.Id pathId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted paths. {@code
     *  PathReceiver.deletedPath()} is invoked when a path is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedPaths()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of deleted paths of the given genus
     *  type.  {@code PathReceiver.deletedPath()} is invoked when a
     *  path is deleted or removed from this graph.
     *
     *  @param  pathGenusType a path genus type 
     *  @throws org.osid.NullArgumentException {@code pathGenusType}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForDeletedPathsByGenusType(org.osid.type.Type pathGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of deleted paths for the given
     *  starting node {@code Id}. {@code PathReceiver.deletedPath()}
     *  is invoked when a {@code Path} is deleted or removed from this
     *  graph.
     *
     *  @param  startingNodeId the {@code Id} of the starting node to monitor
     *  @throws org.osid.NullArgumentException {@code startingNodeId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */
      
    @OSID @Override
    public void registerForDeletedPathsForStartingNode(org.osid.id.Id startingNodeId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of deleted paths for the given
     *  ending node {@code Id}. {@code PathReceiver.deletedPath()} is
     *  invoked when a {@code Path} is deleted or removed from this
     *  graph.
     *
     *  @param  endingNodeId the {@code Id} of the ending node to monitor
     *  @throws org.osid.NullArgumentException {@code endingNodeId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForDeletedPathsForEndingNode(org.osid.id.Id endingNodeId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of a deleted path. {@code
     *  PathReceiver.deletedPath()} is invoked when the specified path
     *  is deleted.
     *
     *  @param pathId the {@code Id} of the
     *          {@code Path} to monitor
     *  @throws org.osid.NullArgumentException {@code pathId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedPath(org.osid.id.Id pathId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
