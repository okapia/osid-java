//
// AbstractBranchSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.journaling.branch.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractBranchSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.journaling.BranchSearchResults {

    private org.osid.journaling.BranchList branches;
    private final org.osid.journaling.BranchQueryInspector inspector;
    private final java.util.Collection<org.osid.journaling.records.BranchSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractBranchSearchResults.
     *
     *  @param branches the result set
     *  @param branchQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>branches</code>
     *          or <code>branchQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractBranchSearchResults(org.osid.journaling.BranchList branches,
                                            org.osid.journaling.BranchQueryInspector branchQueryInspector) {
        nullarg(branches, "branches");
        nullarg(branchQueryInspector, "branch query inspectpr");

        this.branches = branches;
        this.inspector = branchQueryInspector;

        return;
    }


    /**
     *  Gets the branch list resulting from a search.
     *
     *  @return a branch list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.journaling.BranchList getBranches() {
        if (this.branches == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.journaling.BranchList branches = this.branches;
        this.branches = null;
	return (branches);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.journaling.BranchQueryInspector getBranchQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  branch search record <code> Type. </code> This method must
     *  be used to retrieve a branch implementing the requested
     *  record.
     *
     *  @param branchSearchRecordType a branch search 
     *         record type 
     *  @return the branch search
     *  @throws org.osid.NullArgumentException
     *          <code>branchSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(branchSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.journaling.records.BranchSearchResultsRecord getBranchSearchResultsRecord(org.osid.type.Type branchSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.journaling.records.BranchSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(branchSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(branchSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record branch search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addBranchRecord(org.osid.journaling.records.BranchSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "branch record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
