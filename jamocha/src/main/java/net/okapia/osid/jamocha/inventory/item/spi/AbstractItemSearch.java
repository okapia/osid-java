//
// AbstractItemSearch.java
//
//     A template for making an Item Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.item.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing item searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractItemSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.inventory.ItemSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.inventory.records.ItemSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.inventory.ItemSearchOrder itemSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of items. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  itemIds list of items
     *  @throws org.osid.NullArgumentException
     *          <code>itemIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongItems(org.osid.id.IdList itemIds) {
        while (itemIds.hasNext()) {
            try {
                this.ids.add(itemIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongItems</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of item Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getItemIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  itemSearchOrder item search order 
     *  @throws org.osid.NullArgumentException
     *          <code>itemSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>itemSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderItemResults(org.osid.inventory.ItemSearchOrder itemSearchOrder) {
	this.itemSearchOrder = itemSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.inventory.ItemSearchOrder getItemSearchOrder() {
	return (this.itemSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given item search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an item implementing the requested record.
     *
     *  @param itemSearchRecordType an item search record
     *         type
     *  @return the item search record
     *  @throws org.osid.NullArgumentException
     *          <code>itemSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(itemSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.records.ItemSearchRecord getItemSearchRecord(org.osid.type.Type itemSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.inventory.records.ItemSearchRecord record : this.records) {
            if (record.implementsRecordType(itemSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(itemSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this item search. 
     *
     *  @param itemSearchRecord item search record
     *  @param itemSearchRecordType item search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addItemSearchRecord(org.osid.inventory.records.ItemSearchRecord itemSearchRecord, 
                                           org.osid.type.Type itemSearchRecordType) {

        addRecordType(itemSearchRecordType);
        this.records.add(itemSearchRecord);        
        return;
    }
}
