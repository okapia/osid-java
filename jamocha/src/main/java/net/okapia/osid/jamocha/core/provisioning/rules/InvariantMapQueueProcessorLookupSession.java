//
// InvariantMapQueueProcessorLookupSession
//
//    Implements a QueueProcessor lookup service backed by a fixed collection of
//    queueProcessors.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.rules;


/**
 *  Implements a QueueProcessor lookup service backed by a fixed
 *  collection of queue processors. The queue processors are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapQueueProcessorLookupSession
    extends net.okapia.osid.jamocha.core.provisioning.rules.spi.AbstractMapQueueProcessorLookupSession
    implements org.osid.provisioning.rules.QueueProcessorLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapQueueProcessorLookupSession</code> with no
     *  queue processors.
     *  
     *  @param distributor the distributor
     *  @throws org.osid.NullArgumnetException {@code distributor} is
     *          {@code null}
     */

    public InvariantMapQueueProcessorLookupSession(org.osid.provisioning.Distributor distributor) {
        setDistributor(distributor);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapQueueProcessorLookupSession</code> with a single
     *  queue processor.
     *  
     *  @param distributor the distributor
     *  @param queueProcessor a single queue processor
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code queueProcessor} is <code>null</code>
     */

      public InvariantMapQueueProcessorLookupSession(org.osid.provisioning.Distributor distributor,
                                               org.osid.provisioning.rules.QueueProcessor queueProcessor) {
        this(distributor);
        putQueueProcessor(queueProcessor);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapQueueProcessorLookupSession</code> using an array
     *  of queue processors.
     *  
     *  @param distributor the distributor
     *  @param queueProcessors an array of queue processors
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code queueProcessors} is <code>null</code>
     */

      public InvariantMapQueueProcessorLookupSession(org.osid.provisioning.Distributor distributor,
                                               org.osid.provisioning.rules.QueueProcessor[] queueProcessors) {
        this(distributor);
        putQueueProcessors(queueProcessors);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapQueueProcessorLookupSession</code> using a
     *  collection of queue processors.
     *
     *  @param distributor the distributor
     *  @param queueProcessors a collection of queue processors
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code queueProcessors} is <code>null</code>
     */

      public InvariantMapQueueProcessorLookupSession(org.osid.provisioning.Distributor distributor,
                                               java.util.Collection<? extends org.osid.provisioning.rules.QueueProcessor> queueProcessors) {
        this(distributor);
        putQueueProcessors(queueProcessors);
        return;
    }
}
