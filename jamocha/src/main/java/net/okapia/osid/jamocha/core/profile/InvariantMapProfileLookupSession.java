//
// InvariantMapProfileLookupSession
//
//    Implements a Profile lookup service backed by a fixed collection of
//    profiles.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.profile;


/**
 *  Implements a Profile lookup service backed by a fixed
 *  collection of profiles. The profiles are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProfileLookupSession
    extends net.okapia.osid.jamocha.core.profile.spi.AbstractMapProfileLookupSession
    implements org.osid.profile.ProfileLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapProfileLookupSession</code> with no
     *  profiles.
     */

    public InvariantMapProfileLookupSession() {
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapProfileLookupSession</code> with a single
     *  profile.
     *  
     *  @throws org.osid.NullArgumentException {@code profile}
     *          is <code>null</code>
     */

    public InvariantMapProfileLookupSession(org.osid.profile.Profile profile) {
        putProfile(profile);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapProfileLookupSession</code> using an array
     *  of profiles.
     *  
     *  @throws org.osid.NullArgumentException {@code profiles}
     *          is <code>null</code>
     */

    public InvariantMapProfileLookupSession(org.osid.profile.Profile[] profiles) {
        putProfiles(profiles);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapProfileLookupSession</code> using a
     *  collection of profiles.
     *
     *  @throws org.osid.NullArgumentException {@code profiles}
     *          is <code>null</code>
     */

    public InvariantMapProfileLookupSession(java.util.Collection<? extends org.osid.profile.Profile> profiles) {
        putProfiles(profiles);
        return;
    }
}
