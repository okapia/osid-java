//
// AbstractRecurringEventEnablerSearch.java
//
//     A template for making a RecurringEventEnabler Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.rules.recurringeventenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing recurring event enabler searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractRecurringEventEnablerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.calendaring.rules.RecurringEventEnablerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.calendaring.rules.records.RecurringEventEnablerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.calendaring.rules.RecurringEventEnablerSearchOrder recurringEventEnablerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of recurring event enablers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  recurringEventEnablerIds list of recurring event enablers
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventEnablerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongRecurringEventEnablers(org.osid.id.IdList recurringEventEnablerIds) {
        while (recurringEventEnablerIds.hasNext()) {
            try {
                this.ids.add(recurringEventEnablerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongRecurringEventEnablers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of recurring event enabler Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getRecurringEventEnablerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  recurringEventEnablerSearchOrder recurring event enabler search order 
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventEnablerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>recurringEventEnablerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderRecurringEventEnablerResults(org.osid.calendaring.rules.RecurringEventEnablerSearchOrder recurringEventEnablerSearchOrder) {
	this.recurringEventEnablerSearchOrder = recurringEventEnablerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.calendaring.rules.RecurringEventEnablerSearchOrder getRecurringEventEnablerSearchOrder() {
	return (this.recurringEventEnablerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given recurring event enabler search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a recurring event enabler implementing the requested record.
     *
     *  @param recurringEventEnablerSearchRecordType a recurring event enabler search record
     *         type
     *  @return the recurring event enabler search record
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(recurringEventEnablerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.rules.records.RecurringEventEnablerSearchRecord getRecurringEventEnablerSearchRecord(org.osid.type.Type recurringEventEnablerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.calendaring.rules.records.RecurringEventEnablerSearchRecord record : this.records) {
            if (record.implementsRecordType(recurringEventEnablerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(recurringEventEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this recurring event enabler search. 
     *
     *  @param recurringEventEnablerSearchRecord recurring event enabler search record
     *  @param recurringEventEnablerSearchRecordType recurringEventEnabler search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRecurringEventEnablerSearchRecord(org.osid.calendaring.rules.records.RecurringEventEnablerSearchRecord recurringEventEnablerSearchRecord, 
                                           org.osid.type.Type recurringEventEnablerSearchRecordType) {

        addRecordType(recurringEventEnablerSearchRecordType);
        this.records.add(recurringEventEnablerSearchRecord);        
        return;
    }
}
