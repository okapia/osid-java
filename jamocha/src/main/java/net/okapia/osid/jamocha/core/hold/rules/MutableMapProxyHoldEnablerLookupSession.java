//
// MutableMapProxyHoldEnablerLookupSession
//
//    Implements a HoldEnabler lookup service backed by a collection of
//    holdEnablers that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.hold.rules;


/**
 *  Implements a HoldEnabler lookup service backed by a collection of
 *  holdEnablers. The holdEnablers are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of hold enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyHoldEnablerLookupSession
    extends net.okapia.osid.jamocha.core.hold.rules.spi.AbstractMapHoldEnablerLookupSession
    implements org.osid.hold.rules.HoldEnablerLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyHoldEnablerLookupSession}
     *  with no hold enablers.
     *
     *  @param oubliette the oubliette
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code oubliette} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyHoldEnablerLookupSession(org.osid.hold.Oubliette oubliette,
                                                  org.osid.proxy.Proxy proxy) {
        setOubliette(oubliette);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyHoldEnablerLookupSession} with a
     *  single hold enabler.
     *
     *  @param oubliette the oubliette
     *  @param holdEnabler a hold enabler
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code oubliette},
     *          {@code holdEnabler}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyHoldEnablerLookupSession(org.osid.hold.Oubliette oubliette,
                                                org.osid.hold.rules.HoldEnabler holdEnabler, org.osid.proxy.Proxy proxy) {
        this(oubliette, proxy);
        putHoldEnabler(holdEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyHoldEnablerLookupSession} using an
     *  array of hold enablers.
     *
     *  @param oubliette the oubliette
     *  @param holdEnablers an array of hold enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code oubliette},
     *          {@code holdEnablers}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyHoldEnablerLookupSession(org.osid.hold.Oubliette oubliette,
                                                org.osid.hold.rules.HoldEnabler[] holdEnablers, org.osid.proxy.Proxy proxy) {
        this(oubliette, proxy);
        putHoldEnablers(holdEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyHoldEnablerLookupSession} using a
     *  collection of hold enablers.
     *
     *  @param oubliette the oubliette
     *  @param holdEnablers a collection of hold enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code oubliette},
     *          {@code holdEnablers}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyHoldEnablerLookupSession(org.osid.hold.Oubliette oubliette,
                                                java.util.Collection<? extends org.osid.hold.rules.HoldEnabler> holdEnablers,
                                                org.osid.proxy.Proxy proxy) {
   
        this(oubliette, proxy);
        setSessionProxy(proxy);
        putHoldEnablers(holdEnablers);
        return;
    }

    
    /**
     *  Makes a {@code HoldEnabler} available in this session.
     *
     *  @param holdEnabler an hold enabler
     *  @throws org.osid.NullArgumentException {@code holdEnabler{@code 
     *          is {@code null}
     */

    @Override
    public void putHoldEnabler(org.osid.hold.rules.HoldEnabler holdEnabler) {
        super.putHoldEnabler(holdEnabler);
        return;
    }


    /**
     *  Makes an array of holdEnablers available in this session.
     *
     *  @param holdEnablers an array of hold enablers
     *  @throws org.osid.NullArgumentException {@code holdEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putHoldEnablers(org.osid.hold.rules.HoldEnabler[] holdEnablers) {
        super.putHoldEnablers(holdEnablers);
        return;
    }


    /**
     *  Makes collection of hold enablers available in this session.
     *
     *  @param holdEnablers
     *  @throws org.osid.NullArgumentException {@code holdEnabler{@code 
     *          is {@code null}
     */

    @Override
    public void putHoldEnablers(java.util.Collection<? extends org.osid.hold.rules.HoldEnabler> holdEnablers) {
        super.putHoldEnablers(holdEnablers);
        return;
    }


    /**
     *  Removes a HoldEnabler from this session.
     *
     *  @param holdEnablerId the {@code Id} of the hold enabler
     *  @throws org.osid.NullArgumentException {@code holdEnablerId{@code  is
     *          {@code null}
     */

    @Override
    public void removeHoldEnabler(org.osid.id.Id holdEnablerId) {
        super.removeHoldEnabler(holdEnablerId);
        return;
    }    
}
