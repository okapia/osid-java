//
// AbstractAdapterTodoLookupSession.java
//
//    A Todo lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.checklist.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Todo lookup session adapter.
 */

public abstract class AbstractAdapterTodoLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.checklist.TodoLookupSession {

    private final org.osid.checklist.TodoLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterTodoLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterTodoLookupSession(org.osid.checklist.TodoLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Checklist/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Checklist Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getChecklistId() {
        return (this.session.getChecklistId());
    }


    /**
     *  Gets the {@code Checklist} associated with this session.
     *
     *  @return the {@code Checklist} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.Checklist getChecklist()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getChecklist());
    }


    /**
     *  Tests if this user can perform {@code Todo} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupTodos() {
        return (this.session.canLookupTodos());
    }


    /**
     *  A complete view of the {@code Todo} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeTodoView() {
        this.session.useComparativeTodoView();
        return;
    }


    /**
     *  A complete view of the {@code Todo} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryTodoView() {
        this.session.usePlenaryTodoView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include todos in checklists which are children
     *  of this checklist in the checklist hierarchy.
     */

    @OSID @Override
    public void useFederatedChecklistView() {
        this.session.useFederatedChecklistView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this checklist only.
     */

    @OSID @Override
    public void useIsolatedChecklistView() {
        this.session.useIsolatedChecklistView();
        return;
    }
    

    /**
     *  Only todos whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveTodoView() {
        this.session.useEffectiveTodoView();
        return;
    }
    

    /**
     *  All todos of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveTodoView() {
        this.session.useAnyEffectiveTodoView();
        return;
    }


    /**
     *  The returns from the lookup methods omit sequestered
     *  todos.
     */

    @OSID @Override
    public void useSequesteredTodoView() {
        this.session.useSequesteredTodoView();
        return;
    }


    /**
     *  All todos are returned including sequestered todos.
     */

    @OSID @Override
    public void useUnsequesteredTodoView() {
        this.session.useUnsequesteredTodoView();
        return;
    }

     
    /**
     *  Gets the {@code Todo} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Todo} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Todo} and
     *  retained for compatibility.
     *
     *  In effective mode, todos are returned that are currently
     *  effective.  In any effective mode, effective todos and
     *  those currently expired are returned.
     *
     *  @param todoId {@code Id} of the {@code Todo}
     *  @return the todo
     *  @throws org.osid.NotFoundException {@code todoId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code todoId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.Todo getTodo(org.osid.id.Id todoId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTodo(todoId));
    }


    /**
     *  Gets a {@code TodoList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  todos specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Todos} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, todos are returned that are currently
     *  effective.  In any effective mode, effective todos and
     *  those currently expired are returned.
     *
     *  @param  todoIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Todo} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code todoIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.TodoList getTodosByIds(org.osid.id.IdList todoIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTodosByIds(todoIds));
    }


    /**
     *  Gets a {@code TodoList} corresponding to the given
     *  todo genus {@code Type} which does not include
     *  todos of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  todos or an error results. Otherwise, the returned list
     *  may contain only those todos that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, todos are returned that are currently
     *  effective.  In any effective mode, effective todos and
     *  those currently expired are returned.
     *
     *  @param  todoGenusType a todo genus type 
     *  @return the returned {@code Todo} list
     *  @throws org.osid.NullArgumentException
     *          {@code todoGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.TodoList getTodosByGenusType(org.osid.type.Type todoGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTodosByGenusType(todoGenusType));
    }


    /**
     *  Gets a {@code TodoList} corresponding to the given
     *  todo genus {@code Type} and include any additional
     *  todos with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  todos or an error results. Otherwise, the returned list
     *  may contain only those todos that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, todos are returned that are currently
     *  effective.  In any effective mode, effective todos and
     *  those currently expired are returned.
     *
     *  @param  todoGenusType a todo genus type 
     *  @return the returned {@code Todo} list
     *  @throws org.osid.NullArgumentException
     *          {@code todoGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.TodoList getTodosByParentGenusType(org.osid.type.Type todoGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTodosByParentGenusType(todoGenusType));
    }


    /**
     *  Gets a {@code TodoList} containing the given
     *  todo record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  todos or an error results. Otherwise, the returned list
     *  may contain only those todos that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, todos are returned that are currently
     *  effective.  In any effective mode, effective todos and
     *  those currently expired are returned.
     *
     *  @param  todoRecordType a todo record type 
     *  @return the returned {@code Todo} list
     *  @throws org.osid.NullArgumentException
     *          {@code todoRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.TodoList getTodosByRecordType(org.osid.type.Type todoRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTodosByRecordType(todoRecordType));
    }


    /**
     *  Gets a {@code TodoList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  todos or an error results. Otherwise, the returned list
     *  may contain only those todos that are accessible
     *  through this session.
     *  
     *  In active mode, todos are returned that are currently
     *  active. In any status mode, active and inactive todos
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Todo} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.checklist.TodoList getTodosOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTodosOnDate(from, to));
    }
        

    /**
     *  Gets a {@code TodoList} at the given priority {@code
     *  Type} or higher.
     *  
     *  In plenary mode, the returned list contains all known todos or
     *  an error results. Otherwise, the returned list may contain
     *  only those todos that are accessible through this session.
     *  
     *  In effective mode, todos are returned that are currently
     *  effective. In any effective mode, effective todos and those
     *  currently expired are returned.
     *
     *  @param  priorityType a priority type 
     *  @return the returned {@code Todo} list 
     *  @throws org.osid.NullArgumentException {@code priorityType} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.checklist.TodoList getTodosByPriority(org.osid.type.Type priorityType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTodosByPriority(priorityType));
    }


    /**
     *  Gets a {@code TodoList} with a due date within the
     *  given date range inclusive.
     *  
     *  In plenary mode, the returned list contains all known todos or
     *  an error results. Otherwise, the returned list may contain
     *  only those todos that are accessible through this session.
     *  
     *  In effective mode, todos are returned that are currently
     *  effective. In any effective mode, effective todos and those
     *  currently expired are returned.
     *  
     *  In sequestered mode, no sequestered todos are returned. In
     *  unsequestered mode, all todos are returned.
     *
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned {@code Todo} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code from} or {@code 
     *          to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.checklist.TodoList getTodosByDueDate(org.osid.calendaring.DateTime from, 
                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTodosByDueDate(from, to));
    }


    /**
     *  Gets a {@code TodoList} of todos dependent upon the
     *  given todo.
     *  
     *  In plenary mode, the returned list contains all known todos or
     *  an error results. Otherwise, the returned list may contain
     *  only those todos that are accessible through this session.
     *  
     *  In effective mode, todos are returned that are currently
     *  effective. In any effective mode, effective todos and those
     *  currently expired are returned.
     *  
     *  In sequestered mode, no sequestered todos are returned. In
     *  unsequestered mode, all todos are returned.
     *
     *  @param  dependencyTodoId a todo 
     *  @return the returned {@code Todo} list 
     *  @throws org.osid.NullArgumentException {@code dependencyTodoId} 
     *          is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
    
    @OSID @Override
    public org.osid.checklist.TodoList getTodosByDependency(org.osid.id.Id dependencyTodoId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTodosByDependency(dependencyTodoId));
    }

    
    /**
     *  Gets all {@code Todos}. 
     *
     *  In plenary mode, the returned list contains all known
     *  todos or an error results. Otherwise, the returned list
     *  may contain only those todos that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, todos are returned that are currently
     *  effective.  In any effective mode, effective todos and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Todos} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.TodoList getTodos()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTodos());
    }
}
