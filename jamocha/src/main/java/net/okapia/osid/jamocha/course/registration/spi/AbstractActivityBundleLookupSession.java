//
// AbstractActivityBundleLookupSession.java
//
//    A starter implementation framework for providing an ActivityBundle
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.registration.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an ActivityBundle
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getActivityBundles(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractActivityBundleLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.course.registration.ActivityBundleLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.course.CourseCatalog courseCatalog = new net.okapia.osid.jamocha.nil.course.coursecatalog.UnknownCourseCatalog();
    

    /**
     *  Gets the <code>CourseCatalog/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.courseCatalog.getId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.courseCatalog);
    }


    /**
     *  Sets the <code>CourseCatalog</code>.
     *
     *  @param  courseCatalog the course catalog for this session
     *  @throws org.osid.NullArgumentException <code>courseCatalog</code>
     *          is <code>null</code>
     */

    protected void setCourseCatalog(org.osid.course.CourseCatalog courseCatalog) {
        nullarg(courseCatalog, "course catalog");
        this.courseCatalog = courseCatalog;
        return;
    }


    /**
     *  Tests if this user can perform <code>ActivityBundle</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupActivityBundles() {
        return (true);
    }


    /**
     *  A complete view of the <code>ActivityBundle</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeActivityBundleView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>ActivityBundle</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryActivityBundleView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include activity bundles in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>ActivityBundle</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>ActivityBundle</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>ActivityBundle</code> and
     *  retained for compatibility.
     *
     *  @param  activityBundleId <code>Id</code> of the
     *          <code>ActivityBundle</code>
     *  @return the activity bundle
     *  @throws org.osid.NotFoundException <code>activityBundleId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>activityBundleId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundle getActivityBundle(org.osid.id.Id activityBundleId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.course.registration.ActivityBundleList activityBundles = getActivityBundles()) {
            while (activityBundles.hasNext()) {
                org.osid.course.registration.ActivityBundle activityBundle = activityBundles.getNextActivityBundle();
                if (activityBundle.getId().equals(activityBundleId)) {
                    return (activityBundle);
                }
            }
        } 

        throw new org.osid.NotFoundException(activityBundleId + " not found");
    }


    /**
     *  Gets an <code>ActivityBundleList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  activityBundles specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>ActivityBundles</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getActivityBundles()</code>.
     *
     *  @param  activityBundleIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>ActivityBundle</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>activityBundleIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleList getActivityBundlesByIds(org.osid.id.IdList activityBundleIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.course.registration.ActivityBundle> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = activityBundleIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getActivityBundle(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("activity bundle " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.course.registration.activitybundle.LinkedActivityBundleList(ret));
    }


    /**
     *  Gets an <code>ActivityBundleList</code> corresponding to the given
     *  activity bundle genus <code>Type</code> which does not include
     *  activity bundles of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  activity bundles or an error results. Otherwise, the returned list
     *  may contain only those activity bundles that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getActivityBundles()</code>.
     *
     *  @param  activityBundleGenusType an activityBundle genus type 
     *  @return the returned <code>ActivityBundle</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityBundleGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleList getActivityBundlesByGenusType(org.osid.type.Type activityBundleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.registration.activitybundle.ActivityBundleGenusFilterList(getActivityBundles(), activityBundleGenusType));
    }


    /**
     *  Gets an <code>ActivityBundleList</code> corresponding to the given
     *  activity bundle genus <code>Type</code> and include any additional
     *  activity bundles with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  activity bundles or an error results. Otherwise, the returned list
     *  may contain only those activity bundles that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getActivityBundles()</code>.
     *
     *  @param  activityBundleGenusType an activityBundle genus type 
     *  @return the returned <code>ActivityBundle</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityBundleGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleList getActivityBundlesByParentGenusType(org.osid.type.Type activityBundleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getActivityBundlesByGenusType(activityBundleGenusType));
    }


    /**
     *  Gets an <code>ActivityBundleList</code> containing the given
     *  activity bundle record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  activity bundles or an error results. Otherwise, the returned list
     *  may contain only those activity bundles that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getActivityBundles()</code>.
     *
     *  @param  activityBundleRecordType an activityBundle record type 
     *  @return the returned <code>ActivityBundle</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityBundleRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleList getActivityBundlesByRecordType(org.osid.type.Type activityBundleRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.registration.activitybundle.ActivityBundleRecordFilterList(getActivityBundles(), activityBundleRecordType));
    }


    /**
     *  Gets an <code> ActivityBundleList </code> for a given course
     *  offering <code> . </code> In plenary mode, the returned list
     *  contains all known courses or an error results. Otherwise, the
     *  returned list may contain only those activity bundles that are
     *  accessible through this session.
     *
     *  @param  courseOfferingId a course offering <code> Id </code> 
     *  @return the returned <code> ActivityBundle </code> list 
     *  @throws org.osid.NullArgumentException <code> courseOfferingId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleList getActivityBundlesForCourseOffering(org.osid.id.Id courseOfferingId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.registration.activitybundle.ActivityBundleFilterList(new CourseOfferingFilter(courseOfferingId),  getActivityBundles()));
    }


    /**
     *  Gets an <code> ActivityBundleList </code> for a given
     *  activity. In plenary mode, the returned list contains all
     *  known courses or an error results. Otherwise, the returned
     *  list may contain only those activity bundles that are
     *  accessible through this session.
     *
     *  @param  activityId an activity <code> Id </code> 
     *  @return the returned <code> ActivityBundle </code> list 
     *  @throws org.osid.NullArgumentException <code> activityId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleList getActivityBundlesByActivity(org.osid.id.Id activityId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.course.registration.ActivityBundle> ret = new java.util.ArrayList<>();

        try (org.osid.course.registration.ActivityBundleList bundles = getActivityBundles()) {
            while (bundles.hasNext()) {
                org.osid.course.registration.ActivityBundle bundle = bundles.getNextActivityBundle();
                try (org.osid.id.IdList ids = bundle.getActivityIds()) {
                    while (ids.hasNext()) {
                        if (ids.getNextId().equals(activityId)) {
                            ret.add(bundle);
                        }
                    }
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.course.registration.activitybundle.LinkedActivityBundleList(ret));
    }


    /**
     *  Gets all <code>ActivityBundles</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  activity bundles or an error results. Otherwise, the returned list
     *  may contain only those activity bundles that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>ActivityBundles</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.course.registration.ActivityBundleList getActivityBundles()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the activity bundle list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of activity bundles
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.course.registration.ActivityBundleList filterActivityBundlesOnViews(org.osid.course.registration.ActivityBundleList list)
        throws org.osid.OperationFailedException {

        return (list);
    }


    public static class CourseOfferingFilter
        implements net.okapia.osid.jamocha.inline.filter.course.registration.activitybundle.ActivityBundleFilter {

        private final org.osid.id.Id courseOfferingId;

        
        /**
         *  Constructs a new <code>CourseOfferingFilter</code>.
         *
         *  @param courseOfferingId the course offering to filter
         *  @throws org.osid.NullArgumentException
         *          <code>courseOfferingId</code> is <code>null</code>
         */

        public CourseOfferingFilter(org.osid.id.Id courseOfferingId) {
            nullarg(courseOfferingId, "course offering Id");
            this.courseOfferingId = courseOfferingId;
            return;
        }


        /**
         *  Used by the CourseOfferingFilterList to filter the
         *  activity bundle list based on course offing.
         *
         *  @param bundle the activity bundle
         *  @return <code>true</code> to pass the activity bundle,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.registration.ActivityBundle bundle) {
            return (bundle.getCourseOfferingId().equals(this.courseOfferingId));
        }
    }        
}
