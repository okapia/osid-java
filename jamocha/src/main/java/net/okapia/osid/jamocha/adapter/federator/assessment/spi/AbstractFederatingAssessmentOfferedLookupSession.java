//
// AbstractFederatingAssessmentOfferedLookupSession.java
//
//     An abstract federating adapter for an AssessmentOfferedLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.assessment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  AssessmentOfferedLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingAssessmentOfferedLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.assessment.AssessmentOfferedLookupSession>
    implements org.osid.assessment.AssessmentOfferedLookupSession {

    private boolean parallel = false;
    private org.osid.assessment.Bank bank = new net.okapia.osid.jamocha.nil.assessment.bank.UnknownBank();


    /**
     *  Constructs a new <code>AbstractFederatingAssessmentOfferedLookupSession</code>.
     */

    protected AbstractFederatingAssessmentOfferedLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.assessment.AssessmentOfferedLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Bank/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Bank Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBankId() {
        return (this.bank.getId());
    }


    /**
     *  Gets the <code>Bank</code> associated with this 
     *  session.
     *
     *  @return the <code>Bank</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.Bank getBank()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.bank);
    }


    /**
     *  Sets the <code>Bank</code>.
     *
     *  @param  bank the bank for this session
     *  @throws org.osid.NullArgumentException <code>bank</code>
     *          is <code>null</code>
     */

    protected void setBank(org.osid.assessment.Bank bank) {
        nullarg(bank, "bank");
        this.bank = bank;
        return;
    }


    /**
     *  Tests if this user can perform <code>AssessmentOffered</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAssessmentsOffered() {
        for (org.osid.assessment.AssessmentOfferedLookupSession session : getSessions()) {
            if (session.canLookupAssessmentsOffered()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>AssessmentOffered</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAssessmentOfferedView() {
        for (org.osid.assessment.AssessmentOfferedLookupSession session : getSessions()) {
            session.useComparativeAssessmentOfferedView();
        }

        return;
    }


    /**
     *  A complete view of the <code>AssessmentOffered</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAssessmentOfferedView() {
        for (org.osid.assessment.AssessmentOfferedLookupSession session : getSessions()) {
            session.usePlenaryAssessmentOfferedView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include assessments offered in banks which are children
     *  of this bank in the bank hierarchy.
     */

    @OSID @Override
    public void useFederatedBankView() {
        for (org.osid.assessment.AssessmentOfferedLookupSession session : getSessions()) {
            session.useFederatedBankView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this bank only.
     */

    @OSID @Override
    public void useIsolatedBankView() {
        for (org.osid.assessment.AssessmentOfferedLookupSession session : getSessions()) {
            session.useIsolatedBankView();
        }

        return;
    }

     
    /**
     *  Gets the <code>AssessmentOffered</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>AssessmentOffered</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>AssessmentOffered</code> and
     *  retained for compatibility.
     *
     *  @param  assessmentOfferedId <code>Id</code> of the
     *          <code>AssessmentOffered</code>
     *  @return the assessment offered
     *  @throws org.osid.NotFoundException <code>assessmentOfferedId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>assessmentOfferedId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOffered getAssessmentOffered(org.osid.id.Id assessmentOfferedId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.assessment.AssessmentOfferedLookupSession session : getSessions()) {
            try {
                return (session.getAssessmentOffered(assessmentOfferedId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(assessmentOfferedId + " not found");
    }


    /**
     *  Gets an <code>AssessmentOfferedList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  assessmentsOffered specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>AssessmentsOffered</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  assessmentOfferedIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>AssessmentOffered</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentOfferedIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedList getAssessmentsOfferedByIds(org.osid.id.IdList assessmentOfferedIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.assessment.assessmentoffered.MutableAssessmentOfferedList ret = new net.okapia.osid.jamocha.assessment.assessmentoffered.MutableAssessmentOfferedList();

        try (org.osid.id.IdList ids = assessmentOfferedIds) {
            while (ids.hasNext()) {
                ret.addAssessmentOffered(getAssessmentOffered(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>AssessmentOfferedList</code> corresponding to the given
     *  assessment offered genus <code>Type</code> which does not include
     *  assessments offered of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  assessments offered or an error results. Otherwise, the
     *  returned list may contain only those assessments offered that
     *  are accessible through this session. In both cases, the order
     *  of the set is not specified.
     *
     *  @param  assessmentOfferedGenusType an assessmentOffered genus type 
     *  @return the returned <code>AssessmentOffered</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentOfferedGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedList getAssessmentsOfferedByGenusType(org.osid.type.Type assessmentOfferedGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.assessmentoffered.FederatingAssessmentOfferedList ret = getAssessmentOfferedList();

        for (org.osid.assessment.AssessmentOfferedLookupSession session : getSessions()) {
            ret.addAssessmentOfferedList(session.getAssessmentsOfferedByGenusType(assessmentOfferedGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AssessmentOfferedList</code> corresponding to the given
     *  assessment offered genus <code>Type</code> and include any additional
     *  assessments offered with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  assessments offered or an error results. Otherwise, the
     *  returned list may contain only those assessments offered that
     *  are accessible through this session. In both cases, the order
     *  of the set is not specified.
     *
     *  @param  assessmentOfferedGenusType an assessmentOffered genus type 
     *  @return the returned <code>AssessmentOffered</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentOfferedGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedList getAssessmentsOfferedByParentGenusType(org.osid.type.Type assessmentOfferedGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.assessmentoffered.FederatingAssessmentOfferedList ret = getAssessmentOfferedList();

        for (org.osid.assessment.AssessmentOfferedLookupSession session : getSessions()) {
            ret.addAssessmentOfferedList(session.getAssessmentsOfferedByParentGenusType(assessmentOfferedGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AssessmentOfferedList</code> containing the given
     *  assessment offered record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  assessments offered or an error results. Otherwise, the returned list
     *  may contain only those assessments offered that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  assessmentOfferedRecordType an assessmentOffered record type 
     *  @return the returned <code>AssessmentOffered</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentOfferedRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedList getAssessmentsOfferedByRecordType(org.osid.type.Type assessmentOfferedRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.assessmentoffered.FederatingAssessmentOfferedList ret = getAssessmentOfferedList();

        for (org.osid.assessment.AssessmentOfferedLookupSession session : getSessions()) {
            ret.addAssessmentOfferedList(session.getAssessmentsOfferedByRecordType(assessmentOfferedRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AssessmentOfferedList</code> that have
     *  designated start times where the start times fall in the given
     *  range inclusive.  In plenary mode, the returned list contains
     *  all known assessments offered or an error results. Otherwise,
     *  the returned list may contain only those assessments offered
     *  that are accessible through this session.
     *
     *  @param  start start of time range
     *  @param  end end of time range
     *  @return the returned <code> AssessmentOffered </code> list
     *  @throws org.osid.InvalidArgumentException <code> end </code> is less
     *          than <code> start </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     *          occurred
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedList getAssessmentsOfferedByDate(org.osid.calendaring.DateTime start,
                                                                                 org.osid.calendaring.DateTime end)
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.assessmentoffered.FederatingAssessmentOfferedList ret = getAssessmentOfferedList();

        for (org.osid.assessment.AssessmentOfferedLookupSession session : getSessions()) {
            ret.addAssessmentOfferedList(session.getAssessmentsOfferedByDate(start, end));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AssessmentOfferedList</code> by the given
     *  assessment.  In plenary mode, the returned list contains all
     *  known assessments offered or an error results. Otherwise, the
     *  returned list may contain only those assessments offered that
     *  are accessible through this session.
     *
     *  @param  assessmentId <code>Id</code> of an <code>Assessment</code> 
     *  @return the returned <code>AssessmentOffered</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization
     *          failure occurred
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedList getAssessmentsOfferedForAssessment(org.osid.id.Id assessmentId)
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.assessmentoffered.FederatingAssessmentOfferedList ret = getAssessmentOfferedList();

        for (org.osid.assessment.AssessmentOfferedLookupSession session : getSessions()) {
            ret.addAssessmentOfferedList(session.getAssessmentsOfferedForAssessment(assessmentId));
        }

        ret.noMore();
        return (ret);
    }

    
    /**
     *  Gets all <code>AssessmentsOffered</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  assessments offered or an error results. Otherwise, the returned list
     *  may contain only those assessments offered that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>AssessmentsOffered</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedList getAssessmentsOffered()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.assessmentoffered.FederatingAssessmentOfferedList ret = getAssessmentOfferedList();

        for (org.osid.assessment.AssessmentOfferedLookupSession session : getSessions()) {
            ret.addAssessmentOfferedList(session.getAssessmentsOffered());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.assessment.assessmentoffered.FederatingAssessmentOfferedList getAssessmentOfferedList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.assessment.assessmentoffered.ParallelAssessmentOfferedList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.assessment.assessmentoffered.CompositeAssessmentOfferedList());
        }
    }
}
