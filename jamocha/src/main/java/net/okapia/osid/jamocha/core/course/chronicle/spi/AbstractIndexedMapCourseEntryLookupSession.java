//
// AbstractIndexedMapCourseEntryLookupSession.java
//
//    A simple framework for providing a CourseEntry lookup service
//    backed by a fixed collection of course entries with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.chronicle.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a CourseEntry lookup service backed by a
 *  fixed collection of course entries. The course entries are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some course entries may be compatible
 *  with more types than are indicated through these course entry
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>CourseEntries</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapCourseEntryLookupSession
    extends AbstractMapCourseEntryLookupSession
    implements org.osid.course.chronicle.CourseEntryLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.course.chronicle.CourseEntry> courseEntriesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.chronicle.CourseEntry>());
    private final MultiMap<org.osid.type.Type, org.osid.course.chronicle.CourseEntry> courseEntriesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.chronicle.CourseEntry>());


    /**
     *  Makes a <code>CourseEntry</code> available in this session.
     *
     *  @param  courseEntry a course entry
     *  @throws org.osid.NullArgumentException <code>courseEntry<code> is
     *          <code>null</code>
     */

    @Override
    protected void putCourseEntry(org.osid.course.chronicle.CourseEntry courseEntry) {
        super.putCourseEntry(courseEntry);

        this.courseEntriesByGenus.put(courseEntry.getGenusType(), courseEntry);
        
        try (org.osid.type.TypeList types = courseEntry.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.courseEntriesByRecord.put(types.getNextType(), courseEntry);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a course entry from this session.
     *
     *  @param courseEntryId the <code>Id</code> of the course entry
     *  @throws org.osid.NullArgumentException <code>courseEntryId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeCourseEntry(org.osid.id.Id courseEntryId) {
        org.osid.course.chronicle.CourseEntry courseEntry;
        try {
            courseEntry = getCourseEntry(courseEntryId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.courseEntriesByGenus.remove(courseEntry.getGenusType());

        try (org.osid.type.TypeList types = courseEntry.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.courseEntriesByRecord.remove(types.getNextType(), courseEntry);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeCourseEntry(courseEntryId);
        return;
    }


    /**
     *  Gets a <code>CourseEntryList</code> corresponding to the given
     *  course entry genus <code>Type</code> which does not include
     *  course entries of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known course entries or an error results. Otherwise,
     *  the returned list may contain only those course entries that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  courseEntryGenusType a course entry genus type 
     *  @return the returned <code>CourseEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>courseEntryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryList getCourseEntriesByGenusType(org.osid.type.Type courseEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.chronicle.courseentry.ArrayCourseEntryList(this.courseEntriesByGenus.get(courseEntryGenusType)));
    }


    /**
     *  Gets a <code>CourseEntryList</code> containing the given
     *  course entry record <code>Type</code>. In plenary mode, the
     *  returned list contains all known course entries or an error
     *  results. Otherwise, the returned list may contain only those
     *  course entries that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  courseEntryRecordType a course entry record type 
     *  @return the returned <code>courseEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>courseEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryList getCourseEntriesByRecordType(org.osid.type.Type courseEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.chronicle.courseentry.ArrayCourseEntryList(this.courseEntriesByRecord.get(courseEntryRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.courseEntriesByGenus.clear();
        this.courseEntriesByRecord.clear();

        super.close();

        return;
    }
}
