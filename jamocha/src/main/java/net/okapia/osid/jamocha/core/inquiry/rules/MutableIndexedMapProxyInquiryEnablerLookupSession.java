//
// MutableIndexedMapProxyInquiryEnablerLookupSession
//
//    Implements an InquiryEnabler lookup service backed by a collection of
//    inquiryEnablers indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inquiry.rules;


/**
 *  Implements an InquiryEnabler lookup service backed by a collection of
 *  inquiryEnablers. The inquiry enablers are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some inquiryEnablers may be compatible
 *  with more types than are indicated through these inquiryEnabler
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of inquiry enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyInquiryEnablerLookupSession
    extends net.okapia.osid.jamocha.core.inquiry.rules.spi.AbstractIndexedMapInquiryEnablerLookupSession
    implements org.osid.inquiry.rules.InquiryEnablerLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyInquiryEnablerLookupSession} with
     *  no inquiry enabler.
     *
     *  @param inquest the inquest
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code inquest} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyInquiryEnablerLookupSession(org.osid.inquiry.Inquest inquest,
                                                       org.osid.proxy.Proxy proxy) {
        setInquest(inquest);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyInquiryEnablerLookupSession} with
     *  a single inquiry enabler.
     *
     *  @param inquest the inquest
     *  @param  inquiryEnabler an inquiry enabler
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code inquest},
     *          {@code inquiryEnabler}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyInquiryEnablerLookupSession(org.osid.inquiry.Inquest inquest,
                                                       org.osid.inquiry.rules.InquiryEnabler inquiryEnabler, org.osid.proxy.Proxy proxy) {

        this(inquest, proxy);
        putInquiryEnabler(inquiryEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyInquiryEnablerLookupSession} using
     *  an array of inquiry enablers.
     *
     *  @param inquest the inquest
     *  @param  inquiryEnablers an array of inquiry enablers
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code inquest},
     *          {@code inquiryEnablers}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyInquiryEnablerLookupSession(org.osid.inquiry.Inquest inquest,
                                                       org.osid.inquiry.rules.InquiryEnabler[] inquiryEnablers, org.osid.proxy.Proxy proxy) {

        this(inquest, proxy);
        putInquiryEnablers(inquiryEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyInquiryEnablerLookupSession} using
     *  a collection of inquiry enablers.
     *
     *  @param inquest the inquest
     *  @param  inquiryEnablers a collection of inquiry enablers
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code inquest},
     *          {@code inquiryEnablers}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyInquiryEnablerLookupSession(org.osid.inquiry.Inquest inquest,
                                                       java.util.Collection<? extends org.osid.inquiry.rules.InquiryEnabler> inquiryEnablers,
                                                       org.osid.proxy.Proxy proxy) {
        this(inquest, proxy);
        putInquiryEnablers(inquiryEnablers);
        return;
    }

    
    /**
     *  Makes an {@code InquiryEnabler} available in this session.
     *
     *  @param  inquiryEnabler an inquiry enabler
     *  @throws org.osid.NullArgumentException {@code inquiryEnabler{@code 
     *          is {@code null}
     */

    @Override
    public void putInquiryEnabler(org.osid.inquiry.rules.InquiryEnabler inquiryEnabler) {
        super.putInquiryEnabler(inquiryEnabler);
        return;
    }


    /**
     *  Makes an array of inquiry enablers available in this session.
     *
     *  @param  inquiryEnablers an array of inquiry enablers
     *  @throws org.osid.NullArgumentException {@code inquiryEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putInquiryEnablers(org.osid.inquiry.rules.InquiryEnabler[] inquiryEnablers) {
        super.putInquiryEnablers(inquiryEnablers);
        return;
    }


    /**
     *  Makes collection of inquiry enablers available in this session.
     *
     *  @param  inquiryEnablers a collection of inquiry enablers
     *  @throws org.osid.NullArgumentException {@code inquiryEnabler{@code 
     *          is {@code null}
     */

    @Override
    public void putInquiryEnablers(java.util.Collection<? extends org.osid.inquiry.rules.InquiryEnabler> inquiryEnablers) {
        super.putInquiryEnablers(inquiryEnablers);
        return;
    }


    /**
     *  Removes an InquiryEnabler from this session.
     *
     *  @param inquiryEnablerId the {@code Id} of the inquiry enabler
     *  @throws org.osid.NullArgumentException {@code inquiryEnablerId{@code  is
     *          {@code null}
     */

    @Override
    public void removeInquiryEnabler(org.osid.id.Id inquiryEnablerId) {
        super.removeInquiryEnabler(inquiryEnablerId);
        return;
    }    
}
