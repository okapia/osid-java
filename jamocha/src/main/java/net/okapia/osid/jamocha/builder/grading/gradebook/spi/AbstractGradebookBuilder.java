//
// AbstractGradebook.java
//
//     Defines a Gradebook builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.grading.gradebook.spi;


/**
 *  Defines a <code>Gradebook</code> builder.
 */

public abstract class AbstractGradebookBuilder<T extends AbstractGradebookBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidCatalogBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.grading.gradebook.GradebookMiter gradebook;


    /**
     *  Constructs a new <code>AbstractGradebookBuilder</code>.
     *
     *  @param gradebook the gradebook to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractGradebookBuilder(net.okapia.osid.jamocha.builder.grading.gradebook.GradebookMiter gradebook) {
        super(gradebook);
        this.gradebook = gradebook;
        return;
    }


    /**
     *  Builds the gradebook.
     *
     *  @return the new gradebook
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.grading.Gradebook build() {
        (new net.okapia.osid.jamocha.builder.validator.grading.gradebook.GradebookValidator(getValidations())).validate(this.gradebook);
        return (new net.okapia.osid.jamocha.builder.grading.gradebook.ImmutableGradebook(this.gradebook));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the gradebook miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.grading.gradebook.GradebookMiter getMiter() {
        return (this.gradebook);
    }


    /**
     *  Adds a Gradebook record.
     *
     *  @param record a gradebook record
     *  @param recordType the type of gradebook record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.grading.records.GradebookRecord record, org.osid.type.Type recordType) {
        getMiter().addGradebookRecord(record, recordType);
        return (self());
    }
}       


