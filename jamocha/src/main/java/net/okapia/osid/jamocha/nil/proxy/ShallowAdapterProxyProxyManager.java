//
// ShallowProxyProxyManager.java
//
//     A nothing ProxyProxyManager adapter.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.proxy;


/**
 *  A nothing ProxyProxyManager adapter.
 */

public final class ShallowAdapterProxyProxyManager
    extends net.okapia.osid.jamocha.adapter.proxy.spi.AbstractAdapterProxyProxyManager
    implements org.osid.proxy.ProxyProxyManager {


    /**
     *  Constructs a new {@code ShallowAdapterProxyProxyManager} using
     *  the underlying provider.
     *
     *  @param manager the manager to adapt
     *  @throws org.osid.NullArgumentException {@code manager} is
     *          {@code null}
     */

    public ShallowAdapterProxyProxyManager(org.osid.proxy.ProxyProxyManager manager) {
        setAdapteeManager(manager);
        return;
    }


    /**
     *  Constructs a new {@code ShallowAdapterProxyProxyManager}.
     *
     *  @param manager the manager to adapt
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code manager} or
     *          {@code provider} is {@code null}
     */

    public ShallowAdapterProxyProxyManager(org.osid.proxy.ProxyProxyManager manager,
                                               net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        setAdapteeManager(manager);
        return;
    }
}
