//
// OsidEnablerMiter.java
//
//     Miter for OsidEnablers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.spi;


/**
 *  Miter for OsidEnablers.
 */

public interface OsidEnablerMiter
    extends OsidObjectMiter,
            OsidRuleMiter,
            TemporalMiter,
            org.osid.OsidEnabler {


    /**
     *  Sets the schedule and makes this enabler effective by schedule.
     *
     *  @param schedule the schedule
     *  @throws org.osid.NullArgumentException <code>schedule</code> is
     *          <code>null</code>
     */
    
    public void setSchedule(org.osid.calendaring.Schedule schedule);


    /**
     *  Sets the event and makes this enabler effective by event.
     *
     *  @param event the event
     *  @throws org.osid.NullArgumentException <code>event</code> is
     *          <code>null</code>
     */
    
    public void setEvent(org.osid.calendaring.Event event);


    /**
     *  Sets the cyclic event and makes this enabler effective by
     *  cyclic event.
     *
     *  @param cyclicEvent the cyclic event
     *  @throws org.osid.NullArgumentException <code>cyclicEvent</code> is
     *          <code>null</code>
     */
    
    public void setCyclicEvent(org.osid.calendaring.cycle.CyclicEvent cyclicEvent);


    /**
     *  Sets the demographic.
     *
     *  @param resource the demographic resource
     *  @throws org.osid.NullArgumentException <code>resource</code> is
     *          <code>null</code>
     */
    
    public void setDemographic(org.osid.resource.Resource resource);
}
