//
// AbstractCanonicalUnitEnablerSearch.java
//
//     A template for making a CanonicalUnitEnabler Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.rules.canonicalunitenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing canonical unit enabler searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractCanonicalUnitEnablerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.offering.rules.CanonicalUnitEnablerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.offering.rules.records.CanonicalUnitEnablerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.offering.rules.CanonicalUnitEnablerSearchOrder canonicalUnitEnablerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of canonical unit enablers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  canonicalUnitEnablerIds list of canonical unit enablers
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitEnablerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongCanonicalUnitEnablers(org.osid.id.IdList canonicalUnitEnablerIds) {
        while (canonicalUnitEnablerIds.hasNext()) {
            try {
                this.ids.add(canonicalUnitEnablerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongCanonicalUnitEnablers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of canonical unit enabler Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getCanonicalUnitEnablerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  canonicalUnitEnablerSearchOrder canonical unit enabler search order 
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitEnablerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>canonicalUnitEnablerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderCanonicalUnitEnablerResults(org.osid.offering.rules.CanonicalUnitEnablerSearchOrder canonicalUnitEnablerSearchOrder) {
	this.canonicalUnitEnablerSearchOrder = canonicalUnitEnablerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.offering.rules.CanonicalUnitEnablerSearchOrder getCanonicalUnitEnablerSearchOrder() {
	return (this.canonicalUnitEnablerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given canonical unit enabler search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a canonical unit enabler implementing the requested record.
     *
     *  @param canonicalUnitEnablerSearchRecordType a canonical unit enabler search record
     *         type
     *  @return the canonical unit enabler search record
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(canonicalUnitEnablerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.rules.records.CanonicalUnitEnablerSearchRecord getCanonicalUnitEnablerSearchRecord(org.osid.type.Type canonicalUnitEnablerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.offering.rules.records.CanonicalUnitEnablerSearchRecord record : this.records) {
            if (record.implementsRecordType(canonicalUnitEnablerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(canonicalUnitEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this canonical unit enabler search. 
     *
     *  @param canonicalUnitEnablerSearchRecord canonical unit enabler search record
     *  @param canonicalUnitEnablerSearchRecordType canonicalUnitEnabler search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCanonicalUnitEnablerSearchRecord(org.osid.offering.rules.records.CanonicalUnitEnablerSearchRecord canonicalUnitEnablerSearchRecord, 
                                           org.osid.type.Type canonicalUnitEnablerSearchRecordType) {

        addRecordType(canonicalUnitEnablerSearchRecordType);
        this.records.add(canonicalUnitEnablerSearchRecord);        
        return;
    }
}
