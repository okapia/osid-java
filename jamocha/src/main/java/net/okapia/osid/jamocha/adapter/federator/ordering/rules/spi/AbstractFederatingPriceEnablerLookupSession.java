//
// AbstractFederatingPriceEnablerLookupSession.java
//
//     An abstract federating adapter for a PriceEnablerLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.ordering.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  PriceEnablerLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingPriceEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.ordering.rules.PriceEnablerLookupSession>
    implements org.osid.ordering.rules.PriceEnablerLookupSession {

    private boolean parallel = false;
    private org.osid.ordering.Store store = new net.okapia.osid.jamocha.nil.ordering.store.UnknownStore();


    /**
     *  Constructs a new <code>AbstractFederatingPriceEnablerLookupSession</code>.
     */

    protected AbstractFederatingPriceEnablerLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.ordering.rules.PriceEnablerLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Store/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Store Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getStoreId() {
        return (this.store.getId());
    }


    /**
     *  Gets the <code>Store</code> associated with this 
     *  session.
     *
     *  @return the <code>Store</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.Store getStore()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.store);
    }


    /**
     *  Sets the <code>Store</code>.
     *
     *  @param  store the store for this session
     *  @throws org.osid.NullArgumentException <code>store</code>
     *          is <code>null</code>
     */

    protected void setStore(org.osid.ordering.Store store) {
        nullarg(store, "store");
        this.store = store;
        return;
    }


    /**
     *  Tests if this user can perform <code>PriceEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPriceEnablers() {
        for (org.osid.ordering.rules.PriceEnablerLookupSession session : getSessions()) {
            if (session.canLookupPriceEnablers()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>PriceEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePriceEnablerView() {
        for (org.osid.ordering.rules.PriceEnablerLookupSession session : getSessions()) {
            session.useComparativePriceEnablerView();
        }

        return;
    }


    /**
     *  A complete view of the <code>PriceEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPriceEnablerView() {
        for (org.osid.ordering.rules.PriceEnablerLookupSession session : getSessions()) {
            session.usePlenaryPriceEnablerView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include price enablers in stores which are children
     *  of this store in the store hierarchy.
     */

    @OSID @Override
    public void useFederatedStoreView() {
        for (org.osid.ordering.rules.PriceEnablerLookupSession session : getSessions()) {
            session.useFederatedStoreView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this store only.
     */

    @OSID @Override
    public void useIsolatedStoreView() {
        for (org.osid.ordering.rules.PriceEnablerLookupSession session : getSessions()) {
            session.useIsolatedStoreView();
        }

        return;
    }


    /**
     *  Only active price enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActivePriceEnablerView() {
        for (org.osid.ordering.rules.PriceEnablerLookupSession session : getSessions()) {
            session.useActivePriceEnablerView();
        }

        return;
    }


    /**
     *  Active and inactive price enablers are returned by methods in
     *  this session.
     */
    
    @OSID @Override
    public void useAnyStatusPriceEnablerView() {
        for (org.osid.ordering.rules.PriceEnablerLookupSession session : getSessions()) {
            session.useAnyStatusPriceEnablerView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>PriceEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>PriceEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>PriceEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, price enablers are returned that are currently
     *  active. In any status mode, active and inactive price enablers
     *  are returned.
     *
     *  @param  priceEnablerId <code>Id</code> of the
     *          <code>PriceEnabler</code>
     *  @return the price enabler
     *  @throws org.osid.NotFoundException <code>priceEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>priceEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnabler getPriceEnabler(org.osid.id.Id priceEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.ordering.rules.PriceEnablerLookupSession session : getSessions()) {
            try {
                return (session.getPriceEnabler(priceEnablerId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(priceEnablerId + " not found");
    }


    /**
     *  Gets a <code>PriceEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  priceEnablers specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>PriceEnablers</code> may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  In active mode, price enablers are returned that are currently
     *  active. In any status mode, active and inactive price enablers
     *  are returned.
     *
     *  @param  priceEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>PriceEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>priceEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerList getPriceEnablersByIds(org.osid.id.IdList priceEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.ordering.rules.priceenabler.MutablePriceEnablerList ret = new net.okapia.osid.jamocha.ordering.rules.priceenabler.MutablePriceEnablerList();

        try (org.osid.id.IdList ids = priceEnablerIds) {
            while (ids.hasNext()) {
                ret.addPriceEnabler(getPriceEnabler(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>PriceEnablerList</code> corresponding to the given
     *  price enabler genus <code>Type</code> which does not include
     *  price enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known price
     *  enablers or an error results. Otherwise, the returned list may
     *  contain only those price enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, price enablers are returned that are currently
     *  active. In any status mode, active and inactive price enablers
     *  are returned.
     *
     *  @param  priceEnablerGenusType a priceEnabler genus type 
     *  @return the returned <code>PriceEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>priceEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerList getPriceEnablersByGenusType(org.osid.type.Type priceEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.ordering.rules.priceenabler.FederatingPriceEnablerList ret = getPriceEnablerList();

        for (org.osid.ordering.rules.PriceEnablerLookupSession session : getSessions()) {
            ret.addPriceEnablerList(session.getPriceEnablersByGenusType(priceEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PriceEnablerList</code> corresponding to the given
     *  price enabler genus <code>Type</code> and include any additional
     *  price enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known price
     *  enablers or an error results. Otherwise, the returned list may
     *  contain only those price enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, price enablers are returned that are currently
     *  active. In any status mode, active and inactive price enablers
     *  are returned.
     *
     *  @param  priceEnablerGenusType a priceEnabler genus type 
     *  @return the returned <code>PriceEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>priceEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerList getPriceEnablersByParentGenusType(org.osid.type.Type priceEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.ordering.rules.priceenabler.FederatingPriceEnablerList ret = getPriceEnablerList();

        for (org.osid.ordering.rules.PriceEnablerLookupSession session : getSessions()) {
            ret.addPriceEnablerList(session.getPriceEnablersByParentGenusType(priceEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PriceEnablerList</code> containing the given
     *  price enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known price
     *  enablers or an error results. Otherwise, the returned list may
     *  contain only those price enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, price enablers are returned that are currently
     *  active. In any status mode, active and inactive price enablers
     *  are returned.
     *
     *  @param  priceEnablerRecordType a priceEnabler record type 
     *  @return the returned <code>PriceEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>priceEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerList getPriceEnablersByRecordType(org.osid.type.Type priceEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.ordering.rules.priceenabler.FederatingPriceEnablerList ret = getPriceEnablerList();

        for (org.osid.ordering.rules.PriceEnablerLookupSession session : getSessions()) {
            ret.addPriceEnablerList(session.getPriceEnablersByRecordType(priceEnablerRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PriceEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *  
     *  In plenary mode, the returned list contains all known price
     *  enablers or an error results. Otherwise, the returned list may
     *  contain only those price enablers that are accessible through
     *  this session.
     *  
     *  In active mode, price enablers are returned that are currently
     *  active. In any status mode, active and inactive price enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>PriceEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerList getPriceEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.ordering.rules.priceenabler.FederatingPriceEnablerList ret = getPriceEnablerList();

        for (org.osid.ordering.rules.PriceEnablerLookupSession session : getSessions()) {
            ret.addPriceEnablerList(session.getPriceEnablersOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a <code>PriceEnablerList </code> which are effective for
     *  the entire given date range inclusive but not confined to the
     *  date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known price
     *  enablers or an error results. Otherwise, the returned list may
     *  contain only those price enablers that are accessible through
     *  this session.
     *
     *  In active mode, price enablers are returned that are currently
     *  active. In any status mode, active and inactive price enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>PriceEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerList getPriceEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.ordering.rules.priceenabler.FederatingPriceEnablerList ret = getPriceEnablerList();

        for (org.osid.ordering.rules.PriceEnablerLookupSession session : getSessions()) {
            ret.addPriceEnablerList(session.getPriceEnablersOnDateWithAgent(agentId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>PriceEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known price
     *  enablers or an error results. Otherwise, the returned list may
     *  contain only those price enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, price enablers are returned that are currently
     *  active. In any status mode, active and inactive price enablers
     *  are returned.
     *
     *  @return a list of <code>PriceEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerList getPriceEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.ordering.rules.priceenabler.FederatingPriceEnablerList ret = getPriceEnablerList();

        for (org.osid.ordering.rules.PriceEnablerLookupSession session : getSessions()) {
            ret.addPriceEnablerList(session.getPriceEnablers());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.ordering.rules.priceenabler.FederatingPriceEnablerList getPriceEnablerList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.ordering.rules.priceenabler.ParallelPriceEnablerList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.ordering.rules.priceenabler.CompositePriceEnablerList());
        }
    }
}
