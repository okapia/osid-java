//
// AbstractAdapterJobLookupSession.java
//
//    A Job lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.resourcing.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Job lookup session adapter.
 */

public abstract class AbstractAdapterJobLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.resourcing.JobLookupSession {

    private final org.osid.resourcing.JobLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterJobLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterJobLookupSession(org.osid.resourcing.JobLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Foundry/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Foundry Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFoundryId() {
        return (this.session.getFoundryId());
    }


    /**
     *  Gets the {@code Foundry} associated with this session.
     *
     *  @return the {@code Foundry} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Foundry getFoundry()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getFoundry());
    }


    /**
     *  Tests if this user can perform {@code Job} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupJobs() {
        return (this.session.canLookupJobs());
    }


    /**
     *  A complete view of the {@code Job} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeJobView() {
        this.session.useComparativeJobView();
        return;
    }


    /**
     *  A complete view of the {@code Job} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryJobView() {
        this.session.usePlenaryJobView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include jobs in foundries which are children
     *  of this foundry in the foundry hierarchy.
     */

    @OSID @Override
    public void useFederatedFoundryView() {
        this.session.useFederatedFoundryView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this foundry only.
     */

    @OSID @Override
    public void useIsolatedFoundryView() {
        this.session.useIsolatedFoundryView();
        return;
    }
    

    /**
     *  Only active jobs are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveJobView() {
        this.session.useActiveJobView();
        return;
    }


    /**
     *  Active and inactive jobs are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusJobView() {
        this.session.useAnyStatusJobView();
        return;
    }
    
     
    /**
     *  Gets the {@code Job} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Job} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Job} and
     *  retained for compatibility.
     *
     *  In active mode, jobs are returned that are currently
     *  active. In any status mode, active and inactive jobs
     *  are returned.
     *
     *  @param jobId {@code Id} of the {@code Job}
     *  @return the job
     *  @throws org.osid.NotFoundException {@code jobId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code jobId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Job getJob(org.osid.id.Id jobId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJob(jobId));
    }


    /**
     *  Gets a {@code JobList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  jobs specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Jobs} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, jobs are returned that are currently
     *  active. In any status mode, active and inactive jobs
     *  are returned.
     *
     *  @param  jobIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Job} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code jobIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.JobList getJobsByIds(org.osid.id.IdList jobIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJobsByIds(jobIds));
    }


    /**
     *  Gets a {@code JobList} corresponding to the given
     *  job genus {@code Type} which does not include
     *  jobs of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  jobs or an error results. Otherwise, the returned list
     *  may contain only those jobs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, jobs are returned that are currently
     *  active. In any status mode, active and inactive jobs
     *  are returned.
     *
     *  @param  jobGenusType a job genus type 
     *  @return the returned {@code Job} list
     *  @throws org.osid.NullArgumentException
     *          {@code jobGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.JobList getJobsByGenusType(org.osid.type.Type jobGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJobsByGenusType(jobGenusType));
    }


    /**
     *  Gets a {@code JobList} corresponding to the given
     *  job genus {@code Type} and include any additional
     *  jobs with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  jobs or an error results. Otherwise, the returned list
     *  may contain only those jobs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, jobs are returned that are currently
     *  active. In any status mode, active and inactive jobs
     *  are returned.
     *
     *  @param  jobGenusType a job genus type 
     *  @return the returned {@code Job} list
     *  @throws org.osid.NullArgumentException
     *          {@code jobGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.JobList getJobsByParentGenusType(org.osid.type.Type jobGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJobsByParentGenusType(jobGenusType));
    }


    /**
     *  Gets a {@code JobList} containing the given
     *  job record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  jobs or an error results. Otherwise, the returned list
     *  may contain only those jobs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, jobs are returned that are currently
     *  active. In any status mode, active and inactive jobs
     *  are returned.
     *
     *  @param  jobRecordType a job record type 
     *  @return the returned {@code Job} list
     *  @throws org.osid.NullArgumentException
     *          {@code jobRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.JobList getJobsByRecordType(org.osid.type.Type jobRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJobsByRecordType(jobRecordType));
    }


    /**
     *  Gets a {@code JobList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  jobs or an error results. Otherwise, the returned list
     *  may contain only those jobs that are accessible through
     *  this session.
     *
     *  In active mode, jobs are returned that are currently
     *  active. In any status mode, active and inactive jobs
     *  are returned.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Job} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resourcing.JobList getJobsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJobsByProvider(resourceId));
    }


    /**
     *  Gets all {@code Jobs}. 
     *
     *  In plenary mode, the returned list contains all known
     *  jobs or an error results. Otherwise, the returned list
     *  may contain only those jobs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, jobs are returned that are currently
     *  active. In any status mode, active and inactive jobs
     *  are returned.
     *
     *  @return a list of {@code Jobs} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.JobList getJobs()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJobs());
    }
}
