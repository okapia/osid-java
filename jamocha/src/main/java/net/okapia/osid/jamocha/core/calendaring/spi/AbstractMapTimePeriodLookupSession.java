//
// AbstractMapTimePeriodLookupSession
//
//    A simple framework for providing a TimePeriod lookup service
//    backed by a fixed collection of time periods.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a TimePeriod lookup service backed by a
 *  fixed collection of time periods. The time periods are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>TimePeriods</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapTimePeriodLookupSession
    extends net.okapia.osid.jamocha.calendaring.spi.AbstractTimePeriodLookupSession
    implements org.osid.calendaring.TimePeriodLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.calendaring.TimePeriod> timePeriods = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.calendaring.TimePeriod>());


    /**
     *  Makes a <code>TimePeriod</code> available in this session.
     *
     *  @param  timePeriod a time period
     *  @throws org.osid.NullArgumentException <code>timePeriod<code>
     *          is <code>null</code>
     */

    protected void putTimePeriod(org.osid.calendaring.TimePeriod timePeriod) {
        this.timePeriods.put(timePeriod.getId(), timePeriod);
        return;
    }


    /**
     *  Makes an array of time periods available in this session.
     *
     *  @param  timePeriods an array of time periods
     *  @throws org.osid.NullArgumentException <code>timePeriods<code>
     *          is <code>null</code>
     */

    protected void putTimePeriods(org.osid.calendaring.TimePeriod[] timePeriods) {
        putTimePeriods(java.util.Arrays.asList(timePeriods));
        return;
    }


    /**
     *  Makes a collection of time periods available in this session.
     *
     *  @param  timePeriods a collection of time periods
     *  @throws org.osid.NullArgumentException <code>timePeriods<code>
     *          is <code>null</code>
     */

    protected void putTimePeriods(java.util.Collection<? extends org.osid.calendaring.TimePeriod> timePeriods) {
        for (org.osid.calendaring.TimePeriod timePeriod : timePeriods) {
            this.timePeriods.put(timePeriod.getId(), timePeriod);
        }

        return;
    }


    /**
     *  Removes a TimePeriod from this session.
     *
     *  @param  timePeriodId the <code>Id</code> of the time period
     *  @throws org.osid.NullArgumentException <code>timePeriodId<code> is
     *          <code>null</code>
     */

    protected void removeTimePeriod(org.osid.id.Id timePeriodId) {
        this.timePeriods.remove(timePeriodId);
        return;
    }


    /**
     *  Gets the <code>TimePeriod</code> specified by its <code>Id</code>.
     *
     *  @param  timePeriodId <code>Id</code> of the <code>TimePeriod</code>
     *  @return the timePeriod
     *  @throws org.osid.NotFoundException <code>timePeriodId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>timePeriodId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriod getTimePeriod(org.osid.id.Id timePeriodId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.calendaring.TimePeriod timePeriod = this.timePeriods.get(timePeriodId);
        if (timePeriod == null) {
            throw new org.osid.NotFoundException("timePeriod not found: " + timePeriodId);
        }

        return (timePeriod);
    }


    /**
     *  Gets all <code>TimePeriods</code>. In plenary mode, the returned
     *  list contains all known timePeriods or an error
     *  results. Otherwise, the returned list may contain only those
     *  timePeriods that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>TimePeriods</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodList getTimePeriods()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.timeperiod.ArrayTimePeriodList(this.timePeriods.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.timePeriods.clear();
        super.close();
        return;
    }
}
