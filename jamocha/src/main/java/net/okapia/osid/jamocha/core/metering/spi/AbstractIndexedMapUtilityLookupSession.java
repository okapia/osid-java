//
// AbstractIndexedMapUtilityLookupSession.java
//
//    A simple framework for providing an Utility lookup service
//    backed by a fixed collection of utilities with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.metering.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an Utility lookup service backed by a
 *  fixed collection of utilities. The utilities are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some utilities may be compatible
 *  with more types than are indicated through these utility
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Utilities</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapUtilityLookupSession
    extends AbstractMapUtilityLookupSession
    implements org.osid.metering.UtilityLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.metering.Utility> utilitiesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.metering.Utility>());
    private final MultiMap<org.osid.type.Type, org.osid.metering.Utility> utilitiesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.metering.Utility>());


    /**
     *  Makes an <code>Utility</code> available in this session.
     *
     *  @param  utility an utility
     *  @throws org.osid.NullArgumentException <code>utility<code> is
     *          <code>null</code>
     */

    @Override
    protected void putUtility(org.osid.metering.Utility utility) {
        super.putUtility(utility);

        this.utilitiesByGenus.put(utility.getGenusType(), utility);
        
        try (org.osid.type.TypeList types = utility.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.utilitiesByRecord.put(types.getNextType(), utility);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an utility from this session.
     *
     *  @param utilityId the <code>Id</code> of the utility
     *  @throws org.osid.NullArgumentException <code>utilityId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeUtility(org.osid.id.Id utilityId) {
        org.osid.metering.Utility utility;
        try {
            utility = getUtility(utilityId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.utilitiesByGenus.remove(utility.getGenusType());

        try (org.osid.type.TypeList types = utility.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.utilitiesByRecord.remove(types.getNextType(), utility);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeUtility(utilityId);
        return;
    }


    /**
     *  Gets an <code>UtilityList</code> corresponding to the given
     *  utility genus <code>Type</code> which does not include
     *  utilities of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known utilities or an error results. Otherwise,
     *  the returned list may contain only those utilities that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  utilityGenusType an utility genus type 
     *  @return the returned <code>Utility</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>utilityGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.UtilityList getUtilitiesByGenusType(org.osid.type.Type utilityGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.metering.utility.ArrayUtilityList(this.utilitiesByGenus.get(utilityGenusType)));
    }


    /**
     *  Gets an <code>UtilityList</code> containing the given
     *  utility record <code>Type</code>. In plenary mode, the
     *  returned list contains all known utilities or an error
     *  results. Otherwise, the returned list may contain only those
     *  utilities that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  utilityRecordType an utility record type 
     *  @return the returned <code>utility</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>utilityRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.UtilityList getUtilitiesByRecordType(org.osid.type.Type utilityRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.metering.utility.ArrayUtilityList(this.utilitiesByRecord.get(utilityRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.utilitiesByGenus.clear();
        this.utilitiesByRecord.clear();

        super.close();

        return;
    }
}
