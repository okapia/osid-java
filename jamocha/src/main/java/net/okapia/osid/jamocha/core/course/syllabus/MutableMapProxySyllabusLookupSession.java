//
// MutableMapProxySyllabusLookupSession
//
//    Implements a Syllabus lookup service backed by a collection of
//    syllabi that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.syllabus;


/**
 *  Implements a Syllabus lookup service backed by a collection of
 *  syllabi. The syllabi are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of syllabi can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxySyllabusLookupSession
    extends net.okapia.osid.jamocha.core.course.syllabus.spi.AbstractMapSyllabusLookupSession
    implements org.osid.course.syllabus.SyllabusLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxySyllabusLookupSession}
     *  with no syllabi.
     *
     *  @param courseCatalog the course catalog
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxySyllabusLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.proxy.Proxy proxy) {
        setCourseCatalog(courseCatalog);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxySyllabusLookupSession} with a
     *  single syllabus.
     *
     *  @param courseCatalog the course catalog
     *  @param syllabus a syllabus
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code syllabus}, or {@code proxy} is {@code null}
     */

    public MutableMapProxySyllabusLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                org.osid.course.syllabus.Syllabus syllabus, org.osid.proxy.Proxy proxy) {
        this(courseCatalog, proxy);
        putSyllabus(syllabus);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxySyllabusLookupSession} using an
     *  array of syllabi.
     *
     *  @param courseCatalog the course catalog
     *  @param syllabi an array of syllabi
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code syllabi}, or {@code proxy} is {@code null}
     */

    public MutableMapProxySyllabusLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                org.osid.course.syllabus.Syllabus[] syllabi, org.osid.proxy.Proxy proxy) {
        this(courseCatalog, proxy);
        putSyllabi(syllabi);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxySyllabusLookupSession} using a
     *  collection of syllabi.
     *
     *  @param courseCatalog the course catalog
     *  @param syllabi a collection of syllabi
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code syllabi}, or {@code proxy} is {@code null}
     */

    public MutableMapProxySyllabusLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                java.util.Collection<? extends org.osid.course.syllabus.Syllabus> syllabi,
                                                org.osid.proxy.Proxy proxy) {
   
        this(courseCatalog, proxy);
        setSessionProxy(proxy);
        putSyllabi(syllabi);
        return;
    }

    
    /**
     *  Makes a {@code Syllabus} available in this session.
     *
     *  @param syllabus an syllabus
     *  @throws org.osid.NullArgumentException {@code syllabus{@code 
     *          is {@code null}
     */

    @Override
    public void putSyllabus(org.osid.course.syllabus.Syllabus syllabus) {
        super.putSyllabus(syllabus);
        return;
    }


    /**
     *  Makes an array of syllabi available in this session.
     *
     *  @param syllabi an array of syllabi
     *  @throws org.osid.NullArgumentException {@code syllabi{@code 
     *          is {@code null}
     */

    @Override
    public void putSyllabi(org.osid.course.syllabus.Syllabus[] syllabi) {
        super.putSyllabi(syllabi);
        return;
    }


    /**
     *  Makes collection of syllabi available in this session.
     *
     *  @param syllabi
     *  @throws org.osid.NullArgumentException {@code syllabus{@code 
     *          is {@code null}
     */

    @Override
    public void putSyllabi(java.util.Collection<? extends org.osid.course.syllabus.Syllabus> syllabi) {
        super.putSyllabi(syllabi);
        return;
    }


    /**
     *  Removes a Syllabus from this session.
     *
     *  @param syllabusId the {@code Id} of the syllabus
     *  @throws org.osid.NullArgumentException {@code syllabusId{@code  is
     *          {@code null}
     */

    @Override
    public void removeSyllabus(org.osid.id.Id syllabusId) {
        super.removeSyllabus(syllabusId);
        return;
    }    
}
