//
// TemporalOfferingConstrainerEnablerFilterList.java
//
//     Implements a filter for Temporal objects.
//
//
// Tom Coppeto
// Okapia
// 17 December 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.offering.rules.offeringconstrainerenabler;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for Temporal objects.
 */

public final class TemporalOfferingConstrainerEnablerFilterList
    extends net.okapia.osid.jamocha.inline.filter.offering.rules.offeringconstrainerenabler.spi.AbstractOfferingConstrainerEnablerFilterList
    implements org.osid.offering.rules.OfferingConstrainerEnablerList,
               OfferingConstrainerEnablerFilter {

    private final org.osid.calendaring.DateTime start;
    private final org.osid.calendaring.DateTime end;


    /**
     *  Creates a new
     *  <code>TemporalOfferingConstrainerEnablerFilterList</code>. Temporals pass the
     *  filter if the given date range falls inside the start and end
     *  dates inclusive.
     *
     *  @param list an <code>OfferingConstrainerEnablerList</code>
     *  @param from start of date range
     *  @param to end of date range
     *  @throws org.osid.NullArgumentException <code>list</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     */

    public TemporalOfferingConstrainerEnablerFilterList(org.osid.offering.rules.OfferingConstrainerEnablerList list, 
                                      org.osid.calendaring.DateTime from,
                                      org.osid.calendaring.DateTime to) {
        super(list);

        nullarg(from, "start date");
        nullarg(to, "end date");

        this.start = from;
        this.end = to;

        return;
    }    

    
    /**
     *  Filters OfferingConstrainerEnablers.
     *
     *  @param offeringConstrainerEnabler the offering constrainer enabler to filter
     *  @return <code>true</code> if the offering constrainer enabler passes the filter,
     *          <code>false</code> if the offering constrainer enabler should be filtered
     */

    @Override
    public boolean pass(org.osid.offering.rules.OfferingConstrainerEnabler offeringConstrainerEnabler) {
        if (offeringConstrainerEnabler.getStartDate().isGreater(this.start)) {
            return (false);
        }

        if (offeringConstrainerEnabler.getEndDate().isLess(this.end)) {
            return (false);
        }

        return (true);
    }
}
