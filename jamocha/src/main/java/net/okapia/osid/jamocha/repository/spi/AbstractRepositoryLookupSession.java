//
// AbstractRepositoryLookupSession.java
//
//    A starter implementation framework for providing a Repository
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.repository.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A starter implementation framework for providing a Repository
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getRepositories(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractRepositoryLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.repository.RepositoryLookupSession {

    private boolean pedantic = false;
    private org.osid.repository.Repository repository = new net.okapia.osid.jamocha.nil.repository.repository.UnknownRepository();
    

    /**
     *  Tests if this user can perform <code>Repository</code>
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRepositories() {
        return (true);
    }


    /**
     *  A complete view of the <code>Repository</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRepositoryView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Repository</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRepositoryView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }

     
    /**
     *  Gets the <code>Repository</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Repository</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Repository</code> and
     *  retained for compatibility.
     *
     *  @param  repositoryId <code>Id</code> of the
     *          <code>Repository</code>
     *  @return the repository
     *  @throws org.osid.NotFoundException <code>repositoryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>repositoryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.Repository getRepository(org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.repository.RepositoryList repositories = getRepositories()) {
            while (repositories.hasNext()) {
                org.osid.repository.Repository repository = repositories.getNextRepository();
                if (repository.getId().equals(repositoryId)) {
                    return (repository);
                }
            }
        } 

        throw new org.osid.NotFoundException(repositoryId + " not found");
    }


    /**
     *  Gets a <code>RepositoryList</code> corresponding to the given
     *  <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  repositories specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>Repositories</code> may be omitted from the list and may
     *  present the elements in any order including returning a unique
     *  set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getRepositories()</code>.
     *
     *  @param  repositoryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Repository</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>repositoryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.RepositoryList getRepositoriesByIds(org.osid.id.IdList repositoryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.repository.Repository> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = repositoryIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getRepository(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("repository " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.repository.repository.LinkedRepositoryList(ret));
    }


    /**
     *  Gets a <code>RepositoryList</code> corresponding to the given
     *  repository genus <code>Type</code> which does not include
     *  repositories of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  repositories or an error results. Otherwise, the returned list
     *  may contain only those repositories that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getRepositories()</code>.
     *
     *  @param  repositoryGenusType a repository genus type 
     *  @return the returned <code>Repository</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>repositoryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.RepositoryList getRepositoriesByGenusType(org.osid.type.Type repositoryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.repository.repository.RepositoryGenusFilterList(getRepositories(), repositoryGenusType));
    }


    /**
     *  Gets a <code>RepositoryList</code> corresponding to the given
     *  repository genus <code>Type</code> and include any additional
     *  repositories with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  repositories or an error results. Otherwise, the returned list
     *  may contain only those repositories that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getRepositories()</code>.
     *
     *  @param  repositoryGenusType a repository genus type 
     *  @return the returned <code>Repository</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>repositoryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.RepositoryList getRepositoriesByParentGenusType(org.osid.type.Type repositoryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getRepositoriesByGenusType(repositoryGenusType));
    }


    /**
     *  Gets a <code>RepositoryList</code> containing the given
     *  repository record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known
     *  repositories or an error results. Otherwise, the returned list
     *  may contain only those repositories that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getRepositories()</code>.
     *
     *  @param  repositoryRecordType a repository record type 
     *  @return the returned <code>Repository</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>repositoryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.RepositoryList getRepositoriesByRecordType(org.osid.type.Type repositoryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.repository.repository.RepositoryRecordFilterList(getRepositories(), repositoryRecordType));
    }


    /**
     *  Gets a <code>RepositoryList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known repositories or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  repositories that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Repository</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.repository.RepositoryList getRepositoriesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.repository.repository.RepositoryProviderFilterList(getRepositories(), resourceId));
    }


    /**
     *  Gets all <code>Repositories</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  repositories or an error results. Otherwise, the returned list
     *  may contain only those repositories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Repositories</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.repository.RepositoryList getRepositories()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the repository list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of repositories
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.repository.RepositoryList filterRepositoriesOnViews(org.osid.repository.RepositoryList list)
        throws org.osid.OperationFailedException {

        return (list);
    }
}
