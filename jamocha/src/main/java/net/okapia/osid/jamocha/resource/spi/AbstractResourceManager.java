//
// AbstractResourceManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resource.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractResourceManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.resource.ResourceManager,
               org.osid.resource.ResourceProxyManager {

    private final Types resourceRecordTypes                = new TypeRefSet();
    private final Types resourceSearchRecordTypes          = new TypeRefSet();

    private final Types resourceRelationshipRecordTypes    = new TypeRefSet();
    private final Types resourceRelationshipSearchRecordTypes= new TypeRefSet();

    private final Types binRecordTypes                     = new TypeRefSet();
    private final Types binSearchRecordTypes               = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractResourceManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractResourceManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if resource lookup is supported. 
     *
     *  @return <code> true </code> if resource lookup is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceLookup() {
        return (false);
    }


    /**
     *  Tests if resource query is supported. 
     *
     *  @return <code> true </code> if resource query is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceQuery() {
        return (false);
    }


    /**
     *  Tests if resource search is supported. 
     *
     *  @return <code> true </code> if resource search is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceSearch() {
        return (false);
    }


    /**
     *  Tests if resource administration is supported. 
     *
     *  @return <code> true </code> if resource administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceAdmin() {
        return (false);
    }


    /**
     *  Tests if resource notification is supported. Messages may be sent when 
     *  resources are created, modified, or deleted. 
     *
     *  @return <code> true </code> if resource notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceNotification() {
        return (false);
    }


    /**
     *  Tests if rerieving mappings of resource and bins is supported. 
     *
     *  @return <code> true </code> if resource bin mapping retrieval is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceBin() {
        return (false);
    }


    /**
     *  Tests if managing mappings of resource and bins is supported. 
     *
     *  @return <code> true </code> if resource bin assignment is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceBinAssignment() {
        return (false);
    }


    /**
     *  Tests if resource smart bins are available. 
     *
     *  @return <code> true </code> if resource smart bins are supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceSmartBin() {
        return (false);
    }


    /**
     *  Tests if membership queries are supported. 
     *
     *  @return <code> true </code> if membership queries are supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMembership() {
        return (false);
    }


    /**
     *  Tests if group resources are supported. 
     *
     *  @return <code> true </code> if group resources are supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGroup() {
        return (false);
    }


    /**
     *  Tests if group resource assignment is supported. 
     *
     *  @return <code> true </code> if group resource assignment is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGroupAssignment() {
        return (false);
    }


    /**
     *  Tests if group resource notification is supported. 
     *
     *  @return <code> true </code> if group resource notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGroupNotification() {
        return (false);
    }


    /**
     *  Tests if a group resource hierarchy service is supported. 
     *
     *  @return <code> true </code> if group resource hierarchy is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGroupHierarchy() {
        return (false);
    }


    /**
     *  Tests if rerieving mappings of resource and agents is supported. 
     *
     *  @return <code> true </code> if resource agent mapping retrieval is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceAgent() {
        return (false);
    }


    /**
     *  Tests if managing mappings of resources and agents is supported. 
     *
     *  @return <code> true </code> if resource agent assignment is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceAgentAssignment() {
        return (false);
    }


    /**
     *  Tests if looking up resource relationships is supported. 
     *
     *  @return <code> true </code> if resource relationships lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceRelationshipLookup() {
        return (false);
    }


    /**
     *  Tests if querying resource relationships is supported. 
     *
     *  @return <code> true </code> if resource relationships query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceRelationshipQuery() {
        return (false);
    }


    /**
     *  Tests if searching resource relationships is supported. 
     *
     *  @return <code> true </code> if resource relationships search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceRelationshipSearch() {
        return (false);
    }


    /**
     *  Tests if a resource relationships <code> </code> administrative 
     *  service is supported. 
     *
     *  @return <code> true </code> if resource relationships administration 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceRelationshipAdmin() {
        return (false);
    }


    /**
     *  Tests if a resource relationships <code> </code> notification service 
     *  is supported. 
     *
     *  @return <code> true </code> if resource relationships notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceRelationshipNotification() {
        return (false);
    }


    /**
     *  Tests if rerieving mappings of resource relationships and bins is 
     *  supported. 
     *
     *  @return <code> true </code> if resource relationship bin mapping 
     *          retrieval is supported <code> , </code> <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsResourceRelationshipBin() {
        return (false);
    }


    /**
     *  Tests if managing mappings of resource relationships and bins is 
     *  supported. 
     *
     *  @return <code> true </code> if resource relationship bin assignment is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceRelationshipBinAssignment() {
        return (false);
    }


    /**
     *  Tests if resource relationship smart bins are available. 
     *
     *  @return <code> true </code> if resource relationship smart bins are 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceRelationshipSmartBin() {
        return (false);
    }


    /**
     *  Tests if bin lookup is supported. 
     *
     *  @return <code> true </code> if bin lookup is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBinLookup() {
        return (false);
    }


    /**
     *  Tests if bin query is supported. 
     *
     *  @return <code> true </code> if bin query is supported <code> , </code> 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBinQuery() {
        return (false);
    }


    /**
     *  Tests if bin search is supported. 
     *
     *  @return <code> true </code> if bin search is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBinSearch() {
        return (false);
    }


    /**
     *  Tests if bin administration is supported. 
     *
     *  @return <code> true </code> if bin administration is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBinAdmin() {
        return (false);
    }


    /**
     *  Tests if bin notification is supported. Messages may be sent when 
     *  <code> Bin </code> objects are created, deleted or updated. 
     *  Notifications for resources within bins are sent via the resource 
     *  notification session. 
     *
     *  @return <code> true </code> if bin notification is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBinNotification() {
        return (false);
    }


    /**
     *  Tests if a bin hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if a bin hierarchy traversal is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBinHierarchy() {
        return (false);
    }


    /**
     *  Tests if a bin hierarchy design is supported. 
     *
     *  @return <code> true </code> if a bin hierarchy design is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBinHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests if a resource batch service is available. 
     *
     *  @return <code> true </code> if a resource batch service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceBatch() {
        return (false);
    }


    /**
     *  Tests if a resource demographic service is available. 
     *
     *  @return <code> true </code> if a resource demographic service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceDemographic() {
        return (false);
    }


    /**
     *  Gets all the resource record types supported. 
     *
     *  @return the list of supported resource record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getResourceRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.resourceRecordTypes.toCollection()));
    }


    /**
     *  Tests if a given resource record type is supported. 
     *
     *  @param  resourceRecordType the resource type 
     *  @return <code> true </code> if the resource record type is supported 
     *          <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> resourceRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsResourceRecordType(org.osid.type.Type resourceRecordType) {
        return (this.resourceRecordTypes.contains(resourceRecordType));
    }


    /**
     *  Adds support for a resource record type.
     *
     *  @param resourceRecordType a resource record type
     *  @throws org.osid.NullArgumentException
     *  <code>resourceRecordType</code> is <code>null</code>
     */

    protected void addResourceRecordType(org.osid.type.Type resourceRecordType) {
        this.resourceRecordTypes.add(resourceRecordType);
        return;
    }


    /**
     *  Removes support for a resource record type.
     *
     *  @param resourceRecordType a resource record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>resourceRecordType</code> is <code>null</code>
     */

    protected void removeResourceRecordType(org.osid.type.Type resourceRecordType) {
        this.resourceRecordTypes.remove(resourceRecordType);
        return;
    }


    /**
     *  Gets all the resource search record types supported. 
     *
     *  @return the list of supported resource search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getResourceSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.resourceSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if a given resource search type is supported. 
     *
     *  @param  resourceSearchRecordType the resource search type 
     *  @return <code> true </code> if the resource search record type is 
     *          supported <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> resourceSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsResourceSearchRecordType(org.osid.type.Type resourceSearchRecordType) {
        return (this.resourceSearchRecordTypes.contains(resourceSearchRecordType));
    }


    /**
     *  Adds support for a resource search record type.
     *
     *  @param resourceSearchRecordType a resource search record type
     *  @throws org.osid.NullArgumentException
     *  <code>resourceSearchRecordType</code> is <code>null</code>
     */

    protected void addResourceSearchRecordType(org.osid.type.Type resourceSearchRecordType) {
        this.resourceSearchRecordTypes.add(resourceSearchRecordType);
        return;
    }


    /**
     *  Removes support for a resource search record type.
     *
     *  @param resourceSearchRecordType a resource search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>resourceSearchRecordType</code> is <code>null</code>
     */

    protected void removeResourceSearchRecordType(org.osid.type.Type resourceSearchRecordType) {
        this.resourceSearchRecordTypes.remove(resourceSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> ResourceRelationship </code> record types. 
     *
     *  @return a list containing the supported <code> ResourceRelationship 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getResourceRelationshipRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.resourceRelationshipRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> ResourceRelationship </code> record type is 
     *  supported. 
     *
     *  @param  resourceRelationshipRecordType a <code> Type </code> 
     *          indicating a <code> ResourceRelationship </code> record type 
     *  @return <code> true </code> if the given type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          resourceRelationshipRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsResourceRelationshipRecordType(org.osid.type.Type resourceRelationshipRecordType) {
        return (this.resourceRelationshipRecordTypes.contains(resourceRelationshipRecordType));
    }


    /**
     *  Adds support for a resource relationship record type.
     *
     *  @param resourceRelationshipRecordType a resource relationship record type
     *  @throws org.osid.NullArgumentException
     *  <code>resourceRelationshipRecordType</code> is <code>null</code>
     */

    protected void addResourceRelationshipRecordType(org.osid.type.Type resourceRelationshipRecordType) {
        this.resourceRelationshipRecordTypes.add(resourceRelationshipRecordType);
        return;
    }


    /**
     *  Removes support for a resource relationship record type.
     *
     *  @param resourceRelationshipRecordType a resource relationship record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>resourceRelationshipRecordType</code> is <code>null</code>
     */

    protected void removeResourceRelationshipRecordType(org.osid.type.Type resourceRelationshipRecordType) {
        this.resourceRelationshipRecordTypes.remove(resourceRelationshipRecordType);
        return;
    }


    /**
     *  Gets the supported <code> ResourceRelationship </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> ResourceRelationship 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getResourceRelationshipSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.resourceRelationshipSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> ResourceRelationship </code> search record 
     *  type is supported. 
     *
     *  @param  resourceRelationshipSearchRecordType a <code> Type </code> 
     *          indicating a <code> ResourceRelationship </code> search record 
     *          type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          resourceRelationshipSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsResourceRelationshipSearchRecordType(org.osid.type.Type resourceRelationshipSearchRecordType) {
        return (this.resourceRelationshipSearchRecordTypes.contains(resourceRelationshipSearchRecordType));
    }


    /**
     *  Adds support for a resource relationship search record type.
     *
     *  @param resourceRelationshipSearchRecordType a resource relationship search record type
     *  @throws org.osid.NullArgumentException
     *  <code>resourceRelationshipSearchRecordType</code> is <code>null</code>
     */

    protected void addResourceRelationshipSearchRecordType(org.osid.type.Type resourceRelationshipSearchRecordType) {
        this.resourceRelationshipSearchRecordTypes.add(resourceRelationshipSearchRecordType);
        return;
    }


    /**
     *  Removes support for a resource relationship search record type.
     *
     *  @param resourceRelationshipSearchRecordType a resource relationship search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>resourceRelationshipSearchRecordType</code> is <code>null</code>
     */

    protected void removeResourceRelationshipSearchRecordType(org.osid.type.Type resourceRelationshipSearchRecordType) {
        this.resourceRelationshipSearchRecordTypes.remove(resourceRelationshipSearchRecordType);
        return;
    }


    /**
     *  Gets all the bin record types supported. 
     *
     *  @return the list of supported bin record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBinRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.binRecordTypes.toCollection()));
    }


    /**
     *  Tests if a given bin record type is supported. 
     *
     *  @param  binRecordType the bin record type 
     *  @return <code> true </code> if the bin record type is supported <code> 
     *          , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> binRecordType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBinRecordType(org.osid.type.Type binRecordType) {
        return (this.binRecordTypes.contains(binRecordType));
    }


    /**
     *  Adds support for a bin record type.
     *
     *  @param binRecordType a bin record type
     *  @throws org.osid.NullArgumentException
     *  <code>binRecordType</code> is <code>null</code>
     */

    protected void addBinRecordType(org.osid.type.Type binRecordType) {
        this.binRecordTypes.add(binRecordType);
        return;
    }


    /**
     *  Removes support for a bin record type.
     *
     *  @param binRecordType a bin record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>binRecordType</code> is <code>null</code>
     */

    protected void removeBinRecordType(org.osid.type.Type binRecordType) {
        this.binRecordTypes.remove(binRecordType);
        return;
    }


    /**
     *  Gets all the bin search record types supported. 
     *
     *  @return the list of supported bin search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBinSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.binSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if a given bin search record type is supported. 
     *
     *  @param  binSearchRecordType the bin search record type 
     *  @return <code> true </code> if the bin search record type is supported 
     *          <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> binSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBinSearchRecordType(org.osid.type.Type binSearchRecordType) {
        return (this.binSearchRecordTypes.contains(binSearchRecordType));
    }


    /**
     *  Adds support for a bin search record type.
     *
     *  @param binSearchRecordType a bin search record type
     *  @throws org.osid.NullArgumentException
     *  <code>binSearchRecordType</code> is <code>null</code>
     */

    protected void addBinSearchRecordType(org.osid.type.Type binSearchRecordType) {
        this.binSearchRecordTypes.add(binSearchRecordType);
        return;
    }


    /**
     *  Removes support for a bin search record type.
     *
     *  @param binSearchRecordType a bin search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>binSearchRecordType</code> is <code>null</code>
     */

    protected void removeBinSearchRecordType(org.osid.type.Type binSearchRecordType) {
        this.binSearchRecordTypes.remove(binSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  lookup service. 
     *
     *  @return <code> a ResourceLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceLookupSession getResourceLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getResourceLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a ResourceLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceLookupSession getResourceLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getResourceLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  lookup service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the bin 
     *  @return <code> a ResourceLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> binId </code> not found 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceLookupSession getResourceLookupSessionForBin(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getResourceLookupSessionForBin not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  lookup service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the bin 
     *  @param  proxy <code> a proxy </code> 
     *  @return <code> a ResourceLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> binId </code> not found 
     *  @throws org.osid.NullArgumentException <code> binId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceLookupSession getResourceLookupSessionForBin(org.osid.id.Id binId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getResourceLookupSessionForBin not implemented");
    }


    /**
     *  Gets a resource query session. 
     *
     *  @return <code> a ResourceQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuerySession getResourceQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getResourceQuerySession not implemented");
    }


    /**
     *  Gets a resource query session. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a ResourceQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuerySession getResourceQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getResourceQuerySession not implemented");
    }


    /**
     *  Gets a resource query session for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the bin 
     *  @return <code> a ResourceQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> binId </code> not found 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuerySession getResourceQuerySessionForBin(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getResourceQuerySessionForBin not implemented");
    }


    /**
     *  Gets a resource query session for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the bin 
     *  @param  proxy a proxy 
     *  @return <code> a ResourceQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> binId </code> not found 
     *  @throws org.osid.NullArgumentException <code> binId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuerySession getResourceQuerySessionForBin(org.osid.id.Id binId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getResourceQuerySessionForBin not implemented");
    }


    /**
     *  Gets a resource search session. 
     *
     *  @return <code> a ResourceSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchSession getResourceSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getResourceSearchSession not implemented");
    }


    /**
     *  Gets a resource search session. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a ResourceSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchSession getResourceSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getResourceSearchSession not implemented");
    }


    /**
     *  Gets a resource search session for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the bin 
     *  @return <code> a ResourceSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> binId </code> not found 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchSession getResourceSearchSessionForBin(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getResourceSearchSessionForBin not implemented");
    }


    /**
     *  Gets a resource search session for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the bin 
     *  @param  proxy a proxy 
     *  @return <code> a ResourceSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> binId </code> not found 
     *  @throws org.osid.NullArgumentException <code> binId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchSession getResourceSearchSessionForBin(org.osid.id.Id binId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getResourceSearchSessionForBin not implemented");
    }


    /**
     *  Gets a resource administration session for creating, updating and 
     *  deleting resources. 
     *
     *  @return <code> a ResourceAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResourceAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceAdminSession getResourceAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getResourceAdminSession not implemented");
    }


    /**
     *  Gets a resource administration session for creating, updating and 
     *  deleting resources. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a ResourceAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResourceAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceAdminSession getResourceAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getResourceAdminSession not implemented");
    }


    /**
     *  Gets a resource administration session for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the bin 
     *  @return <code> a ResourceAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> binId </code> not found 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResourceAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceAdminSession getResourceAdminSessionForBin(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getResourceAdminSessionForBin not implemented");
    }


    /**
     *  Gets a resource administration session for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the bin 
     *  @param  proxy a proxy 
     *  @return <code> a ResourceAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> binId </code> not found 
     *  @throws org.osid.NullArgumentException <code> binId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResourceAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceAdminSession getResourceAdminSessionForBin(org.osid.id.Id binId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getResourceAdminSessionForBin not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to resource 
     *  changes. 
     *
     *  @param  resourceReceiver the notification callback 
     *  @return <code> a ResourceNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> resourceReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceNotificationSession getResourceNotificationSession(org.osid.resource.ResourceReceiver resourceReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getResourceNotificationSession not implemented");
    }


    /**
     *  Gets the resource notification session for the given bin. 
     *
     *  @param  resourceReceiver notification callback 
     *  @param  proxy a proxy 
     *  @return <code> a ResourceNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> resourceReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceNotificationSession getResourceNotificationSession(org.osid.resource.ResourceReceiver resourceReceiver, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getResourceNotificationSession not implemented");
    }


    /**
     *  Gets the resource notification session for the given bin. 
     *
     *  @param  resourceReceiver the notification callback 
     *  @param  binId the <code> Id </code> of the bin 
     *  @return <code> a ResourceNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> binId </code> not found 
     *  @throws org.osid.NullArgumentException <code> resourceReceiver </code> 
     *          or <code> binId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceNotificationSession getResourceNotificationSessionForBin(org.osid.resource.ResourceReceiver resourceReceiver, 
                                                                                              org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getResourceNotificationSessionForBin not implemented");
    }


    /**
     *  Gets the resource notification session for the given bin. 
     *
     *  @param  resourceReceiver notification callback 
     *  @param  binId the <code> Id </code> of the bin 
     *  @param  proxy a proxy 
     *  @return <code> a ResourceNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> binId </code> not found 
     *  @throws org.osid.NullArgumentException <code> resourceReceiver, binId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceNotificationSession getResourceNotificationSessionForBin(org.osid.resource.ResourceReceiver resourceReceiver, 
                                                                                              org.osid.id.Id binId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getResourceNotificationSessionForBin not implemented");
    }


    /**
     *  Gets the session for retrieving resource to bin mappings. 
     *
     *  @return a <code> ResourceBinSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResourceBin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceBinSession getResourceBinSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getResourceBinSession not implemented");
    }


    /**
     *  Gets the session for retrieving resource to bin mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ResourceBinSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResourceBin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceBinSession getResourceBinSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getResourceBinSession not implemented");
    }


    /**
     *  Gets the session for assigning resource to bin mappings. 
     *
     *  @return a <code> ResourceBinAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceBinAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceBinAssignmentSession getResourceBinAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getResourceBinAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning resource to bin mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ResourceBinAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceBinAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceBinAssignmentSession getResourceBinAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getResourceBinAssignmentSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic resource bins. 
     *
     *  @param  binId the <code> Id </code> of the bin 
     *  @return a <code> ResourceSmartBinSession </code> 
     *  @throws org.osid.NotFoundException <code> binId </code> not found 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceSmartBin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSmartBinSession getResourceSmartBinSession(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getResourceSmartBinSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic resource bins. 
     *
     *  @param  binId the <code> Id </code> of the bin 
     *  @param  proxy a proxy 
     *  @return a <code> ResourceSmartBinSession </code> 
     *  @throws org.osid.NotFoundException <code> binId </code> not found 
     *  @throws org.osid.NullArgumentException <code> binId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceSmartBin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSmartBinSession getResourceSmartBinSession(org.osid.id.Id binId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getResourceSmartBinSession not implemented");
    }


    /**
     *  Gets the session for querying memberships. 
     *
     *  @return a <code> MembershipSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMembership() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.MembershipSession getMembershipSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getMembershipSession not implemented");
    }


    /**
     *  Gets the session for querying memberships. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MembershipSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMembership() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.MembershipSession getMembershipSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getMembershipSession not implemented");
    }


    /**
     *  Gets a resource membership session for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the bin 
     *  @return <code> a MembershipSession </code> 
     *  @throws org.osid.NotFoundException <code> binId </code> not found 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMembership() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.MembershipSession getMembershipSessionForBin(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getMembershipSessionForBin not implemented");
    }


    /**
     *  Gets a resource membership session for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the bin 
     *  @param  proxy a proxy 
     *  @return <code> a MembershipSession </code> 
     *  @throws org.osid.NotFoundException <code> binId </code> not found 
     *  @throws org.osid.NullArgumentException <code> binId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMembership() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.MembershipSession getMembershipSessionForBin(org.osid.id.Id binId, 
                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getMembershipSessionForBin not implemented");
    }


    /**
     *  Gets the session for retrieving gropup memberships. 
     *
     *  @return a <code> GroupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsGroup() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.GroupSession getGroupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getGroupSession not implemented");
    }


    /**
     *  Gets the session for retrieving gropup memberships. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GroupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsGroups() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.GroupSession getGroupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getGroupSession not implemented");
    }


    /**
     *  Gets a group session for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the bin 
     *  @return a <code> GroupSession </code> 
     *  @throws org.osid.NotFoundException <code> binId </code> not found 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsGroup() </code> 
     *          or <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.GroupSession getGroupSessionForBin(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getGroupSessionForBin not implemented");
    }


    /**
     *  Gets a group session for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the bin 
     *  @param  proxy a proxy 
     *  @return a <code> GroupSession </code> 
     *  @throws org.osid.NotFoundException <code> binId </code> not found 
     *  @throws org.osid.NullArgumentException <code> binId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsGroup() </code> 
     *          or <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.GroupSession getGroupSessionForBin(org.osid.id.Id binId, 
                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getGroupSessionForBin not implemented");
    }


    /**
     *  Gets the session for assigning resources to groups. 
     *
     *  @return a <code> GroupAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGroupAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.GroupAssignmentSession getGroupAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getGroupAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning resources to groups. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GroupAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGroupAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.GroupAssignmentSession getGroupAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getGroupAssignmentSession not implemented");
    }


    /**
     *  Gets a group assignment session for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the bin 
     *  @return a <code> GroupAssignmentSession </code> 
     *  @throws org.osid.NotFoundException <code> binId </code> not found 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGroupAssignment() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.GroupAssignmentSession getGroupAssignmentSessionForBin(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getGroupAssignmentSessionForBin not implemented");
    }


    /**
     *  Gets a group assignment session for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the bin 
     *  @param  proxy a proxy 
     *  @return a <code> GroupAssignmentSession </code> 
     *  @throws org.osid.NotFoundException <code> binId </code> not found 
     *  @throws org.osid.NullArgumentException <code> binId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGroupAssignment() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.GroupAssignmentSession getGroupAssignmentSessionForBin(org.osid.id.Id binId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getGroupAssignmentSessionForBin not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to resource 
     *  changes. 
     *
     *  @param  groupRceeiver the notification callback 
     *  @return <code> a GroupNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> groupReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGroupNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.GroupNotificationSession getGroupNotificationSession(org.osid.resource.GroupReceiver groupRceeiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getGroupNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to resource 
     *  changes. 
     *
     *  @param  groupRceeiver the notification callback 
     *  @param  proxy a proxy 
     *  @return <code> a GroupNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> groupReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGroupNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.GroupNotificationSession getGroupNotificationSession(org.osid.resource.GroupReceiver groupRceeiver, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getGroupNotificationSession not implemented");
    }


    /**
     *  Gets the group notification session for the given bin. 
     *
     *  @param  groupRceeiver the notification callback 
     *  @param  binId the <code> Id </code> of the bin 
     *  @return <code> a GroupNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> binId </code> not found 
     *  @throws org.osid.NullArgumentException <code> groupReceiver </code> or 
     *          <code> binId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGroupNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.GroupNotificationSession getGroupNotificationSessionForBin(org.osid.resource.GroupReceiver groupRceeiver, 
                                                                                        org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getGroupNotificationSessionForBin not implemented");
    }


    /**
     *  Gets the group notification session for the given bin. 
     *
     *  @param  groupRceeiver the notification callback 
     *  @param  binId the <code> Id </code> of the bin 
     *  @param  proxy a proxy 
     *  @return <code> a GroupNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> binId </code> not found 
     *  @throws org.osid.NullArgumentException <code> groupReceiver, binId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGroupNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.GroupNotificationSession getGroupNotificationSessionForBin(org.osid.resource.GroupReceiver groupRceeiver, 
                                                                                        org.osid.id.Id binId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getGroupNotificationSessionForBin not implemented");
    }


    /**
     *  Gets a session for retrieving gropup hierarchies. 
     *
     *  @return <code> a GroupHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGroupHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.GroupHierarchySession getGroupHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getGroupHierarchySession not implemented");
    }


    /**
     *  Gets the group hierarchy traversal session for the given resource 
     *  group. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a GroupHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGroupHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.BinHierarchySession getGroupHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getGroupHierarchySession not implemented");
    }


    /**
     *  Gets a group hierarchy session for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the bin 
     *  @return a <code> GroupHierarchySession </code> 
     *  @throws org.osid.NotFoundException <code> binId </code> not found 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGroupHierarchy() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.GroupHierarchySession getGroupHierarchySessionForBin(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getGroupHierarchySessionForBin not implemented");
    }


    /**
     *  Gets a group hierarchy session for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the bin 
     *  @param  proxy a proxy 
     *  @return a <code> GroupHierarchySession </code> 
     *  @throws org.osid.NotFoundException <code> binId </code> not found 
     *  @throws org.osid.NullArgumentException <code> binId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGroupHierarchy() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.GroupHierarchySession getGroupHierarchySessionForBin(org.osid.id.Id binId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getGroupHierarchySessionForBin not implemented");
    }


    /**
     *  Gets the session for retrieving resource agent mappings. 
     *
     *  @return a <code> ResourceAgentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResourceAgent() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceAgentSession getResourceAgentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getResourceAgentSession not implemented");
    }


    /**
     *  Gets the session for retrieving resource agent mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GroupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceAgents() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceAgentSession getResourceAgentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getResourceAgentSession not implemented");
    }


    /**
     *  Gets a resource agent session for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the bin 
     *  @return a <code> ResourceAgentSession </code> 
     *  @throws org.osid.NotFoundException <code> binId </code> not found 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResourceAgent() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceAgentSession getResourceAgentSessionForBin(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getResourceAgentSessionForBin not implemented");
    }


    /**
     *  Gets a resource agent session for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the bin 
     *  @param  proxy a proxy 
     *  @return a <code> ResourceAgentSession </code> 
     *  @throws org.osid.NotFoundException <code> binId </code> not found 
     *  @throws org.osid.NullArgumentException <code> binId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResourceAgent() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceAgentSession getResourceAgentSessionForBin(org.osid.id.Id binId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getResourceAgentSessionForBin not implemented");
    }


    /**
     *  Gets the session for assigning agents to resources. 
     *
     *  @return a <code> ResourceAgentAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceAgentAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceAgentAssignmentSession getResourceAgentAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getResourceAgentAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning agents to resources. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ResourceAgentAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceAgentAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceAgentAssignmentSession getResourceAgentAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getResourceAgentAssignmentSession not implemented");
    }


    /**
     *  Gets a resource agent session for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the bin 
     *  @return a <code> ResourceAgentAssignmentSession </code> 
     *  @throws org.osid.NotFoundException <code> binId </code> not found 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceAgentAssignment() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceAgentAssignmentSession getResourceAgentAssignmentSessionForBin(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getResourceAgentAssignmentSessionForBin not implemented");
    }


    /**
     *  Gets a resource agent session for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ResourceAgentAssignmentSession </code> 
     *  @throws org.osid.NotFoundException <code> binId </code> not found 
     *  @throws org.osid.NullArgumentException <code> binId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceAgentAssignment() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceAgentAssignmentSession getResourceAgentAssignmentSessionForBin(org.osid.id.Id binId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getResourceAgentAssignmentSessionForBin not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  relationship lookup service. 
     *
     *  @return a <code> ResourceRelationshipLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshipLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipLookupSession getResourceRelationshipLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getResourceRelationshipLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  relationship lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ResourceRelationshipLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshipLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipLookupSession getResourceRelationshipLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getResourceRelationshipLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  relationship lookup service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @return a <code> ResourceRelationshipLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshipLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipLookupSession getResourceRelationshipLookupSessionForBin(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getResourceRelationshipLookupSessionForBin not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  relationship lookup service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ResourceRelationshipLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshipLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipLookupSession getResourceRelationshipLookupSessionForBin(org.osid.id.Id binId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getResourceRelationshipLookupSessionForBin not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  relationship query service. 
     *
     *  @return a <code> ResourceRelationshipQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshipQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipQuerySession getResourceRelationshipQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getResourceRelationshipQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  relationship query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ResourceRelationshipQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshipQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipQuerySession getResourceRelationshipQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getResourceRelationshipQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  relationship query service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @return a <code> ResourceRelationshipQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshipQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipQuerySession getResourceRelationshipQuerySessionForBin(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getResourceRelationshipQuerySessionForBin not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  relationship query service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ResourceRelationshipQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshipQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipQuerySession getResourceRelationshipQuerySessionForBin(org.osid.id.Id binId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getResourceRelationshipQuerySessionForBin not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  relationship search service. 
     *
     *  @return a <code> ResourceRelationshipSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshipSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipSearchSession getResourceRelationshipSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getResourceRelationshipSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  relationship search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ResourceRelationshipSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshipSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipSearchSession getResourceRelationshipSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getResourceRelationshipSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  relationship search service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @return a <code> ResourceRelationshipSearchSession </code> 
     *  @throws org.osid.NotFoundException no bin found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshipSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipSearchSession getResourceRelationshipSearchSessionForBin(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getResourceRelationshipSearchSessionForBin not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  relationship search service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ResourceRelationshipSearchSession </code> 
     *  @throws org.osid.NotFoundException no bin found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> binId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshipSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipSearchSession getResourceRelationshipSearchSessionForBin(org.osid.id.Id binId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getResourceRelationshipSearchSessionForBin not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  relationship administration service. 
     *
     *  @return a <code> ResourceRelationshipAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshipAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipAdminSession getResourceRelationshipAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getResourceRelationshipAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  relationship administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ResourceRelationshipAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshipAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipAdminSession getResourceRelationshipAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getResourceRelationshipAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  relationship administration service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @return a <code> ResourceRelationshipAdminSession </code> 
     *  @throws org.osid.NotFoundException no bin found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshipAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipAdminSession getResourceRelationshipAdminSessionForBin(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getResourceRelationshipAdminSessionForBin not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  relationship administration service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ResourceRelationshipAdminSession </code> 
     *  @throws org.osid.NotFoundException no bin found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> binId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshipAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipAdminSession getResourceRelationshipAdminSessionForBin(org.osid.id.Id binId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getResourceRelationshipAdminSessionForBin not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  relationship notification service. 
     *
     *  @param  resourceRelationshipReceiver the notification callback 
     *  @return a <code> ResourceRelationshipNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          resourceRelationshipReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshipNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipNotificationSession getResourceRelationshipNotificationSession(org.osid.resource.ResourceRelationshipReceiver resourceRelationshipReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getResourceRelationshipNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  relationship notification service. 
     *
     *  @param  resourceRelationshipReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> ResourceRelationshipNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          resourceRelationshipReceiver </code> or <code> proxy </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshipNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipNotificationSession getResourceRelationshipNotificationSession(org.osid.resource.ResourceRelationshipReceiver resourceRelationshipReceiver, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getResourceRelationshipNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  relationship notification service for the given bin. 
     *
     *  @param  resourceRelationshipReceiver the notification callback 
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @return a <code> ResourceRelationshipNotificationSession </code> 
     *  @throws org.osid.NotFoundException no bin found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          resourceRelationshipReceiver </code> or <code> binId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshiptNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipNotificationSession getResourceRelationshipNotificationSessionForBin(org.osid.resource.ResourceRelationshipReceiver resourceRelationshipReceiver, 
                                                                                                                      org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getResourceRelationshipNotificationSessionForBin not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  relationship notification service for the given bin. 
     *
     *  @param  resourceRelationshipReceiver the notification callback 
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ResourceRelationshipNotificationSession </code> 
     *  @throws org.osid.NotFoundException no bin found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          resourceRelationshipReceiver, binId </code> or <code> proxy 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshiptNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipNotificationSession getResourceRelationshipNotificationSessionForBin(org.osid.resource.ResourceRelationshipReceiver resourceRelationshipReceiver, 
                                                                                                                      org.osid.id.Id binId, 
                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getResourceRelationshipNotificationSessionForBin not implemented");
    }


    /**
     *  Gets the session for retrieving resource relationship to bin mappings. 
     *
     *  @return a <code> ResourceRelationshipBinSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshipBin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipBinSession getResourceRelationshipBinSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getResourceRelationshipBinSession not implemented");
    }


    /**
     *  Gets the session for retrieving resource relationship to bin mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ResourceRelationshipBinSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshipBin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipBinSession getResourceRelationshipBinSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getResourceRelationshipBinSession not implemented");
    }


    /**
     *  Gets the session for assigning resource relationships to bin mappings. 
     *
     *  @return a <code> ResourceRelationshipBinAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshipBinAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipBinAssignmentSession getResourceRelationshipBinAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getResourceRelationshipBinAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning resource relationship to bin mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ResourceRelationshipBinAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshipBinAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipBinAssignmentSession getResourceRelationshipBinAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getResourceRelationshipBinAssignmentSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic resource relationship bins. 
     *
     *  @param  binId the <code> Id </code> of the bin 
     *  @return a <code> ResourceRelationshipSmartBinSession </code> 
     *  @throws org.osid.NotFoundException <code> binId </code> not found 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshipSmartBin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipSmartBinSession getResourceRelationshipSmartBinSession(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getResourceRelationshipSmartBinSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic resource relationship bins. 
     *
     *  @param  binId the <code> Id </code> of the bin 
     *  @param  proxy a proxy 
     *  @return a <code> ResourceRelationshipSmartBinSession </code> 
     *  @throws org.osid.NotFoundException <code> binId </code> not found 
     *  @throws org.osid.NullArgumentException <code> binId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshipSmartBin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipSmartBinSession getResourceRelationshipSmartBinSession(org.osid.id.Id binId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getResourceRelationshipSmartBinSession not implemented");
    }


    /**
     *  Gets the bin lookup session. 
     *
     *  @return a <code> BinLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBinLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.BinLookupSession getBinLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getBinLookupSession not implemented");
    }


    /**
     *  Gets the bin lookup session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BinLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBinLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.BinLookupSession getBinLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getBinLookupSession not implemented");
    }


    /**
     *  Gets the bin query session. 
     *
     *  @return a <code> BinQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBinQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.BinQuerySession getBinQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getBinQuerySession not implemented");
    }


    /**
     *  Gets the bin query session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BinQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBinQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.BinQuerySession getBinQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getBinQuerySession not implemented");
    }


    /**
     *  Gets the bin search session. 
     *
     *  @return a <code> BinSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBinSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.BinSearchSession getBinSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getBinSearchSession not implemented");
    }


    /**
     *  Gets the bin search session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BinSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBinSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.BinSearchSession getBinSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getBinSearchSession not implemented");
    }


    /**
     *  Gets the bin administrative session for creating, updating and 
     *  deleteing bins. 
     *
     *  @return a <code> BinAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBinAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.BinAdminSession getBinAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getBinAdminSession not implemented");
    }


    /**
     *  Gets the bin administrative session for creating, updating and 
     *  deleteing bins. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BinAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBinAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.BinAdminSession getBinAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getBinAdminSession not implemented");
    }


    /**
     *  Gets the notification session for subscribing to changes to a bin. 
     *
     *  @param  binReceiver the notification callback 
     *  @return a <code> BinNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> binReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBinNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.BinNotificationSession getBinNotificationSession(org.osid.resource.BinReceiver binReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getBinNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for subscribing to changes to a bin. 
     *
     *  @param  binReceiver notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> BinNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> binReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBinNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.BinNotificationSession getBinNotificationSession(org.osid.resource.BinReceiver binReceiver, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getBinNotificationSession not implemented");
    }


    /**
     *  Gets the bin hierarchy traversal session. 
     *
     *  @return <code> a BinHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBinHierarchy() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.BinHierarchySession getBinHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getBinHierarchySession not implemented");
    }


    /**
     *  Gets the bin hierarchy traversal session. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a BinHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnimplementedException <code> supportsBinHierarchy() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.BinHierarchySession getBinHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getBinHierarchySession not implemented");
    }


    /**
     *  Gets the bin hierarchy design session. 
     *
     *  @return a <code> BinHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBinHierarchyDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.BinHierarchyDesignSession getBinHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getBinHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the bin hierarchy design session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BinHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBinHierarchyDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.BinHierarchyDesignSession getBinHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getBinHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> ResourceBatchManager. </code> 
     *
     *  @return a <code> ResourceBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResourceBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.batch.ResourceBatchManager getResourceBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getResourceBatchManager not implemented");
    }


    /**
     *  Gets the <code> ResourceBatchProxyManager. </code> 
     *
     *  @return a <code> ResourceBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResourceBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.batch.ResourceBatchProxyManager getResourceBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getResourceBatchProxyManager not implemented");
    }


    /**
     *  Gets the <code> ResourceDemographicManager. </code> 
     *
     *  @return a <code> ResourceDemographicManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceDemographic() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.ResourceDemographicManager getResourceDemographicManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceManager.getResourceDemographicManager not implemented");
    }


    /**
     *  Gets the <code> ResourceDemographicProxyManager. </code> 
     *
     *  @return a <code> ResourceDemographicProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceDemographic() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.ResourceDemographicProxyManager getResourceDemographicProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.ResourceProxyManager.getResourceDemographicProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.resourceRecordTypes.clear();
        this.resourceRecordTypes.clear();

        this.resourceSearchRecordTypes.clear();
        this.resourceSearchRecordTypes.clear();

        this.resourceRelationshipRecordTypes.clear();
        this.resourceRelationshipRecordTypes.clear();

        this.resourceRelationshipSearchRecordTypes.clear();
        this.resourceRelationshipSearchRecordTypes.clear();

        this.binRecordTypes.clear();
        this.binRecordTypes.clear();

        this.binSearchRecordTypes.clear();
        this.binSearchRecordTypes.clear();

        return;
    }
}
