//
// AbstractDistributorQuery.java
//
//     A template for making a Distributor Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.distributor.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for distributors.
 */

public abstract class AbstractDistributorQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.provisioning.DistributorQuery {

    private final java.util.Collection<org.osid.provisioning.records.DistributorQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the broker <code> Id </code> for this query. 
     *
     *  @param  brokerId the broker <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> brokerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBrokerId(org.osid.id.Id brokerId, boolean match) {
        return;
    }


    /**
     *  Clears the broker <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearBrokerIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> BrokerQuery </code> is available. 
     *
     *  @return <code> true </code> if a broker query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a broker. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the broker query 
     *  @throws org.osid.UnimplementedException <code> supportsBrokerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerQuery getBrokerQuery() {
        throw new org.osid.UnimplementedException("supportsBrokerQuery() is false");
    }


    /**
     *  Matches distributors with any broker. 
     *
     *  @param  match <code> true </code> to match distributors with any 
     *          broker, <code> false </code> to match distributors with no 
     *          broker 
     */

    @OSID @Override
    public void matchAnyBroker(boolean match) {
        return;
    }


    /**
     *  Clears the broker query terms. 
     */

    @OSID @Override
    public void clearBrokerTerms() {
        return;
    }


    /**
     *  Sets the queue <code> Id </code> for this query. 
     *
     *  @param  queueId the queue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> queueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchQueueId(org.osid.id.Id queueId, boolean match) {
        return;
    }


    /**
     *  Clears the queue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearQueueIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> QueueQuery </code> is available. 
     *
     *  @return <code> true </code> if a queue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a queue. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the queue query 
     *  @throws org.osid.UnimplementedException <code> supportsQueueQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueQuery getQueueQuery() {
        throw new org.osid.UnimplementedException("supportsQueueQuery() is false");
    }


    /**
     *  Matches distributors with any queue. 
     *
     *  @param  match <code> true </code> to match distributors with any 
     *          queue, <code> false </code> to match distributors with no 
     *          queue 
     */

    @OSID @Override
    public void matchAnyQueue(boolean match) {
        return;
    }


    /**
     *  Clears the queue query terms. 
     */

    @OSID @Override
    public void clearQueueTerms() {
        return;
    }


    /**
     *  Sets the pool <code> Id </code> for this query. 
     *
     *  @param  poolId the pool <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> poolId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchPoolId(org.osid.id.Id poolId, boolean match) {
        return;
    }


    /**
     *  Clears the pool <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearPoolIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PoolQuery </code> is available. 
     *
     *  @return <code> true </code> if a pool query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolQuery() {
        return (false);
    }


    /**
     *  Gets the query for a pool. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the pool query 
     *  @throws org.osid.UnimplementedException <code> supportsPoolQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolQuery getPoolQuery() {
        throw new org.osid.UnimplementedException("supportsPoolQuery() is false");
    }


    /**
     *  Matches brokers with any pool. 
     *
     *  @param  match <code> true </code> to match distributors with any pool, 
     *          <code> false </code> to match distributors with no pool 
     */

    @OSID @Override
    public void matchAnyPool(boolean match) {
        return;
    }


    /**
     *  Clears the pool query terms. 
     */

    @OSID @Override
    public void clearPoolTerms() {
        return;
    }


    /**
     *  Sets the provision <code> Id </code> for this query to match 
     *  distributors that have a related provisionable. 
     *
     *  @param  provisionId a provision <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> provisionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProvisionId(org.osid.id.Id provisionId, boolean match) {
        return;
    }


    /**
     *  Clears the provision <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearProvisionIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ProvisionQuery </code> is available. 
     *
     *  @return <code> true </code> if a provision query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a provision. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the provision query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionQuery getProvisionQuery() {
        throw new org.osid.UnimplementedException("supportsProvisionQuery() is false");
    }


    /**
     *  Matches distributors that have any provision. 
     *
     *  @param  match <code> true </code> to match distributors with any 
     *          provision, <code> false </code> to match distributors with no 
     *          provision 
     */

    @OSID @Override
    public void matchAnyProvision(boolean match) {
        return;
    }


    /**
     *  Clears the provision query terms. 
     */

    @OSID @Override
    public void clearProvisionTerms() {
        return;
    }


    /**
     *  Sets the distributor <code> Id </code> for this query to match 
     *  distributors that have the specified distributor as an ancestor. 
     *
     *  @param  distributorId a distributor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorDistributorId(org.osid.id.Id distributorId, 
                                           boolean match) {
        return;
    }


    /**
     *  Clears the ancestor distributor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorDistributorIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> DistributorQuery </code> is available. 
     *
     *  @return <code> true </code> if a distributor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorDistributorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a distributor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the distributor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorDistributorQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQuery getAncestorDistributorQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorDistributorQuery() is false");
    }


    /**
     *  Matches distributors with any ancestor. 
     *
     *  @param  match <code> true </code> to match distributors with any 
     *          ancestor, <code> false </code> to match root distributors 
     */

    @OSID @Override
    public void matchAnyAncestorDistributor(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor distributor query terms. 
     */

    @OSID @Override
    public void clearAncestorDistributorTerms() {
        return;
    }


    /**
     *  Sets the distributor <code> Id </code> for this query to match 
     *  distributors that have the specified distributor as a descendant. 
     *
     *  @param  distributorId a distributor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantDistributorId(org.osid.id.Id distributorId, 
                                             boolean match) {
        return;
    }


    /**
     *  Clears the descendant distributor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantDistributorIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> DistributorQuery </code> is available. 
     *
     *  @return <code> true </code> if a distributor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantDistributorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a distributor/ Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the distributor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantDistributorQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQuery getDescendantDistributorQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantDistributorQuery() is false");
    }


    /**
     *  Matches distributors with any descendant. 
     *
     *  @param  match <code> true </code> to match distributors with any 
     *          descendant, <code> false </code> to match leaf distributors 
     */

    @OSID @Override
    public void matchAnyDescendantDistributor(boolean match) {
        return;
    }


    /**
     *  Clears the descendant distributor query terms. 
     */

    @OSID @Override
    public void clearDescendantDistributorTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given distributor query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a distributor implementing the requested record.
     *
     *  @param distributorRecordType a distributor record type
     *  @return the distributor query record
     *  @throws org.osid.NullArgumentException
     *          <code>distributorRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(distributorRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.DistributorQueryRecord getDistributorQueryRecord(org.osid.type.Type distributorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.DistributorQueryRecord record : this.records) {
            if (record.implementsRecordType(distributorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(distributorRecordType + " is not supported");
    }


    /**
     *  Adds a record to this distributor query. 
     *
     *  @param distributorQueryRecord distributor query record
     *  @param distributorRecordType distributor record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addDistributorQueryRecord(org.osid.provisioning.records.DistributorQueryRecord distributorQueryRecord, 
                                          org.osid.type.Type distributorRecordType) {

        addRecordType(distributorRecordType);
        nullarg(distributorQueryRecord, "distributor query record");
        this.records.add(distributorQueryRecord);        
        return;
    }
}
