//
// AbstractWorkflowRulesManager.java
//
//     An adapter for a WorkflowRulesManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.workflow.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a WorkflowRulesManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterWorkflowRulesManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.workflow.rules.WorkflowRulesManager>
    implements org.osid.workflow.rules.WorkflowRulesManager {


    /**
     *  Constructs a new {@code AbstractAdapterWorkflowRulesManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterWorkflowRulesManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterWorkflowRulesManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterWorkflowRulesManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up step constrainer is supported. 
     *
     *  @return <code> true </code> if step constrainer lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepConstrainerLookup() {
        return (getAdapteeManager().supportsStepConstrainerLookup());
    }


    /**
     *  Tests if querying step constrainer is supported. 
     *
     *  @return <code> true </code> if step constrainer query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepConstrainerQuery() {
        return (getAdapteeManager().supportsStepConstrainerQuery());
    }


    /**
     *  Tests if searching step constrainer is supported. 
     *
     *  @return <code> true </code> if step constrainer search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepConstrainerSearch() {
        return (getAdapteeManager().supportsStepConstrainerSearch());
    }


    /**
     *  Tests if a step constrainer administrative service is supported. 
     *
     *  @return <code> true </code> if step constrainer administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepConstrainerAdmin() {
        return (getAdapteeManager().supportsStepConstrainerAdmin());
    }


    /**
     *  Tests if a step constrainer notification service is supported. 
     *
     *  @return <code> true </code> if step constrainer notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepConstrainerNotification() {
        return (getAdapteeManager().supportsStepConstrainerNotification());
    }


    /**
     *  Tests if a step constrainer office lookup service is supported. 
     *
     *  @return <code> true </code> if a step constrainer office lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepConstrainerOffice() {
        return (getAdapteeManager().supportsStepConstrainerOffice());
    }


    /**
     *  Tests if a step constrainer office service is supported. 
     *
     *  @return <code> true </code> if step constrainer office assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepConstrainerOfficeAssignment() {
        return (getAdapteeManager().supportsStepConstrainerOfficeAssignment());
    }


    /**
     *  Tests if a step constrainer office lookup service is supported. 
     *
     *  @return <code> true </code> if a step constrainer office service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepConstrainerSmartOffice() {
        return (getAdapteeManager().supportsStepConstrainerSmartOffice());
    }


    /**
     *  Tests if a step constrainer rule lookup service is supported. 
     *
     *  @return <code> true </code> if a step constrainer rule lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepConstrainerRuleLookup() {
        return (getAdapteeManager().supportsStepConstrainerRuleLookup());
    }


    /**
     *  Tests if a step constrainer rule application service is supported. 
     *
     *  @return <code> true </code> if a step constrainer rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepConstrainerRuleApplication() {
        return (getAdapteeManager().supportsStepConstrainerRuleApplication());
    }


    /**
     *  Tests if looking up step constrainer enablers is supported. 
     *
     *  @return <code> true </code> if step constrainer enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepConstrainerEnablerLookup() {
        return (getAdapteeManager().supportsStepConstrainerEnablerLookup());
    }


    /**
     *  Tests if querying step constrainer enablers is supported. 
     *
     *  @return <code> true </code> if step constrainer enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepConstrainerEnablerQuery() {
        return (getAdapteeManager().supportsStepConstrainerEnablerQuery());
    }


    /**
     *  Tests if searching step constrainer enablers is supported. 
     *
     *  @return <code> true </code> if step constrainer enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepConstrainerEnablerSearch() {
        return (getAdapteeManager().supportsStepConstrainerEnablerSearch());
    }


    /**
     *  Tests if a step constrainer enabler administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if step constrainer enabler administration 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepConstrainerEnablerAdmin() {
        return (getAdapteeManager().supportsStepConstrainerEnablerAdmin());
    }


    /**
     *  Tests if a step constrainer enabler notification service is supported. 
     *
     *  @return <code> true </code> if step constrainer enabler notification 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepConstrainerEnablerNotification() {
        return (getAdapteeManager().supportsStepConstrainerEnablerNotification());
    }


    /**
     *  Tests if a step constrainer enabler office lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a step constrainer enabler office 
     *          lookup service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepConstrainerEnablerOffice() {
        return (getAdapteeManager().supportsStepConstrainerEnablerOffice());
    }


    /**
     *  Tests if a step constrainer enabler office service is supported. 
     *
     *  @return <code> true </code> if step constrainer enabler office 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsStepConstrainerEnablerOfficeAssignment() {
        return (getAdapteeManager().supportsStepConstrainerEnablerOfficeAssignment());
    }


    /**
     *  Tests if a step constrainer enabler office lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a step constrainer enabler office 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepConstrainerEnablerSmartOffice() {
        return (getAdapteeManager().supportsStepConstrainerEnablerSmartOffice());
    }


    /**
     *  Tests if a step constrainer enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a step constrainer enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepConstrainerEnablerRuleLookup() {
        return (getAdapteeManager().supportsStepConstrainerEnablerRuleLookup());
    }


    /**
     *  Tests if a step constrainer enabler rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if step constrainer enabler rule 
     *          application service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsStepConstrainerEnablerRuleApplication() {
        return (getAdapteeManager().supportsStepConstrainerEnablerRuleApplication());
    }


    /**
     *  Tests if looking up step processor is supported. 
     *
     *  @return <code> true </code> if step processor lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepProcessorLookup() {
        return (getAdapteeManager().supportsStepProcessorLookup());
    }


    /**
     *  Tests if querying step processor is supported. 
     *
     *  @return <code> true </code> if step processor query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepProcessorQuery() {
        return (getAdapteeManager().supportsStepProcessorQuery());
    }


    /**
     *  Tests if searching step processor is supported. 
     *
     *  @return <code> true </code> if step processor search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepProcessorSearch() {
        return (getAdapteeManager().supportsStepProcessorSearch());
    }


    /**
     *  Tests if a step processor administrative service is supported. 
     *
     *  @return <code> true </code> if step processor administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepProcessorAdmin() {
        return (getAdapteeManager().supportsStepProcessorAdmin());
    }


    /**
     *  Tests if a step processor notification service is supported. 
     *
     *  @return <code> true </code> if step processor notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepProcessorNotification() {
        return (getAdapteeManager().supportsStepProcessorNotification());
    }


    /**
     *  Tests if a step processor office lookup service is supported. 
     *
     *  @return <code> true </code> if a step processor office lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepProcessorOffice() {
        return (getAdapteeManager().supportsStepProcessorOffice());
    }


    /**
     *  Tests if a step processor office service is supported. 
     *
     *  @return <code> true </code> if step processor office assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepProcessorOfficeAssignment() {
        return (getAdapteeManager().supportsStepProcessorOfficeAssignment());
    }


    /**
     *  Tests if a step processor office lookup service is supported. 
     *
     *  @return <code> true </code> if a step processor office service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepProcessorSmartOffice() {
        return (getAdapteeManager().supportsStepProcessorSmartOffice());
    }


    /**
     *  Tests if a step processor rule lookup service is supported. 
     *
     *  @return <code> true </code> if a step processor rule lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepProcessorRuleLookup() {
        return (getAdapteeManager().supportsStepProcessorRuleLookup());
    }


    /**
     *  Tests if a step processor rule application service is supported. 
     *
     *  @return <code> true </code> if step processor rule application service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepProcessorRuleApplication() {
        return (getAdapteeManager().supportsStepProcessorRuleApplication());
    }


    /**
     *  Tests if looking up step processor enablers is supported. 
     *
     *  @return <code> true </code> if step processor enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepProcessorEnablerLookup() {
        return (getAdapteeManager().supportsStepProcessorEnablerLookup());
    }


    /**
     *  Tests if querying step processor enablers is supported. 
     *
     *  @return <code> true </code> if step processor enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepProcessorEnablerQuery() {
        return (getAdapteeManager().supportsStepProcessorEnablerQuery());
    }


    /**
     *  Tests if searching step processor enablers is supported. 
     *
     *  @return <code> true </code> if step processor enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepProcessorEnablerSearch() {
        return (getAdapteeManager().supportsStepProcessorEnablerSearch());
    }


    /**
     *  Tests if a step processor enabler administrative service is supported. 
     *
     *  @return <code> true </code> if step processor enabler administration 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepProcessorEnablerAdmin() {
        return (getAdapteeManager().supportsStepProcessorEnablerAdmin());
    }


    /**
     *  Tests if a step processor enabler notification service is supported. 
     *
     *  @return <code> true </code> if step processor enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepProcessorEnablerNotification() {
        return (getAdapteeManager().supportsStepProcessorEnablerNotification());
    }


    /**
     *  Tests if a step processor enabler office lookup service is supported. 
     *
     *  @return <code> true </code> if a step processor enabler office lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepProcessorEnablerOffice() {
        return (getAdapteeManager().supportsStepProcessorEnablerOffice());
    }


    /**
     *  Tests if a step processor enabler office service is supported. 
     *
     *  @return <code> true </code> if step processor enabler office 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsStepProcessorEnablerOfficeAssignment() {
        return (getAdapteeManager().supportsStepProcessorEnablerOfficeAssignment());
    }


    /**
     *  Tests if a step processor enabler office lookup service is supported. 
     *
     *  @return <code> true </code> if a step processor enabler office service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepProcessorEnablerSmartOffice() {
        return (getAdapteeManager().supportsStepProcessorEnablerSmartOffice());
    }


    /**
     *  Tests if a step processor enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if an processor enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepProcessorEnablerRuleLookup() {
        return (getAdapteeManager().supportsStepProcessorEnablerRuleLookup());
    }


    /**
     *  Tests if a step processor enabler rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if step processor enabler rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepProcessorEnablerRuleApplication() {
        return (getAdapteeManager().supportsStepProcessorEnablerRuleApplication());
    }


    /**
     *  Tests if looking up process enabler is supported. 
     *
     *  @return <code> true </code> if process enabler lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessEnablerLookup() {
        return (getAdapteeManager().supportsProcessEnablerLookup());
    }


    /**
     *  Tests if querying process enabler is supported. 
     *
     *  @return <code> true </code> if process enabler query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessEnablerQuery() {
        return (getAdapteeManager().supportsProcessEnablerQuery());
    }


    /**
     *  Tests if searching process enabler is supported. 
     *
     *  @return <code> true </code> if process enabler search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessEnablerSearch() {
        return (getAdapteeManager().supportsProcessEnablerSearch());
    }


    /**
     *  Tests if a process enabler administrative service is supported. 
     *
     *  @return <code> true </code> if process enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessEnablerAdmin() {
        return (getAdapteeManager().supportsProcessEnablerAdmin());
    }


    /**
     *  Tests if a process enabler notification service is supported. 
     *
     *  @return <code> true </code> if process enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessEnablerNotification() {
        return (getAdapteeManager().supportsProcessEnablerNotification());
    }


    /**
     *  Tests if a process enabler office lookup service is supported. 
     *
     *  @return <code> true </code> if a process enabler office lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessEnablerOffice() {
        return (getAdapteeManager().supportsProcessEnablerOffice());
    }


    /**
     *  Tests if a process enabler office service is supported. 
     *
     *  @return <code> true </code> if process enabler office assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessEnablerOfficeAssignment() {
        return (getAdapteeManager().supportsProcessEnablerOfficeAssignment());
    }


    /**
     *  Tests if a process enabler office lookup service is supported. 
     *
     *  @return <code> true </code> if a process enabler office service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessEnablerSmartOffice() {
        return (getAdapteeManager().supportsProcessEnablerSmartOffice());
    }


    /**
     *  Tests if a process enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a process enabler rule lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessEnablerRuleLookup() {
        return (getAdapteeManager().supportsProcessEnablerRuleLookup());
    }


    /**
     *  Tests if a process enabler rule application service is supported. 
     *
     *  @return <code> true </code> if a process enabler rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessEnablerRuleApplication() {
        return (getAdapteeManager().supportsProcessEnablerRuleApplication());
    }


    /**
     *  Gets the supported <code> StepConstrainer </code> record types. 
     *
     *  @return a list containing the supported <code> StepConstrainer </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getStepConstrainerRecordTypes() {
        return (getAdapteeManager().getStepConstrainerRecordTypes());
    }


    /**
     *  Tests if the given <code> StepConstrainer </code> record type is 
     *  supported. 
     *
     *  @param  stepConstrainerRecordType a <code> Type </code> indicating a 
     *          <code> StepConstrainer </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          stepConstrainerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsStepConstrainerRecordType(org.osid.type.Type stepConstrainerRecordType) {
        return (getAdapteeManager().supportsStepConstrainerRecordType(stepConstrainerRecordType));
    }


    /**
     *  Gets the supported <code> StepConstrainer </code> search record types. 
     *
     *  @return a list containing the supported <code> StepConstrainer </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getStepConstrainerSearchRecordTypes() {
        return (getAdapteeManager().getStepConstrainerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> StepConstrainer </code> search record type 
     *  is supported. 
     *
     *  @param  stepConstrainerSearchRecordType a <code> Type </code> 
     *          indicating a <code> StepConstrainer </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          stepConstrainerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsStepConstrainerSearchRecordType(org.osid.type.Type stepConstrainerSearchRecordType) {
        return (getAdapteeManager().supportsStepConstrainerSearchRecordType(stepConstrainerSearchRecordType));
    }


    /**
     *  Gets the supported <code> StepConstrainerEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> StepConstrainerEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getStepConstrainerEnablerRecordTypes() {
        return (getAdapteeManager().getStepConstrainerEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> StepConstrainerEnabler </code> record type 
     *  is supported. 
     *
     *  @param  stepConstrainerEnablerRecordType a <code> Type </code> 
     *          indicating a <code> StepConstrainerEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          stepConstrainerEnablerRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsStepConstrainerEnablerRecordType(org.osid.type.Type stepConstrainerEnablerRecordType) {
        return (getAdapteeManager().supportsStepConstrainerEnablerRecordType(stepConstrainerEnablerRecordType));
    }


    /**
     *  Gets the supported <code> StepConstrainerEnabler </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> StepConstrainerEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getStepConstrainerEnablerSearchRecordTypes() {
        return (getAdapteeManager().getStepConstrainerEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> StepConstrainerEnabler </code> search record 
     *  type is supported. 
     *
     *  @param  stepConstrainerEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> StepConstrainerEnabler </code> search 
     *          record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          stepConstrainerEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsStepConstrainerEnablerSearchRecordType(org.osid.type.Type stepConstrainerEnablerSearchRecordType) {
        return (getAdapteeManager().supportsStepConstrainerEnablerSearchRecordType(stepConstrainerEnablerSearchRecordType));
    }


    /**
     *  Gets the supported <code> StepProcessor </code> record types. 
     *
     *  @return a list containing the supported <code> StepProcessor </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getStepProcessorRecordTypes() {
        return (getAdapteeManager().getStepProcessorRecordTypes());
    }


    /**
     *  Tests if the given <code> StepProcessor </code> record type is 
     *  supported. 
     *
     *  @param  stepProcessorRecordType a <code> Type </code> indicating a 
     *          <code> StepProcessor </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> stepProcessorRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsStepProcessorRecordType(org.osid.type.Type stepProcessorRecordType) {
        return (getAdapteeManager().supportsStepProcessorRecordType(stepProcessorRecordType));
    }


    /**
     *  Gets the supported <code> StepProcessor </code> search record types. 
     *
     *  @return a list containing the supported <code> StepProcessor </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getStepProcessorSearchRecordTypes() {
        return (getAdapteeManager().getStepProcessorSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> StepProcessor </code> search record type is 
     *  supported. 
     *
     *  @param  stepProcessorSearchRecordType a <code> Type </code> indicating 
     *          a <code> StepProcessor </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          stepProcessorSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsStepProcessorSearchRecordType(org.osid.type.Type stepProcessorSearchRecordType) {
        return (getAdapteeManager().supportsStepProcessorSearchRecordType(stepProcessorSearchRecordType));
    }


    /**
     *  Gets the supported <code> StepProcessorEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> StepProcessorEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getStepProcessorEnablerRecordTypes() {
        return (getAdapteeManager().getStepProcessorEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> StepProcessorEnabler </code> record type is 
     *  supported. 
     *
     *  @param  stepProcessorEnablerRecordType a <code> Type </code> 
     *          indicating a <code> StepProcessorEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          stepProcessorEnablerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsStepProcessorEnablerRecordType(org.osid.type.Type stepProcessorEnablerRecordType) {
        return (getAdapteeManager().supportsStepProcessorEnablerRecordType(stepProcessorEnablerRecordType));
    }


    /**
     *  Gets the supported <code> StepProcessorEnabler </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> StepProcessorEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getStepProcessorEnablerSearchRecordTypes() {
        return (getAdapteeManager().getStepProcessorEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> StepProcessorEnabler </code> search record 
     *  type is supported. 
     *
     *  @param  stepProcessorEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> StepProcessorEnabler </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          stepProcessorEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsStepProcessorEnablerSearchRecordType(org.osid.type.Type stepProcessorEnablerSearchRecordType) {
        return (getAdapteeManager().supportsStepProcessorEnablerSearchRecordType(stepProcessorEnablerSearchRecordType));
    }


    /**
     *  Gets the supported <code> ProcessEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> ProcessEnabler </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProcessEnablerRecordTypes() {
        return (getAdapteeManager().getProcessEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> ProcessEnabler </code> record type is 
     *  supported. 
     *
     *  @param  processEnablerRecordType a <code> Type </code> indicating a 
     *          <code> ProcessEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> processEnablerRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProcessEnablerRecordType(org.osid.type.Type processEnablerRecordType) {
        return (getAdapteeManager().supportsProcessEnablerRecordType(processEnablerRecordType));
    }


    /**
     *  Gets the supported <code> ProcessEnabler </code> search record types. 
     *
     *  @return a list containing the supported <code> ProcessEnabler </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProcessEnablerSearchRecordTypes() {
        return (getAdapteeManager().getProcessEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> ProcessEnabler </code> search record type is 
     *  supported. 
     *
     *  @param  processEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> ProcessEnabler </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          processEnablerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProcessEnablerSearchRecordType(org.osid.type.Type processEnablerSearchRecordType) {
        return (getAdapteeManager().supportsProcessEnablerSearchRecordType(processEnablerSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer lookup service. 
     *
     *  @return a <code> StepConstrainerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerLookupSession getStepConstrainerLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepConstrainerLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer lookup service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepConstrainerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerLookupSession getStepConstrainerLookupSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStepConstrainerLookupSessionForOffice(officeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer query service. 
     *
     *  @return a <code> StepConstrainerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerQuerySession getStepConstrainerQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepConstrainerQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer query service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepConstrainerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerQuerySession getStepConstrainerQuerySessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStepConstrainerQuerySessionForOffice(officeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer search service. 
     *
     *  @return a <code> StepConstrainerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerSearchSession getStepConstrainerSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepConstrainerSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer earch service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepConstrainerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerSearchSession getStepConstrainerSearchSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStepConstrainerSearchSessionForOffice(officeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer administration service. 
     *
     *  @return a <code> StepConstrainerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerAdminSession getStepConstrainerAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepConstrainerAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer administration service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepConstrainerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerAdminSession getStepConstrainerAdminSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStepConstrainerAdminSessionForOffice(officeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer notification service. 
     *
     *  @param  stepConstrainerReceiver the notification callback 
     *  @return a <code> StepConstrainerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> stepConstrainerReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerNotificationSession getStepConstrainerNotificationSession(org.osid.workflow.rules.StepConstrainerReceiver stepConstrainerReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepConstrainerNotificationSession(stepConstrainerReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer notification service for the given office. 
     *
     *  @param  stepConstrainerReceiver the notification callback 
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepConstrainerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> stepConstrainerReceiver 
     *          </code> or <code> officeId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerNotificationSession getStepConstrainerNotificationSessionForOffice(org.osid.workflow.rules.StepConstrainerReceiver stepConstrainerReceiver, 
                                                                                                                     org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStepConstrainerNotificationSessionForOffice(stepConstrainerReceiver, officeId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup step constrainer/office 
     *  mappings for step constrainers. 
     *
     *  @return a <code> StepConstrainerOfficeSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerOffice() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerOfficeSession getStepConstrainerOfficeSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepConstrainerOfficeSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning step 
     *  constrainer to office. 
     *
     *  @return a <code> StepConstrainerOfficeAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerOfficeAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerOfficeAssignmentSession getStepConstrainerOfficeAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepConstrainerOfficeAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage step constrainer smart 
     *  office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepConstrainerSmartOfficeSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerSmartOffice() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerSmartOfficeSession getStepConstrainerSmartOfficeSession(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStepConstrainerSmartOfficeSession(officeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer mapping lookup service for looking up the rules applied to 
     *  a step. 
     *
     *  @return a <code> StepConstrainerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerRuleLookupSession getStepConstrainerRuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepConstrainerRuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer mapping lookup service for the given office for looking up 
     *  rules applied to a step. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepConstrainerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerRuleLookupSession getStepConstrainerRuleLookupSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStepConstrainerRuleLookupSessionForOffice(officeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer assignment service to apply to steps. 
     *
     *  @return a <code> StepConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerRuleApplicationSession getStepConstrainerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepConstrainerRuleApplicationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer assignment service for the given office to apply to steps. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerRuleApplicationSession getStepConstrainerRuleApplicationSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStepConstrainerRuleApplicationSessionForOffice(officeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer enabler lookup service. 
     *
     *  @return a <code> StepConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerLookupSession getStepConstrainerEnablerLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepConstrainerEnablerLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer enabler lookup service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerLookupSession getStepConstrainerEnablerLookupSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStepConstrainerEnablerLookupSessionForOffice(officeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer enabler query service. 
     *
     *  @return a <code> StepConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerQuerySession getStepConstrainerEnablerQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepConstrainerEnablerQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer enabler query service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerQuerySession getStepConstrainerEnablerQuerySessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStepConstrainerEnablerQuerySessionForOffice(officeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer enabler search service. 
     *
     *  @return a <code> StepConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerSearchSession getStepConstrainerEnablerSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepConstrainerEnablerSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer enablers earch service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerSearchSession getStepConstrainerEnablerSearchSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStepConstrainerEnablerSearchSessionForOffice(officeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer enabler administration service. 
     *
     *  @return a <code> StepConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerAdminSession getStepConstrainerEnablerAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepConstrainerEnablerAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer enabler administration service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerAdminSession getStepConstrainerEnablerAdminSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStepConstrainerEnablerAdminSessionForOffice(officeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer enabler notification service. 
     *
     *  @param  stepConstrainerEnablerReceiver the notification callback 
     *  @return a <code> StepConstrainerEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          stepConstrainerEnablerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerNotificationSession getStepConstrainerEnablerNotificationSession(org.osid.workflow.rules.StepConstrainerEnablerReceiver stepConstrainerEnablerReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepConstrainerEnablerNotificationSession(stepConstrainerEnablerReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer enabler notification service for the given office. 
     *
     *  @param  stepConstrainerEnablerReceiver the notification callback 
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepConstrainerEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          stepConstrainerEnablerReceiver </code> or <code> officeId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerNotificationSession getStepConstrainerEnablerNotificationSessionForOffice(org.osid.workflow.rules.StepConstrainerEnablerReceiver stepConstrainerEnablerReceiver, 
                                                                                                                                   org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStepConstrainerEnablerNotificationSessionForOffice(stepConstrainerEnablerReceiver, officeId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup step constrainer 
     *  enabler/office mappings for step constrainer enablers. 
     *
     *  @return a <code> StepConstrainerEnablerOfficeSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerOffice() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerOfficeSession getStepConstrainerEnablerOfficeSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepConstrainerEnablerOfficeSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning step 
     *  constrainer enablers to office. 
     *
     *  @return a <code> StepConstrainerEnablerOfficeAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerOfficeAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerOfficeAssignmentSession getStepConstrainerEnablerOfficeAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepConstrainerEnablerOfficeAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage step constrainer enabler 
     *  smart office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepConstrainerEnablerSmartOfficeSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerSmartOffice() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerSmartOfficeSession getStepConstrainerEnablerSmartOfficeSession(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStepConstrainerEnablerSmartOfficeSession(officeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer enabler mapping lookup service. 
     *
     *  @return a <code> StepConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerRuleLookupSession getStepConstrainerEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepConstrainerEnablerRuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer enabler mapping lookup service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerRuleLookupSession getStepConstrainerEnablerRuleLookupSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStepConstrainerEnablerRuleLookupSessionForOffice(officeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer enabler assignment service. 
     *
     *  @return a <code> StepConstrainerEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerRuleApplicationSession getStepConstrainerEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepConstrainerEnablerRuleApplicationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer enabler assignment service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepConstrainerEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerRuleApplicationSession getStepConstrainerEnablerRuleApplicationSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStepConstrainerEnablerRuleApplicationSessionForOffice(officeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  lookup service. 
     *
     *  @return a <code> StepProcessorLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorLookupSession getStepProcessorLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepProcessorLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  lookup service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepProcessorLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorLookupSession getStepProcessorLookupSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStepProcessorLookupSessionForOffice(officeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  query service. 
     *
     *  @return a <code> StepProcessorQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorQuerySession getStepProcessorQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepProcessorQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  query service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepProcessorQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorQuerySession getStepProcessorQuerySessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStepProcessorQuerySessionForOffice(officeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  search service. 
     *
     *  @return a <code> StepProcessorSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorSearchSession getStepProcessorSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepProcessorSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  earch service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepProcessorSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorSearchSession getStepProcessorSearchSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStepProcessorSearchSessionForOffice(officeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  administration service. 
     *
     *  @return a <code> StepProcessorAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorAdminSession getStepProcessorAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepProcessorAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  administration service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepProcessorAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorAdminSession getStepProcessorAdminSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStepProcessorAdminSessionForOffice(officeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  notification service. 
     *
     *  @param  stepProcessorReceiver the notification callback 
     *  @return a <code> StepProcessorNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> stepProcessorReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorNotificationSession getStepProcessorNotificationSession(org.osid.workflow.rules.StepProcessorReceiver stepProcessorReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepProcessorNotificationSession(stepProcessorReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  notification service for the given office. 
     *
     *  @param  stepProcessorReceiver the notification callback 
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepProcessorNotificationSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> stepProcessorReceiver 
     *          </code> or <code> officeId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorNotificationSession getStepProcessorNotificationSessionForOffice(org.osid.workflow.rules.StepProcessorReceiver stepProcessorReceiver, 
                                                                                                                 org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStepProcessorNotificationSessionForOffice(stepProcessorReceiver, officeId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup step processor/office 
     *  mappings for step processors. 
     *
     *  @return a <code> StepProcessorOfficeSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorOffice() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorOfficeSession getStepProcessorOfficeSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepProcessorOfficeSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning step 
     *  processor to office. 
     *
     *  @return a <code> StepProcessorOfficeAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorOfficeAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorOfficeAssignmentSession getStepProcessorOfficeAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepProcessorOfficeAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage step processor smart 
     *  office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepProcessorSmartOfficeSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorSmartOffice() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorSmartOfficeSession getStepProcessorSmartOfficeSession(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStepProcessorSmartOfficeSession(officeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  mapping lookup service for looking up the rules applied to a step. 
     *
     *  @return a <code> StepProcessorRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorRuleLookupSession getStepProcessorRuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepProcessorRuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  mapping lookup service for the given office for looking up rules 
     *  applied to a step. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepProcessorRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorRuleLookupSession getStepProcessorRuleLookupSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStepProcessorRuleLookupSessionForOffice(officeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  assignment service. 
     *
     *  @return a <code> StepProcessorRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorRuleApplication() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorRuleApplicationSession getStepProcessorRuleApplicationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepProcessorRuleApplicationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  assignment service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepProcessorRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorRuleApplicationSession getStepProcessorRuleApplicationSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStepProcessorRuleApplicationSessionForOffice(officeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  enabler lookup service. 
     *
     *  @return a <code> StepProcessorEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerLookupSession getStepProcessorEnablerLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepProcessorEnablerLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  enabler lookup service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepProcessorEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerLookupSession getStepProcessorEnablerLookupSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStepProcessorEnablerLookupSessionForOffice(officeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  enabler query service. 
     *
     *  @return a <code> StepProcessorEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerQuerySession getStepProcessorEnablerQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepProcessorEnablerQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  enabler query service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepProcessorEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerQuerySession getStepProcessorEnablerQuerySessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStepProcessorEnablerQuerySessionForOffice(officeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  enabler search service. 
     *
     *  @return a <code> StepProcessorEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerSearchSession getStepProcessorEnablerSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepProcessorEnablerSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  enablers earch service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepProcessorEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerSearchSession getStepProcessorEnablerSearchSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStepProcessorEnablerSearchSessionForOffice(officeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  enabler administration service. 
     *
     *  @return a <code> StepProcessorEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerAdminSession getStepProcessorEnablerAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepProcessorEnablerAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  enabler administration service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepProcessorEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerAdminSession getStepProcessorEnablerAdminSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStepProcessorEnablerAdminSessionForOffice(officeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  enabler notification service. 
     *
     *  @param  stepProcessorEnablerReceiver the notification callback 
     *  @return a <code> StepProcessorEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          stepProcessorEnablerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerNotificationSession getStepProcessorEnablerNotificationSession(org.osid.workflow.rules.StepProcessorEnablerReceiver stepProcessorEnablerReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepProcessorEnablerNotificationSession(stepProcessorEnablerReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  enabler notification service for the given office. 
     *
     *  @param  stepProcessorEnablerReceiver the notification callback 
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepProcessorEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          stepProcessorEnablerReceiver </code> or <code> officeId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerNotificationSession getStepProcessorEnablerNotificationSessionForOffice(org.osid.workflow.rules.StepProcessorEnablerReceiver stepProcessorEnablerReceiver, 
                                                                                                                               org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStepProcessorEnablerNotificationSessionForOffice(stepProcessorEnablerReceiver, officeId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup step processor 
     *  enabler/office mappings for step processor enablers. 
     *
     *  @return a <code> StepProcessorEnablerOfficeSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerOffice() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerOfficeSession getStepProcessorEnablerOfficeSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepProcessorEnablerOfficeSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning step 
     *  processor enablers to office. 
     *
     *  @return a <code> StepProcessorEnablerOfficeAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerOfficeAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerOfficeAssignmentSession getStepProcessorEnablerOfficeAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepProcessorEnablerOfficeAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage step processor enabler 
     *  smart office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepProcessorEnablerSmartOfficeSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerSmartOffice() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerSmartOfficeSession getStepProcessorEnablerSmartOfficeSession(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStepProcessorEnablerSmartOfficeSession(officeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  enabler mapping lookup service. 
     *
     *  @return a <code> StepProcessorEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerRuleLookupSession getStepProcessorEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepProcessorEnablerRuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  enabler mapping lookup service. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepProcessorEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerRuleLookupSession getStepProcessorEnablerRuleLookupSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStepProcessorEnablerRuleLookupSessionForOffice(officeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  enabler assignment service. 
     *
     *  @return a <code> StepProcessorEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerRuleApplicationSession getStepProcessorEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStepProcessorEnablerRuleApplicationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  enabler assignment service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepProcessorEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerRuleApplicationSession getStepProcessorEnablerRuleApplicationSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStepProcessorEnablerRuleApplicationSessionForOffice(officeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  enabler lookup service. 
     *
     *  @return a <code> ProcessEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerLookupSession getProcessEnablerLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessEnablerLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  enabler lookup service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> ProcessEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerLookupSession getProcessEnablerLookupSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessEnablerLookupSessionForOffice(officeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  enabler query service. 
     *
     *  @return a <code> ProcessEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerQuerySession getProcessEnablerQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessEnablerQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  enabler query service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> ProcessEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerQuerySession getProcessEnablerQuerySessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessEnablerQuerySessionForOffice(officeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  enabler search service. 
     *
     *  @return a <code> ProcessEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerSearchSession getProcessEnablerSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessEnablerSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  enabler earch service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> ProcessEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerSearchSession getProcessEnablerSearchSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessEnablerSearchSessionForOffice(officeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  enabler administration service. 
     *
     *  @return a <code> ProcessEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerAdminSession getProcessEnablerAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessEnablerAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  enabler administration service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> ProcessEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerAdminSession getProcessEnablerAdminSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessEnablerAdminSessionForOffice(officeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  enabler notification service. 
     *
     *  @param  processEnablerReceiver the notification callback 
     *  @return a <code> ProcessEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> processEnablerReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerNotificationSession getProcessEnablerNotificationSession(org.osid.workflow.rules.ProcessEnablerReceiver processEnablerReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessEnablerNotificationSession(processEnablerReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  enabler notification service for the given office. 
     *
     *  @param  processEnablerReceiver the notification callback 
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> ProcessEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> processEnablerReceiver 
     *          </code> or <code> officeId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerNotificationSession getProcessEnablerNotificationSessionForOffice(org.osid.workflow.rules.ProcessEnablerReceiver processEnablerReceiver, 
                                                                                                                   org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessEnablerNotificationSessionForOffice(processEnablerReceiver, officeId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup process enabler/office 
     *  mappings for process enablers. 
     *
     *  @return a <code> ProcessEnablerOfficeSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerOffice() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerOfficeSession getProcessEnablerOfficeSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessEnablerOfficeSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning process 
     *  enabler to office. 
     *
     *  @return a <code> ProcessEnablerOfficeAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerOfficeAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerOfficeAssignmentSession getProcessEnablerOfficeAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessEnablerOfficeAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage process enabler smart 
     *  office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> ProcessEnablerSmartOfficeSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerSmartOffice() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerSmartOfficeSession getProcessEnablerSmartOfficeSession(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessEnablerSmartOfficeSession(officeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  enabler mapping lookup service for looking up the rules applied to the 
     *  process. 
     *
     *  @return a <code> ProcessEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerRuleLookupSession getProcessEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessEnablerRuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  enabler mapping lookup service for the given office for looking up 
     *  rules applied to a process. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> ProcessEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerRuleLookupSession getProcessEnablerRuleLookupSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessEnablerRuleLookupSessionForOffice(officeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  enabler assignment service to apply to processs. 
     *
     *  @return a <code> ProcessEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerRuleApplicationSession getProcessEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessEnablerRuleApplicationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  enabler assignment service for the given office to apply to processs. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> ProcessEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerRuleApplicationSession getProcessEnablerRuleApplicationSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessEnablerRuleApplicationSessionForOffice(officeId));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
