//
// AbstractQueryAssessmentOfferedLookupSession.java
//
//    An inline adapter that maps an AssessmentOfferedLookupSession to
//    an AssessmentOfferedQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.assessment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an AssessmentOfferedLookupSession to
 *  an AssessmentOfferedQuerySession.
 */

public abstract class AbstractQueryAssessmentOfferedLookupSession
    extends net.okapia.osid.jamocha.assessment.spi.AbstractAssessmentOfferedLookupSession
    implements org.osid.assessment.AssessmentOfferedLookupSession {

    private final org.osid.assessment.AssessmentOfferedQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryAssessmentOfferedLookupSession.
     *
     *  @param querySession the underlying assessment offered query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryAssessmentOfferedLookupSession(org.osid.assessment.AssessmentOfferedQuerySession querySession) {
        nullarg(querySession, "assessment offered query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Bank</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Bank Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBankId() {
        return (this.session.getBankId());
    }


    /**
     *  Gets the <code>Bank</code> associated with this 
     *  session.
     *
     *  @return the <code>Bank</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.Bank getBank()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getBank());
    }


    /**
     *  Tests if this user can perform <code>AssessmentOffered</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAssessmentsOffered() {
        return (this.session.canSearchAssessmentsOffered());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include assessments offered in banks which are children
     *  of this bank in the bank hierarchy.
     */

    @OSID @Override
    public void useFederatedBankView() {
        this.session.useFederatedBankView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this bank only.
     */

    @OSID @Override
    public void useIsolatedBankView() {
        this.session.useIsolatedBankView();
        return;
    }
    
     
    /**
     *  Gets the <code>AssessmentOffered</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>AssessmentOffered</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>AssessmentOffered</code> and
     *  retained for compatibility.
     *
     *  @param  assessmentOfferedId <code>Id</code> of the
     *          <code>AssessmentOffered</code>
     *  @return the assessment offered
     *  @throws org.osid.NotFoundException <code>assessmentOfferedId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>assessmentOfferedId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOffered getAssessmentOffered(org.osid.id.Id assessmentOfferedId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.assessment.AssessmentOfferedQuery query = getQuery();
        query.matchId(assessmentOfferedId, true);
        org.osid.assessment.AssessmentOfferedList assessmentsOffered = this.session.getAssessmentsOfferedByQuery(query);
        if (assessmentsOffered.hasNext()) {
            return (assessmentsOffered.getNextAssessmentOffered());
        } 
        
        throw new org.osid.NotFoundException(assessmentOfferedId + " not found");
    }


    /**
     *  Gets an <code>AssessmentOfferedList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  assessmentsOffered specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>AssessmentsOffered</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  assessmentOfferedIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>AssessmentOffered</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentOfferedIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedList getAssessmentsOfferedByIds(org.osid.id.IdList assessmentOfferedIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.assessment.AssessmentOfferedQuery query = getQuery();

        try (org.osid.id.IdList ids = assessmentOfferedIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getAssessmentsOfferedByQuery(query));
    }


    /**
     *  Gets an <code>AssessmentOfferedList</code> corresponding to the given
     *  assessment offered genus <code>Type</code> which does not include
     *  assessments offered of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  assessments offered or an error results. Otherwise, the returned list
     *  may contain only those assessments offered that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  assessmentOfferedGenusType an assessmentOffered genus type 
     *  @return the returned <code>AssessmentOffered</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentOfferedGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedList getAssessmentsOfferedByGenusType(org.osid.type.Type assessmentOfferedGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.assessment.AssessmentOfferedQuery query = getQuery();
        query.matchGenusType(assessmentOfferedGenusType, true);
        return (this.session.getAssessmentsOfferedByQuery(query));
    }


    /**
     *  Gets an <code>AssessmentOfferedList</code> corresponding to the given
     *  assessment offered genus <code>Type</code> and include any additional
     *  assessments offered with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  assessments offered or an error results. Otherwise, the returned list
     *  may contain only those assessments offered that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  assessmentOfferedGenusType an assessmentOffered genus type 
     *  @return the returned <code>AssessmentOffered</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentOfferedGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedList getAssessmentsOfferedByParentGenusType(org.osid.type.Type assessmentOfferedGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.assessment.AssessmentOfferedQuery query = getQuery();
        query.matchParentGenusType(assessmentOfferedGenusType, true);
        return (this.session.getAssessmentsOfferedByQuery(query));
    }


    /**
     *  Gets an <code>AssessmentOfferedList</code> containing the given
     *  assessment offered record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  assessments offered or an error results. Otherwise, the returned list
     *  may contain only those assessments offered that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  assessmentOfferedRecordType an assessmentOffered record type 
     *  @return the returned <code>AssessmentOffered</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentOfferedRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedList getAssessmentsOfferedByRecordType(org.osid.type.Type assessmentOfferedRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.assessment.AssessmentOfferedQuery query = getQuery();
        query.matchRecordType(assessmentOfferedRecordType, true);
        return (this.session.getAssessmentsOfferedByQuery(query));
    }

    
    /**
     *  Gets an <code> AssessmentOfferedList </code> that have designated 
     *  start times where the start times fall in the given range inclusive. 
     *  In plenary mode, the returned list contains all known assessments 
     *  offered or an error results. Otherwise, the returned list may contain 
     *  only those assessments offered that are accessible through this 
     *  session. 
     *
     *  @param  start start of time range 
     *  @param  end end of time range 
     *  @return the returned <code> AssessmentOffered </code> list 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is less 
     *          than <code> start </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedList getAssessmentsOfferedByDate(org.osid.calendaring.DateTime start, 
                                                                                 org.osid.calendaring.DateTime end)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.assessment.AssessmentOfferedQuery query = getQuery();
        query.matchStartTime(start, end, true);
        return (this.session.getAssessmentsOfferedByQuery(query));
    }


    /**
     *  Gets an <code> AssessmentOfferedList </code> by the given assessment. 
     *  In plenary mode, the returned list contains all known assessments 
     *  offered or an error results. Otherwise, the returned list may contain 
     *  only those assessments offered that are accessible through this 
     *  session. 
     *
     *  @param  assessmentId <code> Id </code> of an <code> Assessment </code> 
     *  @return the returned <code> AssessmentOffered </code> list 
     *  @throws org.osid.NullArgumentException <code> assessmentId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedList getAssessmentsOfferedForAssessment(org.osid.id.Id assessmentId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.assessment.AssessmentOfferedQuery query = getQuery();
        query.matchAssessmentId(assessmentId, true);
        return (this.session.getAssessmentsOfferedByQuery(query));
    }


    /**
     *  Gets all <code>AssessmentsOffered</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  assessments offered or an error results. Otherwise, the returned list
     *  may contain only those assessments offered that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>AssessmentsOffered</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedList getAssessmentsOffered()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.assessment.AssessmentOfferedQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getAssessmentsOfferedByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.assessment.AssessmentOfferedQuery getQuery() {
        org.osid.assessment.AssessmentOfferedQuery query = this.session.getAssessmentOfferedQuery();
        return (query);
    }
}
