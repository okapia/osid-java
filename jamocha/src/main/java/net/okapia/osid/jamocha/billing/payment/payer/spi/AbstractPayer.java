//
// AbstractPayer.java
//
//     Defines a Payer.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.payment.payer.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Payer</code>.
 */

public abstract class AbstractPayer
    extends net.okapia.osid.jamocha.spi.AbstractTemporalOsidObject
    implements org.osid.billing.payment.Payer {

    private org.osid.resource.Resource resource;
    private org.osid.billing.Customer customer;
    private String creditCardNumber;
    private org.osid.calendaring.DateTime creditCardExpiration;
    private String creditCardCode;
    private String bankRoutingNumber;
    private String bankAccountNumber;
    private boolean usesActivity;
    private boolean usesCash;

    private final java.util.Collection<org.osid.billing.payment.records.PayerRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the resource <code> Id </code> representing the billing contact. 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResourceId() {
        return (this.resource.getId());
    }


    /**
     *  Gets the resource. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource()
        throws org.osid.OperationFailedException {

        return (this.resource);
    }


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    protected void setResource(org.osid.resource.Resource resource) {
        nullarg(resource, "resource");
        this.resource = resource;
        return;
    }


    /**
     *  Tests if this payer is linked directly to a customer account. 
     *
     *  @return <code> true </code> if this payer is linked to a customer, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasCustomer() {
        return (this.customer != null);
    }


    /**
     *  Gets the customer <code> Id. </code> 
     *
     *  @return the customer <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasCustomer()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.id.Id getCustomerId() {
        if (!hasCustomer()) {
            throw new org.osid.IllegalStateException("hasCustomer() is false");
        }

        return (this.customer.getId());
    }


    /**
     *  Gets the customer. 
     *
     *  @return the customer 
     *  @throws org.osid.IllegalStateException <code> hasCustomer() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.billing.Customer getCustomer()
        throws org.osid.OperationFailedException {

        if (!hasCustomer()) {
            throw new org.osid.IllegalStateException("hasCustomer() is false");
        }

        return (this.customer);
    }


    /**
     *  Sets the customer.
     *
     *  @param customer a customer
     *  @throws org.osid.NullArgumentException <code>customer</code>
     *          is <code>null</code>
     */

    protected void setCustomer(org.osid.billing.Customer customer) {
        nullarg(customer, "customer");
        this.customer = customer;
        return;
    }


    /**
     *  Tests if this payer uses the customer activity account for payments. 
     *
     *  @return <code> true </code> if this payer uses the customer activity, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.IllegalStateException <code> hasCustomer() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public boolean usesActivity() {
        return (this.usesActivity);
    }


    /**
     *  Sets the uses activity flag.
     *
     *  @param usesActivity <code> true </code> if this payer uses the
     *          customer activity, <code> false </code> otherwise
     */

    protected void setUsesActivity(boolean usesActivity) {
        reset();
        this.usesActivity = usesActivity;
        return;
    }


    /**
     *  Tests if this payer uses cash for payments. 
     *
     *  @return <code> true </code> if this payer uses the cash,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean usesCash() {
        return (this.usesCash);
    }


    /**
     *  Sets the uses cash flag.
     *
     *  @param usesCash <code> true </code> if this payer uses the
     *          cash, <code> false </code> otherwise
     */

    protected void setUsesCash(boolean usesCash) {
        reset();
        this.usesCash = usesCash;
        return;
    }


    /**
     *  Tests if this payer pays by credit card. 
     *
     *  @return <code> true </code> if this payer pays by credit card,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean hasCreditCard() {
        return (this.creditCardNumber != null);
    }


    /**
     *  Gets the credit card number. 
     *
     *  @return the credit card number 
     *  @throws org.osid.IllegalStateException <code> hasCreditCard()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public String getCreditCardNumber() {
        if (!hasCreditCard()) {
            throw new org.osid.IllegalStateException("hasCreditCard() is false");
        }

        return (this.creditCardNumber);
    }


    /**
     *  Sets the credit card number.
     *
     *  @param number a credit card number
     *  @throws org.osid.NullArgumentException
     *          <code>number</code> is <code>null</code>
     */

    protected void setCreditCardNumber(String number) {
        nullarg(number, "credit card number");
        reset();
        this.creditCardNumber = number;
        return;
    }


    /**
     *  Gets the credit card expiration date. 
     *
     *  @return the expiration date 
     *  @throws org.osid.IllegalStateException <code> hasCreditCard() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getCreditCardExpiration() {
        if (!hasCreditCard()) {
            throw new org.osid.IllegalStateException("hasCreditCard() is false");
        }

        return (this.creditCardExpiration);
    }


    /**
     *  Sets the credit card expiration.
     *
     *  @param expiration a credit card expiration
     *  @throws org.osid.NullArgumentException
     *          <code>expiration</code> is <code>null</code>
     */

    protected void setCreditCardExpiration(org.osid.calendaring.DateTime expiration) {
        nullarg(expiration, "credit card expiration");
        this.creditCardExpiration = expiration;
        return;
    }


    /**
     *  Gets the credit card security code. 
     *
     *  @return the credit card security code 
     *  @throws org.osid.IllegalStateException <code> hasCreditCard() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public String getCreditCardCode() {
        if (!hasCreditCard()) {
            throw new org.osid.IllegalStateException("hasCreditCard() is false");
        }

        return (this.creditCardCode);
    }


    /**
     *  Sets the credit card code.
     *
     *  @param code a credit card code
     *  @throws org.osid.NullArgumentException
     *          <code>code</code> is <code>null</code>
     */

    protected void setCreditCardCode(String code) {
        nullarg(code, "credit card code");
        this.creditCardCode = code;
        return;
    }


    /**
     *  Tests if this payer pays by bank account. 
     *
     *  @return <code> true </code> if this payer pays by bank
     *          account, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean hasBankAccount() {
        return (this.bankAccountNumber != null);
    }


    /**
     *  Gets the bank routing number. 
     *
     *  @return the bank routing number number 
     *  @throws org.osid.IllegalStateException <code> hasBankAccount()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public String getBankRoutingNumber() {
        if (!hasBankAccount()) {
            throw new org.osid.IllegalStateException("hasBankAccount() is false");
        }

        return (this.bankRoutingNumber);
    }


    /**
     *  Sets the bank routing number.
     *
     *  @param number a bank routing number
     *  @throws org.osid.NullArgumentException
     *          <code>number</code> is <code>null</code>
     */

    protected void setBankRoutingNumber(String number) {
        nullarg(number, "bank routing number");
        this.bankRoutingNumber = number;
        return;
    }


    /**
     *  Gets the bank account number. 
     *
     *  @return the bank account number 
     *  @throws org.osid.IllegalStateException <code> hasBankAccount() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public String getBankAccountNumber() {
        if (!hasBankAccount()) {
            throw new org.osid.IllegalStateException("hasBankAccount() is false");
        }

        return (this.bankAccountNumber);
    }


    /**
     *  Sets the bank account number.
     *
     *  @param number a bank account number
     *  @throws org.osid.NullArgumentException <code>number</code> is
     *          <code>null</code>
     */

    protected void setBankAccountNumber(String number) {
        nullarg(number, "bank account number");
        reset();
        this.bankAccountNumber = number;
        return;
    }


    /**
     *  Tests if this payer supports the given record
     *  <code>Type</code>.
     *
     *  @param  payerRecordType a payer record type 
     *  @return <code>true</code> if the payerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>payerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type payerRecordType) {
        for (org.osid.billing.payment.records.PayerRecord record : this.records) {
            if (record.implementsRecordType(payerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>Payer</code>
     *  record <code>Type</code>.
     *
     *  @param  payerRecordType the payer record type 
     *  @return the payer record 
     *  @throws org.osid.NullArgumentException
     *          <code>payerRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(payerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.payment.records.PayerRecord getPayerRecord(org.osid.type.Type payerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.payment.records.PayerRecord record : this.records) {
            if (record.implementsRecordType(payerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(payerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this payer. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param payerRecord the payer record
     *  @param payerRecordType payer record type
     *  @throws org.osid.NullArgumentException
     *          <code>payerRecord</code> or
     *          <code>payerRecordType</code> is
     *          <code>null</code>
     */
            
    protected void addPayerRecord(org.osid.billing.payment.records.PayerRecord payerRecord, 
                                  org.osid.type.Type payerRecordType) {

        nullarg(payerRecord, "payer record");
        addRecordType(payerRecordType);
        this.records.add(payerRecord);
        
        return;
    }


    private void reset() {
        this.usesActivity         = false;
        this.usesCash             = false;
        this.creditCardNumber     = null;
        this.creditCardExpiration = null;
        this.bankAccountNumber    = null;

        return;
    }
}
