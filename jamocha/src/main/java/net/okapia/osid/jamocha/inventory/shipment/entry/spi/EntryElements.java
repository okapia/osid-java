//
// EntryElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.shipment.entry.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class EntryElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the EntryElement Id.
     *
     *  @return the entry element Id
     */

    public static org.osid.id.Id getEntryEntityId() {
        return (makeEntityId("osid.inventory.shipment.Entry"));
    }


    /**
     *  Gets the StockId element Id.
     *
     *  @return the StockId element Id
     */

    public static org.osid.id.Id getStockId() {
        return (makeElementId("osid.inventory.shipment.entry.StockId"));
    }


    /**
     *  Gets the Stock element Id.
     *
     *  @return the Stock element Id
     */

    public static org.osid.id.Id getStock() {
        return (makeElementId("osid.inventory.shipment.entry.Stock"));
    }


    /**
     *  Gets the ModelId element Id.
     *
     *  @return the ModelId element Id
     */

    public static org.osid.id.Id getModelId() {
        return (makeElementId("osid.inventory.shipment.entry.ModelId"));
    }


    /**
     *  Gets the Model element Id.
     *
     *  @return the Model element Id
     */

    public static org.osid.id.Id getModel() {
        return (makeElementId("osid.inventory.shipment.entry.Model"));
    }


    /**
     *  Gets the ItemId element Id.
     *
     *  @return the ItemId element Id
     */

    public static org.osid.id.Id getItemId() {
        return (makeElementId("osid.inventory.shipment.entry.ItemId"));
    }


    /**
     *  Gets the Item element Id.
     *
     *  @return the Item element Id
     */

    public static org.osid.id.Id getItem() {
        return (makeElementId("osid.inventory.shipment.entry.Item"));
    }


    /**
     *  Gets the Quantity element Id.
     *
     *  @return the Quantity element Id
     */

    public static org.osid.id.Id getQuantity() {
        return (makeElementId("osid.inventory.shipment.entry.Quantity"));
    }


    /**
     *  Gets the UnitType element Id.
     *
     *  @return the UnitType element Id
     */

    public static org.osid.id.Id getUnitType() {
        return (makeElementId("osid.inventory.shipment.entry.UnitType"));
    }


    /**
     *  Gets the WarehouseId element Id.
     *
     *  @return the WarehouseId element Id
     */

    public static org.osid.id.Id getWarehouseId() {
        return (makeQueryElementId("osid.inventory.shipment.entry.WarehouseId"));
    }


    /**
     *  Gets the Warehouse element Id.
     *
     *  @return the Warehouse element Id
     */

    public static org.osid.id.Id getWarehouse() {
        return (makeQueryElementId("osid.inventory.shipment.entry.Warehouse"));
    }
}
