//
// AbstractQueueProcessorQueryInspector.java
//
//     A template for making a QueueProcessorQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.tracking.rules.queueprocessor.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for queue processors.
 */

public abstract class AbstractQueueProcessorQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidProcessorQueryInspector
    implements org.osid.tracking.rules.QueueProcessorQueryInspector {

    private final java.util.Collection<org.osid.tracking.rules.records.QueueProcessorQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the queue <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledQueueIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the queue query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.tracking.QueueQueryInspector[] getRuledQueueTerms() {
        return (new org.osid.tracking.QueueQueryInspector[0]);
    }


    /**
     *  Gets the front office <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFrontOfficeIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the front office query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeQueryInspector[] getFrontOfficeTerms() {
        return (new org.osid.tracking.FrontOfficeQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given queue processor query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a queue processor implementing the requested record.
     *
     *  @param queueProcessorRecordType a queue processor record type
     *  @return the queue processor query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>queueProcessorRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(queueProcessorRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.tracking.rules.records.QueueProcessorQueryInspectorRecord getQueueProcessorQueryInspectorRecord(org.osid.type.Type queueProcessorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.tracking.rules.records.QueueProcessorQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(queueProcessorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(queueProcessorRecordType + " is not supported");
    }


    /**
     *  Adds a record to this queue processor query. 
     *
     *  @param queueProcessorQueryInspectorRecord queue processor query inspector
     *         record
     *  @param queueProcessorRecordType queueProcessor record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addQueueProcessorQueryInspectorRecord(org.osid.tracking.rules.records.QueueProcessorQueryInspectorRecord queueProcessorQueryInspectorRecord, 
                                                   org.osid.type.Type queueProcessorRecordType) {

        addRecordType(queueProcessorRecordType);
        nullarg(queueProcessorRecordType, "queue processor record type");
        this.records.add(queueProcessorQueryInspectorRecord);        
        return;
    }
}
