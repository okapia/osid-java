//
// AbstractMapDispatchLookupSession
//
//    A simple framework for providing a Dispatch lookup service
//    backed by a fixed collection of dispatches.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.subscription.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Dispatch lookup service backed by a
 *  fixed collection of dispatches. The dispatches are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Dispatches</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapDispatchLookupSession
    extends net.okapia.osid.jamocha.subscription.spi.AbstractDispatchLookupSession
    implements org.osid.subscription.DispatchLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.subscription.Dispatch> dispatches = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.subscription.Dispatch>());


    /**
     *  Makes a <code>Dispatch</code> available in this session.
     *
     *  @param  dispatch a dispatch
     *  @throws org.osid.NullArgumentException <code>dispatch<code>
     *          is <code>null</code>
     */

    protected void putDispatch(org.osid.subscription.Dispatch dispatch) {
        this.dispatches.put(dispatch.getId(), dispatch);
        return;
    }


    /**
     *  Makes an array of dispatches available in this session.
     *
     *  @param  dispatches an array of dispatches
     *  @throws org.osid.NullArgumentException <code>dispatches<code>
     *          is <code>null</code>
     */

    protected void putDispatches(org.osid.subscription.Dispatch[] dispatches) {
        putDispatches(java.util.Arrays.asList(dispatches));
        return;
    }


    /**
     *  Makes a collection of dispatches available in this session.
     *
     *  @param  dispatches a collection of dispatches
     *  @throws org.osid.NullArgumentException <code>dispatches<code>
     *          is <code>null</code>
     */

    protected void putDispatches(java.util.Collection<? extends org.osid.subscription.Dispatch> dispatches) {
        for (org.osid.subscription.Dispatch dispatch : dispatches) {
            this.dispatches.put(dispatch.getId(), dispatch);
        }

        return;
    }


    /**
     *  Removes a Dispatch from this session.
     *
     *  @param  dispatchId the <code>Id</code> of the dispatch
     *  @throws org.osid.NullArgumentException <code>dispatchId<code> is
     *          <code>null</code>
     */

    protected void removeDispatch(org.osid.id.Id dispatchId) {
        this.dispatches.remove(dispatchId);
        return;
    }


    /**
     *  Gets the <code>Dispatch</code> specified by its <code>Id</code>.
     *
     *  @param  dispatchId <code>Id</code> of the <code>Dispatch</code>
     *  @return the dispatch
     *  @throws org.osid.NotFoundException <code>dispatchId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>dispatchId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.Dispatch getDispatch(org.osid.id.Id dispatchId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.subscription.Dispatch dispatch = this.dispatches.get(dispatchId);
        if (dispatch == null) {
            throw new org.osid.NotFoundException("dispatch not found: " + dispatchId);
        }

        return (dispatch);
    }


    /**
     *  Gets all <code>Dispatches</code>. In plenary mode, the returned
     *  list contains all known dispatches or an error
     *  results. Otherwise, the returned list may contain only those
     *  dispatches that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Dispatches</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.DispatchList getDispatches()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.subscription.dispatch.ArrayDispatchList(this.dispatches.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.dispatches.clear();
        super.close();
        return;
    }
}
