//
// AbstractBallotQueryInspector.java
//
//     A template for making a BallotQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.ballot.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for ballots.
 */

public abstract class AbstractBallotQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidGovernatorQueryInspector
    implements org.osid.voting.BallotQueryInspector {

    private final java.util.Collection<org.osid.voting.records.BallotQueryInspectorRecord> records = new java.util.ArrayList<>();

    private final OsidTemporalQueryInspector inspector = new OsidTemporalQueryInspector();

    
    /**
     *  Gets the effective query terms.
     *
     *  @return the effective terms
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getEffectiveTerms() {
        return (this.inspector.getEffectiveTerms());
    }


    /**
     *  Gets the start date query terms.
     *
     *  @return the start date terms
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getStartDateTerms() {
        return (this.inspector.getStartDateTerms());
    }


    /**
     *  Gets the end date query terms.
     *
     *  @return the end date terms
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getEndDateTerms() {
        return (this.inspector.getEndDateTerms());
    }


    /**
     *  Gets the date query terms.
     *
     *  @return the date terms
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getDateTerms() {
        return (this.inspector.getDateTerms());
    }

    
    /**
     *  Gets the revote query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getRevoteTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the race <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRaceIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the race query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.voting.RaceQueryInspector[] getRaceTerms() {
        return (new org.osid.voting.RaceQueryInspector[0]);
    }


    /**
     *  Gets the polls <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPollsIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the polls query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.voting.PollsQueryInspector[] getPollsTerms() {
        return (new org.osid.voting.PollsQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given ballot query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a ballot implementing the requested record.
     *
     *  @param ballotRecordType a ballot record type
     *  @return the ballot query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>ballotRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(ballotRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.records.BallotQueryInspectorRecord getBallotQueryInspectorRecord(org.osid.type.Type ballotRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.records.BallotQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(ballotRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(ballotRecordType + " is not supported");
    }


    /**
     *  Adds a record to this ballot query. 
     *
     *  @param ballotQueryInspectorRecord ballot query inspector
     *         record
     *  @param ballotRecordType ballot record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBallotQueryInspectorRecord(org.osid.voting.records.BallotQueryInspectorRecord ballotQueryInspectorRecord, 
                                                   org.osid.type.Type ballotRecordType) {

        addRecordType(ballotRecordType);
        nullarg(ballotRecordType, "ballot record type");
        this.records.add(ballotQueryInspectorRecord);        
        return;
    }


    protected class OsidTemporalQueryInspector 
        extends net.okapia.osid.jamocha.spi.AbstractOsidTemporalQueryInspector
        implements org.osid.OsidTemporalQueryInspector {
    }
}
