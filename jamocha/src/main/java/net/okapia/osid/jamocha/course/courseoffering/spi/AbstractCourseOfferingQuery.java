//
// AbstractCourseOfferingQuery.java
//
//     A template for making a CourseOffering Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.courseoffering.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for course offerings.
 */

public abstract class AbstractCourseOfferingQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.course.CourseOfferingQuery {

    private final java.util.Collection<org.osid.course.records.CourseOfferingQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the course <code> Id </code> for this query to match course 
     *  offerings that have a related course. 
     *
     *  @param  courseId a course <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCourseId(org.osid.id.Id courseId, boolean match) {
        return;
    }


    /**
     *  Clears the course <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CourseQuery </code> is available. 
     *
     *  @return <code> true </code> if a course query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the course query 
     *  @throws org.osid.UnimplementedException <code> supportsCourseQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseQuery getCourseQuery() {
        throw new org.osid.UnimplementedException("supportsCourseQuery() is false");
    }


    /**
     *  Clears the course terms. 
     */

    @OSID @Override
    public void clearCourseTerms() {
        return;
    }


    /**
     *  Sets the term <code> Id </code> for this query to match course 
     *  offerings that have a related term. 
     *
     *  @param  termId a term <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> termId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchTermId(org.osid.id.Id termId, boolean match) {
        return;
    }


    /**
     *  Clears the term <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearTermIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> TermQuery </code> is available. 
     *
     *  @return <code> true </code> if a term query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTermQuery() {
        return (false);
    }


    /**
     *  Gets the query for a reporting term. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the term query 
     *  @throws org.osid.UnimplementedException <code> supportsTermQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermQuery getTermQuery() {
        throw new org.osid.UnimplementedException("supportsTermQuery() is false");
    }


    /**
     *  Clears the term terms. 
     */

    @OSID @Override
    public void clearTermTerms() {
        return;
    }


    /**
     *  Adds a title for this query. 
     *
     *  @param  title title string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> title </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> title </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchTitle(String title, org.osid.type.Type stringMatchType, 
                           boolean match) {
        return;
    }


    /**
     *  Matches a title that has any value. 
     *
     *  @param  match <code> true </code> to match course offerings with any 
     *          title, <code> false </code> to match course offerings with no 
     *          title 
     */

    @OSID @Override
    public void matchAnyTitle(boolean match) {
        return;
    }


    /**
     *  Clears the title terms. 
     */

    @OSID @Override
    public void clearTitleTerms() {
        return;
    }


    /**
     *  Adds a course number for this query. 
     *
     *  @param  number course number string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> number </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> number </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchNumber(String number, org.osid.type.Type stringMatchType, 
                            boolean match) {
        return;
    }


    /**
     *  Matches a course number that has any value. 
     *
     *  @param  match <code> true </code> to match course offerings with any 
     *          number, <code> false </code> to match course offerings with no 
     *          number 
     */

    @OSID @Override
    public void matchAnyNumber(boolean match) {
        return;
    }


    /**
     *  Clears the number terms. 
     */

    @OSID @Override
    public void clearNumberTerms() {
        return;
    }


    /**
     *  Sets the resource <code> Id </code> for this query to match course 
     *  offerings that have an instructor. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchInstructorId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the instructor <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearInstructorIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstructorQuery() {
        return (false);
    }


    /**
     *  Gets the query for an instructor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getInstructorQuery() {
        throw new org.osid.UnimplementedException("supportsInstructorQuery() is false");
    }


    /**
     *  Matches course offerings that have any instructor. 
     *
     *  @param  match <code> true </code> to match course offerings with any 
     *          instructor, <code> false </code> to match course offerings 
     *          with no instructors 
     */

    @OSID @Override
    public void matchAnyInstructor(boolean match) {
        return;
    }


    /**
     *  Clears the instructor terms. 
     */

    @OSID @Override
    public void clearInstructorTerms() {
        return;
    }


    /**
     *  Sets the resource <code> Id </code> for this query to match courses 
     *  that have a sponsor. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSponsorId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the sponsor <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSponsorIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSponsorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a sponsor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a resource query 
     *  @throws org.osid.UnimplementedException <code> supportsSponsorQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getSponsorQuery() {
        throw new org.osid.UnimplementedException("supportsSponsorQuery() is false");
    }


    /**
     *  Matches courses that have any sponsor. 
     *
     *  @param  match <code> true </code> to match courses with any sponsor, 
     *          <code> false </code> to match courses with no sponsors 
     */

    @OSID @Override
    public void matchAnySponsor(boolean match) {
        return;
    }


    /**
     *  Clears the sponsor terms. 
     */

    @OSID @Override
    public void clearSponsorTerms() {
        return;
    }


    /**
     *  Sets the grade <code> Id </code> for this query to match
     *  courses that have a credit amount.
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code>gradeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCreditAmountId(org.osid.id.Id gradId, boolean match) {
        return;
    }


    /**
     *  Clears the credit amount <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCreditAmountIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradeQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreditAmountQuery() {
        return (false);
    }


    /**
     *  Gets the query for a credit amount. Multiple retrievals
     *  produce a nested <code> OR </code> term.
     *
     *  @return a grade query 
     *  @throws org.osid.UnimplementedException <code> supportsCreditAmountQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeQuery getCreditAmountQuery() {
        throw new org.osid.UnimplementedException("supportsCreditAmountQuery() is false");
    }


    /**
     *  Matches courses that have any credit amount. 
     *
     *  @param match <code> true </code> to match courses with any
     *         credit amount, <code> false </code> to match courses
     *         with no credit amounts
     */

    @OSID @Override
    public void matchAnyCreditAmount(boolean match) {
        return;
    }


    /**
     *  Clears the credit aount terms. 
     */

    @OSID @Override
    public void clearCreditAmountTerms() {
        return;
    }


    /**
     *  Sets the grade system <code> Id </code> for this query. 
     *
     *  @param  gradeSystemId a grade system <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeSystemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGradingOptionId(org.osid.id.Id gradeSystemId, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the grade system <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradingOptionIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradeSystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade system query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradingOptionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grading option. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a grade system query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradingOptionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQuery getGradingOptionQuery() {
        throw new org.osid.UnimplementedException("supportsGradingOptionQuery() is false");
    }


    /**
     *  Matches course offerings that have any grading option. 
     *
     *  @param  match <code> true </code> to match course offerings with any 
     *          grading option, <code> false </code> to match course offerings 
     *          with no grading options 
     */

    @OSID @Override
    public void matchAnyGradingOption(boolean match) {
        return;
    }


    /**
     *  Clears the grading option terms. 
     */

    @OSID @Override
    public void clearGradingOptionTerms() {
        return;
    }


    /**
     *  Matches course offerings that require registration. 
     *
     *  @param  match <code> true </code> to match course offerings requiring 
     *          registration,, <code> false </code> to match course offerings 
     *          not requiring registration 
     */

    @OSID @Override
    public void matchRequiresRegistration(boolean match) {
        return;
    }


    /**
     *  Clears the requires registration terms. 
     */

    @OSID @Override
    public void clearRequiresRegistrationTerms() {
        return;
    }


    /**
     *  Matches course offerings with minimum seating between the given 
     *  numbers inclusive. 
     *
     *  @param  min low number 
     *  @param  max high number 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> max </code> is less 
     *          than <code> min </code> 
     */

    @OSID @Override
    public void matchMinimumSeats(long min, long max, boolean match) {
        return;
    }


    /**
     *  Matches a course offering that has any minimum seating assigned. 
     *
     *  @param  match <code> true </code> to match course offerings with any 
     *          minimum seats, <code> false </code> to match course offerings 
     *          with no minimum seats 
     */

    @OSID @Override
    public void matchAnyMinimumSeats(boolean match) {
        return;
    }


    /**
     *  Clears the minimum seats terms. 
     */

    @OSID @Override
    public void clearMinimumSeatsTerms() {
        return;
    }


    /**
     *  Matches course offerings with maximum seating between the given 
     *  numbers inclusive. 
     *
     *  @param  min low number 
     *  @param  max high number 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> max </code> is less 
     *          than <code> min </code> 
     */

    @OSID @Override
    public void matchMaximumSeats(long min, long max, boolean match) {
        return;
    }


    /**
     *  Matches a course offering that has any maximum seating assigned. 
     *
     *  @param  match <code> true </code> to match course offerings with any 
     *          maximum seats, <code> false </code> to match course offerings 
     *          with no maximum seats 
     */

    @OSID @Override
    public void matchAnyMaximumSeats(boolean match) {
        return;
    }


    /**
     *  Clears the maximum seats terms. 
     */

    @OSID @Override
    public void clearMaximumSeatsTerms() {
        return;
    }


    /**
     *  Adds a class url for this query. 
     *
     *  @param  url url string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> url </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> url </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchURL(String url, org.osid.type.Type stringMatchType, 
                         boolean match) {
        return;
    }


    /**
     *  Matches a url that has any value. 
     *
     *  @param  match <code> true </code> to match course offerings with any 
     *          url, <code> false </code> to match course offerings with no 
     *          url 
     */

    @OSID @Override
    public void matchAnyURL(boolean match) {
        return;
    }


    /**
     *  Clears the url terms. 
     */

    @OSID @Override
    public void clearURLTerms() {
        return;
    }


    /**
     *  Adds a schedule informational string for this query. 
     *
     *  @param  scheduleInfo schedule string string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> scheduleInfo </code> 
     *          not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> scheduleInfo </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchScheduleInfo(String scheduleInfo, 
                                  org.osid.type.Type stringMatchType, 
                                  boolean match) {
        return;
    }


    /**
     *  Matches a schedule informational string that has any value. 
     *
     *  @param  match <code> true </code> to match course offerings with any 
     *          schedule string, <code> false </code> to match course 
     *          offerings with no schedule string 
     */

    @OSID @Override
    public void matchAnyScheduleInfo(boolean match) {
        return;
    }


    /**
     *  Clears the schedule info terms. 
     */

    @OSID @Override
    public void clearScheduleInfoTerms() {
        return;
    }


    /**
     *  Sets the event <code> Id </code> for this query to match course 
     *  offerings that have an event. 
     *
     *  @param  eventId an event <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> eventId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEventId(org.osid.id.Id eventId, boolean match) {
        return;
    }


    /**
     *  Clears the event <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearEventIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> EventQuery </code> is available. 
     *
     *  @return <code> true </code> if an event query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for an event. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the event query 
     *  @throws org.osid.UnimplementedException <code> supportsEventQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventQuery getEventQuery() {
        throw new org.osid.UnimplementedException("supportsEventQuery() is false");
    }


    /**
     *  Matches any event. 
     *
     *  @param  match <code> true </code> to match course offerings with any 
     *          event., <code> false </code> to match course offerings with no 
     *          events 
     */

    @OSID @Override
    public void matchAnyEvent(boolean match) {
        return;
    }


    /**
     *  Clears the event terms. 
     */

    @OSID @Override
    public void clearEventTerms() {
        return;
    }


    /**
     *  Sets the activity <code> Id </code> for this query to match course 
     *  offerings that have a related activity. 
     *
     *  @param  activityId an activity <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> activityId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchActivityId(org.osid.id.Id activityId, boolean match) {
        return;
    }


    /**
     *  Clears the activity <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearActivityIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> ActivityQuery </code> is available for the 
     *  location. 
     *
     *  @return <code> true </code> if an activity query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityQuery() {
        return (false);
    }


    /**
     *  Gets the query for an activity. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the activity query 
     *  @throws org.osid.UnimplementedException <code> supportsActivityQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityQuery getActivityQuery() {
        throw new org.osid.UnimplementedException("supportsActivityQuery() is false");
    }


    /**
     *  Matches any related activity. 
     *
     *  @param  match <code> true </code> to match course offerings with any 
     *          activity, <code> false </code> to match course offerings with 
     *          no activities 
     */

    @OSID @Override
    public void matchAnyActivity(boolean match) {
        return;
    }


    /**
     *  Clears the activity terms. 
     */

    @OSID @Override
    public void clearActivityTerms() {
        return;
    }


    /**
     *  Sets the course catalog <code> Id </code> for this query to match 
     *  course offerings assigned to course catalogs. 
     *
     *  @param  courseCatalogId the course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given course offering query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a course offering implementing the requested record.
     *
     *  @param courseOfferingRecordType a course offering record type
     *  @return the course offering query record
     *  @throws org.osid.NullArgumentException
     *          <code>courseOfferingRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(courseOfferingRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.CourseOfferingQueryRecord getCourseOfferingQueryRecord(org.osid.type.Type courseOfferingRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.records.CourseOfferingQueryRecord record : this.records) {
            if (record.implementsRecordType(courseOfferingRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(courseOfferingRecordType + " is not supported");
    }


    /**
     *  Adds a record to this course offering query. 
     *
     *  @param courseOfferingQueryRecord course offering query record
     *  @param courseOfferingRecordType courseOffering record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCourseOfferingQueryRecord(org.osid.course.records.CourseOfferingQueryRecord courseOfferingQueryRecord, 
                                          org.osid.type.Type courseOfferingRecordType) {

        addRecordType(courseOfferingRecordType);
        nullarg(courseOfferingQueryRecord, "course offering query record");
        this.records.add(courseOfferingQueryRecord);        
        return;
    }
}
