//
// AbstractQualifierSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authorization.qualifier.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractQualifierSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.authorization.QualifierSearchResults {

    private org.osid.authorization.QualifierList qualifiers;
    private final org.osid.authorization.QualifierQueryInspector inspector;
    private final java.util.Collection<org.osid.authorization.records.QualifierSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractQualifierSearchResults.
     *
     *  @param qualifiers the result set
     *  @param qualifierQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>qualifiers</code>
     *          or <code>qualifierQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractQualifierSearchResults(org.osid.authorization.QualifierList qualifiers,
                                            org.osid.authorization.QualifierQueryInspector qualifierQueryInspector) {
        nullarg(qualifiers, "qualifiers");
        nullarg(qualifierQueryInspector, "qualifier query inspectpr");

        this.qualifiers = qualifiers;
        this.inspector = qualifierQueryInspector;

        return;
    }


    /**
     *  Gets the qualifier list resulting from a search.
     *
     *  @return a qualifier list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.authorization.QualifierList getQualifiers() {
        if (this.qualifiers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.authorization.QualifierList qualifiers = this.qualifiers;
        this.qualifiers = null;
	return (qualifiers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.authorization.QualifierQueryInspector getQualifierQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  qualifier search record <code> Type. </code> This method must
     *  be used to retrieve a qualifier implementing the requested
     *  record.
     *
     *  @param qualifierSearchRecordType a qualifier search 
     *         record type 
     *  @return the qualifier search
     *  @throws org.osid.NullArgumentException
     *          <code>qualifierSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(qualifierSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.authorization.records.QualifierSearchResultsRecord getQualifierSearchResultsRecord(org.osid.type.Type qualifierSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.authorization.records.QualifierSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(qualifierSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(qualifierSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record qualifier search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addQualifierRecord(org.osid.authorization.records.QualifierSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "qualifier record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
