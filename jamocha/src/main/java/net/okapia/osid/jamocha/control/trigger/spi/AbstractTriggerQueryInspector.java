//
// AbstractTriggerQueryInspector.java
//
//     A template for making a TriggerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.trigger.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for triggers.
 */

public abstract class AbstractTriggerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleQueryInspector
    implements org.osid.control.TriggerQueryInspector {

    private final java.util.Collection<org.osid.control.records.TriggerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the controller <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getControllerIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the controller query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.ControllerQueryInspector[] getControllerTerms() {
        return (new org.osid.control.ControllerQueryInspector[0]);
    }


    /**
     *  Gets the ON event query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getTurnedOnTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the OFF event query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getTurnedOffTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the changed amount event query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getChangedVariableAmountTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the exceeds amount event query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getExceedsVariableAmountTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the deceeds amount event query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getDeceedsVariableAmountTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the changed state event query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getChangedDiscreetStateTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the controller <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDiscreetStateIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the discreet state query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.ControllerQueryInspector[] getDiscreetStateTerms() {
        return (new org.osid.control.ControllerQueryInspector[0]);
    }


    /**
     *  Gets the action group <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActionGroupIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the action group query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.ActionGroupQueryInspector[] getActionGroupTerms() {
        return (new org.osid.control.ActionGroupQueryInspector[0]);
    }


    /**
     *  Gets the scene <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSceneIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the scene query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.SceneQueryInspector[] getSceneTerms() {
        return (new org.osid.control.SceneQueryInspector[0]);
    }


    /**
     *  Gets the setting <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSettingIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the setting query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.SettingQueryInspector[] getSettingTerms() {
        return (new org.osid.control.SettingQueryInspector[0]);
    }


    /**
     *  Gets the system <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSystemIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the system query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.SystemQueryInspector[] getSystemTerms() {
        return (new org.osid.control.SystemQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given trigger query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a trigger implementing the requested record.
     *
     *  @param triggerRecordType a trigger record type
     *  @return the trigger query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>triggerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(triggerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.TriggerQueryInspectorRecord getTriggerQueryInspectorRecord(org.osid.type.Type triggerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.TriggerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(triggerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(triggerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this trigger query. 
     *
     *  @param triggerQueryInspectorRecord trigger query inspector
     *         record
     *  @param triggerRecordType trigger record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addTriggerQueryInspectorRecord(org.osid.control.records.TriggerQueryInspectorRecord triggerQueryInspectorRecord, 
                                                   org.osid.type.Type triggerRecordType) {

        addRecordType(triggerRecordType);
        nullarg(triggerRecordType, "trigger record type");
        this.records.add(triggerQueryInspectorRecord);        
        return;
    }
}
