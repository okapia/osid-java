//
// AbstractAdapterAccountLookupSession.java
//
//    An Account lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.financials.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Account lookup session adapter.
 */

public abstract class AbstractAdapterAccountLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.financials.AccountLookupSession {

    private final org.osid.financials.AccountLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterAccountLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterAccountLookupSession(org.osid.financials.AccountLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Business/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Business Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.session.getBusinessId());
    }


    /**
     *  Gets the {@code Business} associated with this session.
     *
     *  @return the {@code Business} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.Business getBusiness()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getBusiness());
    }


    /**
     *  Tests if this user can perform {@code Account} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupAccounts() {
        return (this.session.canLookupAccounts());
    }


    /**
     *  A complete view of the {@code Account} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAccountView() {
        this.session.useComparativeAccountView();
        return;
    }


    /**
     *  A complete view of the {@code Account} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAccountView() {
        this.session.usePlenaryAccountView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include accounts in businesses which are children
     *  of this business in the business hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        this.session.useFederatedBusinessView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        this.session.useIsolatedBusinessView();
        return;
    }
    
     
    /**
     *  Gets the {@code Account} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Account} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Account} and
     *  retained for compatibility.
     *
     *  @param accountId {@code Id} of the {@code Account}
     *  @return the account
     *  @throws org.osid.NotFoundException {@code accountId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code accountId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.Account getAccount(org.osid.id.Id accountId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAccount(accountId));
    }


    /**
     *  Gets an {@code AccountList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  accounts specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Accounts} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  accountIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Account} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code accountIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.AccountList getAccountsByIds(org.osid.id.IdList accountIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAccountsByIds(accountIds));
    }


    /**
     *  Gets an {@code AccountList} corresponding to the given
     *  account genus {@code Type} which does not include
     *  accounts of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  accounts or an error results. Otherwise, the returned list
     *  may contain only those accounts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  accountGenusType an account genus type 
     *  @return the returned {@code Account} list
     *  @throws org.osid.NullArgumentException
     *          {@code accountGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.AccountList getAccountsByGenusType(org.osid.type.Type accountGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAccountsByGenusType(accountGenusType));
    }


    /**
     *  Gets an {@code AccountList} corresponding to the given
     *  account genus {@code Type} and include any additional
     *  accounts with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  accounts or an error results. Otherwise, the returned list
     *  may contain only those accounts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  accountGenusType an account genus type 
     *  @return the returned {@code Account} list
     *  @throws org.osid.NullArgumentException
     *          {@code accountGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.AccountList getAccountsByParentGenusType(org.osid.type.Type accountGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAccountsByParentGenusType(accountGenusType));
    }


    /**
     *  Gets an {@code AccountList} containing the given
     *  account record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  accounts or an error results. Otherwise, the returned list
     *  may contain only those accounts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  accountRecordType an account record type 
     *  @return the returned {@code Account} list
     *  @throws org.osid.NullArgumentException
     *          {@code accountRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.AccountList getAccountsByRecordType(org.osid.type.Type accountRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAccountsByRecordType(accountRecordType));
    }


    /**
     *  Gets an {@code AccountList} associated with the given
     *  code. In plenary mode, the returned list contains all known
     *  accounts or an error results. Otherwise, the returned list may
     *  contain only those accounts that are accessible through this
     *  session.
     *
     *  @param  code n account code 
     *  @return the returned {@code Account} list 
     *  @throws org.osid.NullArgumentException {@code code} is {@code 
     *          null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.AccountList getAccountsByCode(String code)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAccountsByCode(code));
    }


    /**
     *  Gets all {@code Accounts}. 
     *
     *  In plenary mode, the returned list contains all known
     *  accounts or an error results. Otherwise, the returned list
     *  may contain only those accounts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Accounts} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.AccountList getAccounts()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAccounts());
    }
}
