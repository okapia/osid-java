//
// AbstractMutableAllocation.java
//
//     Defines a mutable Allocation.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.filing.allocation.allocation.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Allocation</code>.
 */

public abstract class AbstractMutableAllocation
    extends net.okapia.osid.jamocha.filing.allocation.allocation.spi.AbstractAllocation
    implements org.osid.filing.allocation.Allocation,
               net.okapia.osid.jamocha.builder.filing.allocation.allocation.AllocationMiter {


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this allocation. 
     *
     *  @param record allocation record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addAllocationRecord(org.osid.filing.allocation.records.AllocationRecord record, org.osid.type.Type recordType) {
        super.addAllocationRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the directory.
     *
     *  @param directory a directory
     *  @throws org.osid.NullArgumentException
     *          <code>directory</code> is <code>null</code>
     */

    @Override
    public void setDirectory(org.osid.filing.Directory directory) {
        super.setDirectory(directory);
        return;
    }


    /**
     *  Sets the agent.
     *
     *  @param agent an agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    @Override
    public void setAgent(org.osid.authentication.Agent agent) {
        super.setAgent(agent);
        return;
    }


    /**
     *  Sets the total space.
     *
     *  @param bytes the total space
     *  @throws org.osid.InvalidArgumentException <code>bytes</code>
     *          is negative
     */

    @Override
    public void setTotalSpace(long bytes) {
        super.setTotalSpace(bytes);
        return;
    }


    /**
     *  Sets the used space.
     *
     *  @param bytes the used space
     *  @throws org.osid.InvalidArgumentException <code>bytes</code>
     *          is negative
     */

    @Override
    public void setUsedSpace(long bytes) {
        super.setUsedSpace(bytes);
        return;
    }


    /**
     *  Sets the total files.
     *
     *  @param files total files
     *  @throws org.osid.InvalidArgumentException <code>files</code>
     *          is negative
     */

    @Override
    public void setTotalFiles(long files) {
        super.setTotalFiles(files);
        return;
    }


    /**
     *  Sets the used files.
     *
     *  @param files the used files
     *  @throws org.osid.InvalidArgumentException <code>files</code>
     *          is negative
     */

    @Override
    public void setUsedFiles(long files) {
        super.setUsedFiles(files);
        return;
    }
}

