//
// AbstractQueueConstrainer.java
//
//     Defines a QueueConstrainer builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.tracking.rules.queueconstrainer.spi;


/**
 *  Defines a <code>QueueConstrainer</code> builder.
 */

public abstract class AbstractQueueConstrainerBuilder<T extends AbstractQueueConstrainerBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidConstrainerBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.tracking.rules.queueconstrainer.QueueConstrainerMiter queueConstrainer;


    /**
     *  Constructs a new <code>AbstractQueueConstrainerBuilder</code>.
     *
     *  @param queueConstrainer the queue constrainer to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractQueueConstrainerBuilder(net.okapia.osid.jamocha.builder.tracking.rules.queueconstrainer.QueueConstrainerMiter queueConstrainer) {
        super(queueConstrainer);
        this.queueConstrainer = queueConstrainer;
        return;
    }


    /**
     *  Builds the queue constrainer.
     *
     *  @return the new queue constrainer
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.tracking.rules.QueueConstrainer build() {
        (new net.okapia.osid.jamocha.builder.validator.tracking.rules.queueconstrainer.QueueConstrainerValidator(getValidations())).validate(this.queueConstrainer);
        return (new net.okapia.osid.jamocha.builder.tracking.rules.queueconstrainer.ImmutableQueueConstrainer(this.queueConstrainer));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the queue constrainer miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.tracking.rules.queueconstrainer.QueueConstrainerMiter getMiter() {
        return (this.queueConstrainer);
    }


    /**
     *  Adds a QueueConstrainer record.
     *
     *  @param record a queue constrainer record
     *  @param recordType the type of queue constrainer record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.tracking.rules.records.QueueConstrainerRecord record, org.osid.type.Type recordType) {
        getMiter().addQueueConstrainerRecord(record, recordType);
        return (self());
    }
}       


