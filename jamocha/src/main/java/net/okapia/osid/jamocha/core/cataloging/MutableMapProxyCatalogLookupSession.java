//
// MutableMapProxyCatalogLookupSession
//
//    Implements a Catalog lookup service backed by a collection of
//    catalogs that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.cataloging;


/**
 *  Implements a Catalog lookup service backed by a collection of
 *  catalogs. The catalogs are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of catalogs can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyCatalogLookupSession
    extends net.okapia.osid.jamocha.core.cataloging.spi.AbstractMapCatalogLookupSession
    implements org.osid.cataloging.CatalogLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableMapProxyCatalogLookupSession} with no
     *  catalogs.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public MutableMapProxyCatalogLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyCatalogLookupSession} with a
     *  single catalog.
     *
     *  @param catalog a catalog
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code catalog} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyCatalogLookupSession(org.osid.cataloging.Catalog catalog, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putCatalog(catalog);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyCatalogLookupSession} using an
     *  array of catalogs.
     *
     *  @param catalogs an array of catalogs
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code catalogs} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyCatalogLookupSession(org.osid.cataloging.Catalog[] catalogs, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putCatalogs(catalogs);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyCatalogLookupSession} using
     *  a collection of catalogs.
     *
     *  @param catalogs a collection of catalogs
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code catalogs} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyCatalogLookupSession(java.util.Collection<? extends org.osid.cataloging.Catalog> catalogs,
                                                org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putCatalogs(catalogs);
        return;
    }

    
    /**
     *  Makes a {@code Catalog} available in this session.
     *
     *  @param catalog an catalog
     *  @throws org.osid.NullArgumentException {@code catalog{@code 
     *          is {@code null}
     */

    @Override
    public void putCatalog(org.osid.cataloging.Catalog catalog) {
        super.putCatalog(catalog);
        return;
    }


    /**
     *  Makes an array of catalogs available in this session.
     *
     *  @param catalogs an array of catalogs
     *  @throws org.osid.NullArgumentException {@code catalogs{@code 
     *          is {@code null}
     */

    @Override
    public void putCatalogs(org.osid.cataloging.Catalog[] catalogs) {
        super.putCatalogs(catalogs);
        return;
    }


    /**
     *  Makes collection of catalogs available in this session.
     *
     *  @param catalogs
     *  @throws org.osid.NullArgumentException {@code catalog{@code 
     *          is {@code null}
     */

    @Override
    public void putCatalogs(java.util.Collection<? extends org.osid.cataloging.Catalog> catalogs) {
        super.putCatalogs(catalogs);
        return;
    }


    /**
     *  Removes a Catalog from this session.
     *
     *  @param catalogId the {@code Id} of the catalog
     *  @throws org.osid.NullArgumentException {@code catalogId{@code  is
     *          {@code null}
     */

    @Override
    public void removeCatalog(org.osid.id.Id catalogId) {
        super.removeCatalog(catalogId);
        return;
    }    
}
