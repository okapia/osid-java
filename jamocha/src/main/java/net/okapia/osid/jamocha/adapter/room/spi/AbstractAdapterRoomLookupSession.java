//
// AbstractAdapterRoomLookupSession.java
//
//    A Room lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.room.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Room lookup session adapter.
 */

public abstract class AbstractAdapterRoomLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.room.RoomLookupSession {

    private final org.osid.room.RoomLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterRoomLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterRoomLookupSession(org.osid.room.RoomLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Campus/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Campus Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCampusId() {
        return (this.session.getCampusId());
    }


    /**
     *  Gets the {@code Campus} associated with this session.
     *
     *  @return the {@code Campus} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.Campus getCampus()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCampus());
    }


    /**
     *  Tests if this user can perform {@code Room} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupRooms() {
        return (this.session.canLookupRooms());
    }


    /**
     *  A complete view of the {@code Room} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRoomView() {
        this.session.useComparativeRoomView();
        return;
    }


    /**
     *  A complete view of the {@code Room} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRoomView() {
        this.session.usePlenaryRoomView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include rooms in campuses which are children
     *  of this campus in the campus hierarchy.
     */

    @OSID @Override
    public void useFederatedCampusView() {
        this.session.useFederatedCampusView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this campus only.
     */

    @OSID @Override
    public void useIsolatedCampusView() {
        this.session.useIsolatedCampusView();
        return;
    }
    

    /**
     *  Only rooms whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveRoomView() {
        this.session.useEffectiveRoomView();
        return;
    }
    

    /**
     *  All rooms of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveRoomView() {
        this.session.useAnyEffectiveRoomView();
        return;
    }

     
    /**
     *  Gets the {@code Room} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Room} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Room} and
     *  retained for compatibility.
     *
     *  In effective mode, rooms are returned that are currently
     *  effective.  In any effective mode, effective rooms and
     *  those currently expired are returned.
     *
     *  @param roomId {@code Id} of the {@code Room}
     *  @return the room
     *  @throws org.osid.NotFoundException {@code roomId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code roomId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.Room getRoom(org.osid.id.Id roomId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRoom(roomId));
    }


    /**
     *  Gets a {@code RoomList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  rooms specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Rooms} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, rooms are returned that are currently
     *  effective.  In any effective mode, effective rooms and
     *  those currently expired are returned.
     *
     *  @param  roomIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Room} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code roomIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.RoomList getRoomsByIds(org.osid.id.IdList roomIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRoomsByIds(roomIds));
    }


    /**
     *  Gets a {@code RoomList} corresponding to the given
     *  room genus {@code Type} which does not include
     *  rooms of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  rooms or an error results. Otherwise, the returned list
     *  may contain only those rooms that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, rooms are returned that are currently
     *  effective.  In any effective mode, effective rooms and
     *  those currently expired are returned.
     *
     *  @param  roomGenusType a room genus type 
     *  @return the returned {@code Room} list
     *  @throws org.osid.NullArgumentException
     *          {@code roomGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.RoomList getRoomsByGenusType(org.osid.type.Type roomGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRoomsByGenusType(roomGenusType));
    }


    /**
     *  Gets a {@code RoomList} corresponding to the given
     *  room genus {@code Type} and include any additional
     *  rooms with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  rooms or an error results. Otherwise, the returned list
     *  may contain only those rooms that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, rooms are returned that are currently
     *  effective.  In any effective mode, effective rooms and
     *  those currently expired are returned.
     *
     *  @param  roomGenusType a room genus type 
     *  @return the returned {@code Room} list
     *  @throws org.osid.NullArgumentException
     *          {@code roomGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.RoomList getRoomsByParentGenusType(org.osid.type.Type roomGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRoomsByParentGenusType(roomGenusType));
    }


    /**
     *  Gets a {@code RoomList} containing the given
     *  room record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  rooms or an error results. Otherwise, the returned list
     *  may contain only those rooms that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, rooms are returned that are currently
     *  effective.  In any effective mode, effective rooms and
     *  those currently expired are returned.
     *
     *  @param  roomRecordType a room record type 
     *  @return the returned {@code Room} list
     *  @throws org.osid.NullArgumentException
     *          {@code roomRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.RoomList getRoomsByRecordType(org.osid.type.Type roomRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRoomsByRecordType(roomRecordType));
    }


    /**
     *  Gets a {@code RoomList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  rooms or an error results. Otherwise, the returned list
     *  may contain only those rooms that are accessible
     *  through this session.
     *  
     *  In active mode, rooms are returned that are currently
     *  active. In any status mode, active and inactive rooms
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Room} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.room.RoomList getRoomsOnDate(org.osid.calendaring.DateTime from, 
                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRoomsOnDate(from, to));
    }
        

    /**
     *  Gets a list of all rooms of a genus type effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *  
     *  In plenary mode, the returned list contains all known rooms or
     *  an error results. Otherwise, the returned list may contain
     *  only those rooms that are accessible through this session.
     *  
     *  In effective mode, rooms are returned that are currently
     *  effective. In any effective mode, effective rooms and those
     *  currently expired are returned.
     *
     *  @param  roomGenusType a room genus {@code Type} 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code RoomList} 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code roomGenusType,
     *          from}, or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.RoomList getRoomsByGenusTypeOnDate(org.osid.type.Type roomGenusType, 
                                                            org.osid.calendaring.DateTime from, 
                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRoomsByGenusTypeOnDate(roomGenusType, from, to));
    }


    /**
     *  Gets a list of all rooms corresponding to a building {@code
     *  Id}.
     *  
     *  In plenary mode, the returned list contains all known rooms or
     *  an error results. Otherwise, the returned list may contain
     *  only those rooms that are accessible through this session.
     *  
     *  In effective mode, rooms are returned that are currently
     *  effective. In any effective mode, effective rooms and those
     *  currently expired are returned.
     *
     *  @param  buildingId the {@code Id} of the building 
     *  @return the returned {@code RoomList} 
     *  @throws org.osid.NullArgumentException {@code buildingId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.RoomList getRoomsForBuilding(org.osid.id.Id buildingId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRoomsForBuilding(buildingId));
    }


    /**
     *  Gets a list of all rooms corresponding to a building {@code
     *  Id} and effective during the entire given date range inclusive
     *  but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known rooms or
     *  an error results. Otherwise, the returned list may contain
     *  only those rooms that are accessible through this session.
     *  
     *  In effective mode, rooms are returned that are currently
     *  effective. In any effective mode, effective rooms and those
     *  currently expired are returned.
     *
     *  @param  buildingId a building {@code Id} 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned {@code RoomList} 
     *  @throws org.osid.InvalidArgumentException {@code to} is less 
     *          than {@code from} 
     *  @throws org.osid.NullArgumentException {@code buildingId,
     *          from}, or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.RoomList getRoomsForBuildingOnDate(org.osid.id.Id buildingId, 
                                                            org.osid.calendaring.DateTime from, 
                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRoomsForBuildingOnDate(buildingId, from, to));
    }


    /**
     *  Gets a list of all rooms of the given genus type corresponding
     *  to a building {@code Id.}
     *  
     *  In plenary mode, the returned list contains all known rooms or
     *  an error results. Otherwise, the returned list may contain
     *  only those rooms that are accessible through this session.
     *  
     *  In effective mode, rooms are returned that are currently
     *  effective. In any effective mode, effective rooms and those
     *  currently expired are returned.
     *
     *  @param  buildingId the {@code Id} of the building 
     *  @param  roomGenusType a room genus type 
     *  @return the returned {@code RoomList} 
     *  @throws org.osid.NullArgumentException {@code buildingId} or 
     *          {@code roomGenusType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.RoomList getRoomsByGenusTypeForBuilding(org.osid.id.Id buildingId, 
                                                                 org.osid.type.Type roomGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRoomsByGenusTypeForBuilding(buildingId, roomGenusType));
    }


    /**
     *  Gets a list of all rooms of the given genus type corresponding
     *  to a building {@code Id} and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known rooms or
     *  an error results. Otherwise, the returned list may contain
     *  only those rooms that are accessible through this session.
     *  
     *  In effective mode, rooms are returned that are currently
     *  effective. In any effective mode, effective rooms and those
     *  currently expired are returned.
     *
     *  @param  buildingId a building {@code Id} 
     *  @param  roomGenusType a room genus type 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned {@code RoomList} 
     *  @throws org.osid.InvalidArgumentException {@code to} is less 
     *          than {@code from} 
     *  @throws org.osid.NullArgumentException {@code buildingId,
     *          roomGenusType, from,} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.RoomList getRoomsByGenusTypeForBuildingOnDate(org.osid.id.Id buildingId, 
                                                                       org.osid.type.Type roomGenusType, 
                                                                       org.osid.calendaring.DateTime from, 
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRoomsByGenusTypeForBuildingOnDate(buildingId, roomGenusType, from, to));
    }    


    /**
     *  Gets a list of all rooms corresponding to a floor {@code
     *  Id}.
     *  
     *  In plenary mode, the returned list contains all known rooms or
     *  an error results. Otherwise, the returned list may contain
     *  only those rooms that are accessible through this session.
     *  
     *  In effective mode, rooms are returned that are currently
     *  effective. In any effective mode, effective rooms and those
     *  currently expired are returned.
     *
     *  @param  floorId the {@code Id} of the floor 
     *  @return the returned {@code RoomList} 
     *  @throws org.osid.NullArgumentException {@code floorId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.RoomList getRoomsForFloor(org.osid.id.Id floorId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRoomsForFloor(floorId));
    }


    /**
     *  Gets a list of all rooms corresponding to a floor {@code
     *  Id} and effective during the entire given date range inclusive
     *  but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known rooms or
     *  an error results. Otherwise, the returned list may contain
     *  only those rooms that are accessible through this session.
     *  
     *  In effective mode, rooms are returned that are currently
     *  effective. In any effective mode, effective rooms and those
     *  currently expired are returned.
     *
     *  @param  floorId a floor {@code Id} 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned {@code RoomList} 
     *  @throws org.osid.InvalidArgumentException {@code to} is less 
     *          than {@code from} 
     *  @throws org.osid.NullArgumentException {@code floorId,
     *          from}, or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.RoomList getRoomsForFloorOnDate(org.osid.id.Id floorId, 
                                                         org.osid.calendaring.DateTime from, 
                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRoomsForFloorOnDate(floorId, from, to));
    }


    /**
     *  Gets a list of all rooms of the given genus type corresponding
     *  to a floor {@code Id.}
     *  
     *  In plenary mode, the returned list contains all known rooms or
     *  an error results. Otherwise, the returned list may contain
     *  only those rooms that are accessible through this session.
     *  
     *  In effective mode, rooms are returned that are currently
     *  effective. In any effective mode, effective rooms and those
     *  currently expired are returned.
     *
     *  @param  floorId the {@code Id} of the floor 
     *  @param  roomGenusType a room genus type 
     *  @return the returned {@code RoomList} 
     *  @throws org.osid.NullArgumentException {@code floorId} or 
     *          {@code roomGenusType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.RoomList getRoomsByGenusTypeForFloor(org.osid.id.Id floorId, 
                                                              org.osid.type.Type roomGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRoomsByGenusTypeForFloor(floorId, roomGenusType));
    }


    /**
     *  Gets a list of all rooms of the given genus type corresponding
     *  to a floor {@code Id} and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known rooms or
     *  an error results. Otherwise, the returned list may contain
     *  only those rooms that are accessible through this session.
     *  
     *  In effective mode, rooms are returned that are currently
     *  effective. In any effective mode, effective rooms and those
     *  currently expired are returned.
     *
     *  @param  floorId a floor {@code Id} 
     *  @param  roomGenusType a room genus type 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned {@code RoomList} 
     *  @throws org.osid.InvalidArgumentException {@code to} is less
     *          than {@code from}
     *  @throws org.osid.NullArgumentException {@code floorId,
     *          roomGenusType, from,} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.RoomList getRoomsByGenusTypeForFloorOnDate(org.osid.id.Id floorId, 
                                                                    org.osid.type.Type roomGenusType, 
                                                                    org.osid.calendaring.DateTime from, 
                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRoomsByGenusTypeForFloorOnDate(floorId, roomGenusType, from, to));
    }    

    
    /**
     *  Gets a {@code RoomList} containing of the given room number.
     *  
     *  In plenary mode, the returned list contains all known rooms or
     *  an error results. Otherwise, the returned list may contain
     *  only those rooms that are accessible through this session.
     *  
     *  In effective mode, rooms are returned that are currently
     *  effective. In any effective mode, effective rooms and those
     *  currently expired are returned.
     *
     *  @param  number a room number 
     *  @return the returned {@code Room} list 
     *  @throws org.osid.NullArgumentException {@code number} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.RoomList getRoomsByRoomNumber(String number)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getRoomsByRoomNumber(number));
    }


    /**
     *  Gets all {@code Rooms}. 
     *
     *  In plenary mode, the returned list contains all known
     *  rooms or an error results. Otherwise, the returned list
     *  may contain only those rooms that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, rooms are returned that are currently
     *  effective.  In any effective mode, effective rooms and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Rooms} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.RoomList getRooms()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRooms());
    }
}
