//
// AbstractOffsetEventSearch.java
//
//     A template for making an OffsetEvent Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.offsetevent.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing offset event searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractOffsetEventSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.calendaring.OffsetEventSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.calendaring.records.OffsetEventSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.calendaring.OffsetEventSearchOrder offsetEventSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of offset events. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  offsetEventIds list of offset events
     *  @throws org.osid.NullArgumentException
     *          <code>offsetEventIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongOffsetEvents(org.osid.id.IdList offsetEventIds) {
        while (offsetEventIds.hasNext()) {
            try {
                this.ids.add(offsetEventIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongOffsetEvents</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of offset event Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getOffsetEventIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  offsetEventSearchOrder offset event search order 
     *  @throws org.osid.NullArgumentException
     *          <code>offsetEventSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>offsetEventSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderOffsetEventResults(org.osid.calendaring.OffsetEventSearchOrder offsetEventSearchOrder) {
	this.offsetEventSearchOrder = offsetEventSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.calendaring.OffsetEventSearchOrder getOffsetEventSearchOrder() {
	return (this.offsetEventSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given offset event search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an offset event implementing the requested record.
     *
     *  @param offsetEventSearchRecordType an offset event search record
     *         type
     *  @return the offset event search record
     *  @throws org.osid.NullArgumentException
     *          <code>offsetEventSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(offsetEventSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.OffsetEventSearchRecord getOffsetEventSearchRecord(org.osid.type.Type offsetEventSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.calendaring.records.OffsetEventSearchRecord record : this.records) {
            if (record.implementsRecordType(offsetEventSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(offsetEventSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this offset event search. 
     *
     *  @param offsetEventSearchRecord offset event search record
     *  @param offsetEventSearchRecordType offsetEvent search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addOffsetEventSearchRecord(org.osid.calendaring.records.OffsetEventSearchRecord offsetEventSearchRecord, 
                                           org.osid.type.Type offsetEventSearchRecordType) {

        addRecordType(offsetEventSearchRecordType);
        this.records.add(offsetEventSearchRecord);        
        return;
    }
}
