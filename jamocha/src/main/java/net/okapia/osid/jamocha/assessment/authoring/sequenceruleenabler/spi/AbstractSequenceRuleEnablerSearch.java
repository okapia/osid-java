//
// AbstractSequenceRuleEnablerSearch.java
//
//     A template for making a SequenceRuleEnabler Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.authoring.sequenceruleenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing sequence rule enabler searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractSequenceRuleEnablerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.assessment.authoring.SequenceRuleEnablerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.assessment.authoring.records.SequenceRuleEnablerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.assessment.authoring.SequenceRuleEnablerSearchOrder sequenceRuleEnablerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of sequence rule enablers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  sequenceRuleEnablerIds list of sequence rule enablers
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleEnablerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongSequenceRuleEnablers(org.osid.id.IdList sequenceRuleEnablerIds) {
        while (sequenceRuleEnablerIds.hasNext()) {
            try {
                this.ids.add(sequenceRuleEnablerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongSequenceRuleEnablers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of sequence rule enabler Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getSequenceRuleEnablerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  sequenceRuleEnablerSearchOrder sequence rule enabler search order 
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleEnablerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>sequenceRuleEnablerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderSequenceRuleEnablerResults(org.osid.assessment.authoring.SequenceRuleEnablerSearchOrder sequenceRuleEnablerSearchOrder) {
	this.sequenceRuleEnablerSearchOrder = sequenceRuleEnablerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.assessment.authoring.SequenceRuleEnablerSearchOrder getSequenceRuleEnablerSearchOrder() {
	return (this.sequenceRuleEnablerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given sequence rule enabler search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a sequence rule enabler implementing the requested record.
     *
     *  @param sequenceRuleEnablerSearchRecordType a sequence rule enabler search record
     *         type
     *  @return the sequence rule enabler search record
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(sequenceRuleEnablerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.authoring.records.SequenceRuleEnablerSearchRecord getSequenceRuleEnablerSearchRecord(org.osid.type.Type sequenceRuleEnablerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.assessment.authoring.records.SequenceRuleEnablerSearchRecord record : this.records) {
            if (record.implementsRecordType(sequenceRuleEnablerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(sequenceRuleEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this sequence rule enabler search. 
     *
     *  @param sequenceRuleEnablerSearchRecord sequence rule enabler search record
     *  @param sequenceRuleEnablerSearchRecordType sequenceRuleEnabler search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addSequenceRuleEnablerSearchRecord(org.osid.assessment.authoring.records.SequenceRuleEnablerSearchRecord sequenceRuleEnablerSearchRecord, 
                                           org.osid.type.Type sequenceRuleEnablerSearchRecordType) {

        addRecordType(sequenceRuleEnablerSearchRecordType);
        this.records.add(sequenceRuleEnablerSearchRecord);        
        return;
    }
}
