//
// AbstractRelevancySearch.java
//
//     A template for making a Relevancy Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ontology.relevancy.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing relevancy searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractRelevancySearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.ontology.RelevancySearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.ontology.records.RelevancySearchRecord> records = new java.util.ArrayList<>();
    private org.osid.ontology.RelevancySearchOrder relevancySearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of relevancies. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  relevancyIds list of relevancies
     *  @throws org.osid.NullArgumentException
     *          <code>relevancyIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongRelevancies(org.osid.id.IdList relevancyIds) {
        while (relevancyIds.hasNext()) {
            try {
                this.ids.add(relevancyIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongRelevancies</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of relevancy Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getRelevancyIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  relevancySearchOrder relevancy search order 
     *  @throws org.osid.NullArgumentException
     *          <code>relevancySearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>relevancySearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderRelevancyResults(org.osid.ontology.RelevancySearchOrder relevancySearchOrder) {
	this.relevancySearchOrder = relevancySearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.ontology.RelevancySearchOrder getRelevancySearchOrder() {
	return (this.relevancySearchOrder);
    }


    /**
     *  Gets the record corresponding to the given relevancy search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a relevancy implementing the requested record.
     *
     *  @param relevancySearchRecordType a relevancy search record
     *         type
     *  @return the relevancy search record
     *  @throws org.osid.NullArgumentException
     *          <code>relevancySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(relevancySearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.ontology.records.RelevancySearchRecord getRelevancySearchRecord(org.osid.type.Type relevancySearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.ontology.records.RelevancySearchRecord record : this.records) {
            if (record.implementsRecordType(relevancySearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(relevancySearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this relevancy search. 
     *
     *  @param relevancySearchRecord relevancy search record
     *  @param relevancySearchRecordType relevancy search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRelevancySearchRecord(org.osid.ontology.records.RelevancySearchRecord relevancySearchRecord, 
                                           org.osid.type.Type relevancySearchRecordType) {

        addRecordType(relevancySearchRecordType);
        this.records.add(relevancySearchRecord);        
        return;
    }
}
