//
// AbstractQueryPoolConstrainerEnablerLookupSession.java
//
//    An inline adapter that maps a PoolConstrainerEnablerLookupSession to
//    a PoolConstrainerEnablerQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a PoolConstrainerEnablerLookupSession to
 *  a PoolConstrainerEnablerQuerySession.
 */

public abstract class AbstractQueryPoolConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.provisioning.rules.spi.AbstractPoolConstrainerEnablerLookupSession
    implements org.osid.provisioning.rules.PoolConstrainerEnablerLookupSession {

    private boolean activeonly    = false;
    private boolean effectiveonly = false;
    private final org.osid.provisioning.rules.PoolConstrainerEnablerQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryPoolConstrainerEnablerLookupSession.
     *
     *  @param querySession the underlying pool constrainer enabler query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryPoolConstrainerEnablerLookupSession(org.osid.provisioning.rules.PoolConstrainerEnablerQuerySession querySession) {
        nullarg(querySession, "pool constrainer enabler query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Distributor</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Distributor Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.session.getDistributorId());
    }


    /**
     *  Gets the <code>Distributor</code> associated with this 
     *  session.
     *
     *  @return the <code>Distributor</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getDistributor());
    }


    /**
     *  Tests if this user can perform <code>PoolConstrainerEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPoolConstrainerEnablers() {
        return (this.session.canSearchPoolConstrainerEnablers());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include pool constrainer enablers in distributors which are children
     *  of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        this.session.useFederatedDistributorView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        this.session.useIsolatedDistributorView();
        return;
    }
    

    /**
     *  Only active pool constrainer enablers are returned by methods
     *  in this session.
     */
     
    @OSID @Override
    public void useActivePoolConstrainerEnablerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive pool constrainer enablers are returned by
     *  methods in this session.
     */
    
    @OSID @Override
    public void useAnyStatusPoolConstrainerEnablerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>PoolConstrainerEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>PoolConstrainerEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>PoolConstrainerEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, pool constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  pool constrainer enablers are returned.
     *
     *  @param  poolConstrainerEnablerId <code>Id</code> of the
     *          <code>PoolConstrainerEnabler</code>
     *  @return the pool constrainer enabler
     *  @throws org.osid.NotFoundException <code>poolConstrainerEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>poolConstrainerEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnabler getPoolConstrainerEnabler(org.osid.id.Id poolConstrainerEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.PoolConstrainerEnablerQuery query = getQuery();
        query.matchId(poolConstrainerEnablerId, true);
        org.osid.provisioning.rules.PoolConstrainerEnablerList poolConstrainerEnablers = this.session.getPoolConstrainerEnablersByQuery(query);
        if (poolConstrainerEnablers.hasNext()) {
            return (poolConstrainerEnablers.getNextPoolConstrainerEnabler());
        } 
        
        throw new org.osid.NotFoundException(poolConstrainerEnablerId + " not found");
    }


    /**
     *  Gets a <code>PoolConstrainerEnablerList</code> corresponding
     *  to the given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  poolConstrainerEnablers specified in the <code>Id</code> list,
     *  in the order of the list, including duplicates, or an error
     *  results if an <code>Id</code> in the supplied list is not
     *  found or inaccessible. Otherwise, inaccessible
     *  <code>PoolConstrainerEnablers</code> may be omitted from the
     *  list and may present the elements in any order including
     *  returning a unique set.
     *
     *  In active mode, pool constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  pool constrainer enablers are returned.
     *
     *  @param  poolConstrainerEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>PoolConstrainerEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>poolConstrainerEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnablerList getPoolConstrainerEnablersByIds(org.osid.id.IdList poolConstrainerEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.PoolConstrainerEnablerQuery query = getQuery();

        try (org.osid.id.IdList ids = poolConstrainerEnablerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getPoolConstrainerEnablersByQuery(query));
    }


    /**
     *  Gets a <code>PoolConstrainerEnablerList</code> corresponding
     *  to the given pool constrainer enabler genus <code>Type</code>
     *  which does not include pool constrainer enablers of types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known pool
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those pool constrainer enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, pool constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  pool constrainer enablers are returned.
     *
     *  @param  poolConstrainerEnablerGenusType a poolConstrainerEnabler genus type 
     *  @return the returned <code>PoolConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>poolConstrainerEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnablerList getPoolConstrainerEnablersByGenusType(org.osid.type.Type poolConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.PoolConstrainerEnablerQuery query = getQuery();
        query.matchGenusType(poolConstrainerEnablerGenusType, true);
        return (this.session.getPoolConstrainerEnablersByQuery(query));
    }


    /**
     *  Gets a <code>PoolConstrainerEnablerList</code> corresponding
     *  to the given pool constrainer enabler genus <code>Type</code>
     *  and include any additional pool constrainer enablers with
     *  genus types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known pool
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those pool constrainer enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, pool constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  pool constrainer enablers are returned.
     *
     *  @param  poolConstrainerEnablerGenusType a poolConstrainerEnabler genus type 
     *  @return the returned <code>PoolConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>poolConstrainerEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnablerList getPoolConstrainerEnablersByParentGenusType(org.osid.type.Type poolConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.PoolConstrainerEnablerQuery query = getQuery();
        query.matchParentGenusType(poolConstrainerEnablerGenusType, true);
        return (this.session.getPoolConstrainerEnablersByQuery(query));
    }


    /**
     *  Gets a <code>PoolConstrainerEnablerList</code> containing the
     *  given pool constrainer enabler record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known pool
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those pool constrainer enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, pool constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  pool constrainer enablers are returned.
     *
     *  @param  poolConstrainerEnablerRecordType a poolConstrainerEnabler record type 
     *  @return the returned <code>PoolConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>poolConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnablerList getPoolConstrainerEnablersByRecordType(org.osid.type.Type poolConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.PoolConstrainerEnablerQuery query = getQuery();
        query.matchRecordType(poolConstrainerEnablerRecordType, true);
        return (this.session.getPoolConstrainerEnablersByQuery(query));
    }


    /**
     *  Gets a <code>PoolConstrainerEnablerList</code> effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known pool
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those pool constrainer enablers
     *  that are accessible through this session.
     *  
     *  In active mode, pool constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  pool constrainer enablers are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>PoolConstrainerEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnablerList getPoolConstrainerEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.PoolConstrainerEnablerQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getPoolConstrainerEnablersByQuery(query));
    }
        
    
    /**
     *  Gets all <code>PoolConstrainerEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known pool
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those pool constrainer enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, pool constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  pool constrainer enablers are returned.
     *
     *  @return a list of <code>PoolConstrainerEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnablerList getPoolConstrainerEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.PoolConstrainerEnablerQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getPoolConstrainerEnablersByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.provisioning.rules.PoolConstrainerEnablerQuery getQuery() {
        org.osid.provisioning.rules.PoolConstrainerEnablerQuery query = this.session.getPoolConstrainerEnablerQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
