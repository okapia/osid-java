//
// InvariantMapProxyCourseCatalogLookupSession
//
//    Implements a CourseCatalog lookup service backed by a fixed
//    collection of courseCatalogs. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course;


/**
 *  Implements a CourseCatalog lookup service backed by a fixed
 *  collection of course catalogs. The course catalogs are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyCourseCatalogLookupSession
    extends net.okapia.osid.jamocha.core.course.spi.AbstractMapCourseCatalogLookupSession
    implements org.osid.course.CourseCatalogLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyCourseCatalogLookupSession} with no
     *  course catalogs.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public InvariantMapProxyCourseCatalogLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyCourseCatalogLookupSession} with a
     *  single course catalog.
     *
     *  @param courseCatalog a single course catalog
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyCourseCatalogLookupSession(org.osid.course.CourseCatalog courseCatalog, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putCourseCatalog(courseCatalog);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyCourseCatalogLookupSession} using
     *  an array of course catalogs.
     *
     *  @param courseCatalogs an array of course catalogs
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalogs} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyCourseCatalogLookupSession(org.osid.course.CourseCatalog[] courseCatalogs, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putCourseCatalogs(courseCatalogs);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyCourseCatalogLookupSession} using a
     *  collection of course catalogs.
     *
     *  @param courseCatalogs a collection of course catalogs
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalogs} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyCourseCatalogLookupSession(java.util.Collection<? extends org.osid.course.CourseCatalog> courseCatalogs,
                                                  org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putCourseCatalogs(courseCatalogs);
        return;
    }
}
