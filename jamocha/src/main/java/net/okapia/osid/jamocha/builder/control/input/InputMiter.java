//
// InputMiter.java
//
//     Defines an Input miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.control.input;


/**
 *  Defines an <code>Input</code> miter for use with the builders.
 */

public interface InputMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRelationshipMiter,
            org.osid.control.Input {


    /**
     *  Sets the device.
     *
     *  @param device a device
     *  @throws org.osid.NullArgumentException <code>device</code> is
     *          <code>null</code>
     */

    public void setDevice(org.osid.control.Device device);


    /**
     *  Sets the controller.
     *
     *  @param controller a controller
     *  @throws org.osid.NullArgumentException <code>controller</code>
     *          is <code>null</code>
     */

    public void setController(org.osid.control.Controller controller);


    /**
     *  Adds an Input record.
     *
     *  @param record an input record
     *  @param recordType the type of input record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addInputRecord(org.osid.control.records.InputRecord record, org.osid.type.Type recordType);
}       


