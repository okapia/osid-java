//
// InvariantMapModelLookupSession
//
//    Implements a Model lookup service backed by a fixed collection of
//    models.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inventory;


/**
 *  Implements a Model lookup service backed by a fixed
 *  collection of models. The models are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapModelLookupSession
    extends net.okapia.osid.jamocha.core.inventory.spi.AbstractMapModelLookupSession
    implements org.osid.inventory.ModelLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapModelLookupSession</code> with no
     *  models.
     *  
     *  @param warehouse the warehouse
     *  @throws org.osid.NullArgumnetException {@code warehouse} is
     *          {@code null}
     */

    public InvariantMapModelLookupSession(org.osid.inventory.Warehouse warehouse) {
        setWarehouse(warehouse);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapModelLookupSession</code> with a single
     *  model.
     *  
     *  @param warehouse the warehouse
     *  @param model a single model
     *  @throws org.osid.NullArgumentException {@code warehouse} or
     *          {@code model} is <code>null</code>
     */

      public InvariantMapModelLookupSession(org.osid.inventory.Warehouse warehouse,
                                               org.osid.inventory.Model model) {
        this(warehouse);
        putModel(model);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapModelLookupSession</code> using an array
     *  of models.
     *  
     *  @param warehouse the warehouse
     *  @param models an array of models
     *  @throws org.osid.NullArgumentException {@code warehouse} or
     *          {@code models} is <code>null</code>
     */

      public InvariantMapModelLookupSession(org.osid.inventory.Warehouse warehouse,
                                               org.osid.inventory.Model[] models) {
        this(warehouse);
        putModels(models);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapModelLookupSession</code> using a
     *  collection of models.
     *
     *  @param warehouse the warehouse
     *  @param models a collection of models
     *  @throws org.osid.NullArgumentException {@code warehouse} or
     *          {@code models} is <code>null</code>
     */

      public InvariantMapModelLookupSession(org.osid.inventory.Warehouse warehouse,
                                               java.util.Collection<? extends org.osid.inventory.Model> models) {
        this(warehouse);
        putModels(models);
        return;
    }
}
