//
// AbstractTypeManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.type.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractTypeManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.type.TypeManager,
               org.osid.type.TypeProxyManager {


    /**
     *  Constructs a new <code>AbstractTypeManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractTypeManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if <code> Type </code> lookup is supported. 
     *
     *  @return <code> true </code> if <code> Type </code> lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTypeLookup() {
        return (false);
    }


    /**
     *  Tests if a <code> Type </code> administrative service is supported. 
     *
     *  @return <code> true </code> if <code> Type </code> administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTypeAdmin() {
        return (false);
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the type lookup 
     *  service. 
     *
     *  @return a <code> TypeLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTypeLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.type.TypeLookupSession getTypeLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.type.TypeManager.getTypeLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the type lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TypeLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsTypeLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.type.TypeLookupSession getTypeLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.type.TypeProxyManager.getTypeLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the type admin 
     *  service. 
     *
     *  @return a <code> TypeAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTypeAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.type.TypeAdminSession getTypeAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.type.TypeManager.getTypeAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the <code> 
     *  TypeAdmin </code> service. 
     *
     *  @param  proxy a proxy 
     *  @return the new <code> TypeAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsTypeAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.type.TypeAdminSession getTypeAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.type.TypeProxyManager.getTypeAdminSession not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        return;
    }
}
