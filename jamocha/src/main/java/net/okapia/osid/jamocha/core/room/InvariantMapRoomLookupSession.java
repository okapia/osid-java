//
// InvariantMapRoomLookupSession
//
//    Implements a Room lookup service backed by a fixed collection of
//    rooms.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.room;


/**
 *  Implements a Room lookup service backed by a fixed
 *  collection of rooms. The rooms are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapRoomLookupSession
    extends net.okapia.osid.jamocha.core.room.spi.AbstractMapRoomLookupSession
    implements org.osid.room.RoomLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapRoomLookupSession</code> with no
     *  rooms.
     *  
     *  @param campus the campus
     *  @throws org.osid.NullArgumnetException {@code campus} is
     *          {@code null}
     */

    public InvariantMapRoomLookupSession(org.osid.room.Campus campus) {
        setCampus(campus);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapRoomLookupSession</code> with a single
     *  room.
     *  
     *  @param campus the campus
     *  @param room a single room
     *  @throws org.osid.NullArgumentException {@code campus} or
     *          {@code room} is <code>null</code>
     */

      public InvariantMapRoomLookupSession(org.osid.room.Campus campus,
                                               org.osid.room.Room room) {
        this(campus);
        putRoom(room);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapRoomLookupSession</code> using an array
     *  of rooms.
     *  
     *  @param campus the campus
     *  @param rooms an array of rooms
     *  @throws org.osid.NullArgumentException {@code campus} or
     *          {@code rooms} is <code>null</code>
     */

      public InvariantMapRoomLookupSession(org.osid.room.Campus campus,
                                               org.osid.room.Room[] rooms) {
        this(campus);
        putRooms(rooms);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapRoomLookupSession</code> using a
     *  collection of rooms.
     *
     *  @param campus the campus
     *  @param rooms a collection of rooms
     *  @throws org.osid.NullArgumentException {@code campus} or
     *          {@code rooms} is <code>null</code>
     */

      public InvariantMapRoomLookupSession(org.osid.room.Campus campus,
                                               java.util.Collection<? extends org.osid.room.Room> rooms) {
        this(campus);
        putRooms(rooms);
        return;
    }
}
