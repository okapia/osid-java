//
// AuctionProcessorEnablerValidator.java
//
//     Validates an AuctionProcessorEnabler.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.bidding.rules.auctionprocessorenabler;


/**
 *  Validates an AuctionProcessorEnabler.
 */

public final class AuctionProcessorEnablerValidator
    extends net.okapia.osid.jamocha.builder.validator.bidding.rules.auctionprocessorenabler.spi.AbstractAuctionProcessorEnablerValidator {


    /**
     *  Constructs a new <code>AuctionProcessorEnablerValidator</code>.
     */

    public AuctionProcessorEnablerValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AuctionProcessorEnablerValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    public AuctionProcessorEnablerValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates an AuctionProcessorEnabler with a default validation.
     *
     *  @param auctionProcessorEnabler an auction processor enabler to validate
     *  @return the auction processor enabler
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>auctionProcessorEnabler</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred
     */

    public static org.osid.bidding.rules.AuctionProcessorEnabler validateAuctionProcessorEnabler(org.osid.bidding.rules.AuctionProcessorEnabler auctionProcessorEnabler) {
        AuctionProcessorEnablerValidator validator = new AuctionProcessorEnablerValidator();
        validator.validate(auctionProcessorEnabler);
        return (auctionProcessorEnabler);
    }


    /**
     *  Validates an AuctionProcessorEnabler for the given validations.
     *
     *  @param validation an EnumSet of validations
     *  @param auctionProcessorEnabler an auction processor enabler to validate
     *  @return the auction processor enabler
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          or <code>auctionProcessorEnabler</code> is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred
     */

    public static org.osid.bidding.rules.AuctionProcessorEnabler validateAuctionProcessorEnabler(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation,
                                                       org.osid.bidding.rules.AuctionProcessorEnabler auctionProcessorEnabler) {

        AuctionProcessorEnablerValidator validator = new AuctionProcessorEnablerValidator(validation);
        validator.validate(auctionProcessorEnabler);
        return (auctionProcessorEnabler);
    }
}
