//
// InvariantIndexedMapProxyStepConstrainerLookupSession
//
//    Implements a StepConstrainer lookup service backed by a fixed
//    collection of stepConstrainers indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.workflow.rules;


/**
 *  Implements a StepConstrainer lookup service backed by a fixed
 *  collection of stepConstrainers. The stepConstrainers are indexed by
 *  {@code Id}, genus and record types.
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some stepConstrainers may be compatible
 *  with more types than are indicated through these stepConstrainer
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapProxyStepConstrainerLookupSession
    extends net.okapia.osid.jamocha.core.workflow.rules.spi.AbstractIndexedMapStepConstrainerLookupSession
    implements org.osid.workflow.rules.StepConstrainerLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapProxyStepConstrainerLookupSession}
     *  using an array of step constrainers.
     *
     *  @param office the office
     *  @param stepConstrainers an array of step constrainers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code office},
     *          {@code stepConstrainers} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapProxyStepConstrainerLookupSession(org.osid.workflow.Office office,
                                                         org.osid.workflow.rules.StepConstrainer[] stepConstrainers, 
                                                         org.osid.proxy.Proxy proxy) {

        setOffice(office);
        setSessionProxy(proxy);
        putStepConstrainers(stepConstrainers);

        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapProxyStepConstrainerLookupSession}
     *  using a collection of step constrainers.
     *
     *  @param office the office
     *  @param stepConstrainers a collection of step constrainers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code office},
     *          {@code stepConstrainers} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapProxyStepConstrainerLookupSession(org.osid.workflow.Office office,
                                                         java.util.Collection<? extends org.osid.workflow.rules.StepConstrainer> stepConstrainers,
                                                         org.osid.proxy.Proxy proxy) {

        setOffice(office);
        setSessionProxy(proxy);
        putStepConstrainers(stepConstrainers);

        return;
    }
}
