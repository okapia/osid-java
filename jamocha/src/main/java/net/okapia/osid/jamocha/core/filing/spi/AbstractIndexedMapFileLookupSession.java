//
// AbstractIndexedMapFileLookupSession.java
//
//    A simple framework for providing a File lookup service
//    backed by a fixed collection of files with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.filing.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a File lookup service backed by a
 *  fixed collection of files. The files are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some files may be compatible
 *  with more types than are indicated through these file
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Files</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapFileLookupSession
    extends AbstractMapFileLookupSession
    implements org.osid.filing.FileLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.filing.File> filesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.filing.File>());
    private final MultiMap<org.osid.type.Type, org.osid.filing.File> filesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.filing.File>());


    /**
     *  Makes a <code>File</code> available in this session.
     *
     *  @param  file a file
     *  @throws org.osid.NullArgumentException <code>file<code> is
     *          <code>null</code>
     */

    @Override
    protected void putFile(org.osid.filing.File file) {
        super.putFile(file);

        this.filesByGenus.put(file.getGenusType(), file);
        
        try (org.osid.type.TypeList types = file.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.filesByRecord.put(types.getNextType(), file);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }

    
    /**
     *  Makes an array of files available in this session.
     *
     *  @param  files an array of files
     *  @throws org.osid.NullArgumentException <code>files<code>
     *          is <code>null</code>
     */

    @Override
    protected void putFiles(org.osid.filing.File[] files) {
        for (org.osid.filing.File file : files) {
            putFile(file);
        }

        return;
    }


    /**
     *  Makes a collection of files available in this session.
     *
     *  @param  files a collection of files
     *  @throws org.osid.NullArgumentException <code>files<code>
     *          is <code>null</code>
     */

    @Override
    protected void putFiles(java.util.Collection<? extends org.osid.filing.File> files) {
        for (org.osid.filing.File file : files) {
            putFile(file);
        }

        return;
    }


    /**
     *  Removes a file from this session.
     *
     *  @param fileId the <code>Id</code> of the file
     *  @throws org.osid.NullArgumentException <code>fileId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeFile(org.osid.id.Id fileId) {
        org.osid.filing.File file;
        try {
            file = getFile(fileId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.filesByGenus.remove(file.getGenusType());

        try (org.osid.type.TypeList types = file.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.filesByRecord.remove(types.getNextType(), file);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeFile(fileId);
        return;
    }


    /**
     *  Gets a <code>FileList</code> corresponding to the given
     *  file genus <code>Type</code> which does not include
     *  files of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known files or an error results. Otherwise,
     *  the returned list may contain only those files that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  fileGenusType a file genus type 
     *  @return the returned <code>File</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>fileGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.FileList getFilesByGenusType(org.osid.type.Type fileGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.filing.file.ArrayFileList(this.filesByGenus.get(fileGenusType)));
    }


    /**
     *  Gets a <code>FileList</code> containing the given
     *  file record <code>Type</code>. In plenary mode, the
     *  returned list contains all known files or an error
     *  results. Otherwise, the returned list may contain only those
     *  files that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  fileRecordType a file record type 
     *  @return the returned <code>file</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>fileRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.FileList getFilesByRecordType(org.osid.type.Type fileRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.filing.file.ArrayFileList(this.filesByRecord.get(fileRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.filesByGenus.clear();
        this.filesByRecord.clear();

        super.close();

        return;
    }
}
