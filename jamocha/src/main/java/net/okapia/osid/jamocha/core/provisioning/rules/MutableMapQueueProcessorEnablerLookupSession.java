//
// MutableMapQueueProcessorEnablerLookupSession
//
//    Implements a QueueProcessorEnabler lookup service backed by a collection of
//    queueProcessorEnablers that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.rules;


/**
 *  Implements a QueueProcessorEnabler lookup service backed by a collection of
 *  queue processor enablers. The queue processor enablers are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of queue processor enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapQueueProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.core.provisioning.rules.spi.AbstractMapQueueProcessorEnablerLookupSession
    implements org.osid.provisioning.rules.QueueProcessorEnablerLookupSession {


    /**
     *  Constructs a new {@code MutableMapQueueProcessorEnablerLookupSession}
     *  with no queue processor enablers.
     *
     *  @param distributor the distributor
     *  @throws org.osid.NullArgumentException {@code distributor} is
     *          {@code null}
     */

      public MutableMapQueueProcessorEnablerLookupSession(org.osid.provisioning.Distributor distributor) {
        setDistributor(distributor);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapQueueProcessorEnablerLookupSession} with a
     *  single queueProcessorEnabler.
     *
     *  @param distributor the distributor  
     *  @param queueProcessorEnabler a queue processor enabler
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code queueProcessorEnabler} is {@code null}
     */

    public MutableMapQueueProcessorEnablerLookupSession(org.osid.provisioning.Distributor distributor,
                                           org.osid.provisioning.rules.QueueProcessorEnabler queueProcessorEnabler) {
        this(distributor);
        putQueueProcessorEnabler(queueProcessorEnabler);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapQueueProcessorEnablerLookupSession}
     *  using an array of queue processor enablers.
     *
     *  @param distributor the distributor
     *  @param queueProcessorEnablers an array of queue processor enablers
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code queueProcessorEnablers} is {@code null}
     */

    public MutableMapQueueProcessorEnablerLookupSession(org.osid.provisioning.Distributor distributor,
                                           org.osid.provisioning.rules.QueueProcessorEnabler[] queueProcessorEnablers) {
        this(distributor);
        putQueueProcessorEnablers(queueProcessorEnablers);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapQueueProcessorEnablerLookupSession}
     *  using a collection of queue processor enablers.
     *
     *  @param distributor the distributor
     *  @param queueProcessorEnablers a collection of queue processor enablers
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code queueProcessorEnablers} is {@code null}
     */

    public MutableMapQueueProcessorEnablerLookupSession(org.osid.provisioning.Distributor distributor,
                                           java.util.Collection<? extends org.osid.provisioning.rules.QueueProcessorEnabler> queueProcessorEnablers) {

        this(distributor);
        putQueueProcessorEnablers(queueProcessorEnablers);
        return;
    }

    
    /**
     *  Makes a {@code QueueProcessorEnabler} available in this session.
     *
     *  @param queueProcessorEnabler a queue processor enabler
     *  @throws org.osid.NullArgumentException {@code queueProcessorEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putQueueProcessorEnabler(org.osid.provisioning.rules.QueueProcessorEnabler queueProcessorEnabler) {
        super.putQueueProcessorEnabler(queueProcessorEnabler);
        return;
    }


    /**
     *  Makes an array of queue processor enablers available in this session.
     *
     *  @param queueProcessorEnablers an array of queue processor enablers
     *  @throws org.osid.NullArgumentException {@code queueProcessorEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putQueueProcessorEnablers(org.osid.provisioning.rules.QueueProcessorEnabler[] queueProcessorEnablers) {
        super.putQueueProcessorEnablers(queueProcessorEnablers);
        return;
    }


    /**
     *  Makes collection of queue processor enablers available in this session.
     *
     *  @param queueProcessorEnablers a collection of queue processor enablers
     *  @throws org.osid.NullArgumentException {@code queueProcessorEnablers{@code  is
     *          {@code null}
     */

    @Override
    public void putQueueProcessorEnablers(java.util.Collection<? extends org.osid.provisioning.rules.QueueProcessorEnabler> queueProcessorEnablers) {
        super.putQueueProcessorEnablers(queueProcessorEnablers);
        return;
    }


    /**
     *  Removes a QueueProcessorEnabler from this session.
     *
     *  @param queueProcessorEnablerId the {@code Id} of the queue processor enabler
     *  @throws org.osid.NullArgumentException {@code queueProcessorEnablerId{@code 
     *          is {@code null}
     */

    @Override
    public void removeQueueProcessorEnabler(org.osid.id.Id queueProcessorEnablerId) {
        super.removeQueueProcessorEnabler(queueProcessorEnablerId);
        return;
    }    
}
