//
// CredentialRequirementElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.requisite.credentialrequirement.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class CredentialRequirementElements
    extends net.okapia.osid.jamocha.spi.OsidRuleElements {


    /**
     *  Gets the CredentialRequirementElement Id.
     *
     *  @return the credential requirement element Id
     */

    public static org.osid.id.Id getCredentialRequirementEntityId() {
        return (makeEntityId("osid.course.requisite.CredentialRequirement"));
    }


    /**
     *  Gets the AltRequisites element Id.
     *
     *  @return the AltRequisites element Id
     */

    public static org.osid.id.Id getAltRequisites() {
        return (makeElementId("osid.course.requisite.credentialrequirement.AltRequisites"));
    }


    /**
     *  Gets the CredentialId element Id.
     *
     *  @return the CredentialId element Id
     */

    public static org.osid.id.Id getCredentialId() {
        return (makeElementId("osid.course.requisite.credentialrequirement.CredentialId"));
    }


    /**
     *  Gets the Credential element Id.
     *
     *  @return the Credential element Id
     */

    public static org.osid.id.Id getCredential() {
        return (makeElementId("osid.course.requisite.credentialrequirement.Credential"));
    }


    /**
     *  Gets the Timeframe element Id.
     *
     *  @return the Timeframe element Id
     */

    public static org.osid.id.Id getTimeframe() {
        return (makeElementId("osid.course.requisite.credentialrequirement.Timeframe"));
    }
}
