//
// AbstractSettingQuery.java
//
//     A template for making a Setting Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.setting.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for settings.
 */

public abstract class AbstractSettingQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.control.SettingQuery {

    private final java.util.Collection<org.osid.control.records.SettingQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the controller <code> Id </code> for this query. 
     *
     *  @param  controllerId the controller <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> controllerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchControllerId(org.osid.id.Id controllerId, boolean match) {
        return;
    }


    /**
     *  Clears the controller <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearControllerIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ControllerQuery </code> is available. 
     *
     *  @return <code> true </code> if a controller query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsControllerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a <code> InputQntry. </code> Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the controller query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerQuery getControllerQuery() {
        throw new org.osid.UnimplementedException("supportsControllerQuery() is false");
    }


    /**
     *  Clears the controller query terms. 
     */

    @OSID @Override
    public void clearControllerTerms() {
        return;
    }


    /**
     *  Matches on settings. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchOn(boolean match) {
        return;
    }


    /**
     *  Clears the on query terms. 
     */

    @OSID @Override
    public void clearOnTerms() {
        return;
    }


    /**
     *  Matches off settings. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchOff(boolean match) {
        return;
    }


    /**
     *  Clears the off query terms. 
     */

    @OSID @Override
    public void clearOffTerms() {
        return;
    }


    /**
     *  Matches variable percentages between the given range inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> start or end </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchVariablePercentage(java.math.BigDecimal start, 
                                        java.math.BigDecimal end, 
                                        boolean match) {
        return;
    }


    /**
     *  Matches any variable percentages. 
     *
     *  @param  match <code> true </code> to match settings with variable 
     *          percentages, <code> false </code> to match settings with no 
     *          variable percentages 
     */

    @OSID @Override
    public void matchAnyVariablePercentage(boolean match) {
        return;
    }


    /**
     *  Clears the variable percentages query terms. 
     */

    @OSID @Override
    public void clearVariablePercentageTerms() {
        return;
    }


    /**
     *  Matches variable amount between the given range inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> start or end </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchVariableAmount(java.math.BigDecimal start, 
                                    java.math.BigDecimal end, boolean match) {
        return;
    }


    /**
     *  Matches any variable amount. 
     *
     *  @param  match <code> true </code> to match settings with variable 
     *          amounts, <code> false </code> to match settings with no 
     *          variable amounts 
     */

    @OSID @Override
    public void matchAnyVariableAmount(boolean match) {
        return;
    }


    /**
     *  Clears the variable amount query terms. 
     */

    @OSID @Override
    public void clearVariableAmountTerms() {
        return;
    }


    /**
     *  Sets the state <code> Id </code> for this query. 
     *
     *  @param  stateId the state <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stateId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDiscreetStateId(org.osid.id.Id stateId, boolean match) {
        return;
    }


    /**
     *  Clears the state <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDiscreetStateIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> StateQuery </code> is available. 
     *
     *  @return <code> true </code> if a state query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDiscreetStateQuery() {
        return (false);
    }


    /**
     *  Gets the query for a <code> State. </code> Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the state query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDiscreetStateQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateQuery getDiscreetStateQuery() {
        throw new org.osid.UnimplementedException("supportsDiscreetStateQuery() is false");
    }


    /**
     *  Matches any discreet states. 
     *
     *  @param  match <code> true </code> to match settings with discreet 
     *          states, <code> false </code> to match settings with no 
     *          discreet states 
     */

    @OSID @Override
    public void matchAnyDiscreetState(boolean match) {
        return;
    }


    /**
     *  Clears the state query terms. 
     */

    @OSID @Override
    public void clearDiscreetStateTerms() {
        return;
    }


    /**
     *  Matches ramp rates between the given range inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> start or end </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRampRate(org.osid.calendaring.Duration start, 
                              org.osid.calendaring.Duration end, boolean match) {
        return;
    }


    /**
     *  Matches any ramp rate. 
     *
     *  @param  match <code> true </code> to match settings with ramp rates, 
     *          <code> false </code> to match settings with no ramp rates 
     */

    @OSID @Override
    public void matchAnyRampRate(boolean match) {
        return;
    }


    /**
     *  Clears the ramp rate query terms. 
     */

    @OSID @Override
    public void clearRampRateTerms() {
        return;
    }


    /**
     *  Sets the system <code> Id </code> for this query to match controllers 
     *  assigned to systems. 
     *
     *  @param  systemId the system <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSystemId(org.osid.id.Id systemId, boolean match) {
        return;
    }


    /**
     *  Clears the system <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSystemIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> SystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a system query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSystemQuery() {
        return (false);
    }


    /**
     *  Gets the query for a system. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the system query 
     *  @throws org.osid.UnimplementedException <code> supportsSystemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SystemQuery getSystemQuery() {
        throw new org.osid.UnimplementedException("supportsSystemQuery() is false");
    }


    /**
     *  Clears the system query terms. 
     */

    @OSID @Override
    public void clearSystemTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given setting query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a setting implementing the requested record.
     *
     *  @param settingRecordType a setting record type
     *  @return the setting query record
     *  @throws org.osid.NullArgumentException
     *          <code>settingRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(settingRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.SettingQueryRecord getSettingQueryRecord(org.osid.type.Type settingRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.SettingQueryRecord record : this.records) {
            if (record.implementsRecordType(settingRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(settingRecordType + " is not supported");
    }


    /**
     *  Adds a record to this setting query. 
     *
     *  @param settingQueryRecord setting query record
     *  @param settingRecordType setting record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addSettingQueryRecord(org.osid.control.records.SettingQueryRecord settingQueryRecord, 
                                          org.osid.type.Type settingRecordType) {

        addRecordType(settingRecordType);
        nullarg(settingQueryRecord, "setting query record");
        this.records.add(settingQueryRecord);        
        return;
    }
}
