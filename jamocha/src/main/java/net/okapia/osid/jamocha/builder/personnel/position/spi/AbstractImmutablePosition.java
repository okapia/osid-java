//
// AbstractImmutablePosition.java
//
//     Wraps a mutable Position to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.personnel.position.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Position</code> to hide modifiers. This
 *  wrapper provides an immutized Position from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying position whose state changes are visible.
 */

public abstract class AbstractImmutablePosition
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableTemporalOsidObject
    implements org.osid.personnel.Position {

    private final org.osid.personnel.Position position;


    /**
     *  Constructs a new <code>AbstractImmutablePosition</code>.
     *
     *  @param position the position to immutablize
     *  @throws org.osid.NullArgumentException <code>position</code>
     *          is <code>null</code>
     */

    protected AbstractImmutablePosition(org.osid.personnel.Position position) {
        super(position);
        this.position = position;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the organization to which this position 
     *  is assigned. 
     *
     *  @return the <code> Organization </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getOrganizationId() {
        return (this.position.getOrganizationId());
    }


    /**
     *  Gets the organization for this position. 
     *
     *  @return the <code> Organization </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.personnel.Organization getOrganization()
        throws org.osid.OperationFailedException {

        return (this.position.getOrganization());
    }


    /**
     *  Gets the title for this position. 
     *
     *  @return the position title 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getTitle() {
        return (this.position.getTitle());
    }


    /**
     *  Gets the <code> Id </code> of the job level. 
     *
     *  @return the <code> Grade </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getLevelId() {
        return (this.position.getLevelId());
    }


    /**
     *  Gets the grade level for this position. 
     *
     *  @return the <code> Grade </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.Grade getLevel()
        throws org.osid.OperationFailedException {

        return (this.position.getLevel());
    }


    /**
     *  Tests if qualifications are avilable for this position. 
     *
     *  @return <code> true </code> if qualifications are available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasQualifications() {
        return (this.position.hasQualifications());
    }


    /**
     *  Gets the qualifcation Ids. 
     *
     *  @return a list of objective <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> hasQualifications() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getQualificationIds() {
        return (this.position.getQualificationIds());
    }


    /**
     *  Gets the list of qualifications required for this position. 
     *
     *  @return the <code> Objectives </code> 
     *  @throws org.osid.IllegalStateException <code> hasQualifications() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveList getQualifications()
        throws org.osid.OperationFailedException {

        return (this.position.getQualifications());
    }


    /**
     *  Gets the target number of <code> Appointments </code> desired for this 
     *  <code> Position. </code> 
     *
     *  @return the number of appointments 
     */

    @OSID @Override
    public long getTargetAppointments() {
        return (this.position.getTargetAppointments());
    }


    /**
     *  Gets the required percentage commitment (0-100). 
     *
     *  @return the required percentage commitment 
     */

    @OSID @Override
    public long getRequiredCommitment() {
        return (this.position.getRequiredCommitment());
    }


    /**
     *  Tests if a salary range is available. 
     *
     *  @return <code> true </code> if a salary range is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasSalaryRange() {
        return (this.position.hasSalaryRange());
    }


    /**
     *  Gets the low end of the salary range. 
     *
     *  @return the low slaary 
     *  @throws org.osid.IllegalStateException <code> hasSalaryRange() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.Currency getLowSalaryRange() {
        return (this.position.getLowSalaryRange());
    }


    /**
     *  Gets the midpoint of the salary range. 
     *
     *  @return the midpoint slaary 
     *  @throws org.osid.IllegalStateException <code> hasSalaryRange() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.Currency getMidpointSalaryRange() {
        return (this.position.getMidpointSalaryRange());
    }


    /**
     *  Gets the high end of the salary range. 
     *
     *  @return the high slaary 
     *  @throws org.osid.IllegalStateException <code> hasSalaryRange() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.Currency getHighSalaryRange() {
        return (this.position.getHighSalaryRange());
    }


    /**
     *  Gets the frequency of compensation. 
     *
     *  @return the frequency 
     *  @throws org.osid.IllegalStateException <code> hasSalaryRange() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getCompensationFrequency() {
        return (this.position.getCompensationFrequency());
    }


    /**
     *  Tests if this position is FLSA exempt. 
     *
     *  @return <code> true </code> if this position is exempt, <code> false 
     *          </code> is non-exempt 
     */

    @OSID @Override
    public boolean isExempt() {
        return (this.position.isExempt());
    }


    /**
     *  Tests if this position is eligible for benefits. 
     *
     *  @return <code> true </code> if this position has benefits, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasBenefits() {
        return (this.position.hasBenefits());
    }


    /**
     *  Gets the benefits Type assoicated with this position. 
     *
     *  @return the benefits type 
     *  @throws org.osid.IllegalStateException <code> hasBenefits() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.type.Type getBenefitsType() {
        return (this.position.getBenefitsType());
    }


    /**
     *  Gets the record corresponding to the given <code> Position </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> positionRecordType 
     *  </code> may be the <code> Type </code> returned in <code> 
     *  getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(positionRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  positionRecordType the type of position record to retrieve 
     *  @return the position record 
     *  @throws org.osid.NullArgumentException <code> positionRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(positionRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.personnel.records.PositionRecord getPositionRecord(org.osid.type.Type positionRecordType)
        throws org.osid.OperationFailedException {

        return (this.position.getPositionRecord(positionRecordType));
    }
}

