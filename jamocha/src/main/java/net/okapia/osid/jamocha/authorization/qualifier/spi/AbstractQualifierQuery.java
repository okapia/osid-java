//
// AbstractQualifierQuery.java
//
//     A template for making a Qualifier Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authorization.qualifier.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for qualifiers.
 */

public abstract class AbstractQualifierQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.authorization.QualifierQuery {

    private final java.util.Collection<org.osid.authorization.records.QualifierQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the qualifier hierarchy <code> Id </code> for this query. 
     *
     *  @param  qualifierHierarchyId a hierarchy <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> qualifierHierarchyId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchQualifierHierarchyId(org.osid.id.Id qualifierHierarchyId, 
                                          boolean match) {
        return;
    }


    /**
     *  Clears the qualifier hierarchy <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearQualifierHierarchyIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> HierarchyQuery </code> is available. 
     *
     *  @return <code> true </code> if a qualifier hierarchy query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQualifierHierarchyQuery() {
        return (false);
    }


    /**
     *  Gets the query for a qualifier hierarchy. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the qualifier hierarchy query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierHierarchyQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyQuery getQualifierHierarchyQuery() {
        throw new org.osid.UnimplementedException("supportsQualifierHierarchyQuery() is false");
    }


    /**
     *  Clears the qualifier hierarchy query terms. 
     */

    @OSID @Override
    public void clearQualifierHierarchyTerms() {
        return;
    }


    /**
     *  Sets the authorization <code> Id </code> for this query. 
     *
     *  @param  authorizationId an authorization <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> authorizationId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchAuthorizationId(org.osid.id.Id authorizationId, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the authorization <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAuthorizationIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AuthorizationQuery </code> is available. 
     *
     *  @return <code> true </code> if an authorization query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizationQuery() {
        return (false);
    }


    /**
     *  Gets the query for an authorization. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the authorization query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationQuery getAuthorizationQuery() {
        throw new org.osid.UnimplementedException("supportsAuthorizationQuery() is false");
    }


    /**
     *  Matches qualifiers that have any authorization mapping. 
     *
     *  @param  match <code> true </code> to match qualifiers with any 
     *          authorization mapping, <code> false </code> to match 
     *          qualifiers with no authorization mapping 
     */

    @OSID @Override
    public void matchAnyAuthorization(boolean match) {
        return;
    }


    /**
     *  Clears the authorization query terms. 
     */

    @OSID @Override
    public void clearAuthorizationTerms() {
        return;
    }


    /**
     *  Sets the qualifier <code> Id </code> for this query to match 
     *  qualifiers that have the specified qualifier as an ancestor. 
     *
     *  @param  qualifierId a qualifier <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> qualifierId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorQualifierId(org.osid.id.Id qualifierId, 
                                         boolean match) {
        return;
    }


    /**
     *  Clears the ancestor qualifier <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorQualifierIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> QualifierQuery </code> is available. 
     *
     *  @return <code> true </code> if a qualifier query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorQualifierQuery() {
        return (false);
    }


    /**
     *  Gets the query for a qualifier. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the qualifier query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorQualifierQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierQuery getAncestorQualifierQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorQualifierQuery() is false");
    }


    /**
     *  Matches qualifiers that have any ancestor. 
     *
     *  @param  match <code> true </code> to match qualifiers with any 
     *          ancestor, <code> false </code> to match root qualifiers 
     */

    @OSID @Override
    public void matchAnyAncestorQualifier(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor qualifier query terms. 
     */

    @OSID @Override
    public void clearAncestorQualifierTerms() {
        return;
    }


    /**
     *  Sets the qualifier <code> Id </code> for this query to match 
     *  qualifiers that have the specified qualifier as a descendant. 
     *
     *  @param  qualifierId a qualifier <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> qualifierId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantQualifierId(org.osid.id.Id qualifierId, 
                                           boolean match) {
        return;
    }


    /**
     *  Clears the descendant qualifier <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantQualifierIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> QualifierQuery </code> is available. 
     *
     *  @return <code> true </code> if a qualifier query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantQualifierQuery() {
        return (false);
    }


    /**
     *  Gets the query for a qualifier. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the qualifier query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantQualifierQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierQuery getDescendantQualifierQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantQualifierQuery() is false");
    }


    /**
     *  Matches qualifiers that have any ancestor. 
     *
     *  @param  match <code> true </code> to match qualifiers with any 
     *          ancestor, <code> false </code> to match leaf qualifiers 
     */

    @OSID @Override
    public void matchAnyDescendantQualifier(boolean match) {
        return;
    }


    /**
     *  Clears the descendant qualifier query terms. 
     */

    @OSID @Override
    public void clearDescendantQualifierTerms() {
        return;
    }


    /**
     *  Sets the vault <code> Id </code> for this query. 
     *
     *  @param  vaultId a vault <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchVaultId(org.osid.id.Id vaultId, boolean match) {
        return;
    }


    /**
     *  Clears the vault <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearVaultIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> VaultQuery </code> is available. 
     *
     *  @return <code> true </code> if a vault query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVaultQuery() {
        return (false);
    }


    /**
     *  Gets the query for a vault. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the vault query 
     *  @throws org.osid.UnimplementedException <code> supportsVaultQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.VaultQuery getVaultQuery() {
        throw new org.osid.UnimplementedException("supportsVaultQuery() is false");
    }


    /**
     *  Clears the vault query terms. 
     */

    @OSID @Override
    public void clearVaultTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given qualifier query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a qualifier implementing the requested record.
     *
     *  @param qualifierRecordType a qualifier record type
     *  @return the qualifier query record
     *  @throws org.osid.NullArgumentException
     *          <code>qualifierRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(qualifierRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.authorization.records.QualifierQueryRecord getQualifierQueryRecord(org.osid.type.Type qualifierRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authorization.records.QualifierQueryRecord record : this.records) {
            if (record.implementsRecordType(qualifierRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(qualifierRecordType + " is not supported");
    }


    /**
     *  Adds a record to this qualifier query. 
     *
     *  @param qualifierQueryRecord qualifier query record
     *  @param qualifierRecordType qualifier record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addQualifierQueryRecord(org.osid.authorization.records.QualifierQueryRecord qualifierQueryRecord, 
                                          org.osid.type.Type qualifierRecordType) {

        addRecordType(qualifierRecordType);
        nullarg(qualifierQueryRecord, "qualifier query record");
        this.records.add(qualifierQueryRecord);        
        return;
    }
}
