//
// AbstractAssemblyDictionaryQuery.java
//
//     A DictionaryQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.dictionary.dictionary.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A DictionaryQuery that stores terms.
 */

public abstract class AbstractAssemblyDictionaryQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.dictionary.DictionaryQuery,
               org.osid.dictionary.DictionaryQueryInspector,
               org.osid.dictionary.DictionarySearchOrder {

    private final java.util.Collection<org.osid.dictionary.records.DictionaryQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.dictionary.records.DictionaryQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.dictionary.records.DictionarySearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyDictionaryQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyDictionaryQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the entry <code> Id. </code> 
     *
     *  @param  entryId an entry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> entryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEntryId(org.osid.id.Id entryId, boolean match) {
        getAssembler().addIdTerm(getEntryIdColumn(), entryId, match);
        return;
    }


    /**
     *  Clears the entry <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearEntryIdTerms() {
        getAssembler().clearTerms(getEntryIdColumn());
        return;
    }


    /**
     *  Gets the entry <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEntryIdTerms() {
        return (getAssembler().getIdTerms(getEntryIdColumn()));
    }


    /**
     *  Gets the EntryId column name.
     *
     * @return the column name
     */

    protected String getEntryIdColumn() {
        return ("entry_id");
    }


    /**
     *  Tests if an <code> EntryQuery </code> is available. 
     *
     *  @return <code> true </code> if an entry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for an entry. Multiple retrievale produce a nested 
     *  boolean <code> OR </code> term. 
     *
     *  @return the entry query 
     *  @throws org.osid.UnimplementedException <code> supportsEntryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntryQuery getEntryQuery() {
        throw new org.osid.UnimplementedException("supportsEntryQuery() is false");
    }


    /**
     *  Matches dictionaries with any entry. 
     *
     *  @param  match <code> true </code> to match dictionaries with any 
     *          entry, <code> false </code> to match dictionaries with no 
     *          entry 
     */

    @OSID @Override
    public void matchAnyEntry(boolean match) {
        getAssembler().addIdWildcardTerm(getEntryColumn(), match);
        return;
    }


    /**
     *  Clears the entry terms. 
     */

    @OSID @Override
    public void clearEntryTerms() {
        getAssembler().clearTerms(getEntryColumn());
        return;
    }


    /**
     *  Gets the entry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.dictionary.EntryQueryInspector[] getEntryTerms() {
        return (new org.osid.dictionary.EntryQueryInspector[0]);
    }


    /**
     *  Gets the Entry column name.
     *
     * @return the column name
     */

    protected String getEntryColumn() {
        return ("entry");
    }


    /**
     *  Sets the dictionary <code> Id </code> for to match dictionaries in 
     *  which the specified dictionary is an acestor. 
     *
     *  @param  dictionaryId a dictionary <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> dictionaryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorDictionaryId(org.osid.id.Id dictionaryId, 
                                          boolean match) {
        getAssembler().addIdTerm(getAncestorDictionaryIdColumn(), dictionaryId, match);
        return;
    }


    /**
     *  Clears the ancestor dictionary <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorDictionaryIdTerms() {
        getAssembler().clearTerms(getAncestorDictionaryIdColumn());
        return;
    }


    /**
     *  Gets the ancestor dictionary <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorDictionaryIdTerms() {
        return (getAssembler().getIdTerms(getAncestorDictionaryIdColumn()));
    }


    /**
     *  Gets the AncestorDictionaryId column name.
     *
     * @return the column name
     */

    protected String getAncestorDictionaryIdColumn() {
        return ("ancestor_dictionary_id");
    }


    /**
     *  Tests if a <code> DictionaryQuery </code> is available. 
     *
     *  @return <code> true </code> if a dictionary query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorDictionaryQuery() {
        return (false);
    }


    /**
     *  Gets the query for an ancestor dictionary. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the dictionary query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorDictionaryQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryQuery getAncestorDictionaryQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorDictionaryQuery() is false");
    }


    /**
     *  Matches a dictionary that has any ancestor. 
     *
     *  @param  match <code> true </code> to match dictionaries with any 
     *          ancestor dictionaries, <code> false </code> to match root 
     *          dictionaries 
     */

    @OSID @Override
    public void matchAnyAncestorDictionary(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorDictionaryColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor dictionary terms. 
     */

    @OSID @Override
    public void clearAncestorDictionaryTerms() {
        getAssembler().clearTerms(getAncestorDictionaryColumn());
        return;
    }


    /**
     *  Gets the ancestor dictionary query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryQueryInspector[] getAncestorDictionaryTerms() {
        return (new org.osid.dictionary.DictionaryQueryInspector[0]);
    }


    /**
     *  Gets the AncestorDictionary column name.
     *
     * @return the column name
     */

    protected String getAncestorDictionaryColumn() {
        return ("ancestor_dictionary");
    }


    /**
     *  Sets the dictionary <code> Id </code> for to match dictionaries in 
     *  which the specified dictionary is a descendant. 
     *
     *  @param  dictionaryId a dictionary <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> dictionaryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantDictionaryId(org.osid.id.Id dictionaryId, 
                                            boolean match) {
        getAssembler().addIdTerm(getDescendantDictionaryIdColumn(), dictionaryId, match);
        return;
    }


    /**
     *  Clears the dictionary <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantDictionaryIdTerms() {
        getAssembler().clearTerms(getDescendantDictionaryIdColumn());
        return;
    }


    /**
     *  Gets the descendant dictionary <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantDictionaryIdTerms() {
        return (getAssembler().getIdTerms(getDescendantDictionaryIdColumn()));
    }


    /**
     *  Gets the DescendantDictionaryId column name.
     *
     * @return the column name
     */

    protected String getDescendantDictionaryIdColumn() {
        return ("descendant_dictionary_id");
    }


    /**
     *  Tests if a <code> DictionaryQuery </code> is available. 
     *
     *  @return <code> true </code> if a dictionary query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantDictionaryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a descendant dictioonary. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the dictionary query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantDictionaryQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryQuery getDescendantDictionaryQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantDictionaryQuery() is false");
    }


    /**
     *  Matches a dictionary that has any descendant. 
     *
     *  @param  match <code> true </code> to match dictionaries with any 
     *          descendant dictionaries, <code> false </code> to match leaf 
     *          dictionaries 
     */

    @OSID @Override
    public void matchAnyDescendantDictionary(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantDictionaryColumn(), match);
        return;
    }


    /**
     *  Clears the dictionary terms. 
     */

    @OSID @Override
    public void clearDescendantDictionaryTerms() {
        getAssembler().clearTerms(getDescendantDictionaryColumn());
        return;
    }


    /**
     *  Gets the descendant dictionary query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryQueryInspector[] getDescendantDictionaryTerms() {
        return (new org.osid.dictionary.DictionaryQueryInspector[0]);
    }


    /**
     *  Gets the DescendantDictionary column name.
     *
     * @return the column name
     */

    protected String getDescendantDictionaryColumn() {
        return ("descendant_dictionary");
    }


    /**
     *  Tests if this dictionary supports the given record
     *  <code>Type</code>.
     *
     *  @param  dictionaryRecordType a dictionary record type 
     *  @return <code>true</code> if the dictionaryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>dictionaryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type dictionaryRecordType) {
        for (org.osid.dictionary.records.DictionaryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(dictionaryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  dictionaryRecordType the dictionary record type 
     *  @return the dictionary query record 
     *  @throws org.osid.NullArgumentException
     *          <code>dictionaryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(dictionaryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.dictionary.records.DictionaryQueryRecord getDictionaryQueryRecord(org.osid.type.Type dictionaryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.dictionary.records.DictionaryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(dictionaryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(dictionaryRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  dictionaryRecordType the dictionary record type 
     *  @return the dictionary query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>dictionaryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(dictionaryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.dictionary.records.DictionaryQueryInspectorRecord getDictionaryQueryInspectorRecord(org.osid.type.Type dictionaryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.dictionary.records.DictionaryQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(dictionaryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(dictionaryRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param dictionaryRecordType the dictionary record type
     *  @return the dictionary search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>dictionaryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(dictionaryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.dictionary.records.DictionarySearchOrderRecord getDictionarySearchOrderRecord(org.osid.type.Type dictionaryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.dictionary.records.DictionarySearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(dictionaryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(dictionaryRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this dictionary. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param dictionaryQueryRecord the dictionary query record
     *  @param dictionaryQueryInspectorRecord the dictionary query inspector
     *         record
     *  @param dictionarySearchOrderRecord the dictionary search order record
     *  @param dictionaryRecordType dictionary record type
     *  @throws org.osid.NullArgumentException
     *          <code>dictionaryQueryRecord</code>,
     *          <code>dictionaryQueryInspectorRecord</code>,
     *          <code>dictionarySearchOrderRecord</code> or
     *          <code>dictionaryRecordTypedictionary</code> is
     *          <code>null</code>
     */
            
    protected void addDictionaryRecords(org.osid.dictionary.records.DictionaryQueryRecord dictionaryQueryRecord, 
                                      org.osid.dictionary.records.DictionaryQueryInspectorRecord dictionaryQueryInspectorRecord, 
                                      org.osid.dictionary.records.DictionarySearchOrderRecord dictionarySearchOrderRecord, 
                                      org.osid.type.Type dictionaryRecordType) {

        addRecordType(dictionaryRecordType);

        nullarg(dictionaryQueryRecord, "dictionary query record");
        nullarg(dictionaryQueryInspectorRecord, "dictionary query inspector record");
        nullarg(dictionarySearchOrderRecord, "dictionary search odrer record");

        this.queryRecords.add(dictionaryQueryRecord);
        this.queryInspectorRecords.add(dictionaryQueryInspectorRecord);
        this.searchOrderRecords.add(dictionarySearchOrderRecord);
        
        return;
    }
}
