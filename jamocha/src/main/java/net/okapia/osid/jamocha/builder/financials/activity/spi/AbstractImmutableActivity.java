//
// AbstractImmutableActivity.java
//
//     Wraps a mutable Activity to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.financials.activity.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Activity</code> to hide modifiers. This
 *  wrapper provides an immutized Activity from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying activity whose state changes are visible.
 */

public abstract class AbstractImmutableActivity
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableTemporalOsidObject
    implements org.osid.financials.Activity {

    private final org.osid.financials.Activity activity;


    /**
     *  Constructs a new <code>AbstractImmutableActivity</code>.
     *
     *  @param activity the activity to immutablize
     *  @throws org.osid.NullArgumentException <code>activity</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableActivity(org.osid.financials.Activity activity) {
        super(activity);
        this.activity = activity;
        return;
    }


    /**
     *  Gets the organization <code> Id </code> associated with this activity. 
     *
     *  @return the organization <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getOrganizationId() {
        return (this.activity.getOrganizationId());
    }


    /**
     *  Gets the organization associated with this activity. 
     *
     *  @return the organization 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getOrganization()
        throws org.osid.OperationFailedException {

        return (this.activity.getOrganization());
    }


    /**
     *  Gets the resource <code> Id </code> supervising this activity account. 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSupervisorId() {
        return (this.activity.getSupervisorId());
    }


    /**
     *  Gets the resource supervising this activity account. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getSupervisor()
        throws org.osid.OperationFailedException {

        return (this.activity.getSupervisor());
    }


    /**
     *  Gets the code for this activity account. 
     *
     *  @return the activity code 
     */

    @OSID @Override
    public String getCode() {
        return (this.activity.getCode());
    }


    /**
     *  Gets the activity record corresponding to the given <code> Activity 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> 
     *  activityRecordType </code> may be the <code> Type </code> returned in 
     *  <code> getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(activityRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  activityRecordType the type of activity record to retrieve 
     *  @return the activity record 
     *  @throws org.osid.NullArgumentException <code> activityRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(activityRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.records.ActivityRecord getActivityRecord(org.osid.type.Type activityRecordType)
        throws org.osid.OperationFailedException {

        return (this.activity.getActivityRecord(activityRecordType));
    }
}

