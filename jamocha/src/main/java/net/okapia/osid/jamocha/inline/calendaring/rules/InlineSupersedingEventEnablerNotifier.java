//
// InlineSupersedingEventEnablerNotifier.java
//
//     A callback interface for performing superseding event enabler
//     notifications.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.calendaring.rules.spi;


/**
 *  A callback interface for performing superseding event enabler
 *  notifications.
 */

public interface InlineSupersedingEventEnablerNotifier {



    /**
     *  Notifies the creation of a new superseding event enabler.
     *
     *  @param supersedingEventEnablerId the {@code Id} of the new 
     *         superseding event enabler
     *  @throws org.osid.NullArgumentException {@code [objectId]}
     *          is {@code null}
     */

    public void newSupersedingEventEnabler(org.osid.id.Id supersedingEventEnablerId);


    /**
     *  Notifies the change of an updated superseding event enabler.
     *
     *  @param supersedingEventEnablerId the {@code Id} of the changed
     *         superseding event enabler
     *  @throws org.osid.NullArgumentException {@code [objectId]}
     *          is {@code null}
     */
      
    public void changedSupersedingEventEnabler(org.osid.id.Id supersedingEventEnablerId);


    /**
     *  Notifies the deletion of an superseding event enabler.
     *
     *  @param supersedingEventEnablerId the {@code Id} of the deleted
     *         superseding event enabler
     *  @throws org.osid.NullArgumentException {@code [objectId]} is
     *          {@code null}
     */

    public void deletedSupersedingEventEnabler(org.osid.id.Id supersedingEventEnablerId);

}
