//
// AbstractTransportProxyManager.java
//
//     An adapter for a TransportProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.transport.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a TransportProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterTransportProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.transport.TransportProxyManager>
    implements org.osid.transport.TransportProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterTransportProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterTransportProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterTransportProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterTransportProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any transport endpoint federation is exposed. Federation is 
     *  exposed when a specific endpoint may be used. Federation is not 
     *  exposed when a set of endpoints appears as a single endpoint. 
     *
     *  @return <code> true </code> if federation is visible <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if outbound stream transport is supported. 
     *
     *  @return <code> true </code> if outbound stream transport is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOutboundStream() {
        return (getAdapteeManager().supportsOutboundStream());
    }


    /**
     *  Tests if inbound stream transport is supported. 
     *
     *  @return <code> true </code> if incoming stream transport is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInboundStream() {
        return (getAdapteeManager().supportsInboundStream());
    }


    /**
     *  Tests if outbound message transport is supported. 
     *
     *  @return <code> true </code> if outbound message transport is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOutboundMessage() {
        return (getAdapteeManager().supportsOutboundMessage());
    }


    /**
     *  Tests if inbound message transport is supported. 
     *
     *  @return <code> true </code> if incoming message transport is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInboundMessage() {
        return (getAdapteeManager().supportsInboundMessage());
    }


    /**
     *  Tests if endpoint lookup is supported. 
     *
     *  @return <code> true </code> if endpoint lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEndpointLookup() {
        return (getAdapteeManager().supportsEndpointLookup());
    }


    /**
     *  Gets a list of supported endpoint record types. 
     *
     *  @return a list of supported endpoint record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEndpointRecordTypes() {
        return (getAdapteeManager().getEndpointRecordTypes());
    }


    /**
     *  Tests if an endpoint record type is supported. 
     *
     *  @param  endpointRecordType an endpoint record type 
     *  @return <code> true </code> if the endpoint record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> endpointRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEndpointRecordType(org.osid.type.Type endpointRecordType) {
        return (getAdapteeManager().supportsEndpointRecordType(endpointRecordType));
    }


    /**
     *  Gets a list of supported request record types. 
     *
     *  @return a list of supported request record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRequestRecordTypes() {
        return (getAdapteeManager().getRequestRecordTypes());
    }


    /**
     *  Tests if a request record type is supported. 
     *
     *  @param  requestRecordType a request record type 
     *  @return <code> true </code> if the request record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> requestRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRequestRecordType(org.osid.type.Type requestRecordType) {
        return (getAdapteeManager().supportsRequestRecordType(requestRecordType));
    }


    /**
     *  Gets a list of supported response record types. 
     *
     *  @return a list of supported response record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getResponseRecordTypes() {
        return (getAdapteeManager().getResponseRecordTypes());
    }


    /**
     *  Tests if a response record type is supported. 
     *
     *  @param  responseRecordType a response record type 
     *  @return <code> true </code> if the response record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> responseRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsResponseRecordType(org.osid.type.Type responseRecordType) {
        return (getAdapteeManager().supportsResponseRecordType(responseRecordType));
    }


    /**
     *  Gets a service for outbound stream transport. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OutboundStreamSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOutboundStream() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.transport.OutboundStreamSession getOutboundStreamSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOutboundStreamSession(proxy));
    }


    /**
     *  Gets a service for outbound stream transport using a specified <code> 
     *  Endpoint. </code> 
     *
     *  @param  endpointId a transport endpoint 
     *  @param  proxy a proxy 
     *  @return an <code> OutboundStreamSession </code> 
     *  @throws org.osid.NotFoundException <code> endpointId </code> or <code> 
     *          proxy </code> is not found 
     *  @throws org.osid.NullArgumentException <code> endpointId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOutboundStream() </code> or <code> 
     *          supportsVisibleFederationI() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.transport.OutboundStreamSession getOutboundStreamSessionForEndpoint(org.osid.id.Id endpointId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOutboundStreamSessionForEndpoint(endpointId, proxy));
    }


    /**
     *  Gets a service for inbound stream transport. 
     *
     *  @param  streamReceiver a stream receiver 
     *  @param  proxy a proxy 
     *  @return an <code> InboundStreamSession </code> 
     *  @throws org.osid.NullArgumentException <code> streamReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInboundStream() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.transport.InboundStreamSession getInboundStreamSession(org.osid.transport.StreamReceiver streamReceiver, 
                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInboundStreamSession(streamReceiver, proxy));
    }


    /**
     *  Gets a service for inbound stream transport using a specified <code> 
     *  Endpoint. </code> 
     *
     *  @param  streamReceiver a stream receiver 
     *  @param  endpointId a transport endpoint 
     *  @param  proxy a proxy 
     *  @return an <code> InboundStreamSession </code> 
     *  @throws org.osid.NotFoundException <code> endpointId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> streamReceiver, 
     *          endpointId </code> or <code> porxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsInboundStream() 
     *          </code> or <code> supportsVisibleFederationI() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.transport.InboundStreamSession getInboundStreamSessionForEndpoint(org.osid.transport.StreamReceiver streamReceiver, 
                                                                                      org.osid.id.Id endpointId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInboundStreamSessionForEndpoint(streamReceiver, endpointId, proxy));
    }


    /**
     *  Gets a service for outbound message transport. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OutboundMessageSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOutboundMessage() is false </code> 
     */

    @OSID @Override
    public org.osid.transport.OutboundMessageSession getOutboundMessageSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOutboundMessageSession(proxy));
    }


    /**
     *  Gets a service for outbound message transport using a specified 
     *  Endpoint. 
     *
     *  @param  endpointId a transport endpoint 
     *  @param  proxy a proxy 
     *  @return an <code> OutboundMessageSession </code> 
     *  @throws org.osid.NotFoundException <code> endpointId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> endpointId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOutboundMessage() or supportsVisibleFederationI() is 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.transport.OutboundMessageSession getOutboundMessageSessionForEndpoint(org.osid.id.Id endpointId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOutboundMessageSessionForEndpoint(endpointId, proxy));
    }


    /**
     *  Gets a service for inbound message transport. 
     *
     *  @param  messageReceiver a message receiver 
     *  @param  proxy a proxy 
     *  @return an <code> InboundMessageSession </code> 
     *  @throws org.osid.NullArgumentException <code> messageReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInboundMessage() is false </code> 
     */

    @OSID @Override
    public org.osid.transport.InboundMessageSession getInboundMessageSession(org.osid.transport.MessageReceiver messageReceiver, 
                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInboundMessageSession(messageReceiver, proxy));
    }


    /**
     *  Gets a service for inbound message transport using a specified 
     *  Endpoint. 
     *
     *  @param  messageReceiver a message receiver 
     *  @param  endpointId a transport endpoint 
     *  @param  proxy a proxy 
     *  @return an <code> InboundMessageSession </code> 
     *  @throws org.osid.NotFoundException <code> endpointId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> messageReceiver, 
     *          endpointId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInboundMessage() or supportsVisibleFederationI() is 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.transport.InboundMessageSession getInboundMessageSessionForEndpoint(org.osid.transport.MessageReceiver messageReceiver, 
                                                                                        org.osid.id.Id endpointId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInboundMessageSessionForEndpoint(messageReceiver, endpointId, proxy));
    }


    /**
     *  Gets the endpoint lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EndpointLookupSesson </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInbound() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.transport.EndpointLookupSession getEndpointLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEndpointLookupSession(proxy));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
