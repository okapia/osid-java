//
// ProvisionMiter.java
//
//     Defines a Provision miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.provisioning.provision;


/**
 *  Defines a <code>Provision</code> miter for use with the builders.
 */

public interface ProvisionMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRelationshipMiter,
            org.osid.provisioning.Provision {


    /**
     *  Sets the broker.
     *
     *  @param broker a broker
     *  @throws org.osid.NullArgumentException <code>broker</code> is
     *          <code>null</code>
     */

    public void setBroker(org.osid.provisioning.Broker broker);


    /**
     *  Sets the provisionable.
     *
     *  @param provisionable a provisionable
     *  @throws org.osid.NullArgumentException
     *          <code>provisionable</code> is <code>null</code>
     */

    public void setProvisionable(org.osid.provisioning.Provisionable provisionable);


    /**
     *  Sets the recipient.
     *
     *  @param resource a recipient
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    public void setRecipient(org.osid.resource.Resource resource);


    /**
     *  Sets the request.
     *
     *  @param request a request
     *  @throws org.osid.NullArgumentException <code>request</code> is
     *          <code>null</code>
     */

    public void setRequest(org.osid.provisioning.Request request);


    /**
     *  Sets the provision date.
     *
     *  @param date a provision date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setProvisionDate(org.osid.calendaring.DateTime date);


    /**
     *  Sets the leased flag.
     *
     *  @param leased <code>true</code> if this is a lease,
     *         <code>false</code> if the provision is permanent
     */

    public void setLeased(boolean leased);


    /**
     *  Sets the must return flag.
     *
     *  @param mustReturn <code>true</code> if this is must be
     *         returned, <code>false</code> otherwise
     */

    public void setMustReturn(boolean mustReturn);


    /**
     *  Sets the due date.
     *
     *  @param date a due date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setDueDate(org.osid.calendaring.DateTime date);


    /**
     *  Sets the cost.
     *
     *  @param cost a cost
     *  @throws org.osid.NullArgumentException <code>cost</code> is
     *          <code>null</code>
     */

    public void setCost(org.osid.financials.Currency cost);


    /**
     *  Sets the rate amount.
     *
     *  @param amount a rate amount
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    public void setRateAmount(org.osid.financials.Currency amount);


    /**
     *  Sets the rate period.
     *
     *  @param period a rate period
     *  @throws org.osid.NullArgumentException <code>period</code> is
     *          <code>null</code>
     */

    public void setRatePeriod(org.osid.calendaring.Duration period);


    /**
     *  Sets the provision return.
     *
     *  @param provisionReturn a provision return
     *  @throws org.osid.NullArgumentException
     *          <code>provisionReturn</code> is <code>null</code>
     */

    public void setProvisionReturn(org.osid.provisioning.ProvisionReturn provisionReturn);


    /**
     *  Adds a Provision record.
     *
     *  @param record a provision record
     *  @param recordType the type of provision record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addProvisionRecord(org.osid.provisioning.records.ProvisionRecord record, org.osid.type.Type recordType);
}       


