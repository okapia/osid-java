//
// AbstractOntologyProxyManager.java
//
//     An adapter for a OntologyProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.ontology.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a OntologyProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterOntologyProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.ontology.OntologyProxyManager>
    implements org.osid.ontology.OntologyProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterOntologyProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterOntologyProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterOntologyProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterOntologyProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if a subject lookup service is supported. a subject lookup 
     *  service defines methods to access subjects. 
     *
     *  @return true if subject lookup is supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsSubjectLookup() {
        return (getAdapteeManager().supportsSubjectLookup());
    }


    /**
     *  Tests if a subject query service is supported. 
     *
     *  @return <code> true </code> if subject query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubjectQuery() {
        return (getAdapteeManager().supportsSubjectQuery());
    }


    /**
     *  Tests if a subject search service is supported. 
     *
     *  @return <code> true </code> if subject search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubjectSearch() {
        return (getAdapteeManager().supportsSubjectSearch());
    }


    /**
     *  Tests if a subject administrative service is supported. 
     *
     *  @return <code> true </code> if subject admin is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubjectAdmin() {
        return (getAdapteeManager().supportsSubjectAdmin());
    }


    /**
     *  Tests if subject notification is supported. Messages may be sent when 
     *  subjects are created, modified, or deleted. 
     *
     *  @return <code> true </code> if subject notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubjectNotification() {
        return (getAdapteeManager().supportsSubjectNotification());
    }


    /**
     *  Tests if a subject hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if a subject hierarchy traversal is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubjectHierarchy() {
        return (getAdapteeManager().supportsSubjectHierarchy());
    }


    /**
     *  Tests if subject hierarchy design is supported. 
     *
     *  @return <code> true </code> if a subject hierarchy design is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubjectHierarchyDesign() {
        return (getAdapteeManager().supportsSubjectHierarchyDesign());
    }


    /**
     *  Tests if a subject to ontology lookup session is available. 
     *
     *  @return <code> true </code> if subject ontology lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubjectOntology() {
        return (getAdapteeManager().supportsSubjectOntology());
    }


    /**
     *  Tests if a subject to ontology assignment session is available. 
     *
     *  @return <code> true </code> if subject ontology assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubjectOntologyAssignment() {
        return (getAdapteeManager().supportsSubjectOntologyAssignment());
    }


    /**
     *  Tests if a subject smart ontology session is available. 
     *
     *  @return <code> true </code> if subject smart ontology session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubjectSmartOntology() {
        return (getAdapteeManager().supportsSubjectSmartOntology());
    }


    /**
     *  Tests if a subject relevancy lookup service is supported. 
     *
     *  @return <code> true </code> if relevancy lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelevancyLookup() {
        return (getAdapteeManager().supportsRelevancyLookup());
    }


    /**
     *  Tests if a relevancy query service is supported. 
     *
     *  @return <code> true </code> if relevancy query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelevancyQuery() {
        return (getAdapteeManager().supportsRelevancyQuery());
    }


    /**
     *  Tests if a relevancy search service is supported. 
     *
     *  @return <code> true </code> if relevancy search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelevancySearch() {
        return (getAdapteeManager().supportsRelevancySearch());
    }


    /**
     *  Tests if a relevancy administrative service is supported. 
     *
     *  @return <code> true </code> if relevancy admin is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelevancyAdmin() {
        return (getAdapteeManager().supportsRelevancyAdmin());
    }


    /**
     *  Tests if relevancy notification is supported. Messages may be sent 
     *  when subject relevancies are created, modified, or deleted. 
     *
     *  @return <code> true </code> if relevancy notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelevancyNotification() {
        return (getAdapteeManager().supportsRelevancyNotification());
    }


    /**
     *  Tests if an ontology lookup service is supported. 
     *
     *  @return <code> true </code> if ontology lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOntologyLookup() {
        return (getAdapteeManager().supportsOntologyLookup());
    }


    /**
     *  Tests if a relevancy to ontology lookup session is available. 
     *
     *  @return <code> true </code> if relevancy ontology lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelevancyOntology() {
        return (getAdapteeManager().supportsRelevancyOntology());
    }


    /**
     *  Tests if a relevancy to ontology assignment session is available. 
     *
     *  @return <code> true </code> if relevancy ontology assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelevancyOntologyAssignment() {
        return (getAdapteeManager().supportsRelevancyOntologyAssignment());
    }


    /**
     *  Tests if a relevancy smart ontology session is available. 
     *
     *  @return <code> true </code> if relevancy smart ontology session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelevancySmartOntology() {
        return (getAdapteeManager().supportsRelevancySmartOntology());
    }


    /**
     *  Tests if an ontology query service is supported. 
     *
     *  @return <code> true </code> if ontology query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOntologyQuery() {
        return (getAdapteeManager().supportsOntologyQuery());
    }


    /**
     *  Tests if an ontology search service is supported. 
     *
     *  @return <code> true </code> if ontology search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOntologySearch() {
        return (getAdapteeManager().supportsOntologySearch());
    }


    /**
     *  Tests if an ontology administrative service is supported. 
     *
     *  @return <code> true </code> if ontology admin is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOntologyAdmin() {
        return (getAdapteeManager().supportsOntologyAdmin());
    }


    /**
     *  Tests if ontology notification is supported. Messages may be sent when 
     *  ontologies are created, modified, or deleted. 
     *
     *  @return <code> true </code> if ontology notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOntologyNotification() {
        return (getAdapteeManager().supportsOntologyNotification());
    }


    /**
     *  Tests if an ontology hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if an ontology hierarchy traversal is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOntologyHierarchy() {
        return (getAdapteeManager().supportsOntologyHierarchy());
    }


    /**
     *  Tests if ontology hierarchy design is supported. 
     *
     *  @return <code> true </code> if an ontology hierarchy design is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOntologyHierarchyDesign() {
        return (getAdapteeManager().supportsOntologyHierarchyDesign());
    }


    /**
     *  Tests if <code> Ids </code> can be asssigned to ontologies. 
     *
     *  @return <code> true </code> if an ontology hassignment is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOntologyAssignment() {
        return (getAdapteeManager().supportsOntologyAssignment());
    }


    /**
     *  Tests if an ontology batch service is supported. 
     *
     *  @return <code> true </code> if ontology batch is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOntologyBatch() {
        return (getAdapteeManager().supportsOntologyBatch());
    }


    /**
     *  Tests if an ontology rules service is supported. 
     *
     *  @return <code> true </code> if ontology rules is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOntologyRules() {
        return (getAdapteeManager().supportsOntologyRules());
    }


    /**
     *  Gets the supported <code> Subject </code> record types. 
     *
     *  @return a list containing the supported <code> Subject </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSubjectRecordTypes() {
        return (getAdapteeManager().getSubjectRecordTypes());
    }


    /**
     *  Tests if the given <code> Subject </code> record type is supported. 
     *
     *  @param  subjectRecordType a <code> Type </code> indicating a <code> 
     *          Subject </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> subjectRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSubjectRecordType(org.osid.type.Type subjectRecordType) {
        return (getAdapteeManager().supportsSubjectRecordType(subjectRecordType));
    }


    /**
     *  Gets the supported <code> Subject </code> search record types. 
     *
     *  @return a list containing the supported <code> Subject </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSubjectSearchRecordTypes() {
        return (getAdapteeManager().getSubjectSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Subject </code> search record type is 
     *  supported. 
     *
     *  @param  subjectSearchRecordType a <code> Type </code> indicating a 
     *          <code> Subject </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> subjectSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSubjectSearchRecordType(org.osid.type.Type subjectSearchRecordType) {
        return (getAdapteeManager().supportsSubjectSearchRecordType(subjectSearchRecordType));
    }


    /**
     *  Gets the supported <code> Relevancy </code> record types. 
     *
     *  @return a list containing the supported <code> Relevancy </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRelevancyRecordTypes() {
        return (getAdapteeManager().getRelevancyRecordTypes());
    }


    /**
     *  Tests if the given <code> Relevancy </code> record type is supported. 
     *
     *  @param  relevancyRecordType a <code> Type </code> indicating a <code> 
     *          Relevnacy </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> relevancyRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRelevancyRecordType(org.osid.type.Type relevancyRecordType) {
        return (getAdapteeManager().supportsRelevancyRecordType(relevancyRecordType));
    }


    /**
     *  Gets the supported <code> Relevancy </code> search record types. 
     *
     *  @return a list containing the supported <code> Relevancy </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRelevancySearchRecordTypes() {
        return (getAdapteeManager().getRelevancySearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Relevancy </code> search record type is 
     *  supported. 
     *
     *  @param  relevancySearchRecordType a <code> Type </code> indicating a 
     *          <code> Relevancy </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          relevancySearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRelevancySearchRecordType(org.osid.type.Type relevancySearchRecordType) {
        return (getAdapteeManager().supportsRelevancySearchRecordType(relevancySearchRecordType));
    }


    /**
     *  Gets the supported <code> Ontology </code> record types. 
     *
     *  @return a list containing the supported <code> Ontology </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getOntologyRecordTypes() {
        return (getAdapteeManager().getOntologyRecordTypes());
    }


    /**
     *  Tests if the given <code> Ontology </code> record type is supported. 
     *
     *  @param  ontologyRecordType a <code> Type </code> indicating an <code> 
     *          Ontology </code> type 
     *  @return <code> true </code> if the given ontology record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> ontologyRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsOntologyRecordType(org.osid.type.Type ontologyRecordType) {
        return (getAdapteeManager().supportsOntologyRecordType(ontologyRecordType));
    }


    /**
     *  Gets the supported ontology search record types. 
     *
     *  @return a list containing the supported <code> Ontology </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getOntologySearchRecordTypes() {
        return (getAdapteeManager().getOntologySearchRecordTypes());
    }


    /**
     *  Tests if the given ontology search record type is supported. 
     *
     *  @param  ontologySearchRecordType a <code> Type </code> indicating an 
     *          <code> Ontology </code> search record type 
     *  @return <code> true </code> if the given search record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> ontologySearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsOntologySearchRecordType(org.osid.type.Type ontologySearchRecordType) {
        return (getAdapteeManager().supportsOntologySearchRecordType(ontologySearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subject lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SubjectLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSubjectLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectLookupSession getSubjectLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSubjectLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subject lookup 
     *  service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @param  proxy a proxy 
     *  @return <code> a SubjectLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsSubjectLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectLookupSession getSubjectLookupSessionForOntology(org.osid.id.Id ontologyId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSubjectLookupSessionForOntology(ontologyId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subject lookup 
     *  service for the given <code> Id </code> assigned using the <code> 
     *  OntologyAssignmentSession. </code> 
     *
     *  @param  id an <code> Id </code> 
     *  @param  proxy a proxy 
     *  @return <code> a SubjectLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> id </code> not found 
     *  @throws org.osid.NullArgumentException <code> id </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsSubjectLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectLookupSession getSubjectLookupSessionForId(org.osid.id.Id id, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSubjectLookupSessionForId(id, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subject query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SubjectQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSubjecQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectQuerySession getSubjectQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSubjectQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subject query 
     *  service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @param  proxy a proxy 
     *  @return a <code> SubjectQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsSubjectQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectQuerySession getSubjectQuerySessionForOntology(org.osid.id.Id ontologyId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSubjectQuerySessionForOntology(ontologyId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subject search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SubjectSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSubjectSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectSearchSession getSubjectSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSubjectSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subject search 
     *  service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @param  proxy a proxy 
     *  @return <code> a SubjectSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsSubjectSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectSearchSession getSubjectSearchSessionForOntology(org.osid.id.Id ontologyId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSubjectSearchSessionForOntology(ontologyId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subject 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SubjectAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSubjectAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectAdminSession getSubjectAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSubjectAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subject admin 
     *  service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @param  proxy a proxy 
     *  @return <code> a SubjectAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsSubjectAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectAdminSession getSubjectAdminSessionForOntology(org.osid.id.Id ontologyId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSubjectAdminSessionForOntology(ontologyId, proxy));
    }


    /**
     *  Gets the notification session for notifications pertaining to subject 
     *  changes. 
     *
     *  @param  subjectReceiver the subject receiver 
     *  @param  proxy a proxy 
     *  @return a <code> SubjectNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> subjectReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubjectNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectNotificationSession getSubjectNotificationSession(org.osid.ontology.SubjectReceiver subjectReceiver, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSubjectNotificationSession(subjectReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subject 
     *  notification service for the given ontology. 
     *
     *  @param  subjectReceiver the subject receiver 
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @param  proxy a proxy 
     *  @return <code> a SubjectNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> subjectReceiver, 
     *          ontologyId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubjectNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectNotificationSession getSubjectNotificationSessionForOntology(org.osid.ontology.SubjectReceiver subjectReceiver, 
                                                                                                 org.osid.id.Id ontologyId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSubjectNotificationSessionForOntology(subjectReceiver, ontologyId, proxy));
    }


    /**
     *  Gets the session traversing subject hierarchies. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SubjectHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubjectHierarchy() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectHierarchySession getSubjectHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSubjectHierarchySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subject 
     *  heirarchy traversal service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @param  proxy a proxy 
     *  @return a <code> SubjectHierarchySession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubjectHierarchy() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectHierarchySession getSubjectHierarchySessionForOntology(org.osid.id.Id ontologyId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSubjectHierarchySessionForOntology(ontologyId, proxy));
    }


    /**
     *  Gets the hierarchy session for the given <code> Id </code> assigned 
     *  using the <code> OntologyAssignmentSession. </code> 
     *
     *  @param  id an <code> Id </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SubjectHierarchySession </code> 
     *  @throws org.osid.NotFoundException <code> id </code> not found 
     *  @throws org.osid.NullArgumentException <code> id </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubjectHierarchy() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectHierarchySession getSubjectHierarchySessionForId(org.osid.id.Id id, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSubjectHierarchySessionForId(id, proxy));
    }


    /**
     *  Gets the session designing subject hierarchies. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SubjectHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubjectHierarchyDesign() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectHierarchyDesignSession getSubjectHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSubjectHierarchyDesignSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subject 
     *  heirarchy design service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @param  proxy a proxy 
     *  @return a <code> SubjectHierarchyDesignSession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubjectHierarchyDesign() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectHierarchyDesignSession getSubjectHierarchyDesignSessionForOntology(org.osid.id.Id ontologyId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSubjectHierarchyDesignSessionForOntology(ontologyId, proxy));
    }


    /**
     *  Gets the session retrieving subject ontology mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SubjectOntologySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubjectOntology() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectOntologySession getSubjectOntologySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSubjectOntologySession(proxy));
    }


    /**
     *  Gets the session managing subject ontology mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SubjectOntologyAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubjectOntologyAssignment() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectOntologyAssignmentSession getSubjectOntologyAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSubjectOntologyAssignmentSession(proxy));
    }


    /**
     *  Gets the session managing subject smart ontologies. 
     *
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @param  proxy a proxy 
     *  @return a <code> SubjectSmartOntologySession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubjectSmartOntology() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectOntologySession getSubjectSmartOntologySession(org.osid.id.Id ontologyId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSubjectSmartOntologySession(ontologyId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyLookupSession getRelevancyLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRelevancyLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  lookup service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @param  proxy a proxy 
     *  @return <code> a RelevancyLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyLookupSession getRelevancyLookupSessionForOntology(org.osid.id.Id ontologyId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRelevancyLookupSessionForOntology(ontologyId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyQuerySession getRelevancyQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRelevancyQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  query service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @param  proxy a proxy 
     *  @return <code> a RelevancyQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyQuerySession getRelevancyQuerySessionForOntology(org.osid.id.Id ontologyId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRelevancyQuerySessionForOntology(ontologyId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelevanctSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancySearchSession getRelevancySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRelevancySearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  search service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @param  proxy a proxy 
     *  @return <code> a RelevancySearchSession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancySearchSession getRelevancySearchSessionForOntology(org.osid.id.Id ontologyId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRelevancySearchSessionForOntology(ontologyId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelvancyAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyAdminSession getRelevancyAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRelevancyAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  admin service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @param  proxy a proxy 
     *  @return <code> a RelevancyAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyAdminSession getRelevancyAdminSessionForOntology(org.osid.id.Id ontologyId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRelevancyAdminSessionForOntology(ontologyId, proxy));
    }


    /**
     *  Gets the notification session for notifications pertaining to 
     *  relevancy changes. 
     *
     *  @param  relevancyReceiver the relevancy receiver 
     *  @param  proxy a proxy 
     *  @return <code> a RelevancyNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> relevancyReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyNotificationSession getRelevancyNotificationSession(org.osid.ontology.RelevancyReceiver relevancyReceiver, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRelevancyNotificationSession(relevancyReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  notification service for the given ontology. 
     *
     *  @param  relevancyReceiver the subject receiver 
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @param  proxy a proxy 
     *  @return <code> a RelevancyNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> relevancyReceiver, 
     *          ontologyId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyNotificationSession getRelevancyNotificationSessionForOntology(org.osid.ontology.RelevancyReceiver relevancyReceiver, 
                                                                                                     org.osid.id.Id ontologyId, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRelevancyNotificationSessionForOntology(relevancyReceiver, ontologyId, proxy));
    }


    /**
     *  Gets the session retrieving relevancy ontology mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyOntologySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyOntology() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyOntologySession getRelevancyOntologySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRelevancyOntologySession(proxy));
    }


    /**
     *  Gets the session managing relevancy ontology mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyOntologyAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyOntologyAssignment() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyOntologyAssignmentSession getRelevancyOntologyAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRelevancyOntologyAssignmentSession(proxy));
    }


    /**
     *  Gets the session managing relevancy smart ontologies. 
     *
     *  @param  ontologyId the <code> Id </code> of the ontology 
     *  @param  proxy a proxy 
     *  @return a <code> RelevancySmartOntologySession </code> 
     *  @throws org.osid.NotFoundException <code> ontologyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancySmartOntology() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancySmartOntologySession getRelevancySmartOntologySession(org.osid.id.Id ontologyId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRelevancySmartOntologySession(ontologyId, proxy));
    }


    /**
     *  Gets the OsidSession associated with the ontology lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OntologyLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOntologyLookup() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.OntologyLookupSession getOntologyLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOntologyLookupSession(proxy));
    }


    /**
     *  Gets the OsidSession associated with the ontology query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OntologyQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOntologyQuery() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.OntologyQuerySession getOntologyQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOntologyQuerySession(proxy));
    }


    /**
     *  Gets the OsidSession associated with the ontology search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OntologySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOntologySearch() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.OntologySearchSession getOntologySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOntologySearchSession(proxy));
    }


    /**
     *  Gets the OsidSession associated with the ontology administration 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OntologyAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOntologyAdmin() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.OntologyAdminSession getOntologyAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOntologyAdminSession(proxy));
    }


    /**
     *  Gets the notification session for notifications pertaining to ontology 
     *  service changes. 
     *
     *  @param  ontologyReceiver the ontology receiver 
     *  @param  proxy a proxy 
     *  @return an <code> OntologyNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> ontologyReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOntologyNotification() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.OntologyNotificationSession getOntologyNotificationSession(org.osid.ontology.OntologyReceiver ontologyReceiver, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOntologyNotificationSession(ontologyReceiver, proxy));
    }


    /**
     *  Gets the session traversing ontology hierarchies. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OntologyHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOntologyHierarchy() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.OntologyHierarchySession getOntologyHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOntologyHierarchySession(proxy));
    }


    /**
     *  Gets the session designing ontology hierarchies. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OntologyHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOntologyHierarchyDesign() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.OntologyHierarchyDesignSession getOntologyHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOntologyHierarchyDesignSession(proxy));
    }


    /**
     *  Gets the session to assign <code> Ids </code> to ontologies. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OntologyAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOntologyAssignment() is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.OntologyAssignmentSession getOntologyAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOntologyAssignmentSession(proxy));
    }


    /**
     *  Gets the ontology batch service. 
     *
     *  @return an <code> OntologyBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOntologyBatch() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.batch.OntologyBatchProxyManager getOntologyBatchProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOntologyBatchProxyManager());
    }


    /**
     *  Gets the ontology rules service. 
     *
     *  @return an <code> OntologyRulesProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOntologyRules() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.OntologyRulesProxyManager getOntologyRulesProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOntologyRulesProxyManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
