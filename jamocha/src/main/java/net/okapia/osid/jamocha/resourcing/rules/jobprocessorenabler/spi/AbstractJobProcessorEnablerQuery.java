//
// AbstractJobProcessorEnablerQuery.java
//
//     A template for making a JobProcessorEnabler Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.rules.jobprocessorenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for job processor enablers.
 */

public abstract class AbstractJobProcessorEnablerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.resourcing.rules.JobProcessorEnablerQuery {

    private final java.util.Collection<org.osid.resourcing.rules.records.JobProcessorEnablerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches enablers mapped to the job processor. 
     *
     *  @param  jobProcessorId the job processor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> jobProcessorId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledJobProcessorId(org.osid.id.Id jobProcessorId, 
                                         boolean match) {
        return;
    }


    /**
     *  Clears the job processor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledJobProcessorIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> JobProcessorQuery </code> is available. 
     *
     *  @return <code> true </code> if a job processor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledJobProcessorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a job processor. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the job processor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledJobProcessorQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorQuery getRuledJobProcessorQuery() {
        throw new org.osid.UnimplementedException("supportsRuledJobProcessorQuery() is false");
    }


    /**
     *  Matches enablers mapped to any job processor. 
     *
     *  @param  match <code> true </code> for enablers mapped to any job 
     *          processor, <code> false </code> to match enablers mapped to no 
     *          job processors 
     */

    @OSID @Override
    public void matchAnyRuledJobProcessor(boolean match) {
        return;
    }


    /**
     *  Clears the job processor query terms. 
     */

    @OSID @Override
    public void clearRuledJobProcessorTerms() {
        return;
    }


    /**
     *  Matches enablers mapped to the foundry. 
     *
     *  @param  foundryId the foundry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchFoundryId(org.osid.id.Id foundryId, boolean match) {
        return;
    }


    /**
     *  Clears the foundry <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearFoundryIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> FoundryQuery </code> is available. 
     *
     *  @return <code> true </code> if a foundry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFoundryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a foundry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the foundry query 
     *  @throws org.osid.UnimplementedException <code> supportsFoundryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQuery getFoundryQuery() {
        throw new org.osid.UnimplementedException("supportsFoundryQuery() is false");
    }


    /**
     *  Clears the foundry query terms. 
     */

    @OSID @Override
    public void clearFoundryTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given job processor enabler query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a job processor enabler implementing the requested record.
     *
     *  @param jobProcessorEnablerRecordType a job processor enabler record type
     *  @return the job processor enabler query record
     *  @throws org.osid.NullArgumentException
     *          <code>jobProcessorEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(jobProcessorEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.rules.records.JobProcessorEnablerQueryRecord getJobProcessorEnablerQueryRecord(org.osid.type.Type jobProcessorEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.rules.records.JobProcessorEnablerQueryRecord record : this.records) {
            if (record.implementsRecordType(jobProcessorEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(jobProcessorEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this job processor enabler query. 
     *
     *  @param jobProcessorEnablerQueryRecord job processor enabler query record
     *  @param jobProcessorEnablerRecordType jobProcessorEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addJobProcessorEnablerQueryRecord(org.osid.resourcing.rules.records.JobProcessorEnablerQueryRecord jobProcessorEnablerQueryRecord, 
                                          org.osid.type.Type jobProcessorEnablerRecordType) {

        addRecordType(jobProcessorEnablerRecordType);
        nullarg(jobProcessorEnablerQueryRecord, "job processor enabler query record");
        this.records.add(jobProcessorEnablerQueryRecord);        
        return;
    }
}
