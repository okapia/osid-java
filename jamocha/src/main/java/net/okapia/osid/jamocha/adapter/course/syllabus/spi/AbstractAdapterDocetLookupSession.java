//
// AbstractAdapterDocetLookupSession.java
//
//    A Docet lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.course.syllabus.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Docet lookup session adapter.
 */

public abstract class AbstractAdapterDocetLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.course.syllabus.DocetLookupSession {

    private final org.osid.course.syllabus.DocetLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterDocetLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterDocetLookupSession(org.osid.course.syllabus.DocetLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code CourseCatalog/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code CourseCatalog Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.session.getCourseCatalogId());
    }


    /**
     *  Gets the {@code CourseCatalog} associated with this session.
     *
     *  @return the {@code CourseCatalog} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCourseCatalog());
    }


    /**
     *  Tests if this user can perform {@code Docet} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupDocets() {
        return (this.session.canLookupDocets());
    }


    /**
     *  A complete view of the {@code Docet} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeDocetView() {
        this.session.useComparativeDocetView();
        return;
    }


    /**
     *  A complete view of the {@code Docet} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryDocetView() {
        this.session.usePlenaryDocetView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include docets in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.session.useFederatedCourseCatalogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.session.useIsolatedCourseCatalogView();
        return;
    }
    

    /**
     *  Only docets whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveDocetView() {
        this.session.useEffectiveDocetView();
        return;
    }
    

    /**
     *  All docets of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveDocetView() {
        this.session.useAnyEffectiveDocetView();
        return;
    }

     
    /**
     *  Gets the {@code Docet} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Docet} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Docet} and
     *  retained for compatibility.
     *
     *  In effective mode, docets are returned that are currently
     *  effective.  In any effective mode, effective docets and
     *  those currently expired are returned.
     *
     *  @param docetId {@code Id} of the {@code Docet}
     *  @return the docet
     *  @throws org.osid.NotFoundException {@code docetId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code docetId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.Docet getDocet(org.osid.id.Id docetId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDocet(docetId));
    }


    /**
     *  Gets a {@code DocetList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  docets specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Docets} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, docets are returned that are currently
     *  effective.  In any effective mode, effective docets and
     *  those currently expired are returned.
     *
     *  @param  docetIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Docet} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code docetIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetList getDocetsByIds(org.osid.id.IdList docetIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDocetsByIds(docetIds));
    }


    /**
     *  Gets a {@code DocetList} corresponding to the given
     *  docet genus {@code Type} which does not include
     *  docets of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  docets or an error results. Otherwise, the returned list
     *  may contain only those docets that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, docets are returned that are currently
     *  effective.  In any effective mode, effective docets and
     *  those currently expired are returned.
     *
     *  @param  docetGenusType a docet genus type 
     *  @return the returned {@code Docet} list
     *  @throws org.osid.NullArgumentException
     *          {@code docetGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetList getDocetsByGenusType(org.osid.type.Type docetGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDocetsByGenusType(docetGenusType));
    }


    /**
     *  Gets a {@code DocetList} corresponding to the given
     *  docet genus {@code Type} and include any additional
     *  docets with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  docets or an error results. Otherwise, the returned list
     *  may contain only those docets that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, docets are returned that are currently
     *  effective.  In any effective mode, effective docets and
     *  those currently expired are returned.
     *
     *  @param  docetGenusType a docet genus type 
     *  @return the returned {@code Docet} list
     *  @throws org.osid.NullArgumentException
     *          {@code docetGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetList getDocetsByParentGenusType(org.osid.type.Type docetGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDocetsByParentGenusType(docetGenusType));
    }


    /**
     *  Gets a {@code DocetList} containing the given
     *  docet record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  docets or an error results. Otherwise, the returned list
     *  may contain only those docets that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, docets are returned that are currently
     *  effective.  In any effective mode, effective docets and
     *  those currently expired are returned.
     *
     *  @param  docetRecordType a docet record type 
     *  @return the returned {@code Docet} list
     *  @throws org.osid.NullArgumentException
     *          {@code docetRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetList getDocetsByRecordType(org.osid.type.Type docetRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDocetsByRecordType(docetRecordType));
    }


    /**
     *  Gets a {@code DocetList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  docets or an error results. Otherwise, the returned list
     *  may contain only those docets that are accessible
     *  through this session.
     *  
     *  In active mode, docets are returned that are currently
     *  active. In any status mode, active and inactive docets
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Docet} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.syllabus.DocetList getDocetsOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDocetsOnDate(from, to));
    }
        

    /**
     *  Gets a list of docets corresponding to a module
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  docets or an error results. Otherwise, the returned list
     *  may contain only those docets that are accessible through
     *  this session.
     *
     *  In effective mode, docets are returned that are
     *  currently effective.  In any effective mode, effective
     *  docets and those currently expired are returned.
     *
     *  @param  moduleId the {@code Id} of the module
     *  @return the returned {@code DocetList}
     *  @throws org.osid.NullArgumentException {@code moduleId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetList getDocetsForModule(org.osid.id.Id moduleId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDocetsForModule(moduleId));
    }


    /**
     *  Gets a list of docets corresponding to a module
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  docets or an error results. Otherwise, the returned list
     *  may contain only those docets that are accessible
     *  through this session.
     *
     *  In effective mode, docets are returned that are
     *  currently effective.  In any effective mode, effective
     *  docets and those currently expired are returned.
     *
     *  @param  moduleId the {@code Id} of the module
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code DocetList}
     *  @throws org.osid.NullArgumentException {@code moduleId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetList getDocetsForModuleOnDate(org.osid.id.Id moduleId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDocetsForModuleOnDate(moduleId, from, to));
    }


    /**
     *  Gets a list of docets corresponding to an activity unit {@code
     *  Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  docets or an error results. Otherwise, the returned list
     *  may contain only those docets that are accessible
     *  through this session.
     *
     *  In effective mode, docets are returned that are
     *  currently effective.  In any effective mode, effective
     *  docets and those currently expired are returned.
     *
     *  @param activityUnitId the {@code Id} of the activity unit
     *  @return the returned {@code DocetList}
     *  @throws org.osid.NullArgumentException {@code activityUnitId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetList getDocetsForActivityUnit(org.osid.id.Id activityUnitId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDocetsForActivityUnit(activityUnitId));
    }


    /**
     *  Gets a list of docets corresponding to an activity unit {@code
     *  Id} and effective during the entire given date range inclusive
     *  but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  docets or an error results. Otherwise, the returned list
     *  may contain only those docets that are accessible
     *  through this session.
     *
     *  In effective mode, docets are returned that are
     *  currently effective.  In any effective mode, effective
     *  docets and those currently expired are returned.
     *
     *  @param  activityUnitId the {@code Id} of the activity unit
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code DocetList}
     *  @throws org.osid.NullArgumentException {@code activityUnitId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetList getDocetsForActivityUnitOnDate(org.osid.id.Id activityUnitId,
                                                                             org.osid.calendaring.DateTime from,
                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDocetsForActivityUnitOnDate(activityUnitId, from, to));
    }


    /**
     *  Gets a list of docets corresponding to module and activity
     *  unit {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known
     *  docets or an error results. Otherwise, the returned list
     *  may contain only those docets that are accessible
     *  through this session.
     *
     *  In effective mode, docets are returned that are
     *  currently effective.  In any effective mode, effective
     *  docets and those currently expired are returned.
     *
     *  @param  moduleId the {@code Id} of the module
     *  @param  activityUnitId the {@code Id} of the activity unit
     *  @return the returned {@code DocetList}
     *  @throws org.osid.NullArgumentException {@code moduleId},
     *          {@code activityUnitId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetList getDocetsForModuleAndActivityUnit(org.osid.id.Id moduleId,
                                                                                org.osid.id.Id activityUnitId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDocetsForModuleAndActivityUnit(moduleId, activityUnitId));
    }


    /**
     *  Gets a list of docets corresponding to module and activity
     *  unit {@code Ids} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  docets or an error results. Otherwise, the returned list
     *  may contain only those docets that are accessible
     *  through this session.
     *
     *  In effective mode, docets are returned that are currently
     *  effective. In any effective mode, effective docets and
     *  those currently expired are returned.
     *
     *  @param activityUnitId the {@code Id} of the activity unit
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code DocetList}
     *  @throws org.osid.NullArgumentException {@code moduleId},
     *          {@code activityUnitId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetList getDocetsForModuleAndActivityUnitOnDate(org.osid.id.Id moduleId,
                                                                                      org.osid.id.Id activityUnitId,
                                                                                      org.osid.calendaring.DateTime from,
                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDocetsForModuleAndActivityUnitOnDate(moduleId, activityUnitId, from, to));
    }


    /**
     *  Gets all {@code Docets}. 
     *
     *  In plenary mode, the returned list contains all known
     *  docets or an error results. Otherwise, the returned list
     *  may contain only those docets that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, docets are returned that are currently
     *  effective.  In any effective mode, effective docets and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Docets} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetList getDocets()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDocets());
    }
}
