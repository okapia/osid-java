//
// SupersedingEventElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.supersedingevent.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class SupersedingEventElements
    extends net.okapia.osid.jamocha.spi.OsidRuleElements {


    /**
     *  Gets the SupersedingEventElement Id.
     *
     *  @return the superseding event element Id
     */

    public static org.osid.id.Id getSupersedingEventEntityId() {
        return (makeEntityId("osid.calendaring.SupersedingEvent"));
    }


    /**
     *  Gets the SupersededEventId element Id.
     *
     *  @return the SupersededEventId element Id
     */

    public static org.osid.id.Id getSupersededEventId() {
        return (makeElementId("osid.calendaring.supersedingevent.SupersededEventId"));
    }


    /**
     *  Gets the SupersededEvent element Id.
     *
     *  @return the SupersededEvent element Id
     */

    public static org.osid.id.Id getSupersededEvent() {
        return (makeElementId("osid.calendaring.supersedingevent.SupersededEvent"));
    }


    /**
     *  Gets the SupersedingEventId element Id.
     *
     *  @return the SupersedingEventId element Id
     */

    public static org.osid.id.Id getSupersedingEventId() {
        return (makeElementId("osid.calendaring.supersedingevent.SupersedingEventId"));
    }


    /**
     *  Gets the SupersedingEvent element Id.
     *
     *  @return the SupersedingEvent element Id
     */

    public static org.osid.id.Id getSupersedingEvent() {
        return (makeElementId("osid.calendaring.supersedingevent.SupersedingEvent"));
    }


    /**
     *  Gets the SupersededDate element Id.
     *
     *  @return the SupersededDate element Id
     */

    public static org.osid.id.Id getSupersededDate() {
        return (makeElementId("osid.calendaring.supersedingevent.SupersededDate"));
    }


    /**
     *  Gets the SupersededEventPosition element Id.
     *
     *  @return the SupersededEventPosition element Id
     */

    public static org.osid.id.Id getSupersededEventPosition() {
        return (makeElementId("osid.calendaring.supersedingevent.SupersededEventPosition"));
    }


    /**
     *  Gets the CalendarId element Id.
     *
     *  @return the CalendarId element Id
     */

    public static org.osid.id.Id getCalendarId() {
        return (makeQueryElementId("osid.calendaring.supersedingevent.CalendarId"));
    }


    /**
     *  Gets the Calendar element Id.
     *
     *  @return the Calendar element Id
     */

    public static org.osid.id.Id getCalendar() {
        return (makeQueryElementId("osid.calendaring.supersedingevent.Calendar"));
    }
}
