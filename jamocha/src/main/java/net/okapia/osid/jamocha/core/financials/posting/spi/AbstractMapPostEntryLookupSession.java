//
// AbstractMapPostEntryLookupSession
//
//    A simple framework for providing a PostEntry lookup service
//    backed by a fixed collection of post entries.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.financials.posting.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a PostEntry lookup service backed by a
 *  fixed collection of post entries. The post entries are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>PostEntries</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapPostEntryLookupSession
    extends net.okapia.osid.jamocha.financials.posting.spi.AbstractPostEntryLookupSession
    implements org.osid.financials.posting.PostEntryLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.financials.posting.PostEntry> postEntries = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.financials.posting.PostEntry>());


    /**
     *  Makes a <code>PostEntry</code> available in this session.
     *
     *  @param  postEntry a post entry
     *  @throws org.osid.NullArgumentException <code>postEntry<code>
     *          is <code>null</code>
     */

    protected void putPostEntry(org.osid.financials.posting.PostEntry postEntry) {
        this.postEntries.put(postEntry.getId(), postEntry);
        return;
    }


    /**
     *  Makes an array of post entries available in this session.
     *
     *  @param  postEntries an array of post entries
     *  @throws org.osid.NullArgumentException <code>postEntries<code>
     *          is <code>null</code>
     */

    protected void putPostEntries(org.osid.financials.posting.PostEntry[] postEntries) {
        putPostEntries(java.util.Arrays.asList(postEntries));
        return;
    }


    /**
     *  Makes a collection of post entries available in this session.
     *
     *  @param  postEntries a collection of post entries
     *  @throws org.osid.NullArgumentException <code>postEntries<code>
     *          is <code>null</code>
     */

    protected void putPostEntries(java.util.Collection<? extends org.osid.financials.posting.PostEntry> postEntries) {
        for (org.osid.financials.posting.PostEntry postEntry : postEntries) {
            this.postEntries.put(postEntry.getId(), postEntry);
        }

        return;
    }


    /**
     *  Removes a PostEntry from this session.
     *
     *  @param  postEntryId the <code>Id</code> of the post entry
     *  @throws org.osid.NullArgumentException <code>postEntryId<code> is
     *          <code>null</code>
     */

    protected void removePostEntry(org.osid.id.Id postEntryId) {
        this.postEntries.remove(postEntryId);
        return;
    }


    /**
     *  Gets the <code>PostEntry</code> specified by its <code>Id</code>.
     *
     *  @param  postEntryId <code>Id</code> of the <code>PostEntry</code>
     *  @return the postEntry
     *  @throws org.osid.NotFoundException <code>postEntryId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>postEntryId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntry getPostEntry(org.osid.id.Id postEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.financials.posting.PostEntry postEntry = this.postEntries.get(postEntryId);
        if (postEntry == null) {
            throw new org.osid.NotFoundException("postEntry not found: " + postEntryId);
        }

        return (postEntry);
    }


    /**
     *  Gets all <code>PostEntries</code>. In plenary mode, the returned
     *  list contains all known postEntries or an error
     *  results. Otherwise, the returned list may contain only those
     *  postEntries that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>PostEntries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.financials.posting.postentry.ArrayPostEntryList(this.postEntries.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.postEntries.clear();
        super.close();
        return;
    }
}
