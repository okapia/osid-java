//
// AbstractAdapterAuctionConstrainerEnablerLookupSession.java
//
//    An AuctionConstrainerEnabler lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.bidding.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An AuctionConstrainerEnabler lookup session adapter.
 */

public abstract class AbstractAdapterAuctionConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.bidding.rules.AuctionConstrainerEnablerLookupSession {

    private final org.osid.bidding.rules.AuctionConstrainerEnablerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterAuctionConstrainerEnablerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterAuctionConstrainerEnablerLookupSession(org.osid.bidding.rules.AuctionConstrainerEnablerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code AuctionHouse/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code AuctionHouse Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAuctionHouseId() {
        return (this.session.getAuctionHouseId());
    }


    /**
     *  Gets the {@code AuctionHouse} associated with this session.
     *
     *  @return the {@code AuctionHouse} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouse getAuctionHouse()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getAuctionHouse());
    }


    /**
     *  Tests if this user can perform {@code AuctionConstrainerEnabler} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupAuctionConstrainerEnablers() {
        return (this.session.canLookupAuctionConstrainerEnablers());
    }


    /**
     *  A complete view of the {@code AuctionConstrainerEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAuctionConstrainerEnablerView() {
        this.session.useComparativeAuctionConstrainerEnablerView();
        return;
    }


    /**
     *  A complete view of the {@code AuctionConstrainerEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAuctionConstrainerEnablerView() {
        this.session.usePlenaryAuctionConstrainerEnablerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include auction constrainer enablers in auction houses which are children
     *  of this auction house in the auction house hierarchy.
     */

    @OSID @Override
    public void useFederatedAuctionHouseView() {
        this.session.useFederatedAuctionHouseView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this auction house only.
     */

    @OSID @Override
    public void useIsolatedAuctionHouseView() {
        this.session.useIsolatedAuctionHouseView();
        return;
    }
    

    /**
     *  Only active auction constrainer enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveAuctionConstrainerEnablerView() {
        this.session.useActiveAuctionConstrainerEnablerView();
        return;
    }


    /**
     *  Active and inactive auction constrainer enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusAuctionConstrainerEnablerView() {
        this.session.useAnyStatusAuctionConstrainerEnablerView();
        return;
    }
    
     
    /**
     *  Gets the {@code AuctionConstrainerEnabler} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code AuctionConstrainerEnabler} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code AuctionConstrainerEnabler} and
     *  retained for compatibility.
     *
     *  In active mode, auction constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  auction constrainer enablers are returned.
     *
     *  @param auctionConstrainerEnablerId {@code Id} of the {@code
     *         AuctionConstrainerEnabler}
     *  @return the auction constrainer enabler
     *  @throws org.osid.NotFoundException {@code auctionConstrainerEnablerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code auctionConstrainerEnablerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnabler getAuctionConstrainerEnabler(org.osid.id.Id auctionConstrainerEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuctionConstrainerEnabler(auctionConstrainerEnablerId));
    }


    /**
     *  Gets an {@code AuctionConstrainerEnablerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  auctionConstrainerEnablers specified in the {@code Id} list,
     *  in the order of the list, including duplicates, or an error
     *  results if an {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code
     *  AuctionConstrainerEnablers} may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  In active mode, auction constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  auction constrainer enablers are returned.
     *
     *  @param  auctionConstrainerEnablerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code AuctionConstrainerEnabler} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code auctionConstrainerEnablerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerList getAuctionConstrainerEnablersByIds(org.osid.id.IdList auctionConstrainerEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuctionConstrainerEnablersByIds(auctionConstrainerEnablerIds));
    }


    /**
     *  Gets an {@code AuctionConstrainerEnablerList} corresponding to the given
     *  auction constrainer enabler genus {@code Type} which does not include
     *  auction constrainer enablers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known auction
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those auction constrainer
     *  enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, auction constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  auction constrainer enablers are returned.
     *
     *  @param auctionConstrainerEnablerGenusType an
     *         auctionConstrainerEnabler genus type
     *  @return the returned {@code AuctionConstrainerEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code auctionConstrainerEnablerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerList getAuctionConstrainerEnablersByGenusType(org.osid.type.Type auctionConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuctionConstrainerEnablersByGenusType(auctionConstrainerEnablerGenusType));
    }


    /**
     *  Gets an {@code AuctionConstrainerEnablerList} corresponding to the given
     *  auction constrainer enabler genus {@code Type} and include any additional
     *  auction constrainer enablers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known auction
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those auction constrainer
     *  enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, auction constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  auction constrainer enablers are returned.
     *
     *  @param auctionConstrainerEnablerGenusType an
     *         auctionConstrainerEnabler genus type
     *  @return the returned {@code AuctionConstrainerEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code auctionConstrainerEnablerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerList getAuctionConstrainerEnablersByParentGenusType(org.osid.type.Type auctionConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuctionConstrainerEnablersByParentGenusType(auctionConstrainerEnablerGenusType));
    }


    /**
     *  Gets an {@code AuctionConstrainerEnablerList} containing the given
     *  auction constrainer enabler record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known auction
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those auction constrainer
     *  enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, auction constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  auction constrainer enablers are returned.
     *
     *  @param  auctionConstrainerEnablerRecordType an auctionConstrainerEnabler record type 
     *  @return the returned {@code AuctionConstrainerEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code auctionConstrainerEnablerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerList getAuctionConstrainerEnablersByRecordType(org.osid.type.Type auctionConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuctionConstrainerEnablersByRecordType(auctionConstrainerEnablerRecordType));
    }


    /**
     *  Gets an {@code AuctionConstrainerEnablerList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  auction constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those auction constrainer enablers that are accessible
     *  through this session.
     *  
     *  In active mode, auction constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive auction constrainer enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code AuctionConstrainerEnabler} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerList getAuctionConstrainerEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuctionConstrainerEnablersOnDate(from, to));
    }
        

    /**
     *  Gets an {@code AuctionConstrainerEnablerList } which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known auction
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those auction constrainer
     *  enablers that are accessible through this session.
     *
     *  In active mode, auction constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  auction constrainer enablers are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned {@code AuctionConstrainerEnabler} list
     *  @throws org.osid.InvalidArgumentException {@code from} is
     *          greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code agent},
     *          {@code from}, or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerList getAuctionConstrainerEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (this.session.getAuctionConstrainerEnablersOnDateWithAgent(agentId, from, to));
    }


    /**
     *  Gets all {@code AuctionConstrainerEnablers}. 
     *
     *  In plenary mode, the returned list contains all known auction
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those auction constrainer
     *  enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, auction constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  auction constrainer enablers are returned.
     *
     *  @return a list of {@code AuctionConstrainerEnablers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerList getAuctionConstrainerEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuctionConstrainerEnablers());
    }
}
