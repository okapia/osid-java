//
// AbstractImmutableProgram.java
//
//     Wraps a mutable Program to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.program.program.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Program</code> to hide modifiers. This
 *  wrapper provides an immutized Program from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying program whose state changes are visible.
 */

public abstract class AbstractImmutableProgram
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOperableOsidObject
    implements org.osid.course.program.Program {

    private final org.osid.course.program.Program program;


    /**
     *  Constructs a new <code>AbstractImmutableProgram</code>.
     *
     *  @param program the program to immutablize
     *  @throws org.osid.NullArgumentException <code>program</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableProgram(org.osid.course.program.Program program) {
        super(program);
        this.program = program;
        return;
    }


    /**
     *  Gets the formal title of this program. It may be the same as the 
     *  display name or it may be used to more formally label the program. 
     *
     *  @return the program title 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getTitle() {
        return (this.program.getTitle());
    }


    /**
     *  Gets the program number which is a label generally used to
     *  index the program in a catalog, such as "16c."
     *
     *  @return the program number 
     */

    @OSID @Override
    public String getNumber() {
        return (this.program.getNumber());
    }


    /**
     *  Tests if this program has sponsors. 
     *
     *  @return <code> true </code> if this program has sponsors, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasSponsors() {
        return (this.program.hasSponsors());
    }


    /**
     *  Gets the sponsor <code> Ids. </code> 
     *
     *  @return the sponsor <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> hasSponsors() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getSponsorIds() {
        return (this.program.getSponsorIds());
    }


    /**
     *  Gets the sponsors. 
     *
     *  @return the sponsors 
     *  @throws org.osid.IllegalStateException <code> hasSponsors() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getSponsors()
        throws org.osid.OperationFailedException {

        return (this.program.getSponsors());
    }


    /**
     *  Gets the an informational string for the program completion. 
     *
     *  @return the program completion 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getCompletionRequirementsInfo() {
        return (this.program.getCompletionRequirementsInfo());
    }


    /**
     *  Tests if this program has a rule for the program completion. 
     *
     *  @return <code> true </code> if this program has a completion rule, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasCompletionRequirements() {
        return (this.program.hasCompletionRequirements());
    }


    /**
     *  Gets the <code> Requisite </code> <code> Ids </code> for the program 
     *  completion. 
     *
     *  @return the completion requisite <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> 
     *          hasCompletionRequirements() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getCompletionRequirementIds() {
        return (this.program.getCompletionRequirementIds());
    }


    /**
     *  Gets the requisites for the program completion. Each <code> Requisite 
     *  </code> is an <code> AND </code> term and must be true for the 
     *  requirements to be satisifed. 
     *
     *  @return the completion requisites 
     *  @throws org.osid.IllegalStateException <code> 
     *          hasCompletionRequirements() </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getCompletionRequirements()
        throws org.osid.OperationFailedException {

        return (this.program.getCompletionRequirements());
    }


    /**
     *  Tests if completion of this program results in credentials awarded. 
     *
     *  @return <code> true </code> if this program earns credentials, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean earnsCredentials() {
        return (this.program.earnsCredentials());
    }


    /**
     *  Gets the awarded credential <code> Ids. </code> 
     *
     *  @return the returned list of credential <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> earnsCredentials() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getCredentialIds() {
        return (this.program.getCredentialIds());
    }


    /**
     *  Gets the awarded credentials. 
     *
     *  @return the returned list of credentials 
     *  @throws org.osid.IllegalStateException <code> earnsCredentials() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.program.CredentialList getCredentials()
        throws org.osid.OperationFailedException {

        return (this.program.getCredentials());
    }


    /**
     *  Gets the program record corresponding to the given <code> Program 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> programRecordType 
     *  </code> may be the <code> Type </code> returned in <code> 
     *  getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(programRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  programRecordType the type of program record to retrieve 
     *  @return the program record 
     *  @throws org.osid.NullArgumentException <code> programRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(programRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.program.records.ProgramRecord getProgramRecord(org.osid.type.Type programRecordType)
        throws org.osid.OperationFailedException {

        return (this.program.getProgramRecord(programRecordType));
    }
}

