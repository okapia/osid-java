//
// AbstractGradingManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractGradingManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.grading.GradingManager,
               org.osid.grading.GradingProxyManager {

    private final Types gradeRecordTypes                   = new TypeRefSet();
    private final Types gradeSystemRecordTypes             = new TypeRefSet();
    private final Types gradeSystemSearchRecordTypes       = new TypeRefSet();

    private final Types gradeEntryRecordTypes              = new TypeRefSet();
    private final Types gradeEntrySearchRecordTypes        = new TypeRefSet();

    private final Types gradebookColumnRecordTypes         = new TypeRefSet();
    private final Types gradebookColumnSearchRecordTypes   = new TypeRefSet();

    private final Types gradebookColumnSummaryRecordTypes  = new TypeRefSet();
    private final Types gradebookRecordTypes               = new TypeRefSet();
    private final Types gradebookSearchRecordTypes         = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractGradingManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractGradingManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if a grade system lookup service is supported. 
     *
     *  @return true if grade system lookup is supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSystemLookup() {
        return (false);
    }


    /**
     *  Tests if a grade system query service is supported. 
     *
     *  @return <code> true </code> if grade system query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSystemQuery() {
        return (false);
    }


    /**
     *  Tests if a grade system search service is supported. 
     *
     *  @return <code> true </code> if grade system search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSystemSearch() {
        return (false);
    }


    /**
     *  Tests if a grade system administrative service is supported. 
     *
     *  @return <code> true </code> if grade system admin is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSystemAdmin() {
        return (false);
    }


    /**
     *  Tests if grade system notification is supported. Messages may be sent 
     *  when grade entries are created, modified, or deleted. 
     *
     *  @return <code> true </code> if grade system notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSystemNotification() {
        return (false);
    }


    /**
     *  Tests if a grade system to gradebook lookup session is available. 
     *
     *  @return <code> true </code> if grade system gradebook lookup session 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSystemGradebook() {
        return (false);
    }


    /**
     *  Tests if a grade system to gradebook assignment session is available. 
     *
     *  @return <code> true </code> if grade system gradebook assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSystemGradebookAssignment() {
        return (false);
    }


    /**
     *  Tests if a grade system smart gradebook session is available. 
     *
     *  @return <code> true </code> if grade system smart gradebook is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSystemSmartGradebook() {
        return (false);
    }


    /**
     *  Tests if a grade entry lookup service is supported. 
     *
     *  @return true if grade entry lookup is supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsGradeEntryLookup() {
        return (false);
    }


    /**
     *  Tests if a grade entry query service is supported. 
     *
     *  @return true if grade entry query is supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsGradeEntryQuery() {
        return (false);
    }


    /**
     *  Tests if a grade entry search service is supported. 
     *
     *  @return <code> true </code> if grade entry search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeEntrySearch() {
        return (false);
    }


    /**
     *  Tests if a grade entry administrative service is supported. 
     *
     *  @return <code> true </code> if grade entry admin is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeEntryAdmin() {
        return (false);
    }


    /**
     *  Tests if grade entry notification is supported. 
     *
     *  @return <code> true </code> if grade entry notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeEntryNotification() {
        return (false);
    }


    /**
     *  Tests if a gradebook column lookup service is supported. 
     *
     *  @return true if gradebook column lookup is supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookColumnLookup() {
        return (false);
    }


    /**
     *  Tests if a gradebook column query service is supported. 
     *
     *  @return <code> true </code> if grade system query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookColumnQuery() {
        return (false);
    }


    /**
     *  Tests if a gradebook column search service is supported. 
     *
     *  @return <code> true </code> if grade system search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookColumnSearch() {
        return (false);
    }


    /**
     *  Tests if a gradebook column administrative service is supported. 
     *
     *  @return <code> true </code> if gradebook column admin is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookColumnAdmin() {
        return (false);
    }


    /**
     *  Tests if gradebook column notification is supported. Messages may be 
     *  sent when grade entries are created, modified, or deleted. 
     *
     *  @return <code> true </code> if gradebook column notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookColumnNotification() {
        return (false);
    }


    /**
     *  Tests if a gradebook column to gradebook lookup session is available. 
     *
     *  @return <code> true </code> if gradebook column gradebook lookup 
     *          session is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookColumnGradebook() {
        return (false);
    }


    /**
     *  Tests if a gradebook column to gradebook assignment session is 
     *  available. 
     *
     *  @return <code> true </code> if gradebook column gradebook assignment 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookColumnGradebookAssignment() {
        return (false);
    }


    /**
     *  Tests if a gradebook column smart gradebookt session is available. 
     *
     *  @return <code> true </code> if gradebook column amsrt gradebook is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookColumnSmartGradebook() {
        return (false);
    }


    /**
     *  Tests if a gradebook lookup service is supported. 
     *
     *  @return <code> true </code> if gradebook lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookLookup() {
        return (false);
    }


    /**
     *  Tests if a gradebook query service is supported. 
     *
     *  @return <code> true </code> if gradebook query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookQuery() {
        return (false);
    }


    /**
     *  Tests if a gradebook search service is supported. 
     *
     *  @return <code> true </code> if gradebook search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookSearch() {
        return (false);
    }


    /**
     *  Tests if a gradebook administrative service is supported. 
     *
     *  @return <code> true </code> if gradebook admin is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookAdmin() {
        return (false);
    }


    /**
     *  Tests if gradebook notification is supported. Messages may be sent 
     *  when gradebooks are created, modified, or deleted. 
     *
     *  @return <code> true </code> if gradebook notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookNotification() {
        return (false);
    }


    /**
     *  Tests if a gradebook hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if a gradebook hierarchy traversal is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookHierarchy() {
        return (false);
    }


    /**
     *  Tests if gradebook hierarchy design is supported. 
     *
     *  @return <code> true </code> if a gradebook hierarchy design is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests if a grading batch service is supported. 
     *
     *  @return <code> true </code> if a grading batch service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradingBatch() {
        return (false);
    }


    /**
     *  Tests if a grading calculation service is supported. 
     *
     *  @return <code> true </code> if a grading calculation service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradingCalculation() {
        return (false);
    }


    /**
     *  Tests if a grade system transform service is supported. 
     *
     *  @return <code> true </code> if a grading transform service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradingTransform() {
        return (false);
    }


    /**
     *  Gets the supported <code> Grade </code> record types. 
     *
     *  @return a list containing the supported <code> Grade </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getGradeRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.gradeRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Grade </code> record type is supported. 
     *
     *  @param  gradeRecordType a <code> Type </code> indicating a <code> 
     *          Grade </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> gradeRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsGradeRecordType(org.osid.type.Type gradeRecordType) {
        return (this.gradeRecordTypes.contains(gradeRecordType));
    }


    /**
     *  Adds support for a grade record type.
     *
     *  @param gradeRecordType a grade record type
     *  @throws org.osid.NullArgumentException
     *  <code>gradeRecordType</code> is <code>null</code>
     */

    protected void addGradeRecordType(org.osid.type.Type gradeRecordType) {
        this.gradeRecordTypes.add(gradeRecordType);
        return;
    }


    /**
     *  Removes support for a grade record type.
     *
     *  @param gradeRecordType a grade record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>gradeRecordType</code> is <code>null</code>
     */

    protected void removeGradeRecordType(org.osid.type.Type gradeRecordType) {
        this.gradeRecordTypes.remove(gradeRecordType);
        return;
    }


    /**
     *  Gets the supported <code> GradeSystem </code> record types. 
     *
     *  @return a list containing the supported <code> GradeSystem </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getGradeSystemRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.gradeSystemRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> GradeSystem </code> record type is 
     *  supported. 
     *
     *  @param  gradeSystemRecordType a <code> Type </code> indicating a 
     *          <code> GradeSystem </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> gradeSystemRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsGradeSystemRecordType(org.osid.type.Type gradeSystemRecordType) {
        return (this.gradeSystemRecordTypes.contains(gradeSystemRecordType));
    }


    /**
     *  Adds support for a grade system record type.
     *
     *  @param gradeSystemRecordType a grade system record type
     *  @throws org.osid.NullArgumentException
     *  <code>gradeSystemRecordType</code> is <code>null</code>
     */

    protected void addGradeSystemRecordType(org.osid.type.Type gradeSystemRecordType) {
        this.gradeSystemRecordTypes.add(gradeSystemRecordType);
        return;
    }


    /**
     *  Removes support for a grade system record type.
     *
     *  @param gradeSystemRecordType a grade system record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>gradeSystemRecordType</code> is <code>null</code>
     */

    protected void removeGradeSystemRecordType(org.osid.type.Type gradeSystemRecordType) {
        this.gradeSystemRecordTypes.remove(gradeSystemRecordType);
        return;
    }


    /**
     *  Gets the supported <code> GradeSystem </code> search record types. 
     *
     *  @return a list containing the supported <code> GradeSystem </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getGradeSystemSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.gradeSystemSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> GradeSystem </code> search record type is 
     *  supported. 
     *
     *  @param  gradeSystemSearchRecordType a <code> Type </code> indicating a 
     *          <code> GradeSystem </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          gradeSystemSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsGradeSystemSearchRecordType(org.osid.type.Type gradeSystemSearchRecordType) {
        return (this.gradeSystemSearchRecordTypes.contains(gradeSystemSearchRecordType));
    }


    /**
     *  Adds support for a grade system search record type.
     *
     *  @param gradeSystemSearchRecordType a grade system search record type
     *  @throws org.osid.NullArgumentException
     *  <code>gradeSystemSearchRecordType</code> is <code>null</code>
     */

    protected void addGradeSystemSearchRecordType(org.osid.type.Type gradeSystemSearchRecordType) {
        this.gradeSystemSearchRecordTypes.add(gradeSystemSearchRecordType);
        return;
    }


    /**
     *  Removes support for a grade system search record type.
     *
     *  @param gradeSystemSearchRecordType a grade system search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>gradeSystemSearchRecordType</code> is <code>null</code>
     */

    protected void removeGradeSystemSearchRecordType(org.osid.type.Type gradeSystemSearchRecordType) {
        this.gradeSystemSearchRecordTypes.remove(gradeSystemSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> GradeEntry </code> record types. 
     *
     *  @return a list containing the supported <code> GradeEntry </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getGradeEntryRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.gradeEntryRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> GradeEntry </code> record type is supported. 
     *
     *  @param  gradeEntryRecordType a <code> Type </code> indicating a <code> 
     *          GradeEntry </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> gradeEntryRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsGradeEntryRecordType(org.osid.type.Type gradeEntryRecordType) {
        return (this.gradeEntryRecordTypes.contains(gradeEntryRecordType));
    }


    /**
     *  Adds support for a grade entry record type.
     *
     *  @param gradeEntryRecordType a grade entry record type
     *  @throws org.osid.NullArgumentException
     *  <code>gradeEntryRecordType</code> is <code>null</code>
     */

    protected void addGradeEntryRecordType(org.osid.type.Type gradeEntryRecordType) {
        this.gradeEntryRecordTypes.add(gradeEntryRecordType);
        return;
    }


    /**
     *  Removes support for a grade entry record type.
     *
     *  @param gradeEntryRecordType a grade entry record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>gradeEntryRecordType</code> is <code>null</code>
     */

    protected void removeGradeEntryRecordType(org.osid.type.Type gradeEntryRecordType) {
        this.gradeEntryRecordTypes.remove(gradeEntryRecordType);
        return;
    }


    /**
     *  Gets the supported <code> GradeEntry </code> search record types. 
     *
     *  @return a list containing the supported <code> GradeEntry </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getGradeEntrySearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.gradeEntrySearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> GradeEntry </code> search record type is 
     *  supported. 
     *
     *  @param  gradeEntrySearchRecordType a <code> Type </code> indicating a 
     *          <code> GradeEntry </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          gradeEntrySearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsGradeEntrySearchRecordType(org.osid.type.Type gradeEntrySearchRecordType) {
        return (this.gradeEntrySearchRecordTypes.contains(gradeEntrySearchRecordType));
    }


    /**
     *  Adds support for a grade entry search record type.
     *
     *  @param gradeEntrySearchRecordType a grade entry search record type
     *  @throws org.osid.NullArgumentException
     *  <code>gradeEntrySearchRecordType</code> is <code>null</code>
     */

    protected void addGradeEntrySearchRecordType(org.osid.type.Type gradeEntrySearchRecordType) {
        this.gradeEntrySearchRecordTypes.add(gradeEntrySearchRecordType);
        return;
    }


    /**
     *  Removes support for a grade entry search record type.
     *
     *  @param gradeEntrySearchRecordType a grade entry search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>gradeEntrySearchRecordType</code> is <code>null</code>
     */

    protected void removeGradeEntrySearchRecordType(org.osid.type.Type gradeEntrySearchRecordType) {
        this.gradeEntrySearchRecordTypes.remove(gradeEntrySearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> GradebookColumn </code> record types. 
     *
     *  @return a list containing the supported <code> GradebookColumn </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getGradebookColumnRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.gradebookColumnRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> GradebookColumn </code> record type is 
     *  supported. 
     *
     *  @param  gradebookColumnRecordType a <code> Type </code> indicating a 
     *          <code> GradebookColumn </code> type 
     *  @return <code> true </code> if the given gradebook column record 
     *          <code> Type </code> is supported, <code> false </code> 
     *          otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          gradebookColumnRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsGradebookColumnRecordType(org.osid.type.Type gradebookColumnRecordType) {
        return (this.gradebookColumnRecordTypes.contains(gradebookColumnRecordType));
    }


    /**
     *  Adds support for a gradebook column record type.
     *
     *  @param gradebookColumnRecordType a gradebook column record type
     *  @throws org.osid.NullArgumentException
     *  <code>gradebookColumnRecordType</code> is <code>null</code>
     */

    protected void addGradebookColumnRecordType(org.osid.type.Type gradebookColumnRecordType) {
        this.gradebookColumnRecordTypes.add(gradebookColumnRecordType);
        return;
    }


    /**
     *  Removes support for a gradebook column record type.
     *
     *  @param gradebookColumnRecordType a gradebook column record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>gradebookColumnRecordType</code> is <code>null</code>
     */

    protected void removeGradebookColumnRecordType(org.osid.type.Type gradebookColumnRecordType) {
        this.gradebookColumnRecordTypes.remove(gradebookColumnRecordType);
        return;
    }


    /**
     *  Gets the supported gradebook column search record types. 
     *
     *  @return a list containing the supported <code> GradebookColumn </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getGradebookColumnSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.gradebookColumnSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given gradebook column search record type is supported. 
     *
     *  @param  gradebookColumnSearchRecordType a <code> Type </code> 
     *          indicating a <code> GradebookColumn </code> search record type 
     *  @return <code> true </code> if the given search record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          gradebookColumnSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsGradebookColumnSearchRecordType(org.osid.type.Type gradebookColumnSearchRecordType) {
        return (this.gradebookColumnSearchRecordTypes.contains(gradebookColumnSearchRecordType));
    }


    /**
     *  Adds support for a gradebook column search record type.
     *
     *  @param gradebookColumnSearchRecordType a gradebook column search record type
     *  @throws org.osid.NullArgumentException
     *  <code>gradebookColumnSearchRecordType</code> is <code>null</code>
     */

    protected void addGradebookColumnSearchRecordType(org.osid.type.Type gradebookColumnSearchRecordType) {
        this.gradebookColumnSearchRecordTypes.add(gradebookColumnSearchRecordType);
        return;
    }


    /**
     *  Removes support for a gradebook column search record type.
     *
     *  @param gradebookColumnSearchRecordType a gradebook column search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>gradebookColumnSearchRecordType</code> is <code>null</code>
     */

    protected void removeGradebookColumnSearchRecordType(org.osid.type.Type gradebookColumnSearchRecordType) {
        this.gradebookColumnSearchRecordTypes.remove(gradebookColumnSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> GradebookColumnSummary </code> record types. 
     *
     *  @return a list containing the supported <code> GradebookColumnSummary 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getGradebookColumnSummaryRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.gradebookColumnSummaryRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> GradebookColumnSummary </code> record type 
     *  is supported. 
     *
     *  @param  gradebookColumnSummaryRecordType a <code> Type </code> 
     *          indicating a <code> GradebookColumnSummary </code> type 
     *  @return <code> true </code> if the given gradebook column summary 
     *          record <code> Type </code> is supported, <code> false </code> 
     *          otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          gradebookColumnRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsGradebookColumnSummaryRecordType(org.osid.type.Type gradebookColumnSummaryRecordType) {
        return (this.gradebookColumnSummaryRecordTypes.contains(gradebookColumnSummaryRecordType));
    }


    /**
     *  Adds support for a gradebook column summary record type.
     *
     *  @param gradebookColumnSummaryRecordType a gradebook column summary record type
     *  @throws org.osid.NullArgumentException
     *  <code>gradebookColumnSummaryRecordType</code> is <code>null</code>
     */

    protected void addGradebookColumnSummaryRecordType(org.osid.type.Type gradebookColumnSummaryRecordType) {
        this.gradebookColumnSummaryRecordTypes.add(gradebookColumnSummaryRecordType);
        return;
    }


    /**
     *  Removes support for a gradebook column summary record type.
     *
     *  @param gradebookColumnSummaryRecordType a gradebook column summary record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>gradebookColumnSummaryRecordType</code> is <code>null</code>
     */

    protected void removeGradebookColumnSummaryRecordType(org.osid.type.Type gradebookColumnSummaryRecordType) {
        this.gradebookColumnSummaryRecordTypes.remove(gradebookColumnSummaryRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Gradebook </code> record types. 
     *
     *  @return a list containing the supported <code> Gradebook </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getGradebookRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.gradebookRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Gradebook </code> record type is supported. 
     *
     *  @param  gradebookRecordType a <code> Type </code> indicating a <code> 
     *          Gradebook </code> type 
     *  @return <code> true </code> if the given gradebook record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> gradebookRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsGradebookRecordType(org.osid.type.Type gradebookRecordType) {
        return (this.gradebookRecordTypes.contains(gradebookRecordType));
    }


    /**
     *  Adds support for a gradebook record type.
     *
     *  @param gradebookRecordType a gradebook record type
     *  @throws org.osid.NullArgumentException
     *  <code>gradebookRecordType</code> is <code>null</code>
     */

    protected void addGradebookRecordType(org.osid.type.Type gradebookRecordType) {
        this.gradebookRecordTypes.add(gradebookRecordType);
        return;
    }


    /**
     *  Removes support for a gradebook record type.
     *
     *  @param gradebookRecordType a gradebook record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>gradebookRecordType</code> is <code>null</code>
     */

    protected void removeGradebookRecordType(org.osid.type.Type gradebookRecordType) {
        this.gradebookRecordTypes.remove(gradebookRecordType);
        return;
    }


    /**
     *  Gets the supported gradebook search record types. 
     *
     *  @return a list containing the supported <code> Gradebook </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getGradebookSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.gradebookSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given gradebook search record type is supported. 
     *
     *  @param  gradebookSearchRecordType a <code> Type </code> indicating a 
     *          <code> Gradebook </code> search record type 
     *  @return <code> true </code> if the given search record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          gradebookSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsGradebookSearchRecordType(org.osid.type.Type gradebookSearchRecordType) {
        return (this.gradebookSearchRecordTypes.contains(gradebookSearchRecordType));
    }


    /**
     *  Adds support for a gradebook search record type.
     *
     *  @param gradebookSearchRecordType a gradebook search record type
     *  @throws org.osid.NullArgumentException
     *  <code>gradebookSearchRecordType</code> is <code>null</code>
     */

    protected void addGradebookSearchRecordType(org.osid.type.Type gradebookSearchRecordType) {
        this.gradebookSearchRecordTypes.add(gradebookSearchRecordType);
        return;
    }


    /**
     *  Removes support for a gradebook search record type.
     *
     *  @param gradebookSearchRecordType a gradebook search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>gradebookSearchRecordType</code> is <code>null</code>
     */

    protected void removeGradebookSearchRecordType(org.osid.type.Type gradebookSearchRecordType) {
        this.gradebookSearchRecordTypes.remove(gradebookSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade system 
     *  lookup service. 
     *
     *  @return a <code> GradeSystemLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemLookupSession getGradeSystemLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradeSystemLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade system 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GradeSystemLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemLookupSession getGradeSystemLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradeSystemLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade system 
     *  lookup service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @return <code> a GradeSystemLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemLookupSession getGradeSystemLookupSessionForGradebook(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradeSystemLookupSessionForGradebook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade system 
     *  lookup service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @param  proxy a proxy 
     *  @return <code> a GradeSystemLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemLookupSession getGradeSystemLookupSessionForGradebook(org.osid.id.Id gradebookId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradeSystemLookupSessionForGradebook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade system 
     *  query service. 
     *
     *  @return a <code> GradeSystemQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQuerySession getGradeSystemQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradeSystemQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade system 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GradeSystemQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQuerySession getGradeSystemQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradeSystemQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade system 
     *  query service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @return <code> a GradeSystemQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQuerySession getGradeSystemQuerySessionForGradebook(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradeSystemQuerySessionForGradebook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade system 
     *  query service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @param  proxy a proxy 
     *  @return <code> a GradeSystemQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQuerySession getGradeSystemQuerySessionForGradebook(org.osid.id.Id gradebookId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradeSystemQuerySessionForGradebook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade system 
     *  search service. 
     *
     *  @return a <code> GradeSystemSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemSearchSession getGradeSystemSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradeSystemSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade system 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GradeSystemSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemSearchSession getGradeSystemSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradeSystemSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade system 
     *  search service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @return <code> a GradeSystemSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemSearchSession getGradeSystemSearchSessionForGradebook(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradeSystemSearchSessionForGradebook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade system 
     *  search service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @param  proxy a proxy 
     *  @return <code> a GradeSystemSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemSearchSession getGradeSystemSearchSessionForGradebook(org.osid.id.Id gradebookId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradeSystemSearchSessionForGradebook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade system 
     *  administration service. 
     *
     *  @return a <code> GradeSystemAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemAdminSession getGradeSystemAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradeSystemAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade system 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GradeSystemAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemAdminSession getGradeSystemAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradeSystemAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade system 
     *  admin service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @return <code> a GradeSystemAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemAdminSession getGradeSystemAdminSessionForGradebook(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradeSystemAdminSessionForGradebook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade system 
     *  admin service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @param  proxy a proxy 
     *  @return <code> a GradeSystemAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemAdminSession getGradeSystemAdminSessionForGradebook(org.osid.id.Id gradebookId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradeSystemAdminSessionForGradebook not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to grade 
     *  system changes. 
     *
     *  @param  gradeSystemReceiver the grade system receiver 
     *  @return a <code> GradeSystemNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> gradeSystemReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemNotificationSession getGradeSystemNotificationSession(org.osid.grading.GradeSystemReceiver gradeSystemReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradeSystemNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to grade 
     *  system changes. 
     *
     *  @param  gradeSystemReceiver the grade system receiver 
     *  @param  proxy a proxy 
     *  @return a <code> GradeSystemNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> gradeSystemReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemNotificationSession getGradeSystemNotificationSession(org.osid.grading.GradeSystemReceiver gradeSystemReceiver, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradeSystemNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade system 
     *  notification service for the given gradebook. 
     *
     *  @param  gradeSystemReceiver the grade system receiver 
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @return <code> a GradeSystemNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradeSystemReceiver 
     *          </code> or <code> gradebookId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemNotificationSession getGradeSystemNotificationSessionForGradebook(org.osid.grading.GradeSystemReceiver gradeSystemReceiver, 
                                                                                                         org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradeSystemNotificationSessionForGradebook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade system 
     *  notification service for the given gradebook. 
     *
     *  @param  gradeSystemReceiver the grade system receiver 
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @param  proxy a proxy 
     *  @return <code> a GradeSystemNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradeSystemReceiver, 
     *          gradebookId </code> or <code> porxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemNotificationSession getGradeSystemNotificationSessionForGradebook(org.osid.grading.GradeSystemReceiver gradeSystemReceiver, 
                                                                                                         org.osid.id.Id gradebookId, 
                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradeSystemNotificationSessionForGradebook not implemented");
    }


    /**
     *  Gets the session for retrieving grade system to gradebook mappings. 
     *
     *  @return a <code> GradeSystemGradebookSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemGradebook() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemGradebookSession getGradeSystemGradebookSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradeSystemGradebookSession not implemented");
    }


    /**
     *  Gets the session for retrieving grade system to gradebook mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GradeSystemGradebookSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemGradebook() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemGradebookSession getGradeSystemGradebookSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradeSystemGradebookSession not implemented");
    }


    /**
     *  Gets the session for assigning grade system to gradebook mappings. 
     *
     *  @return a <code> GradeSystemGradebookAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemGradebookAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemGradebookSession getGradeSystemGradebookAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradeSystemGradebookAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning grade system to gradebook mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GradeSystemGradebookAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemGradebookAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemGradebookSession getGradeSystemGradebookAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradeSystemGradebookAssignmentSession not implemented");
    }


    /**
     *  Gets the session for managing smart gradebooks of grade systems. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @return a <code> GradeSystemSmartGradebookSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemSmartGradebook() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemSmartGradebookSession getGradeSystemSmartGradebookSession(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradeSystemSmartGradebookSession not implemented");
    }


    /**
     *  Gets the session for managing smart gradebooks of grade systems. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @param  proxy a proxy 
     *  @return a <code> GradeSystemSmartGradebookSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemSmartGradebook() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemSmartGradebookSession getGradeSystemSmartGradebookSession(org.osid.id.Id gradebookId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradeSystemSmartGradebookSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade entry 
     *  lookup service. 
     *
     *  @return a <code> GradeEntryLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeEntryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryLookupSession getGradeEntryLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradeEntryLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade entry 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GradeEntryLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeEntryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryLookupSession getGradeEntryLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradeEntryLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade entry 
     *  lookup service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @return <code> a GradeEntryLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeEntryLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryLookupSession getGradeEntryLookupSessionForGradebook(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradeEntryLookupSessionForGradebook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade entry 
     *  lookup service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @param  proxy a proxy 
     *  @return <code> a GradeEntryLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeEntryLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryLookupSession getGradeEntryLookupSessionForGradebook(org.osid.id.Id gradebookId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradeEntryLookupSessionForGradebook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade entry 
     *  query service. 
     *
     *  @return a <code> GradeEntryQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryQuerySession getGradeEntryQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradeEntryQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade entry 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GradeEntryQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeEntryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryQuerySession getGradeEntryQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradeEntryQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade entry 
     *  query service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @return <code> a GradeEntryQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeEntryQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryQuerySession getGradeEntryQuerySessionForGradebook(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradeEntryQuerySessionForGradebook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade entry 
     *  query service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @param  proxy a proxy 
     *  @return <code> a GradeEntryQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeEntryQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryQuerySession getGradeEntryQuerySessionForGradebook(org.osid.id.Id gradebookId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradeEntryQuerySessionForGradebook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade entry 
     *  search service. 
     *
     *  @return a <code> GradeEntrySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeEntrySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeEntrySearchSession getGradeEntrySearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradeEntrySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade entry 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GradeEntrySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeEntrySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeEntrySearchSession getGradeEntrySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradeEntrySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade entry 
     *  search service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @return <code> a GradeEntrySearchSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeEntrySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeEntrySearchSession getGradeEntrySearchSessionForGradebook(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradeEntrySearchSessionForGradebook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade entry 
     *  search service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @param  proxy a proxy 
     *  @return <code> a GradeEntrySearchSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeEntrySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeEntrySearchSession getGradeEntrySearchSessionForGradebook(org.osid.id.Id gradebookId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradeEntrySearchSessionForGradebook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade entry 
     *  administration service. 
     *
     *  @return a <code> GradeEntryAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeEntryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryAdminSession getGradeEntryAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradeEntryAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade entry 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GradeEntryAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeEntryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryAdminSession getGradeEntryAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradeEntryAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade entry 
     *  admin service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @return <code> a GradeEntryAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeEntryAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryAdminSession getGradeEntryAdminSessionForGradebook(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradeEntryAdminSessionForGradebook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade entry 
     *  admin service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @param  proxy a proxy 
     *  @return <code> a GradeEntryAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeEntryAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryAdminSession getGradeEntryAdminSessionForGradebook(org.osid.id.Id gradebookId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradeEntryAdminSessionForGradebook not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to grade 
     *  entry changes. 
     *
     *  @param  receiver the grade entry receiver 
     *  @return a <code> GradeEntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> receiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeEntryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryNotificationSession getGradeEntryNotificationSession(org.osid.grading.GradeEntryReceiver receiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradeEntryNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to grade 
     *  entry changes. 
     *
     *  @param  gradeEntryReceiver the grade entry receiver 
     *  @param  proxy a proxy 
     *  @return a <code> GradeEntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> gradeEntryReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeEntryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryNotificationSession getGradeEntryNotificationSession(org.osid.grading.GradeEntryReceiver gradeEntryReceiver, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradeEntryNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade entry 
     *  notification service for the given gradebook. 
     *
     *  @param  receiver the grade entry receiver 
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @return <code> a GradeEntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> receiver </code> or 
     *          <code> gradebookId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryNotificationSession getGradeEntryNotificationSessionForGradebook(org.osid.grading.GradeEntryReceiver receiver, 
                                                                                                       org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradeEntryNotificationSessionForGradebook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade entry 
     *  notification service for the given gradebook. 
     *
     *  @param  gradeEntryReceiver the grade entry receiver 
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @param  proxy a proxy 
     *  @return <code> a GradeEntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradeEntryReceiver, 
     *          gradebookId </code> or <code> porxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryNotificationSession getGradeEntryNotificationSessionForGradebook(org.osid.grading.GradeEntryReceiver gradeEntryReceiver, 
                                                                                                       org.osid.id.Id gradebookId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradeEntryNotificationSessionForGradebook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the gradebook 
     *  column lookup service. 
     *
     *  @return a <code> GradebookColumnLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnLookupSession getGradebookColumnLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradebookColumnLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the gradebook 
     *  column lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GradebookColumnLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnLookupSession getGradebookColumnLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradebookColumnLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the gradebook 
     *  column lookup service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @return <code> a GradebookColumnLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnLookupSession getGradebookColumnLookupSessionForGradebook(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradebookColumnLookupSessionForGradebook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the gradebook 
     *  column lookup service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @param  proxy a proxy 
     *  @return <code> a GradebookColumnLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnLookupSession getGradebookColumnLookupSessionForGradebook(org.osid.id.Id gradebookId, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradebookColumnLookupSessionForGradebook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the gradebook 
     *  column query service. 
     *
     *  @return a <code> GradebookColumnQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnQuerySession getGradebookColumnQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradebookColumnQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the gradebook 
     *  column query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GradebookColumnQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnQuerySession getGradebookColumnQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradebookColumnQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the gradebook 
     *  column query service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @return <code> a GradebookColumnQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnQuerySession getGradebookColumnQuerySessionForGradebook(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradebookColumnQuerySessionForGradebook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the gradebook 
     *  column query service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @param  proxy a proxy 
     *  @return a <code> GradebookColumnQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnQuerySession getGradebookColumnQuerySessionForGradebook(org.osid.id.Id gradebookId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradebookColumnQuerySessionForGradebook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the gradebook 
     *  column search service. 
     *
     *  @return a <code> GradebookColumnSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnSearchSession getGradebookColumnSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradebookColumnSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the gradebook 
     *  column search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GradebookColumnSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnSearchSession getGradebookColumnSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradebookColumnSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the gradebook 
     *  column search service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @return <code> a GradebookColumnSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnSearchSession getGradebookColumnSearchSessionForGradebook(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradebookColumnSearchSessionForGradebook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the gradebook 
     *  column search service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @param  proxy a proxy 
     *  @return <code> a GradebookColumnSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnSearchSession getGradebookColumnSearchSessionForGradebook(org.osid.id.Id gradebookId, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradebookColumnSearchSessionForGradebook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the gradebook 
     *  column administration service. 
     *
     *  @return a <code> GradebookColumnAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnAdminSession getGradebookColumnAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradebookColumnAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the gradebook 
     *  column administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GradebookColumnAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnAdminSession getGradebookColumnAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradebookColumnAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the gradebook 
     *  column admin service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @return <code> a GradebookColumnAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnAdminSession getGradebookColumnAdminSessionForGradebook(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradebookColumnAdminSessionForGradebook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the gradebook 
     *  column admin service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @param  proxy a proxy 
     *  @return <code> a GradebookColumnAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnAdminSession getGradebookColumnAdminSessionForGradebook(org.osid.id.Id gradebookId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradebookColumnAdminSessionForGradebook not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to 
     *  gradebook column changes. 
     *
     *  @param  gradebookColumnReceiver the grade system receiver 
     *  @return a <code> GradebookColumnNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> gradebookColumnReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnNotificationSession getGradebookColumnNotificationSession(org.osid.grading.GradebookColumnReceiver gradebookColumnReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradebookColumnNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to 
     *  gradebook column changes. 
     *
     *  @param  gradebookColumnReceiver the grade system receiver 
     *  @param  proxy a proxy 
     *  @return a <code> GradebookColumnNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> gradebookColumnReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnNotificationSession getGradebookColumnNotificationSession(org.osid.grading.GradebookColumnReceiver gradebookColumnReceiver, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradebookColumnNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the gradebook 
     *  column notification service for the given gradebook. 
     *
     *  @param  gradebookColumnReceiver the gradebook column receiver 
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @return <code> a GradebookColumnNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookColumnReceiver 
     *          </code> or <code> gradebookId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnNotificationSession getGradebookColumnNotificationSessionForGradebook(org.osid.grading.GradebookColumnReceiver gradebookColumnReceiver, 
                                                                                                                 org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradebookColumnNotificationSessionForGradebook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the gradebook 
     *  column notification service for the given gradebook. 
     *
     *  @param  gradebookColumnReceiver the gradebook column receiver 
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @param  proxy a proxy 
     *  @return <code> a GradebookColumnNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookColumnReceiver, 
     *          gradebookId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnNotificationSession getGradebookColumnNotificationSessionForGradebook(org.osid.grading.GradebookColumnReceiver gradebookColumnReceiver, 
                                                                                                                 org.osid.id.Id gradebookId, 
                                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradebookColumnNotificationSessionForGradebook not implemented");
    }


    /**
     *  Gets the session for retrieving gradebook column to gradebook 
     *  mappings. 
     *
     *  @return a <code> GradebookColumnGradebookSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnGradebook() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnGradebookSession getGradebookColumnGradebookSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradebookColumnGradebookSession not implemented");
    }


    /**
     *  Gets the session for retrieving gradebook column to gradebook 
     *  mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GradebookColumnGradebookSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnGradebook() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnGradebookSession getGradebookColumnGradebookSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradebookColumnGradebookSession not implemented");
    }


    /**
     *  Gets the session for assigning gradebook column to gradebook mappings. 
     *
     *  @return a <code> GradebookColumnGradebookAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnGradebookAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnGradebookAssignmentSession getGradebookColumnGradebookAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradebookColumnGradebookAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning gradebook column to gradebook mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GradebookColumnGradebookAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnGradebookAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnGradebookAssignmentSession getGradebookColumnGradebookAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradebookColumnGradebookAssignmentSession not implemented");
    }


    /**
     *  Gets the session for managing smart gradebooks of gradebook columns. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @return a <code> GradebookColumnSmartGradebookSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnSmartGradebook() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnSmartGradebookSession getGradebookColumnSmartGradebookSession(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradebookColumnSmartGradebookSession not implemented");
    }


    /**
     *  Gets the session for managing smart gradebooks of gradebook columns. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @param  proxy a proxy 
     *  @return a <code> GradebookColumnSmartGradebookSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnSmartGradebook() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnSmartGradebookSession getGradebookColumnSmartGradebookSession(org.osid.id.Id gradebookId, 
                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradebookColumnSmartGradebookSession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the gradebook lookup service. 
     *
     *  @return a <code> GradebookLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookLookup() is false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookLookupSession getGradebookLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradebookLookupSession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the gradebook lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GradebookLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookLookup() is false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookLookupSession getGradebookLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradebookLookupSession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the gradebook query service. 
     *
     *  @return a <code> GradebookQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookQuery() is false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookQuerySession getGradebookQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradebookQuerySession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the gradebook query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GradebookQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookQuery() is false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookQuerySession getGradebookQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradebookQuerySession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the gradebook search service. 
     *
     *  @return a <code> GradebookSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookSearch() is false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookSearchSession getGradebookSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradebookSearchSession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the gradebook search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GradebookSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookSearch() is false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookSearchSession getGradebookSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradebookSearchSession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the gradebook administration 
     *  service. 
     *
     *  @return a <code> GradebookAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookAdmin() is false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookAdminSession getGradebookAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradebookAdminSession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the gradebook administration 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GradebookAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookAdmin() is false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookAdminSession getGradebookAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradebookAdminSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to 
     *  gradebook service changes. 
     *
     *  @param  gradebookReceiver the gradebook receiver 
     *  @return a <code> GradebookNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> gradebookReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookNotification() is false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookNotificationSession getGradebookNotificationSession(org.osid.grading.GradebookReceiver gradebookReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradebookNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to 
     *  gradebook service changes. 
     *
     *  @param  gradebookReceiver the gradebook receiver 
     *  @param  proxy a proxy 
     *  @return a <code> GradebookNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> gradebookReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookNotification() is false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookNotificationSession getGradebookNotificationSession(org.osid.grading.GradebookReceiver gradebookReceiver, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradebookNotificationSession not implemented");
    }


    /**
     *  Gets the session traversing gradebook hierarchies. 
     *
     *  @return a <code> GradebookHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookHierarchy() is false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookHierarchySession getGradebookHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradebookHierarchySession not implemented");
    }


    /**
     *  Gets the session traversing gradebook hierarchies. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GradebookHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookHierarchy() is false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookHierarchySession getGradebookHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradebookHierarchySession not implemented");
    }


    /**
     *  Gets the session designing gradebook hierarchies. 
     *
     *  @return a <code> GradebookHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookHierarchyDesign() is false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookHierarchyDesignSession getGradebookHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradebookHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the session designing gradebook hierarchies. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GradebookHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookHierarchyDesign() is false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookHierarchyDesignSession getGradebookHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradebookHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> GradingBatchManager. </code> 
     *
     *  @return a <code> GradingBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsGradingBatch() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.grading.batch.GradingBatchManager getGradingBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradingBatchManager not implemented");
    }


    /**
     *  Gets the <code> GradingBatchProxyManager. </code> 
     *
     *  @return a <code> GradingBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsGradingBatch() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.grading.batch.GradingBatchProxyManager getGradingBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradingBatchProxyManager not implemented");
    }


    /**
     *  Gets the <code> GradingCalculationManager. </code> 
     *
     *  @return a <code> GradingCalculationManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradingCalculation() is false </code> 
     */

    @OSID @Override
    public org.osid.grading.calculation.GradingCalculationManager getGradingCalculationManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradingCalculationManager not implemented");
    }


    /**
     *  Gets the <code> GradingCalculationProxyManager. </code> 
     *
     *  @return a <code> GradingCalculationProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradingCalculation() is false </code> 
     */

    @OSID @Override
    public org.osid.grading.calculation.GradingCalculationProxyManager getGradingCalculationProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradingCalculationProxyManager not implemented");
    }


    /**
     *  Gets the <code> GradingTransformManager. </code> 
     *
     *  @return a <code> GradingTransformManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradingTransform() is false </code> 
     */

    @OSID @Override
    public org.osid.grading.transform.GradingTransformManager getGradingTransformManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingManager.getGradingTransformManager not implemented");
    }


    /**
     *  Gets the <code> GradingTransformProxyManager. </code> 
     *
     *  @return a <code> GradingTransformManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradingTransform() is false </code> 
     */

    @OSID @Override
    public org.osid.grading.transform.GradingTransformProxyManager getGradingTransformProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.GradingProxyManager.getGradingTransformProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.gradeRecordTypes.clear();
        this.gradeRecordTypes.clear();

        this.gradeSystemRecordTypes.clear();
        this.gradeSystemRecordTypes.clear();

        this.gradeSystemSearchRecordTypes.clear();
        this.gradeSystemSearchRecordTypes.clear();

        this.gradeEntryRecordTypes.clear();
        this.gradeEntryRecordTypes.clear();

        this.gradeEntrySearchRecordTypes.clear();
        this.gradeEntrySearchRecordTypes.clear();

        this.gradebookColumnRecordTypes.clear();
        this.gradebookColumnRecordTypes.clear();

        this.gradebookColumnSearchRecordTypes.clear();
        this.gradebookColumnSearchRecordTypes.clear();

        this.gradebookColumnSummaryRecordTypes.clear();
        this.gradebookColumnSummaryRecordTypes.clear();

        this.gradebookRecordTypes.clear();
        this.gradebookRecordTypes.clear();

        this.gradebookSearchRecordTypes.clear();
        this.gradebookSearchRecordTypes.clear();

        return;
    }
}
