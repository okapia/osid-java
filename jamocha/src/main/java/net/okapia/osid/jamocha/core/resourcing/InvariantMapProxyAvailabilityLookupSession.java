//
// InvariantMapProxyAvailabilityLookupSession
//
//    Implements an Availability lookup service backed by a fixed
//    collection of availabilities. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing;


/**
 *  Implements an Availability lookup service backed by a fixed
 *  collection of availabilities. The availabilities are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyAvailabilityLookupSession
    extends net.okapia.osid.jamocha.core.resourcing.spi.AbstractMapAvailabilityLookupSession
    implements org.osid.resourcing.AvailabilityLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyAvailabilityLookupSession} with no
     *  availabilities.
     *
     *  @param foundry the foundry
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyAvailabilityLookupSession(org.osid.resourcing.Foundry foundry,
                                                  org.osid.proxy.Proxy proxy) {
        setFoundry(foundry);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyAvailabilityLookupSession} with a single
     *  availability.
     *
     *  @param foundry the foundry
     *  @param availability an single availability
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry},
     *          {@code availability} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyAvailabilityLookupSession(org.osid.resourcing.Foundry foundry,
                                                  org.osid.resourcing.Availability availability, org.osid.proxy.Proxy proxy) {

        this(foundry, proxy);
        putAvailability(availability);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyAvailabilityLookupSession} using
     *  an array of availabilities.
     *
     *  @param foundry the foundry
     *  @param availabilities an array of availabilities
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry},
     *          {@code availabilities} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyAvailabilityLookupSession(org.osid.resourcing.Foundry foundry,
                                                  org.osid.resourcing.Availability[] availabilities, org.osid.proxy.Proxy proxy) {

        this(foundry, proxy);
        putAvailabilities(availabilities);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyAvailabilityLookupSession} using a
     *  collection of availabilities.
     *
     *  @param foundry the foundry
     *  @param availabilities a collection of availabilities
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry},
     *          {@code availabilities} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyAvailabilityLookupSession(org.osid.resourcing.Foundry foundry,
                                                  java.util.Collection<? extends org.osid.resourcing.Availability> availabilities,
                                                  org.osid.proxy.Proxy proxy) {

        this(foundry, proxy);
        putAvailabilities(availabilities);
        return;
    }
}
