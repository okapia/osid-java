//
// AbstractMapQueryInspector.java
//
//     A template for making a MapQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.map.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for maps.
 */

public abstract class AbstractMapQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.mapping.MapQueryInspector {

    private final java.util.Collection<org.osid.mapping.records.MapQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the location <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLocationIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the location query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.LocationQueryInspector[] getLocationTerms() {
        return (new org.osid.mapping.LocationQueryInspector[0]);
    }


    /**
     *  Gets the path <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPathIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the path query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.path.PathQueryInspector[] getPathTerms() {
        return (new org.osid.mapping.path.PathQueryInspector[0]);
    }


    /**
     *  Gets the route <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRouteIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the route query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteQueryInspector[] getRouteTerms() {
        return (new org.osid.mapping.route.RouteQueryInspector[0]);
    }


    /**
     *  Gets the ancestor map <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorMapIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor map query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.MapQueryInspector[] getAncestorMapTerms() {
        return (new org.osid.mapping.MapQueryInspector[0]);
    }


    /**
     *  Gets the descendant map <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantMapIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant map query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.MapQueryInspector[] getDescendantMapTerms() {
        return (new org.osid.mapping.MapQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given map query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a map implementing the requested record.
     *
     *  @param mapRecordType a map record type
     *  @return the map query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>mapRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(mapRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.records.MapQueryInspectorRecord getMapQueryInspectorRecord(org.osid.type.Type mapRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.records.MapQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(mapRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(mapRecordType + " is not supported");
    }


    /**
     *  Adds a record to this map query. 
     *
     *  @param mapQueryInspectorRecord map query inspector
     *         record
     *  @param mapRecordType map record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addMapQueryInspectorRecord(org.osid.mapping.records.MapQueryInspectorRecord mapQueryInspectorRecord, 
                                                   org.osid.type.Type mapRecordType) {

        addRecordType(mapRecordType);
        nullarg(mapRecordType, "map record type");
        this.records.add(mapQueryInspectorRecord);        
        return;
    }
}
