//
// AbstractMapProcessEnablerLookupSession
//
//    A simple framework for providing a ProcessEnabler lookup service
//    backed by a fixed collection of process enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.workflow.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a ProcessEnabler lookup service backed by a
 *  fixed collection of process enablers. The process enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>ProcessEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapProcessEnablerLookupSession
    extends net.okapia.osid.jamocha.workflow.rules.spi.AbstractProcessEnablerLookupSession
    implements org.osid.workflow.rules.ProcessEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.workflow.rules.ProcessEnabler> processEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.workflow.rules.ProcessEnabler>());


    /**
     *  Makes a <code>ProcessEnabler</code> available in this session.
     *
     *  @param  processEnabler a process enabler
     *  @throws org.osid.NullArgumentException <code>processEnabler<code>
     *          is <code>null</code>
     */

    protected void putProcessEnabler(org.osid.workflow.rules.ProcessEnabler processEnabler) {
        this.processEnablers.put(processEnabler.getId(), processEnabler);
        return;
    }


    /**
     *  Makes an array of process enablers available in this session.
     *
     *  @param  processEnablers an array of process enablers
     *  @throws org.osid.NullArgumentException <code>processEnablers<code>
     *          is <code>null</code>
     */

    protected void putProcessEnablers(org.osid.workflow.rules.ProcessEnabler[] processEnablers) {
        putProcessEnablers(java.util.Arrays.asList(processEnablers));
        return;
    }


    /**
     *  Makes a collection of process enablers available in this session.
     *
     *  @param  processEnablers a collection of process enablers
     *  @throws org.osid.NullArgumentException <code>processEnablers<code>
     *          is <code>null</code>
     */

    protected void putProcessEnablers(java.util.Collection<? extends org.osid.workflow.rules.ProcessEnabler> processEnablers) {
        for (org.osid.workflow.rules.ProcessEnabler processEnabler : processEnablers) {
            this.processEnablers.put(processEnabler.getId(), processEnabler);
        }

        return;
    }


    /**
     *  Removes a ProcessEnabler from this session.
     *
     *  @param  processEnablerId the <code>Id</code> of the process enabler
     *  @throws org.osid.NullArgumentException <code>processEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeProcessEnabler(org.osid.id.Id processEnablerId) {
        this.processEnablers.remove(processEnablerId);
        return;
    }


    /**
     *  Gets the <code>ProcessEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  processEnablerId <code>Id</code> of the <code>ProcessEnabler</code>
     *  @return the processEnabler
     *  @throws org.osid.NotFoundException <code>processEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>processEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnabler getProcessEnabler(org.osid.id.Id processEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.workflow.rules.ProcessEnabler processEnabler = this.processEnablers.get(processEnablerId);
        if (processEnabler == null) {
            throw new org.osid.NotFoundException("processEnabler not found: " + processEnablerId);
        }

        return (processEnabler);
    }


    /**
     *  Gets all <code>ProcessEnablers</code>. In plenary mode, the returned
     *  list contains all known processEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  processEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>ProcessEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerList getProcessEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.workflow.rules.processenabler.ArrayProcessEnablerList(this.processEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.processEnablers.clear();
        super.close();
        return;
    }
}
