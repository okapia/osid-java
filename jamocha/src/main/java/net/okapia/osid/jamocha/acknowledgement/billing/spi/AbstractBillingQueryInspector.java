//
// AbstractBillingQueryInspector.java
//
//     A template for making a BillingQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.acknowledgement.billing.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for billings.
 */

public abstract class AbstractBillingQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.acknowledgement.BillingQueryInspector {

    private final java.util.Collection<org.osid.acknowledgement.records.BillingQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the credit <code> Id </code> query terms. 
     *
     *  @return the credit <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCreditIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the credit query terms. 
     *
     *  @return the credit terms 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditQueryInspector[] getCreditTerms() {
        return (new org.osid.acknowledgement.CreditQueryInspector[0]);
    }


    /**
     *  Gets the ancestor billing <code> Id </code> query terms. 
     *
     *  @return the ancestor billing <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorBillingIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor billing query terms. 
     *
     *  @return the ancestor billing terms 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingQueryInspector[] getAncestorBillingTerms() {
        return (new org.osid.acknowledgement.BillingQueryInspector[0]);
    }


    /**
     *  Gets the descendant billing <code> Id </code> query terms. 
     *
     *  @return the descendant billing <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantBillingIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant billing query terms. 
     *
     *  @return the descendant billing terms 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingQueryInspector[] getDescendantBillingTerms() {
        return (new org.osid.acknowledgement.BillingQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given billing query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a billing implementing the requested record.
     *
     *  @param billingRecordType a billing record type
     *  @return the billing query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>billingRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(billingRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.acknowledgement.records.BillingQueryInspectorRecord getBillingQueryInspectorRecord(org.osid.type.Type billingRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.acknowledgement.records.BillingQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(billingRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(billingRecordType + " is not supported");
    }


    /**
     *  Adds a record to this billing query. 
     *
     *  @param billingQueryInspectorRecord billing query inspector
     *         record
     *  @param billingRecordType billing record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBillingQueryInspectorRecord(org.osid.acknowledgement.records.BillingQueryInspectorRecord billingQueryInspectorRecord, 
                                                   org.osid.type.Type billingRecordType) {

        addRecordType(billingRecordType);
        nullarg(billingRecordType, "billing record type");
        this.records.add(billingQueryInspectorRecord);        
        return;
    }
}
