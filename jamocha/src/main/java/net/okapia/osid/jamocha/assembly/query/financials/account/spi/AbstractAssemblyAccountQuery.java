//
// AbstractAssemblyAccountQuery.java
//
//     An AccountQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.financials.account.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An AccountQuery that stores terms.
 */

public abstract class AbstractAssemblyAccountQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.financials.AccountQuery,
               org.osid.financials.AccountQueryInspector,
               org.osid.financials.AccountSearchOrder {

    private final java.util.Collection<org.osid.financials.records.AccountQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.financials.records.AccountQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.financials.records.AccountSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyAccountQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyAccountQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches accounts that are credit accounts. 
     *
     *  @param  match <code> true </code> to match accounts that are credit 
     *          account, <code> false </code> to match accounts debit accounts 
     */

    @OSID @Override
    public void matchCreditBalance(boolean match) {
        getAssembler().addBooleanTerm(getCreditBalanceColumn(), match);
        return;
    }


    /**
     *  Clears the credit balance terms. 
     */

    @OSID @Override
    public void clearCreditBalanceTerms() {
        getAssembler().clearTerms(getCreditBalanceColumn());
        return;
    }


    /**
     *  Gets the credit balance query terms. 
     *
     *  @return the credit balance query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getCreditBalanceTerms() {
        return (getAssembler().getBooleanTerms(getCreditBalanceColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the credit 
     *  balance flag. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCreditBalance(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCreditBalanceColumn(), style);
        return;
    }


    /**
     *  Gets the CreditBalance column name.
     *
     * @return the column name
     */

    protected String getCreditBalanceColumn() {
        return ("credit_balance");
    }


    /**
     *  Matches an account code. 
     *
     *  @param  code a code 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> code </code> is not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> code </code> is <code> 
     *          null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchCode(String code, org.osid.type.Type stringMatchType, 
                          boolean match) {
        getAssembler().addStringTerm(getCodeColumn(), code, stringMatchType, match);
        return;
    }


    /**
     *  Matches an account that has any code assigned. 
     *
     *  @param  match <code> true </code> to match accounts with any code, 
     *          <code> false </code> to match accounts with no code 
     */

    @OSID @Override
    public void matchAnyCode(boolean match) {
        getAssembler().addStringWildcardTerm(getCodeColumn(), match);
        return;
    }


    /**
     *  Clears the code terms. 
     */

    @OSID @Override
    public void clearCodeTerms() {
        getAssembler().clearTerms(getCodeColumn());
        return;
    }


    /**
     *  Gets the code query terms. 
     *
     *  @return the code query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getCodeTerms() {
        return (getAssembler().getStringTerms(getCodeColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the code. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCode(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCodeColumn(), style);
        return;
    }


    /**
     *  Gets the Code column name.
     *
     * @return the column name
     */

    protected String getCodeColumn() {
        return ("code");
    }


    /**
     *  Tests if a <code> SummaryQuery </code> is available. 
     *
     *  @return <code> true </code> if a summery query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSummaryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a summary. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the summery query 
     *  @throws org.osid.UnimplementedException <code> supportsSummeryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.SummaryQuery getSummaryQuery() {
        throw new org.osid.UnimplementedException("supportsSummaryQuery() is false");
    }


    /**
     *  Clears the summary terms. 
     */

    @OSID @Override
    public void clearSummaryTerms() {
        getAssembler().clearTerms(getSummaryColumn());
        return;
    }


    /**
     *  Gets the summary query terms. 
     *
     *  @return the summary query terms 
     */

    @OSID @Override
    public org.osid.financials.SummaryQueryInspector[] getSummaryTerms() {
        return (new org.osid.financials.SummaryQueryInspector[0]);
    }


    /**
     *  Gets the Summary column name.
     *
     * @return the column name
     */

    protected String getSummaryColumn() {
        return ("summary");
    }


    /**
     *  Sets the account <code> Id </code> for this query to match accounts 
     *  that have the specified account as an ancestor. 
     *
     *  @param  accountId an account <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> accountId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorAccountId(org.osid.id.Id accountId, boolean match) {
        getAssembler().addIdTerm(getAncestorAccountIdColumn(), accountId, match);
        return;
    }


    /**
     *  Clears the ancestor account <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorAccountIdTerms() {
        getAssembler().clearTerms(getAncestorAccountIdColumn());
        return;
    }


    /**
     *  Gets the ancestor account <code> Id </code> query terms. 
     *
     *  @return the ancestor account <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorAccountIdTerms() {
        return (getAssembler().getIdTerms(getAncestorAccountIdColumn()));
    }


    /**
     *  Gets the AncestorAccountId column name.
     *
     * @return the column name
     */

    protected String getAncestorAccountIdColumn() {
        return ("ancestor_account_id");
    }


    /**
     *  Tests if an <code> AccountQuery </code> is available. 
     *
     *  @return <code> true </code> if an account query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorAccountQuery() {
        return (false);
    }


    /**
     *  Gets the query for an account. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the account query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorAccountQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountQuery getAncestorAccountQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorAccountQuery() is false");
    }


    /**
     *  Matches accounts with any account ancestor. 
     *
     *  @param  match <code> true </code> to match accounts with any ancestor, 
     *          <code> false </code> to match root accounts 
     */

    @OSID @Override
    public void matchAnyAncestorAccount(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorAccountColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor account query terms. 
     */

    @OSID @Override
    public void clearAncestorAccountTerms() {
        getAssembler().clearTerms(getAncestorAccountColumn());
        return;
    }


    /**
     *  Gets the ancestor account query terms. 
     *
     *  @return the ancestor account terms 
     */

    @OSID @Override
    public org.osid.financials.AccountQueryInspector[] getAncestorAccountTerms() {
        return (new org.osid.financials.AccountQueryInspector[0]);
    }


    /**
     *  Gets the AncestorAccount column name.
     *
     * @return the column name
     */

    protected String getAncestorAccountColumn() {
        return ("ancestor_account");
    }


    /**
     *  Sets the account <code> Id </code> for this query to match accounts 
     *  that have the specified account as a descendant. 
     *
     *  @param  accountId an account <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> accountId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantAccountId(org.osid.id.Id accountId, 
                                         boolean match) {
        getAssembler().addIdTerm(getDescendantAccountIdColumn(), accountId, match);
        return;
    }


    /**
     *  Clears the descendant account <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantAccountIdTerms() {
        getAssembler().clearTerms(getDescendantAccountIdColumn());
        return;
    }


    /**
     *  Gets the descendant account <code> Id </code> query terms. 
     *
     *  @return the descendant account <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantAccountIdTerms() {
        return (getAssembler().getIdTerms(getDescendantAccountIdColumn()));
    }


    /**
     *  Gets the DescendantAccountId column name.
     *
     * @return the column name
     */

    protected String getDescendantAccountIdColumn() {
        return ("descendant_account_id");
    }


    /**
     *  Tests if an <code> AccountQuery </code> is available. 
     *
     *  @return <code> true </code> if an account query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantAccountQuery() {
        return (false);
    }


    /**
     *  Gets the query for an account. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the account query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantAccountQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountQuery getDescendantAccountQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantAccountQuery() is false");
    }


    /**
     *  Matches accounts with any account descendant. 
     *
     *  @param  match <code> true </code> to match accounts with any 
     *          descendant, <code> false </code> to match leaf accounts 
     */

    @OSID @Override
    public void matchAnyDescendantAccount(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantAccountColumn(), match);
        return;
    }


    /**
     *  Clears the descendant account query terms. 
     */

    @OSID @Override
    public void clearDescendantAccountTerms() {
        getAssembler().clearTerms(getDescendantAccountColumn());
        return;
    }


    /**
     *  Gets the descendant account query terms. 
     *
     *  @return the descendant account terms 
     */

    @OSID @Override
    public org.osid.financials.AccountQueryInspector[] getDescendantAccountTerms() {
        return (new org.osid.financials.AccountQueryInspector[0]);
    }


    /**
     *  Gets the DescendantAccount column name.
     *
     * @return the column name
     */

    protected String getDescendantAccountColumn() {
        return ("descendant_account");
    }


    /**
     *  Sets the business <code> Id </code> for this query to match accounts 
     *  assigned to businesses. 
     *
     *  @param  businessId the business <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBusinessId(org.osid.id.Id businessId, boolean match) {
        getAssembler().addIdTerm(getBusinessIdColumn(), businessId, match);
        return;
    }


    /**
     *  Clears the business <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBusinessIdTerms() {
        getAssembler().clearTerms(getBusinessIdColumn());
        return;
    }


    /**
     *  Gets the business <code> Id </code> query terms. 
     *
     *  @return the business <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBusinessIdTerms() {
        return (getAssembler().getIdTerms(getBusinessIdColumn()));
    }


    /**
     *  Gets the BusinessId column name.
     *
     * @return the column name
     */

    protected String getBusinessIdColumn() {
        return ("business_id");
    }


    /**
     *  Tests if a <code> BusinessQuery </code> is available. 
     *
     *  @return <code> true </code> if a business query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a business. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the business query 
     *  @throws org.osid.UnimplementedException <code> supportsBusinessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.BusinessQuery getBusinessQuery() {
        throw new org.osid.UnimplementedException("supportsBusinessQuery() is false");
    }


    /**
     *  Clears the business terms. 
     */

    @OSID @Override
    public void clearBusinessTerms() {
        getAssembler().clearTerms(getBusinessColumn());
        return;
    }


    /**
     *  Gets the business query terms. 
     *
     *  @return the business query terms 
     */

    @OSID @Override
    public org.osid.financials.BusinessQueryInspector[] getBusinessTerms() {
        return (new org.osid.financials.BusinessQueryInspector[0]);
    }


    /**
     *  Gets the Business column name.
     *
     * @return the column name
     */

    protected String getBusinessColumn() {
        return ("business");
    }


    /**
     *  Tests if this account supports the given record
     *  <code>Type</code>.
     *
     *  @param  accountRecordType an account record type 
     *  @return <code>true</code> if the accountRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>accountRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type accountRecordType) {
        for (org.osid.financials.records.AccountQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(accountRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  accountRecordType the account record type 
     *  @return the account query record 
     *  @throws org.osid.NullArgumentException
     *          <code>accountRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(accountRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.records.AccountQueryRecord getAccountQueryRecord(org.osid.type.Type accountRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.records.AccountQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(accountRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(accountRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  accountRecordType the account record type 
     *  @return the account query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>accountRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(accountRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.records.AccountQueryInspectorRecord getAccountQueryInspectorRecord(org.osid.type.Type accountRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.records.AccountQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(accountRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(accountRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param accountRecordType the account record type
     *  @return the account search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>accountRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(accountRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.records.AccountSearchOrderRecord getAccountSearchOrderRecord(org.osid.type.Type accountRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.records.AccountSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(accountRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(accountRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this account. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param accountQueryRecord the account query record
     *  @param accountQueryInspectorRecord the account query inspector
     *         record
     *  @param accountSearchOrderRecord the account search order record
     *  @param accountRecordType account record type
     *  @throws org.osid.NullArgumentException
     *          <code>accountQueryRecord</code>,
     *          <code>accountQueryInspectorRecord</code>,
     *          <code>accountSearchOrderRecord</code> or
     *          <code>accountRecordTypeaccount</code> is
     *          <code>null</code>
     */
            
    protected void addAccountRecords(org.osid.financials.records.AccountQueryRecord accountQueryRecord, 
                                      org.osid.financials.records.AccountQueryInspectorRecord accountQueryInspectorRecord, 
                                      org.osid.financials.records.AccountSearchOrderRecord accountSearchOrderRecord, 
                                      org.osid.type.Type accountRecordType) {

        addRecordType(accountRecordType);

        nullarg(accountQueryRecord, "account query record");
        nullarg(accountQueryInspectorRecord, "account query inspector record");
        nullarg(accountSearchOrderRecord, "account search odrer record");

        this.queryRecords.add(accountQueryRecord);
        this.queryInspectorRecords.add(accountQueryInspectorRecord);
        this.searchOrderRecords.add(accountSearchOrderRecord);
        
        return;
    }
}
