//
// AbstractUnknownFamily.java
//
//     Defines an unknown Family.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.relationship.family.spi;


/**
 *  Defines an unknown <code>Family</code>.
 */

public abstract class AbstractUnknownFamily
    extends net.okapia.osid.jamocha.relationship.family.spi.AbstractFamily
    implements org.osid.relationship.Family {

    protected static final String OBJECT = "osid.relationship.Family";


    /**
     *  Constructs a new <code>AbstractUnknownFamily</code>.
     */

    public AbstractUnknownFamily() {
        setId(net.okapia.osid.jamocha.nil.privateutil.UnknownId.valueOf(OBJECT));
        setDisplayName(net.okapia.osid.jamocha.nil.privateutil.DisplayName.valueOf(OBJECT));
        setDescription(net.okapia.osid.jamocha.nil.privateutil.Description.valueOf(OBJECT));

        setProvider(net.okapia.osid.provider.Providers.UNKNOWN.getProvider().getResource());
        return;
    }


    /**
     *  Constructs a new <code>AbstractUnknownFamily</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public AbstractUnknownFamily(boolean optional) {
        this();
        addAssetToBranding(new net.okapia.osid.jamocha.nil.repository.asset.UnknownAsset());
        return;
    }
}
