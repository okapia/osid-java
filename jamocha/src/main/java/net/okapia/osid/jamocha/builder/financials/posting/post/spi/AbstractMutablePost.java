//
// AbstractMutablePost.java
//
//     Defines a mutable Post.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.financials.posting.post.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Post</code>.
 */

public abstract class AbstractMutablePost
    extends net.okapia.osid.jamocha.financials.posting.post.spi.AbstractPost
    implements org.osid.financials.posting.Post,
               net.okapia.osid.jamocha.builder.financials.posting.post.PostMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this post. 
     *
     *  @param record post record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addPostRecord(org.osid.financials.posting.records.PostRecord record, org.osid.type.Type recordType) {
        super.addPostRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the display name for this post.
     *
     *  @param displayName the name for this post
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this post.
     *
     *  @param description the description of this post
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException <code>genusType</code>
     *          is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the fiscal period.
     *
     *  @param period a fiscal period
     *  @throws org.osid.NullArgumentException <code>period</code> is
     *          <code>null</code>
     */

    @Override
    public void setFiscalPeriod(org.osid.financials.FiscalPeriod period) {
        super.setFiscalPeriod(period);
        return;
    }


    /**
     *  Sets the posted.
     *
     *  @param posted <code> true </code> if this has been posted,
     *          <code> false </code> if just lying around
     */

    @Override
    public void setPosted(boolean posted) {
        super.setPosted(posted);
        return;
    }


    /**
     *  Sets the date.
     *
     *  @param date a date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setDate(org.osid.calendaring.DateTime date) {
        super.setDate(date);
        return;
    }


    /**
     *  Adds a post entry.
     *
     *  @param entry a post entry
     *  @throws org.osid.NullArgumentException <code>entry</code> is
     *          <code>null</code>
     */

    @Override
    public void addPostEntry(org.osid.financials.posting.PostEntry entry) {
        super.addPostEntry(entry);
        return;
    }


    /**
     *  Sets all the post entries.
     *
     *  @param entries a collection of post entries
     *  @throws org.osid.NullArgumentException <code>entries</code> is
     *          <code>null</code>
     */

    @Override
    public void setPostEntries(java.util.Collection<org.osid.financials.posting.PostEntry> entries) {
        super.setPostEntries(entries);
        return;
    }


    /**
     *  Sets the corrected post.
     *
     *  @param post a corrected post
     *  @throws org.osid.NullArgumentException <code>post</code> is
     *          <code>null</code>
     */

    @Override
    public void setCorrectedPost(org.osid.financials.posting.Post post) {
        super.setCorrectedPost(post);
        return;
    }
}

