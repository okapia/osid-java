//
// AbstractImmutableAccount.java
//
//     Wraps a mutable Account to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.financials.account.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Account</code> to hide modifiers. This
 *  wrapper provides an immutized Account from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying account whose state changes are visible.
 */

public abstract class AbstractImmutableAccount
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.financials.Account {

    private final org.osid.financials.Account account;


    /**
     *  Constructs a new <code>AbstractImmutableAccount</code>.
     *
     *  @param account the account to immutablize
     *  @throws org.osid.NullArgumentException <code>account</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableAccount(org.osid.financials.Account account) {
        super(account);
        this.account = account;
        return;
    }


    /**
     *  Tests if this account is a credit or debit account. 
     *
     *  @return <code> true </code> if credits increase the balance, <code> 
     *          false </code> if credits decrease the balance 
     */

    @OSID @Override
    public boolean isCreditBalance() {
        return (this.account.isCreditBalance());
    }


    /**
     *  Gets a code for this G/L account. 
     *
     *  @return an account code 
     */

    @OSID @Override
    public String getCode() {
        return (this.account.getCode());
    }


    /**
     *  Gets the account record corresponding to the given <code> Account 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> accountRecordType 
     *  </code> may be the <code> Type </code> returned in <code> 
     *  getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(accountRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  accountRecordType the type of account record to retrieve 
     *  @return the account record 
     *  @throws org.osid.NullArgumentException <code> accountRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(accountRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.records.AccountRecord getAccountRecord(org.osid.type.Type accountRecordType)
        throws org.osid.OperationFailedException {

        return (this.account.getAccountRecord(accountRecordType));
    }
}

