//
// AbstractRelevancyEnablerLookupSession.java
//
//    A starter implementation framework for providing a RelevancyEnabler
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ontology.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a RelevancyEnabler
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getRelevancyEnablers(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractRelevancyEnablerLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.ontology.rules.RelevancyEnablerLookupSession {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean federated     = false;
    private org.osid.ontology.Ontology ontology = new net.okapia.osid.jamocha.nil.ontology.ontology.UnknownOntology();
    

    /**
     *  Gets the <code>Ontology/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Ontology Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOntologyId() {
        return (this.ontology.getId());
    }


    /**
     *  Gets the <code>Ontology</code> associated with this 
     *  session.
     *
     *  @return the <code>Ontology</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.Ontology getOntology()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.ontology);
    }


    /**
     *  Sets the <code>Ontology</code>.
     *
     *  @param  ontology the ontology for this session
     *  @throws org.osid.NullArgumentException <code>ontology</code>
     *          is <code>null</code>
     */

    protected void setOntology(org.osid.ontology.Ontology ontology) {
        nullarg(ontology, "ontology");
        this.ontology = ontology;
        return;
    }


    /**
     *  Tests if this user can perform <code>RelevancyEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRelevancyEnablers() {
        return (true);
    }


    /**
     *  A complete view of the <code>RelevancyEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRelevancyEnablerView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>RelevancyEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRelevancyEnablerView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include relevancy enablers in ontologies which are children
     *  of this ontology in the ontology hierarchy.
     */

    @OSID @Override
    public void useFederatedOntologyView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this ontology only.
     */

    @OSID @Override
    public void useIsolatedOntologyView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active relevancy enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveRelevancyEnablerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive relevancy enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusRelevancyEnablerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>RelevancyEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>RelevancyEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>RelevancyEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, relevancy enablers are returned that are currently
     *  active. In any status mode, active and inactive relevancy enablers
     *  are returned.
     *
     *  @param  relevancyEnablerId <code>Id</code> of the
     *          <code>RelevancyEnabler</code>
     *  @return the relevancy enabler
     *  @throws org.osid.NotFoundException <code>relevancyEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>relevancyEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnabler getRelevancyEnabler(org.osid.id.Id relevancyEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.ontology.rules.RelevancyEnablerList relevancyEnablers = getRelevancyEnablers()) {
            while (relevancyEnablers.hasNext()) {
                org.osid.ontology.rules.RelevancyEnabler relevancyEnabler = relevancyEnablers.getNextRelevancyEnabler();
                if (relevancyEnabler.getId().equals(relevancyEnablerId)) {
                    return (relevancyEnabler);
                }
            }
        } 

        throw new org.osid.NotFoundException(relevancyEnablerId + " not found");
    }


    /**
     *  Gets a <code>RelevancyEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  relevancyEnablers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>RelevancyEnablers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, relevancy enablers are returned that are currently
     *  active. In any status mode, active and inactive relevancy enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getRelevancyEnablers()</code>.
     *
     *  @param  relevancyEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>RelevancyEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
*               found
     *  @throws org.osid.NullArgumentException
     *          <code>relevancyEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerList getRelevancyEnablersByIds(org.osid.id.IdList relevancyEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.ontology.rules.RelevancyEnabler> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = relevancyEnablerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getRelevancyEnabler(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("relevancy enabler " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.ontology.rules.relevancyenabler.LinkedRelevancyEnablerList(ret));
    }


    /**
     *  Gets a <code>RelevancyEnablerList</code> corresponding to the given
     *  relevancy enabler genus <code>Type</code> which does not include
     *  relevancy enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  relevancy enablers or an error results. Otherwise, the returned list
     *  may contain only those relevancy enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, relevancy enablers are returned that are currently
     *  active. In any status mode, active and inactive relevancy enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getRelevancyEnablers()</code>.
     *
     *  @param  relevancyEnablerGenusType a relevancyEnabler genus type 
     *  @return the returned <code>RelevancyEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>relevancyEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerList getRelevancyEnablersByGenusType(org.osid.type.Type relevancyEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.ontology.rules.relevancyenabler.RelevancyEnablerGenusFilterList(getRelevancyEnablers(), relevancyEnablerGenusType));
    }


    /**
     *  Gets a <code>RelevancyEnablerList</code> corresponding to the given
     *  relevancy enabler genus <code>Type</code> and include any additional
     *  relevancy enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  relevancy enablers or an error results. Otherwise, the returned list
     *  may contain only those relevancy enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, relevancy enablers are returned that are currently
     *  active. In any status mode, active and inactive relevancy enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getRelevancyEnablers()</code>.
     *
     *  @param  relevancyEnablerGenusType a relevancyEnabler genus type 
     *  @return the returned <code>RelevancyEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>relevancyEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerList getRelevancyEnablersByParentGenusType(org.osid.type.Type relevancyEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getRelevancyEnablersByGenusType(relevancyEnablerGenusType));
    }


    /**
     *  Gets a <code>RelevancyEnablerList</code> containing the given
     *  relevancy enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  relevancy enablers or an error results. Otherwise, the returned list
     *  may contain only those relevancy enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *

     *  In active mode, relevancy enablers are returned that are currently
     *  active. In any status mode, active and inactive relevancy enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getRelevancyEnablers()</code>.
     *
     *  @param  relevancyEnablerRecordType a relevancyEnabler record type 
     *  @return the returned <code>RelevancyEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>relevancyEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerList getRelevancyEnablersByRecordType(org.osid.type.Type relevancyEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.ontology.rules.relevancyenabler.RelevancyEnablerRecordFilterList(getRelevancyEnablers(), relevancyEnablerRecordType));
    }


    /**
     *  Gets a <code>RelevancyEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  relevancy enablers or an error results. Otherwise, the returned list
     *  may contain only those relevancy enablers that are accessible
     *  through this session.
     *  
     *  In active mode, relevancy enablers are returned that are currently
     *  active in addition to being effective during the given date
     *  range. In any status mode, active and inactive relevancy enablers are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>RelevancyEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerList getRelevancyEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.ontology.rules.relevancyenabler.TemporalRelevancyEnablerFilterList(getRelevancyEnablers(), from, to));
    }
        

    /**
     *  Gets a <code>RelevancyEnablerList </code> which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  relevancy enablers or an error results. Otherwise, the returned list
     *  may contain only those relevancy enablers that are accessible
     *  through this session.
     *
     *  In active mode, relevancy enablers are returned that are currently
     *  active in addition to being effective during the date
     *  range. In any status mode, active and inactive relevancy enablers are
     *  returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>RelevancyEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerList getRelevancyEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (getRelevancyEnablersOnDate(from, to));
    }


    /**
     *  Gets all <code>RelevancyEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  relevancy enablers or an error results. Otherwise, the returned list
     *  may contain only those relevancy enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, relevancy enablers are returned that are currently
     *  active. In any status mode, active and inactive relevancy enablers
     *  are returned.
     *
     *  @return a list of <code>RelevancyEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.ontology.rules.RelevancyEnablerList getRelevancyEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the relevancy enabler list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of relevancy enablers
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.ontology.rules.RelevancyEnablerList filterRelevancyEnablersOnViews(org.osid.ontology.rules.RelevancyEnablerList list)
        throws org.osid.OperationFailedException {

        org.osid.ontology.rules.RelevancyEnablerList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.ontology.rules.relevancyenabler.ActiveRelevancyEnablerFilterList(ret);
        }

        return (ret);
    }
}
