//
// AbstractAdapterProgramOfferingLookupSession.java
//
//    A ProgramOffering lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.course.program.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A ProgramOffering lookup session adapter.
 */

public abstract class AbstractAdapterProgramOfferingLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.course.program.ProgramOfferingLookupSession {

    private final org.osid.course.program.ProgramOfferingLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterProgramOfferingLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterProgramOfferingLookupSession(org.osid.course.program.ProgramOfferingLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code CourseCatalog/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code CourseCatalog Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.session.getCourseCatalogId());
    }


    /**
     *  Gets the {@code CourseCatalog} associated with this session.
     *
     *  @return the {@code CourseCatalog} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCourseCatalog());
    }


    /**
     *  Tests if this user can perform {@code ProgramOffering} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupProgramOfferings() {
        return (this.session.canLookupProgramOfferings());
    }


    /**
     *  A complete view of the {@code ProgramOffering} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeProgramOfferingView() {
        this.session.useComparativeProgramOfferingView();
        return;
    }


    /**
     *  A complete view of the {@code ProgramOffering} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryProgramOfferingView() {
        this.session.usePlenaryProgramOfferingView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include program offerings in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.session.useFederatedCourseCatalogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.session.useIsolatedCourseCatalogView();
        return;
    }
    

    /**
     *  Only program offerings whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveProgramOfferingView() {
        this.session.useEffectiveProgramOfferingView();
        return;
    }
    

    /**
     *  All program offerings of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveProgramOfferingView() {
        this.session.useAnyEffectiveProgramOfferingView();
        return;
    }

     
    /**
     *  Gets the {@code ProgramOffering} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code ProgramOffering} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code ProgramOffering} and
     *  retained for compatibility.
     *
     *  In effective mode, program offerings are returned that are currently
     *  effective.  In any effective mode, effective program offerings and
     *  those currently expired are returned.
     *
     *  @param programOfferingId {@code Id} of the {@code ProgramOffering}
     *  @return the program offering
     *  @throws org.osid.NotFoundException {@code programOfferingId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code programOfferingId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOffering getProgramOffering(org.osid.id.Id programOfferingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProgramOffering(programOfferingId));
    }


    /**
     *  Gets a {@code ProgramOfferingList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  programOfferings specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code ProgramOfferings} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, program offerings are returned that are currently
     *  effective.  In any effective mode, effective program offerings and
     *  those currently expired are returned.
     *
     *  @param  programOfferingIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code ProgramOffering} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code programOfferingIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsByIds(org.osid.id.IdList programOfferingIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProgramOfferingsByIds(programOfferingIds));
    }


    /**
     *  Gets a {@code ProgramOfferingList} corresponding to the given
     *  program offering genus {@code Type} which does not include
     *  program offerings of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  program offerings or an error results. Otherwise, the returned list
     *  may contain only those program offerings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, program offerings are returned that are currently
     *  effective.  In any effective mode, effective program offerings and
     *  those currently expired are returned.
     *
     *  @param  programOfferingGenusType a programOffering genus type 
     *  @return the returned {@code ProgramOffering} list
     *  @throws org.osid.NullArgumentException
     *          {@code programOfferingGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsByGenusType(org.osid.type.Type programOfferingGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProgramOfferingsByGenusType(programOfferingGenusType));
    }


    /**
     *  Gets a {@code ProgramOfferingList} corresponding to the given
     *  program offering genus {@code Type} and include any additional
     *  program offerings with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  program offerings or an error results. Otherwise, the returned list
     *  may contain only those program offerings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, program offerings are returned that are currently
     *  effective.  In any effective mode, effective program offerings and
     *  those currently expired are returned.
     *
     *  @param  programOfferingGenusType a programOffering genus type 
     *  @return the returned {@code ProgramOffering} list
     *  @throws org.osid.NullArgumentException
     *          {@code programOfferingGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsByParentGenusType(org.osid.type.Type programOfferingGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProgramOfferingsByParentGenusType(programOfferingGenusType));
    }


    /**
     *  Gets a {@code ProgramOfferingList} containing the given
     *  program offering record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  program offerings or an error results. Otherwise, the returned list
     *  may contain only those program offerings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, program offerings are returned that are currently
     *  effective.  In any effective mode, effective program offerings and
     *  those currently expired are returned.
     *
     *  @param  programOfferingRecordType a programOffering record type 
     *  @return the returned {@code ProgramOffering} list
     *  @throws org.osid.NullArgumentException
     *          {@code programOfferingRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsByRecordType(org.osid.type.Type programOfferingRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProgramOfferingsByRecordType(programOfferingRecordType));
    }


    /**
     *  Gets a {@code ProgramOfferingList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  program offerings or an error results. Otherwise, the returned list
     *  may contain only those program offerings that are accessible
     *  through this session.
     *  
     *  In active mode, program offerings are returned that are currently
     *  active. In any status mode, active and inactive program offerings
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code ProgramOffering} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsOnDate(org.osid.calendaring.DateTime from, 
                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProgramOfferingsOnDate(from, to));
    }
        

    /**
     *  Gets a list of program offerings corresponding to a program
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  program offerings or an error results. Otherwise, the returned list
     *  may contain only those program offerings that are accessible through
     *  this session.
     *
     *  In effective mode, program offerings are returned that are
     *  currently effective.  In any effective mode, effective
     *  program offerings and those currently expired are returned.
     *
     *  @param  programId the {@code Id} of the program
     *  @return the returned {@code ProgramOfferingList}
     *  @throws org.osid.NullArgumentException {@code programId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsForProgram(org.osid.id.Id programId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProgramOfferingsForProgram(programId));
    }


    /**
     *  Gets a list of program offerings corresponding to a program
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  program offerings or an error results. Otherwise, the returned list
     *  may contain only those program offerings that are accessible
     *  through this session.
     *
     *  In effective mode, program offerings are returned that are
     *  currently effective.  In any effective mode, effective
     *  program offerings and those currently expired are returned.
     *
     *  @param  programId the {@code Id} of the program
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ProgramOfferingList}
     *  @throws org.osid.NullArgumentException {@code programId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsForProgramOnDate(org.osid.id.Id programId,
                                                                                           org.osid.calendaring.DateTime from,
                                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProgramOfferingsForProgramOnDate(programId, from, to));
    }


    /**
     *  Gets a list of program offerings corresponding to a term
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  program offerings or an error results. Otherwise, the returned list
     *  may contain only those program offerings that are accessible
     *  through this session.
     *
     *  In effective mode, program offerings are returned that are
     *  currently effective.  In any effective mode, effective
     *  program offerings and those currently expired are returned.
     *
     *  @param  termId the {@code Id} of the term
     *  @return the returned {@code ProgramOfferingList}
     *  @throws org.osid.NullArgumentException {@code termId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsForTerm(org.osid.id.Id termId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProgramOfferingsForTerm(termId));
    }


    /**
     *  Gets a list of program offerings corresponding to a term
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  program offerings or an error results. Otherwise, the returned list
     *  may contain only those program offerings that are accessible
     *  through this session.
     *
     *  In effective mode, program offerings are returned that are
     *  currently effective.  In any effective mode, effective
     *  program offerings and those currently expired are returned.
     *
     *  @param  termId the {@code Id} of the term
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ProgramOfferingList}
     *  @throws org.osid.NullArgumentException {@code termId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsForTermOnDate(org.osid.id.Id termId,
                                                                                        org.osid.calendaring.DateTime from,
                                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProgramOfferingsForTermOnDate(termId, from, to));
    }


    /**
     *  Gets a list of program offerings corresponding to program and term
     *  {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known
     *  program offerings or an error results. Otherwise, the returned list
     *  may contain only those program offerings that are accessible
     *  through this session.
     *
     *  In effective mode, program offerings are returned that are
     *  currently effective.  In any effective mode, effective
     *  program offerings and those currently expired are returned.
     *
     *  @param  programId the {@code Id} of the program
     *  @param  termId the {@code Id} of the term
     *  @return the returned {@code ProgramOfferingList}
     *  @throws org.osid.NullArgumentException {@code programId},
     *          {@code termId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsForProgramAndTerm(org.osid.id.Id programId,
                                                                                            org.osid.id.Id termId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProgramOfferingsForProgramAndTerm(programId, termId));
    }


    /**
     *  Gets a list of program offerings corresponding to program and term
     *  {@code Ids} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  program offerings or an error results. Otherwise, the returned list
     *  may contain only those program offerings that are accessible
     *  through this session.
     *
     *  In effective mode, program offerings are returned that are currently
     *  effective. In any effective mode, effective program offerings and
     *  those currently expired are returned.
     *
     *  @param  termId the {@code Id} of the term
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ProgramOfferingList}
     *  @throws org.osid.NullArgumentException {@code programId},
     *          {@code termId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsForProgramAndTermOnDate(org.osid.id.Id programId,
                                                                                                  org.osid.id.Id termId,
                                                                                                  org.osid.calendaring.DateTime from,
                                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProgramOfferingsForProgramAndTermOnDate(programId, termId, from, to));
    }


    /**
     *  Gets all {@code ProgramOfferings}. 
     *
     *  In plenary mode, the returned list contains all known
     *  program offerings or an error results. Otherwise, the returned list
     *  may contain only those program offerings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, program offerings are returned that are currently
     *  effective.  In any effective mode, effective program offerings and
     *  those currently expired are returned.
     *
     *  @return a list of {@code ProgramOfferings} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferings()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProgramOfferings());
    }
}
