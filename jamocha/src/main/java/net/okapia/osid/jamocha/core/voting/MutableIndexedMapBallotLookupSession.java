//
// MutableIndexedMapBallotLookupSession
//
//    Implements a Ballot lookup service backed by a collection of
//    ballots indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting;


/**
 *  Implements a Ballot lookup service backed by a collection of
 *  ballots. The ballots are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some ballots may be compatible
 *  with more types than are indicated through these ballot
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of ballots can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapBallotLookupSession
    extends net.okapia.osid.jamocha.core.voting.spi.AbstractIndexedMapBallotLookupSession
    implements org.osid.voting.BallotLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapBallotLookupSession} with no ballots.
     *
     *  @param polls the polls
     *  @throws org.osid.NullArgumentException {@code polls}
     *          is {@code null}
     */

      public MutableIndexedMapBallotLookupSession(org.osid.voting.Polls polls) {
        setPolls(polls);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapBallotLookupSession} with a
     *  single ballot.
     *  
     *  @param polls the polls
     *  @param  ballot a single ballot
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code ballot} is {@code null}
     */

    public MutableIndexedMapBallotLookupSession(org.osid.voting.Polls polls,
                                                  org.osid.voting.Ballot ballot) {
        this(polls);
        putBallot(ballot);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapBallotLookupSession} using an
     *  array of ballots.
     *
     *  @param polls the polls
     *  @param  ballots an array of ballots
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code ballots} is {@code null}
     */

    public MutableIndexedMapBallotLookupSession(org.osid.voting.Polls polls,
                                                  org.osid.voting.Ballot[] ballots) {
        this(polls);
        putBallots(ballots);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapBallotLookupSession} using a
     *  collection of ballots.
     *
     *  @param polls the polls
     *  @param  ballots a collection of ballots
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code ballots} is {@code null}
     */

    public MutableIndexedMapBallotLookupSession(org.osid.voting.Polls polls,
                                                  java.util.Collection<? extends org.osid.voting.Ballot> ballots) {

        this(polls);
        putBallots(ballots);
        return;
    }
    

    /**
     *  Makes a {@code Ballot} available in this session.
     *
     *  @param  ballot a ballot
     *  @throws org.osid.NullArgumentException {@code ballot{@code  is
     *          {@code null}
     */

    @Override
    public void putBallot(org.osid.voting.Ballot ballot) {
        super.putBallot(ballot);
        return;
    }


    /**
     *  Makes an array of ballots available in this session.
     *
     *  @param  ballots an array of ballots
     *  @throws org.osid.NullArgumentException {@code ballots{@code 
     *          is {@code null}
     */

    @Override
    public void putBallots(org.osid.voting.Ballot[] ballots) {
        super.putBallots(ballots);
        return;
    }


    /**
     *  Makes collection of ballots available in this session.
     *
     *  @param  ballots a collection of ballots
     *  @throws org.osid.NullArgumentException {@code ballot{@code  is
     *          {@code null}
     */

    @Override
    public void putBallots(java.util.Collection<? extends org.osid.voting.Ballot> ballots) {
        super.putBallots(ballots);
        return;
    }


    /**
     *  Removes a Ballot from this session.
     *
     *  @param ballotId the {@code Id} of the ballot
     *  @throws org.osid.NullArgumentException {@code ballotId{@code  is
     *          {@code null}
     */

    @Override
    public void removeBallot(org.osid.id.Id ballotId) {
        super.removeBallot(ballotId);
        return;
    }    
}
