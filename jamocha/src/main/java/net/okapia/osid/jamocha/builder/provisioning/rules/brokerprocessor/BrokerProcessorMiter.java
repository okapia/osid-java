//
// BrokerProcessorMiter.java
//
//     Defines a BrokerProcessor miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.provisioning.rules.brokerprocessor;


/**
 *  Defines a <code>BrokerProcessor</code> miter for use with the builders.
 */

public interface BrokerProcessorMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidProcessorMiter,
            org.osid.provisioning.rules.BrokerProcessor {


    /**
     *  Sets the fixed lease duration.
     *
     *  @param duration a fixed lease duration
     *  @throws org.osid.NullArgumentException <code>duration</code>
     *          is <code>null</code>
     */

    public void setFixedLeaseDuration(org.osid.calendaring.Duration duration);


    /**
     *  Adds a BrokerProcessor record.
     *
     *  @param record a brokerProcessor record
     *  @param recordType the type of brokerProcessor record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addBrokerProcessorRecord(org.osid.provisioning.rules.records.BrokerProcessorRecord record, org.osid.type.Type recordType);
}       


