//
// AbstractQueryQueueConstrainerLookupSession.java
//
//    An inline adapter that maps a QueueConstrainerLookupSession to
//    a QueueConstrainerQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.tracking.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a QueueConstrainerLookupSession to
 *  a QueueConstrainerQuerySession.
 */

public abstract class AbstractQueryQueueConstrainerLookupSession
    extends net.okapia.osid.jamocha.tracking.rules.spi.AbstractQueueConstrainerLookupSession
    implements org.osid.tracking.rules.QueueConstrainerLookupSession {

      private boolean activeonly    = false;

    private final org.osid.tracking.rules.QueueConstrainerQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryQueueConstrainerLookupSession.
     *
     *  @param querySession the underlying queue constrainer query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryQueueConstrainerLookupSession(org.osid.tracking.rules.QueueConstrainerQuerySession querySession) {
        nullarg(querySession, "queue constrainer query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>FrontOffice</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>FrontOffice Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFrontOfficeId() {
        return (this.session.getFrontOfficeId());
    }


    /**
     *  Gets the <code>FrontOffice</code> associated with this 
     *  session.
     *
     *  @return the <code>FrontOffice</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOffice getFrontOffice()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getFrontOffice());
    }


    /**
     *  Tests if this user can perform <code>QueueConstrainer</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupQueueConstrainers() {
        return (this.session.canSearchQueueConstrainers());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include queue constrainers in front offices which
     *  are children of this front office in the front office
     *  hierarchy.
     */

    @OSID @Override
    public void useFederatedFrontOfficeView() {
        this.session.useFederatedFrontOfficeView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this front office only.
     */

    @OSID @Override
    public void useIsolatedFrontOfficeView() {
        this.session.useIsolatedFrontOfficeView();
        return;
    }
    

    /**
     *  Only active queue constrainers are returned by methods in this
     *  session.
     */
     
    @OSID @Override
    public void useActiveQueueConstrainerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive queue constrainers are returned by methods
     *  in this session.
     */
    
    @OSID @Override
    public void useAnyStatusQueueConstrainerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>QueueConstrainer</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>QueueConstrainer</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>QueueConstrainer</code> and retained for compatibility.
     *
     *  In active mode, queue constrainers are returned that are
     *  currently active. In any status mode, active and inactive
     *  queue constrainers are returned.
     *
     *  @param  queueConstrainerId <code>Id</code> of the
     *          <code>QueueConstrainer</code>
     *  @return the queue constrainer
     *  @throws org.osid.NotFoundException <code>queueConstrainerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>queueConstrainerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainer getQueueConstrainer(org.osid.id.Id queueConstrainerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.tracking.rules.QueueConstrainerQuery query = getQuery();
        query.matchId(queueConstrainerId, true);
        org.osid.tracking.rules.QueueConstrainerList queueConstrainers = this.session.getQueueConstrainersByQuery(query);
        if (queueConstrainers.hasNext()) {
            return (queueConstrainers.getNextQueueConstrainer());
        } 
        
        throw new org.osid.NotFoundException(queueConstrainerId + " not found");
    }


    /**
     *  Gets a <code>QueueConstrainerList</code> corresponding to the
     *  given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  queueConstrainers specified in the <code>Id</code> list, in
     *  the order of the list, including duplicates, or an error
     *  results if an <code>Id</code> in the supplied list is not
     *  found or inaccessible. Otherwise, inaccessible
     *  <code>QueueConstrainers</code> may be omitted from the list
     *  and may present the elements in any order including returning
     *  a unique set.
     *
     *  In active mode, queue constrainers are returned that are
     *  currently active. In any status mode, active and inactive
     *  queue constrainers are returned.
     *
     *  @param  queueConstrainerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>QueueConstrainer</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerList getQueueConstrainersByIds(org.osid.id.IdList queueConstrainerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.tracking.rules.QueueConstrainerQuery query = getQuery();

        try (org.osid.id.IdList ids = queueConstrainerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getQueueConstrainersByQuery(query));
    }


    /**
     *  Gets a <code>QueueConstrainerList</code> corresponding to the
     *  given queue constrainer genus <code>Type</code> which does not
     *  include queue constrainers of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known queue
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those queue constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, queue constrainers are returned that are
     *  currently active. In any status mode, active and inactive
     *  queue constrainers are returned.
     *
     *  @param  queueConstrainerGenusType a queueConstrainer genus type 
     *  @return the returned <code>QueueConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerList getQueueConstrainersByGenusType(org.osid.type.Type queueConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.tracking.rules.QueueConstrainerQuery query = getQuery();
        query.matchGenusType(queueConstrainerGenusType, true);
        return (this.session.getQueueConstrainersByQuery(query));
    }


    /**
     *  Gets a <code>QueueConstrainerList</code> corresponding to the
     *  given queue constrainer genus <code>Type</code> and include
     *  any additional queue constrainers with genus types derived
     *  from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known queue
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those queue constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, queue constrainers are returned that are
     *  currently active. In any status mode, active and inactive
     *  queue constrainers are returned.
     *
     *  @param  queueConstrainerGenusType a queueConstrainer genus type 
     *  @return the returned <code>QueueConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerList getQueueConstrainersByParentGenusType(org.osid.type.Type queueConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.tracking.rules.QueueConstrainerQuery query = getQuery();
        query.matchParentGenusType(queueConstrainerGenusType, true);
        return (this.session.getQueueConstrainersByQuery(query));
    }


    /**
     *  Gets a <code>QueueConstrainerList</code> containing the given
     *  queue constrainer record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known queue
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those queue constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, queue constrainers are returned that are
     *  currently active. In any status mode, active and inactive
     *  queue constrainers are returned.
     *
     *  @param  queueConstrainerRecordType a queueConstrainer record type 
     *  @return the returned <code>QueueConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerList getQueueConstrainersByRecordType(org.osid.type.Type queueConstrainerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.tracking.rules.QueueConstrainerQuery query = getQuery();
        query.matchRecordType(queueConstrainerRecordType, true);
        return (this.session.getQueueConstrainersByQuery(query));
    }

    
    /**
     *  Gets all <code>QueueConstrainers</code>. 
     *
     *  In plenary mode, the returned list contains all known queue
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those queue constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, queue constrainers are returned that are
     *  currently active. In any status mode, active and inactive
     *  queue constrainers are returned.
     *
     *  @return a list of <code>QueueConstrainers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerList getQueueConstrainers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.tracking.rules.QueueConstrainerQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getQueueConstrainersByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.tracking.rules.QueueConstrainerQuery getQuery() {
        org.osid.tracking.rules.QueueConstrainerQuery query = this.session.getQueueConstrainerQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
