//
// AbstractProvisionQueryInspector.java
//
//     A template for making a ProvisionQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.provision.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for provisions.
 */

public abstract class AbstractProvisionQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQueryInspector
    implements org.osid.provisioning.ProvisionQueryInspector {

    private final java.util.Collection<org.osid.provisioning.records.ProvisionQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the broker <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBrokerIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the broker query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerQueryInspector[] getBrokerTerms() {
        return (new org.osid.provisioning.BrokerQueryInspector[0]);
    }


    /**
     *  Gets the provisionable <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProvisionableIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the provisionable query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableQueryInspector[] getProvisionableTerms() {
        return (new org.osid.provisioning.ProvisionableQueryInspector[0]);
    }


    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRecipientIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getRecipientTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the request <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRequestIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the request query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.RequestQueryInspector[] getRequestTerms() {
        return (new org.osid.provisioning.RequestQueryInspector[0]);
    }


    /**
     *  Gets the provision date query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getProvisionDateTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the leased query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getLeasedTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the must return query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getMustReturnTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the due date query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getDueDateTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the cost query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getCostTerms() {
        return (new org.osid.search.terms.CurrencyRangeTerm[0]);
    }


    /**
     *  Gets the rate amount query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getRateAmountTerms() {
        return (new org.osid.search.terms.CurrencyRangeTerm[0]);
    }


    /**
     *  Gets the rate period query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getRatePeriodTerms() {
        return (new org.osid.search.terms.DurationRangeTerm[0]);
    }


    /**
     *  Gets the provision return query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionReturnQueryInspector[] getProvisionReturnTerms() {
        return (new org.osid.provisioning.ProvisionReturnQueryInspector[0]);
    }


    /**
     *  Gets the distributor <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDistributorIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the distributor query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQueryInspector[] getDistributorTerms() {
        return (new org.osid.provisioning.DistributorQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given provision query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a provision implementing the requested record.
     *
     *  @param provisionRecordType a provision record type
     *  @return the provision query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>provisionRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(provisionRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.ProvisionQueryInspectorRecord getProvisionQueryInspectorRecord(org.osid.type.Type provisionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.ProvisionQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(provisionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(provisionRecordType + " is not supported");
    }


    /**
     *  Adds a record to this provision query. 
     *
     *  @param provisionQueryInspectorRecord provision query inspector
     *         record
     *  @param provisionRecordType provision record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addProvisionQueryInspectorRecord(org.osid.provisioning.records.ProvisionQueryInspectorRecord provisionQueryInspectorRecord, 
                                                   org.osid.type.Type provisionRecordType) {

        addRecordType(provisionRecordType);
        nullarg(provisionRecordType, "provision record type");
        this.records.add(provisionQueryInspectorRecord);        
        return;
    }
}
