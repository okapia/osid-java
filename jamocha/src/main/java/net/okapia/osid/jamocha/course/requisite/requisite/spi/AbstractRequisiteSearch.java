//
// AbstractRequisiteSearch.java
//
//     A template for making a Requisite Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.requisite.requisite.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing requisite searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractRequisiteSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.course.requisite.RequisiteSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.course.requisite.records.RequisiteSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.course.requisite.RequisiteSearchOrder requisiteSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of requisites. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  requisiteIds list of requisites
     *  @throws org.osid.NullArgumentException
     *          <code>requisiteIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongRequisites(org.osid.id.IdList requisiteIds) {
        while (requisiteIds.hasNext()) {
            try {
                this.ids.add(requisiteIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongRequisites</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of requisite Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getRequisiteIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  requisiteSearchOrder requisite search order 
     *  @throws org.osid.NullArgumentException
     *          <code>requisiteSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>requisiteSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderRequisiteResults(org.osid.course.requisite.RequisiteSearchOrder requisiteSearchOrder) {
	this.requisiteSearchOrder = requisiteSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.course.requisite.RequisiteSearchOrder getRequisiteSearchOrder() {
	return (this.requisiteSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given requisite search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a requisite implementing the requested record.
     *
     *  @param requisiteSearchRecordType a requisite search record
     *         type
     *  @return the requisite search record
     *  @throws org.osid.NullArgumentException
     *          <code>requisiteSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(requisiteSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.requisite.records.RequisiteSearchRecord getRequisiteSearchRecord(org.osid.type.Type requisiteSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.course.requisite.records.RequisiteSearchRecord record : this.records) {
            if (record.implementsRecordType(requisiteSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(requisiteSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this requisite search. 
     *
     *  @param requisiteSearchRecord requisite search record
     *  @param requisiteSearchRecordType requisite search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRequisiteSearchRecord(org.osid.course.requisite.records.RequisiteSearchRecord requisiteSearchRecord, 
                                           org.osid.type.Type requisiteSearchRecordType) {

        addRecordType(requisiteSearchRecordType);
        this.records.add(requisiteSearchRecord);        
        return;
    }
}
