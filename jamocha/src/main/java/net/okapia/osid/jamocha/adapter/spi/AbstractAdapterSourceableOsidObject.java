//
// AbstractAdapterSourceableOsidObject.java
//
//     Defines an Sourceable wrapper.
//
//
// Tom Coppeto
// Okapia
// 20 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an adapter for <code>Sourceable</code> around an OsidObject.
 */

public abstract class AbstractAdapterSourceableOsidObject
    extends AbstractAdapterOsidObject
    implements org.osid.OsidObject,
               org.osid.Sourceable {               

    private final org.osid.Sourceable sourceable;


    /**
     *  Constructs a new <code>AbstractAdapterSourceableOsidObject</code>.
     *
     *  @param object
     *  @throws org.osid.NullArgumentException
     *          <code>object</code> is <code>null</code>
     */

    protected AbstractAdapterSourceableOsidObject(org.osid.OsidObject object) {
        super(object);

        if (!(object instanceof org.osid.Sourceable)) {
            throw new org.osid.UnsupportedException("object not a Sourceable");
        }

        this.sourceable = (org.osid.Sourceable) object;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> Provider </code> of
     *  this <code> Sourceable. </code>
     *
     *  @return the <code> Provider Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getProviderId() {
        return (this.sourceable.getProviderId());
    }


    /**
     *  Gets the <code> Resource </code> representing the provider of
     *  this sourceable.
     *
     *  @return the provider 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getProvider()
        throws org.osid.OperationFailedException {
        
        return (this.sourceable.getProvider());
    }


    /**
     *  Gets a branding, such as an image or logo, expressed using the
     *  <code> Asset </code> interface.
     *
     *  @return a list of assets 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.repository.AssetList getBranding()
        throws org.osid.OperationFailedException {

        return (this.sourceable.getBranding());
    }
}
