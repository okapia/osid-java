//
// InvariantMapAvailabilityLookupSession
//
//    Implements an Availability lookup service backed by a fixed collection of
//    availabilities.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing;


/**
 *  Implements an Availability lookup service backed by a fixed
 *  collection of availabilities. The availabilities are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapAvailabilityLookupSession
    extends net.okapia.osid.jamocha.core.resourcing.spi.AbstractMapAvailabilityLookupSession
    implements org.osid.resourcing.AvailabilityLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapAvailabilityLookupSession</code> with no
     *  availabilities.
     *  
     *  @param foundry the foundry
     *  @throws org.osid.NullArgumnetException {@code foundry} is
     *          {@code null}
     */

    public InvariantMapAvailabilityLookupSession(org.osid.resourcing.Foundry foundry) {
        setFoundry(foundry);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAvailabilityLookupSession</code> with a single
     *  availability.
     *  
     *  @param foundry the foundry
     *  @param availability an single availability
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code availability} is <code>null</code>
     */

      public InvariantMapAvailabilityLookupSession(org.osid.resourcing.Foundry foundry,
                                               org.osid.resourcing.Availability availability) {
        this(foundry);
        putAvailability(availability);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAvailabilityLookupSession</code> using an array
     *  of availabilities.
     *  
     *  @param foundry the foundry
     *  @param availabilities an array of availabilities
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code availabilities} is <code>null</code>
     */

      public InvariantMapAvailabilityLookupSession(org.osid.resourcing.Foundry foundry,
                                               org.osid.resourcing.Availability[] availabilities) {
        this(foundry);
        putAvailabilities(availabilities);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAvailabilityLookupSession</code> using a
     *  collection of availabilities.
     *
     *  @param foundry the foundry
     *  @param availabilities a collection of availabilities
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code availabilities} is <code>null</code>
     */

      public InvariantMapAvailabilityLookupSession(org.osid.resourcing.Foundry foundry,
                                               java.util.Collection<? extends org.osid.resourcing.Availability> availabilities) {
        this(foundry);
        putAvailabilities(availabilities);
        return;
    }
}
