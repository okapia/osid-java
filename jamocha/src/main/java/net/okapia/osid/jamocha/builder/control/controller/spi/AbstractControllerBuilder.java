//
// AbstractController.java
//
//     Defines a Controller builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.control.controller.spi;


/**
 *  Defines a <code>Controller</code> builder.
 */

public abstract class AbstractControllerBuilder<T extends AbstractControllerBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOperableOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.control.controller.ControllerMiter controller;


    /**
     *  Constructs a new <code>AbstractControllerBuilder</code>.
     *
     *  @param controller the controller to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractControllerBuilder(net.okapia.osid.jamocha.builder.control.controller.ControllerMiter controller) {
        super(controller);
        this.controller = controller;
        return;
    }


    /**
     *  Builds the controller.
     *
     *  @return the new controller
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.NullArgumentException <code>controller</code>
     *          is <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.control.Controller build() {
        (new net.okapia.osid.jamocha.builder.validator.control.controller.ControllerValidator(getValidations())).validate(this.controller);
        return (new net.okapia.osid.jamocha.builder.control.controller.ImmutableController(this.controller));
    }


    /**
     *  Gets the controller. This method is used to get the miter
     *  interface for further updates. Use <code>build()</code> to
     *  finalize and validate construction.
     *
     *  @return the new controller
     */

    @Override
    public net.okapia.osid.jamocha.builder.control.controller.ControllerMiter getMiter() {
        return (this.controller);
    }


    /**
     *  Sets the address.
     *
     *  @param address an address
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>address</code> is <code>null</code>
     */

    public T address(String address) {
        getMiter().setAddress(address);
        return (self());
    }


    /**
     *  Sets the model.
     *
     *  @param model a model
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>model</code> is <code>null</code>
     */

    public T model(org.osid.inventory.Model model) {
        getMiter().setModel(model);
        return (self());
    }


    /**
     *  Sets the version.
     *
     *  @param version a version
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>version</code> is <code>null</code>
     */

    public T version(org.osid.installation.Version version) {
        getMiter().setVersion(version);
        return (self());
    }


    /**
     *  Sets the toggleable.
     *
     *  @param toggleable {@code true} if a toggle, {@code false}
     *         otherwise
     */

    public T toggleable(boolean toggleable) {
        getMiter().setToggleable(toggleable);
        return (self());
    }


    /**
     *  Sets the variable.
     *
     *  @param variable {@code true} if a variable dial, {@code false}
     *         otherwise
     */

    public T variable(boolean variable) {
        getMiter().setVariable(variable);
        return (self());
    }


    /**
     *  Sets the variable by percentage.
     *
     *  @param variableByPercentage {@code true} if a variable
     *         percentage dial, {@code false} otherwise
     */

    public T variableByPercentage(boolean variableByPercentage) {
        getMiter().setVariableByPercentage(variableByPercentage);
        return (self());
    }


    /**
     *  Sets the variable minimum.
     *
     *  @param value a variable minimum
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    public T variableMinimum(java.math.BigDecimal value) {
        getMiter().setVariableMinimum(value);
        return (self());
    }


    /**
     *  Sets the variable maximum.
     *
     *  @param value a variable maximum
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    public T variableMaximum(java.math.BigDecimal value) {
        getMiter().setVariableMaximum(value);
        return (self());
    }


    /**
     *  Sets the variable increment.
     *
     *  @param increment a variable increment
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>increment</code>
     *          is <code>null</code>
     */

    public T increment(java.math.BigDecimal increment) {
        getMiter().setVariableIncrement(increment);
        return (self());
    }


    /**
     *  Adds a discreet state.
     *
     *  @param state a discreet state
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>state</code> is <code>null</code>
     */

    public T state(org.osid.process.State state) {
        getMiter().addDiscreetState(state);
        return (self());
    }


    /**
     *  Sets all the discreet states.
     *
     *  @param states a collection of discreet states
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>states</code> is <code>null</code>
     */

    public T states(java.util.Collection<org.osid.process.State> states) {
        getMiter().setDiscreetStates(states);
        return (self());
    }


    /**
     *  Adds a Controller record.
     *
     *  @param record a controller record
     *  @param recordType the type of controller record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.control.records.ControllerRecord record, org.osid.type.Type recordType) {
        getMiter().addControllerRecord(record, recordType);
        return (self());
    }
}       


