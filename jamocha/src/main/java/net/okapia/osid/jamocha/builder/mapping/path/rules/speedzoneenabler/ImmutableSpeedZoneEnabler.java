//
// ImmutableSpeedZoneEnabler.java
//
//     Wraps a mutable SpeedZoneEnabler to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.mapping.path.rules.speedzoneenabler;


/**
 *  Wraps a mutable <code>SpeedZoneEnabler</code> to hide modifiers. This
 *  wrapper provides an immutized SpeedZoneEnabler from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying speedZoneEnabler whose state changes are visible.
 */

public final class ImmutableSpeedZoneEnabler
    extends net.okapia.osid.jamocha.builder.mapping.path.rules.speedzoneenabler.spi.AbstractImmutableSpeedZoneEnabler
    implements org.osid.mapping.path.rules.SpeedZoneEnabler {


    /**
     *  Constructs a new <code>ImmutableSpeedZoneEnabler</code>.
     *
     *  @param speedZoneEnabler the speed zone enabler
     *  @throws org.osid.NullArgumentException <code>speedZoneEnabler</code>
     *          is <code>null</code>
     */

    public ImmutableSpeedZoneEnabler(org.osid.mapping.path.rules.SpeedZoneEnabler speedZoneEnabler) {
        super(speedZoneEnabler);
        return;
    }
}

