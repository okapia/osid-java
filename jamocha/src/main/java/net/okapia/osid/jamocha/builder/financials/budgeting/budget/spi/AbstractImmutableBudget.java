//
// AbstractImmutableBudget.java
//
//     Wraps a mutable Budget to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.financials.budgeting.budget.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Budget</code> to hide modifiers. This
 *  wrapper provides an immutized Budget from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying budget whose state changes are visible.
 */

public abstract class AbstractImmutableBudget
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.financials.budgeting.Budget {

    private final org.osid.financials.budgeting.Budget budget;


    /**
     *  Constructs a new <code>AbstractImmutableBudget</code>.
     *
     *  @param budget the budget to immutablize
     *  @throws org.osid.NullArgumentException <code>budget</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableBudget(org.osid.financials.budgeting.Budget budget) {
        super(budget);
        this.budget = budget;
        return;
    }


    /**
     *  Gets the actvity <code> Id. </code> 
     *
     *  @return the activity <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getActivityId() {
        return (this.budget.getActivityId());
    }


    /**
     *  Gets the activity. 
     *
     *  @return the activity 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.financials.Activity getActivity()
        throws org.osid.OperationFailedException {

        return (this.budget.getActivity());
    }


    /**
     *  Gets the <code> Id </code> of the <code> FiscalPeriod. </code> 
     *
     *  @return the <code> FiscalPeriod </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getFiscalPeriodId() {
        return (this.budget.getFiscalPeriodId());
    }


    /**
     *  Gets the <code> FiscalPeriod. </code> 
     *
     *  @return the fiscal period 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriod getFiscalPeriod()
        throws org.osid.OperationFailedException {

        return (this.budget.getFiscalPeriod());
    }


    /**
     *  Gets the budget record corresponding to the given <code> Budget 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> budgetRecordType 
     *  </code> may be the <code> Type </code> returned in <code> 
     *  getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(budgetRecordType) </code> 
     *  is <code> true </code> . 
     *
     *  @param  budgetRecordType the type of budget record to retrieve 
     *  @return the budget record 
     *  @throws org.osid.NullArgumentException <code> budgetRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(budgetRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.records.BudgetRecord getBudgetRecord(org.osid.type.Type budgetRecordType)
        throws org.osid.OperationFailedException {

        return (this.budget.getBudgetRecord(budgetRecordType));
    }
}

