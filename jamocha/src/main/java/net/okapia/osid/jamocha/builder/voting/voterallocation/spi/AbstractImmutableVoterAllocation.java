//
// AbstractImmutableVoterAllocation.java
//
//     Wraps a mutable VoterAllocation to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.voting.voterallocation.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>VoterAllocation</code> to hide modifiers. This
 *  wrapper provides an immutized VoterAllocation from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying voterAllocation whose state changes are visible.
 */

public abstract class AbstractImmutableVoterAllocation
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.voting.VoterAllocation {

    private final org.osid.voting.VoterAllocation voterAllocation;


    /**
     *  Constructs a new <code>AbstractImmutableVoterAllocation</code>.
     *
     *  @param voterAllocation the voter allocation to immutablize
     *  @throws org.osid.NullArgumentException <code>voterAllocation</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableVoterAllocation(org.osid.voting.VoterAllocation voterAllocation) {
        super(voterAllocation);
        this.voterAllocation = voterAllocation;
        return;
    }


    /**
     *  Gets the race <code> Id </code> of the vote. 
     *
     *  @return the ballot item <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getRaceId() {
        return (this.voterAllocation.getRaceId());
    }


    /**
     *  Gets the race of the vote. 
     *
     *  @return the race 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.voting.Race getRace()
        throws org.osid.OperationFailedException {

        return (this.voterAllocation.getRace());
    }


    /**
     *  Gets the resource <code> Id. </code> 
     *
     *  @return a resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getVoterId() {
        return (this.voterAllocation.getVoterId());
    }


    /**
     *  Gets the <code> Resource. </code> 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getVoter()
        throws org.osid.OperationFailedException {

        return (this.voterAllocation.getVoter());
    }


    /**
     *  Gets the total number of votes the resource can cast in this
     *  race.
     *
     *  @return the number of votes 
     */

    @OSID @Override
    public long getTotalVotes() {
        return (this.voterAllocation.getTotalVotes());
    }


    /**
     *  Gets the maximum votes per candidate the resource can cast in
     *  this race.
     *
     *  @return the max votes per candidate
     */

    @OSID @Override
    public long getMaxVotesPerCandidate() {
        return (this.voterAllocation.getMaxVotesPerCandidate());
    }


    /**
     *  Tests if this resource can vote for a limited number of
     *  candidates.
     *
     *  @return <code> true </code> if a limit on the number of
     *          candidates exists, <code> false </code> if no limit
     *          exists
     */

    @OSID @Override
    public boolean hasMaxCandidates() {
        return (this.voterAllocation.hasMaxCandidates());
    }


    /**
     *  Gets the maximum number of candidates for which this resource
     *  can vote.
     *
     *  @return the maximum candidates 
     *  @throws org.osid.IllegalStateException <code> hasMaxCandidates() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public long getMaxCandidates() {
        return (this.voterAllocation.getMaxCandidates());
    }


    /**
     *  Tests if this resource gets the number of votes cast for a candidate 
     *  when the candidate is no longer running in a race. 
     *
     *  @return <code> true </code> if votes are returned to the resource when 
     *          the candidate drops from the race, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean reallocatesVotesWhenCandidateDrops() {
        return (this.voterAllocation.reallocatesVotesWhenCandidateDrops());
    }


    /**
     *  Gets the voter allocation record corresponding to the given
     *  <code> VoterAllocation </code> record <code> Type. </code>
     *  This method is used to retrieve an object implementing the
     *  requested record. The <code> voterAllocationRecordType </code>
     *  may be the <code> Type </code> returned in <code>
     *  getRecordTypes() </code> or any of its parents in a <code>
     *  Type </code> hierarchy where <code>
     *  hasRecordType(voterAllocationRecordType) </code> is <code>
     *  true </code>.
     *
     *  @param  voterAllocationRecordType the type of the record to retrieve 
     *  @return the voter allocation record 
     *  @throws org.osid.NullArgumentException <code> 
     *          voterAllocationRecordType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(voterAllocationRecordType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.records.VoterAllocationRecord getVoterAllocationRecord(org.osid.type.Type voterAllocationRecordType)
        throws org.osid.OperationFailedException {

        return (this.voterAllocation.getVoterAllocationRecord(voterAllocationRecordType));
    }
}

