//
// AbstractRuleQueryInspector.java
//
//     A template for making a RuleQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.rules.rule.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for rules.
 */

public abstract class AbstractRuleQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.rules.RuleQueryInspector {

    private final java.util.Collection<org.osid.rules.records.RuleQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the engine <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEngineIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the engine query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.rules.EngineQueryInspector[] getEngineTerms() {
        return (new org.osid.rules.EngineQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given rule query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a rule implementing the requested record.
     *
     *  @param ruleRecordType a rule record type
     *  @return the rule query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>ruleRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(ruleRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.rules.records.RuleQueryInspectorRecord getRuleQueryInspectorRecord(org.osid.type.Type ruleRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.rules.records.RuleQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(ruleRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(ruleRecordType + " is not supported");
    }


    /**
     *  Adds a record to this rule query. 
     *
     *  @param ruleQueryInspectorRecord rule query inspector
     *         record
     *  @param ruleRecordType rule record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRuleQueryInspectorRecord(org.osid.rules.records.RuleQueryInspectorRecord ruleQueryInspectorRecord, 
                                                   org.osid.type.Type ruleRecordType) {

        addRecordType(ruleRecordType);
        nullarg(ruleRecordType, "rule record type");
        this.records.add(ruleQueryInspectorRecord);        
        return;
    }
}
