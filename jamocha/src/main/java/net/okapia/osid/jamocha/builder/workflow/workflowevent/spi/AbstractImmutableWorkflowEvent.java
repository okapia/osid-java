//
// AbstractImmutableWorkflowEvent.java
//
//     Wraps a mutable WorkflowEvent to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.workflow.workflowevent.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>WorkflowEvent</code> to hide modifiers. This
 *  wrapper provides an immutized WorkflowEvent from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying workflowEvent whose state changes are visible.
 */

public abstract class AbstractImmutableWorkflowEvent
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.workflow.WorkflowEvent {

    private final org.osid.workflow.WorkflowEvent workflowEvent;


    /**
     *  Constructs a new <code>AbstractImmutableWorkflowEvent</code>.
     *
     *  @param workflowEvent the workflow event to immutablize
     *  @throws org.osid.NullArgumentException <code>workflowEvent</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableWorkflowEvent(org.osid.workflow.WorkflowEvent workflowEvent) {
        super(workflowEvent);
        this.workflowEvent = workflowEvent;
        return;
    }


    /**
     *  Gets the timestamp of this event. 
     *
     *  @return the timestamp 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getTimestamp() {
        return (this.workflowEvent.getTimestamp());
    }


    /**
     *  Gets the <code> Id </code> of the process. 
     *
     *  @return the process <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getProcessId() {
        return (this.workflowEvent.getProcessId());
    }


    /**
     *  Gets the process. 
     *
     *  @return the process 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.workflow.Process getProcess()
        throws org.osid.OperationFailedException {

        return (this.workflowEvent.getProcess());
    }


    /**
     *  Gets the <code> Id </code> of the resource that caused this event. 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getWorkerId() {
        return (this.workflowEvent.getWorkerId());
    }


    /**
     *  Gets the resource that caused this event. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getWorker()
        throws org.osid.OperationFailedException {

        return (this.workflowEvent.getWorker());
    }


    /**
     *  Gets the <code> Id </code> of the agent that caused this event. 
     *
     *  @return the agent <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getWorkingAgentId() {
        return (this.workflowEvent.getWorkingAgentId());
    }


    /**
     *  Gets the agent that caused this event. 
     *
     *  @return the agent 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getWorkingAgent()
        throws org.osid.OperationFailedException {

        return (this.workflowEvent.getWorkingAgent());
    }


    /**
     *  Gets the <code> Id </code> of the work. 
     *
     *  @return the work <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getWorkId() {
        return (this.workflowEvent.getWorkId());
    }


    /**
     *  Gets the work. 
     *
     *  @return the work 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.workflow.Work getWork()
        throws org.osid.OperationFailedException {

        return (this.workflowEvent.getWork());
    }


    /**
     *  Tests if this work event indicates the work has been canceled the 
     *  workflow and is not associated with a step at the time of this event. 
     *
     *  @return <code> true </code> if the work canceled, <code> false </code> 
     *          if the work is associated with a step in this event 
     */

    @OSID @Override
    public boolean didCancel() {
        return (this.workflowEvent.didCancel());
    }


    /**
     *  Gets the <code> Id </code> of the step at which the work is in at the 
     *  time of this event. 
     *
     *  @return the step <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> didComplete() </code> or 
     *          <code> didCancel() </code> is <code> true </code> 
     */

    @OSID @Override
    public org.osid.id.Id getStepId() {
        return (this.workflowEvent.getStepId());
    }


    /**
     *  Gets the step at which the work is in at the time of this event. 
     *
     *  @return the step 
     *  @throws org.osid.IllegalStateException <code> didComplete() </code> or 
     *          <code> didCancel() </code> is <code> true </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.workflow.Step getStep()
        throws org.osid.OperationFailedException {

        return (this.workflowEvent.getStep());
    }


    /**
     *  Gets the workflow event record corresponding to the given <code> 
     *  WorkflowEvent </code> record <code> Type. </code> This method is used 
     *  to retrieve an object implementing the requested record. The <code> 
     *  workflowEventRecordType </code> may be the <code> Type </code> 
     *  returned in <code> getRecordTypes() </code> or any of its parents in a 
     *  <code> Type </code> hierarchy where <code> 
     *  hasRecordType(workflowEventRecordType) </code> is <code> true </code> 
     *  . 
     *
     *  @param  workFlowRecordType the type of workflow event record to 
     *          retrieve 
     *  @return the workflow event record 
     *  @throws org.osid.NullArgumentException <code> workRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(workflowEventRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.records.WorkflowEventRecord getWorkflowEventRecord(org.osid.type.Type workFlowRecordType)
        throws org.osid.OperationFailedException {

        return (this.workflowEvent.getWorkflowEventRecord(workFlowRecordType));
    }
}

