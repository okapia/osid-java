//
// AbstractAssemblyInquiryEnablerQuery.java
//
//     An InquiryEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.inquiry.rules.inquiryenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An InquiryEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblyInquiryEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.inquiry.rules.InquiryEnablerQuery,
               org.osid.inquiry.rules.InquiryEnablerQueryInspector,
               org.osid.inquiry.rules.InquiryEnablerSearchOrder {

    private final java.util.Collection<org.osid.inquiry.rules.records.InquiryEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.inquiry.rules.records.InquiryEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.inquiry.rules.records.InquiryEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyInquiryEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyInquiryEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to the audit. 
     *
     *  @param  auditId the audit <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> auditId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledAuditId(org.osid.id.Id auditId, boolean match) {
        getAssembler().addIdTerm(getRuledAuditIdColumn(), auditId, match);
        return;
    }


    /**
     *  Clears the audit <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledAuditIdTerms() {
        getAssembler().clearTerms(getRuledAuditIdColumn());
        return;
    }


    /**
     *  Gets the audit <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledAuditIdTerms() {
        return (getAssembler().getIdTerms(getRuledAuditIdColumn()));
    }


    /**
     *  Gets the RuledAuditId column name.
     *
     * @return the column name
     */

    protected String getRuledAuditIdColumn() {
        return ("ruled_audit_id");
    }


    /**
     *  Tests if an <code> AuditQuery </code> is available. 
     *
     *  @return <code> true </code> if an audit query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledAuditQuery() {
        return (false);
    }


    /**
     *  Gets the query for an audit. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the audit query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledAuditQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditQuery getRuledAuditQuery() {
        throw new org.osid.UnimplementedException("supportsRuledAuditQuery() is false");
    }


    /**
     *  Matches enablers mapped to any audit. 
     *
     *  @param  match <code> true </code> for enablers mapped to any audit, 
     *          <code> false </code> to match enablers mapped to no audits 
     */

    @OSID @Override
    public void matchAnyRuledAudit(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledAuditColumn(), match);
        return;
    }


    /**
     *  Clears the audit query terms. 
     */

    @OSID @Override
    public void clearRuledAuditTerms() {
        getAssembler().clearTerms(getRuledAuditColumn());
        return;
    }


    /**
     *  Gets the audit query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.inquiry.AuditQueryInspector[] getRuledAuditTerms() {
        return (new org.osid.inquiry.AuditQueryInspector[0]);
    }


    /**
     *  Gets the RuledAudit column name.
     *
     * @return the column name
     */

    protected String getRuledAuditColumn() {
        return ("ruled_audit");
    }


    /**
     *  Matches enablers mapped to the inquest. 
     *
     *  @param  inquestId the inquest <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchInquestId(org.osid.id.Id inquestId, boolean match) {
        getAssembler().addIdTerm(getInquestIdColumn(), inquestId, match);
        return;
    }


    /**
     *  Clears the inquest <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearInquestIdTerms() {
        getAssembler().clearTerms(getInquestIdColumn());
        return;
    }


    /**
     *  Gets the inquest <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getInquestIdTerms() {
        return (getAssembler().getIdTerms(getInquestIdColumn()));
    }


    /**
     *  Gets the InquestId column name.
     *
     * @return the column name
     */

    protected String getInquestIdColumn() {
        return ("inquest_id");
    }


    /**
     *  Tests if an <code> InquestQuery </code> is available. 
     *
     *  @return <code> true </code> if an inquest query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquestQuery() {
        return (false);
    }


    /**
     *  Gets the query for an inquest. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the inquest query 
     *  @throws org.osid.UnimplementedException <code> supportsInquestQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquestQuery getInquestQuery() {
        throw new org.osid.UnimplementedException("supportsInquestQuery() is false");
    }


    /**
     *  Clears the inquest query terms. 
     */

    @OSID @Override
    public void clearInquestTerms() {
        getAssembler().clearTerms(getInquestColumn());
        return;
    }


    /**
     *  Gets the inquest query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.inquiry.InquestQueryInspector[] getInquestTerms() {
        return (new org.osid.inquiry.InquestQueryInspector[0]);
    }


    /**
     *  Gets the Inquest column name.
     *
     * @return the column name
     */

    protected String getInquestColumn() {
        return ("inquest");
    }


    /**
     *  Tests if this inquiryEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  inquiryEnablerRecordType an inquiry enabler record type 
     *  @return <code>true</code> if the inquiryEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>inquiryEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type inquiryEnablerRecordType) {
        for (org.osid.inquiry.rules.records.InquiryEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(inquiryEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  inquiryEnablerRecordType the inquiry enabler record type 
     *  @return the inquiry enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>inquiryEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(inquiryEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inquiry.rules.records.InquiryEnablerQueryRecord getInquiryEnablerQueryRecord(org.osid.type.Type inquiryEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inquiry.rules.records.InquiryEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(inquiryEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(inquiryEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  inquiryEnablerRecordType the inquiry enabler record type 
     *  @return the inquiry enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>inquiryEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(inquiryEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inquiry.rules.records.InquiryEnablerQueryInspectorRecord getInquiryEnablerQueryInspectorRecord(org.osid.type.Type inquiryEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inquiry.rules.records.InquiryEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(inquiryEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(inquiryEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param inquiryEnablerRecordType the inquiry enabler record type
     *  @return the inquiry enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>inquiryEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(inquiryEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inquiry.rules.records.InquiryEnablerSearchOrderRecord getInquiryEnablerSearchOrderRecord(org.osid.type.Type inquiryEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inquiry.rules.records.InquiryEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(inquiryEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(inquiryEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this inquiry enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param inquiryEnablerQueryRecord the inquiry enabler query record
     *  @param inquiryEnablerQueryInspectorRecord the inquiry enabler query inspector
     *         record
     *  @param inquiryEnablerSearchOrderRecord the inquiry enabler search order record
     *  @param inquiryEnablerRecordType inquiry enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>inquiryEnablerQueryRecord</code>,
     *          <code>inquiryEnablerQueryInspectorRecord</code>,
     *          <code>inquiryEnablerSearchOrderRecord</code> or
     *          <code>inquiryEnablerRecordTypeinquiryEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addInquiryEnablerRecords(org.osid.inquiry.rules.records.InquiryEnablerQueryRecord inquiryEnablerQueryRecord, 
                                      org.osid.inquiry.rules.records.InquiryEnablerQueryInspectorRecord inquiryEnablerQueryInspectorRecord, 
                                      org.osid.inquiry.rules.records.InquiryEnablerSearchOrderRecord inquiryEnablerSearchOrderRecord, 
                                      org.osid.type.Type inquiryEnablerRecordType) {

        addRecordType(inquiryEnablerRecordType);

        nullarg(inquiryEnablerQueryRecord, "inquiry enabler query record");
        nullarg(inquiryEnablerQueryInspectorRecord, "inquiry enabler query inspector record");
        nullarg(inquiryEnablerSearchOrderRecord, "inquiry enabler search odrer record");

        this.queryRecords.add(inquiryEnablerQueryRecord);
        this.queryInspectorRecords.add(inquiryEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(inquiryEnablerSearchOrderRecord);
        
        return;
    }
}
