//
// AbstractTopologyManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.topology.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractTopologyManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.topology.TopologyManager,
               org.osid.topology.TopologyProxyManager {

    private final Types nodeRecordTypes                    = new TypeRefSet();
    private final Types nodeSearchRecordTypes              = new TypeRefSet();

    private final Types edgeRecordTypes                    = new TypeRefSet();
    private final Types edgeSearchRecordTypes              = new TypeRefSet();

    private final Types graphRecordTypes                   = new TypeRefSet();
    private final Types graphSearchRecordTypes             = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractTopologyManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractTopologyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any graph federation is exposed. Federation is exposed when a 
     *  specific graph may be identified, selected and used to create a lookup 
     *  or admin session. Federation is not exposed when a set of graphs 
     *  appears as a single graph. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if traversing topologies is supported. 
     *
     *  @return <code> true </code> if topology traversal is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTopologyTraversal() {
        return (false);
    }


    /**
     *  Tests if topology routing is supported. 
     *
     *  @return <code> true </code> if topology routing is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTopologyRouting() {
        return (false);
    }


    /**
     *  Tests if looking up nodes is supported. 
     *
     *  @return <code> true </code> if node lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsNodeLookup() {
        return (false);
    }


    /**
     *  Tests if querying nodes is supported. 
     *
     *  @return <code> true </code> if node query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsNodeQuery() {
        return (false);
    }


    /**
     *  Tests if searching nodes is supported. 
     *
     *  @return <code> true </code> if node search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsNodeSearch() {
        return (false);
    }


    /**
     *  Tests if node <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if node administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsNodeAdmin() {
        return (false);
    }


    /**
     *  Tests if a node <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if node notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsNodeNotification() {
        return (false);
    }


    /**
     *  Tests if a node graph mapping lookup service is supported. 
     *
     *  @return <code> true </code> if a node graph lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsNodeGraph() {
        return (false);
    }


    /**
     *  Tests if a node graph mapping service is supported. 
     *
     *  @return <code> true </code> if node to graph mapping service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsNodeGraphAssignment() {
        return (false);
    }


    /**
     *  Tests if a node smart graph cataloging service is supported. 
     *
     *  @return <code> true </code> if node smart graphs are supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsNodeSmartGraph() {
        return (false);
    }


    /**
     *  Tests if looking up edges is supported. 
     *
     *  @return <code> true </code> if edge lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEdgeLookup() {
        return (false);
    }


    /**
     *  Tests if searching edges is supported. 
     *
     *  @return <code> true </code> if edge search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEdgeSearch() {
        return (false);
    }


    /**
     *  Tests if edge <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if edge administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEdgeAdmin() {
        return (false);
    }


    /**
     *  Tests if an edge <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if edge notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEdgeNotification() {
        return (false);
    }


    /**
     *  Tests if an edge graph mapping lookup service is supported. 
     *
     *  @return <code> true </code> if an edge graph lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEdgeGraph() {
        return (false);
    }


    /**
     *  Tests if an edge graph mapping service is supported. 
     *
     *  @return <code> true </code> if edge to graph mapping service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEdgeGraphAssignment() {
        return (false);
    }


    /**
     *  Tests if an edgesmart graph cataloging service is supported. 
     *
     *  @return <code> true </code> if edge smart graphs are supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEdgeSmartGraph() {
        return (false);
    }


    /**
     *  Tests if looking up graphs is supported. 
     *
     *  @return <code> true </code> if graph lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGraphLookup() {
        return (false);
    }


    /**
     *  Tests if querying graphs is supported. 
     *
     *  @return <code> true </code> if graph query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGraphQuery() {
        return (false);
    }


    /**
     *  Tests if searching graphs is supported. 
     *
     *  @return <code> true </code> if graph search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGraphSearch() {
        return (false);
    }


    /**
     *  Tests if graph administrative service is supported. 
     *
     *  @return <code> true </code> if graph administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGraphAdmin() {
        return (false);
    }


    /**
     *  Tests if a graph <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if graph notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGraphNotification() {
        return (false);
    }


    /**
     *  Tests for the availability of a graph hierarchy traversal service. 
     *
     *  @return <code> true </code> if graph hierarchy traversal is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGraphHierarchy() {
        return (false);
    }


    /**
     *  Tests for the availability of a graph hierarchy design service. 
     *
     *  @return <code> true </code> if graph hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGraphHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests for the availability of a topology pbatchath service. 
     *
     *  @return <code> true </code> if a topology batch service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTopologyBatch() {
        return (false);
    }


    /**
     *  Tests for the availability of a topology path service. 
     *
     *  @return <code> true </code> if a topology path service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTopologyPath() {
        return (false);
    }


    /**
     *  Tests for the availability of a topology rules service. 
     *
     *  @return <code> true </code> if a topology rules service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTopologyRules() {
        return (false);
    }


    /**
     *  Gets the supported <code> Node </code> record types. 
     *
     *  @return a list containing the supported <code> Node </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getNodeRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.nodeRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Node </code> record type is supported. 
     *
     *  @param  nodeRecordType a <code> Type </code> indicating a <code> Node 
     *          </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> nodeRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsNodeRecordType(org.osid.type.Type nodeRecordType) {
        return (this.nodeRecordTypes.contains(nodeRecordType));
    }


    /**
     *  Adds support for a node record type.
     *
     *  @param nodeRecordType a node record type
     *  @throws org.osid.NullArgumentException
     *  <code>nodeRecordType</code> is <code>null</code>
     */

    protected void addNodeRecordType(org.osid.type.Type nodeRecordType) {
        this.nodeRecordTypes.add(nodeRecordType);
        return;
    }


    /**
     *  Removes support for a node record type.
     *
     *  @param nodeRecordType a node record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>nodeRecordType</code> is <code>null</code>
     */

    protected void removeNodeRecordType(org.osid.type.Type nodeRecordType) {
        this.nodeRecordTypes.remove(nodeRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Node </code> search types. 
     *
     *  @return a list containing the supported <code> Node </code> search 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getNodeSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.nodeSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Node </code> search type is supported. 
     *
     *  @param  nodeSearchRecordType a <code> Type </code> indicating a <code> 
     *          Node </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> nodeSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsNodeSearchRecordType(org.osid.type.Type nodeSearchRecordType) {
        return (this.nodeSearchRecordTypes.contains(nodeSearchRecordType));
    }


    /**
     *  Adds support for a node search record type.
     *
     *  @param nodeSearchRecordType a node search record type
     *  @throws org.osid.NullArgumentException
     *  <code>nodeSearchRecordType</code> is <code>null</code>
     */

    protected void addNodeSearchRecordType(org.osid.type.Type nodeSearchRecordType) {
        this.nodeSearchRecordTypes.add(nodeSearchRecordType);
        return;
    }


    /**
     *  Removes support for a node search record type.
     *
     *  @param nodeSearchRecordType a node search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>nodeSearchRecordType</code> is <code>null</code>
     */

    protected void removeNodeSearchRecordType(org.osid.type.Type nodeSearchRecordType) {
        this.nodeSearchRecordTypes.remove(nodeSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Edge </code> record types. 
     *
     *  @return a list containing the supported <code> Edge </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEdgeRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.edgeRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Edge </code> record type is supported. 
     *
     *  @param  edgeRecordType a <code> Type </code> indicating an <code> Edge 
     *          </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> edgeRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEdgeRecordType(org.osid.type.Type edgeRecordType) {
        return (this.edgeRecordTypes.contains(edgeRecordType));
    }


    /**
     *  Adds support for an edge record type.
     *
     *  @param edgeRecordType an edge record type
     *  @throws org.osid.NullArgumentException
     *  <code>edgeRecordType</code> is <code>null</code>
     */

    protected void addEdgeRecordType(org.osid.type.Type edgeRecordType) {
        this.edgeRecordTypes.add(edgeRecordType);
        return;
    }


    /**
     *  Removes support for an edge record type.
     *
     *  @param edgeRecordType an edge record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>edgeRecordType</code> is <code>null</code>
     */

    protected void removeEdgeRecordType(org.osid.type.Type edgeRecordType) {
        this.edgeRecordTypes.remove(edgeRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Edge </code> search record types. 
     *
     *  @return a list containing the supported <code> Edge </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEdgeSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.edgeSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Edge </code> search record type is 
     *  supported. 
     *
     *  @param  edgeSearchRecordType a <code> Type </code> indicating an 
     *          <code> Edge </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> edgeSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEdgeSearchRecordType(org.osid.type.Type edgeSearchRecordType) {
        return (this.edgeSearchRecordTypes.contains(edgeSearchRecordType));
    }


    /**
     *  Adds support for an edge search record type.
     *
     *  @param edgeSearchRecordType an edge search record type
     *  @throws org.osid.NullArgumentException
     *  <code>edgeSearchRecordType</code> is <code>null</code>
     */

    protected void addEdgeSearchRecordType(org.osid.type.Type edgeSearchRecordType) {
        this.edgeSearchRecordTypes.add(edgeSearchRecordType);
        return;
    }


    /**
     *  Removes support for an edge search record type.
     *
     *  @param edgeSearchRecordType an edge search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>edgeSearchRecordType</code> is <code>null</code>
     */

    protected void removeEdgeSearchRecordType(org.osid.type.Type edgeSearchRecordType) {
        this.edgeSearchRecordTypes.remove(edgeSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Graph </code> record types. 
     *
     *  @return a list containing the supported <code> Graph </code> types 
     */

    @OSID @Override
    public org.osid.type.TypeList getGraphRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.graphRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Graph </code> record type is supported. 
     *
     *  @param  graphRecordType a <code> Type </code> indicating a <code> 
     *          Graph </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> graphRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsGraphRecordType(org.osid.type.Type graphRecordType) {
        return (this.graphRecordTypes.contains(graphRecordType));
    }


    /**
     *  Adds support for a graph record type.
     *
     *  @param graphRecordType a graph record type
     *  @throws org.osid.NullArgumentException
     *  <code>graphRecordType</code> is <code>null</code>
     */

    protected void addGraphRecordType(org.osid.type.Type graphRecordType) {
        this.graphRecordTypes.add(graphRecordType);
        return;
    }


    /**
     *  Removes support for a graph record type.
     *
     *  @param graphRecordType a graph record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>graphRecordType</code> is <code>null</code>
     */

    protected void removeGraphRecordType(org.osid.type.Type graphRecordType) {
        this.graphRecordTypes.remove(graphRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Graph </code> search record types. 
     *
     *  @return a list containing the supported <code> Graph </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getGraphSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.graphSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Graph </code> search record type is 
     *  supported. 
     *
     *  @param  graphSearchRecordType a <code> Type </code> indicating a 
     *          <code> Graph </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> graphSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsGraphSearchRecordType(org.osid.type.Type graphSearchRecordType) {
        return (this.graphSearchRecordTypes.contains(graphSearchRecordType));
    }


    /**
     *  Adds support for a graph search record type.
     *
     *  @param graphSearchRecordType a graph search record type
     *  @throws org.osid.NullArgumentException
     *  <code>graphSearchRecordType</code> is <code>null</code>
     */

    protected void addGraphSearchRecordType(org.osid.type.Type graphSearchRecordType) {
        this.graphSearchRecordTypes.add(graphSearchRecordType);
        return;
    }


    /**
     *  Removes support for a graph search record type.
     *
     *  @param graphSearchRecordType a graph search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>graphSearchRecordType</code> is <code>null</code>
     */

    protected void removeGraphSearchRecordType(org.osid.type.Type graphSearchRecordType) {
        this.graphSearchRecordTypes.remove(graphSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the topology 
     *  traversal service. 
     *
     *  @return a <code> TopologyTraversalSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTopologyTraversal() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.TopologyTraversalSession getTopologyTraversalSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyManager.getTopologyTraversalSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the topology 
     *  traversal service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TopologyTraversalSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTopologyTraversal() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.TopologyTraversalSession getTopologyTraversalSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyProxyManager.getTopologyTraversalSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the topology 
     *  traversal service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the graph 
     *  @return a <code> TopologyTraversalSession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTopologyTraversal() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.TopologyTraversalSession getTopologyTraversalForGraph(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyManager.getTopologyTraversalForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the topology 
     *  traversal service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the graph 
     *  @param  proxy a proxy 
     *  @return a <code> TopologyTraversalSession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTopologyTraversal() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.TopologyTraversalSession getTopologyTraversalForGraph(org.osid.id.Id graphId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyProxyManager.getTopologyTraversalForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the topology 
     *  routing service. 
     *
     *  @return a <code> TopologyRoutingSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTopologyRouting() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.TopologyRoutingSession getTopologyRoutingSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyManager.getTopologyRoutingSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the topology 
     *  routing service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TopologyRoutingSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTopologyRouting() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.TopologyRoutingSession getTopologyRoutingSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyProxyManager.getTopologyRoutingSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the topology 
     *  routing service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the graph 
     *  @return a <code> TopologyRoutingSession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTopologyRouting() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.TopologyRoutingSession getTopologyRoutingForGraph(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyManager.getTopologyRoutingForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the topology 
     *  routing service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the graph 
     *  @param  proxy a proxy 
     *  @return a <code> TopologyRoutingSession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTopologyRouting() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.TopologyRoutingSession getTopologyRoutingForGraph(org.osid.id.Id graphId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyProxyManager.getTopologyRoutingForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the node lookup 
     *  service. 
     *
     *  @return a <code> NodeLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsNodeLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeLookupSession getNodeLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyManager.getNodeLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the node lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> NodeLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsNodeLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeLookupSession getNodeLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyProxyManager.getNodeLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the node lookup 
     *  service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the graph 
     *  @return a <code> NodeLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsNodeLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeLookupSession getNodeLookupSessionForGraph(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyManager.getNodeLookupSessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the node lookup 
     *  service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the graph 
     *  @param  proxy a proxy 
     *  @return a <code> NodeLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsNodeLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeLookupSession getNodeLookupSessionForGraph(org.osid.id.Id graphId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyProxyManager.getNodeLookupSessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the node query 
     *  service. 
     *
     *  @return a <code> NodeQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsNodeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeQuerySession getNodeQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyManager.getNodeQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the node query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> NodeQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsNodeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeQuerySession getNodeQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyProxyManager.getNodeQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the node query 
     *  service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the graph 
     *  @return a <code> NodeQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsNodeQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeQuerySession getNodeQuerySessionForGraph(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyManager.getNodeQuerySessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the node query 
     *  service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the graph 
     *  @param  proxy a proxy 
     *  @return a <code> NodeQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsNodeQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeQuerySession getNodeQuerySessionForGraph(org.osid.id.Id graphId, 
                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyProxyManager.getNodeQuerySessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the node search 
     *  service. 
     *
     *  @return a <code> NodeSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsNodeSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeSearchSession getNodeSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyManager.getNodeSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the node search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> NodeSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsNodeSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeSearchSession getNodeSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyProxyManager.getNodeSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the node search 
     *  service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @return a <code> NodeSearchSession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsNodeSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeSearchSession getNodeSearchSessionForGraph(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyManager.getNodeSearchSessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the node search 
     *  service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @param  proxy a proxy 
     *  @return a <code> NodeSearchSession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsNodeSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeSearchSession getNodeSearchSessionForGraph(org.osid.id.Id graphId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyProxyManager.getNodeSearchSessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the node 
     *  administration service. 
     *
     *  @return a <code> NodeAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsNodeAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeAdminSession getNodeAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyManager.getNodeAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the node 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> NodeAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsNodeAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeAdminSession getNodeAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyProxyManager.getNodeAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the node 
     *  administration service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @return a <code> NodeAdminSession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsNodeAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeAdminSession getNodeAdminSessionForGraph(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyManager.getNodeAdminSessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the node 
     *  administration service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @param  proxy a proxy 
     *  @return a <code> NodeAdminSession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsNodeAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeAdminSession getNodeAdminSessionForGraph(org.osid.id.Id graphId, 
                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyProxyManager.getNodeAdminSessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the node 
     *  notification service. 
     *
     *  @param  nodeReceiver the notification callback 
     *  @return a <code> NodeNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> nodeReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsNodeNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeNotificationSession getNodeNotificationSession(org.osid.topology.NodeReceiver nodeReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyManager.getNodeNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the node 
     *  notification service. 
     *
     *  @param  nodeReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> NodeNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> nodeReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsNodeNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeNotificationSession getNodeNotificationSession(org.osid.topology.NodeReceiver nodeReceiver, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyProxyManager.getNodeNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the node 
     *  notification service for the given graph. 
     *
     *  @param  nodeReceiver the notification callback 
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @return a <code> NodeNotificationSession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> nodeReceiver </code> or 
     *          <code> graphId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsNodeNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeNotificationSession getNodeNotificationSessionForGraph(org.osid.topology.NodeReceiver nodeReceiver, 
                                                                                        org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyManager.getNodeNotificationSessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the node 
     *  notification service for the given graph. 
     *
     *  @param  nodeReceiver the notification callback 
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @param  proxy a proxy 
     *  @return a <code> NodeNotificationSession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> nodeReceiver, graphId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsNodeNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeNotificationSession getNodeNotificationSessionForGraph(org.osid.topology.NodeReceiver nodeReceiver, 
                                                                                        org.osid.id.Id graphId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyProxyManager.getNodeNotificationSessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup node/graph mappings. 
     *
     *  @return a <code> NodeGraphSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsNodeGraph() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeGraphSession getNodeGraphSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyManager.getNodeGraphSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup node/graph mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> NodeGraphSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsNodeGraph() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeGraphSession getNodeGraphSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyProxyManager.getNodeGraphSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning nodes to 
     *  graphs. 
     *
     *  @return a <code> NodeGraphAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsNodeGraphAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeGraphAssignmentSession getNodeGraphAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyManager.getNodeGraphAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning nodes to 
     *  graphs. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> NodeGraphAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsNodeGraphAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeGraphAssignmentSession getNodeGraphAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyProxyManager.getNodeGraphAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage dynamic graphs of 
     *  retlationships. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @return a <code> NodeSmartGraphSession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsNodeSmartGraph() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeSmartGraphSession getNodeSmartGraphSession(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyManager.getNodeSmartGraphSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage dynamic graphs of 
     *  retlationships. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @param  proxy a proxy 
     *  @return a <code> NodeSmartGraphSession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsNodeSmartGraph() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeSmartGraphSession getNodeSmartGraphSession(org.osid.id.Id graphId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyProxyManager.getNodeSmartGraphSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge lookup 
     *  service. 
     *
     *  @return an <code> EdgeLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEdgeLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeLookupSession getEdgeLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyManager.getEdgeLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EdgeLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEdgeLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeLookupSession getEdgeLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyProxyManager.getEdgeLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge lookup 
     *  service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @return an <code> EdgeLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEdgeLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeLookupSession getEdgeLookupSessionForGraph(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyManager.getEdgeLookupSessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge lookup 
     *  service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @param  proxy a proxy 
     *  @return an <code> EdgeLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEdgeLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeLookupSession getEdgeLookupSessionForGraph(org.osid.id.Id graphId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyProxyManager.getEdgeLookupSessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge query 
     *  service. 
     *
     *  @return an <code> EdgeQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEdgeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeQuerySession getEdgeQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyManager.getEdgeQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EdgeQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEdgeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeQuerySession getEdgeQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyProxyManager.getEdgeQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge query 
     *  service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the graph 
     *  @return an <code> EdgeQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEdgeQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeQuerySession getEdgeQuerySessionForGraph(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyManager.getEdgeQuerySessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge query 
     *  service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the graph 
     *  @param  proxy a proxy 
     *  @return an <code> EdgeQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEdgeQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeQuerySession getEdgeQuerySessionForGraph(org.osid.id.Id graphId, 
                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyProxyManager.getEdgeQuerySessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge search 
     *  service. 
     *
     *  @return an <code> EdgeSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEdgeSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeSearchSession getEdgeSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyManager.getEdgeSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EdgeSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEdgeSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeSearchSession getEdgeSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyProxyManager.getEdgeSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge search 
     *  service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @return an <code> EdgeSearchSession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEdgeSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeSearchSession getEdgeSearchSessionForGraph(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyManager.getEdgeSearchSessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge search 
     *  service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @param  proxy a proxy 
     *  @return an <code> EdgeSearchSession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEdgeSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeSearchSession getEdgeSearchSessionForGraph(org.osid.id.Id graphId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyProxyManager.getEdgeSearchSessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge 
     *  administration service. 
     *
     *  @return an <code> EdgeAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEdgeAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeAdminSession getEdgeAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyManager.getEdgeAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EdgeAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEdgeAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeAdminSession getEdgeAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyProxyManager.getEdgeAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge 
     *  administration service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @return an <code> EdgeAdminSession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEdgeAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeAdminSession getEdgeAdminSessionForGraph(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyManager.getEdgeAdminSessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge 
     *  administration service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @param  proxy a proxy 
     *  @return an <code> EdgeAdminSession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEdgeAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeAdminSession getEdgeAdminSessionForGraph(org.osid.id.Id graphId, 
                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyProxyManager.getEdgeAdminSessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge 
     *  notification service. 
     *
     *  @param  edgeReceiver the notification callback 
     *  @return an <code> EdgeNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> edgeReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeNotificationSession getEdgeNotificationSession(org.osid.topology.EdgeReceiver edgeReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyManager.getEdgeNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge 
     *  notification service. 
     *
     *  @param  edgeReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> EdgeNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> edgeReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeNotificationSession getEdgeNotificationSession(org.osid.topology.EdgeReceiver edgeReceiver, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyProxyManager.getEdgeNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge 
     *  notification service for the given graph. 
     *
     *  @param  edgeReceiver the notification callback 
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @return an <code> EdgeNotificationSession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> edgeReceiver </code> or 
     *          <code> graphId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeNotificationSession getEdgeNotificationSessionForGraph(org.osid.topology.EdgeReceiver edgeReceiver, 
                                                                                        org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyManager.getEdgeNotificationSessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge 
     *  notification service for the given graph. 
     *
     *  @param  edgeReceiver the notification callback 
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @param  proxy a proxy 
     *  @return an <code> EdgeNotificationSession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> edgeReceiver, graphId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeNotificationSession getEdgeNotificationSessionForGraph(org.osid.topology.EdgeReceiver edgeReceiver, 
                                                                                        org.osid.id.Id graphId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyProxyManager.getEdgeNotificationSessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup edge/graph mappings. 
     *
     *  @return an <code> EdgeGraphSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEdgeGraph() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeGraphSession getEdgeGraphSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyManager.getEdgeGraphSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup edge/graph mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EdgeGraphSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEdgeGraph() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeGraphSession getEdgeGraphSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyProxyManager.getEdgeGraphSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning edges to 
     *  graphs. 
     *
     *  @return an <code> EdgeGraphAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeGraphAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeGraphAssignmentSession getEdgeGraphAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyManager.getEdgeGraphAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning edges to 
     *  graphs. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EdgeGraphAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeGraphAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeGraphAssignmentSession getEdgeGraphAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyProxyManager.getEdgeGraphAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage dynamic graphs of 
     *  retlationships. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @return an <code> EdgeSmartGraphSession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeSmartGraph() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeSmartGraphSession getEdgeSmartGraphSession(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyManager.getEdgeSmartGraphSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage dynamic graphs of 
     *  retlationships. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @param  proxy a proxy 
     *  @return an <code> EdgeSmartGraphSession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeSmartGraph() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeSmartGraphSession getEdgeSmartGraphSession(org.osid.id.Id graphId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyProxyManager.getEdgeSmartGraphSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the graph lookup 
     *  service. 
     *
     *  @return a <code> GraphLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsGraphLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.GraphLookupSession getGraphLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyManager.getGraphLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the graph lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GraphLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsGraphLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.GraphLookupSession getGraphLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyProxyManager.getGraphLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the graph query 
     *  service. 
     *
     *  @return a <code> GraphQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsGraphQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.GraphQuerySession getGraphQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyManager.getGraphQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the graph query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GraphQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsGraphQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.GraphQuerySession getGraphQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyProxyManager.getGraphQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the graph search 
     *  service. 
     *
     *  @return a <code> GraphSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsGraphSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.GraphSearchSession getGraphSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyManager.getGraphSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the graph search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GraphSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsGraphSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.GraphSearchSession getGraphSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyProxyManager.getGraphSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the graph 
     *  administrative service. 
     *
     *  @return a <code> GraphAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsGraphAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.GraphAdminSession getGraphAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyManager.getGraphAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the graph 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GraphAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsGraphAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.GraphAdminSession getGraphAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyProxyManager.getGraphAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the graph 
     *  notification service. 
     *
     *  @param  graphReceiver the notification callback 
     *  @return a <code> GraphNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> graphReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGraphNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.GraphNotificationSession getGraphNotificationSession(org.osid.topology.GraphReceiver graphReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyManager.getGraphNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the graph 
     *  notification service. 
     *
     *  @param  graphReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> GraphNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> graphReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGraphNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.GraphNotificationSession getGraphNotificationSession(org.osid.topology.GraphReceiver graphReceiver, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyProxyManager.getGraphNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the graph 
     *  hierarchy service. 
     *
     *  @return a <code> GraphHierarchySession </code> for graphs 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGraphHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.GraphHierarchySession getGraphHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyManager.getGraphHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the graph 
     *  hierarchy service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GraphHierarchySession </code> for graphs 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGraphHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.GraphHierarchySession getGraphHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyProxyManager.getGraphHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the graph 
     *  hierarchy design service. 
     *
     *  @return a <code> HierarchyDesignSession </code> for graphs 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGraphHierarchyDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.GraphHierarchyDesignSession getGraphHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyManager.getGraphHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the graph 
     *  hierarchy design service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HierarchyDesignSession </code> for graphs 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGraphHierarchyDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.GraphHierarchyDesignSession getGraphHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyProxyManager.getGraphHierarchyDesignSession not implemented");
    }


    /**
     *  Gets a <code> TopologyBatchManager. </code> 
     *
     *  @return a <code> TopologyBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTopologyBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.batch.TopologyBatchManager getTopologyBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyManager.getTopologyBatchManager not implemented");
    }


    /**
     *  Gets a <code> TopologyBatchManager. </code> 
     *
     *  @return a <code> TopologyBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTopologyBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.batch.TopologyBatchProxyManager getTopologyBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyProxyManager.getTopologyBatchProxyManager not implemented");
    }


    /**
     *  Gets a <code> TopologyPathManager. </code> 
     *
     *  @return a <code> TopologyPathManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTopologyPath() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.TopologyPathManager getTopologyPathManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyManager.getTopologyPathManager not implemented");
    }


    /**
     *  Gets a <code> TopologyPathManager. </code> 
     *
     *  @return a <code> TopologyPathProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTopologyPath() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.TopologyPathProxyManager getTopologyPathProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyProxyManager.getTopologyPathProxyManager not implemented");
    }


    /**
     *  Gets a <code> TopologyRulesManager. </code> 
     *
     *  @return a <code> TopologyRulesManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTopologyRules() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.TopologyRulesManager getTopologyRulesManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyManager.getTopologyRulesManager not implemented");
    }


    /**
     *  Gets a <code> TopologyRulesManager. </code> 
     *
     *  @return a <code> TopologyRulesProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTopologyRules() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.TopologyRulesProxyManager getTopologyRulesProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.TopologyProxyManager.getTopologyRulesProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.nodeRecordTypes.clear();
        this.nodeRecordTypes.clear();

        this.nodeSearchRecordTypes.clear();
        this.nodeSearchRecordTypes.clear();

        this.edgeRecordTypes.clear();
        this.edgeRecordTypes.clear();

        this.edgeSearchRecordTypes.clear();
        this.edgeSearchRecordTypes.clear();

        this.graphRecordTypes.clear();
        this.graphRecordTypes.clear();

        this.graphSearchRecordTypes.clear();
        this.graphSearchRecordTypes.clear();

        return;
    }
}
