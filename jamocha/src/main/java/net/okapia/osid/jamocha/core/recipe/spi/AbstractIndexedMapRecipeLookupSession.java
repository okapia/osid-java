//
// AbstractIndexedMapRecipeLookupSession.java
//
//    A simple framework for providing a Recipe lookup service
//    backed by a fixed collection of recipes with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.recipe.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Recipe lookup service backed by a
 *  fixed collection of recipes. The recipes are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some recipes may be compatible
 *  with more types than are indicated through these recipe
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Recipes</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapRecipeLookupSession
    extends AbstractMapRecipeLookupSession
    implements org.osid.recipe.RecipeLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.recipe.Recipe> recipesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.recipe.Recipe>());
    private final MultiMap<org.osid.type.Type, org.osid.recipe.Recipe> recipesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.recipe.Recipe>());


    /**
     *  Makes a <code>Recipe</code> available in this session.
     *
     *  @param  recipe a recipe
     *  @throws org.osid.NullArgumentException <code>recipe<code> is
     *          <code>null</code>
     */

    @Override
    protected void putRecipe(org.osid.recipe.Recipe recipe) {
        super.putRecipe(recipe);

        this.recipesByGenus.put(recipe.getGenusType(), recipe);
        
        try (org.osid.type.TypeList types = recipe.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.recipesByRecord.put(types.getNextType(), recipe);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a recipe from this session.
     *
     *  @param recipeId the <code>Id</code> of the recipe
     *  @throws org.osid.NullArgumentException <code>recipeId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeRecipe(org.osid.id.Id recipeId) {
        org.osid.recipe.Recipe recipe;
        try {
            recipe = getRecipe(recipeId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.recipesByGenus.remove(recipe.getGenusType());

        try (org.osid.type.TypeList types = recipe.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.recipesByRecord.remove(types.getNextType(), recipe);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeRecipe(recipeId);
        return;
    }


    /**
     *  Gets a <code>RecipeList</code> corresponding to the given
     *  recipe genus <code>Type</code> which does not include
     *  recipes of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known recipes or an error results. Otherwise,
     *  the returned list may contain only those recipes that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  recipeGenusType a recipe genus type 
     *  @return the returned <code>Recipe</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>recipeGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.RecipeList getRecipesByGenusType(org.osid.type.Type recipeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.recipe.recipe.ArrayRecipeList(this.recipesByGenus.get(recipeGenusType)));
    }


    /**
     *  Gets a <code>RecipeList</code> containing the given
     *  recipe record <code>Type</code>. In plenary mode, the
     *  returned list contains all known recipes or an error
     *  results. Otherwise, the returned list may contain only those
     *  recipes that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  recipeRecordType a recipe record type 
     *  @return the returned <code>recipe</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>recipeRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.RecipeList getRecipesByRecordType(org.osid.type.Type recipeRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.recipe.recipe.ArrayRecipeList(this.recipesByRecord.get(recipeRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.recipesByGenus.clear();
        this.recipesByRecord.clear();

        super.close();

        return;
    }
}
