//
// AbstractHoldSearch.java
//
//     A template for making a Hold Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.hold.hold.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing hold searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractHoldSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.hold.HoldSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.hold.records.HoldSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.hold.HoldSearchOrder holdSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of holds. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  holdIds list of holds
     *  @throws org.osid.NullArgumentException
     *          <code>holdIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongHolds(org.osid.id.IdList holdIds) {
        while (holdIds.hasNext()) {
            try {
                this.ids.add(holdIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongHolds</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of hold Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getHoldIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  holdSearchOrder hold search order 
     *  @throws org.osid.NullArgumentException
     *          <code>holdSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>holdSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderHoldResults(org.osid.hold.HoldSearchOrder holdSearchOrder) {
	this.holdSearchOrder = holdSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.hold.HoldSearchOrder getHoldSearchOrder() {
	return (this.holdSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given hold search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a hold implementing the requested record.
     *
     *  @param holdSearchRecordType a hold search record
     *         type
     *  @return the hold search record
     *  @throws org.osid.NullArgumentException
     *          <code>holdSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(holdSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.hold.records.HoldSearchRecord getHoldSearchRecord(org.osid.type.Type holdSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.hold.records.HoldSearchRecord record : this.records) {
            if (record.implementsRecordType(holdSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(holdSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this hold search. 
     *
     *  @param holdSearchRecord hold search record
     *  @param holdSearchRecordType hold search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addHoldSearchRecord(org.osid.hold.records.HoldSearchRecord holdSearchRecord, 
                                           org.osid.type.Type holdSearchRecordType) {

        addRecordType(holdSearchRecordType);
        this.records.add(holdSearchRecord);        
        return;
    }
}
