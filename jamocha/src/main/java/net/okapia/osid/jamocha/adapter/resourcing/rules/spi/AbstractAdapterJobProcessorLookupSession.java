//
// AbstractAdapterJobProcessorLookupSession.java
//
//    A JobProcessor lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.resourcing.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A JobProcessor lookup session adapter.
 */

public abstract class AbstractAdapterJobProcessorLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.resourcing.rules.JobProcessorLookupSession {

    private final org.osid.resourcing.rules.JobProcessorLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterJobProcessorLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterJobProcessorLookupSession(org.osid.resourcing.rules.JobProcessorLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Foundry/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Foundry Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFoundryId() {
        return (this.session.getFoundryId());
    }


    /**
     *  Gets the {@code Foundry} associated with this session.
     *
     *  @return the {@code Foundry} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Foundry getFoundry()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getFoundry());
    }


    /**
     *  Tests if this user can perform {@code JobProcessor} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupJobProcessors() {
        return (this.session.canLookupJobProcessors());
    }


    /**
     *  A complete view of the {@code JobProcessor} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeJobProcessorView() {
        this.session.useComparativeJobProcessorView();
        return;
    }


    /**
     *  A complete view of the {@code JobProcessor} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryJobProcessorView() {
        this.session.usePlenaryJobProcessorView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include job processors in foundries which are children
     *  of this foundry in the foundry hierarchy.
     */

    @OSID @Override
    public void useFederatedFoundryView() {
        this.session.useFederatedFoundryView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this foundry only.
     */

    @OSID @Override
    public void useIsolatedFoundryView() {
        this.session.useIsolatedFoundryView();
        return;
    }
    

    /**
     *  Only active job processors are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveJobProcessorView() {
        this.session.useActiveJobProcessorView();
        return;
    }


    /**
     *  Active and inactive job processors are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusJobProcessorView() {
        this.session.useAnyStatusJobProcessorView();
        return;
    }
    
     
    /**
     *  Gets the {@code JobProcessor} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code JobProcessor} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code JobProcessor} and
     *  retained for compatibility.
     *
     *  In active mode, job processors are returned that are currently
     *  active. In any status mode, active and inactive job processors
     *  are returned.
     *
     *  @param jobProcessorId {@code Id} of the {@code JobProcessor}
     *  @return the job processor
     *  @throws org.osid.NotFoundException {@code jobProcessorId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code jobProcessorId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessor getJobProcessor(org.osid.id.Id jobProcessorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJobProcessor(jobProcessorId));
    }


    /**
     *  Gets a {@code JobProcessorList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  jobProcessors specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code JobProcessors} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, job processors are returned that are currently
     *  active. In any status mode, active and inactive job processors
     *  are returned.
     *
     *  @param  jobProcessorIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code JobProcessor} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code jobProcessorIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorList getJobProcessorsByIds(org.osid.id.IdList jobProcessorIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJobProcessorsByIds(jobProcessorIds));
    }


    /**
     *  Gets a {@code JobProcessorList} corresponding to the given
     *  job processor genus {@code Type} which does not include
     *  job processors of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  job processors or an error results. Otherwise, the returned list
     *  may contain only those job processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, job processors are returned that are currently
     *  active. In any status mode, active and inactive job processors
     *  are returned.
     *
     *  @param  jobProcessorGenusType a jobProcessor genus type 
     *  @return the returned {@code JobProcessor} list
     *  @throws org.osid.NullArgumentException
     *          {@code jobProcessorGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorList getJobProcessorsByGenusType(org.osid.type.Type jobProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJobProcessorsByGenusType(jobProcessorGenusType));
    }


    /**
     *  Gets a {@code JobProcessorList} corresponding to the given
     *  job processor genus {@code Type} and include any additional
     *  job processors with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  job processors or an error results. Otherwise, the returned list
     *  may contain only those job processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, job processors are returned that are currently
     *  active. In any status mode, active and inactive job processors
     *  are returned.
     *
     *  @param  jobProcessorGenusType a jobProcessor genus type 
     *  @return the returned {@code JobProcessor} list
     *  @throws org.osid.NullArgumentException
     *          {@code jobProcessorGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorList getJobProcessorsByParentGenusType(org.osid.type.Type jobProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJobProcessorsByParentGenusType(jobProcessorGenusType));
    }


    /**
     *  Gets a {@code JobProcessorList} containing the given
     *  job processor record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  job processors or an error results. Otherwise, the returned list
     *  may contain only those job processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, job processors are returned that are currently
     *  active. In any status mode, active and inactive job processors
     *  are returned.
     *
     *  @param  jobProcessorRecordType a jobProcessor record type 
     *  @return the returned {@code JobProcessor} list
     *  @throws org.osid.NullArgumentException
     *          {@code jobProcessorRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorList getJobProcessorsByRecordType(org.osid.type.Type jobProcessorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJobProcessorsByRecordType(jobProcessorRecordType));
    }


    /**
     *  Gets all {@code JobProcessors}. 
     *
     *  In plenary mode, the returned list contains all known
     *  job processors or an error results. Otherwise, the returned list
     *  may contain only those job processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, job processors are returned that are currently
     *  active. In any status mode, active and inactive job processors
     *  are returned.
     *
     *  @return a list of {@code JobProcessors} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorList getJobProcessors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJobProcessors());
    }
}
