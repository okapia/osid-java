//
// AbstractMapBrokerLookupSession
//
//    A simple framework for providing a Broker lookup service
//    backed by a fixed collection of brokers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Broker lookup service backed by a
 *  fixed collection of brokers. The brokers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Brokers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapBrokerLookupSession
    extends net.okapia.osid.jamocha.provisioning.spi.AbstractBrokerLookupSession
    implements org.osid.provisioning.BrokerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.provisioning.Broker> brokers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.provisioning.Broker>());


    /**
     *  Makes a <code>Broker</code> available in this session.
     *
     *  @param  broker a broker
     *  @throws org.osid.NullArgumentException <code>broker<code>
     *          is <code>null</code>
     */

    protected void putBroker(org.osid.provisioning.Broker broker) {
        this.brokers.put(broker.getId(), broker);
        return;
    }


    /**
     *  Makes an array of brokers available in this session.
     *
     *  @param  brokers an array of brokers
     *  @throws org.osid.NullArgumentException <code>brokers<code>
     *          is <code>null</code>
     */

    protected void putBrokers(org.osid.provisioning.Broker[] brokers) {
        putBrokers(java.util.Arrays.asList(brokers));
        return;
    }


    /**
     *  Makes a collection of brokers available in this session.
     *
     *  @param  brokers a collection of brokers
     *  @throws org.osid.NullArgumentException <code>brokers<code>
     *          is <code>null</code>
     */

    protected void putBrokers(java.util.Collection<? extends org.osid.provisioning.Broker> brokers) {
        for (org.osid.provisioning.Broker broker : brokers) {
            this.brokers.put(broker.getId(), broker);
        }

        return;
    }


    /**
     *  Removes a Broker from this session.
     *
     *  @param  brokerId the <code>Id</code> of the broker
     *  @throws org.osid.NullArgumentException <code>brokerId<code> is
     *          <code>null</code>
     */

    protected void removeBroker(org.osid.id.Id brokerId) {
        this.brokers.remove(brokerId);
        return;
    }


    /**
     *  Gets the <code>Broker</code> specified by its <code>Id</code>.
     *
     *  @param  brokerId <code>Id</code> of the <code>Broker</code>
     *  @return the broker
     *  @throws org.osid.NotFoundException <code>brokerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>brokerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Broker getBroker(org.osid.id.Id brokerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.provisioning.Broker broker = this.brokers.get(brokerId);
        if (broker == null) {
            throw new org.osid.NotFoundException("broker not found: " + brokerId);
        }

        return (broker);
    }


    /**
     *  Gets all <code>Brokers</code>. In plenary mode, the returned
     *  list contains all known brokers or an error
     *  results. Otherwise, the returned list may contain only those
     *  brokers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Brokers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerList getBrokers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.broker.ArrayBrokerList(this.brokers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.brokers.clear();
        super.close();
        return;
    }
}
