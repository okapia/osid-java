//
// AbstractDeedSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.squatting.deed.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractDeedSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.room.squatting.DeedSearchResults {

    private org.osid.room.squatting.DeedList deeds;
    private final org.osid.room.squatting.DeedQueryInspector inspector;
    private final java.util.Collection<org.osid.room.squatting.records.DeedSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractDeedSearchResults.
     *
     *  @param deeds the result set
     *  @param deedQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>deeds</code>
     *          or <code>deedQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractDeedSearchResults(org.osid.room.squatting.DeedList deeds,
                                            org.osid.room.squatting.DeedQueryInspector deedQueryInspector) {
        nullarg(deeds, "deeds");
        nullarg(deedQueryInspector, "deed query inspectpr");

        this.deeds = deeds;
        this.inspector = deedQueryInspector;

        return;
    }


    /**
     *  Gets the deed list resulting from a search.
     *
     *  @return a deed list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeeds() {
        if (this.deeds == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.room.squatting.DeedList deeds = this.deeds;
        this.deeds = null;
	return (deeds);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.room.squatting.DeedQueryInspector getDeedQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  deed search record <code> Type. </code> This method must
     *  be used to retrieve a deed implementing the requested
     *  record.
     *
     *  @param deedSearchRecordType a deed search 
     *         record type 
     *  @return the deed search
     *  @throws org.osid.NullArgumentException
     *          <code>deedSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(deedSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.room.squatting.records.DeedSearchResultsRecord getDeedSearchResultsRecord(org.osid.type.Type deedSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.room.squatting.records.DeedSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(deedSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(deedSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record deed search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addDeedRecord(org.osid.room.squatting.records.DeedSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "deed record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
