//
// AbstractCheckResult.java
//
//     Defines a CheckResult.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.rules.check.checkresult.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>CheckResult</code>.
 */

public abstract class AbstractCheckResult
    extends net.okapia.osid.jamocha.spi.AbstractOsidResult
    implements org.osid.rules.check.CheckResult {

    private org.osid.rules.check.Instruction instruction;
    private boolean failed;
    private boolean warning;
    private org.osid.locale.DisplayText message;

    private final java.util.Collection<org.osid.rules.check.records.CheckResultRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the instruction <code> Id </code> for this result. 
     *
     *  @return the instruction <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getInstructionId() {
        return (this.instruction.getId());
    }


    /**
     *  Gets the instruction for this result. 
     *
     *  @return the instruction 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.rules.check.Instruction getInstruction()
        throws org.osid.OperationFailedException {
        
        return (this.instruction);
    }


    /**
     *  Sets the instruction.
     *
     *  @param instruction the instruction
     *  @throws org.osid.NullArgumentException
     *          <code>instruction</code> is <code>null</code>
     */

    protected void setInstruction(org.osid.rules.check.Instruction instruction) {
        nullarg(instruction, "instruction");
        this.instruction = instruction;
        return;
    }


    /**
     *  Tests if the corresponding <code> Check </code> has
     *  failed. Failed checks resulting in warnings should return
     *  <code> false. </code>
     *
     *  @return <code> true </code> if this check failed, <code> false
     *          </code> otherwise
     */

    @OSID @Override
    public boolean hasFailed() {
        return (this.failed);
    }

    
    /**
     *  Sets the failed flag.
     *
     *  @param failed <code> true </code> if this check failed, <code> false
     *         </code> otherwise
     */

    protected void setFailed(boolean failed) {
        this.failed = failed;
        return;
    }


    /**
     *  Tests if the informational message should be displayed as a
     *  warning.
     *
     *  @return <code> true </code> if this check is a warning, <code>
     *          false </code> otherwise
     */

    @OSID @Override
    public boolean isWarning() {
        return (this.warning);
    }


    /**
     *  Sets the warning flag.
     *
     *  @param warning <code> true </code> if this check is a warning,
     *         <code> false </code> otherwise
     */

    protected void setWarning(boolean warning) {
        this.warning = warning;
        return;
    }


    /**
     *  Gets a message which may be the cause of the failure, a warning 
     *  message, or the weather. 
     *
     *  @return a message 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getMessage() {
        return (this.message);
    }

    
    /**
     *  Sets message.
     *
     *  @param message the message
     *  @throws org.osid.NullArgumentException
     *          <code>message</code> is <code>null</code>
     */

    protected void setMessage(org.osid.locale.DisplayText message) {
        nullarg(message, "message");
        this.message = message;
        return;
    }


    /**
     *  Tests if this check result supports the given record
     *  <code>Type</code>.
     *
     *  @param  checkResultRecordType a check result record type 
     *  @return <code>true</code> if the checkResultRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>checkResultRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type checkResultRecordType) {
        for (org.osid.rules.check.records.CheckResultRecord record : this.records) {
            if (record.implementsRecordType(checkResultRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>CheckResult</code>
     *  record <code>Type</code>.
     *
     *  @param  checkResultRecordType thecheck result record type 
     *  @return the check result record 
     *  @throws org.osid.NullArgumentException
     *          <code>checkResultRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(checkResultRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.rules.check.records.CheckResultRecord getCheckResultRecord(org.osid.type.Type checkResultRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.rules.check.records.CheckResultRecord record : this.records) {
            if (record.implementsRecordType(checkResultRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(checkResultRecordType + " is not supported");
    }


    /**
     *  Adds a record to this check result.
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param checkResultRecord the check result record
     *  @param checkResultRecordType check result record type
     *  @throws org.osid.NullArgumentException
     *          <code>checkResultRecord</code> or
     *          <code>checkResultRecordType</code> is
     *          <code>null</code>
     */
            
    protected void addCheckResultRecord(org.osid.rules.check.records.CheckResultRecord checkResultRecord, 
                                        org.osid.type.Type checkResultRecordType) {

        nullarg(checkResultRecord, "check result record");
        addRecordType(checkResultRecordType);
        this.records.add(checkResultRecord);
        
        return;
    }
}
