//
// AbstractProgram.java
//
//     Defines a Program builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.program.program.spi;


/**
 *  Defines a <code>Program</code> builder.
 */

public abstract class AbstractProgramBuilder<T extends AbstractProgramBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOperableOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.course.program.program.ProgramMiter program;


    /**
     *  Constructs a new <code>AbstractProgramBuilder</code>.
     *
     *  @param program the program to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractProgramBuilder(net.okapia.osid.jamocha.builder.course.program.program.ProgramMiter program) {
        super(program);
        this.program = program;
        return;
    }


    /**
     *  Builds the program.
     *
     *  @return the new program
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.course.program.Program build() {
        (new net.okapia.osid.jamocha.builder.validator.course.program.program.ProgramValidator(getValidations())).validate(this.program);
        return (new net.okapia.osid.jamocha.builder.course.program.program.ImmutableProgram(this.program));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the program miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.course.program.program.ProgramMiter getMiter() {
        return (this.program);
    }


    /**
     *  Sets the title.
     *
     *  @param title a title
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>title</code> is
     *          <code>null</code>
     */

    public T title(org.osid.locale.DisplayText title) {
        getMiter().setTitle(title);
        return (self());
    }


    /**
     *  Sets the number.
     *
     *  @param number a number
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>number</code> is
     *          <code>null</code>
     */

    public T number(String number) {
        getMiter().setNumber(number);
        return (self());
    }


    /**
     *  Adds a sponsor.
     *
     *  @param sponsor a sponsor
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>sponsor</code> is
     *          <code>null</code>
     */

    public T sponsor(org.osid.resource.Resource sponsor) {
        getMiter().addSponsor(sponsor);
        return (self());
    }


    /**
     *  Sets all the sponsors.
     *
     *  @param sponsors a collection of sponsors
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>sponsors</code>
     *          is <code>null</code>
     */

    public T sponsors(java.util.Collection<org.osid.resource.Resource> sponsors) {
        getMiter().setSponsors(sponsors);
        return (self());
    }


    /**
     *  Sets the completion requirements info.
     *
     *  @param info a completion requirements info
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>info</code> is
     *          <code>null</code>
     */

    public T completionRequirementsInfo(org.osid.locale.DisplayText info) {
        getMiter().setCompletionRequirementsInfo(info);
        return (self());
    }


    /**
     *  Adds a completion requirement.
     *
     *  @param requirement a completion requirement
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>requirement</code> is <code>null</code>
     */

    public T completionRequirement(org.osid.course.requisite.Requisite requirement) {
        getMiter().addCompletionRequirement(requirement);
        return (self());
    }


    /**
     *  Sets all the completion requirements.
     *
     *  @param requirements a collection of completion requirements
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>requirements</code> is <code>null</code>
     */

    public T completionRequirements(java.util.Collection<org.osid.course.requisite.Requisite> requirements) {
        getMiter().setCompletionRequirements(requirements);
        return (self());
    }


    /**
     *  Adds a credential.
     *
     *  @param credential a credential
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>credential</code>
     *          is <code>null</code>
     */

    public T credential(org.osid.course.program.Credential credential) {
        getMiter().addCredential(credential);
        return (self());
    }


    /**
     *  Sets all the credentials.
     *
     *  @param credentials a collection of credentials
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>credentials</code> is <code>null</code>
     */

    public T credentials(java.util.Collection<org.osid.course.program.Credential> credentials) {
        getMiter().setCredentials(credentials);
        return (self());
    }


    /**
     *  Adds a Program record.
     *
     *  @param record a program record
     *  @param recordType the type of program record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.course.program.records.ProgramRecord record, org.osid.type.Type recordType) {
        getMiter().addProgramRecord(record, recordType);
        return (self());
    }
}       


