//
// AbstractInquiry.java
//
//     Defines an Inquiry.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inquiry.inquiry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Inquiry</code>.
 */

public abstract class AbstractInquiry
    extends net.okapia.osid.jamocha.spi.AbstractOsidRule
    implements org.osid.inquiry.Inquiry {

    private org.osid.inquiry.Audit audit;
    private org.osid.locale.DisplayText question;

    private boolean required;
    private boolean affirmationRequired;
    private boolean needsOneResponse;

    private final java.util.Collection<org.osid.inquiry.records.InquiryRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the audit. 
     *
     *  @return the audit <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAuditId() {
        return (this.audit.getId());
    }


    /**
     *  Gets the audit. 
     *
     *  @return the audit 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.inquiry.Audit getAudit()
        throws org.osid.OperationFailedException {

        return (this.audit);
    }


    /**
     *  Sets the audit.
     *
     *  @param audit an audit
     *  @throws org.osid.NullArgumentException <code>audit</code> is
     *          <code>null</code>
     */

    protected void setAudit(org.osid.inquiry.Audit audit) {
        nullarg(audit, "audit");
        this.audit = audit;
        return;
    }


    /**
     *  Gets the question. 
     *
     *  @return the question 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getQuestion() {
        return (this.question);
    }


    /**
     *  Sets the question.
     *
     *  @param question a question
     *  @throws org.osid.NullArgumentException <code>question</code>
     *          is <code>null</code>
     */

    protected void setQuestion(org.osid.locale.DisplayText question) {
        nullarg(question, "question");
        this.question = question;
        return;
    }


    /**
     *  Tests if a <code> Response </code> is required for this <code> 
     *  Inquiry. </code> 
     *
     *  @return <code> true </code> if a response is required, false if 
     *          optional 
     */

    @OSID @Override
    public boolean isRequired() {
        return (this.required);
    }


    /**
     *  Sets the required.
     *
     *  @param required {@code true} if required, {@code false} if not
     *         required
     */

    protected void setRequired(boolean required) {
        this.required = required;
        return;
    }


    /**
     *  Tests if a positive <code> Response </code> is required for
     *  this <code> Inquiry </code> to pass the <code> Audit. </code>
     *
     *  @return <code> true </code> if a positive response is
     *          required, false if optional
     */

    @OSID @Override
    public boolean isAffirmationRequired() {
        return (this.affirmationRequired);
    }


    /**
     *  Sets the affirmation required.
     *
     *  @param required {@code true} if required, {@code false} if not
     *         required
     */

    protected void setAffirmationRequired(boolean required) {
        this.affirmationRequired = required;
        return;
    }


    /**
     *  Tests if a single effective <code> Response </code> is required. 
     *
     *  @return <code> true </code> if a single effective response is 
     *          required, false responses should be collected for every 
     *          inquiry 
     */

    @OSID @Override
    public boolean needsOneResponse() {
        return (this.needsOneResponse);
    }


    /**
     *  Sets the needs one response.
     *
     *  @param one {@code true} if one response is needed, {@code
     *         false} if a response is always needed
     */

    protected void setNeedsOneResponse(boolean one) {
        this.needsOneResponse = one;
        return;
    }


    /**
     *  Tests if this inquiry supports the given record
     *  <code>Type</code>.
     *
     *  @param  inquiryRecordType an inquiry record type 
     *  @return <code>true</code> if the inquiryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>inquiryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type inquiryRecordType) {
        for (org.osid.inquiry.records.InquiryRecord record : this.records) {
            if (record.implementsRecordType(inquiryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  inquiryRecordType the inquiry record type 
     *  @return the inquiry record 
     *  @throws org.osid.NullArgumentException
     *          <code>inquiryRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(inquiryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inquiry.records.InquiryRecord getInquiryRecord(org.osid.type.Type inquiryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inquiry.records.InquiryRecord record : this.records) {
            if (record.implementsRecordType(inquiryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(inquiryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this inquiry. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param inquiryRecord the inquiry record
     *  @param inquiryRecordType inquiry record type
     *  @throws org.osid.NullArgumentException
     *          <code>inquiryRecord</code> or
     *          <code>inquiryRecordTypeinquiry</code> is
     *          <code>null</code>
     */
            
    protected void addInquiryRecord(org.osid.inquiry.records.InquiryRecord inquiryRecord, 
                                     org.osid.type.Type inquiryRecordType) {

        addRecordType(inquiryRecordType);
        this.records.add(inquiryRecord);
        
        return;
    }
}
