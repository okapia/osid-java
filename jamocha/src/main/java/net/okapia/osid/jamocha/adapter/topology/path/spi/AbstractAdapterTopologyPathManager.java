//
// AbstractTopologyPathManager.java
//
//     An adapter for a TopologyPathManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.topology.path.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a TopologyPathManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterTopologyPathManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.topology.path.TopologyPathManager>
    implements org.osid.topology.path.TopologyPathManager {


    /**
     *  Constructs a new {@code AbstractAdapterTopologyPathManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterTopologyPathManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterTopologyPathManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterTopologyPathManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any graph federation is exposed. Federation is exposed when a 
     *  specific graph may be identified, selected and used to create a lookup 
     *  or admin session. Federation is not exposed when a set of graphs 
     *  appears as a single graph. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up paths is supported. 
     *
     *  @return <code> true </code> if path lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathLookup() {
        return (getAdapteeManager().supportsPathLookup());
    }


    /**
     *  Tests if querying paths is supported. 
     *
     *  @return <code> true </code> if path query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathQuery() {
        return (getAdapteeManager().supportsPathQuery());
    }


    /**
     *  Tests if searching paths is supported. 
     *
     *  @return <code> true </code> if path search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathSearch() {
        return (getAdapteeManager().supportsPathSearch());
    }


    /**
     *  Tests if path <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if path administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathAdmin() {
        return (getAdapteeManager().supportsPathAdmin());
    }


    /**
     *  Tests if a path <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if path notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathNotification() {
        return (getAdapteeManager().supportsPathNotification());
    }


    /**
     *  Tests if a path graph lookup service is supported. 
     *
     *  @return <code> true </code> if a path graph lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathGraph() {
        return (getAdapteeManager().supportsPathGraph());
    }


    /**
     *  Tests if a path graph service is supported. 
     *
     *  @return <code> true </code> if path to graph assignment service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathGraphAssignment() {
        return (getAdapteeManager().supportsPathGraphAssignment());
    }


    /**
     *  Tests if a path smart graph lookup service is supported. 
     *
     *  @return <code> true </code> if a path smart graph service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathSmartGraph() {
        return (getAdapteeManager().supportsPathSmartGraph());
    }


    /**
     *  Gets the supported <code> Path </code> record types. 
     *
     *  @return a list containing the supported <code> Path </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPathRecordTypes() {
        return (getAdapteeManager().getPathRecordTypes());
    }


    /**
     *  Tests if the given <code> Path </code> record type is supported. 
     *
     *  @param  pathRecordType a <code> Type </code> indicating a <code> Path 
     *          </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> pathRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPathRecordType(org.osid.type.Type pathRecordType) {
        return (getAdapteeManager().supportsPathRecordType(pathRecordType));
    }


    /**
     *  Gets the supported <code> Path </code> search types. 
     *
     *  @return a list containing the supported <code> Path </code> search 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPathSearchRecordTypes() {
        return (getAdapteeManager().getPathSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Path </code> search type is supported. 
     *
     *  @param  pathSearchRecordType a <code> Type </code> indicating a <code> 
     *          Path </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> pathSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPathSearchRecordType(org.osid.type.Type pathSearchRecordType) {
        return (getAdapteeManager().supportsPathSearchRecordType(pathSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path lookup 
     *  service. 
     *
     *  @return a <code> PathLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.PathLookupSession getPathLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPathLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path lookup 
     *  service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @return a <code> PathLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.PathLookupSession getPathLookupSessionForGraph(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPathLookupSessionForGraph(graphId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path query 
     *  service. 
     *
     *  @return a <code> PathQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.PathQuerySession getPathQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPathQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path query 
     *  service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @return a <code> PathQuerySession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.PathQuerySession getPathQuerySessionForGraph(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPathQuerySessionForGraph(graphId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path search 
     *  service. 
     *
     *  @return a <code> PathSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.PathSearchSession getPathSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPathSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path search 
     *  service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @return a <code> PathSearchSession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.PathSearchSession getPathSearchSessionForGraph(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPathSearchSessionForGraph(graphId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path 
     *  administration service. 
     *
     *  @return a <code> PathAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.PathAdminSession getPathAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPathAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path 
     *  administration service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> graph </code> 
     *  @return a <code> PathAdminSession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.PathAdminSession getPathAdminSessionForGraph(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPathAdminSessionForGraph(graphId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path 
     *  notification service. 
     *
     *  @param  pathReceiver the notification callback 
     *  @return a <code> PathNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> pathReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPathNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.PathNotificationSession getPathNotificationSession(org.osid.topology.path.PathReceiver pathReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPathNotificationSession(pathReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path 
     *  notification service for the given graph. 
     *
     *  @param  pathReceiver the notification callback 
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @return a <code> PathNotificationSession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> pathReceiver </code> or 
     *          <code> graphId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPathNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.PathNotificationSession getPathNotificationSessionForGraph(org.osid.topology.path.PathReceiver pathReceiver, 
                                                                                             org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPathNotificationSessionForGraph(pathReceiver, graphId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup path/graph locations. 
     *
     *  @return a <code> PathGraphSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathGraph() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.PathGraphSession getPathGraphSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPathGraphSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning paths to 
     *  graphs. 
     *
     *  @return a <code> PathGraphAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPathGraphAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.PathGraphAssignmentSession getPathGraphAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPathGraphAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage path smart graphs. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @return a <code> PathSmartGraphSession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> pathReceiver </code> or 
     *          <code> graphId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPathSmartGraph() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.PathSmartGraphSession getPathSmartGraphSession(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPathSmartGraphSession(graphId));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
