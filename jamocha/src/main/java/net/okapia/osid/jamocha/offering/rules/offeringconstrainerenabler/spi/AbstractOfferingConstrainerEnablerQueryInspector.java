//
// AbstractOfferingConstrainerEnablerQueryInspector.java
//
//     A template for making an OfferingConstrainerEnablerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.rules.offeringconstrainerenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for offering constrainer enablers.
 */

public abstract class AbstractOfferingConstrainerEnablerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQueryInspector
    implements org.osid.offering.rules.OfferingConstrainerEnablerQueryInspector {

    private final java.util.Collection<org.osid.offering.rules.records.OfferingConstrainerEnablerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the offering constrainer <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledOfferingConstrainerIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the offering constrainer query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerQueryInspector[] getRuledOfferingConstrainerTerms() {
        return (new org.osid.offering.rules.OfferingConstrainerQueryInspector[0]);
    }


    /**
     *  Gets the catalogue <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCatalogueIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the catalogue query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.offering.CatalogueQueryInspector[] getCatalogueTerms() {
        return (new org.osid.offering.CatalogueQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given offering constrainer enabler query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an offering constrainer enabler implementing the requested record.
     *
     *  @param offeringConstrainerEnablerRecordType an offering constrainer enabler record type
     *  @return the offering constrainer enabler query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>offeringConstrainerEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(offeringConstrainerEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.rules.records.OfferingConstrainerEnablerQueryInspectorRecord getOfferingConstrainerEnablerQueryInspectorRecord(org.osid.type.Type offeringConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.rules.records.OfferingConstrainerEnablerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(offeringConstrainerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(offeringConstrainerEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this offering constrainer enabler query. 
     *
     *  @param offeringConstrainerEnablerQueryInspectorRecord offering constrainer enabler query inspector
     *         record
     *  @param offeringConstrainerEnablerRecordType offeringConstrainerEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addOfferingConstrainerEnablerQueryInspectorRecord(org.osid.offering.rules.records.OfferingConstrainerEnablerQueryInspectorRecord offeringConstrainerEnablerQueryInspectorRecord, 
                                                   org.osid.type.Type offeringConstrainerEnablerRecordType) {

        addRecordType(offeringConstrainerEnablerRecordType);
        nullarg(offeringConstrainerEnablerRecordType, "offering constrainer enabler record type");
        this.records.add(offeringConstrainerEnablerQueryInspectorRecord);        
        return;
    }
}
