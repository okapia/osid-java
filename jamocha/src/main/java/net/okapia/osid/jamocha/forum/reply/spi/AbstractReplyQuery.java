//
// AbstractReplyQuery.java
//
//     A template for making a Reply Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.forum.reply.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for replies.
 */

public abstract class AbstractReplyQuery    
    extends net.okapia.osid.jamocha.spi.AbstractContainableOsidObjectQuery
    implements org.osid.forum.ReplyQuery {

    private final java.util.Collection<org.osid.forum.records.ReplyQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the post <code> Id </code> for this query to match replies to 
     *  posts. 
     *
     *  @param  postId a post <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> postId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchPostId(org.osid.id.Id postId, boolean match) {
        return;
    }


    /**
     *  Clears the post <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPostIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PostQuery </code> is available. 
     *
     *  @return <code> true </code> if a post query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostQuery() {
        return (false);
    }


    /**
     *  Gets the query for a post query. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the post query 
     *  @throws org.osid.UnimplementedException <code> supportsPostQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostQuery getPostQuery() {
        throw new org.osid.UnimplementedException("supportsPostQuery() is false");
    }


    /**
     *  Clears the post terms. 
     */

    @OSID @Override
    public void clearPostTerms() {
        return;
    }


    /**
     *  Matches entries whose sent time is between the supplied range 
     *  inclusive. 
     *
     *  @param  startTime start time 
     *  @param  endTime end time 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> startTime </code> is 
     *          greater than <code> endTime </code> 
     *  @throws org.osid.NullArgumentException <code> startTime </code> or 
     *          <code> endTime </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchTimestamp(org.osid.calendaring.DateTime startTime, 
                               org.osid.calendaring.DateTime endTime, 
                               boolean match) {
        return;
    }


    /**
     *  Clears the timestamp terms. 
     */

    @OSID @Override
    public void clearTimestampTerms() {
        return;
    }


    /**
     *  Matches the poster of the entry. 
     *
     *  @param  resourceId resource <code> Id </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPosterId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the poster resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPosterIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available for querying 
     *  posters. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPosterQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsPosterQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getPosterQuery() {
        throw new org.osid.UnimplementedException("supportsPosterQuery() is false");
    }


    /**
     *  Clears the poster terms. 
     */

    @OSID @Override
    public void clearPosterTerms() {
        return;
    }


    /**
     *  Matches the posting agent of the entry. 
     *
     *  @param  agentId agent <code> Id </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPostingAgentId(org.osid.id.Id agentId, boolean match) {
        return;
    }


    /**
     *  Clears the posting agent <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPostingAgentIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available for querying 
     *  posting agents. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostingAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agent. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostingAgentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getPostingAgentQuery() {
        throw new org.osid.UnimplementedException("supportsPostingAgentQuery() is false");
    }


    /**
     *  Clears the posting agent terms. 
     */

    @OSID @Override
    public void clearPostingAgentTerms() {
        return;
    }


    /**
     *  Adds a subject line to match. Multiple subject line matches can be 
     *  added to perform a boolean <code> OR </code> among them. 
     *
     *  @param  subject display name to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> subject is </code> 
     *          not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> subject </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchSubjectLine(String subject, 
                                 org.osid.type.Type stringMatchType, 
                                 boolean match) {
        return;
    }


    /**
     *  Matches entries with any subject line. 
     *
     *  @param  match <code> true </code> to match entries with any subject 
     *          line, <code> false </code> to match entries with no subject 
     *          line 
     */

    @OSID @Override
    public void matchAnySubjectLine(boolean match) {
        return;
    }


    /**
     *  Clears the subject line terms. 
     */

    @OSID @Override
    public void clearSubjectLineTerms() {
        return;
    }


    /**
     *  Adds text to match. Multiple text matches can be added to perform a 
     *  boolean <code> OR </code> among them. 
     *
     *  @param  text text to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> text is </code> not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> text </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchText(String text, org.osid.type.Type stringMatchType, 
                          boolean match) {
        return;
    }


    /**
     *  Matches entries with any text. 
     *
     *  @param  match <code> true </code> to match entries with any text, 
     *          <code> false </code> to match entries with no text 
     */

    @OSID @Override
    public void matchAnyText(boolean match) {
        return;
    }


    /**
     *  Clears the text terms. 
     */

    @OSID @Override
    public void clearTextTerms() {
        return;
    }


    /**
     *  Sets the reply <code> Id </code> for this query to match replies that 
     *  have the specified reply as an ancestor. 
     *
     *  @param  replyId a reply <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> replyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchContainingReplyId(org.osid.id.Id replyId, boolean match) {
        return;
    }


    /**
     *  Clears the containing reply <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearContainingReplyIdTerms() {
        return;
    }


    /**
     *  Tests if a containing reply query is available. 
     *
     *  @return <code> true </code> if a containing reply query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContainingReplyQuery() {
        return (false);
    }


    /**
     *  Gets the query for a containing reply. 
     *
     *  @return the containing reply query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContainingReplyQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ReplyQuery getContainingReplyQuery() {
        throw new org.osid.UnimplementedException("supportsContainingReplyQuery() is false");
    }


    /**
     *  Matches replies with any ancestor reply. 
     *
     *  @param  match <code> true </code> to match replies with any ancestor 
     *          reply, <code> false </code> to match replies with no ancestor 
     *          replies 
     */

    @OSID @Override
    public void matchAnyContainingReply(boolean match) {
        return;
    }


    /**
     *  Clears the containing reply terms. 
     */

    @OSID @Override
    public void clearContainingReplyTerms() {
        return;
    }


    /**
     *  Sets the reply <code> Id </code> for this query to match replies that 
     *  have the specified reply as a descendant. 
     *
     *  @param  replyId a reply <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> replyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchContainedReplyId(org.osid.id.Id replyId, boolean match) {
        return;
    }


    /**
     *  Clears the contained reply <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearContainedReplyIdTerms() {
        return;
    }


    /**
     *  Tests if a contained reply query is available. 
     *
     *  @return <code> true </code> if a contained reply query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContainedReplyQuery() {
        return (false);
    }


    /**
     *  Gets the query for a contained reply. 
     *
     *  @return the contained reply query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContainedReplyQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ReplyQuery getContainedReplyQuery() {
        throw new org.osid.UnimplementedException("supportsContainedReplyQuery() is false");
    }


    /**
     *  Matches replies with any descednant reply. 
     *
     *  @param  match <code> true </code> to match replies with any descendant 
     *          reply, <code> false </code> to match replies with no 
     *          descendant replies 
     */

    @OSID @Override
    public void matchAnyContainedReply(boolean match) {
        return;
    }


    /**
     *  Clears the contained reply terms. 
     */

    @OSID @Override
    public void clearContainedReplyTerms() {
        return;
    }


    /**
     *  Sets the post <code> Id </code> for this query to match replies 
     *  assigned to forums. 
     *
     *  @param  forumId a forum <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> forumId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchForumId(org.osid.id.Id forumId, boolean match) {
        return;
    }


    /**
     *  Clears the forum <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearForumIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ForumQuery </code> is available. 
     *
     *  @return <code> true </code> if a forum query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsForumQuery() {
        return (false);
    }


    /**
     *  Gets the query for a forum query. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the forum query 
     *  @throws org.osid.UnimplementedException <code> supportsForumQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ForumQuery getForumQuery() {
        throw new org.osid.UnimplementedException("supportsForumQuery() is false");
    }


    /**
     *  Clears the forum terms. 
     */

    @OSID @Override
    public void clearForumTerms() {
        return;
    }


    /**
     *  Gets the record corresponding to the given reply query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a reply implementing the requested record.
     *
     *  @param replyRecordType a reply record type
     *  @return the reply query record
     *  @throws org.osid.NullArgumentException
     *          <code>replyRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(replyRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.forum.records.ReplyQueryRecord getReplyQueryRecord(org.osid.type.Type replyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.forum.records.ReplyQueryRecord record : this.records) {
            if (record.implementsRecordType(replyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(replyRecordType + " is not supported");
    }


    /**
     *  Adds a record to this reply query. 
     *
     *  @param replyQueryRecord reply query record
     *  @param replyRecordType reply record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addReplyQueryRecord(org.osid.forum.records.ReplyQueryRecord replyQueryRecord, 
                                       org.osid.type.Type replyRecordType) {

        addRecordType(replyRecordType);
        nullarg(replyQueryRecord, "reply query record");
        this.records.add(replyQueryRecord);        
        return;
    }
}
