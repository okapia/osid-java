//
// AbstractCourseManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractCourseManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.course.CourseManager,
               org.osid.course.CourseProxyManager {

    private final Types courseRecordTypes                  = new TypeRefSet();
    private final Types courseSearchRecordTypes            = new TypeRefSet();

    private final Types activityUnitRecordTypes            = new TypeRefSet();
    private final Types activityUnitSearchRecordTypes      = new TypeRefSet();

    private final Types courseOfferingRecordTypes          = new TypeRefSet();
    private final Types courseOfferingSearchRecordTypes    = new TypeRefSet();

    private final Types activityRecordTypes                = new TypeRefSet();
    private final Types activitySearchRecordTypes          = new TypeRefSet();

    private final Types termRecordTypes                    = new TypeRefSet();
    private final Types termSearchRecordTypes              = new TypeRefSet();

    private final Types courseCatalogRecordTypes           = new TypeRefSet();
    private final Types courseCatalogSearchRecordTypes     = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractCourseManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractCourseManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any course catalog federation is exposed. Federation is 
     *  exposed when a specific course catalog may be identified, selected and 
     *  used to create a lookup or admin session. Federation is not exposed 
     *  when a set of catalogs appears as a single catalog. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if looking up courses is supported. 
     *
     *  @return <code> true </code> if course lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseLookup() {
        return (false);
    }


    /**
     *  Tests if querying courses is supported. 
     *
     *  @return <code> true </code> if course query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseQuery() {
        return (false);
    }


    /**
     *  Tests if searching courses is supported. 
     *
     *  @return <code> true </code> if course search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseSearch() {
        return (false);
    }


    /**
     *  Tests if course <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if course administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseAdmin() {
        return (false);
    }


    /**
     *  Tests if a course <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if course notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseNotification() {
        return (false);
    }


    /**
     *  Tests if a course cataloging service is supported. 
     *
     *  @return <code> true </code> if course cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCourseCatalog() {
        return (false);
    }


    /**
     *  Tests if a course cataloging service is supported. A course cataloging 
     *  service maps courses to catalogs. 
     *
     *  @return <code> true </code> if course cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCourseCatalogAssignment() {
        return (false);
    }


    /**
     *  Tests if a course smart course catalog session is available. 
     *
     *  @return <code> true </code> if a course smart course catalog session 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseSmartCourseCatalog() {
        return (false);
    }


    /**
     *  Tests if looking up activity units is supported. 
     *
     *  @return <code> true </code> if activity unit lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityUnitLookup() {
        return (false);
    }


    /**
     *  Tests if querying activity units is supported. 
     *
     *  @return <code> true </code> if activity unit query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityUnitQuery() {
        return (false);
    }


    /**
     *  Tests if searching activity units is supported. 
     *
     *  @return <code> true </code> if activity unit search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityUnitSearch() {
        return (false);
    }


    /**
     *  Tests if an activity unit <code> </code> administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if activity unit administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityUnitAdmin() {
        return (false);
    }


    /**
     *  Tests if an activity unit <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if activity unit notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityUnitNotification() {
        return (false);
    }


    /**
     *  Tests if an activity unit cataloging service is supported. 
     *
     *  @return <code> true </code> if activity unit catalog is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityUnitCourseCatalog() {
        return (false);
    }


    /**
     *  Tests if an activity unit cataloging service is supported. A 
     *  cataloging service maps activity units to catalogs. 
     *
     *  @return <code> true </code> if activity unit cataloging is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityUnitCourseCatalogAssignment() {
        return (false);
    }


    /**
     *  Tests if an activity unit smart course catalog session is available. 
     *
     *  @return <code> true </code> if an activity unit smart course catalog 
     *          session is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityUnitSmartCourseCatalog() {
        return (false);
    }


    /**
     *  Tests if looking up course offerings is supported. 
     *
     *  @return <code> true </code> if course offering lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseOfferingLookup() {
        return (false);
    }


    /**
     *  Tests if querying course offerings is supported. 
     *
     *  @return <code> true </code> if course offering query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseOfferingQuery() {
        return (false);
    }


    /**
     *  Tests if searching course offerings is supported. 
     *
     *  @return <code> true </code> if course offering search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseOfferingSearch() {
        return (false);
    }


    /**
     *  Tests if course <code> </code> offering <code> </code> administrative 
     *  service is supported. 
     *
     *  @return <code> true </code> if course offering administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseOfferingAdmin() {
        return (false);
    }


    /**
     *  Tests if a course offering <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if course offering notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseOfferingNotification() {
        return (false);
    }


    /**
     *  Tests if a course offering cataloging service is supported. 
     *
     *  @return <code> true </code> if course offering catalog is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseOfferingCourseCatalog() {
        return (false);
    }


    /**
     *  Tests if a course offering cataloging service is supported. A 
     *  cataloging service maps course offerings to catalogs. 
     *
     *  @return <code> true </code> if course offering cataloging is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseOfferingCourseCatalogAssignment() {
        return (false);
    }


    /**
     *  Tests if a course offering smart course catalog session is available. 
     *
     *  @return <code> true </code> if a course offering smart course catalog 
     *          session is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseOfferingSmartCourseCatalog() {
        return (false);
    }


    /**
     *  Tests if looking up activities is supported. 
     *
     *  @return <code> true </code> if activity lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityLookup() {
        return (false);
    }


    /**
     *  Tests if querying activities is supported. 
     *
     *  @return <code> true </code> if activity query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityQuery() {
        return (false);
    }


    /**
     *  Tests if searching activities is supported. 
     *
     *  @return <code> true </code> if activity search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivitySearch() {
        return (false);
    }


    /**
     *  Tests if activity administrative service is supported. 
     *
     *  @return <code> true </code> if activity administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityAdmin() {
        return (false);
    }


    /**
     *  Tests if an activity <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if activity notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityNotification() {
        return (false);
    }


    /**
     *  Tests if an activity cataloging service is supported. 
     *
     *  @return <code> true </code> if activity catalog is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityCourseCatalog() {
        return (false);
    }


    /**
     *  Tests if an activity cataloging service is supported. A cataloging 
     *  service maps activities to catalogs. 
     *
     *  @return <code> true </code> if activity cataloging is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityCourseCatalogAssignment() {
        return (false);
    }


    /**
     *  Tests if an activity smart course catalog session is available. 
     *
     *  @return <code> true </code> if an activity smart course catalog 
     *          session is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivitySmartCourseCatalog() {
        return (false);
    }


    /**
     *  Tests if unravelling activities is supported. 
     *
     *  @return <code> true </code> if unravelling activities is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityUnravelling() {
        return (false);
    }


    /**
     *  Tests if looking up terms is supported. 
     *
     *  @return <code> true </code> if term lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTermLookup() {
        return (false);
    }


    /**
     *  Tests if querying terms is supported. 
     *
     *  @return <code> true </code> if term query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTermQuery() {
        return (false);
    }


    /**
     *  Tests if searching terms is supported. 
     *
     *  @return <code> true </code> if term search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTermSearch() {
        return (false);
    }


    /**
     *  Tests if term <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if term administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTermAdmin() {
        return (false);
    }


    /**
     *  Tests if a term <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if term notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTermNotification() {
        return (false);
    }


    /**
     *  Tests if term <code> </code> hierarchy traversal service is supported. 
     *
     *  @return <code> true </code> if term hierarchy is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTermHierarchy() {
        return (false);
    }


    /**
     *  Tests if a term <code> </code> hierarchy design service is supported. 
     *
     *  @return <code> true </code> if term hierarchy design is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTermHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests if a term cataloging service is supported. 
     *
     *  @return <code> true </code> if term catalog is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTermCourseCatalog() {
        return (false);
    }


    /**
     *  Tests if a term cataloging service is supported. A cataloging service 
     *  maps terms to catalogs. 
     *
     *  @return <code> true </code> if term cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTermCourseCatalogAssignment() {
        return (false);
    }


    /**
     *  Tests if a term smart course catalog session is available. 
     *
     *  @return <code> true </code> if a term smart course catalog session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTermSmartCourseCatalog() {
        return (false);
    }


    /**
     *  Tests if looking up course catalogs is supported. 
     *
     *  @return <code> true </code> if course catalog lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogLookup() {
        return (false);
    }


    /**
     *  Tests if searching course catalogs is supported. 
     *
     *  @return <code> true </code> if course catalog search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogSearch() {
        return (false);
    }


    /**
     *  Tests if querying course catalogs is supported. 
     *
     *  @return <code> true </code> if course catalog query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Tests if course catalog administrative service is supported. 
     *
     *  @return <code> true </code> if course catalog administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogAdmin() {
        return (false);
    }


    /**
     *  Tests if a course catalog <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if course catalog notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogNotification() {
        return (false);
    }


    /**
     *  Tests for the availability of a course catalog hierarchy traversal 
     *  service. 
     *
     *  @return <code> true </code> if course catalog hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogHierarchy() {
        return (false);
    }


    /**
     *  Tests for the availability of a course catalog hierarchy design 
     *  service. 
     *
     *  @return <code> true </code> if course catalog hierarchy design is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests for the availability of a course batch service. 
     *
     *  @return <code> true </code> if a course batch service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseBatch() {
        return (false);
    }


    /**
     *  Tests for the availability of a course program service. 
     *
     *  @return <code> true </code> if a course program service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseProgram() {
        return (false);
    }


    /**
     *  Tests for the availability of a course registration service. 
     *
     *  @return <code> true </code> if a course registration service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseRegistration() {
        return (false);
    }


    /**
     *  Tests for the availability of a course requisite service. 
     *
     *  @return <code> true </code> if a course requisite service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseRequisite() {
        return (false);
    }


    /**
     *  Tests for the availability of a course syllabus service. 
     *
     *  @return <code> true </code> if a course syllabus service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseSyllabus() {
        return (false);
    }


    /**
     *  Tests for the availability of a course plan service. 
     *
     *  @return <code> true </code> if a course plan service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCoursePlan() {
        return (false);
    }


    /**
     *  Tests for the availability of a course chronicle service. 
     *
     *  @return <code> true </code> if a course chronicle service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseChronicle() {
        return (false);
    }


    /**
     *  Gets the supported <code> Course </code> record types. 
     *
     *  @return a list containing the supported <code> Course </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCourseRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.courseRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Course </code> record type is supported. 
     *
     *  @param  courseRecordType a <code> Type </code> indicating a <code> 
     *          Course </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> courseRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCourseRecordType(org.osid.type.Type courseRecordType) {
        return (this.courseRecordTypes.contains(courseRecordType));
    }


    /**
     *  Adds support for a course record type.
     *
     *  @param courseRecordType a course record type
     *  @throws org.osid.NullArgumentException
     *  <code>courseRecordType</code> is <code>null</code>
     */

    protected void addCourseRecordType(org.osid.type.Type courseRecordType) {
        this.courseRecordTypes.add(courseRecordType);
        return;
    }


    /**
     *  Removes support for a course record type.
     *
     *  @param courseRecordType a course record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>courseRecordType</code> is <code>null</code>
     */

    protected void removeCourseRecordType(org.osid.type.Type courseRecordType) {
        this.courseRecordTypes.remove(courseRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Course </code> search record types. 
     *
     *  @return a list containing the supported <code> Course </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCourseSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.courseSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Course </code> search record type is 
     *  supported. 
     *
     *  @param  courseSearchRecordType a <code> Type </code> indicating a 
     *          <code> Course </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> courseSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCourseSearchRecordType(org.osid.type.Type courseSearchRecordType) {
        return (this.courseSearchRecordTypes.contains(courseSearchRecordType));
    }


    /**
     *  Adds support for a course search record type.
     *
     *  @param courseSearchRecordType a course search record type
     *  @throws org.osid.NullArgumentException
     *  <code>courseSearchRecordType</code> is <code>null</code>
     */

    protected void addCourseSearchRecordType(org.osid.type.Type courseSearchRecordType) {
        this.courseSearchRecordTypes.add(courseSearchRecordType);
        return;
    }


    /**
     *  Removes support for a course search record type.
     *
     *  @param courseSearchRecordType a course search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>courseSearchRecordType</code> is <code>null</code>
     */

    protected void removeCourseSearchRecordType(org.osid.type.Type courseSearchRecordType) {
        this.courseSearchRecordTypes.remove(courseSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> ActivityUnit </code> record types. 
     *
     *  @return a list containing the supported <code> ActivityUnit </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getActivityUnitRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.activityUnitRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> ActivityUnit </code> record type is 
     *  supported. 
     *
     *  @param  activityUnitRecordType a <code> Type </code> indicating an 
     *          <code> ActivityUnit </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> activityUnitRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsActivityUnitRecordType(org.osid.type.Type activityUnitRecordType) {
        return (this.activityUnitRecordTypes.contains(activityUnitRecordType));
    }


    /**
     *  Adds support for an activity unit record type.
     *
     *  @param activityUnitRecordType an activity unit record type
     *  @throws org.osid.NullArgumentException
     *  <code>activityUnitRecordType</code> is <code>null</code>
     */

    protected void addActivityUnitRecordType(org.osid.type.Type activityUnitRecordType) {
        this.activityUnitRecordTypes.add(activityUnitRecordType);
        return;
    }


    /**
     *  Removes support for an activity unit record type.
     *
     *  @param activityUnitRecordType an activity unit record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>activityUnitRecordType</code> is <code>null</code>
     */

    protected void removeActivityUnitRecordType(org.osid.type.Type activityUnitRecordType) {
        this.activityUnitRecordTypes.remove(activityUnitRecordType);
        return;
    }


    /**
     *  Gets the supported <code> ActivityUnit </code> search record types. 
     *
     *  @return a list containing the supported <code> ActivityUnit </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getActivityUnitSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.activityUnitSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> ActivityUnit </code> search record type is 
     *  supported. 
     *
     *  @param  activityUnitSearchRecordType a <code> Type </code> indicating 
     *          an <code> ActivityUnit </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          activityUnitSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsActivityUnitSearchRecordType(org.osid.type.Type activityUnitSearchRecordType) {
        return (this.activityUnitSearchRecordTypes.contains(activityUnitSearchRecordType));
    }


    /**
     *  Adds support for an activity unit search record type.
     *
     *  @param activityUnitSearchRecordType an activity unit search record type
     *  @throws org.osid.NullArgumentException
     *  <code>activityUnitSearchRecordType</code> is <code>null</code>
     */

    protected void addActivityUnitSearchRecordType(org.osid.type.Type activityUnitSearchRecordType) {
        this.activityUnitSearchRecordTypes.add(activityUnitSearchRecordType);
        return;
    }


    /**
     *  Removes support for an activity unit search record type.
     *
     *  @param activityUnitSearchRecordType an activity unit search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>activityUnitSearchRecordType</code> is <code>null</code>
     */

    protected void removeActivityUnitSearchRecordType(org.osid.type.Type activityUnitSearchRecordType) {
        this.activityUnitSearchRecordTypes.remove(activityUnitSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> CourseOffering </code> record types. 
     *
     *  @return a list containing the supported <code> CourseOffering </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCourseOfferingRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.courseOfferingRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> CourseOffering </code> record type is 
     *  supported. 
     *
     *  @param  courseOfferingRecordType a <code> Type </code> indicating an 
     *          <code> CourseOffering </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> courseOfferingRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCourseOfferingRecordType(org.osid.type.Type courseOfferingRecordType) {
        return (this.courseOfferingRecordTypes.contains(courseOfferingRecordType));
    }


    /**
     *  Adds support for a course offering record type.
     *
     *  @param courseOfferingRecordType a course offering record type
     *  @throws org.osid.NullArgumentException
     *  <code>courseOfferingRecordType</code> is <code>null</code>
     */

    protected void addCourseOfferingRecordType(org.osid.type.Type courseOfferingRecordType) {
        this.courseOfferingRecordTypes.add(courseOfferingRecordType);
        return;
    }


    /**
     *  Removes support for a course offering record type.
     *
     *  @param courseOfferingRecordType a course offering record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>courseOfferingRecordType</code> is <code>null</code>
     */

    protected void removeCourseOfferingRecordType(org.osid.type.Type courseOfferingRecordType) {
        this.courseOfferingRecordTypes.remove(courseOfferingRecordType);
        return;
    }


    /**
     *  Gets the supported <code> CourseOffering </code> search record types. 
     *
     *  @return a list containing the supported <code> CourseOffering </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCourseOfferingSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.courseOfferingSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> CourseOffering </code> search record type is 
     *  supported. 
     *
     *  @param  courseOfferingSearchRecordType a <code> Type </code> 
     *          indicating an <code> CourseOffering </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          courseOfferingSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCourseOfferingSearchRecordType(org.osid.type.Type courseOfferingSearchRecordType) {
        return (this.courseOfferingSearchRecordTypes.contains(courseOfferingSearchRecordType));
    }


    /**
     *  Adds support for a course offering search record type.
     *
     *  @param courseOfferingSearchRecordType a course offering search record type
     *  @throws org.osid.NullArgumentException
     *  <code>courseOfferingSearchRecordType</code> is <code>null</code>
     */

    protected void addCourseOfferingSearchRecordType(org.osid.type.Type courseOfferingSearchRecordType) {
        this.courseOfferingSearchRecordTypes.add(courseOfferingSearchRecordType);
        return;
    }


    /**
     *  Removes support for a course offering search record type.
     *
     *  @param courseOfferingSearchRecordType a course offering search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>courseOfferingSearchRecordType</code> is <code>null</code>
     */

    protected void removeCourseOfferingSearchRecordType(org.osid.type.Type courseOfferingSearchRecordType) {
        this.courseOfferingSearchRecordTypes.remove(courseOfferingSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Activity </code> record types. 
     *
     *  @return a list containing the supported <code> Activity </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getActivityRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.activityRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Activity </code> record type is supported. 
     *
     *  @param  activityRecordType a <code> Type </code> indicating an <code> 
     *          Activity </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> activityRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsActivityRecordType(org.osid.type.Type activityRecordType) {
        return (this.activityRecordTypes.contains(activityRecordType));
    }


    /**
     *  Adds support for an activity record type.
     *
     *  @param activityRecordType an activity record type
     *  @throws org.osid.NullArgumentException
     *  <code>activityRecordType</code> is <code>null</code>
     */

    protected void addActivityRecordType(org.osid.type.Type activityRecordType) {
        this.activityRecordTypes.add(activityRecordType);
        return;
    }


    /**
     *  Removes support for an activity record type.
     *
     *  @param activityRecordType an activity record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>activityRecordType</code> is <code>null</code>
     */

    protected void removeActivityRecordType(org.osid.type.Type activityRecordType) {
        this.activityRecordTypes.remove(activityRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Activity </code> search record types. 
     *
     *  @return a list containing the supported <code> Activity </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getActivitySearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.activitySearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Activity </code> search record type is 
     *  supported. 
     *
     *  @param  activitySearchRecordType a <code> Type </code> indicating an 
     *          <code> Activity </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> activitySearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsActivitySearchRecordType(org.osid.type.Type activitySearchRecordType) {
        return (this.activitySearchRecordTypes.contains(activitySearchRecordType));
    }


    /**
     *  Adds support for an activity search record type.
     *
     *  @param activitySearchRecordType an activity search record type
     *  @throws org.osid.NullArgumentException
     *  <code>activitySearchRecordType</code> is <code>null</code>
     */

    protected void addActivitySearchRecordType(org.osid.type.Type activitySearchRecordType) {
        this.activitySearchRecordTypes.add(activitySearchRecordType);
        return;
    }


    /**
     *  Removes support for an activity search record type.
     *
     *  @param activitySearchRecordType an activity search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>activitySearchRecordType</code> is <code>null</code>
     */

    protected void removeActivitySearchRecordType(org.osid.type.Type activitySearchRecordType) {
        this.activitySearchRecordTypes.remove(activitySearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Term </code> record types. 
     *
     *  @return a list containing the supported <code> Term </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getTermRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.termRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Term </code> record type is supported. 
     *
     *  @param  termRecordType a <code> Type </code> indicating a <code> Term 
     *          </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> termRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsTermRecordType(org.osid.type.Type termRecordType) {
        return (this.termRecordTypes.contains(termRecordType));
    }


    /**
     *  Adds support for a term record type.
     *
     *  @param termRecordType a term record type
     *  @throws org.osid.NullArgumentException
     *  <code>termRecordType</code> is <code>null</code>
     */

    protected void addTermRecordType(org.osid.type.Type termRecordType) {
        this.termRecordTypes.add(termRecordType);
        return;
    }


    /**
     *  Removes support for a term record type.
     *
     *  @param termRecordType a term record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>termRecordType</code> is <code>null</code>
     */

    protected void removeTermRecordType(org.osid.type.Type termRecordType) {
        this.termRecordTypes.remove(termRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Term </code> search record types. 
     *
     *  @return a list containing the supported <code> Term </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getTermSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.termSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Term </code> search record type is 
     *  supported. 
     *
     *  @param  termSearchRecordType a <code> Type </code> indicating a <code> 
     *          Term </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> termSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsTermSearchRecordType(org.osid.type.Type termSearchRecordType) {
        return (this.termSearchRecordTypes.contains(termSearchRecordType));
    }


    /**
     *  Adds support for a term search record type.
     *
     *  @param termSearchRecordType a term search record type
     *  @throws org.osid.NullArgumentException
     *  <code>termSearchRecordType</code> is <code>null</code>
     */

    protected void addTermSearchRecordType(org.osid.type.Type termSearchRecordType) {
        this.termSearchRecordTypes.add(termSearchRecordType);
        return;
    }


    /**
     *  Removes support for a term search record type.
     *
     *  @param termSearchRecordType a term search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>termSearchRecordType</code> is <code>null</code>
     */

    protected void removeTermSearchRecordType(org.osid.type.Type termSearchRecordType) {
        this.termSearchRecordTypes.remove(termSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> CourseCatalog </code> record types. 
     *
     *  @return a list containing the supported <code> CourseCatalog </code> 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCourseCatalogRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.courseCatalogRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> CourseCatalog </code> record type is 
     *  supported. 
     *
     *  @param  courseCatalogRecordType a <code> Type </code> indicating an 
     *          <code> CourseCatalog </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> courseCatalogRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCourseCatalogRecordType(org.osid.type.Type courseCatalogRecordType) {
        return (this.courseCatalogRecordTypes.contains(courseCatalogRecordType));
    }


    /**
     *  Adds support for a course catalog record type.
     *
     *  @param courseCatalogRecordType a course catalog record type
     *  @throws org.osid.NullArgumentException
     *  <code>courseCatalogRecordType</code> is <code>null</code>
     */

    protected void addCourseCatalogRecordType(org.osid.type.Type courseCatalogRecordType) {
        this.courseCatalogRecordTypes.add(courseCatalogRecordType);
        return;
    }


    /**
     *  Removes support for a course catalog record type.
     *
     *  @param courseCatalogRecordType a course catalog record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>courseCatalogRecordType</code> is <code>null</code>
     */

    protected void removeCourseCatalogRecordType(org.osid.type.Type courseCatalogRecordType) {
        this.courseCatalogRecordTypes.remove(courseCatalogRecordType);
        return;
    }


    /**
     *  Gets the supported <code> CourseCatalog </code> search record types. 
     *
     *  @return a list containing the supported <code> CourseCatalog </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCourseCatalogSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.courseCatalogSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> CourseCatalog </code> search record type is 
     *  supported. 
     *
     *  @param  courseCatalogSearchRecordType a <code> Type </code> indicating 
     *          an <code> CourseCatalog </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          courseCatalogSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCourseCatalogSearchRecordType(org.osid.type.Type courseCatalogSearchRecordType) {
        return (this.courseCatalogSearchRecordTypes.contains(courseCatalogSearchRecordType));
    }


    /**
     *  Adds support for a course catalog search record type.
     *
     *  @param courseCatalogSearchRecordType a course catalog search record type
     *  @throws org.osid.NullArgumentException
     *  <code>courseCatalogSearchRecordType</code> is <code>null</code>
     */

    protected void addCourseCatalogSearchRecordType(org.osid.type.Type courseCatalogSearchRecordType) {
        this.courseCatalogSearchRecordTypes.add(courseCatalogSearchRecordType);
        return;
    }


    /**
     *  Removes support for a course catalog search record type.
     *
     *  @param courseCatalogSearchRecordType a course catalog search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>courseCatalogSearchRecordType</code> is <code>null</code>
     */

    protected void removeCourseCatalogSearchRecordType(org.osid.type.Type courseCatalogSearchRecordType) {
        this.courseCatalogSearchRecordTypes.remove(courseCatalogSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course lookup 
     *  service. 
     *
     *  @return a <code> CourseLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCourseLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseLookupSession getCourseLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getCourseLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course lookup 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CourseLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCourseLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseLookupSession getCourseLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getCourseLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course lookup 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @return a <code> CourseLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCourseLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseLookupSession getCourseLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getCourseLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course lookup 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @param  proxy proxy 
     *  @return a <code> CourseLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCourseLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseLookupSession getCourseLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getCourseLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course query 
     *  service. 
     *
     *  @return a <code> CourseQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCourseQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseQuerySession getCourseQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getCourseQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course query 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CourseQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCourseQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseQuerySession getCourseQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getCourseQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course query 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CourseQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCourseQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseQuerySession getCourseQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getCourseQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course query 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> CourseQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCourseQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseQuerySession getCourseQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getCourseQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course search 
     *  service. 
     *
     *  @return a <code> CourseSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCourseSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseSearchSession getCourseSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getCourseSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course search 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CourseSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCourseSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseSearchSession getCourseSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getCourseSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course search 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CourseSearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCourseSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseSearchSession getCourseSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getCourseSearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course search 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> CourseSearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCourseSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseSearchSession getCourseSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getCourseSearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  administration service. 
     *
     *  @return a <code> CourseAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCourseAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseAdminSession getCourseAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getCourseAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CourseAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCourseAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseAdminSession getCourseAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getCourseAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CourseAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCourseAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseAdminSession getCourseAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getCourseAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> CourseAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCourseAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseAdminSession getCourseAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getCourseAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  notification service. 
     *
     *  @param  courseReceiver the notification callback 
     *  @return a <code> CourseNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> courseReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseNotificationSession getCourseNotificationSession(org.osid.course.CourseReceiver courseReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getCourseNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  notification service. 
     *
     *  @param  courseReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return a <code> CourseNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> courseReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseNotificationSession getCourseNotificationSession(org.osid.course.CourseReceiver courseReceiver, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getCourseNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  notification service for the given course catalog. 
     *
     *  @param  courseReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CourseNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseReceiver </code> 
     *          or <code> courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseNotificationSession getCourseNotificationSessionForCourseCatalog(org.osid.course.CourseReceiver courseReceiver, 
                                                                                                  org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getCourseNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  notification service for the given course catalog. 
     *
     *  @param  courseReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> CourseNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseReceiver, 
     *          courseCatalogId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseNotificationSession getCourseNotificationSessionForCourseCatalog(org.osid.course.CourseReceiver courseReceiver, 
                                                                                                  org.osid.id.Id courseCatalogId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getCourseNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup course/catalog mappings. 
     *
     *  @return a <code> CourseCourseCourseCatalogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCourseCatalog() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCourseCatalogSession getCourseCourseCatalogSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getCourseCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup course/catalog mappings. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CourseCourseCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCourseCatalog() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCourseCatalogSession getCourseCourseCatalogSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getCourseCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning courses 
     *  to course catalogs. 
     *
     *  @return a <code> CourseCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCourseCatalogAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCourseCatalogAssignmentSession getCourseCourseCatalogAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getCourseCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning courses 
     *  to course catalogs. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CourseCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCourseCatalogAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCourseCatalogAssignmentSession getCourseCourseCatalogAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getCourseCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course smart 
     *  course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CourseSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseSmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseSmartCourseCatalogSession getCourseSmartCourseCatalogSession(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getCourseSmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course smart 
     *  course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> CourseSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseSmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseSmartCourseCatalogSession getCourseSmartCourseCatalogSession(org.osid.id.Id courseCatalogId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getCourseSmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity unit 
     *  lookup service. 
     *
     *  @return an <code> ActivityUnitSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitLookupSession getActivityUnitLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getActivityUnitLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity unit 
     *  lookup service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ActivityUnitSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitLookupSession getActivityUnitLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getActivityUnitLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity unit 
     *  lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @return an <code> ActivityUnitLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitLookupSession getActivityUnitLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getActivityUnitLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity unit 
     *  lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @param  proxy proxy 
     *  @return an <code> ActivityUnitLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitLookupSession getActivityUnitLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getActivityUnitLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity unit 
     *  query service. 
     *
     *  @return an <code> ActivityUnitQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitQuerySession getActivityUnitQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getActivityUnitQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity unit 
     *  query service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ActivityUnitQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitQuerySession getActivityUnitQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getActivityUnitQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity unit 
     *  query service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivityUnitQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitQuerySession getActivityUnitQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getActivityUnitQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity unit 
     *  query service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> ActivityUnitQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitQuerySession getActivityUnitQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getActivityUnitQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity unit 
     *  search service. 
     *
     *  @return an <code> ActivityUnitSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitSearchSession getActivityUnitSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getActivityUnitSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity unit 
     *  search service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ActivityUnitSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitSearchSession getActivityUnitSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getActivityUnitSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity unit 
     *  search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivityUnitSearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitSearchSession getActivityUnitSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getActivityUnitSearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity unit 
     *  search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> ActivityUnitSearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitSearchSession getActivityUnitSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getActivityUnitSearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity unit 
     *  administration service. 
     *
     *  @return an <code> ActivityUnitAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitAdminSession getActivityUnitAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getActivityUnitAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity unit 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ActivityUnitAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitAdminSession getActivityUnitAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getActivityUnitAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity unit 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivityUnitAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitAdminSession getActivityUnitAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getActivityUnitAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity unit 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> ActivityUnitAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitAdminSession getActivityUnitAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getActivityUnitAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity unit 
     *  notification service. 
     *
     *  @param  activityUnitReceiver the notification callback 
     *  @return an <code> ActivityUnitNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> activityUnitReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitNotificationSession getActivityUnitNotificationSession(org.osid.course.ActivityUnitReceiver activityUnitReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getActivityUnitNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity unit 
     *  notification service. 
     *
     *  @param  activityUnitReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return an <code> ActivityUnitNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> activityUnitReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitNotificationSession getActivityUnitNotificationSession(org.osid.course.ActivityUnitReceiver activityUnitReceiver, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getActivityUnitNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity unit 
     *  notification service for the given course catalog. 
     *
     *  @param  activityUnitReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivityUnitNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> activityUnitReceiver 
     *          </code> or <code> courseCatalogId </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitNotificationSession getActivityUnitNotificationSessionForCourseCatalog(org.osid.course.ActivityUnitReceiver activityUnitReceiver, 
                                                                                                              org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getActivityUnitNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity unit 
     *  notification service for the given course catalog. 
     *
     *  @param  activityUnitReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> ActivityUnitNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> activityUnitReceiver, 
     *          courseCatalogId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitNotificationSession getActivityUnitNotificationSessionForCourseCatalog(org.osid.course.ActivityUnitReceiver activityUnitReceiver, 
                                                                                                              org.osid.id.Id courseCatalogId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getActivityUnitNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup activity unit/catalog 
     *  mappings. 
     *
     *  @return an <code> ActivityUnitCourseCatalogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitCourseCatalogSession getActivityUnitCourseCatalogSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getActivityUnitCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup activity unit/catalog 
     *  mappings. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ActivityUnitCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitCatalog() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitCourseCatalogSession getActivityUnitCourseCatalogSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getActivityUnitCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning activity 
     *  units to course catalogs. 
     *
     *  @return an <code> ActivityUnitCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitCourseCatalogAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitCourseCatalogAssignmentSession getActivityUnitCourseCatalogAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getActivityUnitCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning activity 
     *  units to course catalogs. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ActivityUnitCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitCatalogAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitCourseCatalogAssignmentSession getActivityUnitCourseCatalogAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getActivityUnitCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity unit 
     *  smart course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivityUnitSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitSmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitSmartCourseCatalogSession getActivityUnitSmartCourseCatalogSession(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getActivityUnitSmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity unit 
     *  smart course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> ActivityUnitSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitSmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitSmartCourseCatalogSession getActivityUnitSmartCourseCatalogSession(org.osid.id.Id courseCatalogId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getActivityUnitSmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  offering lookup service. 
     *
     *  @return a <code> CourseOfferingSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingLookupSession getCourseOfferingLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getCourseOfferingLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  offering lookup service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CourseOfferingSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingLookupSession getCourseOfferingLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getCourseOfferingLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  offering lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @return a <code> CourseOfferingLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingLookupSession getCourseOfferingLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getCourseOfferingLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  offering lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @param  proxy proxy 
     *  @return a <code> CourseOfferingLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingLookupSession getCourseOfferingLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getCourseOfferingLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  offering query service. 
     *
     *  @return a <code> CourseOfferingQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingQuerySession getCourseOfferingQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getCourseOfferingQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  offering query service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CourseOfferingQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingQuerySession getCourseOfferingQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getCourseOfferingQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  offering query service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CourseOfferingQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingQuerySession getCourseOfferingQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getCourseOfferingQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  offering query service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> CourseOfferingQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingQuerySession getCourseOfferingQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getCourseOfferingQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  offering search service. 
     *
     *  @return a <code> CourseOfferingSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingSearchSession getCourseOfferingSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getCourseOfferingSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  offering search service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CourseOfferingSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingSearchSession getCourseOfferingSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getCourseOfferingSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  offering search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CourseOfferingSearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingSearchSession getCourseOfferingSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getCourseOfferingSearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  offering search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> CourseOfferingSearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingSearchSession getCourseOfferingSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getCourseOfferingSearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  offering administration service. 
     *
     *  @return a <code> CourseOfferingAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingAdminSession getCourseOfferingAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getCourseOfferingAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  offering administration service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CourseOfferingAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingAdminSession getCourseOfferingAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getCourseOfferingAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  offering administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CourseOfferingAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingAdminSession getCourseOfferingAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getCourseOfferingAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  offering administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> CourseOfferingAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingAdminSession getCourseOfferingAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getCourseOfferingAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  offering notification service. 
     *
     *  @param  courseOfferingReceiver the notification callback 
     *  @return a <code> CourseOfferingNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> courseOfferingReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingNotificationSession getCourseOfferingNotificationSession(org.osid.course.CourseOfferingReceiver courseOfferingReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getCourseOfferingNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  offering notification service. 
     *
     *  @param  courseOfferingReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return a <code> CourseOfferingNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> courseOfferingReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingNotificationSession getCourseOfferingNotificationSession(org.osid.course.CourseOfferingReceiver courseOfferingReceiver, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getCourseOfferingNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  offering notification service for the given course catalog. 
     *
     *  @param  courseOfferingReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CourseOfferingNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseOfferingReceiver 
     *          </code> or <code> courseCatalogId </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingNotificationSession getCourseOfferingNotificationSessionForCourseCatalog(org.osid.course.CourseOfferingReceiver courseOfferingReceiver, 
                                                                                                                  org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getCourseOfferingNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  offering notification service for the given course catalog. 
     *
     *  @param  courseOfferingReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> CourseOfferingNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseOfferingReceiver, 
     *          courseCatalogId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingNotificationSession getCourseOfferingNotificationSessionForCourseCatalog(org.osid.course.CourseOfferingReceiver courseOfferingReceiver, 
                                                                                                                  org.osid.id.Id courseCatalogId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getCourseOfferingNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup course offering/catalog 
     *  mappings. 
     *
     *  @return a <code> CourseOfferingCourseCatalogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingCourseCatalogSession getCourseOfferingCourseCatalogSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getCourseOfferingCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup course offering/catalog 
     *  mappings. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CourseOfferingCourseCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingCourseCatalogSession getCourseOfferingCourseCatalogSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getCourseOfferingCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning course 
     *  offerings to course catalogs. 
     *
     *  @return a <code> CourseOfferingCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingCourseCatalogAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingCourseCatalogAssignmentSession getCourseOfferingCourseCatalogAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getCourseOfferingCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning course 
     *  offerings to course catalogs. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CourseOfferingCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingCourseCatalogAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingCourseCatalogAssignmentSession getCourseOfferingCourseCatalogAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getCourseOfferingCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  offering smart course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CourseOfferingSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingSmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingSmartCourseCatalogSession getCourseOfferingSmartCourseCatalogSession(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getCourseOfferingSmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  offering smart course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> CourseOfferingSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingSmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingSmartCourseCatalogSession getCourseOfferingSmartCourseCatalogSession(org.osid.id.Id courseCatalogId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getCourseOfferingSmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  lookup service. 
     *
     *  @return an <code> ActivityLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityLookupSession getActivityLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getActivityLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  lookup service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ActivityLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityLookupSession getActivityLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getActivityLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivityLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityLookupSession getActivityLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getActivityLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> ActivityLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityLookupSession getActivityLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getActivityLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity query 
     *  service. 
     *
     *  @return an <code> ActivityQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsActivityQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityQuerySession getActivityQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getActivityQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity query 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ActivityQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsActivityQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityQuerySession getActivityQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getActivityQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity query 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivityQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsActivityQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityQuerySession getActivityQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getActivityQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity query 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> ActivityQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsActivityQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityQuerySession getActivityQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getActivityQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  search service. 
     *
     *  @return an <code> ActivitySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivitySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivitySearchSession getActivitySearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getActivitySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  search service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ActivitySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivitySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivitySearchSession getActivitySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getActivitySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivitySearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivitySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivitySearchSession getActivitySearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getActivitySearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> ActivitySearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivitySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivitySearchSession getActivitySearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getActivitySearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  administration service. 
     *
     *  @return an <code> ActivityAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsActivityAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityAdminSession getActivityAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getActivityAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ActivityAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsActivityAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityAdminSession getActivityAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getActivityAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivityAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsActivityAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityAdminSession getActivityAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getActivityAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> ActivityAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsActivityAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityAdminSession getActivityAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getActivityAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  notification service. 
     *
     *  @param  activityReceiver the notification callback 
     *  @return an <code> ActivityNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> activityReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityNotificationSession getActivityNotificationSession(org.osid.course.ActivityReceiver activityReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getActivityNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  notification service. 
     *
     *  @param  activityReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return an <code> ActivityNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> activityReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityNotificationSession getActivityNotificationSession(org.osid.course.ActivityReceiver activityReceiver, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getActivityNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  notification service for the given course catalog. 
     *
     *  @param  activityReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivityNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> activityReceiver </code> 
     *          or <code> courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityNotificationSession getActivityNotificationSessionForCourseCatalog(org.osid.course.ActivityReceiver activityReceiver, 
                                                                                                      org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getActivityNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  notification service for the given course catalog. 
     *
     *  @param  activityReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> ActivityNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> activityReceiver, 
     *          courseCatalogId, </code> or <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityNotificationSession getActivityNotificationSessionForCourseCatalog(org.osid.course.ActivityReceiver activityReceiver, 
                                                                                                      org.osid.id.Id courseCatalogId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getActivityNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup activity/catalog 
     *  mappings. 
     *
     *  @return an <code> ActivityCourseCatalogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityCourseCatalogSession getActivityCourseCatalogSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getActivityCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup activity/catalog 
     *  mappings. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ActivityCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityCatalog() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityCourseCatalogSession getActivityCourseCatalogSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getActivityCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  activities to course catalogs. 
     *
     *  @return an <code> ActivityCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityCourseCatalogAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityCourseCatalogAssignmentSession getActivityCourseCatalogAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getActivityCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  activities to course catalogs. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ActivityCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityCatalogAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityCourseCatalogAssignmentSession getActivityCourseCatalogAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getActivityCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity smart 
     *  course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivitySmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivitySmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivitySmartCourseCatalogSession getActivitySmartCourseCatalogSession(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getActivitySmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity smart 
     *  course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> ActivitySmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivitySmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivitySmartCourseCatalogSession getActivitySmartCourseCatalogSession(org.osid.id.Id courseCatalogId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getActivitySmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with unravelling 
     *  activities into meeting times. 
     *
     *  @return an <code> ActivityUnravellingSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnravelling() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnravellingSession getActivityUnravellingSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getActivityUnravellingSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with unravelling 
     *  activities into meeting times. 
     *
     *  @param  proxy proxy 
     *  @return an <code> ActivityUnravellingSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnravelling() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnravellingSession getActivityUnravellingSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getActivityUnravellingSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with unravelling 
     *  activities into meeting times for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @return an <code> ActivityUnravellingSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnravelling() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnravellingSession getActivityUnravellingSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getActivityUnravellingSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with unravelling 
     *  activities into meeting times for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @param  proxy proxy 
     *  @return an <code> ActivityUnravellingSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnravelling() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnravellingSession getActivityUnravellingSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getActivityUnravellingSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the term lookup 
     *  service. 
     *
     *  @return a <code> TermLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTermLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermLookupSession getTermLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getTermLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the term lookup 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> TermLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTermLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermLookupSession getTermLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getTermLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the term lookup 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> TermLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTermLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermLookupSession getTermLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getTermLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the term lookup 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> TermLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTermLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermLookupSession getTermLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getTermLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the term query 
     *  service. 
     *
     *  @return a <code> TermQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTermQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermQuerySession getTermQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getTermQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the term query 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> TermQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTermQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermQuerySession getTermQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getTermQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the term query 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> TermQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTermQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermQuerySession getTermQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getTermQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the term query 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> TermQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTermQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermQuerySession getTermQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getTermQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the term search 
     *  service. 
     *
     *  @return a <code> TermSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTermSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermSearchSession getTermSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getTermSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the term search 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> TermSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTermSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermSearchSession getTermSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getTermSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the term search 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> TermSearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTermSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermSearchSession getTermSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getTermSearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the term search 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> TermSearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTermSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermSearchSession getTermSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getTermSearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the term 
     *  administration service. 
     *
     *  @return a <code> TermAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTermAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermAdminSession getTermAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getTermAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the term 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> TermAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTermAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermAdminSession getTermAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getTermAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the term 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> TermAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTermAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermAdminSession getTermAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getTermAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the term 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> TermAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTermAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermAdminSession getTermAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getTermAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the term 
     *  notification service. 
     *
     *  @param  termReceiver the notification callback 
     *  @return a <code> TermNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> termReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTermNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermNotificationSession getTermNotificationSession(org.osid.course.TermReceiver termReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getTermNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the term 
     *  notification service. 
     *
     *  @param  termReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return a <code> TermNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> termReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTermNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermNotificationSession getTermNotificationSession(org.osid.course.TermReceiver termReceiver, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getTermNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the term 
     *  notification service for the given course catalog. 
     *
     *  @param  termReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> TermNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> termReceiver </code> or 
     *          <code> courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTermNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermNotificationSession getTermNotificationSessionForCourseCatalog(org.osid.course.TermReceiver termReceiver, 
                                                                                              org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getTermNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the term 
     *  notification service for the given course catalog. 
     *
     *  @param  termReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> TermNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> termReceiver, 
     *          courseCatalogId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTermNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermNotificationSession getTermNotificationSessionForCourseCatalog(org.osid.course.TermReceiver termReceiver, 
                                                                                              org.osid.id.Id courseCatalogId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getTermNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the term hierarchy traversal session. 
     *
     *  @return a <code> TermHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTermHierarchy() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermHierarchySession getTermHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getTermHierarchySession not implemented");
    }


    /**
     *  Gets the term hierarchy traversal session. 
     *
     *  @param  proxy proxy 
     *  @return a <code> TermHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTermHierarchy() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermHierarchySession getTermHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getTermHierarchySession not implemented");
    }


    /**
     *  Gets the term hierarchy traversal session for the given course 
     *  catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> TermHierarchySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTermHierarchy() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermHierarchySession getTermHierarchySessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getTermHierarchySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the term hierarchy traversal session for the given catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> TermHierarchySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTermHierarchy() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermHierarchySession getTermHierarchySessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getTermHierarchySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the term hierarchy design session. 
     *
     *  @return a <code> TermHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTermHierarchyDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermHierarchyDesignSession getTermHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getTermHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the term hierarchy design session. 
     *
     *  @param  proxy proxy 
     *  @return a <code> TermHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTermHierarchyDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermHierarchyDesignSession getTermHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getTermHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the term hierarchy design session for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> TermHierarchyDesignSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTermHierarchyDesign() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermHierarchyDesignSession getTermHierarchyDesignSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getTermHierarchyDesignSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the term hierarchy design session for the given catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> TermHierarchyDesignSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTermHierarchyDesign() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermHierarchyDesignSession getTermHierarchyDesignSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getTermHierarchyDesignSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup term/catalog mappings. 
     *
     *  @return a <code> TermCourseCatalogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTermCourseCatalog() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermCourseCatalogSession getTermCourseCatalogSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getTermCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup term/catalog mappings. 
     *
     *  @param  proxy proxy 
     *  @return a <code> TermCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTermCatalog() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermCourseCatalogSession getTermCourseCatalogSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getTermCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning terms to 
     *  course catalogs. 
     *
     *  @return a <code> TermCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTermCourseCatalogAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.TermCourseCatalogAssignmentSession getTermCourseCatalogAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getTermCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning terms to 
     *  course catalogs. 
     *
     *  @param  proxy proxy 
     *  @return a <code> TermCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTermCatalogAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.TermCourseCatalogAssignmentSession getTermCourseCatalogAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getTermCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the term smart 
     *  course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> TermSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTermSmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermSmartCourseCatalogSession getTermSmartCourseCatalogSession(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getTermSmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the term smart 
     *  course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> TermSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTermSmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermSmartCourseCatalogSession getTermSmartCourseCatalogSession(org.osid.id.Id courseCatalogId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getTermSmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course catalog 
     *  lookup service. 
     *
     *  @return a <code> CourseCatalogLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogLookupSession getCourseCatalogLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getCourseCatalogLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course catalog 
     *  lookup service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CourseCatalogLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogLookupSession getCourseCatalogLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getCourseCatalogLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course catalog 
     *  query service. 
     *
     *  @return a <code> CourseCatalogQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuerySession getCourseCatalogQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getCourseCatalogQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course catalog 
     *  query service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CourseCatalogQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuerySession getCourseCatalogQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getCourseCatalogQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course catalog 
     *  search service. 
     *
     *  @return a <code> CourseCatalogSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogSearchSession getCourseCatalogSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getCourseCatalogSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course catalog 
     *  search service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CourseCatalogSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogSearchSession getCourseCatalogSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getCourseCatalogSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course catalog 
     *  administrative service. 
     *
     *  @return a <code> CourseCatalogAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogAdminSession getCourseCatalogAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getCourseCatalogAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course catalog 
     *  administrative service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CourseCatalogAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogAdminSession getCourseCatalogAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getCourseCatalogAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course catalog 
     *  notification service. 
     *
     *  @param  courseCatalogReceiver the notification callback 
     *  @return a <code> CourseCatalogNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogNotificationSession getCourseCatalogNotificationSession(org.osid.course.CourseCatalogReceiver courseCatalogReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getCourseCatalogNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course catalog 
     *  notification service. 
     *
     *  @param  courseCatalogReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return a <code> CourseCatalogNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogNotificationSession getCourseCatalogNotificationSession(org.osid.course.CourseCatalogReceiver courseCatalogReceiver, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getCourseCatalogNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course catalog 
     *  hierarchy service. 
     *
     *  @return a <code> CourseCatalogHierarchySession </code> for course 
     *          catalogs 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogHierarchy() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogHierarchySession getCourseCatalogHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getCourseCatalogHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course catalog 
     *  hierarchy service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CourseCatalogHierarchySession </code> for course 
     *          catalogs 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogHierarchy() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogHierarchySession getCourseCatalogHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getCourseCatalogHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course catalog 
     *  hierarchy design service. 
     *
     *  @return a <code> HierarchyDesignSession </code> for course catalogs 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogHierarchyDesignSession getCourseCatalogHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getCourseCatalogHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course catalog 
     *  hierarchy design service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> HierarchyDesignSession </code> for course catalogs 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogHierarchyDesignSession getCourseCatalogHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getCourseCatalogHierarchyDesignSession not implemented");
    }


    /**
     *  Gets a <code> CourseBatchManager. </code> 
     *
     *  @return a <code> CourseBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCourseBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.batch.CourseBatchManager getCourseBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getCourseBatchManager not implemented");
    }


    /**
     *  Gets a <code> CourseBatchProxyManager. </code> 
     *
     *  @return a <code> CourseBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCourseBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.batch.CourseBatchProxyManager getCourseBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getCourseBatchProxyManager not implemented");
    }


    /**
     *  Gets a <code> CourseProgramManager. </code> 
     *
     *  @return a <code> CourseProgramManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCourseProgram() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CourseProgramManager getCourseProgramManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getCourseProgramManager not implemented");
    }


    /**
     *  Gets a <code> CourseProgramProxyManager. </code> 
     *
     *  @return a <code> CourseProgramProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCourseProgram() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CourseProgramProxyManager getCourseProgramProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getCourseProgramProxyManager not implemented");
    }


    /**
     *  Gets a <code> CourseRegistrationManager. </code> 
     *
     *  @return a <code> CourseRegistrationManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseRegistration() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.CourseRegistrationManager getCourseRegistrationManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getCourseRegistrationManager not implemented");
    }


    /**
     *  Gets a <code> CourseRegistrationProxyManager. </code> 
     *
     *  @return a <code> CourseRegistrationProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseRegistration() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.CourseRegistrationProxyManager getCourseRegistrationProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getCourseRegistrationProxyManager not implemented");
    }


    /**
     *  Gets a <code> CourseRequisiteManager. </code> 
     *
     *  @return a <code> CourseRequisiteManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseRequisite() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.CourseRequisiteManager getCourseRequisiteManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getCourseRequisiteManager not implemented");
    }


    /**
     *  Gets a <code> CourseRequisiteProxyManager. </code> 
     *
     *  @return a <code> CourseRequisiteProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseRequisite() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.CourseRequisiteProxyManager getCourseRequisiteProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getCourseRequisiteProxyManager not implemented");
    }


    /**
     *  Gets a <code> CourseSyllabusManager. </code> 
     *
     *  @return a <code> CourseSyllabusManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseSyllabus() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.CourseSyllabusManager getCourseSyllabusManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getCourseSyllabusManager not implemented");
    }


    /**
     *  Gets a <code> CourseSyllabusProxyManager. </code> 
     *
     *  @return a <code> CourseSyllabusProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseSyllabus() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.CourseSyllabusProxyManager getCourseSyllabusProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getCourseSyllabusProxyManager not implemented");
    }


    /**
     *  Gets a <code> CoursePlanManager. </code> 
     *
     *  @return a <code> CoursePlanManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCoursePlan() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.CoursePlanManager getCoursePlanManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getCoursePlanManager not implemented");
    }


    /**
     *  Gets a <code> CoursePlanProxyManager. </code> 
     *
     *  @return a <code> CoursePlanProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCoursePlan() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.CoursePlanProxyManager getCoursePlanProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getCoursePlanProxyManager not implemented");
    }


    /**
     *  Gets a <code> CourseChronicleManager. </code> 
     *
     *  @return a <code> CourseChronicleManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseChronicle() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseChronicleManager getCourseChronicleManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseManager.getCourseChronicleManager not implemented");
    }


    /**
     *  Gets a <code> CourseChronicleProxyManager. </code> 
     *
     *  @return a <code> CourseChronicleProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseChronicle() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseChronicleProxyManager getCourseChronicleProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.CourseProxyManager.getCourseChronicleProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.courseRecordTypes.clear();
        this.courseRecordTypes.clear();

        this.courseSearchRecordTypes.clear();
        this.courseSearchRecordTypes.clear();

        this.activityUnitRecordTypes.clear();
        this.activityUnitRecordTypes.clear();

        this.activityUnitSearchRecordTypes.clear();
        this.activityUnitSearchRecordTypes.clear();

        this.courseOfferingRecordTypes.clear();
        this.courseOfferingRecordTypes.clear();

        this.courseOfferingSearchRecordTypes.clear();
        this.courseOfferingSearchRecordTypes.clear();

        this.activityRecordTypes.clear();
        this.activityRecordTypes.clear();

        this.activitySearchRecordTypes.clear();
        this.activitySearchRecordTypes.clear();

        this.termRecordTypes.clear();
        this.termRecordTypes.clear();

        this.termSearchRecordTypes.clear();
        this.termSearchRecordTypes.clear();

        this.courseCatalogRecordTypes.clear();
        this.courseCatalogRecordTypes.clear();

        this.courseCatalogSearchRecordTypes.clear();
        this.courseCatalogSearchRecordTypes.clear();

        return;
    }
}
