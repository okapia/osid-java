//
// AbstractPayerBatchFormList
//
//     Implements a filter for a PayerBatchFormList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.billing.payment.batch.payerbatchform.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for a PayerBatchFormList. Subclasses call this
 *  constructor and implement the <code>pass()</code> method. This
 *  filter is synchronous but can be wrapped in a BufferedPayerBatchFormList
 *  to improve performance.
 */

public abstract class AbstractPayerBatchFormFilterList
    extends net.okapia.osid.jamocha.billing.payment.batch.payerbatchform.spi.AbstractPayerBatchFormList
    implements org.osid.billing.payment.batch.PayerBatchFormList,
               net.okapia.osid.jamocha.inline.filter.billing.payment.batch.payerbatchform.PayerBatchFormFilter {

    private org.osid.billing.payment.batch.PayerBatchForm payerBatchForm;
    private final org.osid.billing.payment.batch.PayerBatchFormList list;
    private org.osid.OsidException error;


    /**
     *  Creates a new <code>AbstractPayerBatchFormFilterList</code>.
     *
     *  @param payerBatchFormList a <code>PayerBatchFormList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>payerBatchFormList</code> is <code>null</code>
     */

    protected AbstractPayerBatchFormFilterList(org.osid.billing.payment.batch.PayerBatchFormList payerBatchFormList) {
        nullarg(payerBatchFormList, "payer batch form list");
        this.list = payerBatchFormList;
        return;
    }    

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in
     *          this list, <code> false </code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (hasError()) {
            return (true);
        }

        prime();

        if (this.payerBatchForm == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code> PayerBatchForm </code> in this list. 
     *
     *  @return the next <code> PayerBatchForm </code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code> PayerBatchForm </code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.billing.payment.batch.PayerBatchForm getNextPayerBatchForm()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        if (hasNext()) {
            org.osid.billing.payment.batch.PayerBatchForm payerBatchForm = this.payerBatchForm;
            this.payerBatchForm = null;
            return (payerBatchForm);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in payer batch form list");
        }
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.payerBatchForm = null;
        this.list.close();
        return;
    }

    
    /**
     *  Filters PayerBatchForms.
     *
     *  @param payerBatchForm the payer batch form to filter
     *  @return <code>true</code> if the payer batch form passes the filter,
     *          <code>false</code> if the payer batch form should be filtered
     */

    public abstract boolean pass(org.osid.billing.payment.batch.PayerBatchForm payerBatchForm);


    protected void prime() {
        if (this.payerBatchForm != null) {
            return;
        }

        org.osid.billing.payment.batch.PayerBatchForm payerBatchForm = null;

        while (this.list.hasNext()) {
            try {
                payerBatchForm = this.list.getNextPayerBatchForm();
            } catch (org.osid.OsidException oe) {
                error(oe);
                return;
            }

            if (pass(payerBatchForm)) {
                this.payerBatchForm = payerBatchForm;
                return;
            }
        }

        return;
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    protected void error(org.osid.OsidException error) {
        this.error = error;
        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    protected boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }
}
