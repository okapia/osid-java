//
// AbstractActionEnablerQueryInspector.java
//
//     A template for making an ActionEnablerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.rules.actionenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for action enablers.
 */

public abstract class AbstractActionEnablerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQueryInspector
    implements org.osid.control.rules.ActionEnablerQueryInspector {

    private final java.util.Collection<org.osid.control.rules.records.ActionEnablerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the action <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledActionIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the action query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.ActionQueryInspector[] getRuledActionTerms() {
        return (new org.osid.control.ActionQueryInspector[0]);
    }


    /**
     *  Gets the system <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSystemIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the system query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.SystemQueryInspector[] getSystemTerms() {
        return (new org.osid.control.SystemQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given action enabler query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an action enabler implementing the requested record.
     *
     *  @param actionEnablerRecordType an action enabler record type
     *  @return the action enabler query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>actionEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(actionEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.control.rules.records.ActionEnablerQueryInspectorRecord getActionEnablerQueryInspectorRecord(org.osid.type.Type actionEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.rules.records.ActionEnablerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(actionEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(actionEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this action enabler query. 
     *
     *  @param actionEnablerQueryInspectorRecord action enabler query inspector
     *         record
     *  @param actionEnablerRecordType actionEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addActionEnablerQueryInspectorRecord(org.osid.control.rules.records.ActionEnablerQueryInspectorRecord actionEnablerQueryInspectorRecord, 
                                                   org.osid.type.Type actionEnablerRecordType) {

        addRecordType(actionEnablerRecordType);
        nullarg(actionEnablerRecordType, "action enabler record type");
        this.records.add(actionEnablerQueryInspectorRecord);        
        return;
    }
}
