//
// MutableRequestTransactionPeerList.java
//
//     Implements a RequestTransactionPeerList. This list allows RequestTransactionPeers to be
//     added after this list has been created.
//
//
// Tom Coppeto
// OnTapSolutions
// 29 June 2008
//
//
// Copyright (c) 2008, 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.batch.requesttransactionpeer;


/**
 *  <p>Implements a RequestTransactionPeerList. This list allows RequestTransactionPeers to be
 *  added after this requestTransactionPeer has been created. One this list has been
 *  returned to the consumer, all subsequent additions occur in a
 *  separate processing thread.  The creator of this requestTransactionPeer must
 *  invoke <code>eol()</code> when there are no more requestTransactionPeers to be
 *  added.</p>
 *
 *  <p> If the consumer of the <code>RequestTransactionPeerList</code> interface
 *  reaches the end of the internal buffer before <code>eol()</code>,
 *  then methods will block until more requestTransactionPeers are added or
 *  <code>eol()</code> is invoked.</p>
 *
 *  <p><code>available()</code> never blocks but may return
 *  <code>0</code> if waiting for more requestTransactionPeers to be added.</p>
 */

public final class MutableRequestTransactionPeerList
    extends net.okapia.osid.jamocha.provisioning.batch.requesttransactionpeer.spi.AbstractMutableRequestTransactionPeerList
    implements org.osid.provisioning.batch.RequestTransactionPeerList {


    /**
     *  Creates a new empty <code>MutableRequestTransactionPeerList</code>.
     */

    public MutableRequestTransactionPeerList() {
        super();
    }


    /**
     *  Creates a new <code>MutableRequestTransactionPeerList</code>.
     *
     *  @param requestTransactionPeer a <code>RequestTransactionPeer</code>
     *  @throws org.osid.NullArgumentException <code>requestTransactionPeer</code>
     *          is <code>null</code>
     */

    public MutableRequestTransactionPeerList(org.osid.provisioning.batch.RequestTransactionPeer requestTransactionPeer) {
        super(requestTransactionPeer);
        return;
    }


    /**
     *  Creates a new <code>MutableRequestTransactionPeerList</code>.
     *
     *  @param array an array of requesttransactionpeers
     *  @throws org.osid.NullArgumentException <code>array</code>
     *          is <code>null</code>
     */

    public MutableRequestTransactionPeerList(org.osid.provisioning.batch.RequestTransactionPeer[] array) {
        super(array);
        return;
    }


    /**
     *  Creates a new <code>MutableRequestTransactionPeerList</code>.
     *
     *  @param collection a java.util.Collection of requesttransactionpeers
     *  @throws org.osid.NullArgumentException <code>collection</code>
     *          is <code>null</code>
     */

    public MutableRequestTransactionPeerList(java.util.Collection<org.osid.provisioning.batch.RequestTransactionPeer> collection) {
        super(collection);
        return;
    }
}
