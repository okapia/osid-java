//
// AbstractAdapterRequestTransactionLookupSession.java
//
//    A RequestTransaction lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.provisioning.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A RequestTransaction lookup session adapter.
 */

public abstract class AbstractAdapterRequestTransactionLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.provisioning.RequestTransactionLookupSession {

    private final org.osid.provisioning.RequestTransactionLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterRequestTransactionLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterRequestTransactionLookupSession(org.osid.provisioning.RequestTransactionLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Distributor/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Distributor Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.session.getDistributorId());
    }


    /**
     *  Gets the {@code Distributor} associated with this session.
     *
     *  @return the {@code Distributor} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getDistributor());
    }


    /**
     *  Tests if this user can perform {@code RequestTransaction} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupRequestTransactions() {
        return (this.session.canLookupRequestTransactions());
    }


    /**
     *  A complete view of the {@code RequestTransaction} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRequestTransactionView() {
        this.session.useComparativeRequestTransactionView();
        return;
    }


    /**
     *  A complete view of the {@code RequestTransaction} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRequestTransactionView() {
        this.session.usePlenaryRequestTransactionView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include request transactions in distributors which are children
     *  of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        this.session.useFederatedDistributorView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        this.session.useIsolatedDistributorView();
        return;
    }
    

    /**
     *  Only request transactions whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveRequestTransactionView() {
        this.session.useEffectiveRequestTransactionView();
        return;
    }
    

    /**
     *  All request transactions of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveRequestTransactionView() {
        this.session.useAnyEffectiveRequestTransactionView();
        return;
    }

     
    /**
     *  Gets the {@code RequestTransaction} specified by its {@code
     *  Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a {@code
     *  NOT_FOUND} results. Otherwise, the returned {@code
     *  RequestTransaction} may have a different {@code Id} than
     *  requested, such as the case where a duplicate {@code Id} was
     *  assigned to a {@code RequestTransaction} and retained for
     *  compatibility.
     *
     *  In effective mode, request transactions are returned that are
     *  currently effective.  In any effective mode, effective request
     *  transactions and those currently expired are returned.
     *
     *  @param requestTransactionId {@code Id} of the {@code RequestTransaction}
     *  @return the request transaction
     *  @throws org.osid.NotFoundException {@code requestTransactionId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code requestTransactionId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransaction getRequestTransaction(org.osid.id.Id requestTransactionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRequestTransaction(requestTransactionId));
    }


    /**
     *  Gets a {@code RequestTransactionList} corresponding to the
     *  given {@code IdList}.
     *
     *  In plenary mode, the returned list contains all of the
     *  requestTransactions specified in the {@code Id} list, in the
     *  order of the list, including duplicates, or an error results
     *  if an {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code
     *  RequestTransactions} may be omitted from the list and may
     *  present the elements in any order including returning a unique
     *  set.
     *
     *  In effective mode, request transactions are returned that are
     *  currently effective.  In any effective mode, effective request
     *  transactions and those currently expired are returned.
     *
     *  @param  requestTransactionIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code RequestTransaction} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code requestTransactionIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionList getRequestTransactionsByIds(org.osid.id.IdList requestTransactionIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRequestTransactionsByIds(requestTransactionIds));
    }


    /**
     *  Gets a {@code RequestTransactionList} corresponding to the
     *  given request transaction genus {@code Type} which does not
     *  include request transactions of types derived from the
     *  specified {@code Type}.
     *
     *  In plenary mode, the returned list contains all known request
     *  transactions or an error results. Otherwise, the returned list
     *  may contain only those request transactions that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In effective mode, request transactions are returned that are
     *  currently effective.  In any effective mode, effective request
     *  transactions and those currently expired are returned.
     *
     *  @param  requestTransactionGenusType a requestTransaction genus type 
     *  @return the returned {@code RequestTransaction} list
     *  @throws org.osid.NullArgumentException
     *          {@code requestTransactionGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionList getRequestTransactionsByGenusType(org.osid.type.Type requestTransactionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRequestTransactionsByGenusType(requestTransactionGenusType));
    }


    /**
     *  Gets a {@code RequestTransactionList} corresponding to the
     *  given request transaction genus {@code Type} and include any
     *  additional request transactions with genus types derived from
     *  the specified {@code Type}.
     *
     *  In plenary mode, the returned list contains all known request
     *  transactions or an error results. Otherwise, the returned list
     *  may contain only those request transactions that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In effective mode, request transactions are returned that are
     *  currently effective.  In any effective mode, effective request
     *  transactions and those currently expired are returned.
     *
     *  @param  requestTransactionGenusType a requestTransaction genus type 
     *  @return the returned {@code RequestTransaction} list
     *  @throws org.osid.NullArgumentException
     *          {@code requestTransactionGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionList getRequestTransactionsByParentGenusType(org.osid.type.Type requestTransactionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRequestTransactionsByParentGenusType(requestTransactionGenusType));
    }


    /**
     *  Gets a {@code RequestTransactionList} containing the given
     *  request transaction record {@code Type}.
     * 
     *  In plenary mode, the returned list contains all known request
     *  transactions or an error results. Otherwise, the returned list
     *  may contain only those request transactions that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In effective mode, request transactions are returned that are
     *  currently effective.  In any effective mode, effective request
     *  transactions and those currently expired are returned.
     *
     *  @param  requestTransactionRecordType a requestTransaction record type 
     *  @return the returned {@code RequestTransaction} list
     *  @throws org.osid.NullArgumentException
     *          {@code requestTransactionRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionList getRequestTransactionsByRecordType(org.osid.type.Type requestTransactionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRequestTransactionsByRecordType(requestTransactionRecordType));
    }


    /**
     *  Gets a list of request transactions by request. 
     *  
     *  In plenary mode, the returned list contains all known request
     *  transaction or an error results. Otherwise, the returned list
     *  may contain only those request transactions that are
     *  accessible through this session.
     *  
     *  In effective mode, request transactions are returned that are 
     *  currently effective. In any effective mode, effective request 
     *  transactions and those currently expired are returned. 
     *
     *  @param  requestId a request {@code Id} 
     *  @return the returned {@code RequestTransaction} list 
     *  @throws org.osid.NullArgumentException {@code requestId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionList getRequestTransactionsByRequest(org.osid.id.Id requestId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRequestTransactionsByRequest(requestId));
    }

    
    /**
     *  Gets a {@code RequestTransactionList} effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *  
     *  In plenary mode, the returned list contains all known request
     *  transactions or an error results. Otherwise, the returned list
     *  may contain only those request transactions that are
     *  accessible through this session.
     *  
     *  In active mode, request transactions are returned that are
     *  currently active. In any status mode, active and inactive
     *  request transactions are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code RequestTransaction} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.provisioning.RequestTransactionList getRequestTransactionsOnDate(org.osid.calendaring.DateTime from, 
                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRequestTransactionsOnDate(from, to));
    }
        

    /**
     *  Gets a list of request transactions corresponding to a
     *  submitter {@code Id}.
     *
     *  In plenary mode, the returned list contains all known request
     *  transactions or an error results. Otherwise, the returned list
     *  may contain only those request transactions that are
     *  accessible through this session.
     *
     *  In effective mode, request transactions are returned that are
     *  currently effective.  In any effective mode, effective request
     *  transactions and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the submitter
     *  @return the returned {@code RequestTransactionList}
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionList getRequestTransactionsForSubmitter(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRequestTransactionsForSubmitter(resourceId));
    }


    /**
     *  Gets a list of request transactions corresponding to a
     *  submitter {@code Id} and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known request
     *  transactions or an error results. Otherwise, the returned list
     *  may contain only those request transactions that are
     *  accessible through this session.
     *
     *  In effective mode, request transactions are returned that are
     *  currently effective.  In any effective mode, effective request
     *  transactions and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the submitter
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code RequestTransactionList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionList getRequestTransactionsForSubmitterOnDate(org.osid.id.Id resourceId,
                                                                                                 org.osid.calendaring.DateTime from,
                                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRequestTransactionsForSubmitterOnDate(resourceId, from, to));
    }


    /**
     *  Gets a list of request transactions corresponding to a broker
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known request
     *  transactions or an error results. Otherwise, the returned list
     *  may contain only those request transactions that are
     *  accessible through this session.
     *
     *  In effective mode, request transactions are returned that are
     *  currently effective.  In any effective mode, effective request
     *  transactions and those currently expired are returned.
     *
     *  @param  brokerId the {@code Id} of the broker
     *  @return the returned {@code RequestTransactionList}
     *  @throws org.osid.NullArgumentException {@code brokerId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionList getRequestTransactionsForBroker(org.osid.id.Id brokerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRequestTransactionsForBroker(brokerId));
    }


    /**
     *  Gets a list of request transactions corresponding to a broker
     *  {@code Id} and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known request
     *  transactions or an error results. Otherwise, the returned list
     *  may contain only those request transactions that are
     *  accessible through this session.
     *
     *  In effective mode, request transactions are returned that are
     *  currently effective.  In any effective mode, effective request
     *  transactions and those currently expired are returned.
     *
     *  @param  brokerId the {@code Id} of the broker
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code RequestTransactionList}
     *  @throws org.osid.NullArgumentException {@code brokerId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionList getRequestTransactionsForBrokerOnDate(org.osid.id.Id brokerId,
                                                                                              org.osid.calendaring.DateTime from,
                                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRequestTransactionsForBrokerOnDate(brokerId, from, to));
    }


    /**
     *  Gets a list of request transactions corresponding to submitter
     *  and broker {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known request
     *  transactions or an error results. Otherwise, the returned list
     *  may contain only those request transactions that are
     *  accessible through this session.
     *
     *  In effective mode, request transactions are returned that are
     *  currently effective.  In any effective mode, effective request
     *  transactions and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the submitter
     *  @param  brokerId the {@code Id} of the broker
     *  @return the returned {@code RequestTransactionList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code brokerId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionList getRequestTransactionsForSubmitterAndBroker(org.osid.id.Id resourceId,
                                                                                                    org.osid.id.Id brokerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRequestTransactionsForSubmitterAndBroker(resourceId, brokerId));
    }


    /**
     *  Gets a list of request transactions corresponding to submitter
     *  and broker {@code Ids} and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known request
     *  transactions or an error results. Otherwise, the returned list
     *  may contain only those request transactions that are
     *  accessible through this session.
     *
     *  In effective mode, request transactions are returned that are
     *  currently effective. In any effective mode, effective request
     *  transactions and those currently expired are returned.
     *
     *  @param  brokerId the {@code Id} of the broker
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code RequestTransactionList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code brokerId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionList getRequestTransactionsForSubmitterAndBrokerOnDate(org.osid.id.Id resourceId,
                                                                                                          org.osid.id.Id brokerId,
                                                                                                          org.osid.calendaring.DateTime from,
                                                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRequestTransactionsForSubmitterAndBrokerOnDate(resourceId, brokerId, from, to));
    }


    /**
     *  Gets all {@code RequestTransactions}. 
     *
     *  In plenary mode, the returned list contains all known
     *  request transactions or an error results. Otherwise, the returned list
     *  may contain only those request transactions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, request transactions are returned that are currently
     *  effective.  In any effective mode, effective request transactions and
     *  those currently expired are returned.
     *
     *  @return a list of {@code RequestTransactions} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionList getRequestTransactions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRequestTransactions());
    }
}
