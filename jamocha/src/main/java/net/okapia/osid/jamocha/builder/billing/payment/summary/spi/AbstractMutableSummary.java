//
// AbstractMutableSummary.java
//
//     Defines a mutable Summary.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.billing.payment.summary.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Summary</code>.
 */

public abstract class AbstractMutableSummary
    extends net.okapia.osid.jamocha.billing.payment.summary.spi.AbstractSummary
    implements org.osid.billing.payment.Summary,
               net.okapia.osid.jamocha.builder.billing.payment.summary.SummaryMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this summary. 
     *
     *  @param record summary record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addSummaryRecord(org.osid.billing.payment.records.SummaryRecord record, org.osid.type.Type recordType) {
        super.addSummaryRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the display name for this summary.
     *
     *  @param displayName the name for this summary
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this summary.
     *
     *  @param description the description of this summary
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException <code>genusType</code>
     *          is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the start date.
     *
     *  @param date the start date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setStartDate(org.osid.calendaring.DateTime date) {
        super.setStartDate(date);
        return;
    }


    /**
     *  Sets the end date.
     *
     *  @param date the end date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setEndDate(org.osid.calendaring.DateTime date) {
        super.setEndDate(date);
        return;
    }


    /**
     *  Sets the interpolated flag.
     *
     *  @param interpolated {@code true} is interpolated, {@code
     *         false} otherwise
     */

    @Override
    public void setInterpolated(boolean interpolated) {
        super.setInterpolated(interpolated);
        return;
    }
        

    /**
     *  Sets the extrapolated flag.
     *
     *  @param extrapolated {@code true} is extrapolated, {@code
     *         false} otherwise
     */

    @Override
    public void setExtrapolated(boolean extrapolated) {
        super.setExtrapolated(extrapolated);
        return;
    }


    /**
     *  Sets the customer.
     *
     *  @param customer a customer
     *  @throws org.osid.NullArgumentException <code>customer</code>
     *          is <code>null</code>
     */

    @Override
    public void setCustomer(org.osid.billing.Customer customer) {
        super.setCustomer(customer);
        return;
    }


    /**
     *  Sets the period.
     *
     *  @param period a period
     *  @throws org.osid.NullArgumentException <code>period</code>
     *          is <code>null</code>
     */

    @Override
    public void setPeriod(org.osid.billing.Period period) {
        super.setPeriod(period);
        return;
    }


    /**
     *  Sets the credit balance flag.
     *
     *  @param credit <code>true</code> if a credit,
     *         <code>false</code> if a debit
     */

    @Override
    public void setCreditBalance(boolean credit) {
        super.setCreditBalance(credit);
        return;
    }


    /**
     *  Sets the balance.
     * 
     *  @param balance the balance amount
     *  @throws org.osid.NullArgumentException <code>balance</code> is
     *          <code>null</code>
     */

    @Override
    public void setBalance(org.osid.financials.Currency balance) {
        super.setBalance(balance);
        return;
    }


    /**
     *  Sets the last payment date.
     *
     *  @param date the last payment date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setLastPaymentDate(org.osid.calendaring.DateTime date) {
        super.setLastPaymentDate(date);
        return;
    }


    /**
     *  Sets the last payment amount.
     *
     *  @param amount the last payment amount
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    @Override
    public void setLastPaymentAmount(org.osid.financials.Currency amount) {
        super.setLastPaymentAmount(amount);
        return;
    }


    /**
     *  Sets the due date.
     *
     *  @param date due date
     *  @throws org.osid.NullArgumentException
     *          <code>date</code> is <code>null</code>
     */

    @Override
    public void setPaymentDueDate(org.osid.calendaring.DateTime date) {
        super.setPaymentDueDate(date);
        return;
    }
}

