//
// InvariantMapResourceRelationshipLookupSession
//
//    Implements a ResourceRelationship lookup service backed by a fixed collection of
//    resourceRelationships.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resource;


/**
 *  Implements a ResourceRelationship lookup service backed by a fixed
 *  collection of resource relationships. The resource relationships are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapResourceRelationshipLookupSession
    extends net.okapia.osid.jamocha.core.resource.spi.AbstractMapResourceRelationshipLookupSession
    implements org.osid.resource.ResourceRelationshipLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapResourceRelationshipLookupSession</code> with no
     *  resource relationships.
     *  
     *  @param bin the bin
     *  @throws org.osid.NullArgumnetException {@code bin} is
     *          {@code null}
     */

    public InvariantMapResourceRelationshipLookupSession(org.osid.resource.Bin bin) {
        setBin(bin);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapResourceRelationshipLookupSession</code> with a single
     *  resource relationship.
     *  
     *  @param bin the bin
     *  @param resourceRelationship a single resource relationship
     *  @throws org.osid.NullArgumentException {@code bin} or
     *          {@code resourceRelationship} is <code>null</code>
     */

      public InvariantMapResourceRelationshipLookupSession(org.osid.resource.Bin bin,
                                               org.osid.resource.ResourceRelationship resourceRelationship) {
        this(bin);
        putResourceRelationship(resourceRelationship);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapResourceRelationshipLookupSession</code> using an array
     *  of resource relationships.
     *  
     *  @param bin the bin
     *  @param resourceRelationships an array of resource relationships
     *  @throws org.osid.NullArgumentException {@code bin} or
     *          {@code resourceRelationships} is <code>null</code>
     */

      public InvariantMapResourceRelationshipLookupSession(org.osid.resource.Bin bin,
                                               org.osid.resource.ResourceRelationship[] resourceRelationships) {
        this(bin);
        putResourceRelationships(resourceRelationships);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapResourceRelationshipLookupSession</code> using a
     *  collection of resource relationships.
     *
     *  @param bin the bin
     *  @param resourceRelationships a collection of resource relationships
     *  @throws org.osid.NullArgumentException {@code bin} or
     *          {@code resourceRelationships} is <code>null</code>
     */

      public InvariantMapResourceRelationshipLookupSession(org.osid.resource.Bin bin,
                                               java.util.Collection<? extends org.osid.resource.ResourceRelationship> resourceRelationships) {
        this(bin);
        putResourceRelationships(resourceRelationships);
        return;
    }
}
