//
// AbstractBusinessSearch.java
//
//     A template for making a Business Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.business.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing business searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractBusinessSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.billing.BusinessSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.billing.records.BusinessSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.billing.BusinessSearchOrder businessSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of businesses. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  businessIds list of businesses
     *  @throws org.osid.NullArgumentException
     *          <code>businessIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongBusinesses(org.osid.id.IdList businessIds) {
        while (businessIds.hasNext()) {
            try {
                this.ids.add(businessIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongBusinesses</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of business Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getBusinessIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  businessSearchOrder business search order 
     *  @throws org.osid.NullArgumentException
     *          <code>businessSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>businessSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderBusinessResults(org.osid.billing.BusinessSearchOrder businessSearchOrder) {
	this.businessSearchOrder = businessSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.billing.BusinessSearchOrder getBusinessSearchOrder() {
	return (this.businessSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given business search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a business implementing the requested record.
     *
     *  @param businessSearchRecordType a business search record
     *         type
     *  @return the business search record
     *  @throws org.osid.NullArgumentException
     *          <code>businessSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(businessSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.records.BusinessSearchRecord getBusinessSearchRecord(org.osid.type.Type businessSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.billing.records.BusinessSearchRecord record : this.records) {
            if (record.implementsRecordType(businessSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(businessSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this business search. 
     *
     *  @param businessSearchRecord business search record
     *  @param businessSearchRecordType business search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBusinessSearchRecord(org.osid.billing.records.BusinessSearchRecord businessSearchRecord, 
                                           org.osid.type.Type businessSearchRecordType) {

        addRecordType(businessSearchRecordType);
        this.records.add(businessSearchRecord);        
        return;
    }
}
