//
// OrganizationElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.personnel.organization.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class OrganizationElements
    extends net.okapia.osid.jamocha.spi.TemporalOsidObjectElements {


    /**
     *  Gets the OrganizationElement Id.
     *
     *  @return the organization element Id
     */

    public static org.osid.id.Id getOrganizationEntityId() {
        return (makeEntityId("osid.personnel.Organization"));
    }


    /**
     *  Gets the DisplayLabel element Id.
     *
     *  @return the DisplayLabel element Id
     */

    public static org.osid.id.Id getDisplayLabel() {
        return (makeElementId("osid.personnel.organization.DisplayLabel"));
    }


    /**
     *  Gets the AncestorOrganizationId element Id.
     *
     *  @return the AncestorOrganizationId element Id
     */

    public static org.osid.id.Id getAncestorOrganizationId() {
        return (makeQueryElementId("osid.personnel.organization.AncestorOrganizationId"));
    }


    /**
     *  Gets the AncestorOrganization element Id.
     *
     *  @return the AncestorOrganization element Id
     */

    public static org.osid.id.Id getAncestorOrganization() {
        return (makeQueryElementId("osid.personnel.organization.AncestorOrganization"));
    }


    /**
     *  Gets the DescendantOrganizationId element Id.
     *
     *  @return the DescendantOrganizationId element Id
     */

    public static org.osid.id.Id getDescendantOrganizationId() {
        return (makeQueryElementId("osid.personnel.organization.DescendantOrganizationId"));
    }


    /**
     *  Gets the DescendantOrganization element Id.
     *
     *  @return the DescendantOrganization element Id
     */

    public static org.osid.id.Id getDescendantOrganization() {
        return (makeQueryElementId("osid.personnel.organization.DescendantOrganization"));
    }


    /**
     *  Gets the RealmId element Id.
     *
     *  @return the RealmId element Id
     */

    public static org.osid.id.Id getRealmId() {
        return (makeQueryElementId("osid.personnel.organization.RealmId"));
    }


    /**
     *  Gets the Realm element Id.
     *
     *  @return the Realm element Id
     */

    public static org.osid.id.Id getRealm() {
        return (makeQueryElementId("osid.personnel.organization.Realm"));
    }
}
