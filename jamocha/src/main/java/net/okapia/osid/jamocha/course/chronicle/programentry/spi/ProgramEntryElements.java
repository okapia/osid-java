//
// ProgramEntryElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.chronicle.programentry.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ProgramEntryElements
    extends net.okapia.osid.jamocha.spi.OsidRelationshipElements {


    /**
     *  Gets the ProgramEntryElement Id.
     *
     *  @return the program entry element Id
     */

    public static org.osid.id.Id getProgramEntryEntityId() {
        return (makeEntityId("osid.course.chronicle.ProgramEntry"));
    }


    /**
     *  Gets the StudentId element Id.
     *
     *  @return the StudentId element Id
     */

    public static org.osid.id.Id getStudentId() {
        return (makeElementId("osid.course.chronicle.programentry.StudentId"));
    }


    /**
     *  Gets the Student element Id.
     *
     *  @return the Student element Id
     */

    public static org.osid.id.Id getStudent() {
        return (makeElementId("osid.course.chronicle.programentry.Student"));
    }


    /**
     *  Gets the ProgramId element Id.
     *
     *  @return the ProgramId element Id
     */

    public static org.osid.id.Id getProgramId() {
        return (makeElementId("osid.course.chronicle.programentry.ProgramId"));
    }


    /**
     *  Gets the Program element Id.
     *
     *  @return the Program element Id
     */

    public static org.osid.id.Id getProgram() {
        return (makeElementId("osid.course.chronicle.programentry.Program"));
    }


    /**
     *  Gets the AdmissionDate element Id.
     *
     *  @return the AdmissionDate element Id
     */

    public static org.osid.id.Id getAdmissionDate() {
        return (makeElementId("osid.course.chronicle.programentry.AdmissionDate"));
    }


    /**
     *  Gets the TermId element Id.
     *
     *  @return the TermId element Id
     */

    public static org.osid.id.Id getTermId() {
        return (makeElementId("osid.course.chronicle.programentry.TermId"));
    }


    /**
     *  Gets the Term element Id.
     *
     *  @return the Term element Id
     */

    public static org.osid.id.Id getTerm() {
        return (makeElementId("osid.course.chronicle.programentry.Term"));
    }


    /**
     *  Gets the CreditScaleId element Id.
     *
     *  @return the CreditScaleId element Id
     */

    public static org.osid.id.Id getCreditScaleId() {
        return (makeElementId("osid.course.chronicle.programentry.CreditScaleId"));
    }


    /**
     *  Gets the CreditScale element Id.
     *
     *  @return the CreditScale element Id
     */

    public static org.osid.id.Id getCreditScale() {
        return (makeElementId("osid.course.chronicle.programentry.CreditScale"));
    }


    /**
     *  Gets the CreditsEarned element Id.
     *
     *  @return the CreditsEarned element Id
     */

    public static org.osid.id.Id getCreditsEarned() {
        return (makeElementId("osid.course.chronicle.programentry.CreditsEarned"));
    }


    /**
     *  Gets the GPAScaleId element Id.
     *
     *  @return the GPAScaleId element Id
     */

    public static org.osid.id.Id getGPAScaleId() {
        return (makeElementId("osid.course.chronicle.programentry.GPAScaleId"));
    }


    /**
     *  Gets the GPAScale element Id.
     *
     *  @return the GPAScale element Id
     */

    public static org.osid.id.Id getGPAScale() {
        return (makeElementId("osid.course.chronicle.programentry.GPAScale"));
    }


    /**
     *  Gets the GPA element Id.
     *
     *  @return the GPA element Id
     */

    public static org.osid.id.Id getGPA() {
        return (makeElementId("osid.course.chronicle.programentry.GPA"));
    }


    /**
     *  Gets the EnrollmentIds element Id.
     *
     *  @return the EnrollmentIds element Id
     */

    public static org.osid.id.Id getEnrollmentIds() {
        return (makeElementId("osid.course.chronicle.programentry.EnrollmentIds"));
    }


    /**
     *  Gets the Enrollments element Id.
     *
     *  @return the Enrollments element Id
     */

    public static org.osid.id.Id getEnrollments() {
        return (makeElementId("osid.course.chronicle.programentry.Enrollments"));
    }


    /**
     *  Gets the Complete element Id.
     *
     *  @return the Complete element Id
     */

    public static org.osid.id.Id getComplete() {
        return (makeElementId("osid.course.chronicle.programentry.Complete"));
    }


    /**
     *  Gets the CourseCatalogId element Id.
     *
     *  @return the CourseCatalogId element Id
     */

    public static org.osid.id.Id getCourseCatalogId() {
        return (makeQueryElementId("osid.course.chronicle.programentry.CourseCatalogId"));
    }


    /**
     *  Gets the CourseCatalog element Id.
     *
     *  @return the CourseCatalog element Id
     */

    public static org.osid.id.Id getCourseCatalog() {
        return (makeQueryElementId("osid.course.chronicle.programentry.CourseCatalog"));
    }
}
