//
// AbstractImmutableRegistration.java
//
//     Wraps a mutable Registration to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.registration.registration.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Registration</code> to hide modifiers. This
 *  wrapper provides an immutized Registration from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying registration whose state changes are visible.
 */

public abstract class AbstractImmutableRegistration
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.course.registration.Registration {

    private final org.osid.course.registration.Registration registration;


    /**
     *  Constructs a new <code>AbstractImmutableRegistration</code>.
     *
     *  @param registration the registration to immutablize
     *  @throws org.osid.NullArgumentException <code>registration</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableRegistration(org.osid.course.registration.Registration registration) {
        super(registration);
        this.registration = registration;
        return;
    }


    /**
     *  Gets the activity bundle <code> Id </code> associated with this 
     *  registration. 
     *
     *  @return the activity bundle <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getActivityBundleId() {
        return (this.registration.getActivityBundleId());
    }


    /**
     *  Gets the activity bundle associated with this registration. 
     *
     *  @return the activity bundle 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundle getActivityBundle()
        throws org.osid.OperationFailedException {

        return (this.registration.getActivityBundle());
    }


    /**
     *  Gets the <code> Id </code> of the student <code> Resource. </code> 
     *
     *  @return the <code> Student </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getStudentId() {
        return (this.registration.getStudentId());
    }


    /**
     *  Gets the student <code> Resource. </code> 
     *
     *  @return the student 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getStudent()
        throws org.osid.OperationFailedException {

        return (this.registration.getStudent());
    }


    /**
     *  Tests if this registration defines credits to be earned. 
     *
     *  @return <code> true </code> if this registration has credits, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean definesCredits() {
        return (this.registration.definesCredits());
    }


    /**
     *  Gets the number of credits the student is registered to earn. Multiple 
     *  credit options indicates a set of credits to be determined at the 
     *  completion of the course. 
     *
     *  @return the number of credits 
     *  @throws org.osid.IllegalStateException <code> definesCredits() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public java.math.BigDecimal[] getCredits() {
        return (this.registration.getCredits());
    }


    /**
     *  Tests if this registration includes a specific grading option. 
     *
     *  @return <code> true </code> if this course is graded, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isGraded() {
        return (this.registration.isGraded());
    }


    /**
     *  Gets the grading option <code> Id </code> for this registration. 
     *
     *  @return the grading option <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isGraded() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getGradingOptionId() {
        return (this.registration.getGradingOptionId());
    }


    /**
     *  Gets the grading option for this registration. 
     *
     *  @return a grading system 
     *  @throws org.osid.IllegalStateException <code> isGraded() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeSystem getGradingOption()
        throws org.osid.OperationFailedException {

        return (this.registration.getGradingOption());
    }


    /**
     *  Gets the registration record corresponding to the given <code> 
     *  Registration </code> record <code> Type. </code> This method is used 
     *  to retrieve an object implementing the requested record. The <code> 
     *  registrationRecordType </code> may be the <code> Type </code> returned 
     *  in <code> getRecordTypes() </code> or any of its parents in a <code> 
     *  Type </code> hierarchy where <code> 
     *  hasRecordType(registrationRecordType) </code> is <code> true </code> . 
     *
     *  @param  registrationRecordType the type of registration record to 
     *          retrieve 
     *  @return the registration record 
     *  @throws org.osid.NullArgumentException <code> registrationRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(registrationRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.registration.records.RegistrationRecord getRegistrationRecord(org.osid.type.Type registrationRecordType)
        throws org.osid.OperationFailedException {

        return (this.registration.getRegistrationRecord(registrationRecordType));
    }
}

