//
// AbstractQueryParticipantLookupSession.java
//
//    A ParticipantQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.offering.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ParticipantQuerySession adapter.
 */

public abstract class AbstractAdapterParticipantQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.offering.ParticipantQuerySession {

    private final org.osid.offering.ParticipantQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterParticipantQuerySession.
     *
     *  @param session the underlying participant query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterParticipantQuerySession(org.osid.offering.ParticipantQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeCatalogue</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeCatalogue Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCatalogueId() {
        return (this.session.getCatalogueId());
    }


    /**
     *  Gets the {@codeCatalogue</code> associated with this 
     *  session.
     *
     *  @return the {@codeCatalogue</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Catalogue getCatalogue()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCatalogue());
    }


    /**
     *  Tests if this user can perform {@codeParticipant</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchParticipants() {
        return (this.session.canSearchParticipants());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include participants in catalogues which are children
     *  of this catalogue in the catalogue hierarchy.
     */

    @OSID @Override
    public void useFederatedCatalogueView() {
        this.session.useFederatedCatalogueView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this catalogue only.
     */
    
    @OSID @Override
    public void useIsolatedCatalogueView() {
        this.session.useIsolatedCatalogueView();
        return;
    }
    
      
    /**
     *  Gets a participant query. The returned query will not have an
     *  extension query.
     *
     *  @return the participant query 
     */
      
    @OSID @Override
    public org.osid.offering.ParticipantQuery getParticipantQuery() {
        return (this.session.getParticipantQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  participantQuery the participant query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code participantQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code participantQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByQuery(org.osid.offering.ParticipantQuery participantQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getParticipantsByQuery(participantQuery));
    }
}
