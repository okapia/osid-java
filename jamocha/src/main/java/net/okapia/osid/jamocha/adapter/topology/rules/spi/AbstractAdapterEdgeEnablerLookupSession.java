//
// AbstractAdapterEdgeEnablerLookupSession.java
//
//    An EdgeEnabler lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.topology.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An EdgeEnabler lookup session adapter.
 */

public abstract class AbstractAdapterEdgeEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.topology.rules.EdgeEnablerLookupSession {

    private final org.osid.topology.rules.EdgeEnablerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterEdgeEnablerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterEdgeEnablerLookupSession(org.osid.topology.rules.EdgeEnablerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Graph/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Graph Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getGraphId() {
        return (this.session.getGraphId());
    }


    /**
     *  Gets the {@code Graph} associated with this session.
     *
     *  @return the {@code Graph} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.Graph getGraph()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getGraph());
    }


    /**
     *  Tests if this user can perform {@code EdgeEnabler} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupEdgeEnablers() {
        return (this.session.canLookupEdgeEnablers());
    }


    /**
     *  A complete view of the {@code EdgeEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeEdgeEnablerView() {
        this.session.useComparativeEdgeEnablerView();
        return;
    }


    /**
     *  A complete view of the {@code EdgeEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryEdgeEnablerView() {
        this.session.usePlenaryEdgeEnablerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include edge enablers in graphs which are children
     *  of this graph in the graph hierarchy.
     */

    @OSID @Override
    public void useFederatedGraphView() {
        this.session.useFederatedGraphView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this graph only.
     */

    @OSID @Override
    public void useIsolatedGraphView() {
        this.session.useIsolatedGraphView();
        return;
    }
    

    /**
     *  Only active edge enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveEdgeEnablerView() {
        this.session.useActiveEdgeEnablerView();
        return;
    }


    /**
     *  Active and inactive edge enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusEdgeEnablerView() {
        this.session.useAnyStatusEdgeEnablerView();
        return;
    }
    
     
    /**
     *  Gets the {@code EdgeEnabler} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code EdgeEnabler} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code EdgeEnabler} and
     *  retained for compatibility.
     *
     *  In active mode, edge enablers are returned that are currently
     *  active. In any status mode, active and inactive edge enablers
     *  are returned.
     *
     *  @param edgeEnablerId {@code Id} of the {@code EdgeEnabler}
     *  @return the edge enabler
     *  @throws org.osid.NotFoundException {@code edgeEnablerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code edgeEnablerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnabler getEdgeEnabler(org.osid.id.Id edgeEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEdgeEnabler(edgeEnablerId));
    }


    /**
     *  Gets an {@code EdgeEnablerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  edgeEnablers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code EdgeEnablers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, edge enablers are returned that are currently
     *  active. In any status mode, active and inactive edge enablers
     *  are returned.
     *
     *  @param  edgeEnablerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code EdgeEnabler} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code edgeEnablerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerList getEdgeEnablersByIds(org.osid.id.IdList edgeEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEdgeEnablersByIds(edgeEnablerIds));
    }


    /**
     *  Gets an {@code EdgeEnablerList} corresponding to the given
     *  edge enabler genus {@code Type} which does not include
     *  edge enablers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  edge enablers or an error results. Otherwise, the returned list
     *  may contain only those edge enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, edge enablers are returned that are currently
     *  active. In any status mode, active and inactive edge enablers
     *  are returned.
     *
     *  @param  edgeEnablerGenusType an edgeEnabler genus type 
     *  @return the returned {@code EdgeEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code edgeEnablerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerList getEdgeEnablersByGenusType(org.osid.type.Type edgeEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEdgeEnablersByGenusType(edgeEnablerGenusType));
    }


    /**
     *  Gets an {@code EdgeEnablerList} corresponding to the given
     *  edge enabler genus {@code Type} and include any additional
     *  edge enablers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  edge enablers or an error results. Otherwise, the returned list
     *  may contain only those edge enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, edge enablers are returned that are currently
     *  active. In any status mode, active and inactive edge enablers
     *  are returned.
     *
     *  @param  edgeEnablerGenusType an edgeEnabler genus type 
     *  @return the returned {@code EdgeEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code edgeEnablerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerList getEdgeEnablersByParentGenusType(org.osid.type.Type edgeEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEdgeEnablersByParentGenusType(edgeEnablerGenusType));
    }


    /**
     *  Gets an {@code EdgeEnablerList} containing the given
     *  edge enabler record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  edge enablers or an error results. Otherwise, the returned list
     *  may contain only those edge enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, edge enablers are returned that are currently
     *  active. In any status mode, active and inactive edge enablers
     *  are returned.
     *
     *  @param  edgeEnablerRecordType an edgeEnabler record type 
     *  @return the returned {@code EdgeEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code edgeEnablerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerList getEdgeEnablersByRecordType(org.osid.type.Type edgeEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEdgeEnablersByRecordType(edgeEnablerRecordType));
    }


    /**
     *  Gets an {@code EdgeEnablerList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  edge enablers or an error results. Otherwise, the returned list
     *  may contain only those edge enablers that are accessible
     *  through this session.
     *  
     *  In active mode, edge enablers are returned that are currently
     *  active. In any status mode, active and inactive edge enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code EdgeEnabler} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerList getEdgeEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEdgeEnablersOnDate(from, to));
    }
        

    /**
     *  Gets an {@code EdgeEnablerList } which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  edge enablers or an error results. Otherwise, the returned list
     *  may contain only those edge enablers that are accessible
     *  through this session.
     *
     *  In active mode, edge enablers are returned that are currently
     *  active. In any status mode, active and inactive edge enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned {@code EdgeEnabler} list
     *  @throws org.osid.InvalidArgumentException {@code from} is
     *          greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code agent},
     *          {@code from}, or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerList getEdgeEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (this.session.getEdgeEnablersOnDateWithAgent(agentId, from, to));
    }


    /**
     *  Gets all {@code EdgeEnablers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  edge enablers or an error results. Otherwise, the returned list
     *  may contain only those edge enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, edge enablers are returned that are currently
     *  active. In any status mode, active and inactive edge enablers
     *  are returned.
     *
     *  @return a list of {@code EdgeEnablers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerList getEdgeEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEdgeEnablers());
    }
}
