//
// AbstractScheduleSearchOdrer.java
//
//     Defines a ScheduleSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.schedule.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code ScheduleSearchOrder}.
 */

public abstract class AbstractScheduleSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectSearchOrder
    implements org.osid.calendaring.ScheduleSearchOrder {

    private final java.util.Collection<org.osid.calendaring.records.ScheduleSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specified a preference for ordering results by the schedule slot. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByScheduleSlot(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a <code> ScheduleSlotSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a schedule slot search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleSlotSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for the schedule slot. 
     *
     *  @return the schdeule slot search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotSearchOrder getScheduleSlotSearchOrder() {
        throw new org.osid.UnimplementedException("supportsScheduleSlotSearchOrder() is false");
    }


    /**
     *  Specified a preference for ordering results by the time period. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTimePeriod(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a <code> TimePeriodSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a time period search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTimePeriodSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for the time period. 
     *
     *  @return the time period search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodSearchOrder getTimePeriodSearchOrder() {
        throw new org.osid.UnimplementedException("supportsTimePeriodSearchOrder() is false");
    }


    /**
     *  Specified a preference for ordering results by the schedule start. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByScheduleStart(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the schedule end. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByScheduleEnd(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the schedule duration. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTotalDuration(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the occurrence limit. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByLimit(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the location 
     *  description. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByLocationDescription(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the location. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByLocation(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a <code> LocationSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a location search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a location. 
     *
     *  @return the location search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationSearchOrder getLocationSearchOrder() {
        throw new org.osid.UnimplementedException("supportsLocationSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  scheduleRecordType a schedule record type 
     *  @return {@code true} if the scheduleRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code scheduleRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type scheduleRecordType) {
        for (org.osid.calendaring.records.ScheduleSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(scheduleRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  scheduleRecordType the schedule record type 
     *  @return the schedule search order record
     *  @throws org.osid.NullArgumentException
     *          {@code scheduleRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(scheduleRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.calendaring.records.ScheduleSearchOrderRecord getScheduleSearchOrderRecord(org.osid.type.Type scheduleRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.ScheduleSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(scheduleRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(scheduleRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this schedule. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param scheduleRecord the schedule search odrer record
     *  @param scheduleRecordType schedule record type
     *  @throws org.osid.NullArgumentException
     *          {@code scheduleRecord} or
     *          {@code scheduleRecordTypeschedule} is
     *          {@code null}
     */
            
    protected void addScheduleRecord(org.osid.calendaring.records.ScheduleSearchOrderRecord scheduleSearchOrderRecord, 
                                     org.osid.type.Type scheduleRecordType) {

        addRecordType(scheduleRecordType);
        this.records.add(scheduleSearchOrderRecord);
        
        return;
    }
}
