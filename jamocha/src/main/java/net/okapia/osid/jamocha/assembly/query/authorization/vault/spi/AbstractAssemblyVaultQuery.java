//
// AbstractAssemblyVaultQuery.java
//
//     A VaultQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.authorization.vault.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A VaultQuery that stores terms.
 */

public abstract class AbstractAssemblyVaultQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.authorization.VaultQuery,
               org.osid.authorization.VaultQueryInspector,
               org.osid.authorization.VaultSearchOrder {

    private final java.util.Collection<org.osid.authorization.records.VaultQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.authorization.records.VaultQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.authorization.records.VaultSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyVaultQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyVaultQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the function <code> Id </code> for this query. 
     *
     *  @param  functionId a function <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> functionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchFunctionId(org.osid.id.Id functionId, boolean match) {
        getAssembler().addIdTerm(getFunctionIdColumn(), functionId, match);
        return;
    }


    /**
     *  Clears the function <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearFunctionIdTerms() {
        getAssembler().clearTerms(getFunctionIdColumn());
        return;
    }


    /**
     *  Gets the function <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFunctionIdTerms() {
        return (getAssembler().getIdTerms(getFunctionIdColumn()));
    }


    /**
     *  Gets the FunctionId column name.
     *
     * @return the column name
     */

    protected String getFunctionIdColumn() {
        return ("function_id");
    }


    /**
     *  Tests if a <code> FunctionQuery </code> is available. 
     *
     *  @return <code> true </code> if a function query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFunctionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a function. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the function query 
     *  @throws org.osid.UnimplementedException <code> supportsFunctionQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionQuery getFunctionQuery() {
        throw new org.osid.UnimplementedException("supportsFunctionQuery() is false");
    }


    /**
     *  Matches vaults that have any function. 
     *
     *  @param  match <code> true </code> to match vaults with any function 
     *          mapping, <code> false </code> to match vaults with no function 
     *          mapping 
     */

    @OSID @Override
    public void matchAnyFunction(boolean match) {
        getAssembler().addIdWildcardTerm(getFunctionColumn(), match);
        return;
    }


    /**
     *  Clears the function query terms. 
     */

    @OSID @Override
    public void clearFunctionTerms() {
        getAssembler().clearTerms(getFunctionColumn());
        return;
    }


    /**
     *  Gets the function query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authorization.FunctionQueryInspector[] getFunctionTerms() {
        return (new org.osid.authorization.FunctionQueryInspector[0]);
    }


    /**
     *  Gets the Function column name.
     *
     * @return the column name
     */

    protected String getFunctionColumn() {
        return ("function");
    }


    /**
     *  Sets the qualifier <code> Id </code> for this query. 
     *
     *  @param  qualifierId a qualifier <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> qualifierId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchQualifierId(org.osid.id.Id qualifierId, boolean match) {
        getAssembler().addIdTerm(getQualifierIdColumn(), qualifierId, match);
        return;
    }


    /**
     *  Clears the qualifier <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearQualifierIdTerms() {
        getAssembler().clearTerms(getQualifierIdColumn());
        return;
    }


    /**
     *  Gets the qualifier <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getQualifierIdTerms() {
        return (getAssembler().getIdTerms(getQualifierIdColumn()));
    }


    /**
     *  Gets the QualifierId column name.
     *
     * @return the column name
     */

    protected String getQualifierIdColumn() {
        return ("qualifier_id");
    }


    /**
     *  Tests if a <code> QualifierQuery </code> is available. 
     *
     *  @return <code> true </code> if a qualifier query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQualifierQuery() {
        return (false);
    }


    /**
     *  Gets the query for a qualifier. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the qualifier query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierQuery getQualifierQuery() {
        throw new org.osid.UnimplementedException("supportsQualifierQuery() is false");
    }


    /**
     *  Matches vaults that have any qualifier. 
     *
     *  @param  match <code> true </code> to match vaults with any qualifier 
     *          mapping, <code> false </code> to match vaults with no 
     *          qualifier mapping 
     */

    @OSID @Override
    public void matchAnyQualifier(boolean match) {
        getAssembler().addIdWildcardTerm(getQualifierColumn(), match);
        return;
    }


    /**
     *  Clears the qualifier query terms. 
     */

    @OSID @Override
    public void clearQualifierTerms() {
        getAssembler().clearTerms(getQualifierColumn());
        return;
    }


    /**
     *  Gets the qualifier query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authorization.QualifierQueryInspector[] getQualifierTerms() {
        return (new org.osid.authorization.QualifierQueryInspector[0]);
    }


    /**
     *  Gets the Qualifier column name.
     *
     * @return the column name
     */

    protected String getQualifierColumn() {
        return ("qualifier");
    }


    /**
     *  Sets the authorization <code> Id </code> for this query. 
     *
     *  @param  authorizationId an authorization <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> authorizationId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchAuthorizationId(org.osid.id.Id authorizationId, 
                                     boolean match) {
        getAssembler().addIdTerm(getAuthorizationIdColumn(), authorizationId, match);
        return;
    }


    /**
     *  Clears the authorization <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAuthorizationIdTerms() {
        getAssembler().clearTerms(getAuthorizationIdColumn());
        return;
    }


    /**
     *  Gets the authorization <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAuthorizationIdTerms() {
        return (getAssembler().getIdTerms(getAuthorizationIdColumn()));
    }


    /**
     *  Gets the AuthorizationId column name.
     *
     * @return the column name
     */

    protected String getAuthorizationIdColumn() {
        return ("authorization_id");
    }


    /**
     *  Tests if an <code> AuthorizationQuery </code> is available. 
     *
     *  @return <code> true </code> if an authorization query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizationQuery() {
        return (false);
    }


    /**
     *  Gets the query for an authorization. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the authorization query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationQuery getAuthorizationQuery() {
        throw new org.osid.UnimplementedException("supportsAuthorizationQuery() is false");
    }


    /**
     *  Matches vaults that have any authorization. 
     *
     *  @param  match <code> true </code> to match vaults with any 
     *          authorization mapping, <code> false </code> to match vaults 
     *          with no authorization mapping 
     */

    @OSID @Override
    public void matchAnyAuthorization(boolean match) {
        getAssembler().addIdWildcardTerm(getAuthorizationColumn(), match);
        return;
    }


    /**
     *  Clears the authorization query terms. 
     */

    @OSID @Override
    public void clearAuthorizationTerms() {
        getAssembler().clearTerms(getAuthorizationColumn());
        return;
    }


    /**
     *  Gets the authorization query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationQueryInspector[] getAuthorizationTerms() {
        return (new org.osid.authorization.AuthorizationQueryInspector[0]);
    }


    /**
     *  Gets the Authorization column name.
     *
     * @return the column name
     */

    protected String getAuthorizationColumn() {
        return ("authorization");
    }


    /**
     *  Sets the vault <code> Id </code> for this query to match vaults that 
     *  have the specified vault as an ancestor. 
     *
     *  @param  vaultId a vault <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorVaultId(org.osid.id.Id vaultId, boolean match) {
        getAssembler().addIdTerm(getAncestorVaultIdColumn(), vaultId, match);
        return;
    }


    /**
     *  Clears the ancestor vault <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorVaultIdTerms() {
        getAssembler().clearTerms(getAncestorVaultIdColumn());
        return;
    }


    /**
     *  Gets the ancestor vault <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorVaultIdTerms() {
        return (getAssembler().getIdTerms(getAncestorVaultIdColumn()));
    }


    /**
     *  Gets the AncestorVaultId column name.
     *
     * @return the column name
     */

    protected String getAncestorVaultIdColumn() {
        return ("ancestor_vault_id");
    }


    /**
     *  Tests if a <code> VaultQuery </code> is available. 
     *
     *  @return <code> true </code> if a vault query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorVaultQuery() {
        return (false);
    }


    /**
     *  Gets the query for a vault. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the vault query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorVaultQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.VaultQuery getAncestorVaultQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorVaultQuery() is false");
    }


    /**
     *  Matches vaults that have any ancestor. 
     *
     *  @param  match <code> true </code> to match vaults with any ancestor, 
     *          <code> false </code> to match root vaults 
     */

    @OSID @Override
    public void matchAnyAncestorVault(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorVaultColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor vault query terms. 
     */

    @OSID @Override
    public void clearAncestorVaultTerms() {
        getAssembler().clearTerms(getAncestorVaultColumn());
        return;
    }


    /**
     *  Gets the ancestor vault query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authorization.VaultQueryInspector[] getAncestorVaultTerms() {
        return (new org.osid.authorization.VaultQueryInspector[0]);
    }


    /**
     *  Gets the AncestorVault column name.
     *
     * @return the column name
     */

    protected String getAncestorVaultColumn() {
        return ("ancestor_vault");
    }


    /**
     *  Sets the vault <code> Id </code> for this query to match vaults that 
     *  have the specified vault as a descendant. 
     *
     *  @param  vaultId a vault <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantVaultId(org.osid.id.Id vaultId, boolean match) {
        getAssembler().addIdTerm(getDescendantVaultIdColumn(), vaultId, match);
        return;
    }


    /**
     *  Clears the descendant vault <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantVaultIdTerms() {
        getAssembler().clearTerms(getDescendantVaultIdColumn());
        return;
    }


    /**
     *  Gets the descendant vault <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantVaultIdTerms() {
        return (getAssembler().getIdTerms(getDescendantVaultIdColumn()));
    }


    /**
     *  Gets the DescendantVaultId column name.
     *
     * @return the column name
     */

    protected String getDescendantVaultIdColumn() {
        return ("descendant_vault_id");
    }


    /**
     *  Tests if a <code> VaultQuery </code> is available. 
     *
     *  @return <code> true </code> if a vault query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantVaultQuery() {
        return (false);
    }


    /**
     *  Gets the query for a vault. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the vault query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantVaultQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.VaultQuery getDescendantVaultQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantVaultQuery() is false");
    }


    /**
     *  Matches vaults that have any descendant. 
     *
     *  @param  match <code> true </code> to match vaults with any Ddscendant, 
     *          <code> false </code> to match leaf vaults 
     */

    @OSID @Override
    public void matchAnyDescendantVault(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantVaultColumn(), match);
        return;
    }


    /**
     *  Clears the descendant vault query terms. 
     */

    @OSID @Override
    public void clearDescendantVaultTerms() {
        getAssembler().clearTerms(getDescendantVaultColumn());
        return;
    }


    /**
     *  Gets the descendant vault query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authorization.VaultQueryInspector[] getDescendantVaultTerms() {
        return (new org.osid.authorization.VaultQueryInspector[0]);
    }


    /**
     *  Gets the DescendantVault column name.
     *
     * @return the column name
     */

    protected String getDescendantVaultColumn() {
        return ("descendant_vault");
    }


    /**
     *  Tests if this vault supports the given record
     *  <code>Type</code>.
     *
     *  @param  vaultRecordType a vault record type 
     *  @return <code>true</code> if the vaultRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>vaultRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type vaultRecordType) {
        for (org.osid.authorization.records.VaultQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(vaultRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  vaultRecordType the vault record type 
     *  @return the vault query record 
     *  @throws org.osid.NullArgumentException
     *          <code>vaultRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(vaultRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.authorization.records.VaultQueryRecord getVaultQueryRecord(org.osid.type.Type vaultRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authorization.records.VaultQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(vaultRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(vaultRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  vaultRecordType the vault record type 
     *  @return the vault query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>vaultRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(vaultRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.authorization.records.VaultQueryInspectorRecord getVaultQueryInspectorRecord(org.osid.type.Type vaultRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authorization.records.VaultQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(vaultRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(vaultRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param vaultRecordType the vault record type
     *  @return the vault search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>vaultRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(vaultRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.authorization.records.VaultSearchOrderRecord getVaultSearchOrderRecord(org.osid.type.Type vaultRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authorization.records.VaultSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(vaultRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(vaultRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this vault. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param vaultQueryRecord the vault query record
     *  @param vaultQueryInspectorRecord the vault query inspector
     *         record
     *  @param vaultSearchOrderRecord the vault search order record
     *  @param vaultRecordType vault record type
     *  @throws org.osid.NullArgumentException
     *          <code>vaultQueryRecord</code>,
     *          <code>vaultQueryInspectorRecord</code>,
     *          <code>vaultSearchOrderRecord</code> or
     *          <code>vaultRecordTypevault</code> is
     *          <code>null</code>
     */
            
    protected void addVaultRecords(org.osid.authorization.records.VaultQueryRecord vaultQueryRecord, 
                                      org.osid.authorization.records.VaultQueryInspectorRecord vaultQueryInspectorRecord, 
                                      org.osid.authorization.records.VaultSearchOrderRecord vaultSearchOrderRecord, 
                                      org.osid.type.Type vaultRecordType) {

        addRecordType(vaultRecordType);

        nullarg(vaultQueryRecord, "vault query record");
        nullarg(vaultQueryInspectorRecord, "vault query inspector record");
        nullarg(vaultSearchOrderRecord, "vault search odrer record");

        this.queryRecords.add(vaultQueryRecord);
        this.queryInspectorRecords.add(vaultQueryInspectorRecord);
        this.searchOrderRecords.add(vaultSearchOrderRecord);
        
        return;
    }
}
