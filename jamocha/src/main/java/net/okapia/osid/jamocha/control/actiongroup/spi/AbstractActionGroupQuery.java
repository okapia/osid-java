//
// AbstractActionGroupQuery.java
//
//     A template for making an ActionGroup Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.actiongroup.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for action groups.
 */

public abstract class AbstractActionGroupQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.control.ActionGroupQuery {

    private final java.util.Collection<org.osid.control.records.ActionGroupQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the action <code> Id </code> for this query. 
     *
     *  @param  actionId the action <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> actionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchActionId(org.osid.id.Id actionId, boolean match) {
        return;
    }


    /**
     *  Clears the action <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearActionIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> ActionQuery </code> is available. 
     *
     *  @return <code> true </code> if an action query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionQuery() {
        return (false);
    }


    /**
     *  Gets the query for an action. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the action query 
     *  @throws org.osid.UnimplementedException <code> supportsActionQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ActionQuery getActionQuery() {
        throw new org.osid.UnimplementedException("supportsActionQuery() is false");
    }


    /**
     *  Matches action groups with any action. 
     *
     *  @param  match <code> true </code> to match action groups with any 
     *          action, <code> false </code> to match action groups with no 
     *          actions 
     */

    @OSID @Override
    public void matchAnyAction(boolean match) {
        return;
    }


    /**
     *  Clears the action query terms. 
     */

    @OSID @Override
    public void clearActionTerms() {
        return;
    }


    /**
     *  Sets the action group <code> Id </code> for this query to match 
     *  controllers assigned to action groups. 
     *
     *  @param  actionGroupId the action group <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> actionGroupId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSystemId(org.osid.id.Id actionGroupId, boolean match) {
        return;
    }


    /**
     *  Clears the system <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSystemIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> SystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a system query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSystemQuery() {
        return (false);
    }


    /**
     *  Gets the query for a system. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the system query 
     *  @throws org.osid.UnimplementedException <code> supportsSystemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SystemQuery getSystemQuery() {
        throw new org.osid.UnimplementedException("supportsSystemQuery() is false");
    }


    /**
     *  Clears the system query terms. 
     */

    @OSID @Override
    public void clearSystemTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given action group query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an action group implementing the requested record.
     *
     *  @param actionGroupRecordType an action group record type
     *  @return the action group query record
     *  @throws org.osid.NullArgumentException
     *          <code>actionGroupRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(actionGroupRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.ActionGroupQueryRecord getActionGroupQueryRecord(org.osid.type.Type actionGroupRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.ActionGroupQueryRecord record : this.records) {
            if (record.implementsRecordType(actionGroupRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(actionGroupRecordType + " is not supported");
    }


    /**
     *  Adds a record to this action group query. 
     *
     *  @param actionGroupQueryRecord action group query record
     *  @param actionGroupRecordType actionGroup record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addActionGroupQueryRecord(org.osid.control.records.ActionGroupQueryRecord actionGroupQueryRecord, 
                                          org.osid.type.Type actionGroupRecordType) {

        addRecordType(actionGroupRecordType);
        nullarg(actionGroupQueryRecord, "action group query record");
        this.records.add(actionGroupQueryRecord);        
        return;
    }
}
