//
// FamilyFilterList.java
//
//     Implements a filtering FamilyList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.relationship.family;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filtering FamilyList.
 */

public final class FamilyFilterList
    extends net.okapia.osid.jamocha.inline.filter.relationship.family.spi.AbstractFamilyFilterList
    implements org.osid.relationship.FamilyList,
               FamilyFilter {

    private final FamilyFilter filter;


    /**
     *  Creates a new <code>FamilyFilterList</code>.
     *
     *  @param filter an inline query filter
     *  @param list a <code>FamilyList</code>
     *  @throws org.osid.NullArgumentException <code>filter</code> or
     *          <code>list</code> is <code>null</code>
     */

    public FamilyFilterList(FamilyFilter filter, org.osid.relationship.FamilyList list) {
        super(list);

        nullarg(filter, "inline query filter");
        this.filter = filter;

        return;
    }    

    
    /**
     *  Filters Families.
     *
     *  @param family the family to filter
     *  @return <code>true</code> if the family passes the filter,
     *          <code>false</code> if the family should be filtered
     */

    @Override
    public boolean pass(org.osid.relationship.Family family) {
        return (this.filter.pass(family));
    }
}
