//
// InvariantMapLocationLookupSession
//
//    Implements a Location lookup service backed by a fixed collection of
//    locations.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping;


/**
 *  Implements a Location lookup service backed by a fixed
 *  collection of locations. The locations are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapLocationLookupSession
    extends net.okapia.osid.jamocha.core.mapping.spi.AbstractMapLocationLookupSession
    implements org.osid.mapping.LocationLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapLocationLookupSession</code> with no
     *  locations.
     *  
     *  @param map the map
     *  @throws org.osid.NullArgumnetException {@code map} is
     *          {@code null}
     */

    public InvariantMapLocationLookupSession(org.osid.mapping.Map map) {
        setMap(map);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapLocationLookupSession</code> with a single
     *  location.
     *  
     *  @param map the map
     *  @param location a single location
     *  @throws org.osid.NullArgumentException {@code map} or
     *          {@code location} is <code>null</code>
     */

      public InvariantMapLocationLookupSession(org.osid.mapping.Map map,
                                               org.osid.mapping.Location location) {
        this(map);
        putLocation(location);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapLocationLookupSession</code> using an array
     *  of locations.
     *  
     *  @param map the map
     *  @param locations an array of locations
     *  @throws org.osid.NullArgumentException {@code map} or
     *          {@code locations} is <code>null</code>
     */

      public InvariantMapLocationLookupSession(org.osid.mapping.Map map,
                                               org.osid.mapping.Location[] locations) {
        this(map);
        putLocations(locations);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapLocationLookupSession</code> using a
     *  collection of locations.
     *
     *  @param map the map
     *  @param locations a collection of locations
     *  @throws org.osid.NullArgumentException {@code map} or
     *          {@code locations} is <code>null</code>
     */

      public InvariantMapLocationLookupSession(org.osid.mapping.Map map,
                                               java.util.Collection<? extends org.osid.mapping.Location> locations) {
        this(map);
        putLocations(locations);
        return;
    }
}
