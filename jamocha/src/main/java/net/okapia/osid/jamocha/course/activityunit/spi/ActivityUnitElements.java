//
// ActivityUnitElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.activityunit.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ActivityUnitElements
    extends net.okapia.osid.jamocha.spi.OperableOsidObjectElements {


    /**
     *  Gets the ActivityUnitElement Id.
     *
     *  @return the activity unit element Id
     */

    public static org.osid.id.Id getActivityUnitEntityId() {
        return (makeEntityId("osid.course.ActivityUnit"));
    }


    /**
     *  Gets the CourseId element Id.
     *
     *  @return the CourseId element Id
     */

    public static org.osid.id.Id getCourseId() {
        return (makeElementId("osid.course.activityunit.CourseId"));
    }


    /**
     *  Gets the Course element Id.
     *
     *  @return the Course element Id
     */

    public static org.osid.id.Id getCourse() {
        return (makeElementId("osid.course.activityunit.Course"));
    }


    /**
     *  Gets the TotalTargetEffort element Id.
     *
     *  @return the TotalTargetEffort element Id
     */

    public static org.osid.id.Id getTotalTargetEffort() {
        return (makeElementId("osid.course.activityunit.TotalTargetEffort"));
    }


    /**
     *  Gets the TotalTargetContactTime element Id.
     *
     *  @return the TotalTargetContactTime element Id
     */

    public static org.osid.id.Id getTotalTargetContactTime() {
        return (makeElementId("osid.course.activityunit.TotalTargetContactTime"));
    }


    /**
     *  Gets the TotalTargetIndividualEffort element Id.
     *
     *  @return the TotalTargetIndividualEffort element Id
     */

    public static org.osid.id.Id getTotalTargetIndividualEffort() {
        return (makeElementId("osid.course.activityunit.TotalTargetIndividualEffort"));
    }


    /**
     *  Gets the WeeklyEffort element Id.
     *
     *  @return the WeeklyEffort element Id
     */

    public static org.osid.id.Id getWeeklyEffort() {
        return (makeElementId("osid.course.activityunit.WeeklyEffort"));
    }


    /**
     *  Gets the WeeklyContactTime element Id.
     *
     *  @return the WeeklyContactTime element Id
     */

    public static org.osid.id.Id getWeeklyContactTime() {
        return (makeElementId("osid.course.activityunit.WeeklyContactTime"));
    }


    /**
     *  Gets the WeeklyIndividualEffort element Id.
     *
     *  @return the WeeklyIndividualEffort element Id
     */

    public static org.osid.id.Id getWeeklyIndividualEffort() {
        return (makeElementId("osid.course.activityunit.WeeklyIndividualEffort"));
    }


    /**
     *  Gets the LearningObjectiveIds element Id.
     *
     *  @return the LearningObjectiveIds element Id
     */

    public static org.osid.id.Id getLearningObjectiveIds() {
        return (makeElementId("osid.course.activityunit.LearningObjectiveIds"));
    }


    /**
     *  Gets the LearningObjectives element Id.
     *
     *  @return the LearningObjectives element Id
     */

    public static org.osid.id.Id getLearningObjectives() {
        return (makeElementId("osid.course.activityunit.LearningObjectives"));
    }


    /**
     *  Gets the Contact element Id.
     *
     *  @return the Contact element Id
     */

    public static org.osid.id.Id getContact() {
        return (makeElementId("osid.course.activityunit.Contact"));
    }


    /**
     *  Gets the RecurringWeekly element Id.
     *
     *  @return the RecurringWeekly element Id
     */

    public static org.osid.id.Id getRecurringWeekly() {
        return (makeElementId("osid.course.activityunit.RecurringWeekly"));
    }


    /**
     *  Gets the ActivityId element Id.
     *
     *  @return the ActivityId element Id
     */

    public static org.osid.id.Id getActivityId() {
        return (makeQueryElementId("osid.course.activityunit.ActivityId"));
    }


    /**
     *  Gets the Activity element Id.
     *
     *  @return the Activity element Id
     */

    public static org.osid.id.Id getActivity() {
        return (makeQueryElementId("osid.course.activityunit.Activity"));
    }


    /**
     *  Gets the CourseCatalogId element Id.
     *
     *  @return the CourseCatalogId element Id
     */

    public static org.osid.id.Id getCourseCatalogId() {
        return (makeQueryElementId("osid.course.activityunit.CourseCatalogId"));
    }


    /**
     *  Gets the CourseCatalog element Id.
     *
     *  @return the CourseCatalog element Id
     */

    public static org.osid.id.Id getCourseCatalog() {
        return (makeQueryElementId("osid.course.activityunit.CourseCatalog"));
    }
}
