//
// AbstractWarehouseQueryInspector.java
//
//     A template for making a WarehouseQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.warehouse.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for warehouses.
 */

public abstract class AbstractWarehouseQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.inventory.WarehouseQueryInspector {

    private final java.util.Collection<org.osid.inventory.records.WarehouseQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the item <code> Id </code> query terms. 
     *
     *  @return the item <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getItemIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the item query terms. 
     *
     *  @return the item query terms 
     */

    @OSID @Override
    public org.osid.inventory.ItemQueryInspector[] getItemTerms() {
        return (new org.osid.inventory.ItemQueryInspector[0]);
    }


    /**
     *  Gets the stock <code> Id </code> query terms. 
     *
     *  @return the stock <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStockIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the stock query terms. 
     *
     *  @return the stock query terms 
     */

    @OSID @Override
    public org.osid.inventory.StockQueryInspector[] getStockTerms() {
        return (new org.osid.inventory.StockQueryInspector[0]);
    }


    /**
     *  Gets the model <code> Id </code> query terms. 
     *
     *  @return the model <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getModelIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the model query terms. 
     *
     *  @return the model query terms 
     */

    @OSID @Override
    public org.osid.inventory.ModelQueryInspector[] getModelTerms() {
        return (new org.osid.inventory.ModelQueryInspector[0]);
    }


    /**
     *  Gets the inventory <code> Id </code> query terms. 
     *
     *  @return the inventory <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getInventoryIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the inventory query terms. 
     *
     *  @return the inventory query terms 
     */

    @OSID @Override
    public org.osid.inventory.InventoryQueryInspector[] getInventoryTerms() {
        return (new org.osid.inventory.InventoryQueryInspector[0]);
    }


    /**
     *  Gets the ancestor warehouse <code> Id </code> query terms. 
     *
     *  @return the ancestor warehouse <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorWarehouseIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor warehouse query terms. 
     *
     *  @return the ancestor warehouse terms 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseQueryInspector[] getAncestorWarehouseTerms() {
        return (new org.osid.inventory.WarehouseQueryInspector[0]);
    }


    /**
     *  Gets the descendant warehouse <code> Id </code> query terms. 
     *
     *  @return the descendant warehouse <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantWarehouseIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant warehouse query terms. 
     *
     *  @return the descendant warehouse terms 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseQueryInspector[] getDescendantWarehouseTerms() {
        return (new org.osid.inventory.WarehouseQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given warehouse query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a warehouse implementing the requested record.
     *
     *  @param warehouseRecordType a warehouse record type
     *  @return the warehouse query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>warehouseRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(warehouseRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.records.WarehouseQueryInspectorRecord getWarehouseQueryInspectorRecord(org.osid.type.Type warehouseRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inventory.records.WarehouseQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(warehouseRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(warehouseRecordType + " is not supported");
    }


    /**
     *  Adds a record to this warehouse query. 
     *
     *  @param warehouseQueryInspectorRecord warehouse query inspector
     *         record
     *  @param warehouseRecordType warehouse record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addWarehouseQueryInspectorRecord(org.osid.inventory.records.WarehouseQueryInspectorRecord warehouseQueryInspectorRecord, 
                                                   org.osid.type.Type warehouseRecordType) {

        addRecordType(warehouseRecordType);
        nullarg(warehouseRecordType, "warehouse record type");
        this.records.add(warehouseQueryInspectorRecord);        
        return;
    }
}
