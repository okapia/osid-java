//
// SessionProxy
//
//     A means to proxy session management information through fixed
//     implementation classes in the toolkit.
//
//
// Tom Coppeto
// Okapia
// 10 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha;

import static net.okapia.osid.util.MethodCheck.nullarg;


/**
 *  The various OSID Session implementations in the toolkit can accept
 *  a <code>SessionProxy</code> to handle the session management
 *  information such as locale or authenticated entities. Generally,
 *  OSID session behaviors should be handled by subclassing the
 *  abstract classes in this toolkit however this class provides an
 *  alternative way to customize these aspects OSID Session
 *  implementations using the fixed implemnetations.
 */


public class SessionProxy {

    private org.osid.locale.Locale locale;
    private org.osid.authentication.Agent agent;
    private Authenticator authenticator;


    /**
     *  Creates a new <code>SessionProxy</code>.
     *
     *  @param locale the locale returned from
     *         <code>OsidSession.getLocale()</code>
     *  @throws org.osid.NullArgumentException <code>locale</code> is
     *          <code>null</code>
     */

    public SessionProxy(org.osid.locale.Locale locale) { 
        nullarg(locale, "locale");
	this.locale = locale;
	return;
    }


    /**
     *  Creates a new <code>SessionProxy</code>.
     *
     *  @param agent the agent that should be returned from
     *         <code>OsidSession.getAuthenticatedAgents()</code>
     */

    public SessionProxy(org.osid.authentication.Agent agent) {
        nullarg(agent, "agent");
	this.agent = agent;
	return;
    }


    /**
     *  Creates a new <code>SessionProxy</code>.
     *
     *  @param locale the locale returned from
     *         <code>OsidSession.getLocale()</code>
     *  @param agent the agent that should be returned from
     *         <code>OsidSession.getAuthenticatedAgents()</code>
     *  @param 
     */

    public SessionProxy(org.osid.locale.Locale locale, org.osid.authentication.Agent agent) {
        nullarg(locale, "locale");
        nullarg(agent, "agent");

	this.locale = locale;
	this.agent = agent;

	return;
    }


    /**
     *  Creates a new <code>SessionProxy</code>.
     *
     *  @param authenticator a callback used by the
     *         <code>AbstractOsidSession</code> to access
     *         authentication information
     */

    public SessionProxy(Authenticator authenticator) {
        nullarg(authenticator, "authenticator");
	this.authenticator = authenticator;
	return;
    }


    /**
     *  Creates a new <code>SessionProxy</code>.
     *
     *  @param locale the locale returned from
     *         <code>OsidSession.getLocale()</code>
     *  @param authenticator a callback used by the
     *         <code>AbstractOsidSession</code> to access
     *         authentication information
     */

    public SessionProxy(org.osid.locale.Locale locale, Authenticator authenticator) {
        nullarg(locale, "locale");
        nullarg(authenticator, "authenticator");

	this.languageType  = languageType;
	this.authenticator = authenticator;

	return;
    }

    
    /**
     *  Gets the locale in this proxy.
     *
     *  @return the locale
     */

    protected org.osid.type.Type getLocale() {
	return (this.locale);
    }


    /**
     *  Gets the agent set in this proxy.
     *
     *  @return the agent set
     */
    
    protected org.osid.authentication.Agent getAgent() {
	return (this.agent);
    }

    
    /**
     *  Gets the authenticator in this proxy.
     *
     *  @return the authenticator set
     */

    protected Authenticator getAuthenticator() {
	return (this.authenticator);
    }
}