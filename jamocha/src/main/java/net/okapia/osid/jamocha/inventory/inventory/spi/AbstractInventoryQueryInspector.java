//
// AbstractInventoryQueryInspector.java
//
//     A template for making an InventoryQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.inventory.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for inventories.
 */

public abstract class AbstractInventoryQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.inventory.InventoryQueryInspector {

    private final java.util.Collection<org.osid.inventory.records.InventoryQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the stock <code> Id </code> query terms. 
     *
     *  @return the stock <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStockIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the stock query terms. 
     *
     *  @return the stock query terms 
     */

    @OSID @Override
    public org.osid.inventory.StockQueryInspector[] getStockTerms() {
        return (new org.osid.inventory.StockQueryInspector[0]);
    }


    /**
     *  Gets the date query terms. 
     *
     *  @return the date query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeTerm[] getDateTerms() {
        return (new org.osid.search.terms.DateTimeTerm[0]);
    }


    /**
     *  Gets the date query terms. 
     *
     *  @return the date range query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getDateInclusiveTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the quantity terms. 
     *
     *  @return the quantity query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getQuantityTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the warehouse <code> Id </code> query terms. 
     *
     *  @return the warehouse <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getWarehouseIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the warehouse query terms. 
     *
     *  @return the warehouse query terms 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseQueryInspector[] getWarehouseTerms() {
        return (new org.osid.inventory.WarehouseQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given inventory query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an inventory implementing the requested record.
     *
     *  @param inventoryRecordType an inventory record type
     *  @return the inventory query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>inventoryRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(inventoryRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.records.InventoryQueryInspectorRecord getInventoryQueryInspectorRecord(org.osid.type.Type inventoryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inventory.records.InventoryQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(inventoryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(inventoryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this inventory query. 
     *
     *  @param inventoryQueryInspectorRecord inventory query inspector
     *         record
     *  @param inventoryRecordType inventory record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addInventoryQueryInspectorRecord(org.osid.inventory.records.InventoryQueryInspectorRecord inventoryQueryInspectorRecord, 
                                                   org.osid.type.Type inventoryRecordType) {

        addRecordType(inventoryRecordType);
        nullarg(inventoryRecordType, "inventory record type");
        this.records.add(inventoryQueryInspectorRecord);        
        return;
    }
}
