//
// AbstractRecurringEventQueryInspector.java
//
//     A template for making a RecurringEventQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.recurringevent.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for recurring events.
 */

public abstract class AbstractRecurringEventQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleQueryInspector
    implements org.osid.calendaring.RecurringEventQueryInspector {

    private final java.util.Collection<org.osid.calendaring.records.RecurringEventQueryInspectorRecord> records = new java.util.ArrayList<>();
    private final OsidContainableQueryInspector inspector = new OsidContainableQueryInspector();

    
    /**
     *  Gets the sequestered query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getSequesteredTerms() {
        return (this.inspector.getSequesteredTerms());
    }


    /**
     *  Gets the schedule <code> Id </code> terms. 
     *
     *  @return the schedule <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getScheduleIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the schedule terms. 
     *
     *  @return the schedule terms 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleQueryInspector[] getScheduleTerms() {
        return (new org.osid.calendaring.ScheduleQueryInspector[0]);
    }


    /**
     *  Gets the superseding event <code> Id </code> terms. 
     *
     *  @return the superseding event <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSupersedingEventIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the superseding event terms. 
     *
     *  @return the superseding event terms 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventQueryInspector[] getSupersedingEventTerms() {
        return (new org.osid.calendaring.SupersedingEventQueryInspector[0]);
    }


    /**
     *  Gets the event <code> Id </code> terms. 
     *
     *  @return the event <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEventIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the event terms. 
     *
     *  @return the event terms 
     */

    @OSID @Override
    public org.osid.calendaring.EventQueryInspector[] getEventTerms() {
        return (new org.osid.calendaring.EventQueryInspector[0]);
    }


    /**
     *  Gets the blackout terms. 
     *
     *  @return the time terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeTerm[] getBlackoutTerms() {
        return (new org.osid.search.terms.DateTimeTerm[0]);
    }


    /**
     *  Gets the inclusive blackout terms. 
     *
     *  @return the time range terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getBlackoutInclusiveTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the sponsor <code> Id </code> terms. 
     *
     *  @return the sponsor <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSponsorIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the sponsor terms. 
     *
     *  @return the sponsor terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getSponsorTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the date terms. 
     *
     *  @return the time terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeTerm[] getSpecificMeetingTimeTerms() {
        return (new org.osid.search.terms.DateTimeTerm[0]);
    }


    /**
     *  Gets the calendar <code> Id </code> terms. 
     *
     *  @return the calendar <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCalendarIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the calendar terms. 
     *
     *  @return the calendar terms 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQueryInspector[] getCalendarTerms() {
        return (new org.osid.calendaring.CalendarQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given recurring event query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a recurring event implementing the requested record.
     *
     *  @param recurringEventRecordType a recurring event record type
     *  @return the recurring event query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(recurringEventRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.RecurringEventQueryInspectorRecord getRecurringEventQueryInspectorRecord(org.osid.type.Type recurringEventRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.RecurringEventQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(recurringEventRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(recurringEventRecordType + " is not supported");
    }


    /**
     *  Adds a record to this recurring event query. 
     *
     *  @param recurringEventQueryInspectorRecord recurring event query inspector
     *         record
     *  @param recurringEventRecordType recurringEvent record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRecurringEventQueryInspectorRecord(org.osid.calendaring.records.RecurringEventQueryInspectorRecord recurringEventQueryInspectorRecord, 
                                                   org.osid.type.Type recurringEventRecordType) {

        addRecordType(recurringEventRecordType);
        nullarg(recurringEventRecordType, "recurring event record type");
        this.records.add(recurringEventQueryInspectorRecord);        
        return;
    }


    protected class OsidContainableQueryInspector
        extends net.okapia.osid.jamocha.spi.AbstractOsidContainableQueryInspector
        implements org.osid.OsidContainableQueryInspector {
    }
}
