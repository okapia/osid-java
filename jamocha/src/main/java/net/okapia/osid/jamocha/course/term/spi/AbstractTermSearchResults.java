//
// AbstractTermSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.term.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractTermSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.course.TermSearchResults {

    private org.osid.course.TermList terms;
    private final org.osid.course.TermQueryInspector inspector;
    private final java.util.Collection<org.osid.course.records.TermSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractTermSearchResults.
     *
     *  @param terms the result set
     *  @param termQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>terms</code>
     *          or <code>termQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractTermSearchResults(org.osid.course.TermList terms,
                                            org.osid.course.TermQueryInspector termQueryInspector) {
        nullarg(terms, "terms");
        nullarg(termQueryInspector, "term query inspectpr");

        this.terms = terms;
        this.inspector = termQueryInspector;

        return;
    }


    /**
     *  Gets the term list resulting from a search.
     *
     *  @return a term list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.course.TermList getTerms() {
        if (this.terms == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.course.TermList terms = this.terms;
        this.terms = null;
	return (terms);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.course.TermQueryInspector getTermQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  term search record <code> Type. </code> This method must
     *  be used to retrieve a term implementing the requested
     *  record.
     *
     *  @param termSearchRecordType a term search 
     *         record type 
     *  @return the term search
     *  @throws org.osid.NullArgumentException
     *          <code>termSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(termSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.TermSearchResultsRecord getTermSearchResultsRecord(org.osid.type.Type termSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.course.records.TermSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(termSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(termSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record term search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addTermRecord(org.osid.course.records.TermSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "term record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
