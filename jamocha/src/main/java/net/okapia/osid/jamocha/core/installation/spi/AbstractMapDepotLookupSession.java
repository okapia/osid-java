//
// AbstractMapDepotLookupSession
//
//    A simple framework for providing a Depot lookup service
//    backed by a fixed collection of depots.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.installation.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Depot lookup service backed by a
 *  fixed collection of depots. The depots are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Depots</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapDepotLookupSession
    extends net.okapia.osid.jamocha.installation.spi.AbstractDepotLookupSession
    implements org.osid.installation.DepotLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.installation.Depot> depots = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.installation.Depot>());


    /**
     *  Makes a <code>Depot</code> available in this session.
     *
     *  @param  depot a depot
     *  @throws org.osid.NullArgumentException <code>depot<code>
     *          is <code>null</code>
     */

    protected void putDepot(org.osid.installation.Depot depot) {
        this.depots.put(depot.getId(), depot);
        return;
    }


    /**
     *  Makes an array of depots available in this session.
     *
     *  @param  depots an array of depots
     *  @throws org.osid.NullArgumentException <code>depots<code>
     *          is <code>null</code>
     */

    protected void putDepots(org.osid.installation.Depot[] depots) {
        putDepots(java.util.Arrays.asList(depots));
        return;
    }


    /**
     *  Makes a collection of depots available in this session.
     *
     *  @param  depots a collection of depots
     *  @throws org.osid.NullArgumentException <code>depots<code>
     *          is <code>null</code>
     */

    protected void putDepots(java.util.Collection<? extends org.osid.installation.Depot> depots) {
        for (org.osid.installation.Depot depot : depots) {
            this.depots.put(depot.getId(), depot);
        }

        return;
    }


    /**
     *  Removes a Depot from this session.
     *
     *  @param  depotId the <code>Id</code> of the depot
     *  @throws org.osid.NullArgumentException <code>depotId<code> is
     *          <code>null</code>
     */

    protected void removeDepot(org.osid.id.Id depotId) {
        this.depots.remove(depotId);
        return;
    }


    /**
     *  Gets the <code>Depot</code> specified by its <code>Id</code>.
     *
     *  @param  depotId <code>Id</code> of the <code>Depot</code>
     *  @return the depot
     *  @throws org.osid.NotFoundException <code>depotId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>depotId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.Depot getDepot(org.osid.id.Id depotId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.installation.Depot depot = this.depots.get(depotId);
        if (depot == null) {
            throw new org.osid.NotFoundException("depot not found: " + depotId);
        }

        return (depot);
    }


    /**
     *  Gets all <code>Depots</code>. In plenary mode, the returned
     *  list contains all known depots or an error
     *  results. Otherwise, the returned list may contain only those
     *  depots that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Depots</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.DepotList getDepots()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.installation.depot.ArrayDepotList(this.depots.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.depots.clear();
        super.close();
        return;
    }
}
