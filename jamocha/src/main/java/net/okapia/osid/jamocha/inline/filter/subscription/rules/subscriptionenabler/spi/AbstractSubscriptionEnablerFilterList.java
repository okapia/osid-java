//
// AbstractSubscriptionEnablerList
//
//     Implements a filter for a SubscriptionEnablerList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.subscription.rules.subscriptionenabler.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for a SubscriptionEnablerList. Subclasses call this
 *  constructor and implement the <code>pass()</code> method. This
 *  filter is synchronous but can be wrapped in a BufferedSubscriptionEnablerList
 *  to improve performance.
 */

public abstract class AbstractSubscriptionEnablerFilterList
    extends net.okapia.osid.jamocha.subscription.rules.subscriptionenabler.spi.AbstractSubscriptionEnablerList
    implements org.osid.subscription.rules.SubscriptionEnablerList,
               net.okapia.osid.jamocha.inline.filter.subscription.rules.subscriptionenabler.SubscriptionEnablerFilter {

    private org.osid.subscription.rules.SubscriptionEnabler subscriptionEnabler;
    private final org.osid.subscription.rules.SubscriptionEnablerList list;
    private org.osid.OsidException error;


    /**
     *  Creates a new <code>AbstractSubscriptionEnablerFilterList</code>.
     *
     *  @param subscriptionEnablerList a <code>SubscriptionEnablerList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionEnablerList</code> is <code>null</code>
     */

    protected AbstractSubscriptionEnablerFilterList(org.osid.subscription.rules.SubscriptionEnablerList subscriptionEnablerList) {
        nullarg(subscriptionEnablerList, "subscription enabler list");
        this.list = subscriptionEnablerList;
        return;
    }    

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in
     *          this list, <code> false </code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (hasError()) {
            return (true);
        }

        prime();

        if (this.subscriptionEnabler == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code> SubscriptionEnabler </code> in this list. 
     *
     *  @return the next <code> SubscriptionEnabler </code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code> SubscriptionEnabler </code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnabler getNextSubscriptionEnabler()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        if (hasNext()) {
            org.osid.subscription.rules.SubscriptionEnabler subscriptionEnabler = this.subscriptionEnabler;
            this.subscriptionEnabler = null;
            return (subscriptionEnabler);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in subscription enabler list");
        }
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.subscriptionEnabler = null;
        this.list.close();
        return;
    }

    
    /**
     *  Filters SubscriptionEnablers.
     *
     *  @param subscriptionEnabler the subscription enabler to filter
     *  @return <code>true</code> if the subscription enabler passes the filter,
     *          <code>false</code> if the subscription enabler should be filtered
     */

    public abstract boolean pass(org.osid.subscription.rules.SubscriptionEnabler subscriptionEnabler);


    protected void prime() {
        if (this.subscriptionEnabler != null) {
            return;
        }

        org.osid.subscription.rules.SubscriptionEnabler subscriptionEnabler = null;

        while (this.list.hasNext()) {
            try {
                subscriptionEnabler = this.list.getNextSubscriptionEnabler();
            } catch (org.osid.OsidException oe) {
                error(oe);
                return;
            }

            if (pass(subscriptionEnabler)) {
                this.subscriptionEnabler = subscriptionEnabler;
                return;
            }
        }

        return;
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    protected void error(org.osid.OsidException error) {
        this.error = error;
        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    protected boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }
}
