//
// AbstractCardinalMetadata.java
//
//     Defines a cardinal Metadata.
//
//
// Tom Coppeto
// Okapia
// 15 March 2013
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metadata.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a cardinal Metadata.
 */

public abstract class AbstractCardinalMetadata
    extends AbstractMetadata
    implements org.osid.Metadata {

    private long minimum = 0;
    private long maximum = Long.MAX_VALUE;
    private final java.util.Collection<Long> set      = new java.util.LinkedHashSet<>();
    private final java.util.Collection<Long> defvals  = new java.util.LinkedHashSet<>();
    private final java.util.Collection<Long> existing = new java.util.LinkedHashSet<>();


    /**
     *  Constructs a new {@code AbstractCardinalMetadata}.
     *
     *  @param elementId the Id of the element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    protected AbstractCardinalMetadata(org.osid.id.Id elementId) {
        super(org.osid.Syntax.CARDINAL, elementId);
        return;
    }


    /**
     *  Constructs a new {@code AbstractCardinalMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @param isLinked {@code true} if the element is linked to
     *         another element, {@code false} otherwise
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    protected AbstractCardinalMetadata(org.osid.id.Id elementId, boolean isArray, boolean isLinked) {
        super(org.osid.Syntax.CARDINAL, elementId, isArray, isLinked);
        return;
    }


    /**
     *  Gets the minimum cardinal value. 
     *
     *  @return the minimum cardinal 
     *  @throws org.osid.IllegalStateException syntax is not a <code> CARDINAL 
     *          </code> 
     */

    @OSID @Override
    public long getMinimumCardinal() {
        return (this.minimum);
    }


    /**
     *  Gets the maximum cardinal value. 
     *
     *  @return the maximum cardinal 
     *  @throws org.osid.IllegalStateException syntax is not a <code> CARDINAL 
     *          </code> 
     */

    @OSID @Override
    public long getMaximumCardinal() {
        return (this.maximum);
    }

    
    /**
     *  Sets the cardinal range.
     *
     *  @param min the minimum value
     *  @param max the maximum value
     *  @throws org.osid.InvalidArgumentException {@code min} is
     *          greater than {@code max} or, {@code min} or {@code
     *          max} is negative
     */

    protected void setCardinalRange(long min, long max) {
        cardinalarg(min, "min");
        cardinalarg(max, "max");

        if (min > max) {
            throw new org.osid.InvalidArgumentException("min is greater than max");
        }

        this.minimum = min;
        this.maximum = max;

        return;
    }


    /**
     *  Gets the set of acceptable cardinal values. 
     *
     *  @return a set of cardinals or an empty array if not restricted 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          CARDINAL </code>
     */

    @OSID @Override
    public long[] getCardinalSet() {
        Long[] longs = this.set.toArray(new Long[this.set.size()]);
        long[] ret = new long[longs.length];
        for (int i = 0; i < longs.length; i++) {
            ret[i] = longs[i];
        }

        return (ret);
    }

    
    /**
     *  Sets the cardinal set.
     *
     *  @param values a collection of accepted cardinal values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setCardinalSet(java.util.Collection<Long> values) {
        clearCardinalSet();
        addToCardinalSet(values);
        return;
    }


    /**
     *  Adds a collection of values to the cardinal set.
     *
     *  @param values a collection of accepted cardinal values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addToCardinalSet(java.util.Collection<Long> values) {
        nullarg(values, "cardinal set");
        
        for (long value : values) {
            addToCardinalSet(value);
        }

        return;
    }


    /**
     *  Adds a value to the cardinal set.
     *
     *  @param value a cardinal value
     *  @throws org.osid.InvalidArgumentException value is negative
     */

    protected void addToCardinalSet(long value) {
        cardinalarg(value, "cardinal value");
        this.set.add(value);
        return;
    }


    /**
     *  Removes a value from the cardinal set.
     *
     *  @param value a cardinal value
     *  @throws org.osid.InvalidArgumentException value is negative
     */

    protected void removeFromCardinalSet(long value) {
        cardinalarg(value, "cardinal value");
        this.set.remove(value);
        return;
    }


    /**
     *  Clears the cardinal set.
     */

    protected void clearCardinalSet() {
        this.set.clear();
        return;
    }


    /**
     *  Gets the default cardinal values. These are the values used if
     *  the element value is not provided or is cleared. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the default cardinal values 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          CARDINAL </code> or <code> isRequired() </code> is
     *          <code> true </code>
     */

    @OSID @Override
    public long[] getDefaultCardinalValues() {
        Long[] longs = this.set.toArray(new Long[this.defvals.size()]);
        long[] ret = new long[longs.length];
        for (int i = 0; i < longs.length; i++) {
            ret[i] = longs[i];
        }

        return (ret);
    }


    /**
     *  Values the default cardinal values.
     *
     *  @param values a collection of default cardinal values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setDefaultCardinalValues(java.util.Collection<Long> values) {
        clearDefaultCardinalValues();
        addDefaultCardinalValues(values);
        return;
    }


    /**
     *  Adds a collection of default cardinal values.
     *
     *  @param values a collection of default cardinal values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addDefaultCardinalValues(java.util.Collection<Long> values) {
        nullarg(values, "default cardinal values");
        
        for (long value : values) {
            addDefaultCardinalValue(value);
        }

        return;
    }


    /**
     *  Adds a default cardinal value.
     *
     *  @param value a cardinal value
     *  @throws org.osid.InvalidArgumentException value is negative
     */

    protected void addDefaultCardinalValue(long value) {
        cardinalarg(value, "default cardinal value");
        this.defvals.add(value);
        return;
    }


    /**
     *  Removes a default cardinal value.
     *
     *  @param value a cardinal value
     *  @throws org.osid.InvalidArgumentException value is negative
     */

    protected void removeDefaultCardinalValue(long value) {
        cardinalarg(value, "default cardinal value");
        this.defvals.remove(value);
        return;
    }


    /**
     *  Clears the default cardinal values.
     */

    protected void clearDefaultCardinalValues() {
        this.defvals.clear();
        return;
    }


    /**
     *  Gets the existing cardinal values. If <code> hasValue()
     *  </code> and <code> isRequired() </code> are <code> false,
     *  </code> then these values are the default values. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the existing cardinal values 
     *  @throws org.osid.IllegalStateException syntax is not a <code> CARDINAL 
     *          </code> or <code> isValueKnown() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public long[] getExistingCardinalValues() {
        Long[] longs = this.existing.toArray(new Long[this.existing.size()]);
        long[] ret = new long[longs.length];
        for (int i = 0; i < longs.length; i++) {
            ret[i] = longs[i];
        }

        return (ret);
    }


    /**
     *  Valuess the existing cardinal values.
     *
     *  @param values a collection of existing cardinal values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setExistingCardinalValues(java.util.Collection<Long> values) {
        clearExistingCardinalValues();
        addExistingCardinalValues(values);
        return;
    }


    /**
     *  Adds a collection of existing cardinal values.
     *
     *  @param values a collection of existing cardinal values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addExistingCardinalValues(java.util.Collection<Long> values) {
        nullarg(values, "existing cardinal values");
        
        for (long value : values) {
            addExistingCardinalValue(value);
        }

        return;
    }


    /**
     *  Adds a existing cardinal value.
     *
     *  @param value a cardinal value
     *  @throws org.osid.InvalidArgumentException value is negative
     */

    protected void addExistingCardinalValue(long value) {
        cardinalarg(value, "existing cardinal value");

        this.existing.add(value);
        setValueKnown(true);

        return;
    }


    /**
     *  Removes a existing cardinal value.
     *
     *  @param value a cardinal value
     *  @throws org.osid.InvalidArgumentException value is negative
     */

    protected void removeExistingCardinalValue(long value) {
        cardinalarg(value, "existing cardinal value");
        this.existing.remove(value);
        return;
    }


    /**
     *  Clears the existing cardinal values.
     */

    protected void clearExistingCardinalValues() {
        this.existing.clear();
        setValueKnown(false);
        return;
    }
}