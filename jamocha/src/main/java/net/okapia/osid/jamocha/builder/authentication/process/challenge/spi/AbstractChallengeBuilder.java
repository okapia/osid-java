//
// AbstractChallenge.java
//
//     Defines a Challenge builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.authentication.process.challenge.spi;


/**
 *  Defines a <code>Challenge</code> builder.
 */

public abstract class AbstractChallengeBuilder<T extends AbstractChallengeBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.authentication.process.challenge.ChallengeMiter challenge;


    /**
     *  Constructs a new <code>AbstractChallengeBuilder</code>.
     *
     *  @param challenge the challenge to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractChallengeBuilder(net.okapia.osid.jamocha.builder.authentication.process.challenge.ChallengeMiter challenge) {
        super(challenge);
        this.challenge = challenge;
        return;
    }


    /**
     *  Builds the challenge.
     *
     *  @return the new challenge
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.authentication.process.Challenge build() {
        (new net.okapia.osid.jamocha.builder.validator.authentication.process.challenge.ChallengeValidator(getValidations())).validate(this.challenge);
        return (new net.okapia.osid.jamocha.builder.authentication.process.challenge.ImmutableChallenge(this.challenge));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the challenge miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.authentication.process.challenge.ChallengeMiter getMiter() {
        return (this.challenge);
    }


    /**
     *  Adds a Challenge record.
     *
     *  @param record a challenge record
     *  @param recordType the type of challenge record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.authentication.process.records.ChallengeRecord record, org.osid.type.Type recordType) {
        getMiter().addChallengeRecord(record, recordType);
        return (self());
    }
}       


