//
// MutableMapRequestLookupSession
//
//    Implements a Request lookup service backed by a collection of
//    requests that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning;


/**
 *  Implements a Request lookup service backed by a collection of
 *  requests. The requests are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of requests can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapRequestLookupSession
    extends net.okapia.osid.jamocha.core.provisioning.spi.AbstractMapRequestLookupSession
    implements org.osid.provisioning.RequestLookupSession {


    /**
     *  Constructs a new {@code MutableMapRequestLookupSession}
     *  with no requests.
     *
     *  @param distributor the distributor
     *  @throws org.osid.NullArgumentException {@code distributor} is
     *          {@code null}
     */

      public MutableMapRequestLookupSession(org.osid.provisioning.Distributor distributor) {
        setDistributor(distributor);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapRequestLookupSession} with a
     *  single request.
     *
     *  @param distributor the distributor  
     *  @param request a request
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code request} is {@code null}
     */

    public MutableMapRequestLookupSession(org.osid.provisioning.Distributor distributor,
                                           org.osid.provisioning.Request request) {
        this(distributor);
        putRequest(request);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapRequestLookupSession}
     *  using an array of requests.
     *
     *  @param distributor the distributor
     *  @param requests an array of requests
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code requests} is {@code null}
     */

    public MutableMapRequestLookupSession(org.osid.provisioning.Distributor distributor,
                                           org.osid.provisioning.Request[] requests) {
        this(distributor);
        putRequests(requests);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapRequestLookupSession}
     *  using a collection of requests.
     *
     *  @param distributor the distributor
     *  @param requests a collection of requests
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code requests} is {@code null}
     */

    public MutableMapRequestLookupSession(org.osid.provisioning.Distributor distributor,
                                           java.util.Collection<? extends org.osid.provisioning.Request> requests) {

        this(distributor);
        putRequests(requests);
        return;
    }

    
    /**
     *  Makes a {@code Request} available in this session.
     *
     *  @param request a request
     *  @throws org.osid.NullArgumentException {@code request{@code  is
     *          {@code null}
     */

    @Override
    public void putRequest(org.osid.provisioning.Request request) {
        super.putRequest(request);
        return;
    }


    /**
     *  Makes an array of requests available in this session.
     *
     *  @param requests an array of requests
     *  @throws org.osid.NullArgumentException {@code requests{@code 
     *          is {@code null}
     */

    @Override
    public void putRequests(org.osid.provisioning.Request[] requests) {
        super.putRequests(requests);
        return;
    }


    /**
     *  Makes collection of requests available in this session.
     *
     *  @param requests a collection of requests
     *  @throws org.osid.NullArgumentException {@code requests{@code  is
     *          {@code null}
     */

    @Override
    public void putRequests(java.util.Collection<? extends org.osid.provisioning.Request> requests) {
        super.putRequests(requests);
        return;
    }


    /**
     *  Removes a Request from this session.
     *
     *  @param requestId the {@code Id} of the request
     *  @throws org.osid.NullArgumentException {@code requestId{@code 
     *          is {@code null}
     */

    @Override
    public void removeRequest(org.osid.id.Id requestId) {
        super.removeRequest(requestId);
        return;
    }    
}
