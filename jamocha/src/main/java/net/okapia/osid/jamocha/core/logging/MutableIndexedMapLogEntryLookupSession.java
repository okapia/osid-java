//
// MutableIndexedMapLogEntryLookupSession
//
//    Implements a LogEntry lookup service backed by a collection of
//    logEntries indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.logging;


/**
 *  Implements a LogEntry lookup service backed by a collection of
 *  log entries. The log entries are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some log entries may be compatible
 *  with more types than are indicated through these log entry
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of log entries can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapLogEntryLookupSession
    extends net.okapia.osid.jamocha.core.logging.spi.AbstractIndexedMapLogEntryLookupSession
    implements org.osid.logging.LogEntryLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapLogEntryLookupSession} with no log entries.
     *
     *  @param log the log
     *  @throws org.osid.NullArgumentException {@code log}
     *          is {@code null}
     */

      public MutableIndexedMapLogEntryLookupSession(org.osid.logging.Log log) {
        setLog(log);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapLogEntryLookupSession} with a
     *  single log entry.
     *  
     *  @param log the log
     *  @param  logEntry a single logEntry
     *  @throws org.osid.NullArgumentException {@code log} or
     *          {@code logEntry} is {@code null}
     */

    public MutableIndexedMapLogEntryLookupSession(org.osid.logging.Log log,
                                                  org.osid.logging.LogEntry logEntry) {
        this(log);
        putLogEntry(logEntry);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapLogEntryLookupSession} using an
     *  array of log entries.
     *
     *  @param log the log
     *  @param  logEntries an array of log entries
     *  @throws org.osid.NullArgumentException {@code log} or
     *          {@code logEntries} is {@code null}
     */

    public MutableIndexedMapLogEntryLookupSession(org.osid.logging.Log log,
                                                  org.osid.logging.LogEntry[] logEntries) {
        this(log);
        putLogEntries(logEntries);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapLogEntryLookupSession} using a
     *  collection of log entries.
     *
     *  @param log the log
     *  @param  logEntries a collection of log entries
     *  @throws org.osid.NullArgumentException {@code log} or
     *          {@code logEntries} is {@code null}
     */

    public MutableIndexedMapLogEntryLookupSession(org.osid.logging.Log log,
                                                  java.util.Collection<? extends org.osid.logging.LogEntry> logEntries) {

        this(log);
        putLogEntries(logEntries);
        return;
    }
    

    /**
     *  Makes a {@code LogEntry} available in this session.
     *
     *  @param  logEntry a log entry
     *  @throws org.osid.NullArgumentException {@code logEntry{@code  is
     *          {@code null}
     */

    @Override
    public void putLogEntry(org.osid.logging.LogEntry logEntry) {
        super.putLogEntry(logEntry);
        return;
    }


    /**
     *  Makes an array of log entries available in this session.
     *
     *  @param  logEntries an array of log entries
     *  @throws org.osid.NullArgumentException {@code logEntries{@code 
     *          is {@code null}
     */

    @Override
    public void putLogEntries(org.osid.logging.LogEntry[] logEntries) {
        super.putLogEntries(logEntries);
        return;
    }


    /**
     *  Makes collection of log entries available in this session.
     *
     *  @param  logEntries a collection of log entries
     *  @throws org.osid.NullArgumentException {@code logEntry{@code  is
     *          {@code null}
     */

    @Override
    public void putLogEntries(java.util.Collection<? extends org.osid.logging.LogEntry> logEntries) {
        super.putLogEntries(logEntries);
        return;
    }


    /**
     *  Removes a LogEntry from this session.
     *
     *  @param logEntryId the {@code Id} of the log entry
     *  @throws org.osid.NullArgumentException {@code logEntryId{@code  is
     *          {@code null}
     */

    @Override
    public void removeLogEntry(org.osid.id.Id logEntryId) {
        super.removeLogEntry(logEntryId);
        return;
    }    
}
