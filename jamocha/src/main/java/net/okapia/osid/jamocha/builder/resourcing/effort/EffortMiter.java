//
// EffortMiter.java
//
//     Defines an Effort miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.resourcing.effort;


/**
 *  Defines an <code>Effort</code> miter for use with the builders.
 */

public interface EffortMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRelationshipMiter,
            org.osid.resourcing.Effort {


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    public void setResource(org.osid.resource.Resource resource);


    /**
     *  Sets the commission.
     *
     *  @param commission a commission
     *  @throws org.osid.NullArgumentException <code>commission</code>
     *          is <code>null</code>
     */

    public void setCommission(org.osid.resourcing.Commission commission);


    /**
     *  Sets the time spent.
     *
     *  @param timeSpent a time spent
     *  @throws org.osid.NullArgumentException <code>timeSpent</code>
     *          is <code>null</code>
     */

    public void setTimeSpent(org.osid.calendaring.Duration timeSpent);


    /**
     *  Adds an Effort record.
     *
     *  @param record an effort record
     *  @param recordType the type of effort record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addEffortRecord(org.osid.resourcing.records.EffortRecord record, org.osid.type.Type recordType);
}       


