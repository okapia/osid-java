//
// AbstractQueryDirectoryLookupSession.java
//
//    An inline adapter that maps a DirectoryLookupSession to
//    a DirectoryQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filing.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a DirectoryLookupSession to
 *  a DirectoryQuerySession.
 */

public abstract class AbstractQueryDirectoryLookupSession
    extends net.okapia.osid.jamocha.filing.spi.AbstractDirectoryLookupSession
    implements org.osid.filing.DirectoryLookupSession {

    private final org.osid.filing.DirectoryQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryDirectoryLookupSession.
     *
     *  @param querySession the underlying directory query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryDirectoryLookupSession(org.osid.filing.DirectoryQuerySession querySession) {
        nullarg(querySession, "directory query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Directory</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Directory Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDirectoryId() {
        return (this.session.getDirectoryId());
    }


    /**
     *  Gets the <code>Directory</code> associated with this 
     *  session.
     *
     *  @return the <code>Directory</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.Directory getDirectory()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getDirectory());
    }


    /**
     *  Tests if this user can perform <code>Directory</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupDirectories() {
        return (this.session.canSearchDirectories());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include files in directories which are children
     *  of this directory in the directory hierarchy.
     */

    @OSID @Override
    public void useFederatedDirectoryView() {
        this.session.useFederatedDirectoryView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this directory only.
     */

    @OSID @Override
    public void useIsolatedDirectoryView() {
        this.session.useIsolatedDirectoryView();
        return;
    }

     
    /**
     *  Gets the <code>Directory</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Directory</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Directory</code> and
     *  retained for compatibility.
     *
     *  @param  directoryId <code>Id</code> of the
     *          <code>Directory</code>
     *  @return the directory
     *  @throws org.osid.NotFoundException <code>directoryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>directoryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.Directory getDirectory(org.osid.id.Id directoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.filing.DirectoryQuery query = getQuery();
        query.matchId(directoryId, true);
        org.osid.filing.DirectoryList directories = this.session.getDirectoriesByQuery(query);
        if (directories.hasNext()) {
            return (directories.getNextDirectory());
        } 
        
        throw new org.osid.NotFoundException(directoryId + " not found");
    }


    /**
     *  Gets a <code>DirectoryList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  directories specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Directories</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  directoryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Directory</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>directoryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.DirectoryList getDirectoriesByIds(org.osid.id.IdList directoryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.filing.DirectoryQuery query = getQuery();

        try (org.osid.id.IdList ids = directoryIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getDirectoriesByQuery(query));
    }


    /**
     *  Gets a <code>DirectoryList</code> corresponding to the given
     *  directory genus <code>Type</code> which does not include
     *  directories of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  directories or an error results. Otherwise, the returned list
     *  may contain only those directories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  directoryGenusType a directory genus type 
     *  @return the returned <code>Directory</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>directoryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.DirectoryList getDirectoriesByGenusType(org.osid.type.Type directoryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.filing.DirectoryQuery query = getQuery();
        query.matchGenusType(directoryGenusType, true);
        return (this.session.getDirectoriesByQuery(query));
    }


    /**
     *  Gets a <code>DirectoryList</code> corresponding to the given
     *  directory genus <code>Type</code> and include any additional
     *  directories with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  directories or an error results. Otherwise, the returned list
     *  may contain only those directories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  directoryGenusType a directory genus type 
     *  @return the returned <code>Directory</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>directoryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.DirectoryList getDirectoriesByParentGenusType(org.osid.type.Type directoryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.filing.DirectoryQuery query = getQuery();
        query.matchParentGenusType(directoryGenusType, true);
        return (this.session.getDirectoriesByQuery(query));
    }


    /**
     *  Gets a <code>DirectoryList</code> containing the given
     *  directory record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  directories or an error results. Otherwise, the returned list
     *  may contain only those directories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  directoryRecordType a directory record type 
     *  @return the returned <code>Directory</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>directoryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.DirectoryList getDirectoriesByRecordType(org.osid.type.Type directoryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.filing.DirectoryQuery query = getQuery();
        query.matchRecordType(directoryRecordType, true);
        return (this.session.getDirectoriesByQuery(query));
    }


    /**
     *  Gets a <code>DirectoryList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known directories or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  directories that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Directory</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.filing.DirectoryList getDirectoriesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.filing.DirectoryQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getDirectoriesByQuery(query));        
    }

    
    /**
     *  Gets all <code>Directories</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  directories or an error results. Otherwise, the returned list
     *  may contain only those directories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Directories</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.DirectoryList getDirectories()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {


        org.osid.filing.DirectoryQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getDirectoriesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.filing.DirectoryQuery getQuery() {
        org.osid.filing.DirectoryQuery query = this.session.getDirectoryQuery();
        
        return (query);
    }
}
