//
// MutableIndexedMapStepLookupSession
//
//    Implements a Step lookup service backed by a collection of
//    steps indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.workflow;


/**
 *  Implements a Step lookup service backed by a collection of
 *  steps. The steps are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some steps may be compatible
 *  with more types than are indicated through these step
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of steps can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapStepLookupSession
    extends net.okapia.osid.jamocha.core.workflow.spi.AbstractIndexedMapStepLookupSession
    implements org.osid.workflow.StepLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapStepLookupSession} with no steps.
     *
     *  @param office the office
     *  @throws org.osid.NullArgumentException {@code office}
     *          is {@code null}
     */

      public MutableIndexedMapStepLookupSession(org.osid.workflow.Office office) {
        setOffice(office);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapStepLookupSession} with a
     *  single step.
     *  
     *  @param office the office
     *  @param  step a single step
     *  @throws org.osid.NullArgumentException {@code office} or
     *          {@code step} is {@code null}
     */

    public MutableIndexedMapStepLookupSession(org.osid.workflow.Office office,
                                                  org.osid.workflow.Step step) {
        this(office);
        putStep(step);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapStepLookupSession} using an
     *  array of steps.
     *
     *  @param office the office
     *  @param  steps an array of steps
     *  @throws org.osid.NullArgumentException {@code office} or
     *          {@code steps} is {@code null}
     */

    public MutableIndexedMapStepLookupSession(org.osid.workflow.Office office,
                                                  org.osid.workflow.Step[] steps) {
        this(office);
        putSteps(steps);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapStepLookupSession} using a
     *  collection of steps.
     *
     *  @param office the office
     *  @param  steps a collection of steps
     *  @throws org.osid.NullArgumentException {@code office} or
     *          {@code steps} is {@code null}
     */

    public MutableIndexedMapStepLookupSession(org.osid.workflow.Office office,
                                                  java.util.Collection<? extends org.osid.workflow.Step> steps) {

        this(office);
        putSteps(steps);
        return;
    }
    

    /**
     *  Makes a {@code Step} available in this session.
     *
     *  @param  step a step
     *  @throws org.osid.NullArgumentException {@code step{@code  is
     *          {@code null}
     */

    @Override
    public void putStep(org.osid.workflow.Step step) {
        super.putStep(step);
        return;
    }


    /**
     *  Makes an array of steps available in this session.
     *
     *  @param  steps an array of steps
     *  @throws org.osid.NullArgumentException {@code steps{@code 
     *          is {@code null}
     */

    @Override
    public void putSteps(org.osid.workflow.Step[] steps) {
        super.putSteps(steps);
        return;
    }


    /**
     *  Makes collection of steps available in this session.
     *
     *  @param  steps a collection of steps
     *  @throws org.osid.NullArgumentException {@code step{@code  is
     *          {@code null}
     */

    @Override
    public void putSteps(java.util.Collection<? extends org.osid.workflow.Step> steps) {
        super.putSteps(steps);
        return;
    }


    /**
     *  Removes a Step from this session.
     *
     *  @param stepId the {@code Id} of the step
     *  @throws org.osid.NullArgumentException {@code stepId{@code  is
     *          {@code null}
     */

    @Override
    public void removeStep(org.osid.id.Id stepId) {
        super.removeStep(stepId);
        return;
    }    
}
