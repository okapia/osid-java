//
// AbstractCompositionEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.repository.rules.compositionenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractCompositionEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.repository.rules.CompositionEnablerSearchResults {

    private org.osid.repository.rules.CompositionEnablerList compositionEnablers;
    private final org.osid.repository.rules.CompositionEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.repository.rules.records.CompositionEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractCompositionEnablerSearchResults.
     *
     *  @param compositionEnablers the result set
     *  @param compositionEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>compositionEnablers</code>
     *          or <code>compositionEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractCompositionEnablerSearchResults(org.osid.repository.rules.CompositionEnablerList compositionEnablers,
                                            org.osid.repository.rules.CompositionEnablerQueryInspector compositionEnablerQueryInspector) {
        nullarg(compositionEnablers, "composition enablers");
        nullarg(compositionEnablerQueryInspector, "composition enabler query inspectpr");

        this.compositionEnablers = compositionEnablers;
        this.inspector = compositionEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the composition enabler list resulting from a search.
     *
     *  @return a composition enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerList getCompositionEnablers() {
        if (this.compositionEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.repository.rules.CompositionEnablerList compositionEnablers = this.compositionEnablers;
        this.compositionEnablers = null;
	return (compositionEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.repository.rules.CompositionEnablerQueryInspector getCompositionEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  composition enabler search record <code> Type. </code> This method must
     *  be used to retrieve a compositionEnabler implementing the requested
     *  record.
     *
     *  @param compositionEnablerSearchRecordType a compositionEnabler search 
     *         record type 
     *  @return the composition enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>compositionEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(compositionEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.repository.rules.records.CompositionEnablerSearchResultsRecord getCompositionEnablerSearchResultsRecord(org.osid.type.Type compositionEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.repository.rules.records.CompositionEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(compositionEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(compositionEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record composition enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addCompositionEnablerRecord(org.osid.repository.rules.records.CompositionEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "composition enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
