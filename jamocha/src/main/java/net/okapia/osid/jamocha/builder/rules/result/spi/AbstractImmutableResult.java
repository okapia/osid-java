//
// AbstractImmutableResult.java
//
//     Wraps a mutable Result to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.rules.result.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Result</code> to hide modifiers. This
 *  wrapper provides an immutized Result from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying result whose state changes are visible.
 */

public abstract class AbstractImmutableResult
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidResult
    implements org.osid.rules.Result {

    private final org.osid.rules.Result result;


    /**
     *  Constructs a new <code>AbstractImmutableResult</code>.
     *
     *  @param result the result to immutablize
     *  @throws org.osid.NullArgumentException <code>result</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableResult(org.osid.rules.Result result) {
        super(result);
        this.result = result;
        return;
    }


    /**
     *  Gets a boolean result from the rule evaluation. Based on the 
     *  definition of the Rule, the result may be a simple boolean value or 
     *  something more complex defined in the record type. 
     *
     *  @return <code> true </code> or <code> false </code> 
     */

    @OSID @Override
    public boolean getBooleanValue() {
        return (this.result.getBooleanValue());
    }


    /**
     *  Gets the result record corresponding to the given <code>
     *  Result </code> record <code> Type. </code> This method is used
     *  to retrieve an object implementing the requested record. The
     *  <code> resultRecordType </code> may be the <code> Type </code>
     *  returned in <code> getRecordTypes() </code> or any of its
     *  parents in a <code> Type </code> hierarchy where <code>
     *  hasRecordType(resultRecordType) </code> is <code> true </code>
     *  .
     *
     *  @param  resultRecordType the type of the record to retrieve 
     *  @return the result record 
     *  @throws org.osid.NullArgumentException <code> resultRecordType
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(resultRecordType) </code> is <code>
     *          false </code>
     */

    @OSID @Override
    public org.osid.rules.records.ResultRecord getResultRecord(org.osid.type.Type resultRecordType)
        throws org.osid.OperationFailedException {

        return (this.result.getResultRecord(resultRecordType));
    }
}

