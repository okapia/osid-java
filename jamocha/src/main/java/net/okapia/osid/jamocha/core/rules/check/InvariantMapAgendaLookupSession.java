//
// InvariantMapAgendaLookupSession
//
//    Implements an Agenda lookup service backed by a fixed collection of
//    agendas.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.rules.check;


/**
 *  Implements an Agenda lookup service backed by a fixed
 *  collection of agendas. The agendas are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapAgendaLookupSession
    extends net.okapia.osid.jamocha.core.rules.check.spi.AbstractMapAgendaLookupSession
    implements org.osid.rules.check.AgendaLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapAgendaLookupSession</code> with no
     *  agendas.
     *  
     *  @param engine the engine
     *  @throws org.osid.NullArgumnetException {@code engine} is
     *          {@code null}
     */

    public InvariantMapAgendaLookupSession(org.osid.rules.Engine engine) {
        setEngine(engine);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAgendaLookupSession</code> with a single
     *  agenda.
     *  
     *  @param engine the engine
     *  @param agenda an single agenda
     *  @throws org.osid.NullArgumentException {@code engine} or
     *          {@code agenda} is <code>null</code>
     */

      public InvariantMapAgendaLookupSession(org.osid.rules.Engine engine,
                                               org.osid.rules.check.Agenda agenda) {
        this(engine);
        putAgenda(agenda);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAgendaLookupSession</code> using an array
     *  of agendas.
     *  
     *  @param engine the engine
     *  @param agendas an array of agendas
     *  @throws org.osid.NullArgumentException {@code engine} or
     *          {@code agendas} is <code>null</code>
     */

      public InvariantMapAgendaLookupSession(org.osid.rules.Engine engine,
                                               org.osid.rules.check.Agenda[] agendas) {
        this(engine);
        putAgendas(agendas);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAgendaLookupSession</code> using a
     *  collection of agendas.
     *
     *  @param engine the engine
     *  @param agendas a collection of agendas
     *  @throws org.osid.NullArgumentException {@code engine} or
     *          {@code agendas} is <code>null</code>
     */

      public InvariantMapAgendaLookupSession(org.osid.rules.Engine engine,
                                               java.util.Collection<? extends org.osid.rules.check.Agenda> agendas) {
        this(engine);
        putAgendas(agendas);
        return;
    }
}
