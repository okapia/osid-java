//
// AbstractImmutablePayment.java
//
//     Wraps a mutable Payment to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.billing.payment.payment.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Payment</code> to hide modifiers. This
 *  wrapper provides an immutized Payment from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying payment whose state changes are visible.
 */

public abstract class AbstractImmutablePayment
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.billing.payment.Payment {

    private final org.osid.billing.payment.Payment payment;


    /**
     *  Constructs a new <code>AbstractImmutablePayment</code>.
     *
     *  @param payment the payment to immutablize
     *  @throws org.osid.NullArgumentException <code>payment</code>
     *          is <code>null</code>
     */

    protected AbstractImmutablePayment(org.osid.billing.payment.Payment payment) {
        super(payment);
        this.payment = payment;
        return;
    }


    /**
     *  Gets the payer <code> Id. </code> 
     *
     *  @return the payer <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getPayerId() {
        return (this.payment.getPayerId());
    }


    /**
     *  Gets the payer. 
     *
     *  @return the payer 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.billing.payment.Payer getPayer()
        throws org.osid.OperationFailedException {

        return (this.payment.getPayer());
    }


    /**
     *  Gets the customer <code> Id </code> for which this payment applies. 
     *
     *  @return the customer <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCustomerId() {
        return (this.payment.getCustomerId());
    }


    /**
     *  Gets the customer for which this payment applies. 
     *
     *  @return the customer 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.billing.Customer getCustomer()
        throws org.osid.OperationFailedException {

        return (this.payment.getCustomer());
    }


    /**
     *  Gets the billing period <code> Id </code> to which this payment 
     *  applies. 
     *
     *  @return the period <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getPeriodId() {
        return (this.payment.getPeriodId());
    }


    /**
     *  Gets the billing period to which this payment applied. 
     *
     *  @return the period 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.billing.Period getPeriod()
        throws org.osid.OperationFailedException {

        return (this.payment.getPeriod());
    }


    /**
     *  Gets the date the payment was made. 
     *
     *  @return the payment date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getPaymentDate() {
        return (this.payment.getPaymentDate());
    }


    /**
     *  Gets the date the payment was or will be processed. 
     *
     *  @return the process date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getProcessDate() {
        return (this.payment.getProcessDate());
    }


    /**
     *  Gets the amount of this payment. 
     *
     *  @return the amount 
     */

    @OSID @Override
    public org.osid.financials.Currency getAmount() {
        return (this.payment.getAmount());
    }


    /**
     *  Gets the payment record corresponding to the given <code> Payment 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> paymentRecordType 
     *  </code> may be the <code> Type </code> returned in <code> 
     *  getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(paymentRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  paymentRecordType the type of payment record to retrieve 
     *  @return the payment record 
     *  @throws org.osid.NullArgumentException <code> paymentRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(paymentRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.records.PaymentRecord getPaymentRecord(org.osid.type.Type paymentRecordType)
        throws org.osid.OperationFailedException {

        return (this.payment.getPaymentRecord(paymentRecordType));
    }
}

