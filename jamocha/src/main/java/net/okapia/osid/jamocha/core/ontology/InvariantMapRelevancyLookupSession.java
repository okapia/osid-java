//
// InvariantMapRelevancyLookupSession
//
//    Implements a Relevancy lookup service backed by a fixed collection of
//    relevancies.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.ontology;


/**
 *  Implements a Relevancy lookup service backed by a fixed
 *  collection of relevancies. The relevancies are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapRelevancyLookupSession
    extends net.okapia.osid.jamocha.core.ontology.spi.AbstractMapRelevancyLookupSession
    implements org.osid.ontology.RelevancyLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapRelevancyLookupSession</code> with no
     *  relevancies.
     *  
     *  @param ontology the ontology
     *  @throws org.osid.NullArgumnetException {@code ontology} is
     *          {@code null}
     */

    public InvariantMapRelevancyLookupSession(org.osid.ontology.Ontology ontology) {
        setOntology(ontology);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapRelevancyLookupSession</code> with a single
     *  relevancy.
     *  
     *  @param ontology the ontology
     *  @param relevancy a single relevancy
     *  @throws org.osid.NullArgumentException {@code ontology} or
     *          {@code relevancy} is <code>null</code>
     */

      public InvariantMapRelevancyLookupSession(org.osid.ontology.Ontology ontology,
                                               org.osid.ontology.Relevancy relevancy) {
        this(ontology);
        putRelevancy(relevancy);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapRelevancyLookupSession</code> using an array
     *  of relevancies.
     *  
     *  @param ontology the ontology
     *  @param relevancies an array of relevancies
     *  @throws org.osid.NullArgumentException {@code ontology} or
     *          {@code relevancies} is <code>null</code>
     */

      public InvariantMapRelevancyLookupSession(org.osid.ontology.Ontology ontology,
                                               org.osid.ontology.Relevancy[] relevancies) {
        this(ontology);
        putRelevancies(relevancies);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapRelevancyLookupSession</code> using a
     *  collection of relevancies.
     *
     *  @param ontology the ontology
     *  @param relevancies a collection of relevancies
     *  @throws org.osid.NullArgumentException {@code ontology} or
     *          {@code relevancies} is <code>null</code>
     */

      public InvariantMapRelevancyLookupSession(org.osid.ontology.Ontology ontology,
                                               java.util.Collection<? extends org.osid.ontology.Relevancy> relevancies) {
        this(ontology);
        putRelevancies(relevancies);
        return;
    }
}
