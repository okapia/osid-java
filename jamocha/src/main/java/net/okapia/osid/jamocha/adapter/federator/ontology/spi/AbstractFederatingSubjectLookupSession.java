//
// AbstractFederatingSubjectLookupSession.java
//
//     An abstract federating adapter for a SubjectLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.ontology.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  SubjectLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingSubjectLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.ontology.SubjectLookupSession>
    implements org.osid.ontology.SubjectLookupSession {

    private boolean parallel = false;
    private org.osid.ontology.Ontology ontology = new net.okapia.osid.jamocha.nil.ontology.ontology.UnknownOntology();


    /**
     *  Constructs a new <code>AbstractFederatingSubjectLookupSession</code>.
     */

    protected AbstractFederatingSubjectLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.ontology.SubjectLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Ontology/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Ontology Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOntologyId() {
        return (this.ontology.getId());
    }


    /**
     *  Gets the <code>Ontology</code> associated with this 
     *  session.
     *
     *  @return the <code>Ontology</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.Ontology getOntology()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.ontology);
    }


    /**
     *  Sets the <code>Ontology</code>.
     *
     *  @param  ontology the ontology for this session
     *  @throws org.osid.NullArgumentException <code>ontology</code>
     *          is <code>null</code>
     */

    protected void setOntology(org.osid.ontology.Ontology ontology) {
        nullarg(ontology, "ontology");
        this.ontology = ontology;
        return;
    }


    /**
     *  Tests if this user can perform <code>Subject</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupSubjects() {
        for (org.osid.ontology.SubjectLookupSession session : getSessions()) {
            if (session.canLookupSubjects()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Subject</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeSubjectView() {
        for (org.osid.ontology.SubjectLookupSession session : getSessions()) {
            session.useComparativeSubjectView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Subject</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenarySubjectView() {
        for (org.osid.ontology.SubjectLookupSession session : getSessions()) {
            session.usePlenarySubjectView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include subjects in ontologies which are children
     *  of this ontology in the ontology hierarchy.
     */

    @OSID @Override
    public void useFederatedOntologyView() {
        for (org.osid.ontology.SubjectLookupSession session : getSessions()) {
            session.useFederatedOntologyView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this ontology only.
     */

    @OSID @Override
    public void useIsolatedOntologyView() {
        for (org.osid.ontology.SubjectLookupSession session : getSessions()) {
            session.useIsolatedOntologyView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Subject</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Subject</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Subject</code> and
     *  retained for compatibility.
     *
     *  @param  subjectId <code>Id</code> of the
     *          <code>Subject</code>
     *  @return the subject
     *  @throws org.osid.NotFoundException <code>subjectId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>subjectId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.Subject getSubject(org.osid.id.Id subjectId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.ontology.SubjectLookupSession session : getSessions()) {
            try {
                return (session.getSubject(subjectId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(subjectId + " not found");
    }


    /**
     *  Gets a <code>SubjectList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  subjects specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Subjects</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  subjectIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Subject</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>subjectIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.SubjectList getSubjectsByIds(org.osid.id.IdList subjectIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.ontology.subject.MutableSubjectList ret = new net.okapia.osid.jamocha.ontology.subject.MutableSubjectList();

        try (org.osid.id.IdList ids = subjectIds) {
            while (ids.hasNext()) {
                ret.addSubject(getSubject(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>SubjectList</code> corresponding to the given
     *  subject genus <code>Type</code> which does not include
     *  subjects of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  subjects or an error results. Otherwise, the returned list
     *  may contain only those subjects that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  subjectGenusType a subject genus type 
     *  @return the returned <code>Subject</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>subjectGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.SubjectList getSubjectsByGenusType(org.osid.type.Type subjectGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.ontology.subject.FederatingSubjectList ret = getSubjectList();

        for (org.osid.ontology.SubjectLookupSession session : getSessions()) {
            ret.addSubjectList(session.getSubjectsByGenusType(subjectGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>SubjectList</code> corresponding to the given
     *  subject genus <code>Type</code> and include any additional
     *  subjects with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  subjects or an error results. Otherwise, the returned list
     *  may contain only those subjects that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  subjectGenusType a subject genus type 
     *  @return the returned <code>Subject</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>subjectGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.SubjectList getSubjectsByParentGenusType(org.osid.type.Type subjectGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.ontology.subject.FederatingSubjectList ret = getSubjectList();

        for (org.osid.ontology.SubjectLookupSession session : getSessions()) {
            ret.addSubjectList(session.getSubjectsByParentGenusType(subjectGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>SubjectList</code> containing the given
     *  subject record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  subjects or an error results. Otherwise, the returned list
     *  may contain only those subjects that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  subjectRecordType a subject record type 
     *  @return the returned <code>Subject</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>subjectRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.SubjectList getSubjectsByRecordType(org.osid.type.Type subjectRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.ontology.subject.FederatingSubjectList ret = getSubjectList();

        for (org.osid.ontology.SubjectLookupSession session : getSessions()) {
            ret.addSubjectList(session.getSubjectsByRecordType(subjectRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Subjects</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  subjects or an error results. Otherwise, the returned list
     *  may contain only those subjects that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Subjects</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.SubjectList getSubjects()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.ontology.subject.FederatingSubjectList ret = getSubjectList();

        for (org.osid.ontology.SubjectLookupSession session : getSessions()) {
            ret.addSubjectList(session.getSubjects());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.ontology.subject.FederatingSubjectList getSubjectList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.ontology.subject.ParallelSubjectList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.ontology.subject.CompositeSubjectList());
        }
    }
}
