//
// MutableIndexedMapProxyInventoryLookupSession
//
//    Implements an Inventory lookup service backed by a collection of
//    inventories indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inventory;


/**
 *  Implements an Inventory lookup service backed by a collection of
 *  inventories. The inventories are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some inventories may be compatible
 *  with more types than are indicated through these inventory
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of inventories can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyInventoryLookupSession
    extends net.okapia.osid.jamocha.core.inventory.spi.AbstractIndexedMapInventoryLookupSession
    implements org.osid.inventory.InventoryLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyInventoryLookupSession} with
     *  no inventory.
     *
     *  @param warehouse the warehouse
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code warehouse} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyInventoryLookupSession(org.osid.inventory.Warehouse warehouse,
                                                       org.osid.proxy.Proxy proxy) {
        setWarehouse(warehouse);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyInventoryLookupSession} with
     *  a single inventory.
     *
     *  @param warehouse the warehouse
     *  @param  inventory an inventory
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code warehouse},
     *          {@code inventory}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyInventoryLookupSession(org.osid.inventory.Warehouse warehouse,
                                                       org.osid.inventory.Inventory inventory, org.osid.proxy.Proxy proxy) {

        this(warehouse, proxy);
        putInventory(inventory);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyInventoryLookupSession} using
     *  an array of inventories.
     *
     *  @param warehouse the warehouse
     *  @param  inventories an array of inventories
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code warehouse},
     *          {@code inventories}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyInventoryLookupSession(org.osid.inventory.Warehouse warehouse,
                                                       org.osid.inventory.Inventory[] inventories, org.osid.proxy.Proxy proxy) {

        this(warehouse, proxy);
        putInventories(inventories);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyInventoryLookupSession} using
     *  a collection of inventories.
     *
     *  @param warehouse the warehouse
     *  @param  inventories a collection of inventories
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code warehouse},
     *          {@code inventories}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyInventoryLookupSession(org.osid.inventory.Warehouse warehouse,
                                                       java.util.Collection<? extends org.osid.inventory.Inventory> inventories,
                                                       org.osid.proxy.Proxy proxy) {
        this(warehouse, proxy);
        putInventories(inventories);
        return;
    }

    
    /**
     *  Makes an {@code Inventory} available in this session.
     *
     *  @param  inventory an inventory
     *  @throws org.osid.NullArgumentException {@code inventory{@code 
     *          is {@code null}
     */

    @Override
    public void putInventory(org.osid.inventory.Inventory inventory) {
        super.putInventory(inventory);
        return;
    }


    /**
     *  Makes an array of inventories available in this session.
     *
     *  @param  inventories an array of inventories
     *  @throws org.osid.NullArgumentException {@code inventories{@code 
     *          is {@code null}
     */

    @Override
    public void putInventories(org.osid.inventory.Inventory[] inventories) {
        super.putInventories(inventories);
        return;
    }


    /**
     *  Makes collection of inventories available in this session.
     *
     *  @param  inventories a collection of inventories
     *  @throws org.osid.NullArgumentException {@code inventory{@code 
     *          is {@code null}
     */

    @Override
    public void putInventories(java.util.Collection<? extends org.osid.inventory.Inventory> inventories) {
        super.putInventories(inventories);
        return;
    }


    /**
     *  Removes an Inventory from this session.
     *
     *  @param inventoryId the {@code Id} of the inventory
     *  @throws org.osid.NullArgumentException {@code inventoryId{@code  is
     *          {@code null}
     */

    @Override
    public void removeInventory(org.osid.id.Id inventoryId) {
        super.removeInventory(inventoryId);
        return;
    }    
}
