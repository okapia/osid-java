//
// AbstractCommitmentEnablerQuery.java
//
//     A template for making a CommitmentEnabler Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.rules.commitmentenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for commitment enablers.
 */

public abstract class AbstractCommitmentEnablerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.calendaring.rules.CommitmentEnablerQuery {

    private final java.util.Collection<org.osid.calendaring.rules.records.CommitmentEnablerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches enablers mapped to the commitment. 
     *
     *  @param  commitmentId the commitment book <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> commitmentBookId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledCommitmentId(org.osid.id.Id commitmentId, 
                                       boolean match) {
        return;
    }


    /**
     *  Clears the commitment <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledCommitmentIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CommitmentBookQuery </code> is available. 
     *
     *  @return <code> true </code> if a commitment query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledCommitmentQuery() {
        return (false);
    }


    /**
     *  Gets the query for a commitment. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the calendar query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentQuery getRuledCommitmentQuery() {
        throw new org.osid.UnimplementedException("supportsRuledCommitmentQuery() is false");
    }


    /**
     *  Matches enablers mapped to any commitment. 
     *
     *  @param  match <code> true </code> for enablers mapped to any 
     *          commitment, <code> false </code> to match enablers mapped to 
     *          no commitment 
     */

    @OSID @Override
    public void matchAnyRuledCommitment(boolean match) {
        return;
    }


    /**
     *  Clears the commitment query terms. 
     */

    @OSID @Override
    public void clearRuledCommitmentTerms() {
        return;
    }


    /**
     *  Matches enablers mapped to the calendar. 
     *
     *  @param  calendarId the calendar <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCalendarId(org.osid.id.Id calendarId, boolean match) {
        return;
    }


    /**
     *  Clears the calendar <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCalendarIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> CalendarQuery </code> is available. 
     *
     *  @return <code> true </code> if an calendar query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarQuery() {
        return (false);
    }


    /**
     *  Gets the query for an calendar. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the calendar query 
     *  @throws org.osid.UnimplementedException <code> supportsCalendarQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQuery getCalendarQuery() {
        throw new org.osid.UnimplementedException("supportsCalendarQuery() is false");
    }


    /**
     *  Clears the calendar query terms. 
     */

    @OSID @Override
    public void clearCalendarTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given commitment enabler query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a commitment enabler implementing the requested record.
     *
     *  @param commitmentEnablerRecordType a commitment enabler record type
     *  @return the commitment enabler query record
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(commitmentEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.rules.records.CommitmentEnablerQueryRecord getCommitmentEnablerQueryRecord(org.osid.type.Type commitmentEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.rules.records.CommitmentEnablerQueryRecord record : this.records) {
            if (record.implementsRecordType(commitmentEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(commitmentEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this commitment enabler query. 
     *
     *  @param commitmentEnablerQueryRecord commitment enabler query record
     *  @param commitmentEnablerRecordType commitmentEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCommitmentEnablerQueryRecord(org.osid.calendaring.rules.records.CommitmentEnablerQueryRecord commitmentEnablerQueryRecord, 
                                          org.osid.type.Type commitmentEnablerRecordType) {

        addRecordType(commitmentEnablerRecordType);
        nullarg(commitmentEnablerQueryRecord, "commitment enabler query record");
        this.records.add(commitmentEnablerQueryRecord);        
        return;
    }
}
