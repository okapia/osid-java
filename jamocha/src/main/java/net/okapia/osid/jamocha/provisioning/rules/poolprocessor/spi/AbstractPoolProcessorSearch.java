//
// AbstractPoolProcessorSearch.java
//
//     A template for making a PoolProcessor Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.poolprocessor.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing pool processor searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractPoolProcessorSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.provisioning.rules.PoolProcessorSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.provisioning.rules.records.PoolProcessorSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.provisioning.rules.PoolProcessorSearchOrder poolProcessorSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of pool processors. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  poolProcessorIds list of pool processors
     *  @throws org.osid.NullArgumentException
     *          <code>poolProcessorIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongPoolProcessors(org.osid.id.IdList poolProcessorIds) {
        while (poolProcessorIds.hasNext()) {
            try {
                this.ids.add(poolProcessorIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongPoolProcessors</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of pool processor Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getPoolProcessorIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  poolProcessorSearchOrder pool processor search order 
     *  @throws org.osid.NullArgumentException
     *          <code>poolProcessorSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>poolProcessorSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderPoolProcessorResults(org.osid.provisioning.rules.PoolProcessorSearchOrder poolProcessorSearchOrder) {
	this.poolProcessorSearchOrder = poolProcessorSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.provisioning.rules.PoolProcessorSearchOrder getPoolProcessorSearchOrder() {
	return (this.poolProcessorSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given pool processor search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a pool processor implementing the requested record.
     *
     *  @param poolProcessorSearchRecordType a pool processor search record
     *         type
     *  @return the pool processor search record
     *  @throws org.osid.NullArgumentException
     *          <code>poolProcessorSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(poolProcessorSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.PoolProcessorSearchRecord getPoolProcessorSearchRecord(org.osid.type.Type poolProcessorSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.provisioning.rules.records.PoolProcessorSearchRecord record : this.records) {
            if (record.implementsRecordType(poolProcessorSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(poolProcessorSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this pool processor search. 
     *
     *  @param poolProcessorSearchRecord pool processor search record
     *  @param poolProcessorSearchRecordType poolProcessor search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPoolProcessorSearchRecord(org.osid.provisioning.rules.records.PoolProcessorSearchRecord poolProcessorSearchRecord, 
                                           org.osid.type.Type poolProcessorSearchRecordType) {

        addRecordType(poolProcessorSearchRecordType);
        this.records.add(poolProcessorSearchRecord);        
        return;
    }
}
