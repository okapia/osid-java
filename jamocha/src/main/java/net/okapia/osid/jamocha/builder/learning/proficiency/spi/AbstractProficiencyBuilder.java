//
// AbstractProficiency.java
//
//     Defines a Proficiency builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.learning.proficiency.spi;


/**
 *  Defines a <code>Proficiency</code> builder.
 */

public abstract class AbstractProficiencyBuilder<T extends AbstractProficiencyBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.learning.proficiency.ProficiencyMiter proficiency;


    /**
     *  Constructs a new <code>AbstractProficiencyBuilder</code>.
     *
     *  @param proficiency the proficiency to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractProficiencyBuilder(net.okapia.osid.jamocha.builder.learning.proficiency.ProficiencyMiter proficiency) {
        super(proficiency);
        this.proficiency = proficiency;
        return;
    }


    /**
     *  Builds the proficiency.
     *
     *  @return the new proficiency
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.learning.Proficiency build() {
        (new net.okapia.osid.jamocha.builder.validator.learning.proficiency.ProficiencyValidator(getValidations())).validate(this.proficiency);
        return (new net.okapia.osid.jamocha.builder.learning.proficiency.ImmutableProficiency(this.proficiency));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the proficiency miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.learning.proficiency.ProficiencyMiter getMiter() {
        return (this.proficiency);
    }


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>resource</code> is <code>null</code>
     */

    public T resource(org.osid.resource.Resource resource) {
        getMiter().setResource(resource);
        return (self());
    }


    /**
     *  Sets the objective.
     *
     *  @param objective an objective
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>objective</code>
     *          is <code>null</code>
     */

    public T objective(org.osid.learning.Objective objective) {
        getMiter().setObjective(objective);
        return (self());
    }


    /**
     *  Sets the completion.
     *
     *  @param completion a completion
     *  @return the builder
     *  @throws org.osid.InvalidArgumentException
     *          <code>completion</code> is out of range
     *  @throws org.osid.NullArgumentException <code>completion</code>
     *          is <code>null</code>
     */

    public T completion(java.math.BigDecimal completion) {
        getMiter().setCompletion(completion);
        return (self());
    }


    /**
     *  Sets the level.
     *
     *  @param level a level
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>level</code> is
     *          <code>null</code>
     */

    public T level(org.osid.grading.Grade level) {
        getMiter().setLevel(level);
        return (self());
    }


    /**
     *  Adds a Proficiency record.
     *
     *  @param record a proficiency record
     *  @param recordType the type of proficiency record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.learning.records.ProficiencyRecord record, org.osid.type.Type recordType) {
        getMiter().addProficiencyRecord(record, recordType);
        return (self());
    }
}       


