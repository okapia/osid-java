//
// InvariantMapPoolProcessorLookupSession
//
//    Implements a PoolProcessor lookup service backed by a fixed collection of
//    poolProcessors.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.rules;


/**
 *  Implements a PoolProcessor lookup service backed by a fixed
 *  collection of pool processors. The pool processors are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapPoolProcessorLookupSession
    extends net.okapia.osid.jamocha.core.provisioning.rules.spi.AbstractMapPoolProcessorLookupSession
    implements org.osid.provisioning.rules.PoolProcessorLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapPoolProcessorLookupSession</code> with no
     *  pool processors.
     *  
     *  @param distributor the distributor
     *  @throws org.osid.NullArgumnetException {@code distributor} is
     *          {@code null}
     */

    public InvariantMapPoolProcessorLookupSession(org.osid.provisioning.Distributor distributor) {
        setDistributor(distributor);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapPoolProcessorLookupSession</code> with a single
     *  pool processor.
     *  
     *  @param distributor the distributor
     *  @param poolProcessor a single pool processor
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code poolProcessor} is <code>null</code>
     */

      public InvariantMapPoolProcessorLookupSession(org.osid.provisioning.Distributor distributor,
                                               org.osid.provisioning.rules.PoolProcessor poolProcessor) {
        this(distributor);
        putPoolProcessor(poolProcessor);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapPoolProcessorLookupSession</code> using an array
     *  of pool processors.
     *  
     *  @param distributor the distributor
     *  @param poolProcessors an array of pool processors
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code poolProcessors} is <code>null</code>
     */

      public InvariantMapPoolProcessorLookupSession(org.osid.provisioning.Distributor distributor,
                                               org.osid.provisioning.rules.PoolProcessor[] poolProcessors) {
        this(distributor);
        putPoolProcessors(poolProcessors);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapPoolProcessorLookupSession</code> using a
     *  collection of pool processors.
     *
     *  @param distributor the distributor
     *  @param poolProcessors a collection of pool processors
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code poolProcessors} is <code>null</code>
     */

      public InvariantMapPoolProcessorLookupSession(org.osid.provisioning.Distributor distributor,
                                               java.util.Collection<? extends org.osid.provisioning.rules.PoolProcessor> poolProcessors) {
        this(distributor);
        putPoolProcessors(poolProcessors);
        return;
    }
}
