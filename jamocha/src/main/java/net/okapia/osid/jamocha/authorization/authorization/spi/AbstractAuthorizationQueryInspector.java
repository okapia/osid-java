//
// AbstractAuthorizationQueryInspector.java
//
//     A template for making an AuthorizationQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authorization.authorization.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for authorizations.
 */

public abstract class AbstractAuthorizationQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQueryInspector
    implements org.osid.authorization.AuthorizationQueryInspector {

    private final java.util.Collection<org.osid.authorization.records.AuthorizationQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the explicit authorization query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getExplicitAuthorizationsTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the related authorization <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRelatedAuthorizationIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the related authorization query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationQueryInspector[] getRelatedAuthorizationTerms() {
        return (new org.osid.authorization.AuthorizationQueryInspector[0]);
    }


    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResourceIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the trust <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getTrustIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the agent <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAgentIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the agent query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Gets the function <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFunctionIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the function query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authorization.FunctionQueryInspector[] getFunctionTerms() {
        return (new org.osid.authorization.FunctionQueryInspector[0]);
    }


    /**
     *  Gets the qualifier <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getQualifierIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the qualifier query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authorization.QualifierQueryInspector[] getQualifierTerms() {
        return (new org.osid.authorization.QualifierQueryInspector[0]);
    }


    /**
     *  Gets the vault <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getVaultIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the vault query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authorization.VaultQueryInspector[] getVaultTerms() {
        return (new org.osid.authorization.VaultQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given authorization query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an authorization implementing the requested record.
     *
     *  @param authorizationRecordType an authorization record type
     *  @return the authorization query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(authorizationRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.authorization.records.AuthorizationQueryInspectorRecord getAuthorizationQueryInspectorRecord(org.osid.type.Type authorizationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authorization.records.AuthorizationQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(authorizationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(authorizationRecordType + " is not supported");
    }


    /**
     *  Adds a record to this authorization query. 
     *
     *  @param authorizationQueryInspectorRecord authorization query inspector
     *         record
     *  @param authorizationRecordType authorization record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAuthorizationQueryInspectorRecord(org.osid.authorization.records.AuthorizationQueryInspectorRecord authorizationQueryInspectorRecord, 
                                                   org.osid.type.Type authorizationRecordType) {

        addRecordType(authorizationRecordType);
        nullarg(authorizationRecordType, "authorization record type");
        this.records.add(authorizationQueryInspectorRecord);        
        return;
    }
}
