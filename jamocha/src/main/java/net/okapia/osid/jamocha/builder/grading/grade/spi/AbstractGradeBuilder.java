//
// AbstractGrade.java
//
//     Defines a Grade builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.grading.grade.spi;


/**
 *  Defines a <code>Grade</code> builder.
 */

public abstract class AbstractGradeBuilder<T extends AbstractGradeBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.grading.grade.GradeMiter grade;


    /**
     *  Constructs a new <code>AbstractGradeBuilder</code>.
     *
     *  @param grade the grade to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractGradeBuilder(net.okapia.osid.jamocha.builder.grading.grade.GradeMiter grade) {
        super(grade);
        this.grade = grade;
        return;
    }


    /**
     *  Builds the grade.
     *
     *  @return the new grade
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.grading.Grade build() {
        (new net.okapia.osid.jamocha.builder.validator.grading.grade.GradeValidator(getValidations())).validate(this.grade);
        return (new net.okapia.osid.jamocha.builder.grading.grade.ImmutableGrade(this.grade));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the grade miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.grading.grade.GradeMiter getMiter() {
        return (this.grade);
    }


    /**
     *  Sets the grade system.
     *
     *  @param system a grade system
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>system</code> is <code>null</code>
     */

    public T gradeSystem(org.osid.grading.GradeSystem system) {
        getMiter().setGradeSystem(system);
        return (self());
    }


    /**
     *  Sets the input score start range.
     *
     *  @param score an input score start range
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>score</code> is
     *          <code>null</code>
     */

    public T inputScoreStartRange(java.math.BigDecimal score) {
        getMiter().setInputScoreStartRange(score);
        return (self());
    }


    /**
     *  Sets the input score end range.
     *
     *  @param score an input score end range
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>score</code> is
     *          <code>null</code>
     */

    public T inputScoreEndRange(java.math.BigDecimal score) {
        getMiter().setInputScoreEndRange(score);
        return (self());
    }


    /**
     *  Sets the output score.
     *
     *  @param score an output score
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>score</code> is
     *          <code>null</code>
     */

    public T outputScore(java.math.BigDecimal score) {
        getMiter().setOutputScore(score);
        return (self());
    }


    /**
     *  Adds a Grade record.
     *
     *  @param record a grade record
     *  @param recordType the type of grade record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.grading.records.GradeRecord record, org.osid.type.Type recordType) {
        getMiter().addGradeRecord(record, recordType);
        return (self());
    }
}       


