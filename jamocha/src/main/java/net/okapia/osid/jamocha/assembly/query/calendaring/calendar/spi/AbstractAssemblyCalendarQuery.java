//
// AbstractAssemblyCalendarQuery.java
//
//     A CalendarQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.calendaring.calendar.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A CalendarQuery that stores terms.
 */

public abstract class AbstractAssemblyCalendarQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.calendaring.CalendarQuery,
               org.osid.calendaring.CalendarQueryInspector,
               org.osid.calendaring.CalendarSearchOrder {

    private final java.util.Collection<org.osid.calendaring.records.CalendarQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.records.CalendarQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.records.CalendarSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyCalendarQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyCalendarQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the event <code> Id </code> for this query. 
     *
     *  @param  eventId an event <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> eventId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEventId(org.osid.id.Id eventId, boolean match) {
        getAssembler().addIdTerm(getEventIdColumn(), eventId, match);
        return;
    }


    /**
     *  Clears the event <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearEventIdTerms() {
        getAssembler().clearTerms(getEventIdColumn());
        return;
    }


    /**
     *  Gets the event <code> Id </code> terms. 
     *
     *  @return the event <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEventIdTerms() {
        return (getAssembler().getIdTerms(getEventIdColumn()));
    }


    /**
     *  Gets the EventId column name.
     *
     * @return the column name
     */

    protected String getEventIdColumn() {
        return ("event_id");
    }


    /**
     *  Tests if an <code> EventQuery </code> is available. 
     *
     *  @return <code> true </code> if an event query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for an event. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the event query 
     *  @throws org.osid.UnimplementedException <code> supportsEventQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventQuery getEventQuery() {
        throw new org.osid.UnimplementedException("supportsEventQuery() is false");
    }


    /**
     *  Matches a calendar that has any event assigned. 
     *
     *  @param  match <code> true </code> to match calendars with any event, 
     *          <code> false </code> to match calendars with no events 
     */

    @OSID @Override
    public void matchAnyEvent(boolean match) {
        getAssembler().addIdWildcardTerm(getEventColumn(), match);
        return;
    }


    /**
     *  Clears the event terms. 
     */

    @OSID @Override
    public void clearEventTerms() {
        getAssembler().clearTerms(getEventColumn());
        return;
    }


    /**
     *  Gets the event terms. 
     *
     *  @return the event terms 
     */

    @OSID @Override
    public org.osid.calendaring.EventQueryInspector[] getEventTerms() {
        return (new org.osid.calendaring.EventQueryInspector[0]);
    }


    /**
     *  Gets the Event column name.
     *
     * @return the column name
     */

    protected String getEventColumn() {
        return ("event");
    }


    /**
     *  Sets the time period <code> Id </code> for this query. 
     *
     *  @param  timePeriodId a time period <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> timePeriodId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchTimePeriodId(org.osid.id.Id timePeriodId, boolean match) {
        getAssembler().addIdTerm(getTimePeriodIdColumn(), timePeriodId, match);
        return;
    }


    /**
     *  Clears the time period <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearTimePeriodIdTerms() {
        getAssembler().clearTerms(getTimePeriodIdColumn());
        return;
    }


    /**
     *  Gets the time period <code> Id </code> terms. 
     *
     *  @return the time period <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getTimePeriodIdTerms() {
        return (getAssembler().getIdTerms(getTimePeriodIdColumn()));
    }


    /**
     *  Gets the TimePeriodId column name.
     *
     * @return the column name
     */

    protected String getTimePeriodIdColumn() {
        return ("time_period_id");
    }


    /**
     *  Tests if a <code> TimePeriodQuery </code> is available. 
     *
     *  @return <code> true </code> if a time period query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTimePeriodQuery() {
        return (false);
    }


    /**
     *  Gets the query for a time period. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the tiem period query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodQuery getTimePeriodQuery() {
        throw new org.osid.UnimplementedException("supportsTimePeriodQuery() is false");
    }


    /**
     *  Matches a calendar that has any time period assigned. 
     *
     *  @param  match <code> true </code> to match calendars with any time 
     *          period, <code> false </code> to match calendars with no time 
     *          periods 
     */

    @OSID @Override
    public void matchAnyTimePeriod(boolean match) {
        getAssembler().addIdWildcardTerm(getTimePeriodColumn(), match);
        return;
    }


    /**
     *  Clears the time period terms. 
     */

    @OSID @Override
    public void clearTimePeriodTerms() {
        getAssembler().clearTerms(getTimePeriodColumn());
        return;
    }


    /**
     *  Gets the time period terms. 
     *
     *  @return the time period terms 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodQueryInspector[] getTimePeriodTerms() {
        return (new org.osid.calendaring.TimePeriodQueryInspector[0]);
    }


    /**
     *  Gets the TimePeriod column name.
     *
     * @return the column name
     */

    protected String getTimePeriodColumn() {
        return ("time_period");
    }


    /**
     *  Sets the commitment <code> Id </code> for this query. 
     *
     *  @param  commitmentId a commitment <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> commitmentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCommitmentId(org.osid.id.Id commitmentId, boolean match) {
        getAssembler().addIdTerm(getCommitmentIdColumn(), commitmentId, match);
        return;
    }


    /**
     *  Clears the commitment <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCommitmentIdTerms() {
        getAssembler().clearTerms(getCommitmentIdColumn());
        return;
    }


    /**
     *  Gets the commitment <code> Id </code> terms. 
     *
     *  @return the commitment <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCommitmentIdTerms() {
        return (getAssembler().getIdTerms(getCommitmentIdColumn()));
    }


    /**
     *  Gets the CommitmentId column name.
     *
     * @return the column name
     */

    protected String getCommitmentIdColumn() {
        return ("commitment_id");
    }


    /**
     *  Tests if a <code> CommitmentQuery </code> is available. 
     *
     *  @return <code> true </code> if a commitment query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommitmentQuery() {
        return (false);
    }


    /**
     *  Gets the query for a commitment. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the commitment query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentQuery getCommitmentQuery() {
        throw new org.osid.UnimplementedException("supportsCommitmentQuery() is false");
    }


    /**
     *  Matches a calendar that has any event commitment. 
     *
     *  @param  match <code> true </code> to match calendars with any 
     *          commitment, <code> false </code> to match calendars with no 
     *          commitments 
     */

    @OSID @Override
    public void matchAnyCommitment(boolean match) {
        getAssembler().addIdWildcardTerm(getCommitmentColumn(), match);
        return;
    }


    /**
     *  Clears the commitment terms. 
     */

    @OSID @Override
    public void clearCommitmentTerms() {
        getAssembler().clearTerms(getCommitmentColumn());
        return;
    }


    /**
     *  Gets the commitment terms. 
     *
     *  @return the commitment terms 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentQueryInspector[] getCommitmentTerms() {
        return (new org.osid.calendaring.CommitmentQueryInspector[0]);
    }


    /**
     *  Gets the Commitment column name.
     *
     * @return the column name
     */

    protected String getCommitmentColumn() {
        return ("commitment");
    }


    /**
     *  Sets the calendar <code> Id </code> for this query to match calendars 
     *  that have the specified calendar as an ancestor. 
     *
     *  @param  calendarId a calendar <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorCalendarId(org.osid.id.Id calendarId, 
                                        boolean match) {
        getAssembler().addIdTerm(getAncestorCalendarIdColumn(), calendarId, match);
        return;
    }


    /**
     *  Clears the ancestor calendar <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorCalendarIdTerms() {
        getAssembler().clearTerms(getAncestorCalendarIdColumn());
        return;
    }


    /**
     *  Gets the ancestor calendar <code> Id </code> terms. 
     *
     *  @return the ancestor calendar <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorCalendarIdTerms() {
        return (getAssembler().getIdTerms(getAncestorCalendarIdColumn()));
    }


    /**
     *  Gets the AncestorCalendarId column name.
     *
     * @return the column name
     */

    protected String getAncestorCalendarIdColumn() {
        return ("ancestor_calendar_id");
    }


    /**
     *  Tests if a <code> CalendarQuery </code> is available. 
     *
     *  @return <code> true </code> if a calendar query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorCalendarQuery() {
        return (false);
    }


    /**
     *  Gets the query for a calendar. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the calendar query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorCalendarQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQuery getAncestorCalendarQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorCalendarQuery() is false");
    }


    /**
     *  Matches a calendar that has any ancestor. 
     *
     *  @param  match <code> true </code> to match calendars with any 
     *          ancestor, <code> false </code> to match root calendars 
     */

    @OSID @Override
    public void matchAnyAncestorCalendar(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorCalendarColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor calendar terms. 
     */

    @OSID @Override
    public void clearAncestorCalendarTerms() {
        getAssembler().clearTerms(getAncestorCalendarColumn());
        return;
    }


    /**
     *  Gets the ancestor calendar terms. 
     *
     *  @return the ancestor calendar terms 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQueryInspector[] getAncestorCalendarTerms() {
        return (new org.osid.calendaring.CalendarQueryInspector[0]);
    }


    /**
     *  Gets the AncestorCalendar column name.
     *
     * @return the column name
     */

    protected String getAncestorCalendarColumn() {
        return ("ancestor_calendar");
    }


    /**
     *  Sets the calendar <code> Id </code> for this query to match calendars 
     *  that have the specified calendar as a descendant. 
     *
     *  @param  calendarId a calendar <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantCalendarId(org.osid.id.Id calendarId, 
                                          boolean match) {
        getAssembler().addIdTerm(getDescendantCalendarIdColumn(), calendarId, match);
        return;
    }


    /**
     *  Clears the descendant calendar <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantCalendarIdTerms() {
        getAssembler().clearTerms(getDescendantCalendarIdColumn());
        return;
    }


    /**
     *  Gets the descendant calendar <code> Id </code> terms. 
     *
     *  @return the descendant calendar <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantCalendarIdTerms() {
        return (getAssembler().getIdTerms(getDescendantCalendarIdColumn()));
    }


    /**
     *  Gets the DescendantCalendarId column name.
     *
     * @return the column name
     */

    protected String getDescendantCalendarIdColumn() {
        return ("descendant_calendar_id");
    }


    /**
     *  Tests if a <code> CalendarQuery. </code> 
     *
     *  @return <code> true </code> if a calendar query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantCalendarQuery() {
        return (false);
    }


    /**
     *  Gets the query for a calendar. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the calendar query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantCalendarQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQuery getDescendantCalendarQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantCalendarQuery() is false");
    }


    /**
     *  Matches a calendar that has any descendant. 
     *
     *  @param  match <code> true </code> to match calendars with any 
     *          descendant, <code> false </code> to match leaf calendars 
     */

    @OSID @Override
    public void matchAnyDescendantCalendar(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantCalendarColumn(), match);
        return;
    }


    /**
     *  Clears the descendant calendar terms. 
     */

    @OSID @Override
    public void clearDescendantCalendarTerms() {
        getAssembler().clearTerms(getDescendantCalendarColumn());
        return;
    }


    /**
     *  Gets the descendant calendar terms. 
     *
     *  @return the descendant calendar terms 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQueryInspector[] getDescendantCalendarTerms() {
        return (new org.osid.calendaring.CalendarQueryInspector[0]);
    }


    /**
     *  Gets the DescendantCalendar column name.
     *
     * @return the column name
     */

    protected String getDescendantCalendarColumn() {
        return ("descendant_calendar");
    }


    /**
     *  Tests if this calendar supports the given record
     *  <code>Type</code>.
     *
     *  @param  calendarRecordType a calendar record type 
     *  @return <code>true</code> if the calendarRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>calendarRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type calendarRecordType) {
        for (org.osid.calendaring.records.CalendarQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(calendarRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  calendarRecordType the calendar record type 
     *  @return the calendar query record 
     *  @throws org.osid.NullArgumentException
     *          <code>calendarRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(calendarRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.CalendarQueryRecord getCalendarQueryRecord(org.osid.type.Type calendarRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.CalendarQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(calendarRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(calendarRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  calendarRecordType the calendar record type 
     *  @return the calendar query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>calendarRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(calendarRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.CalendarQueryInspectorRecord getCalendarQueryInspectorRecord(org.osid.type.Type calendarRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.CalendarQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(calendarRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(calendarRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param calendarRecordType the calendar record type
     *  @return the calendar search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>calendarRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(calendarRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.CalendarSearchOrderRecord getCalendarSearchOrderRecord(org.osid.type.Type calendarRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.CalendarSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(calendarRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(calendarRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this calendar. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param calendarQueryRecord the calendar query record
     *  @param calendarQueryInspectorRecord the calendar query inspector
     *         record
     *  @param calendarSearchOrderRecord the calendar search order record
     *  @param calendarRecordType calendar record type
     *  @throws org.osid.NullArgumentException
     *          <code>calendarQueryRecord</code>,
     *          <code>calendarQueryInspectorRecord</code>,
     *          <code>calendarSearchOrderRecord</code> or
     *          <code>calendarRecordTypecalendar</code> is
     *          <code>null</code>
     */
            
    protected void addCalendarRecords(org.osid.calendaring.records.CalendarQueryRecord calendarQueryRecord, 
                                      org.osid.calendaring.records.CalendarQueryInspectorRecord calendarQueryInspectorRecord, 
                                      org.osid.calendaring.records.CalendarSearchOrderRecord calendarSearchOrderRecord, 
                                      org.osid.type.Type calendarRecordType) {

        addRecordType(calendarRecordType);

        nullarg(calendarQueryRecord, "calendar query record");
        nullarg(calendarQueryInspectorRecord, "calendar query inspector record");
        nullarg(calendarSearchOrderRecord, "calendar search odrer record");

        this.queryRecords.add(calendarQueryRecord);
        this.queryInspectorRecords.add(calendarQueryInspectorRecord);
        this.searchOrderRecords.add(calendarSearchOrderRecord);
        
        return;
    }
}
