//
// AbstractAssemblyJobQuery.java
//
//     A JobQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.resourcing.job.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A JobQuery that stores terms.
 */

public abstract class AbstractAssemblyJobQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidGovernatorQuery
    implements org.osid.resourcing.JobQuery,
               org.osid.resourcing.JobQueryInspector,
               org.osid.resourcing.JobSearchOrder {

    private final java.util.Collection<org.osid.resourcing.records.JobQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resourcing.records.JobQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resourcing.records.JobSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyJobQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyJobQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the competency <code> Id </code> for this query. 
     *
     *  @param  competencyId the competency <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> competencyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCompetencyId(org.osid.id.Id competencyId, boolean match) {
        getAssembler().addIdTerm(getCompetencyIdColumn(), competencyId, match);
        return;
    }


    /**
     *  Clears the competency <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCompetencyIdTerms() {
        getAssembler().clearTerms(getCompetencyIdColumn());
        return;
    }


    /**
     *  Gets the competency <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCompetencyIdTerms() {
        return (getAssembler().getIdTerms(getCompetencyIdColumn()));
    }


    /**
     *  Gets the CompetencyId column name.
     *
     * @return the column name
     */

    protected String getCompetencyIdColumn() {
        return ("competency_id");
    }


    /**
     *  Tests if a <code> CompetencyQuery </code> is available. 
     *
     *  @return <code> true </code> if a competency query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompetencyQuery() {
        return (false);
    }


    /**
     *  Gets the query for a competency. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the competency query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencyQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyQuery getCompetencyQuery() {
        throw new org.osid.UnimplementedException("supportsCompetencyQuery() is false");
    }


    /**
     *  Matches work that have any competency. 
     *
     *  @param  match <code> true </code> to match jobs with any competency, 
     *          <code> false </code> to match work with no competency 
     */

    @OSID @Override
    public void matchAnyCompetency(boolean match) {
        getAssembler().addIdWildcardTerm(getCompetencyColumn(), match);
        return;
    }


    /**
     *  Clears the competency query terms. 
     */

    @OSID @Override
    public void clearCompetencyTerms() {
        getAssembler().clearTerms(getCompetencyColumn());
        return;
    }


    /**
     *  Gets the competency query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyQueryInspector[] getCompetencyTerms() {
        return (new org.osid.resourcing.CompetencyQueryInspector[0]);
    }


    /**
     *  Gets the Competency column name.
     *
     * @return the column name
     */

    protected String getCompetencyColumn() {
        return ("competency");
    }


    /**
     *  Sets the work <code> Id </code> for this query. 
     *
     *  @param  workId the work <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> workId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchWorkId(org.osid.id.Id workId, boolean match) {
        getAssembler().addIdTerm(getWorkIdColumn(), workId, match);
        return;
    }


    /**
     *  Clears the work <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearWorkIdTerms() {
        getAssembler().clearTerms(getWorkIdColumn());
        return;
    }


    /**
     *  Gets the work <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getWorkIdTerms() {
        return (getAssembler().getIdTerms(getWorkIdColumn()));
    }


    /**
     *  Gets the WorkId column name.
     *
     * @return the column name
     */

    protected String getWorkIdColumn() {
        return ("work_id");
    }


    /**
     *  Tests if a <code> WorkQuery </code> is available. 
     *
     *  @return <code> true </code> if a work query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkQuery() {
        return (false);
    }


    /**
     *  Gets the query for a work. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the work query 
     *  @throws org.osid.UnimplementedException <code> supportsWorkQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkQuery getWorkQuery() {
        throw new org.osid.UnimplementedException("supportsWorkQuery() is false");
    }


    /**
     *  Clears the work query terms. 
     */

    @OSID @Override
    public void clearWorkTerms() {
        getAssembler().clearTerms(getWorkColumn());
        return;
    }


    /**
     *  Gets the work query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.WorkQueryInspector[] getWorkTerms() {
        return (new org.osid.resourcing.WorkQueryInspector[0]);
    }


    /**
     *  Gets the Work column name.
     *
     * @return the column name
     */

    protected String getWorkColumn() {
        return ("work");
    }


    /**
     *  Sets the availability <code> Id </code> for this query. 
     *
     *  @param  availabilityId the availability <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> availabilityId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchAvailabilityId(org.osid.id.Id availabilityId, 
                                    boolean match) {
        getAssembler().addIdTerm(getAvailabilityIdColumn(), availabilityId, match);
        return;
    }


    /**
     *  Clears the availability <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAvailabilityIdTerms() {
        getAssembler().clearTerms(getAvailabilityIdColumn());
        return;
    }


    /**
     *  Gets the availability <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAvailabilityIdTerms() {
        return (getAssembler().getIdTerms(getAvailabilityIdColumn()));
    }


    /**
     *  Gets the AvailabilityId column name.
     *
     * @return the column name
     */

    protected String getAvailabilityIdColumn() {
        return ("availability_id");
    }


    /**
     *  Tests if an <code> AvailabilityQuery </code> is available. 
     *
     *  @return <code> true </code> if an availability query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilityQuery() {
        return (false);
    }


    /**
     *  Gets the query for an availability. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the availability query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityQuery getAvailabilityQuery() {
        throw new org.osid.UnimplementedException("supportsAvailabilityQuery() is false");
    }


    /**
     *  Matches work that have any availability. 
     *
     *  @param  match <code> true </code> to match work with any availability, 
     *          <code> false </code> to match work with no availability 
     */

    @OSID @Override
    public void matchAnyAvailability(boolean match) {
        getAssembler().addIdWildcardTerm(getAvailabilityColumn(), match);
        return;
    }


    /**
     *  Clears the availability query terms. 
     */

    @OSID @Override
    public void clearAvailabilityTerms() {
        getAssembler().clearTerms(getAvailabilityColumn());
        return;
    }


    /**
     *  Gets the availability query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityQueryInspector[] getAvailabilityTerms() {
        return (new org.osid.resourcing.AvailabilityQueryInspector[0]);
    }


    /**
     *  Gets the Availability column name.
     *
     * @return the column name
     */

    protected String getAvailabilityColumn() {
        return ("availability");
    }


    /**
     *  Sets the foundry <code> Id </code> for this query to match jobs 
     *  assigned to foundries. 
     *
     *  @param  foundryId the foundry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchFoundryId(org.osid.id.Id foundryId, boolean match) {
        getAssembler().addIdTerm(getFoundryIdColumn(), foundryId, match);
        return;
    }


    /**
     *  Clears the foundry <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearFoundryIdTerms() {
        getAssembler().clearTerms(getFoundryIdColumn());
        return;
    }


    /**
     *  Gets the foundry <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFoundryIdTerms() {
        return (getAssembler().getIdTerms(getFoundryIdColumn()));
    }


    /**
     *  Gets the FoundryId column name.
     *
     * @return the column name
     */

    protected String getFoundryIdColumn() {
        return ("foundry_id");
    }


    /**
     *  Tests if a <code> FoundryQuery </code> is available. 
     *
     *  @return <code> true </code> if a foundry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFoundryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a foundry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the foundry query 
     *  @throws org.osid.UnimplementedException <code> supportsFoundryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQuery getFoundryQuery() {
        throw new org.osid.UnimplementedException("supportsFoundryQuery() is false");
    }


    /**
     *  Clears the foundry query terms. 
     */

    @OSID @Override
    public void clearFoundryTerms() {
        getAssembler().clearTerms(getFoundryColumn());
        return;
    }


    /**
     *  Gets the foundry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQueryInspector[] getFoundryTerms() {
        return (new org.osid.resourcing.FoundryQueryInspector[0]);
    }


    /**
     *  Gets the Foundry column name.
     *
     * @return the column name
     */

    protected String getFoundryColumn() {
        return ("foundry");
    }


    /**
     *  Tests if this job supports the given record
     *  <code>Type</code>.
     *
     *  @param  jobRecordType a job record type 
     *  @return <code>true</code> if the jobRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>jobRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type jobRecordType) {
        for (org.osid.resourcing.records.JobQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(jobRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  jobRecordType the job record type 
     *  @return the job query record 
     *  @throws org.osid.NullArgumentException
     *          <code>jobRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(jobRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.JobQueryRecord getJobQueryRecord(org.osid.type.Type jobRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.records.JobQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(jobRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(jobRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  jobRecordType the job record type 
     *  @return the job query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>jobRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(jobRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.JobQueryInspectorRecord getJobQueryInspectorRecord(org.osid.type.Type jobRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.records.JobQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(jobRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(jobRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param jobRecordType the job record type
     *  @return the job search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>jobRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(jobRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.JobSearchOrderRecord getJobSearchOrderRecord(org.osid.type.Type jobRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.records.JobSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(jobRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(jobRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this job. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param jobQueryRecord the job query record
     *  @param jobQueryInspectorRecord the job query inspector
     *         record
     *  @param jobSearchOrderRecord the job search order record
     *  @param jobRecordType job record type
     *  @throws org.osid.NullArgumentException
     *          <code>jobQueryRecord</code>,
     *          <code>jobQueryInspectorRecord</code>,
     *          <code>jobSearchOrderRecord</code> or
     *          <code>jobRecordTypejob</code> is
     *          <code>null</code>
     */
            
    protected void addJobRecords(org.osid.resourcing.records.JobQueryRecord jobQueryRecord, 
                                      org.osid.resourcing.records.JobQueryInspectorRecord jobQueryInspectorRecord, 
                                      org.osid.resourcing.records.JobSearchOrderRecord jobSearchOrderRecord, 
                                      org.osid.type.Type jobRecordType) {

        addRecordType(jobRecordType);

        nullarg(jobQueryRecord, "job query record");
        nullarg(jobQueryInspectorRecord, "job query inspector record");
        nullarg(jobSearchOrderRecord, "job search odrer record");

        this.queryRecords.add(jobQueryRecord);
        this.queryInspectorRecords.add(jobQueryInspectorRecord);
        this.searchOrderRecords.add(jobSearchOrderRecord);
        
        return;
    }
}
