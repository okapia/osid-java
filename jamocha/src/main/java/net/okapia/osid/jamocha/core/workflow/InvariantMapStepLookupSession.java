//
// InvariantMapStepLookupSession
//
//    Implements a Step lookup service backed by a fixed collection of
//    steps.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.workflow;


/**
 *  Implements a Step lookup service backed by a fixed
 *  collection of steps. The steps are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapStepLookupSession
    extends net.okapia.osid.jamocha.core.workflow.spi.AbstractMapStepLookupSession
    implements org.osid.workflow.StepLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapStepLookupSession</code> with no
     *  steps.
     *  
     *  @param office the office
     *  @throws org.osid.NullArgumnetException {@code office} is
     *          {@code null}
     */

    public InvariantMapStepLookupSession(org.osid.workflow.Office office) {
        setOffice(office);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapStepLookupSession</code> with a single
     *  step.
     *  
     *  @param office the office
     *  @param step a single step
     *  @throws org.osid.NullArgumentException {@code office} or
     *          {@code step} is <code>null</code>
     */

      public InvariantMapStepLookupSession(org.osid.workflow.Office office,
                                               org.osid.workflow.Step step) {
        this(office);
        putStep(step);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapStepLookupSession</code> using an array
     *  of steps.
     *  
     *  @param office the office
     *  @param steps an array of steps
     *  @throws org.osid.NullArgumentException {@code office} or
     *          {@code steps} is <code>null</code>
     */

      public InvariantMapStepLookupSession(org.osid.workflow.Office office,
                                               org.osid.workflow.Step[] steps) {
        this(office);
        putSteps(steps);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapStepLookupSession</code> using a
     *  collection of steps.
     *
     *  @param office the office
     *  @param steps a collection of steps
     *  @throws org.osid.NullArgumentException {@code office} or
     *          {@code steps} is <code>null</code>
     */

      public InvariantMapStepLookupSession(org.osid.workflow.Office office,
                                               java.util.Collection<? extends org.osid.workflow.Step> steps) {
        this(office);
        putSteps(steps);
        return;
    }
}
