//
// PoolElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.pool.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class PoolElements
    extends net.okapia.osid.jamocha.spi.OsidGovernatorElements {


    /**
     *  Gets the PoolElement Id.
     *
     *  @return the pool element Id
     */

    public static org.osid.id.Id getPoolEntityId() {
        return (makeEntityId("osid.provisioning.Pool"));
    }


    /**
     *  Gets the BrokerId element Id.
     *
     *  @return the BrokerId element Id
     */

    public static org.osid.id.Id getBrokerId() {
        return (makeElementId("osid.provisioning.pool.BrokerId"));
    }


    /**
     *  Gets the Broker element Id.
     *
     *  @return the Broker element Id
     */

    public static org.osid.id.Id getBroker() {
        return (makeElementId("osid.provisioning.pool.Broker"));
    }


    /**
     *  Gets the Size element Id.
     *
     *  @return the Size element Id
     */

    public static org.osid.id.Id getSize() {
        return (makeElementId("osid.provisioning.pool.Size"));
    }


    /**
     *  Gets the ProvisionableId element Id.
     *
     *  @return the ProvisionableId element Id
     */

    public static org.osid.id.Id getProvisionableId() {
        return (makeQueryElementId("osid.provisioning.pool.ProvisionableId"));
    }


    /**
     *  Gets the Provisionable element Id.
     *
     *  @return the Provisionable element Id
     */

    public static org.osid.id.Id getProvisionable() {
        return (makeQueryElementId("osid.provisioning.pool.Provisionable"));
    }


    /**
     *  Gets the DistributorId element Id.
     *
     *  @return the DistributorId element Id
     */

    public static org.osid.id.Id getDistributorId() {
        return (makeQueryElementId("osid.provisioning.pool.DistributorId"));
    }


    /**
     *  Gets the Distributor element Id.
     *
     *  @return the Distributor element Id
     */

    public static org.osid.id.Id getDistributor() {
        return (makeQueryElementId("osid.provisioning.pool.Distributor"));
    }
}
