//
// BudgetEntryMiter.java
//
//     Defines a BudgetEntry miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.financials.budgeting.budgetentry;


/**
 *  Defines a <code>BudgetEntry</code> miter for use with the builders.
 */

public interface BudgetEntryMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRelationshipMiter,
            org.osid.financials.budgeting.BudgetEntry {


    /**
     *  Sets the budget.
     *
     *  @param budget a budget
     *  @throws org.osid.NullArgumentException <code>budget</code> is
     *          <code>null</code>
     */

    public void setBudget(org.osid.financials.budgeting.Budget budget);


    /**
     *  Sets the account.
     *
     *  @param account an account
     *  @throws org.osid.NullArgumentException <code>account</code> is
     *          <code>null</code>
     */

    public void setAccount(org.osid.financials.Account account);


    /**
     *  Sets the amount.
     *
     *  @param amount an amount
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    public void setAmount(org.osid.financials.Currency amount);


    /**
     *  Sets the debit flag.
     *
     *  @param debit <code> true </code> if this item amount is a
     *         debit, <code> false </code> if it is a credit
     */

    public void setDebit(boolean debit);


    /**
     *  Adds a BudgetEntry record.
     *
     *  @param record a budgetEntry record
     *  @param recordType the type of budgetEntry record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addBudgetEntryRecord(org.osid.financials.budgeting.records.BudgetEntryRecord record, org.osid.type.Type recordType);
}       


