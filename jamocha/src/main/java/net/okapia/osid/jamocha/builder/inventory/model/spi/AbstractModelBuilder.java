//
// AbstractModel.java
//
//     Defines a Model builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.inventory.model.spi;


/**
 *  Defines a <code>Model</code> builder.
 */

public abstract class AbstractModelBuilder<T extends AbstractModelBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.inventory.model.ModelMiter model;


    /**
     *  Constructs a new <code>AbstractModelBuilder</code>.
     *
     *  @param model the model to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractModelBuilder(net.okapia.osid.jamocha.builder.inventory.model.ModelMiter model) {
        super(model);
        this.model = model;
        return;
    }


    /**
     *  Builds the model.
     *
     *  @return the new model
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.inventory.Model build() {
        (new net.okapia.osid.jamocha.builder.validator.inventory.model.ModelValidator(getValidations())).validate(this.model);
        return (new net.okapia.osid.jamocha.builder.inventory.model.ImmutableModel(this.model));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the model miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.inventory.model.ModelMiter getMiter() {
        return (this.model);
    }


    /**
     *  Sets the manufacturer.
     *
     *  @param manufacturer a manufacturer
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>manufacturer</code> is <code>null</code>
     */

    public T manufacturer(org.osid.resource.Resource manufacturer) {
        getMiter().setManufacturer(manufacturer);
        return (self());
    }


    /**
     *  Sets the archetype.
     *
     *  @param archetype an archetype
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>archetype</code>
     *          is <code>null</code>
     */

    public T archetype(String archetype) {
        getMiter().setArchetype(archetype);
        return (self());
    }


    /**
     *  Sets the number.
     *
     *  @param number a number
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>number</code> is
     *          <code>null</code>
     */

    public T number(String number) {
        getMiter().setNumber(number);
        return (self());
    }


    /**
     *  Adds a Model record.
     *
     *  @param record a model record
     *  @param recordType the type of model record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.inventory.records.ModelRecord record, org.osid.type.Type recordType) {
        getMiter().addModelRecord(record, recordType);
        return (self());
    }
}       


