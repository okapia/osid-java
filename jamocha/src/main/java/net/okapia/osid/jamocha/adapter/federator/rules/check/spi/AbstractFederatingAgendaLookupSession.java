//
// AbstractFederatingAgendaLookupSession.java
//
//     An abstract federating adapter for an AgendaLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.rules.check.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  AgendaLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingAgendaLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.rules.check.AgendaLookupSession>
    implements org.osid.rules.check.AgendaLookupSession {

    private boolean parallel = false;
    private org.osid.rules.Engine engine = new net.okapia.osid.jamocha.nil.rules.engine.UnknownEngine();


    /**
     *  Constructs a new <code>AbstractFederatingAgendaLookupSession</code>.
     */

    protected AbstractFederatingAgendaLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.rules.check.AgendaLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Engine/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Engine Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getEngineId() {
        return (this.engine.getId());
    }


    /**
     *  Gets the <code>Engine</code> associated with this 
     *  session.
     *
     *  @return the <code>Engine</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.Engine getEngine()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.engine);
    }


    /**
     *  Sets the <code>Engine</code>.
     *
     *  @param  engine the engine for this session
     *  @throws org.osid.NullArgumentException <code>engine</code>
     *          is <code>null</code>
     */

    protected void setEngine(org.osid.rules.Engine engine) {
        nullarg(engine, "engine");
        this.engine = engine;
        return;
    }


    /**
     *  Tests if this user can perform <code>Agenda</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAgendas() {
        for (org.osid.rules.check.AgendaLookupSession session : getSessions()) {
            if (session.canLookupAgendas()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Agenda</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAgendaView() {
        for (org.osid.rules.check.AgendaLookupSession session : getSessions()) {
            session.useComparativeAgendaView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Agenda</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAgendaView() {
        for (org.osid.rules.check.AgendaLookupSession session : getSessions()) {
            session.usePlenaryAgendaView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include agendas in engines which are children
     *  of this engine in the engine hierarchy.
     */

    @OSID @Override
    public void useFederatedEngineView() {
        for (org.osid.rules.check.AgendaLookupSession session : getSessions()) {
            session.useFederatedEngineView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this engine only.
     */

    @OSID @Override
    public void useIsolatedEngineView() {
        for (org.osid.rules.check.AgendaLookupSession session : getSessions()) {
            session.useIsolatedEngineView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Agenda</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Agenda</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Agenda</code> and
     *  retained for compatibility.
     *
     *  @param  agendaId <code>Id</code> of the
     *          <code>Agenda</code>
     *  @return the agenda
     *  @throws org.osid.NotFoundException <code>agendaId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>agendaId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.Agenda getAgenda(org.osid.id.Id agendaId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.rules.check.AgendaLookupSession session : getSessions()) {
            try {
                return (session.getAgenda(agendaId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(agendaId + " not found");
    }


    /**
     *  Gets an <code>AgendaList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the agendas
     *  specified in the <code>Id</code> list, in the order of the
     *  list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Agendas</code> may
     *  be omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  agendaIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Agenda</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>agendaIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaList getAgendasByIds(org.osid.id.IdList agendaIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.rules.check.agenda.MutableAgendaList ret = new net.okapia.osid.jamocha.rules.check.agenda.MutableAgendaList();

        try (org.osid.id.IdList ids = agendaIds) {
            while (ids.hasNext()) {
                ret.addAgenda(getAgenda(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>AgendaList</code> corresponding to the given
     *  agenda genus <code>Type</code> which does not include
     *  agendas of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known agendas
     *  or an error results. Otherwise, the returned list may contain
     *  only those agendas that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  @param  agendaGenusType an agenda genus type 
     *  @return the returned <code>Agenda</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>agendaGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaList getAgendasByGenusType(org.osid.type.Type agendaGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.rules.check.agenda.FederatingAgendaList ret = getAgendaList();

        for (org.osid.rules.check.AgendaLookupSession session : getSessions()) {
            ret.addAgendaList(session.getAgendasByGenusType(agendaGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AgendaList</code> corresponding to the given
     *  agenda genus <code>Type</code> and include any additional
     *  agendas with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known agendas
     *  or an error results. Otherwise, the returned list may contain
     *  only those agendas that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  @param  agendaGenusType an agenda genus type 
     *  @return the returned <code>Agenda</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>agendaGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaList getAgendasByParentGenusType(org.osid.type.Type agendaGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.rules.check.agenda.FederatingAgendaList ret = getAgendaList();

        for (org.osid.rules.check.AgendaLookupSession session : getSessions()) {
            ret.addAgendaList(session.getAgendasByParentGenusType(agendaGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AgendaList</code> containing the given
     *  agenda record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known agendas
     *  or an error results. Otherwise, the returned list may contain
     *  only those agendas that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  @param  agendaRecordType an agenda record type 
     *  @return the returned <code>Agenda</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>agendaRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaList getAgendasByRecordType(org.osid.type.Type agendaRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.rules.check.agenda.FederatingAgendaList ret = getAgendaList();

        for (org.osid.rules.check.AgendaLookupSession session : getSessions()) {
            ret.addAgendaList(session.getAgendasByRecordType(agendaRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Agendas</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  agendas or an error results. Otherwise, the returned list
     *  may contain only those agendas that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Agendas</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaList getAgendas()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.rules.check.agenda.FederatingAgendaList ret = getAgendaList();

        for (org.osid.rules.check.AgendaLookupSession session : getSessions()) {
            ret.addAgendaList(session.getAgendas());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.rules.check.agenda.FederatingAgendaList getAgendaList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.rules.check.agenda.ParallelAgendaList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.rules.check.agenda.CompositeAgendaList());
        }
    }
}
