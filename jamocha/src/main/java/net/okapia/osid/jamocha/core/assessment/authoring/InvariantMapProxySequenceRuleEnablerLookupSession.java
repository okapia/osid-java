//
// InvariantMapProxySequenceRuleEnablerLookupSession
//
//    Implements a SequenceRuleEnabler lookup service backed by a fixed
//    collection of sequenceRuleEnablers. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.assessment.authoring;


/**
 *  Implements a SequenceRuleEnabler lookup service backed by a fixed
 *  collection of sequence rule enablers. The sequence rule enablers are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxySequenceRuleEnablerLookupSession
    extends net.okapia.osid.jamocha.core.assessment.authoring.spi.AbstractMapSequenceRuleEnablerLookupSession
    implements org.osid.assessment.authoring.SequenceRuleEnablerLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxySequenceRuleEnablerLookupSession} with no
     *  sequence rule enablers.
     *
     *  @param bank the bank
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bank} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxySequenceRuleEnablerLookupSession(org.osid.assessment.Bank bank,
                                                  org.osid.proxy.Proxy proxy) {
        setBank(bank);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxySequenceRuleEnablerLookupSession} with a single
     *  sequence rule enabler.
     *
     *  @param bank the bank
     *  @param sequenceRuleEnabler a single sequence rule enabler
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bank},
     *          {@code sequenceRuleEnabler} or {@code proxy} is {@code null}
     */

    public InvariantMapProxySequenceRuleEnablerLookupSession(org.osid.assessment.Bank bank,
                                                  org.osid.assessment.authoring.SequenceRuleEnabler sequenceRuleEnabler, org.osid.proxy.Proxy proxy) {

        this(bank, proxy);
        putSequenceRuleEnabler(sequenceRuleEnabler);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxySequenceRuleEnablerLookupSession} using
     *  an array of sequence rule enablers.
     *
     *  @param bank the bank
     *  @param sequenceRuleEnablers an array of sequence rule enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bank},
     *          {@code sequenceRuleEnablers} or {@code proxy} is {@code null}
     */

    public InvariantMapProxySequenceRuleEnablerLookupSession(org.osid.assessment.Bank bank,
                                                  org.osid.assessment.authoring.SequenceRuleEnabler[] sequenceRuleEnablers, org.osid.proxy.Proxy proxy) {

        this(bank, proxy);
        putSequenceRuleEnablers(sequenceRuleEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxySequenceRuleEnablerLookupSession} using a
     *  collection of sequence rule enablers.
     *
     *  @param bank the bank
     *  @param sequenceRuleEnablers a collection of sequence rule enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bank},
     *          {@code sequenceRuleEnablers} or {@code proxy} is {@code null}
     */

    public InvariantMapProxySequenceRuleEnablerLookupSession(org.osid.assessment.Bank bank,
                                                  java.util.Collection<? extends org.osid.assessment.authoring.SequenceRuleEnabler> sequenceRuleEnablers,
                                                  org.osid.proxy.Proxy proxy) {

        this(bank, proxy);
        putSequenceRuleEnablers(sequenceRuleEnablers);
        return;
    }
}
