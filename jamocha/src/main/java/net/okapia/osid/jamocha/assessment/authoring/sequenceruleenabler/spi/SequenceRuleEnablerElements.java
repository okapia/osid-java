//
// SequenceRuleEnablerElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.authoring.sequenceruleenabler.spi;

/**
 *  Ids for object elements for use in forms and queries.
 */

public class SequenceRuleEnablerElements
    extends net.okapia.osid.jamocha.spi.OsidEnablerElements {


    /**
     *  Gets the SequenceRuleEnablerElement Id.
     *
     *  @return the sequence rule enabler element Id
     */

    public static org.osid.id.Id getSequenceRuleEnablerEntityId() {
        return (makeEntityId("osid.assessment.authoring.SequenceRuleEnabler"));
    }


    /**
     *  Gets the RuledSequenceRuleId element Id.
     *
     *  @return the RuledSequenceRuleId element Id
     */

    public static org.osid.id.Id getRuledSequenceRuleId() {
        return (makeQueryElementId("osid.assessment.authoring.sequenceruleenabler.RuledSequenceRuleId"));
    }


    /**
     *  Gets the RuledSequenceRule element Id.
     *
     *  @return the RuledSequenceRule element Id
     */

    public static org.osid.id.Id getRuledSequenceRule() {
        return (makeQueryElementId("osid.assessment.authoring.sequenceruleenabler.RuledSequenceRule"));
    }


    /**
     *  Gets the BankId element Id.
     *
     *  @return the BankId element Id
     */

    public static org.osid.id.Id getBankId() {
        return (makeQueryElementId("osid.assessment.authoring.sequenceruleenabler.BankId"));
    }


    /**
     *  Gets the Bank element Id.
     *
     *  @return the Bank element Id
     */

    public static org.osid.id.Id getBank() {
        return (makeQueryElementId("osid.assessment.authoring.sequenceruleenabler.Bank"));
    }
}
