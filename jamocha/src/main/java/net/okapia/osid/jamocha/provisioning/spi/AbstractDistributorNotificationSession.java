//
// AbstractDistributorNotificationSession.java
//
//     A template for making DistributorNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Distributor} objects. This session is
 *  intended for consumers needing to synchronize their state with
 *  this service without the use of polling. Notifications are
 *  cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Distributor} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for distributor entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractDistributorNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.provisioning.DistributorNotificationSession {


    /**
     *  Tests if this user can register for {@code Distributor}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForDistributorNotifications() {
        return (true);
    }


    /**
     *  Register for notifications of new distributors. {@code
     *  DistributorReceiver.newDistributor()} is invoked when a new
     *  {@code Distributor} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewDistributors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that introduces a new ancestor of the specified
     *  distributor. {@code
     *  DistributorReceiver.newAncestorDistributor()} is invoked when
     *  the specified distributor node gets a new ancestor.
     *
     *  @param distributorId the {@code Id} of the
     *         {@code Distributor} node to monitor
     *  @throws org.osid.NullArgumentException {@code distributorId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewDistributorAncestors(org.osid.id.Id distributorId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that introduces a new descendant of the specified
     *  distributor. {@code
     *  DistributorReceiver.newDescendantDistributor()} is invoked
     *  when the specified distributor node gets a new descendant.
     *
     *  @param distributorId the {@code Id} of the
     *         {@code Distributor} node to monitor
     *  @throws org.osid.NullArgumentException {@code distributorId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewDistributorDescendants(org.osid.id.Id distributorId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated distributors. {@code
     *  DistributorReceiver.changedDistributor()} is invoked when a
     *  distributor is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedDistributors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated distributor. {@code
     *  DistributorReceiver.changedDistributor()} is invoked when the
     *  specified distributor is changed.
     *
     *  @param distributorId the {@code Id} of the {@code Distributor} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code distributorId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedDistributor(org.osid.id.Id distributorId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted distributors. {@code
     *  DistributorReceiver.deletedDistributor()} is invoked when a
     *  distributor is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedDistributors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted distributor. {@code
     *  DistributorReceiver.deletedDistributor()} is invoked when the
     *  specified distributor is deleted.
     *
     *  @param distributorId the {@code Id} of the
     *          {@code Distributor} to monitor
     *  @throws org.osid.NullArgumentException {@code distributorId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedDistributor(org.osid.id.Id distributorId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that removes an ancestor of the specified distributor. {@code
     *  DistributorReceiver.deletedAncestor()} is invoked when the
     *  specified distributor node loses an ancestor.
     *
     *  @param distributorId the {@code Id} of the
     *         {@code Distributor} node to monitor
     *  @throws org.osid.NullArgumentException {@code distributorId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedDistributorAncestors(org.osid.id.Id distributorId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that removes a descendant of the specified distributor. {@code
     *  DistributorReceiver.deletedDescendant()} is invoked when the
     *  specified distributor node loses a descendant.
     *
     *  @param distributorId the {@code Id} of the
     *          {@code Distributor} node to monitor
     *  @throws org.osid.NullArgumentException {@code distributorId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedDistributorDescendants(org.osid.id.Id distributorId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
