//
// AssessmentTakenValidator.java
//
//     Validates an AssessmentTaken.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.assessment.assessmenttaken;


/**
 *  Validates an AssessmentTaken.
 */

public final class AssessmentTakenValidator
    extends net.okapia.osid.jamocha.builder.validator.assessment.assessmenttaken.spi.AbstractAssessmentTakenValidator {


    /**
     *  Constructs a new <code>AssessmentTakenValidator</code>.
     */

    public AssessmentTakenValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AssessmentTakenValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    public AssessmentTakenValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates an AssessmentTaken with a default validation.
     *
     *  @param assessmentTaken an assessment taken to validate
     *  @return the assessment taken
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>assessmentTaken</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred
     */

    public static org.osid.assessment.AssessmentTaken validateAssessmentTaken(org.osid.assessment.AssessmentTaken assessmentTaken) {
        AssessmentTakenValidator validator = new AssessmentTakenValidator();
        validator.validate(assessmentTaken);
        return (assessmentTaken);
    }


    /**
     *  Validates an AssessmentTaken for the given validations.
     *
     *  @param validation an EnumSet of validations
     *  @param assessmentTaken an assessment taken to validate
     *  @return the assessment taken
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          or <code>assessmentTaken</code> is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred
     */

    public static org.osid.assessment.AssessmentTaken validateAssessmentTaken(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation,
                                                       org.osid.assessment.AssessmentTaken assessmentTaken) {

        AssessmentTakenValidator validator = new AssessmentTakenValidator(validation);
        validator.validate(assessmentTaken);
        return (assessmentTaken);
    }
}
