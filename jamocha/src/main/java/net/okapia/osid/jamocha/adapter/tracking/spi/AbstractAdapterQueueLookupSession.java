//
// AbstractAdapterQueueLookupSession.java
//
//    A Queue lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.tracking.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Queue lookup session adapter.
 */

public abstract class AbstractAdapterQueueLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.tracking.QueueLookupSession {

    private final org.osid.tracking.QueueLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterQueueLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterQueueLookupSession(org.osid.tracking.QueueLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code FrontOffice/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code FrontOffice Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFrontOfficeId() {
        return (this.session.getFrontOfficeId());
    }


    /**
     *  Gets the {@code FrontOffice} associated with this session.
     *
     *  @return the {@code FrontOffice} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOffice getFrontOffice()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getFrontOffice());
    }


    /**
     *  Tests if this user can perform {@code Queue} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupQueues() {
        return (this.session.canLookupQueues());
    }


    /**
     *  A complete view of the {@code Queue} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeQueueView() {
        this.session.useComparativeQueueView();
        return;
    }


    /**
     *  A complete view of the {@code Queue} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryQueueView() {
        this.session.usePlenaryQueueView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include queues in front offices which are children
     *  of this front office in the front office hierarchy.
     */

    @OSID @Override
    public void useFederatedFrontOfficeView() {
        this.session.useFederatedFrontOfficeView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this front office only.
     */

    @OSID @Override
    public void useIsolatedFrontOfficeView() {
        this.session.useIsolatedFrontOfficeView();
        return;
    }
    

    /**
     *  Only active queues are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveQueueView() {
        this.session.useActiveQueueView();
        return;
    }


    /**
     *  Active and inactive queues are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusQueueView() {
        this.session.useAnyStatusQueueView();
        return;
    }
    
     
    /**
     *  Gets the {@code Queue} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Queue} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Queue} and
     *  retained for compatibility.
     *
     *  In active mode, queues are returned that are currently
     *  active. In any status mode, active and inactive queues
     *  are returned.
     *
     *  @param queueId {@code Id} of the {@code Queue}
     *  @return the queue
     *  @throws org.osid.NotFoundException {@code queueId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code queueId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.Queue getQueue(org.osid.id.Id queueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getQueue(queueId));
    }


    /**
     *  Gets a {@code QueueList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  queues specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Queues} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, queues are returned that are currently
     *  active. In any status mode, active and inactive queues
     *  are returned.
     *
     *  @param  queueIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Queue} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code queueIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.QueueList getQueuesByIds(org.osid.id.IdList queueIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getQueuesByIds(queueIds));
    }


    /**
     *  Gets a {@code QueueList} corresponding to the given
     *  queue genus {@code Type} which does not include
     *  queues of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  queues or an error results. Otherwise, the returned list
     *  may contain only those queues that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, queues are returned that are currently
     *  active. In any status mode, active and inactive queues
     *  are returned.
     *
     *  @param  queueGenusType a queue genus type 
     *  @return the returned {@code Queue} list
     *  @throws org.osid.NullArgumentException
     *          {@code queueGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.QueueList getQueuesByGenusType(org.osid.type.Type queueGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getQueuesByGenusType(queueGenusType));
    }


    /**
     *  Gets a {@code QueueList} corresponding to the given
     *  queue genus {@code Type} and include any additional
     *  queues with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  queues or an error results. Otherwise, the returned list
     *  may contain only those queues that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, queues are returned that are currently
     *  active. In any status mode, active and inactive queues
     *  are returned.
     *
     *  @param  queueGenusType a queue genus type 
     *  @return the returned {@code Queue} list
     *  @throws org.osid.NullArgumentException
     *          {@code queueGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.QueueList getQueuesByParentGenusType(org.osid.type.Type queueGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getQueuesByParentGenusType(queueGenusType));
    }


    /**
     *  Gets a {@code QueueList} containing the given
     *  queue record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  queues or an error results. Otherwise, the returned list
     *  may contain only those queues that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, queues are returned that are currently
     *  active. In any status mode, active and inactive queues
     *  are returned.
     *
     *  @param  queueRecordType a queue record type 
     *  @return the returned {@code Queue} list
     *  @throws org.osid.NullArgumentException
     *          {@code queueRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.QueueList getQueuesByRecordType(org.osid.type.Type queueRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getQueuesByRecordType(queueRecordType));
    }


    /**
     *  Gets a {@code QueueList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  queues or an error results. Otherwise, the returned list
     *  may contain only those queues that are accessible through
     *  this session.
     *
     *  In active mode, queues are returned that are currently
     *  active. In any status mode, active and inactive queues
     *  are returned.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Queue} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.tracking.QueueList getQueuesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getQueuesByProvider(resourceId));
    }


    /**
     *  Gets all {@code Queues}. 
     *
     *  In plenary mode, the returned list contains all known
     *  queues or an error results. Otherwise, the returned list
     *  may contain only those queues that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, queues are returned that are currently
     *  active. In any status mode, active and inactive queues
     *  are returned.
     *
     *  @return a list of {@code Queues} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.QueueList getQueues()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getQueues());
    }
}
