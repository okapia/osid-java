//
// AbstractImmutableResourceLocation.java
//
//     Wraps a mutable ResourceLocation to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.mapping.resourcelocation.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>ResourceLocation</code> to hide
 *  modifiers. This wrapper provides an immutized ResourceLocation
 *  from the point of view external to the builder. Methods are passed
 *  through to the underlying ResourceLocation whose state changes are
 *  visible.
 */

public abstract class AbstractImmutableResourceLocation
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidCompendium
    implements org.osid.mapping.ResourceLocation {

    private final org.osid.mapping.ResourceLocation resourceLocation;


    /**
     *  Constructs a new <code>AbstractImmutableResourceLocation</code>.
     *
     *  @param resourceLocation the resource location to immutablize
     *  @throws org.osid.NullArgumentException
     *          <code>resourceLocation</code> is <code>null</code>
     */

    protected AbstractImmutableResourceLocation(org.osid.mapping.ResourceLocation resourceLocation) {
        super(resourceLocation);
        this.resourceLocation = resourceLocation;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the resource.
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResourceId() {
        return (this.resourceLocation.getResourceId());
    }


    /**
     *  Gets the resource on the route. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource()
        throws org.osid.OperationFailedException {
        
        return (this.resourceLocation.getResource());
    }


    /**
     *  Gets the <code> Id </code> of the location.
     *
     *  @return the location <code> Id </code>
     *  @throws org.osid.IllegalStateException <code> hasLocation() </code> is 
     *          <code> false </code> 

     */

    @OSID @Override
    public org.osid.id.Id getLocationId() {
        return (this.resourceLocation.getLocationId());
    }


    /**
     *  Tests if this resource has a known location. 
     *
     *  @return <code> true </code> if a location is known, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasLocation() {
        return (this.resourceLocation.hasLocation());
    }


    /**
     *  Gets the location of the resource. 
     *
     *  @return the location of the resource 
     *  @throws org.osid.IllegalStateException <code> hasLocation() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.mapping.Location getLocation()
        throws org.osid.OperationFailedException {
        
        return (this.resourceLocation.getLocation());
    }


    /**
     *  Tests if this resource has a known coordinate.
     *
     *  @return <code> true </code> if a coordinate is known, <code> false
     *          </code> otherwise
     */

    @OSID @Override
    public boolean hasCoordinate() {
        return (this.resourceLocation.hasCoordinate());
    }


    /**
     *  Gets the coordinate of the resource.
     *
     *  @return the coordinate of the resource
     *  @throws org.osid.IllegalStateException <code> hasCoordinate() </code>
     *          is <code> false </code>
     */

    @OSID @Override
    public org.osid.mapping.Coordinate getCoordinate() {
        return (this.resourceLocation.getCoordinate());
    }


    /**
     *  Gets the resource location record corresponding to the given
     *  <code> ResourceLocation </code> record <code> Type. </code>
     *  This method is used to retrieve an object implementing the
     *  requested record.. The <code> resourceLocationRecordType
     *  </code> may be the <code> Type </code> returned in <code>
     *  getRecordTypes() </code> or any of its parents in a <code>
     *  Type </code> hierarchy where <code>
     *  hasRecordType(resourceLocationRecordType) </code> is <code>
     *  true </code> .
     *
     *  @param resourceLocationRecordType the type of resource
     *         location record to retrieve
     *  @return the resource location record 
     *  @throws org.osid.NullArgumentException <code> resourceLocationRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(resourceLocationRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.records.ResourceLocationRecord getResourceLocationRecord(org.osid.type.Type resourceLocationRecordType)
        throws org.osid.OperationFailedException {

        return (this.resourceLocation.getResourceLocationRecord(resourceLocationRecordType));
    }
}

