//
// AbstractUnknownLogEntry.java
//
//     Defines an unknown LogEntry.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.logging.logentry.spi;


/**
 *  Defines an unknown <code>LogEntry</code>.
 */

public abstract class AbstractUnknownLogEntry
    extends net.okapia.osid.jamocha.logging.logentry.spi.AbstractLogEntry
    implements org.osid.logging.LogEntry {

    protected static final String OBJECT = "osid.logging.LogEntry";


    /**
     *  Constructs a new <code>AbstractUnknownLogEntry</code>.
     */

    public AbstractUnknownLogEntry() {
        setId(net.okapia.osid.jamocha.nil.privateutil.UnknownId.valueOf(OBJECT));
        setDisplayName(net.okapia.osid.jamocha.nil.privateutil.DisplayName.valueOf(OBJECT));
        setDescription(net.okapia.osid.jamocha.nil.privateutil.Description.valueOf(OBJECT));

        setPriority(new net.okapia.osid.primordium.type.BasicType("okapia.net", "osid.logging.LogEntry", "nil"));
        setTimestamp(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.unknown());
        setResource(new net.okapia.osid.jamocha.nil.resource.resource.UnknownResource());
        setAgent(new net.okapia.osid.jamocha.nil.authentication.agent.UnknownAgent());
        
        return;
    }


    /**
     *  Constructs a new <code>AbstractUnknownLogEntry</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public AbstractUnknownLogEntry(boolean optional) {
        this();
        return;
    }
}
