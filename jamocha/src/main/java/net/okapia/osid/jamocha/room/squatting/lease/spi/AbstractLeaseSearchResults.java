//
// AbstractLeaseSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.squatting.lease.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractLeaseSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.room.squatting.LeaseSearchResults {

    private org.osid.room.squatting.LeaseList leases;
    private final org.osid.room.squatting.LeaseQueryInspector inspector;
    private final java.util.Collection<org.osid.room.squatting.records.LeaseSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractLeaseSearchResults.
     *
     *  @param leases the result set
     *  @param leaseQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>leases</code>
     *          or <code>leaseQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractLeaseSearchResults(org.osid.room.squatting.LeaseList leases,
                                            org.osid.room.squatting.LeaseQueryInspector leaseQueryInspector) {
        nullarg(leases, "leases");
        nullarg(leaseQueryInspector, "lease query inspectpr");

        this.leases = leases;
        this.inspector = leaseQueryInspector;

        return;
    }


    /**
     *  Gets the lease list resulting from a search.
     *
     *  @return a lease list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeases() {
        if (this.leases == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.room.squatting.LeaseList leases = this.leases;
        this.leases = null;
	return (leases);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.room.squatting.LeaseQueryInspector getLeaseQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  lease search record <code> Type. </code> This method must
     *  be used to retrieve a lease implementing the requested
     *  record.
     *
     *  @param leaseSearchRecordType a lease search 
     *         record type 
     *  @return the lease search
     *  @throws org.osid.NullArgumentException
     *          <code>leaseSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(leaseSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.room.squatting.records.LeaseSearchResultsRecord getLeaseSearchResultsRecord(org.osid.type.Type leaseSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.room.squatting.records.LeaseSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(leaseSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(leaseSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record lease search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addLeaseRecord(org.osid.room.squatting.records.LeaseSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "lease record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
