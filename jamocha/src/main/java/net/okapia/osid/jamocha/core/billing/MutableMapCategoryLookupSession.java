//
// MutableMapCategoryLookupSession
//
//    Implements a Category lookup service backed by a collection of
//    categories that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.billing;


/**
 *  Implements a Category lookup service backed by a collection of
 *  categories. The categories are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of categories can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapCategoryLookupSession
    extends net.okapia.osid.jamocha.core.billing.spi.AbstractMapCategoryLookupSession
    implements org.osid.billing.CategoryLookupSession {


    /**
     *  Constructs a new {@code MutableMapCategoryLookupSession}
     *  with no categories.
     *
     *  @param business the business
     *  @throws org.osid.NullArgumentException {@code business} is
     *          {@code null}
     */

      public MutableMapCategoryLookupSession(org.osid.billing.Business business) {
        setBusiness(business);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapCategoryLookupSession} with a
     *  single category.
     *
     *  @param business the business  
     *  @param category a category
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code category} is {@code null}
     */

    public MutableMapCategoryLookupSession(org.osid.billing.Business business,
                                           org.osid.billing.Category category) {
        this(business);
        putCategory(category);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapCategoryLookupSession}
     *  using an array of categories.
     *
     *  @param business the business
     *  @param categories an array of categories
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code categories} is {@code null}
     */

    public MutableMapCategoryLookupSession(org.osid.billing.Business business,
                                           org.osid.billing.Category[] categories) {
        this(business);
        putCategories(categories);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapCategoryLookupSession}
     *  using a collection of categories.
     *
     *  @param business the business
     *  @param categories a collection of categories
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code categories} is {@code null}
     */

    public MutableMapCategoryLookupSession(org.osid.billing.Business business,
                                           java.util.Collection<? extends org.osid.billing.Category> categories) {

        this(business);
        putCategories(categories);
        return;
    }

    
    /**
     *  Makes a {@code Category} available in this session.
     *
     *  @param category a category
     *  @throws org.osid.NullArgumentException {@code category{@code  is
     *          {@code null}
     */

    @Override
    public void putCategory(org.osid.billing.Category category) {
        super.putCategory(category);
        return;
    }


    /**
     *  Makes an array of categories available in this session.
     *
     *  @param categories an array of categories
     *  @throws org.osid.NullArgumentException {@code categories{@code 
     *          is {@code null}
     */

    @Override
    public void putCategories(org.osid.billing.Category[] categories) {
        super.putCategories(categories);
        return;
    }


    /**
     *  Makes collection of categories available in this session.
     *
     *  @param categories a collection of categories
     *  @throws org.osid.NullArgumentException {@code categories{@code  is
     *          {@code null}
     */

    @Override
    public void putCategories(java.util.Collection<? extends org.osid.billing.Category> categories) {
        super.putCategories(categories);
        return;
    }


    /**
     *  Removes a Category from this session.
     *
     *  @param categoryId the {@code Id} of the category
     *  @throws org.osid.NullArgumentException {@code categoryId{@code 
     *          is {@code null}
     */

    @Override
    public void removeCategory(org.osid.id.Id categoryId) {
        super.removeCategory(categoryId);
        return;
    }    
}
