//
// CyclicTimePeriodFilterList.java
//
//     Implements a filtering CyclicTimePeriodList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.calendaring.cycle.cyclictimeperiod;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filtering CyclicTimePeriodList.
 */

public final class CyclicTimePeriodFilterList
    extends net.okapia.osid.jamocha.inline.filter.calendaring.cycle.cyclictimeperiod.spi.AbstractCyclicTimePeriodFilterList
    implements org.osid.calendaring.cycle.CyclicTimePeriodList,
               CyclicTimePeriodFilter {

    private final CyclicTimePeriodFilter filter;


    /**
     *  Creates a new <code>CyclicTimePeriodFilterList</code>.
     *
     *  @param filter an inline query filter
     *  @param list a <code>CyclicTimePeriodList</code>
     *  @throws org.osid.NullArgumentException <code>filter</code> or
     *          <code>list</code> is <code>null</code>
     */

    public CyclicTimePeriodFilterList(CyclicTimePeriodFilter filter, org.osid.calendaring.cycle.CyclicTimePeriodList list) {
        super(list);

        nullarg(filter, "inline query filter");
        this.filter = filter;

        return;
    }    

    
    /**
     *  Filters CyclicTimePeriods.
     *
     *  @param cyclicTimePeriod the cyclic time period to filter
     *  @return <code>true</code> if the cyclic time period passes the filter,
     *          <code>false</code> if the cyclic time period should be filtered
     */

    @Override
    public boolean pass(org.osid.calendaring.cycle.CyclicTimePeriod cyclicTimePeriod) {
        return (this.filter.pass(cyclicTimePeriod));
    }
}
