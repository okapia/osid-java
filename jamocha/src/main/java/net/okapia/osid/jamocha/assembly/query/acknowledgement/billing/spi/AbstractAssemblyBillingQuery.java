//
// AbstractAssemblyBillingQuery.java
//
//     A BillingQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.acknowledgement.billing.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A BillingQuery that stores terms.
 */

public abstract class AbstractAssemblyBillingQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.acknowledgement.BillingQuery,
               org.osid.acknowledgement.BillingQueryInspector,
               org.osid.acknowledgement.BillingSearchOrder {

    private final java.util.Collection<org.osid.acknowledgement.records.BillingQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.acknowledgement.records.BillingQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.acknowledgement.records.BillingSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyBillingQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyBillingQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the credit <code> Id </code> for this query to match credits 
     *  assigned to billings. 
     *
     *  @param  creditId a credit <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> creditId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCreditId(org.osid.id.Id creditId, boolean match) {
        getAssembler().addIdTerm(getCreditIdColumn(), creditId, match);
        return;
    }


    /**
     *  Clears all credit <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCreditIdTerms() {
        getAssembler().clearTerms(getCreditIdColumn());
        return;
    }


    /**
     *  Gets the credit <code> Id </code> query terms. 
     *
     *  @return the credit <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCreditIdTerms() {
        return (getAssembler().getIdTerms(getCreditIdColumn()));
    }


    /**
     *  Gets the CreditId column name.
     *
     * @return the column name
     */

    protected String getCreditIdColumn() {
        return ("credit_id");
    }


    /**
     *  Tests if a credit query is available. 
     *
     *  @return <code> true </code> if a credit query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreditQuery() {
        return (false);
    }


    /**
     *  Gets the query for a billing. 
     *
     *  @return the credit query 
     *  @throws org.osid.UnimplementedException <code> supportsCreditQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditQuery getCreditQuery() {
        throw new org.osid.UnimplementedException("supportsCreditQuery() is false");
    }


    /**
     *  Matches billings with any credit. 
     *
     *  @param  match <code> true </code> to match billings with any credit, 
     *          <code> false </code> to match billings with no credits 
     */

    @OSID @Override
    public void matchAnyCredit(boolean match) {
        getAssembler().addIdWildcardTerm(getCreditColumn(), match);
        return;
    }


    /**
     *  Clears all credit terms. 
     */

    @OSID @Override
    public void clearCreditTerms() {
        getAssembler().clearTerms(getCreditColumn());
        return;
    }


    /**
     *  Gets the credit query terms. 
     *
     *  @return the credit terms 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditQueryInspector[] getCreditTerms() {
        return (new org.osid.acknowledgement.CreditQueryInspector[0]);
    }


    /**
     *  Gets the Credit column name.
     *
     * @return the column name
     */

    protected String getCreditColumn() {
        return ("credit");
    }


    /**
     *  Sets the billing <code> Id </code> for this query to match billings 
     *  that have the specified billing as an ancestor. 
     *
     *  @param  billingId a billing <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> billingId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorBillingId(org.osid.id.Id billingId, boolean match) {
        getAssembler().addIdTerm(getAncestorBillingIdColumn(), billingId, match);
        return;
    }


    /**
     *  Clears all ancestor billing <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorBillingIdTerms() {
        getAssembler().clearTerms(getAncestorBillingIdColumn());
        return;
    }


    /**
     *  Gets the ancestor billing <code> Id </code> query terms. 
     *
     *  @return the ancestor billing <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorBillingIdTerms() {
        return (getAssembler().getIdTerms(getAncestorBillingIdColumn()));
    }


    /**
     *  Gets the AncestorBillingId column name.
     *
     * @return the column name
     */

    protected String getAncestorBillingIdColumn() {
        return ("ancestor_billing_id");
    }


    /**
     *  Tests if a <code> BillingQuery </code> is available. 
     *
     *  @return <code> true </code> if a billing query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorBillingQuery() {
        return (false);
    }


    /**
     *  Gets the query for a billing. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the billing query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorBillingQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingQuery getAncestorBillingQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorBillingQuery() is false");
    }


    /**
     *  Matches billings with any ancestor. 
     *
     *  @param  match <code> true </code> to match billings with any ancestor, 
     *          <code> false </code> to match root billings 
     */

    @OSID @Override
    public void matchAnyAncestorBilling(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorBillingColumn(), match);
        return;
    }


    /**
     *  Clears all ancestor billing terms. 
     */

    @OSID @Override
    public void clearAncestorBillingTerms() {
        getAssembler().clearTerms(getAncestorBillingColumn());
        return;
    }


    /**
     *  Gets the ancestor billing query terms. 
     *
     *  @return the ancestor billing terms 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingQueryInspector[] getAncestorBillingTerms() {
        return (new org.osid.acknowledgement.BillingQueryInspector[0]);
    }


    /**
     *  Gets the AncestorBilling column name.
     *
     * @return the column name
     */

    protected String getAncestorBillingColumn() {
        return ("ancestor_billing");
    }


    /**
     *  Sets the billing <code> Id </code> for this query to match billings 
     *  that have the specified billing as a descendant. 
     *
     *  @param  billingId a billing <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> billingId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantBillingId(org.osid.id.Id billingId, 
                                         boolean match) {
        getAssembler().addIdTerm(getDescendantBillingIdColumn(), billingId, match);
        return;
    }


    /**
     *  Clears all descendant billing <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantBillingIdTerms() {
        getAssembler().clearTerms(getDescendantBillingIdColumn());
        return;
    }


    /**
     *  Gets the descendant billing <code> Id </code> query terms. 
     *
     *  @return the descendant billing <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantBillingIdTerms() {
        return (getAssembler().getIdTerms(getDescendantBillingIdColumn()));
    }


    /**
     *  Gets the DescendantBillingId column name.
     *
     * @return the column name
     */

    protected String getDescendantBillingIdColumn() {
        return ("descendant_billing_id");
    }


    /**
     *  Tests if a <code> BillingQuery </code> is available. 
     *
     *  @return <code> true </code> if a billing query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantBillingQuery() {
        return (false);
    }


    /**
     *  Gets the query for a billing. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the billing query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantBillingQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingQuery getDescendantBillingQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantBillingQuery() is false");
    }


    /**
     *  Matches billings with any descendant. 
     *
     *  @param  match <code> true </code> to match billings with any 
     *          descendant, <code> false </code> to match leaf billings 
     */

    @OSID @Override
    public void matchAnyDescendantBilling(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantBillingColumn(), match);
        return;
    }


    /**
     *  Clears all descendant billing terms. 
     */

    @OSID @Override
    public void clearDescendantBillingTerms() {
        getAssembler().clearTerms(getDescendantBillingColumn());
        return;
    }


    /**
     *  Gets the descendant billing query terms. 
     *
     *  @return the descendant billing terms 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingQueryInspector[] getDescendantBillingTerms() {
        return (new org.osid.acknowledgement.BillingQueryInspector[0]);
    }


    /**
     *  Gets the DescendantBilling column name.
     *
     * @return the column name
     */

    protected String getDescendantBillingColumn() {
        return ("descendant_billing");
    }


    /**
     *  Tests if this billing supports the given record
     *  <code>Type</code>.
     *
     *  @param  billingRecordType a billing record type 
     *  @return <code>true</code> if the billingRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>billingRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type billingRecordType) {
        for (org.osid.acknowledgement.records.BillingQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(billingRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  billingRecordType the billing record type 
     *  @return the billing query record 
     *  @throws org.osid.NullArgumentException
     *          <code>billingRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(billingRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.acknowledgement.records.BillingQueryRecord getBillingQueryRecord(org.osid.type.Type billingRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.acknowledgement.records.BillingQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(billingRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(billingRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  billingRecordType the billing record type 
     *  @return the billing query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>billingRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(billingRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.acknowledgement.records.BillingQueryInspectorRecord getBillingQueryInspectorRecord(org.osid.type.Type billingRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.acknowledgement.records.BillingQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(billingRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(billingRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param billingRecordType the billing record type
     *  @return the billing search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>billingRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(billingRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.acknowledgement.records.BillingSearchOrderRecord getBillingSearchOrderRecord(org.osid.type.Type billingRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.acknowledgement.records.BillingSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(billingRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(billingRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this billing. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param billingQueryRecord the billing query record
     *  @param billingQueryInspectorRecord the billing query inspector
     *         record
     *  @param billingSearchOrderRecord the billing search order record
     *  @param billingRecordType billing record type
     *  @throws org.osid.NullArgumentException
     *          <code>billingQueryRecord</code>,
     *          <code>billingQueryInspectorRecord</code>,
     *          <code>billingSearchOrderRecord</code> or
     *          <code>billingRecordTypebilling</code> is
     *          <code>null</code>
     */
            
    protected void addBillingRecords(org.osid.acknowledgement.records.BillingQueryRecord billingQueryRecord, 
                                      org.osid.acknowledgement.records.BillingQueryInspectorRecord billingQueryInspectorRecord, 
                                      org.osid.acknowledgement.records.BillingSearchOrderRecord billingSearchOrderRecord, 
                                      org.osid.type.Type billingRecordType) {

        addRecordType(billingRecordType);

        nullarg(billingQueryRecord, "billing query record");
        nullarg(billingQueryInspectorRecord, "billing query inspector record");
        nullarg(billingSearchOrderRecord, "billing search odrer record");

        this.queryRecords.add(billingQueryRecord);
        this.queryInspectorRecords.add(billingQueryInspectorRecord);
        this.searchOrderRecords.add(billingSearchOrderRecord);
        
        return;
    }
}
