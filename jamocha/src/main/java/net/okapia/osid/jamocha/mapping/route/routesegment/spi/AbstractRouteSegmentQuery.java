//
// AbstractRouteSegmentQuery.java
//
//     A template for making a RouteSegment Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.route.routesegment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for route segments.
 */

public abstract class AbstractRouteSegmentQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.mapping.route.RouteSegmentQuery {

    private final java.util.Collection<org.osid.mapping.route.records.RouteSegmentQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches the starting instructions for the segment. 
     *
     *  @param  startingInstructions the starting instructions 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.InvalidArgumentException <code> startingInstructions 
     *          </code> is not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> startingInstructions 
     *          </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchStartingInstructions(String startingInstructions, 
                                          org.osid.type.Type stringMatchType, 
                                          boolean match) {
        return;
    }


    /**
     *  Matches segments that have any starting instructions. 
     *
     *  @param  match <code> true </code> to match segments with any starting 
     *          instructions, <code> false </code> to match segments with no 
     *          starting instriuctions 
     */

    @OSID @Override
    public void matchAnyStartingInstructions(boolean match) {
        return;
    }


    /**
     *  Clears the starting instruction query terms. 
     */

    @OSID @Override
    public void clearStartingInstructionsTerms() {
        return;
    }


    /**
     *  Matches the ending instructions for the segment. 
     *
     *  @param  endingInstructions the ending instructions 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.InvalidArgumentException <code> endingInstructions 
     *          </code> is not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> endingInstructions 
     *          </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchEndingInstructions(String endingInstructions, 
                                        org.osid.type.Type stringMatchType, 
                                        boolean match) {
        return;
    }


    /**
     *  Matches segments that have any ending instructions. 
     *
     *  @param  match <code> true </code> to match segments with any ending 
     *          instructions, <code> false </code> to match segments with no 
     *          ending instriuctions 
     */

    @OSID @Override
    public void matchAnyEndingInstructions(boolean match) {
        return;
    }


    /**
     *  Clears the ending instruction query terms. 
     */

    @OSID @Override
    public void clearEndingInstructionsTerms() {
        return;
    }


    /**
     *  Matches route segments with distances in the specified range. 
     *
     *  @param  from starting range 
     *  @param  to ending range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> ie less 
     *          than <code> from </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDistance(org.osid.mapping.Distance from, 
                              org.osid.mapping.Distance to, boolean match) {
        return;
    }


    /**
     *  Clears the distance query terms. 
     */

    @OSID @Override
    public void clearDistanceTerms() {
        return;
    }


    /**
     *  Matches route segments with estimated ravel times in the specified 
     *  range. 
     *
     *  @param  from starting range 
     *  @param  to ending range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> ie less 
     *          than <code> from </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchETA(org.osid.calendaring.Duration from, 
                         org.osid.calendaring.Duration to, boolean match) {
        return;
    }


    /**
     *  Clears the ETA query terms. 
     */

    @OSID @Override
    public void clearETATerms() {
        return;
    }


    /**
     *  Sets the path <code> Id </code> for this query to match route segments 
     *  containing paths. 
     *
     *  @param  pathId the path <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> pathId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchPathId(org.osid.id.Id pathId, boolean match) {
        return;
    }


    /**
     *  Clears the path <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearPathIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PathQuery </code> is available. 
     *
     *  @return <code> true </code> if a path query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathQuery() {
        return (false);
    }


    /**
     *  Gets the query for a path. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the path query 
     *  @throws org.osid.UnimplementedException <code> supportsPathQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathQuery getPathQuery() {
        throw new org.osid.UnimplementedException("supportsPathQuery() is false");
    }


    /**
     *  Matches route segments that have any path. 
     *
     *  @param  match <code> true </code> to match route segments with any 
     *          path, <code> false </code> to match route segments with no 
     *          path 
     */

    @OSID @Override
    public void matchAnyPath(boolean match) {
        return;
    }


    /**
     *  Clears the path query terms. 
     */

    @OSID @Override
    public void clearPathTerms() {
        return;
    }


    /**
     *  Sets the path <code> Id </code> for this query to match route segments 
     *  containing paths. 
     *
     *  @param  pathId the path <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> pathId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchRouteId(org.osid.id.Id pathId, boolean match) {
        return;
    }


    /**
     *  Clears the route <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRouteIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> RouteQuery </code> is available. 
     *
     *  @return <code> true </code> if a route query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRouteQuery() {
        return (false);
    }


    /**
     *  Gets the query for a route. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the route query 
     *  @throws org.osid.UnimplementedException <code> supportsRouteQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteQuery getRouteQuery() {
        throw new org.osid.UnimplementedException("supportsRouteQuery() is false");
    }


    /**
     *  Clears the route query terms. 
     */

    @OSID @Override
    public void clearRouteTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given route segment query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a route segment implementing the requested record.
     *
     *  @param routeSegmentRecordType a route segment record type
     *  @return the route segment query record
     *  @throws org.osid.NullArgumentException
     *          <code>routeSegmentRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(routeSegmentRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.route.records.RouteSegmentQueryRecord getRouteSegmentQueryRecord(org.osid.type.Type routeSegmentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.route.records.RouteSegmentQueryRecord record : this.records) {
            if (record.implementsRecordType(routeSegmentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(routeSegmentRecordType + " is not supported");
    }


    /**
     *  Adds a record to this route segment query. 
     *
     *  @param routeSegmentQueryRecord route segment query record
     *  @param routeSegmentRecordType routeSegment record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRouteSegmentQueryRecord(org.osid.mapping.route.records.RouteSegmentQueryRecord routeSegmentQueryRecord, 
                                          org.osid.type.Type routeSegmentRecordType) {

        addRecordType(routeSegmentRecordType);
        nullarg(routeSegmentQueryRecord, "route segment query record");
        this.records.add(routeSegmentQueryRecord);        
        return;
    }
}
