//
// AbstractAdapterAuctionHouseLookupSession.java
//
//    An AuctionHouse lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.bidding.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An AuctionHouse lookup session adapter.
 */

public abstract class AbstractAdapterAuctionHouseLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.bidding.AuctionHouseLookupSession {

    private final org.osid.bidding.AuctionHouseLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterAuctionHouseLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterAuctionHouseLookupSession(org.osid.bidding.AuctionHouseLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code AuctionHouse} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupAuctionHouses() {
        return (this.session.canLookupAuctionHouses());
    }


    /**
     *  A complete view of the {@code AuctionHouse} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAuctionHouseView() {
        this.session.useComparativeAuctionHouseView();
        return;
    }


    /**
     *  A complete view of the {@code AuctionHouse} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAuctionHouseView() {
        this.session.usePlenaryAuctionHouseView();
        return;
    }

     
    /**
     *  Gets the {@code AuctionHouse} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code AuctionHouse} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code AuctionHouse} and
     *  retained for compatibility.
     *
     *  @param auctionHouseId {@code Id} of the {@code AuctionHouse}
     *  @return the auction house
     *  @throws org.osid.NotFoundException {@code auctionHouseId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code auctionHouseId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouse getAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuctionHouse(auctionHouseId));
    }


    /**
     *  Gets an {@code AuctionHouseList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  auctionHouses specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code AuctionHouses} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *
     *  @param  auctionHouseIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code AuctionHouse} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code auctionHouseIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseList getAuctionHousesByIds(org.osid.id.IdList auctionHouseIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuctionHousesByIds(auctionHouseIds));
    }


    /**
     *  Gets an {@code AuctionHouseList} corresponding to the given
     *  auction house genus {@code Type} which does not include
     *  auction houses of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known auction
     *  houses or an error results. Otherwise, the returned list may
     *  contain only those auction houses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  auctionHouseGenusType an auctionHouse genus type 
     *  @return the returned {@code AuctionHouse} list
     *  @throws org.osid.NullArgumentException
     *          {@code auctionHouseGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseList getAuctionHousesByGenusType(org.osid.type.Type auctionHouseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuctionHousesByGenusType(auctionHouseGenusType));
    }


    /**
     *  Gets an {@code AuctionHouseList} corresponding to the given
     *  auction house genus {@code Type} and include any additional
     *  auction houses with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  auction houses or an error results. Otherwise, the returned list
     *  may contain only those auction houses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  auctionHouseGenusType an auctionHouse genus type 
     *  @return the returned {@code AuctionHouse} list
     *  @throws org.osid.NullArgumentException
     *          {@code auctionHouseGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseList getAuctionHousesByParentGenusType(org.osid.type.Type auctionHouseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuctionHousesByParentGenusType(auctionHouseGenusType));
    }


    /**
     *  Gets an {@code AuctionHouseList} containing the given
     *  auction house record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  auction houses or an error results. Otherwise, the returned list
     *  may contain only those auction houses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  auctionHouseRecordType an auctionHouse record type 
     *  @return the returned {@code AuctionHouse} list
     *  @throws org.osid.NullArgumentException
     *          {@code auctionHouseRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseList getAuctionHousesByRecordType(org.osid.type.Type auctionHouseRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuctionHousesByRecordType(auctionHouseRecordType));
    }


    /**
     *  Gets an {@code AuctionHouseList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  auction houses or an error results. Otherwise, the returned list
     *  may contain only those auction houses that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code AuctionHouse} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseList getAuctionHousesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuctionHousesByProvider(resourceId));
    }


    /**
     *  Gets all {@code AuctionHouses}. 
     *
     *  In plenary mode, the returned list contains all known
     *  auction houses or an error results. Otherwise, the returned list
     *  may contain only those auction houses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code AuctionHouses} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseList getAuctionHouses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuctionHouses());
    }
}
