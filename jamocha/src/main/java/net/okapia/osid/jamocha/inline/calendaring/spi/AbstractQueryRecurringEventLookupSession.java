//
// AbstractQueryRecurringEventLookupSession.java
//
//    An inline adapter that maps a RecurringEventLookupSession to
//    a RecurringEventQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a RecurringEventLookupSession to
 *  a RecurringEventQuerySession.
 */

public abstract class AbstractQueryRecurringEventLookupSession
    extends net.okapia.osid.jamocha.calendaring.spi.AbstractRecurringEventLookupSession
    implements org.osid.calendaring.RecurringEventLookupSession {

    private boolean activeonly    = false;
    private boolean sequestered   = false;
    private final org.osid.calendaring.RecurringEventQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryRecurringEventLookupSession.
     *
     *  @param querySession the underlying recurring event query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryRecurringEventLookupSession(org.osid.calendaring.RecurringEventQuerySession querySession) {
        nullarg(querySession, "recurring event query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Calendar</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Calendar Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.session.getCalendarId());
    }


    /**
     *  Gets the <code>Calendar</code> associated with this 
     *  session.
     *
     *  @return the <code>Calendar</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCalendar());
    }


    /**
     *  Tests if this user can perform <code>RecurringEvent</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRecurringEvents() {
        return (this.session.canSearchRecurringEvents());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include recurring events in calendars which are children
     *  of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        this.session.useFederatedCalendarView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        this.session.useIsolatedCalendarView();
        return;
    }
    

    /**
     *  Only active recurring events are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveRecurringEventView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive recurring events are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusRecurringEventView() {
        this.activeonly = false;
        return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }


    /**
     *  The returns from the lookup methods omit sequestered
     *  recurring events.
     */

    @OSID @Override
    public void useSequesteredRecurringEventView() {
        this.sequestered = true;
        return;
    }


    /**
     *  All recurring events are returned including sequestered recurring events.
     */

    @OSID @Override
    public void useUnsequesteredRecurringEventView() {
        this.sequestered = false;
        return;
    }


    /**
     *  Tests if a sequestered or unsequestered view is set.
     *
     *  @return <code>true</code> if sequestered</code>,
     *          <code>false</code> if unsequestered
     */

    protected boolean isSequestered() {
        return (this.sequestered);
    }

     
    /**
     *  Gets the <code>RecurringEvent</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>RecurringEvent</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>RecurringEvent</code> and
     *  retained for compatibility.
     *
     *  @param  recurringEventId <code>Id</code> of the
     *          <code>RecurringEvent</code>
     *  @return the recurring event
     *  @throws org.osid.NotFoundException <code>recurringEventId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>recurringEventId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEvent getRecurringEvent(org.osid.id.Id recurringEventId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.RecurringEventQuery query = getQuery();
        query.matchId(recurringEventId, true);
        org.osid.calendaring.RecurringEventList recurringEvents = this.session.getRecurringEventsByQuery(query);
        if (recurringEvents.hasNext()) {
            return (recurringEvents.getNextRecurringEvent());
        } 
        
        throw new org.osid.NotFoundException(recurringEventId + " not found");
    }


    /**
     *  Gets a <code>RecurringEventList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  recurringEvents specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>RecurringEvents</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  recurringEventIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>RecurringEvent</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventList getRecurringEventsByIds(org.osid.id.IdList recurringEventIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.RecurringEventQuery query = getQuery();

        try (org.osid.id.IdList ids = recurringEventIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getRecurringEventsByQuery(query));
    }


    /**
     *  Gets a <code>RecurringEventList</code> corresponding to the given
     *  recurring event genus <code>Type</code> which does not include
     *  recurring events of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  recurring events or an error results. Otherwise, the returned list
     *  may contain only those recurring events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  recurringEventGenusType a recurringEvent genus type 
     *  @return the returned <code>RecurringEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventList getRecurringEventsByGenusType(org.osid.type.Type recurringEventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.RecurringEventQuery query = getQuery();
        query.matchGenusType(recurringEventGenusType, true);
        return (this.session.getRecurringEventsByQuery(query));
    }


    /**
     *  Gets a <code>RecurringEventList</code> corresponding to the given
     *  recurring event genus <code>Type</code> and include any additional
     *  recurring events with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  recurring events or an error results. Otherwise, the returned list
     *  may contain only those recurring events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  recurringEventGenusType a recurringEvent genus type 
     *  @return the returned <code>RecurringEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventList getRecurringEventsByParentGenusType(org.osid.type.Type recurringEventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.RecurringEventQuery query = getQuery();
        query.matchParentGenusType(recurringEventGenusType, true);
        return (this.session.getRecurringEventsByQuery(query));
    }


    /**
     *  Gets a <code>RecurringEventList</code> containing the given
     *  recurring event record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  recurring events or an error results. Otherwise, the returned list
     *  may contain only those recurring events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  recurringEventRecordType a recurringEvent record type 
     *  @return the returned <code>RecurringEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventList getRecurringEventsByRecordType(org.osid.type.Type recurringEventRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.RecurringEventQuery query = getQuery();
        query.matchRecordType(recurringEventRecordType, true);
        return (this.session.getRecurringEventsByQuery(query));
    }


    /**
     *  Gets the <code> RecurringEvents </code> containing the given
     *  schedule slot <code> </code> In plenary mode, the returned
     *  list contains all matching recurring events or an error
     *  results. Otherwise, the returned list may contain only those
     *  recurring events that are accessible through this session.
     *
     *  @param  scheduleSlotId a schedule slot <code> Id </code> 
     *  @return the returned <code> RecurringEvent </code> list 
     *  @throws org.osid.NullArgumentException <code> scheduleSlotId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventList getRecurringEventsByScheduleSlot(org.osid.id.Id scheduleSlotId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.RecurringEventQuery query = getQuery();
        if (query.supportsScheduleQuery()) {
            query.getScheduleQuery().matchScheduleSlotId(scheduleSlotId, true);
            return (this.session.getRecurringEventsByQuery(query));
        }

        return (super.getRecurringEventsByScheduleSlot(scheduleSlotId));
    }


    /**
     *  Gets all <code>RecurringEvents</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  recurring events or an error results. Otherwise, the returned list
     *  may contain only those recurring events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>RecurringEvents</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventList getRecurringEvents()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.RecurringEventQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getRecurringEventsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.calendaring.RecurringEventQuery getQuery() {
        org.osid.calendaring.RecurringEventQuery query = this.session.getRecurringEventQuery();
        
        if (isSequestered()) {
            query.matchSequestered(true);
        }

        return (query);
    }
}
