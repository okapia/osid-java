//
// AbstractPublisherQuery.java
//
//     A template for making a Publisher Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.subscription.publisher.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for publishers.
 */

public abstract class AbstractPublisherQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.subscription.PublisherQuery {

    private final java.util.Collection<org.osid.subscription.records.PublisherQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the subscription <code> Id </code> for this query to match 
     *  subscriptions assigned to publishers. 
     *
     *  @param  subscriptionId a subscription <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> subscriptionId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchSubscriptionId(org.osid.id.Id subscriptionId, 
                                    boolean match) {
        return;
    }


    /**
     *  Clears the subscription <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSubscriptionIdTerms() {
        return;
    }


    /**
     *  Tests if a subscription query is available. 
     *
     *  @return <code> true </code> if a subscription query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriptionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a publisher. 
     *
     *  @return the subscription query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionQuery getSubscriptionQuery() {
        throw new org.osid.UnimplementedException("supportsSubscriptionQuery() is false");
    }


    /**
     *  Matches publishers with any subscription. 
     *
     *  @param  match <code> true </code> to match publishers with any 
     *          subscription, <code> false </code> to match publishers with no 
     *          subscriptions 
     */

    @OSID @Override
    public void matchAnySubscription(boolean match) {
        return;
    }


    /**
     *  Clears the subscription terms. 
     */

    @OSID @Override
    public void clearSubscriptionTerms() {
        return;
    }


    /**
     *  Sets the dispatch <code> Id </code> for this query to match 
     *  subscriptions assigned to dispatches. 
     *
     *  @param  dispatchId a dispatch <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> dispatchId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDispatchId(org.osid.id.Id dispatchId, boolean match) {
        return;
    }


    /**
     *  Clears the dispatch <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDispatchIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> DispatchQuery </code> is available. 
     *
     *  @return <code> true </code> if a dispatch query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDispatchQuery() {
        return (false);
    }


    /**
     *  Gets the query for a dispatch query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the dispatch query 
     *  @throws org.osid.UnimplementedException <code> supportsDispatchQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchQuery getDispatchQuery() {
        throw new org.osid.UnimplementedException("supportsDispatchQuery() is false");
    }


    /**
     *  Matches publishers with any dispatch. 
     *
     *  @param  match <code> true </code> to match publishers with any 
     *          dispatch, <code> false </code> to match publishers with no 
     *          dispatches 
     */

    @OSID @Override
    public void matchAnyDispatch(boolean match) {
        return;
    }


    /**
     *  Clears the dispatch terms. 
     */

    @OSID @Override
    public void clearDispatchTerms() {
        return;
    }


    /**
     *  Sets the publisher <code> Id </code> for this query to match 
     *  publishers that have the specified publisher as an ancestor. 
     *
     *  @param  publisherId a publisher <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorPublisherId(org.osid.id.Id publisherId, 
                                         boolean match) {
        return;
    }


    /**
     *  Clears the ancestor publisher <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorPublisherIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PublisherQuery </code> is available. 
     *
     *  @return <code> true </code> if a publisher query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorPublisherQuery() {
        return (false);
    }


    /**
     *  Gets the query for a publisher. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the publisher query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorPublisherQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.subscription.PublisherQuery getAncestorPublisherQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorPublisherQuery() is false");
    }


    /**
     *  Matches publishers with any ancestor. 
     *
     *  @param  match <code> true </code> to match publishers with any 
     *          ancestor, <code> false </code> to match root publishers 
     */

    @OSID @Override
    public void matchAnyAncestorPublisher(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor publisher terms. 
     */

    @OSID @Override
    public void clearAncestorPublisherTerms() {
        return;
    }


    /**
     *  Sets the publisher <code> Id </code> for this query to match 
     *  publishers that have the specified publisher as a descendant. 
     *
     *  @param  publisherId a publisher <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantPublisherId(org.osid.id.Id publisherId, 
                                           boolean match) {
        return;
    }


    /**
     *  Clears the descendant publisher <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantPublisherIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PublisherQuery </code> is available. 
     *
     *  @return <code> true </code> if a publisher query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantPublisherQuery() {
        return (false);
    }


    /**
     *  Gets the query for a publisher. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the publisher query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantPublisherQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.subscription.PublisherQuery getDescendantPublisherQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantPublisherQuery() is false");
    }


    /**
     *  Matches publishers with any descendant. 
     *
     *  @param  match <code> true </code> to match publishers with any 
     *          descendant, <code> false </code> to match leaf publishers 
     */

    @OSID @Override
    public void matchAnyDescendantPublisher(boolean match) {
        return;
    }


    /**
     *  Clears the descendant publisher terms. 
     */

    @OSID @Override
    public void clearDescendantPublisherTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given publisher query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a publisher implementing the requested record.
     *
     *  @param publisherRecordType a publisher record type
     *  @return the publisher query record
     *  @throws org.osid.NullArgumentException
     *          <code>publisherRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(publisherRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.subscription.records.PublisherQueryRecord getPublisherQueryRecord(org.osid.type.Type publisherRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.subscription.records.PublisherQueryRecord record : this.records) {
            if (record.implementsRecordType(publisherRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(publisherRecordType + " is not supported");
    }


    /**
     *  Adds a record to this publisher query. 
     *
     *  @param publisherQueryRecord publisher query record
     *  @param publisherRecordType publisher record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPublisherQueryRecord(org.osid.subscription.records.PublisherQueryRecord publisherQueryRecord, 
                                          org.osid.type.Type publisherRecordType) {

        addRecordType(publisherRecordType);
        nullarg(publisherQueryRecord, "publisher query record");
        this.records.add(publisherQueryRecord);        
        return;
    }
}
