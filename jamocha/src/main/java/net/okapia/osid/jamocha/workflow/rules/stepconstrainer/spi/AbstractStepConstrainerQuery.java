//
// AbstractStepConstrainerQuery.java
//
//     A template for making a StepConstrainer Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.rules.stepconstrainer.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for step constrainers.
 */

public abstract class AbstractStepConstrainerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidConstrainerQuery
    implements org.osid.workflow.rules.StepConstrainerQuery {

    private final java.util.Collection<org.osid.workflow.rules.records.StepConstrainerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches mapped to a step. 
     *
     *  @param  stepId the step <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stepId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchRuledStepId(org.osid.id.Id stepId, boolean match) {
        return;
    }


    /**
     *  Clears the step <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledStepIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> StepQuery </code> is available. 
     *
     *  @return <code> true </code> if a step query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledStepQuery() {
        return (false);
    }


    /**
     *  Gets the query for a step. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the step query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledStepQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepQuery getRuledStepQuery() {
        throw new org.osid.UnimplementedException("supportsRuledStepQuery() is false");
    }


    /**
     *  Matches constrainers mapped to any step. 
     *
     *  @param  match <code> true </code> for constrainers mapped to any step, 
     *          <code> false </code> to match constrainers mapped to no steps 
     */

    @OSID @Override
    public void matchAnyRuledStep(boolean match) {
        return;
    }


    /**
     *  Clears the step query terms. 
     */

    @OSID @Override
    public void clearRuledStepTerms() {
        return;
    }


    /**
     *  Matches constrainers mapped to the office. 
     *
     *  @param  officeId the office <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOfficeId(org.osid.id.Id officeId, boolean match) {
        return;
    }


    /**
     *  Clears the office <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearOfficeIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> OfficeQuery </code> is available. 
     *
     *  @return <code> true </code> if an office query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfficeQuery() {
        return (false);
    }


    /**
     *  Gets the query for an office. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the office query 
     *  @throws org.osid.UnimplementedException <code> supportsOfficeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.OfficeQuery getOfficeQuery() {
        throw new org.osid.UnimplementedException("supportsOfficeQuery() is false");
    }


    /**
     *  Clears the office query terms. 
     */

    @OSID @Override
    public void clearOfficeTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given step constrainer query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a step constrainer implementing the requested record.
     *
     *  @param stepConstrainerRecordType a step constrainer record type
     *  @return the step constrainer query record
     *  @throws org.osid.NullArgumentException
     *          <code>stepConstrainerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(stepConstrainerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.rules.records.StepConstrainerQueryRecord getStepConstrainerQueryRecord(org.osid.type.Type stepConstrainerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.workflow.rules.records.StepConstrainerQueryRecord record : this.records) {
            if (record.implementsRecordType(stepConstrainerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(stepConstrainerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this step constrainer query. 
     *
     *  @param stepConstrainerQueryRecord step constrainer query record
     *  @param stepConstrainerRecordType stepConstrainer record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addStepConstrainerQueryRecord(org.osid.workflow.rules.records.StepConstrainerQueryRecord stepConstrainerQueryRecord, 
                                          org.osid.type.Type stepConstrainerRecordType) {

        addRecordType(stepConstrainerRecordType);
        nullarg(stepConstrainerQueryRecord, "step constrainer query record");
        this.records.add(stepConstrainerQueryRecord);        
        return;
    }
}
