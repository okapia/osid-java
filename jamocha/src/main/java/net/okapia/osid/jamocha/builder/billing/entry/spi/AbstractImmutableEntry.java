//
// AbstractImmutableEntry.java
//
//     Wraps a mutable Entry to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.billing.entry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Entry</code> to hide modifiers. This
 *  wrapper provides an immutized Entry from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying entry whose state changes are visible.
 */

public abstract class AbstractImmutableEntry
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.billing.Entry {

    private final org.osid.billing.Entry entry;


    /**
     *  Constructs a new <code>AbstractImmutableEntry</code>.
     *
     *  @param entry the entry to immutablize
     *  @throws org.osid.NullArgumentException <code>entry</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableEntry(org.osid.billing.Entry entry) {
        super(entry);
        this.entry = entry;
        return;
    }


    /**
     *  Gets the customer <code> Id </code> associated with this entry. 
     *
     *  @return the customer <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCustomerId() {
        return (this.entry.getCustomerId());
    }


    /**
     *  Gets the customer associated with this entry. 
     *
     *  @return the customer 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.billing.Customer getCustomer()
        throws org.osid.OperationFailedException {

        return (this.entry.getCustomer());
    }


    /**
     *  Gets the item <code> Id </code> associated with this entry. 
     *
     *  @return the item <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getItemId() {
        return (this.entry.getItemId());
    }


    /**
     *  Gets the item associated with this entry. 
     *
     *  @return the item 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.billing.Item getItem()
        throws org.osid.OperationFailedException {

        return (this.entry.getItem());
    }


    /**
     *  Gets the <code> Id </code> of the <code> Period </code> of this 
     *  offering. 
     *
     *  @return the <code> Period </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getPeriodId() {
        return (this.entry.getPeriodId());
    }


    /**
     *  Gets the <code> Period </code> of this offering. 
     *
     *  @return the period 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.billing.Period getPeriod()
        throws org.osid.OperationFailedException {

        return (this.entry.getPeriod());
    }


    /**
     *  Gets the quantity of the item. 
     *
     *  @return the quantity 
     *  @throws org.osid.IllegalStateException <code> hasProduct() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public long getQuantity() {
        return (this.entry.getQuantity());
    }


    /**
     *  Gets the amount of this entry. 
     *
     *  @return the amount 
     */

    @OSID @Override
    public org.osid.financials.Currency getAmount() {
        return (this.entry.getAmount());
    }


    /**
     *  Tests if the amount is a debit or a credit. 
     *
     *  @return <code> true </code> if this item amount is a debit, <code> 
     *          false </code> if it is a credit 
     */

    @OSID @Override
    public boolean isDebit() {
        return (this.entry.isDebit());
    }


    /**
     *  Gets the entry record corresponding to the given <code> Entry </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> entryRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(entryRecordType) </code> is <code> true </code> . 
     *
     *  @param  entryRecordType the type of entry record to retrieve 
     *  @return the entry record 
     *  @throws org.osid.NullArgumentException <code> entryRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(entryRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.records.EntryRecord getEntryRecord(org.osid.type.Type entryRecordType)
        throws org.osid.OperationFailedException {

        return (this.entry.getEntryRecord(entryRecordType));
    }
}

