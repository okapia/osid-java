//
// AbstractAssemblyAssessmentPartQuery.java
//
//     An AssessmentPartQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.assessment.authoring.assessmentpart.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An AssessmentPartQuery that stores terms.
 */

public abstract class AbstractAssemblyAssessmentPartQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOperableOsidObjectQuery
    implements org.osid.assessment.authoring.AssessmentPartQuery,
               org.osid.assessment.authoring.AssessmentPartQueryInspector,
               org.osid.assessment.authoring.AssessmentPartSearchOrder {

    private final java.util.Collection<org.osid.assessment.authoring.records.AssessmentPartQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.assessment.authoring.records.AssessmentPartQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.assessment.authoring.records.AssessmentPartSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();

    private final AssemblyOsidContainableQuery query;


    /** 
     *  Constructs a new <code>AbstractAssemblyAssessmentPartQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyAssessmentPartQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        this.query = new AssemblyOsidContainableQuery(assembler);
        return;
    }
    

     /**
     *  Match containables that are sequestered. 
     *
     *  @param  match <code> true </code> to match any sequestered 
     *          containables, <code> false </code> to match non-sequestered 
     *          containables 
     */

    @OSID @Override
    public void matchSequestered(boolean match) {
        this.query.matchSequestered(match);
        return;
    }


    /**
     *  Clears the sequestered query terms. 
     */

    @OSID @Override
    public void clearSequesteredTerms() {
        this.query.clearSequesteredTerms();
        return;
    }

    
    /**
     *  Gets the sequestered query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getSequesteredTerms() {
        return (this.query.getSequesteredTerms());
    }

    
    /**
     *  Specifies a preference for ordering the result set by the sequestered 
     *  flag. 
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySequestered(org.osid.SearchOrderStyle style) {
        this.query.orderBySequestered(style);
        return;
    }


    /**
     *  Gets the column name for the sequestered field.
     *
     *  @return the column name
     */

    protected String getSequesteredColumn() {
        return (this.query.getSequesteredColumn());
    }


    /**
     *  Sets the assessment <code> Id </code> for this query. 
     *
     *  @param  assessmentId an assessment <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAssessmentId(org.osid.id.Id assessmentId, boolean match) {
        getAssembler().addIdTerm(getAssessmentIdColumn(), assessmentId, match);
        return;
    }


    /**
     *  Clears all assessment <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAssessmentIdTerms() {
        getAssembler().clearTerms(getAssessmentIdColumn());
        return;
    }


    /**
     *  Gets the assessment <code> Id </code> query terms. 
     *
     *  @return the assessment <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssessmentIdTerms() {
        return (getAssembler().getIdTerms(getAssessmentIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the assessment. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAssessment(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAssessmentColumn(), style);
        return;
    }


    /**
     *  Gets the AssessmentId column name.
     *
     * @return the column name
     */

    protected String getAssessmentIdColumn() {
        return ("assessment_id");
    }


    /**
     *  Tests if an <code> AssessmentQuery </code> is available. 
     *
     *  @return <code> true </code> if an assessment query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an assessment. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the assessment query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentQuery getAssessmentQuery() {
        throw new org.osid.UnimplementedException("supportsAssessmentQuery() is false");
    }


    /**
     *  Clears all assessment terms. 
     */

    @OSID @Override
    public void clearAssessmentTerms() {
        getAssembler().clearTerms(getAssessmentColumn());
        return;
    }


    /**
     *  Gets the assessment query terms. 
     *
     *  @return the assessment terms 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentQueryInspector[] getAssessmentTerms() {
        return (new org.osid.assessment.AssessmentQueryInspector[0]);
    }


    /**
     *  Tests if an assessment search order is available. 
     *
     *  @return <code> true </code> if an assessment search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the assessment order. 
     *
     *  @return the assessment search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentSearchOrder getAssessmentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAssessmentSearchOrder() is false");
    }


    /**
     *  Gets the Assessment column name.
     *
     * @return the column name
     */

    protected String getAssessmentColumn() {
        return ("assessment");
    }


    /**
     *  Sets the assessment part <code> Id </code> for this query. 
     *
     *  @param  assessmentPartId an assessment part <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentPartId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchParentAssessmentPartId(org.osid.id.Id assessmentPartId, 
                                            boolean match) {
        getAssembler().addIdTerm(getParentAssessmentPartIdColumn(), assessmentPartId, match);
        return;
    }


    /**
     *  Clears all assessment part <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearParentAssessmentPartIdTerms() {
        getAssembler().clearTerms(getParentAssessmentPartIdColumn());
        return;
    }


    /**
     *  Gets the assessment part <code> Id </code> query terms. 
     *
     *  @return the assessment parent <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getParentAssessmentPartIdTerms() {
        return (getAssembler().getIdTerms(getParentAssessmentPartIdColumn()));
    }


    /**
     *  Gets the ParentAssessmentPartId column name.
     *
     * @return the column name
     */

    protected String getParentAssessmentPartIdColumn() {
        return ("parent_assessment_part_id");
    }


    /**
     *  Tests if an <code> AssessmentPartQuery </code> is available. 
     *
     *  @return <code> true </code> if an assessment part query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParentAssessmentPartQuery() {
        return (false);
    }


    /**
     *  Gets the query for an assessment part. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the assessment part query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParentAssessmentPartQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartQuery getParentAssessmentPartQuery() {
        throw new org.osid.UnimplementedException("supportsParentAssessmentPartQuery() is false");
    }


    /**
     *  Matches assessment parts with any parent assessment part. 
     *
     *  @param  match <code> true </code> to match assessment parts with any 
     *          parent, <code> false </code> to match assessment parts with no 
     *          parents 
     */

    @OSID @Override
    public void matchAnyParentAssessmentPart(boolean match) {
        getAssembler().addIdWildcardTerm(getParentAssessmentPartColumn(), match);
        return;
    }


    /**
     *  Clears all assessment part terms. 
     */

    @OSID @Override
    public void clearParentAssessmentPartTerms() {
        getAssembler().clearTerms(getParentAssessmentPartColumn());
        return;
    }


    /**
     *  Gets the assessment part query terms. 
     *
     *  @return the assessment part terms 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartQueryInspector[] getParentAssessmentPartTerms() {
        return (new org.osid.assessment.authoring.AssessmentPartQueryInspector[0]);
    }


    /**
     *  Gets the ParentAssessmentPart column name.
     *
     * @return the column name
     */

    protected String getParentAssessmentPartColumn() {
        return ("parent_assessment_part");
    }


    /**
     *  Matches assessment parts that are also used as sections. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchSection(boolean match) {
        getAssembler().addBooleanTerm(getSectionColumn(), match);
        return;
    }


    /**
     *  Clears all section terms. 
     */

    @OSID @Override
    public void clearSectionTerms() {
        getAssembler().clearTerms(getSectionColumn());
        return;
    }


    /**
     *  Gets the section query terms. 
     *
     *  @return the section terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getSectionTerms() {
        return (getAssembler().getBooleanTerms(getSectionColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the section. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySection(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSectionColumn(), style);
        return;
    }


    /**
     *  Gets the Section column name.
     *
     * @return the column name
     */

    protected String getSectionColumn() {
        return ("section");
    }


    /**
     *  Matches assessment parts that fall in between the given weights 
     *  inclusive. 
     *
     *  @param  low low end of range 
     *  @param  high high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchWeight(long low, long high, boolean match) {
        getAssembler().addCardinalRangeTerm(getWeightColumn(), low, high, match);
        return;
    }


    /**
     *  Matches assessment parts with any weight assigned. 
     *
     *  @param  match <code> true </code> to match assessment parts with any 
     *          wieght, <code> false </code> to match assessment parts with no 
     *          weight 
     */

    @OSID @Override
    public void matchAnyWeight(boolean match) {
        getAssembler().addCardinalRangeWildcardTerm(getWeightColumn(), match);
        return;
    }


    /**
     *  Clears all weight terms. 
     */

    @OSID @Override
    public void clearWeightTerms() {
        getAssembler().clearTerms(getWeightColumn());
        return;
    }


    /**
     *  Gets the weight terms. 
     *
     *  @return the resource <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalTerm[] getWeightTerms() {
        return (getAssembler().getCardinalTerms(getWeightColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the weight. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByWeight(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getWeightColumn(), style);
        return;
    }


    /**
     *  Gets the Weight column name.
     *
     * @return the column name
     */

    protected String getWeightColumn() {
        return ("weight");
    }


    /**
     *  Matches assessment parts hose allocated time falls in between the 
     *  given times inclusive. 
     *
     *  @param  low low end of range 
     *  @param  high high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchAllocatedTime(org.osid.calendaring.Duration low, 
                                   org.osid.calendaring.Duration high, 
                                   boolean match) {
        getAssembler().addDurationRangeTerm(getAllocatedTimeColumn(), low, high, match);
        return;
    }


    /**
     *  Matches assessment parts with any time assigned. 
     *
     *  @param  match <code> true </code> to match assessment parts with any 
     *          alloocated time, <code> false </code> to match assessment 
     *          parts with no allocated time 
     */

    @OSID @Override
    public void matchAnyAllocatedTime(boolean match) {
        getAssembler().addDurationRangeWildcardTerm(getAllocatedTimeColumn(), match);
        return;
    }


    /**
     *  Clears all allocated time terms. 
     */

    @OSID @Override
    public void clearAllocatedTimeTerms() {
        getAssembler().clearTerms(getAllocatedTimeColumn());
        return;
    }


    /**
     *  Gets the allocated time terms. 
     *
     *  @return the time terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationTerm[] getAllocatedTimeTerms() {
        return (getAssembler().getDurationTerms(getAllocatedTimeColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the allocated 
     *  time. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAllocatedTime(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAllocatedTimeColumn(), style);
        return;
    }


    /**
     *  Gets the AllocatedTime column name.
     *
     * @return the column name
     */

    protected String getAllocatedTimeColumn() {
        return ("allocated_time");
    }


    /**
     *  Sets the assessment part <code> Id </code> for this query. 
     *
     *  @param  assessmentPartId an assessment part <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentPartId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchChildAssessmentPartId(org.osid.id.Id assessmentPartId, 
                                           boolean match) {
        getAssembler().addIdTerm(getChildAssessmentPartIdColumn(), assessmentPartId, match);
        return;
    }


    /**
     *  Clears all assessment part <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearChildAssessmentPartIdTerms() {
        getAssembler().clearTerms(getChildAssessmentPartIdColumn());
        return;
    }


    /**
     *  Gets the assessment part <code> Id </code> query terms. 
     *
     *  @return the assessment parent <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getChildAssessmentPartIdTerms() {
        return (getAssembler().getIdTerms(getChildAssessmentPartIdColumn()));
    }


    /**
     *  Gets the ChildAssessmentPartId column name.
     *
     * @return the column name
     */

    protected String getChildAssessmentPartIdColumn() {
        return ("child_assessment_part_id");
    }


    /**
     *  Tests if an <code> AssessmentPartQuery </code> is available. 
     *
     *  @return <code> true </code> if an assessment part query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChildAssessmentPartQuery() {
        return (false);
    }


    /**
     *  Gets the query for an assessment part. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the assessment part query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChildAssessmentPartQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartQuery getChildAssessmentPartQuery() {
        throw new org.osid.UnimplementedException("supportsChildAssessmentPartQuery() is false");
    }


    /**
     *  Matches assessment parts with any child assessment part. 
     *
     *  @param  match <code> true </code> to match assessment parts with any 
     *          children, <code> false </code> to match assessment parts with 
     *          no children 
     */

    @OSID @Override
    public void matchAnyChildAssessmentPart(boolean match) {
        getAssembler().addIdWildcardTerm(getChildAssessmentPartColumn(), match);
        return;
    }


    /**
     *  Clears all assessment part terms. 
     */

    @OSID @Override
    public void clearChildAssessmentPartTerms() {
        getAssembler().clearTerms(getChildAssessmentPartColumn());
        return;
    }


    /**
     *  Gets the assessment part query terms. 
     *
     *  @return the assessment part terms 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartQueryInspector[] getChildAssessmentPartTerms() {
        return (new org.osid.assessment.authoring.AssessmentPartQueryInspector[0]);
    }


    /**
     *  Gets the ChildAssessmentPart column name.
     *
     * @return the column name
     */

    protected String getChildAssessmentPartColumn() {
        return ("child_assessment_part");
    }


    /**
     *  Matches constrainers mapped to the bank. 
     *
     *  @param  bankId the bank <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchBankId(org.osid.id.Id bankId, boolean match) {
        getAssembler().addIdTerm(getBankIdColumn(), bankId, match);
        return;
    }


    /**
     *  Clears the bank <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearBankIdTerms() {
        getAssembler().clearTerms(getBankIdColumn());
        return;
    }


    /**
     *  Gets the bank <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBankIdTerms() {
        return (getAssembler().getIdTerms(getBankIdColumn()));
    }


    /**
     *  Gets the BankId column name.
     *
     * @return the column name
     */

    protected String getBankIdColumn() {
        return ("bank_id");
    }


    /**
     *  Tests if an <code> BankQuery </code> is available. 
     *
     *  @return <code> true </code> if a bank query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBankQuery() {
        return (false);
    }


    /**
     *  Gets the query for a bank. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the bank query 
     *  @throws org.osid.UnimplementedException <code> supportsBankQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.BankQuery getBankQuery() {
        throw new org.osid.UnimplementedException("supportsBankQuery() is false");
    }


    /**
     *  Clears the bank query terms. 
     */

    @OSID @Override
    public void clearBankTerms() {
        getAssembler().clearTerms(getBankColumn());
        return;
    }


    /**
     *  Gets the bank query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.assessment.BankQueryInspector[] getBankTerms() {
        return (new org.osid.assessment.BankQueryInspector[0]);
    }


    /**
     *  Gets the Bank column name.
     *
     * @return the column name
     */

    protected String getBankColumn() {
        return ("bank");
    }


    
    /**
     *  Tests if this assessmentPart supports the given record
     *  <code>Type</code>.
     *
     *  @param  assessmentPartRecordType an assessment part record type 
     *  @return <code>true</code> if the assessmentPartRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentPartRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type assessmentPartRecordType) {
        for (org.osid.assessment.authoring.records.AssessmentPartQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(assessmentPartRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  assessmentPartRecordType the assessment part record type 
     *  @return the assessment part query record 
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentPartRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assessmentPartRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.authoring.records.AssessmentPartQueryRecord getAssessmentPartQueryRecord(org.osid.type.Type assessmentPartRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.authoring.records.AssessmentPartQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(assessmentPartRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assessmentPartRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  assessmentPartRecordType the assessment part record type 
     *  @return the assessment part query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentPartRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assessmentPartRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.authoring.records.AssessmentPartQueryInspectorRecord getAssessmentPartQueryInspectorRecord(org.osid.type.Type assessmentPartRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.authoring.records.AssessmentPartQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(assessmentPartRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assessmentPartRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param assessmentPartRecordType the assessment part record type
     *  @return the assessment part search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentPartRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assessmentPartRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.authoring.records.AssessmentPartSearchOrderRecord getAssessmentPartSearchOrderRecord(org.osid.type.Type assessmentPartRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.authoring.records.AssessmentPartSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(assessmentPartRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assessmentPartRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this assessment part. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param assessmentPartQueryRecord the assessment part query record
     *  @param assessmentPartQueryInspectorRecord the assessment part query inspector
     *         record
     *  @param assessmentPartSearchOrderRecord the assessment part search order record
     *  @param assessmentPartRecordType assessment part record type
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentPartQueryRecord</code>,
     *          <code>assessmentPartQueryInspectorRecord</code>,
     *          <code>assessmentPartSearchOrderRecord</code> or
     *          <code>assessmentPartRecordTypeassessmentPart</code> is
     *          <code>null</code>
     */
            
    protected void addAssessmentPartRecords(org.osid.assessment.authoring.records.AssessmentPartQueryRecord assessmentPartQueryRecord, 
                                      org.osid.assessment.authoring.records.AssessmentPartQueryInspectorRecord assessmentPartQueryInspectorRecord, 
                                      org.osid.assessment.authoring.records.AssessmentPartSearchOrderRecord assessmentPartSearchOrderRecord, 
                                      org.osid.type.Type assessmentPartRecordType) {

        addRecordType(assessmentPartRecordType);

        nullarg(assessmentPartQueryRecord, "assessment part query record");
        nullarg(assessmentPartQueryInspectorRecord, "assessment part query inspector record");
        nullarg(assessmentPartSearchOrderRecord, "assessment part search odrer record");

        this.queryRecords.add(assessmentPartQueryRecord);
        this.queryInspectorRecords.add(assessmentPartQueryInspectorRecord);
        this.searchOrderRecords.add(assessmentPartSearchOrderRecord);
        
        return;
    }
    
    protected class AssemblyOsidContainableQuery
        extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidContainableQuery
        implements org.osid.OsidContainableQuery,
                   org.osid.OsidContainableQueryInspector,
                   org.osid.OsidContainableSearchOrder {
        
        
        /** 
         *  Constructs a new <code>AssemblyOsidContainableQuery</code>.
         *
         *  @param assembler the query assembler
         *  @throws org.osid.NullArgumentException <code>assembler</code>
         *          is <code>null</code>
         */
        
        protected AssemblyOsidContainableQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
            super(assembler);
            return;
        }


        /**
         *  Gets the column name for the sequestered field.
         *
         *  @return the column name
         */
        
        protected String getSequesteredColumn() {
            return (super.getSequesteredColumn());
        }
    }    
}
