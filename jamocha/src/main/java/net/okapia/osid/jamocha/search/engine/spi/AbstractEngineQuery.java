//
// AbstractEngineQuery.java
//
//     A template for making an Engine Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.search.engine.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for engines.
 */

public abstract class AbstractEngineQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.search.EngineQuery {

    private final java.util.Collection<org.osid.search.records.EngineQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the engine <code> Id </code> for this query to match engines that 
     *  have the specified engine as an ancestor. 
     *
     *  @param  engineId an engine <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorEngineId(org.osid.id.Id engineId, boolean match) {
        return;
    }


    /**
     *  Clears all ancestor engine <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorEngineIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> EngineQuery </code> is available. 
     *
     *  @return <code> true </code> if an engine query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorEngineQuery() {
        return (false);
    }


    /**
     *  Gets the query for an engine. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the engine query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorEngineQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.search.EngineQuery getAncestorEngineQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorEngineQuery() is false");
    }


    /**
     *  Matches engines with any ancestor. 
     *
     *  @param  match <code> true </code> to match engine with any ancestor, 
     *          <code> false </code> to match root engines 
     */

    @OSID @Override
    public void matchAnyAncestorEngine(boolean match) {
        return;
    }


    /**
     *  Clears all ancestor engine terms. 
     */

    @OSID @Override
    public void clearAncestorEngineTerms() {
        return;
    }


    /**
     *  Sets the engine <code> Id </code> for this query to match engines that 
     *  have the specified engine as a descendant. 
     *
     *  @param  engineId an engine <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantEngineId(org.osid.id.Id engineId, boolean match) {
        return;
    }


    /**
     *  Clears all descendant engine <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantEngineIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> EngineQuery </code> is available. 
     *
     *  @return <code> true </code> if an engine query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantEngineQuery() {
        return (false);
    }


    /**
     *  Gets the query for an engine. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the engine query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantEngineQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.search.EngineQuery getDescendantEngineQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantEngineQuery() is false");
    }


    /**
     *  Matches engines with any descendant. 
     *
     *  @param  match <code> true </code> to match engine with any descendant, 
     *          <code> false </code> to match leaf engines 
     */

    @OSID @Override
    public void matchAnyDescendantEngine(boolean match) {
        return;
    }


    /**
     *  Clears all descendant engine terms. 
     */

    @OSID @Override
    public void clearDescendantEngineTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given engine query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an engine implementing the requested record.
     *
     *  @param engineRecordType an engine record type
     *  @return the engine query record
     *  @throws org.osid.NullArgumentException
     *          <code>engineRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(engineRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.search.records.EngineQueryRecord getEngineQueryRecord(org.osid.type.Type engineRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.search.records.EngineQueryRecord record : this.records) {
            if (record.implementsRecordType(engineRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(engineRecordType + " is not supported");
    }


    /**
     *  Adds a record to this engine query. 
     *
     *  @param engineQueryRecord engine query record
     *  @param engineRecordType engine record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addEngineQueryRecord(org.osid.search.records.EngineQueryRecord engineQueryRecord, 
                                          org.osid.type.Type engineRecordType) {

        addRecordType(engineRecordType);
        nullarg(engineQueryRecord, "engine query record");
        this.records.add(engineQueryRecord);        
        return;
    }
}
