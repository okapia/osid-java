//
// AbstractJournalLookupSession.java
//
//    A starter implementation framework for providing a Journal
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.journaling.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A starter implementation framework for providing a Journal
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getJournals(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractJournalLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.journaling.JournalLookupSession {

    private boolean pedantic = false;
    private org.osid.journaling.Journal journal = new net.okapia.osid.jamocha.nil.journaling.journal.UnknownJournal();


    /**
     *  Tests if this user can perform <code>Journal</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupJournals() {
        return (true);
    }


    /**
     *  A complete view of the <code>Journal</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeJournalView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Journal</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryJournalView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }

     
    /**
     *  Gets the <code>Journal</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Journal</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Journal</code> and
     *  retained for compatibility.
     *
     *  @param  journalId <code>Id</code> of the
     *          <code>Journal</code>
     *  @return the journal
     *  @throws org.osid.NotFoundException <code>journalId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>journalId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.Journal getJournal(org.osid.id.Id journalId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.journaling.JournalList journals = getJournals()) {
            while (journals.hasNext()) {
                org.osid.journaling.Journal journal = journals.getNextJournal();
                if (journal.getId().equals(journalId)) {
                    return (journal);
                }
            }
        } 

        throw new org.osid.NotFoundException(journalId + " not found");
    }


    /**
     *  Gets a <code>JournalList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  journals specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Journals</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getJournals()</code>.
     *
     *  @param  journalIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Journal</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>journalIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalList getJournalsByIds(org.osid.id.IdList journalIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.journaling.Journal> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = journalIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getJournal(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("journal " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.journaling.journal.LinkedJournalList(ret));
    }


    /**
     *  Gets a <code>JournalList</code> corresponding to the given
     *  journal genus <code>Type</code> which does not include
     *  journals of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  journals or an error results. Otherwise, the returned list
     *  may contain only those journals that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getJournals()</code>.
     *
     *  @param  journalGenusType a journal genus type 
     *  @return the returned <code>Journal</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>journalGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalList getJournalsByGenusType(org.osid.type.Type journalGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.journaling.journal.JournalGenusFilterList(getJournals(), journalGenusType));
    }


    /**
     *  Gets a <code>JournalList</code> corresponding to the given
     *  journal genus <code>Type</code> and include any additional
     *  journals with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  journals or an error results. Otherwise, the returned list
     *  may contain only those journals that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getJournals()</code>.
     *
     *  @param  journalGenusType a journal genus type 
     *  @return the returned <code>Journal</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>journalGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalList getJournalsByParentGenusType(org.osid.type.Type journalGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getJournalsByGenusType(journalGenusType));
    }


    /**
     *  Gets a <code>JournalList</code> containing the given
     *  journal record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  journals or an error results. Otherwise, the returned list
     *  may contain only those journals that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getJournals()</code>.
     *
     *  @param  journalRecordType a journal record type 
     *  @return the returned <code>Journal</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>journalRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalList getJournalsByRecordType(org.osid.type.Type journalRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.journaling.journal.JournalRecordFilterList(getJournals(), journalRecordType));
    }


    /**
     *  Gets a <code>JournalList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known journals or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  journals that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Journal</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.journaling.JournalList getJournalsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.journaling.journal.JournalProviderFilterList(getJournals(), resourceId));
    }


    /**
     *  Gets all <code>Journals</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  journals or an error results. Otherwise, the returned list
     *  may contain only those journals that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Journals</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.journaling.JournalList getJournals()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the journal list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of journals
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.journaling.JournalList filterJournalsOnViews(org.osid.journaling.JournalList list)
        throws org.osid.OperationFailedException {

        return (list);
    }
}
