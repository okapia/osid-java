//
// AbstractStockQueryInspector.java
//
//     A template for making a StockQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.stock.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for stocks.
 */

public abstract class AbstractStockQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.inventory.StockQueryInspector {

    private final java.util.Collection<org.osid.inventory.records.StockQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the SKU query terms. 
     *
     *  @return the SKU query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getSKUTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the model <code> Id </code> query terms. 
     *
     *  @return the model <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getModelIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the model query terms. 
     *
     *  @return the model query terms 
     */

    @OSID @Override
    public org.osid.inventory.ModelQueryInspector[] getModelTerms() {
        return (new org.osid.inventory.ModelQueryInspector[0]);
    }


    /**
     *  Gets the location description query terms. 
     *
     *  @return the location description query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getLocationDescriptionTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the location <code> Id </code> query terms. 
     *
     *  @return the location <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLocationIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the location query terms. 
     *
     *  @return the location query terms 
     */

    @OSID @Override
    public org.osid.mapping.LocationQueryInspector[] getLocationTerms() {
        return (new org.osid.mapping.LocationQueryInspector[0]);
    }


    /**
     *  Gets the ancestor stock <code> Id </code> query terms. 
     *
     *  @return the ancestor stock <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorStockIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor stock query terms. 
     *
     *  @return the ancestor stock terms 
     */

    @OSID @Override
    public org.osid.inventory.StockQueryInspector[] getAncestorStockTerms() {
        return (new org.osid.inventory.StockQueryInspector[0]);
    }


    /**
     *  Gets the descendant stock <code> Id </code> query terms. 
     *
     *  @return the descendant stock <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantStockIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant stock query terms. 
     *
     *  @return the descendant stock terms 
     */

    @OSID @Override
    public org.osid.inventory.StockQueryInspector[] getDescendantStockTerms() {
        return (new org.osid.inventory.StockQueryInspector[0]);
    }


    /**
     *  Gets the warehouse <code> Id </code> query terms. 
     *
     *  @return the warehouse <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getWarehouseIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the warehouse query terms. 
     *
     *  @return the warehouse query terms 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseQueryInspector[] getWarehouseTerms() {
        return (new org.osid.inventory.WarehouseQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given stock query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a stock implementing the requested record.
     *
     *  @param stockRecordType a stock record type
     *  @return the stock query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>stockRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(stockRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.records.StockQueryInspectorRecord getStockQueryInspectorRecord(org.osid.type.Type stockRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inventory.records.StockQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(stockRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(stockRecordType + " is not supported");
    }


    /**
     *  Adds a record to this stock query. 
     *
     *  @param stockQueryInspectorRecord stock query inspector
     *         record
     *  @param stockRecordType stock record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addStockQueryInspectorRecord(org.osid.inventory.records.StockQueryInspectorRecord stockQueryInspectorRecord, 
                                                   org.osid.type.Type stockRecordType) {

        addRecordType(stockRecordType);
        nullarg(stockRecordType, "stock record type");
        this.records.add(stockQueryInspectorRecord);        
        return;
    }
}
