//
// MutableIndexedMapCompetencyLookupSession
//
//    Implements a Competency lookup service backed by a collection of
//    competencies indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing;


/**
 *  Implements a Competency lookup service backed by a collection of
 *  competencies. The competencies are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some competencies may be compatible
 *  with more types than are indicated through these competency
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of competencies can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapCompetencyLookupSession
    extends net.okapia.osid.jamocha.core.resourcing.spi.AbstractIndexedMapCompetencyLookupSession
    implements org.osid.resourcing.CompetencyLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapCompetencyLookupSession} with no competencies.
     *
     *  @param foundry the foundry
     *  @throws org.osid.NullArgumentException {@code foundry}
     *          is {@code null}
     */

      public MutableIndexedMapCompetencyLookupSession(org.osid.resourcing.Foundry foundry) {
        setFoundry(foundry);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapCompetencyLookupSession} with a
     *  single competency.
     *  
     *  @param foundry the foundry
     *  @param  competency a single competency
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code competency} is {@code null}
     */

    public MutableIndexedMapCompetencyLookupSession(org.osid.resourcing.Foundry foundry,
                                                  org.osid.resourcing.Competency competency) {
        this(foundry);
        putCompetency(competency);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapCompetencyLookupSession} using an
     *  array of competencies.
     *
     *  @param foundry the foundry
     *  @param  competencies an array of competencies
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code competencies} is {@code null}
     */

    public MutableIndexedMapCompetencyLookupSession(org.osid.resourcing.Foundry foundry,
                                                  org.osid.resourcing.Competency[] competencies) {
        this(foundry);
        putCompetencies(competencies);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapCompetencyLookupSession} using a
     *  collection of competencies.
     *
     *  @param foundry the foundry
     *  @param  competencies a collection of competencies
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code competencies} is {@code null}
     */

    public MutableIndexedMapCompetencyLookupSession(org.osid.resourcing.Foundry foundry,
                                                  java.util.Collection<? extends org.osid.resourcing.Competency> competencies) {

        this(foundry);
        putCompetencies(competencies);
        return;
    }
    

    /**
     *  Makes a {@code Competency} available in this session.
     *
     *  @param  competency a competency
     *  @throws org.osid.NullArgumentException {@code competency{@code  is
     *          {@code null}
     */

    @Override
    public void putCompetency(org.osid.resourcing.Competency competency) {
        super.putCompetency(competency);
        return;
    }


    /**
     *  Makes an array of competencies available in this session.
     *
     *  @param  competencies an array of competencies
     *  @throws org.osid.NullArgumentException {@code competencies{@code 
     *          is {@code null}
     */

    @Override
    public void putCompetencies(org.osid.resourcing.Competency[] competencies) {
        super.putCompetencies(competencies);
        return;
    }


    /**
     *  Makes collection of competencies available in this session.
     *
     *  @param  competencies a collection of competencies
     *  @throws org.osid.NullArgumentException {@code competency{@code  is
     *          {@code null}
     */

    @Override
    public void putCompetencies(java.util.Collection<? extends org.osid.resourcing.Competency> competencies) {
        super.putCompetencies(competencies);
        return;
    }


    /**
     *  Removes a Competency from this session.
     *
     *  @param competencyId the {@code Id} of the competency
     *  @throws org.osid.NullArgumentException {@code competencyId{@code  is
     *          {@code null}
     */

    @Override
    public void removeCompetency(org.osid.id.Id competencyId) {
        super.removeCompetency(competencyId);
        return;
    }    
}
