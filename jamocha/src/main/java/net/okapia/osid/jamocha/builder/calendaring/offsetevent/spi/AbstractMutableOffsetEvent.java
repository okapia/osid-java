//
// AbstractMutableOffsetEvent.java
//
//     Defines a mutable OffsetEvent.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.calendaring.offsetevent.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>OffsetEvent</code>.
 */

public abstract class AbstractMutableOffsetEvent
    extends net.okapia.osid.jamocha.calendaring.offsetevent.spi.AbstractOffsetEvent
    implements org.osid.calendaring.OffsetEvent,
               net.okapia.osid.jamocha.builder.calendaring.offsetevent.OffsetEventMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this offset event. 
     *
     *  @param record offset event record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addOffsetEventRecord(org.osid.calendaring.records.OffsetEventRecord record, org.osid.type.Type recordType) {
        super.addOffsetEventRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Enables this offset event. Enabling an operable overrides any
     *  enabling rule that may exist.
     *  
     *  @param enabled <code>true</code> if enabled, <code>false<code>
     *         otherwise
     */
    
    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        return;
    }


    /**
     *  Disables this offset event. Disabling an operable overrides any
     *  enabling rule that may exist.
     *
     *  @param disabled <code> true </code> if this object is
     *         disabled, <code> false </code> otherwise
     */
    
    @Override
    public void setDisabled(boolean disabled) {
        super.setDisabled(disabled);
        return;
    }


    /**
     *  Sets the operational flag.
     *
     *  @param operational <code>true</code>if operational,
     *         <code>false</code> if not operational
     */
    
    @Override
    public void setOperational(boolean operational) {
        super.setOperational(operational);
        return;
    }


    /**
     *  Sets the display name for this offset event.
     *
     *  @param displayName the name for this offset event
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this offset event.
     *
     *  @param description the description of this offset event
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException <code>genusType</code>
     *          is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the rule.
     *
     *  @param rule the rule
     *  @throws org.osid.NullArgumentException <code>rule</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setRule(org.osid.rules.Rule rule) {
        super.setRule(rule);
        return;
    }


    /**
     *  Sets the fixed start time.
     *
     *  @param time a fixed start time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    @Override
    public void setFixedStartTime(org.osid.calendaring.DateTime time) {
        super.setFixedStartTime(time);
        return;
    }


    /**
     *  Sets the start reference event.
     *
     *  @param event a start reference event
     *  @throws org.osid.NullArgumentException <code>event</code> is
     *          <code>null</code>
     */

    @Override
    public void setStartReferenceEvent(org.osid.calendaring.Event event) {
        super.setStartReferenceEvent(event);
        return;
    }


    /**
     *  Sets the fixed start offset.
     *
     *  @param offset a fixed start offset
     *  @throws org.osid.NullArgumentException <code>offset</code> is
     *          <code>null</code>
     */

    @Override
    public void setFixedStartOffset(org.osid.calendaring.Duration offset) {
        super.setFixedStartOffset(offset);
        return;
    }


    /**
     *  Sets the relative weekday start offset.
     *
     *  @param offset a relative weekday start offset
     */

    @Override
    public void setRelativeWeekdayStartOffset(long offset) {
        super.setRelativeWeekdayStartOffset(offset);
        return;
    }


    /**
     *  Sets the relative start weekday.
     *
     *  @param weekday a relative start weekday
     */

    @Override
    public void setRelativeStartWeekday(long weekday) {
        super.setRelativeStartWeekday(weekday);
        return;
    }


    /**
     *  Sets the fixed duration.
     *
     *  @param duration a duration
     *  @throws org.osid.NullArgumentException
     *          <code>duration</code> is <code>null</code>
     */

    @Override
    public void setFixedDuration(org.osid.calendaring.Duration duration) {
        super.setFixedDuration(duration);
        return;
    }


    /**
     *  Sets the end reference event.
     *
     *  @param event an end reference event
     *  @throws org.osid.NullArgumentException <code>event</code> is
     *          <code>null</code>
     */

    @Override
    public void setEndReferenceEvent(org.osid.calendaring.Event event) {
        super.setEndReferenceEvent(event);
        return;
    }


    /**
     *  Sets the fixed end offset.
     *
     *  @param offset a fixed end offset
     *  @throws org.osid.NullArgumentException <code>offset</code> is
     *          <code>null</code>
     */

    @Override
    public void setFixedEndOffset(org.osid.calendaring.Duration offset) {
        super.setFixedEndOffset(offset);
        return;
    }


    /**
     *  Sets the relative weekday end offset.
     *
     *  @param offset a relative weekday end offset
     */

    @Override
    public void setRelativeWeekdayEndOffset(long offset) {
        super.setRelativeWeekdayEndOffset(offset);
        return;
    }


    /**
     *  Sets the relative end weekday.
     *
     *  @param weekday a relative end weekday
     */

    @Override
    public void setRelativeEndWeekday(long weekday) {
        super.setRelativeEndWeekday(weekday);
        return;
    }


    /**
     *  Sets the location description.
     *
     *  @param description a location description
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setLocationDescription(org.osid.locale.DisplayText description) {
        super.setLocationDescription(description);
        return;
    }


    /**
     *  Sets the location.
     *
     *  @param location a location
     *  @throws org.osid.NullArgumentException <code>location</code>
     *          is <code>null</code>
     */

    @Override
    public void setLocation(org.osid.mapping.Location location) {
        super.setLocation(location);
        return;
    }


    /**
     *  Adds a sponsor.
     *
     *  @param sponsor a sponsor
     *  @throws org.osid.NullArgumentException <code>sponsor</code> is
     *          <code>null</code>
     */

    @Override
    public void addSponsor(org.osid.resource.Resource sponsor) {
        super.addSponsor(sponsor);
        return;
    }


    /**
     *  Sets all the sponsors.
     *
     *  @param sponsors a collection of sponsors
     *  @throws org.osid.NullArgumentException <code>sponsors</code>
     *          is <code>null</code>
     */

    @Override
    public void setSponsors(java.util.Collection<org.osid.resource.Resource> sponsors) {
        super.setSponsors(sponsors);
        return;
    }
}

