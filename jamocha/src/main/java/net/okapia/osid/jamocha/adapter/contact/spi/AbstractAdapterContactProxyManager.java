//
// AbstractContactProxyManager.java
//
//     An adapter for a ContactProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.contact.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a ContactProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterContactProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.contact.ContactProxyManager>
    implements org.osid.contact.ContactProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterContactProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterContactProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterContactProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterContactProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any address federation is exposed. Federation is exposed when 
     *  a specific address may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  addresses appears as a single address. 
     *
     *  @return <code> true </code> if visible federation is supproted, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests for the availability of a contact service for getting available 
     *  contacts for a resource. 
     *
     *  @return <code> true </code> if contact is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContact() {
        return (getAdapteeManager().supportsContact());
    }


    /**
     *  Tests for the availability of a contact lookup service. 
     *
     *  @return <code> true </code> if contact lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContactLookup() {
        return (getAdapteeManager().supportsContactLookup());
    }


    /**
     *  Tests if querying contacts is available. 
     *
     *  @return <code> true </code> if contact query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContactQuery() {
        return (getAdapteeManager().supportsContactQuery());
    }


    /**
     *  Tests if searching for contacts is available. 
     *
     *  @return <code> true </code> if contact search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContactSearch() {
        return (getAdapteeManager().supportsContactSearch());
    }


    /**
     *  Tests if managing contacts is available. 
     *
     *  @return <code> true </code> if contact admin is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContactAdmin() {
        return (getAdapteeManager().supportsContactAdmin());
    }


    /**
     *  Tests if contact notification is available. 
     *
     *  @return <code> true </code> if contact notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContactNotification() {
        return (getAdapteeManager().supportsContactNotification());
    }


    /**
     *  Tests if a contact to address book lookup session is available. 
     *
     *  @return <code> true </code> if contact address book lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContactAddressBook() {
        return (getAdapteeManager().supportsContactAddressBook());
    }


    /**
     *  Tests if a contact to address book assignment session is available. 
     *
     *  @return <code> true </code> if contact address book assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContactAddressBookAssignment() {
        return (getAdapteeManager().supportsContactAddressBookAssignment());
    }


    /**
     *  Tests if a contact smart address book session is available. 
     *
     *  @return <code> true </code> if contact smart address book is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContactSmartAddressBook() {
        return (getAdapteeManager().supportsContactSmartAddressBook());
    }


    /**
     *  Tests for the availability of an address lookup service. 
     *
     *  @return <code> true </code> if address lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAddressLookup() {
        return (getAdapteeManager().supportsAddressLookup());
    }


    /**
     *  Tests if querying addresses is available. 
     *
     *  @return <code> true </code> if address query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAddressQuery() {
        return (getAdapteeManager().supportsAddressQuery());
    }


    /**
     *  Tests if searching for addresses is available. 
     *
     *  @return <code> true </code> if address search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAddressSearch() {
        return (getAdapteeManager().supportsAddressSearch());
    }


    /**
     *  Tests for the availability of an address administrative service for 
     *  creating and deleting addresses. 
     *
     *  @return <code> true </code> if address administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAddressAdmin() {
        return (getAdapteeManager().supportsAddressAdmin());
    }


    /**
     *  Tests for the availability of an address notification service. 
     *
     *  @return <code> true </code> if address notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAddressNotification() {
        return (getAdapteeManager().supportsAddressNotification());
    }


    /**
     *  Tests if an address to address book lookup session is available. 
     *
     *  @return <code> true </code> if address address book lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAddressAddressBook() {
        return (getAdapteeManager().supportsAddressAddressBook());
    }


    /**
     *  Tests if an address to address book assignment session is available. 
     *
     *  @return <code> true </code> if address address book assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAddressAddressBookAssignment() {
        return (getAdapteeManager().supportsAddressAddressBookAssignment());
    }


    /**
     *  Tests if an address smart address book session is available. 
     *
     *  @return <code> true </code> if address smart address book is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAddressSmartAddressBook() {
        return (getAdapteeManager().supportsAddressSmartAddressBook());
    }


    /**
     *  Tests for the availability of an address book lookup service. 
     *
     *  @return <code> true </code> if address book lookup is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAddressBookLookup() {
        return (getAdapteeManager().supportsAddressBookLookup());
    }


    /**
     *  Tests if querying address books is available. 
     *
     *  @return <code> true </code> if address book query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAddressBookQuery() {
        return (getAdapteeManager().supportsAddressBookQuery());
    }


    /**
     *  Tests if searching for address books is available. 
     *
     *  @return <code> true </code> if address book search is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAddressBookSearch() {
        return (getAdapteeManager().supportsAddressBookSearch());
    }


    /**
     *  Tests for the availability of an address book administrative service 
     *  for creating and deleting address books. 
     *
     *  @return <code> true </code> if address book administration is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAddressBookAdmin() {
        return (getAdapteeManager().supportsAddressBookAdmin());
    }


    /**
     *  Tests for the availability of an address book notification service. 
     *
     *  @return <code> true </code> if address book notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAddressBookNotification() {
        return (getAdapteeManager().supportsAddressBookNotification());
    }


    /**
     *  Tests for the availability of an address book hierarchy traversal 
     *  service. 
     *
     *  @return <code> true </code> if address book hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAddressBookHierarchy() {
        return (getAdapteeManager().supportsAddressBookHierarchy());
    }


    /**
     *  Tests for the availability of an address book hierarchy design 
     *  service. 
     *
     *  @return <code> true </code> if address book hierarchy design is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAddressBookHierarchyDesign() {
        return (getAdapteeManager().supportsAddressBookHierarchyDesign());
    }


    /**
     *  Tests for the availability of a contact batch service. 
     *
     *  @return <code> true </code> if a contact batch service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContactBatch() {
        return (getAdapteeManager().supportsContactBatch());
    }


    /**
     *  Tests for the availability of a contact rules service. 
     *
     *  @return <code> true </code> if a contact rules service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContactRules() {
        return (getAdapteeManager().supportsContactRules());
    }


    /**
     *  Gets the supported <code> Contact </code> record types. 
     *
     *  @return a list containing the supported contact record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getContactRecordTypes() {
        return (getAdapteeManager().getContactRecordTypes());
    }


    /**
     *  Tests if the given <code> Contact </code> record type is supported. 
     *
     *  @param  contactRecordType a <code> Type </code> indicating a <code> 
     *          Contact </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> contactRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsContactRecordType(org.osid.type.Type contactRecordType) {
        return (getAdapteeManager().supportsContactRecordType(contactRecordType));
    }


    /**
     *  Gets the supported contact search record types. 
     *
     *  @return a list containing the supported contact search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getContactSearchRecordTypes() {
        return (getAdapteeManager().getContactSearchRecordTypes());
    }


    /**
     *  Tests if the given contact search record type is supported. 
     *
     *  @param  contactSearchRecordType a <code> Type </code> indicating a 
     *          contact record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> contactSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsContactSearchRecordType(org.osid.type.Type contactSearchRecordType) {
        return (getAdapteeManager().supportsContactSearchRecordType(contactSearchRecordType));
    }


    /**
     *  Gets the supported <code> Address </code> record types. 
     *
     *  @return a list containing the supported address record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAddressRecordTypes() {
        return (getAdapteeManager().getAddressRecordTypes());
    }


    /**
     *  Tests if the given <code> Address </code> record type is supported. 
     *
     *  @param  addressRecordType a <code> Type </code> indicating a <code> 
     *          Address </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> addressRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAddressRecordType(org.osid.type.Type addressRecordType) {
        return (getAdapteeManager().supportsAddressRecordType(addressRecordType));
    }


    /**
     *  Gets the supported address search record types. 
     *
     *  @return a list containing the supported address search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAddressSearchRecordTypes() {
        return (getAdapteeManager().getAddressSearchRecordTypes());
    }


    /**
     *  Tests if the given address search record type is supported. 
     *
     *  @param  addressSearchRecordType a <code> Type </code> indicating an 
     *          address record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> addressSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAddressSearchRecordType(org.osid.type.Type addressSearchRecordType) {
        return (getAdapteeManager().supportsAddressSearchRecordType(addressSearchRecordType));
    }


    /**
     *  Gets the supported <code> AddressBook </code> record types. 
     *
     *  @return a list containing the supported address book record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAddressBookRecordTypes() {
        return (getAdapteeManager().getAddressBookRecordTypes());
    }


    /**
     *  Tests if the given <code> AddressBook </code> record type is 
     *  supported. 
     *
     *  @param  addressBookRecordType a <code> Type </code> indicating a 
     *          <code> AddressBook </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> addressBookRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAddressBookRecordType(org.osid.type.Type addressBookRecordType) {
        return (getAdapteeManager().supportsAddressBookRecordType(addressBookRecordType));
    }


    /**
     *  Gets the supported address book search record types. 
     *
     *  @return a list containing the supported address book search record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAddressBookSearchRecordTypes() {
        return (getAdapteeManager().getAddressBookSearchRecordTypes());
    }


    /**
     *  Tests if the given address book search record type is supported. 
     *
     *  @param  addressBookSearchRecordType a <code> Type </code> indicating 
     *          an address book record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          addressBookSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAddressBookSearchRecordType(org.osid.type.Type addressBookSearchRecordType) {
        return (getAdapteeManager().supportsAddressBookSearchRecordType(addressBookSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ContactSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsContact() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.ContactSession getContactSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getContactSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  service for the given address book. 
     *
     *  @param  addressId the <code> Id </code> of the <code> Address </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ContactLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Address </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> addressId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsContact() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.ContactSession getContactSessionForAddressBook(org.osid.id.Id addressId, 
                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getContactSessionForAddressBook(addressId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ContactLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsContactLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.ContactLookupSession getContactLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getContactLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact lookup 
     *  service for the given address book. 
     *
     *  @param  addressId the <code> Id </code> of the <code> Address </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ContactLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Address </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> addressId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsContactLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.ContactLookupSession getContactLookupSessionForAddressBook(org.osid.id.Id addressId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getContactLookupSessionForAddressBook(addressId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ContactQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsContactQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.ContactQuerySession getContactQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getContactQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact query 
     *  service for the given address book. 
     *
     *  @param  addressId the <code> Id </code> of the <code> Address </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ContactQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Contact </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> addressId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsContactQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.ContactQuerySession getContactQuerySessionForAddressBook(org.osid.id.Id addressId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getContactQuerySessionForAddressBook(addressId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ContactSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsContactSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.ContactSearchSession getContactSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getContactSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact search 
     *  service for the given address book. 
     *
     *  @param  addressId the <code> Id </code> of the <code> Address </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ContactSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Contact </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> addressId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsContactSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.ContactSearchSession getContactSearchSessionForAddressBook(org.osid.id.Id addressId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getContactSearchSessionForAddressBook(addressId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ContactAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsContactAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.ContactAdminSession getContactAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getContactAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  administration service for the given address book. 
     *
     *  @param  addressId the <code> Id </code> of the <code> Address </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ContactAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Contact </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> addressId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsContactAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.ContactAdminSession getContactAdminSessionForAddressBook(org.osid.id.Id addressId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getContactAdminSessionForAddressBook(addressId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  notification service. 
     *
     *  @param  contactReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> ContactNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> contactReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.ContactNotificationSession getContactNotificationSession(org.osid.contact.ContactReceiver contactReceiver, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getContactNotificationSession(contactReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the contact 
     *  notification service for the given address book. 
     *
     *  @param  contactReceiver the receiver 
     *  @param  addressId the <code> Id </code> of the <code> Address </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ContactNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Contact </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> contactReceiver, 
     *          addressId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.ContactNotificationSession getContactNotificationSessionForAddressBook(org.osid.contact.ContactReceiver contactReceiver, 
                                                                                                   org.osid.id.Id addressId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getContactNotificationSessionForAddressBook(contactReceiver, addressId, proxy));
    }


    /**
     *  Gets the session for retrieving contact to address book mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ContactAddressBookSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactAddressBook() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.ContactAddressBookSession getContactAddressBookSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getContactAddressBookSession(proxy));
    }


    /**
     *  Gets the session for assigning contact to address book mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ContactAddressBookAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactAddressBookAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.contact.ContactAddressBookAssignmentSession getContactAddressBookAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getContactAddressBookAssignmentSession(proxy));
    }


    /**
     *  Gets the session associated with the contact smart address book for 
     *  the given address book. 
     *
     *  @param  addressBookId the <code> Id </code> of the contact book 
     *  @param  proxy a proxy 
     *  @return a <code> ContactSmartAddressBookSession </code> 
     *  @throws org.osid.NotFoundException <code> contactBookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> contactBookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactSmartAddressBook() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.contact.ContactSmartAddressBookSession getContactSmartAddressBookSession(org.osid.id.Id addressBookId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getContactSmartAddressBookSession(addressBookId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the address lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AddressLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAddressLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressLookupSession getAddressLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAddressLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the address lookup 
     *  service for the given address book. 
     *
     *  @param  addressBookId the <code> Id </code> of the <code> AddressBook 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AddressLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> AddressBook </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> addressBookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAddressLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressLookupSession getAddressLookupSessionForAddressBook(org.osid.id.Id addressBookId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAddressLookupSessionForAddressBook(addressBookId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the address query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AddressQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAddressQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressQuerySession getAddressQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAddressQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the address query 
     *  service for the given address book. 
     *
     *  @param  addressBookId the <code> Id </code> of the <code> AddressBook 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AddressQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Address </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> addressBookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAddressQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressQuerySession getAddressQuerySessionForAddressBook(org.osid.id.Id addressBookId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAddressQuerySessionForAddressBook(addressBookId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the address search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AddressSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAddressSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressSearchSession getAddressSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAddressSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the address search 
     *  service for the given address book. 
     *
     *  @param  addressBookId the <code> Id </code> of the <code> AddressBook 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AddressSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Address </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> addressBookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAddressSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressSearchSession getAddressSearchSessionForAddressBook(org.osid.id.Id addressBookId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAddressSearchSessionForAddressBook(addressBookId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the address 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AddressAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAddressAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressAdminSession getAddressAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAddressAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the address 
     *  administration service for the given address book. 
     *
     *  @param  addressBookId the <code> Id </code> of the <code> AddressBook 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AddressAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Address </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> addressBookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAddressAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressAdminSession getAddressAdminSessionForAddressBook(org.osid.id.Id addressBookId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAddressAdminSessionForAddressBook(addressBookId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the address 
     *  notification service. 
     *
     *  @param  addressReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return an <code> AddressNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> addressReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAddressNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressNotificationSession getAddressNotificationSession(org.osid.contact.AddressReceiver addressReceiver, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAddressNotificationSession(addressReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the address 
     *  notification service for the given address book. 
     *
     *  @param  addressReceiver the receiver 
     *  @param  addressBookId the <code> Id </code> of the <code> AddressBook 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AddressNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Address </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> addressReceiver, 
     *          addressBookId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAddressNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressNotificationSession getAddressNotificationSessionForAddressBook(org.osid.contact.AddressReceiver addressReceiver, 
                                                                                                   org.osid.id.Id addressBookId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAddressNotificationSessionForAddressBook(addressReceiver, addressBookId, proxy));
    }


    /**
     *  Gets the session for retrieving address to address book mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AddressAddressBookSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAddressAddressBook() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressAddressBookSession getAddressAddressBookSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAddressAddressBookSession(proxy));
    }


    /**
     *  Gets the session for assigning address to address book mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AddressAddressBookAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAddressAddressBookAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressAddressBookAssignmentSession getAddressAddressBookAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAddressAddressBookAssignmentSession(proxy));
    }


    /**
     *  Gets the session for managing dynamic address address books for the 
     *  given address book. 
     *
     *  @param  addressBookId the <code> Id </code> of an address book 
     *  @param  proxy a proxy 
     *  @return an <code> AddressSmartAddressBookSession </code> 
     *  @throws org.osid.NotFoundException <code> addressBookId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> addressBookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAddressSmartAddressBook() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressSmartAddressBookSession getAddressSmartAddressBookSession(org.osid.id.Id addressBookId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAddressSmartAddressBookSession(addressBookId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the address book 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AddressBookLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAddressBookLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressBookLookupSession getAddressBookLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAddressBookLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the address book 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AddressBookQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAddressBookQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressBookQuerySession getAddressBookQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAddressBookQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the address book 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AddressBookSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAddressBookSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressBookSearchSession getAddressBookSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAddressBookSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the address book 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AddressBookAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAddressBookAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressBookAdminSession getAddressBookAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAddressBookAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the address book 
     *  notification service. 
     *
     *  @param  addressBookReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return an <code> AddressBookNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> addressBookReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAddressBookNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressBookNotificationSession getAddressBookNotificationSession(org.osid.contact.AddressBookReceiver addressBookReceiver, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAddressBookNotificationSession(addressBookReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the address book 
     *  hierarchy service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AddressBookHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAddressBookHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressBookHierarchySession getAddressBookHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAddressBookHierarchySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the address book 
     *  hierarchy design service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AddressBookHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAddressBookHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressBookHierarchyDesignSession getAddressBookHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAddressBookHierarchyDesignSession(proxy));
    }


    /**
     *  Gets the <code> ContactBatchProxyManager. </code> 
     *
     *  @return a <code> ContactBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsContactBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.batch.ContactBatchProxyManager getContactBatchProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getContactBatchProxyManager());
    }


    /**
     *  Gets the <code> ContactRulesProxyManager. </code> 
     *
     *  @return a <code> ContactRulesProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsContactRules() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactRulesProxyManager getContactRulesProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getContactRulesProxyManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
