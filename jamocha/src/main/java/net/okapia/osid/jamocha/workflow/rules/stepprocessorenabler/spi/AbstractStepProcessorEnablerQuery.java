//
// AbstractStepProcessorEnablerQuery.java
//
//     A template for making a StepProcessorEnabler Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.rules.stepprocessorenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for step processor enablers.
 */

public abstract class AbstractStepProcessorEnablerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.workflow.rules.StepProcessorEnablerQuery {

    private final java.util.Collection<org.osid.workflow.rules.records.StepProcessorEnablerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches enablers mapped to the step processor. 
     *
     *  @param  stepProcessorId the step processor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stepProcessorId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledStepProcessorId(org.osid.id.Id stepProcessorId, 
                                          boolean match) {
        return;
    }


    /**
     *  Clears the step processor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledStepProcessorIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> StepProcessorQuery </code> is available. 
     *
     *  @return <code> true </code> if a step processor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledStepProcessorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a step processor. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the step processor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledStepProcessorQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorQuery getRuledStepProcessorQuery() {
        throw new org.osid.UnimplementedException("supportsRuledStepProcessorQuery() is false");
    }


    /**
     *  Matches enablers mapped to any step processor. 
     *
     *  @param  match <code> true </code> for enablers mapped to any step 
     *          processor, <code> false </code> to match enablers mapped to no 
     *          step processors 
     */

    @OSID @Override
    public void matchAnyRuledStepProcessor(boolean match) {
        return;
    }


    /**
     *  Clears the step processor query terms. 
     */

    @OSID @Override
    public void clearRuledStepProcessorTerms() {
        return;
    }


    /**
     *  Matches enablers mapped to the office. 
     *
     *  @param  officeId the office <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOfficeId(org.osid.id.Id officeId, boolean match) {
        return;
    }


    /**
     *  Clears the office <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearOfficeIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> OfficeQuery </code> is available. 
     *
     *  @return <code> true </code> if an office query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfficeQuery() {
        return (false);
    }


    /**
     *  Gets the query for an office. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the office query 
     *  @throws org.osid.UnimplementedException <code> supportsOfficeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.OfficeQuery getOfficeQuery() {
        throw new org.osid.UnimplementedException("supportsOfficeQuery() is false");
    }


    /**
     *  Clears the office query terms. 
     */

    @OSID @Override
    public void clearOfficeTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given step processor enabler query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a step processor enabler implementing the requested record.
     *
     *  @param stepProcessorEnablerRecordType a step processor enabler record type
     *  @return the step processor enabler query record
     *  @throws org.osid.NullArgumentException
     *          <code>stepProcessorEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(stepProcessorEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.rules.records.StepProcessorEnablerQueryRecord getStepProcessorEnablerQueryRecord(org.osid.type.Type stepProcessorEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.workflow.rules.records.StepProcessorEnablerQueryRecord record : this.records) {
            if (record.implementsRecordType(stepProcessorEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(stepProcessorEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this step processor enabler query. 
     *
     *  @param stepProcessorEnablerQueryRecord step processor enabler query record
     *  @param stepProcessorEnablerRecordType stepProcessorEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addStepProcessorEnablerQueryRecord(org.osid.workflow.rules.records.StepProcessorEnablerQueryRecord stepProcessorEnablerQueryRecord, 
                                          org.osid.type.Type stepProcessorEnablerRecordType) {

        addRecordType(stepProcessorEnablerRecordType);
        nullarg(stepProcessorEnablerQueryRecord, "step processor enabler query record");
        this.records.add(stepProcessorEnablerQueryRecord);        
        return;
    }
}
