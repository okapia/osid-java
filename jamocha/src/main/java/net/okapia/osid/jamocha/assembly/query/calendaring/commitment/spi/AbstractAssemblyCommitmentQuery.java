//
// AbstractAssemblyCommitmentQuery.java
//
//     A CommitmentQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.calendaring.commitment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A CommitmentQuery that stores terms.
 */

public abstract class AbstractAssemblyCommitmentQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.calendaring.CommitmentQuery,
               org.osid.calendaring.CommitmentQueryInspector,
               org.osid.calendaring.CommitmentSearchOrder {

    private final java.util.Collection<org.osid.calendaring.records.CommitmentQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.records.CommitmentQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.records.CommitmentSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyCommitmentQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyCommitmentQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the event <code> Id </code> for this query. 
     *
     *  @param  eventId an event <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> eventId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEventId(org.osid.id.Id eventId, boolean match) {
        getAssembler().addIdTerm(getEventIdColumn(), eventId, match);
        return;
    }


    /**
     *  Clears the event <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearEventIdTerms() {
        getAssembler().clearTerms(getEventIdColumn());
        return;
    }


    /**
     *  Gets the event <code> Id </code> terms. 
     *
     *  @return the event <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEventIdTerms() {
        return (getAssembler().getIdTerms(getEventIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the event. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByEvent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getEventColumn(), style);
        return;
    }


    /**
     *  Gets the EventId column name.
     *
     * @return the column name
     */

    protected String getEventIdColumn() {
        return ("event_id");
    }


    /**
     *  Tests if an <code> EventQuery </code> is available. 
     *
     *  @return <code> true </code> if an event query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for an event. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the event query 
     *  @throws org.osid.UnimplementedException <code> supportsEventQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventQuery getEventQuery() {
        throw new org.osid.UnimplementedException("supportsEventQuery() is false");
    }


    /**
     *  Clears the event terms. 
     */

    @OSID @Override
    public void clearEventTerms() {
        getAssembler().clearTerms(getEventColumn());
        return;
    }


    /**
     *  Gets the event terms. 
     *
     *  @return the event terms 
     */

    @OSID @Override
    public org.osid.calendaring.EventQueryInspector[] getEventTerms() {
        return (new org.osid.calendaring.EventQueryInspector[0]);
    }


    /**
     *  Tests if an <code> EventSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if an event search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEventSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for an event. 
     *
     *  @return the event search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEventSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventSearchOrder getEventSearchOrder() {
        throw new org.osid.UnimplementedException("supportsEventSearchOrder() is false");
    }


    /**
     *  Gets the Event column name.
     *
     * @return the column name
     */

    protected String getEventColumn() {
        return ("event");
    }


    /**
     *  Sets the resource <code> Id </code> for this query. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResourceId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getResourceIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearResourceIdTerms() {
        getAssembler().clearTerms(getResourceIdColumn());
        return;
    }


    /**
     *  Gets the resource <code> Id </code> terms. 
     *
     *  @return the resource <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResourceIdTerms() {
        return (getAssembler().getIdTerms(getResourceIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the resource. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByResource(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getResourceColumn(), style);
        return;
    }


    /**
     *  Gets the ResourceId column name.
     *
     * @return the column name
     */

    protected String getResourceIdColumn() {
        return ("resource_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available for querying 
     *  resources. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResourceQuery() {
        throw new org.osid.UnimplementedException("supportsResourceQuery() is false");
    }


    /**
     *  Clears the resource terms. 
     */

    @OSID @Override
    public void clearResourceTerms() {
        getAssembler().clearTerms(getResourceColumn());
        return;
    }


    /**
     *  Gets the resource terms. 
     *
     *  @return the resource terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a <code> ResourceSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a resource. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getResourceSearchOrder() {
        throw new org.osid.UnimplementedException("supportsResourceSearchOrder() is false");
    }


    /**
     *  Gets the Resource column name.
     *
     * @return the column name
     */

    protected String getResourceColumn() {
        return ("resource");
    }


    /**
     *  Sets the calendar <code> Id </code> for this query. 
     *
     *  @param  calendarId a calendar <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCalendarId(org.osid.id.Id calendarId, boolean match) {
        getAssembler().addIdTerm(getCalendarIdColumn(), calendarId, match);
        return;
    }


    /**
     *  Clears the calendar <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCalendarIdTerms() {
        getAssembler().clearTerms(getCalendarIdColumn());
        return;
    }


    /**
     *  Gets the calendar <code> Id </code> terms. 
     *
     *  @return the calendar <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCalendarIdTerms() {
        return (getAssembler().getIdTerms(getCalendarIdColumn()));
    }


    /**
     *  Gets the CalendarId column name.
     *
     * @return the column name
     */

    protected String getCalendarIdColumn() {
        return ("calendar_id");
    }


    /**
     *  Tests if a <code> CalendarQuery </code> is available for querying 
     *  resources. 
     *
     *  @return <code> true </code> if a calendar query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarQuery() {
        return (false);
    }


    /**
     *  Gets the query for a calendar. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the calendar query 
     *  @throws org.osid.UnimplementedException <code> supportsCalendarQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQuery getCalendarQuery() {
        throw new org.osid.UnimplementedException("supportsCalendarQuery() is false");
    }


    /**
     *  Clears the calendar terms. 
     */

    @OSID @Override
    public void clearCalendarTerms() {
        getAssembler().clearTerms(getCalendarColumn());
        return;
    }


    /**
     *  Gets the calendar terms. 
     *
     *  @return the calendar terms 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQueryInspector[] getCalendarTerms() {
        return (new org.osid.calendaring.CalendarQueryInspector[0]);
    }


    /**
     *  Gets the Calendar column name.
     *
     * @return the column name
     */

    protected String getCalendarColumn() {
        return ("calendar");
    }


    /**
     *  Tests if this commitment supports the given record
     *  <code>Type</code>.
     *
     *  @param  commitmentRecordType a commitment record type 
     *  @return <code>true</code> if the commitmentRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type commitmentRecordType) {
        for (org.osid.calendaring.records.CommitmentQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(commitmentRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  commitmentRecordType the commitment record type 
     *  @return the commitment query record 
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(commitmentRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.CommitmentQueryRecord getCommitmentQueryRecord(org.osid.type.Type commitmentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.CommitmentQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(commitmentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(commitmentRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  commitmentRecordType the commitment record type 
     *  @return the commitment query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(commitmentRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.CommitmentQueryInspectorRecord getCommitmentQueryInspectorRecord(org.osid.type.Type commitmentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.CommitmentQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(commitmentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(commitmentRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param commitmentRecordType the commitment record type
     *  @return the commitment search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(commitmentRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.CommitmentSearchOrderRecord getCommitmentSearchOrderRecord(org.osid.type.Type commitmentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.CommitmentSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(commitmentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(commitmentRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this commitment. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param commitmentQueryRecord the commitment query record
     *  @param commitmentQueryInspectorRecord the commitment query inspector
     *         record
     *  @param commitmentSearchOrderRecord the commitment search order record
     *  @param commitmentRecordType commitment record type
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentQueryRecord</code>,
     *          <code>commitmentQueryInspectorRecord</code>,
     *          <code>commitmentSearchOrderRecord</code> or
     *          <code>commitmentRecordTypecommitment</code> is
     *          <code>null</code>
     */
            
    protected void addCommitmentRecords(org.osid.calendaring.records.CommitmentQueryRecord commitmentQueryRecord, 
                                      org.osid.calendaring.records.CommitmentQueryInspectorRecord commitmentQueryInspectorRecord, 
                                      org.osid.calendaring.records.CommitmentSearchOrderRecord commitmentSearchOrderRecord, 
                                      org.osid.type.Type commitmentRecordType) {

        addRecordType(commitmentRecordType);

        nullarg(commitmentQueryRecord, "commitment query record");
        nullarg(commitmentQueryInspectorRecord, "commitment query inspector record");
        nullarg(commitmentSearchOrderRecord, "commitment search odrer record");

        this.queryRecords.add(commitmentQueryRecord);
        this.queryInspectorRecords.add(commitmentQueryInspectorRecord);
        this.searchOrderRecords.add(commitmentSearchOrderRecord);
        
        return;
    }
}
