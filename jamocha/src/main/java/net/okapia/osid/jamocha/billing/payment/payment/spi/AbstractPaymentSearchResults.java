//
// AbstractPaymentSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.payment.payment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractPaymentSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.billing.payment.PaymentSearchResults {

    private org.osid.billing.payment.PaymentList payments;
    private final org.osid.billing.payment.PaymentQueryInspector inspector;
    private final java.util.Collection<org.osid.billing.payment.records.PaymentSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractPaymentSearchResults.
     *
     *  @param payments the result set
     *  @param paymentQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>payments</code>
     *          or <code>paymentQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractPaymentSearchResults(org.osid.billing.payment.PaymentList payments,
                                            org.osid.billing.payment.PaymentQueryInspector paymentQueryInspector) {
        nullarg(payments, "payments");
        nullarg(paymentQueryInspector, "payment query inspectpr");

        this.payments = payments;
        this.inspector = paymentQueryInspector;

        return;
    }


    /**
     *  Gets the payment list resulting from a search.
     *
     *  @return a payment list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPayments() {
        if (this.payments == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.billing.payment.PaymentList payments = this.payments;
        this.payments = null;
	return (payments);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.billing.payment.PaymentQueryInspector getPaymentQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  payment search record <code> Type. </code> This method must
     *  be used to retrieve a payment implementing the requested
     *  record.
     *
     *  @param paymentSearchRecordType a payment search 
     *         record type 
     *  @return the payment search
     *  @throws org.osid.NullArgumentException
     *          <code>paymentSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(paymentSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.payment.records.PaymentSearchResultsRecord getPaymentSearchResultsRecord(org.osid.type.Type paymentSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.billing.payment.records.PaymentSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(paymentSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(paymentSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record payment search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addPaymentRecord(org.osid.billing.payment.records.PaymentSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "payment record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
