//
// AbstractIndexedMapTriggerLookupSession.java
//
//    A simple framework for providing a Trigger lookup service
//    backed by a fixed collection of triggers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Trigger lookup service backed by a
 *  fixed collection of triggers. The triggers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some triggers may be compatible
 *  with more types than are indicated through these trigger
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Triggers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapTriggerLookupSession
    extends AbstractMapTriggerLookupSession
    implements org.osid.control.TriggerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.control.Trigger> triggersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.control.Trigger>());
    private final MultiMap<org.osid.type.Type, org.osid.control.Trigger> triggersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.control.Trigger>());


    /**
     *  Makes a <code>Trigger</code> available in this session.
     *
     *  @param  trigger a trigger
     *  @throws org.osid.NullArgumentException <code>trigger<code> is
     *          <code>null</code>
     */

    @Override
    protected void putTrigger(org.osid.control.Trigger trigger) {
        super.putTrigger(trigger);

        this.triggersByGenus.put(trigger.getGenusType(), trigger);
        
        try (org.osid.type.TypeList types = trigger.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.triggersByRecord.put(types.getNextType(), trigger);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }

    
    /**
     *  Makes an array of triggers available in this session.
     *
     *  @param  triggers an array of triggers
     *  @throws org.osid.NullArgumentException <code>triggers<code>
     *          is <code>null</code>
     */

    @Override
    protected void putTriggers(org.osid.control.Trigger[] triggers) {
        for (org.osid.control.Trigger trigger : triggers) {
            putTrigger(trigger);
        }

        return;
    }


    /**
     *  Makes a collection of triggers available in this session.
     *
     *  @param  triggers a collection of triggers
     *  @throws org.osid.NullArgumentException <code>triggers<code>
     *          is <code>null</code>
     */

    @Override
    protected void putTriggers(java.util.Collection<? extends org.osid.control.Trigger> triggers) {
        for (org.osid.control.Trigger trigger : triggers) {
            putTrigger(trigger);
        }

        return;
    }


    /**
     *  Removes a trigger from this session.
     *
     *  @param triggerId the <code>Id</code> of the trigger
     *  @throws org.osid.NullArgumentException <code>triggerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeTrigger(org.osid.id.Id triggerId) {
        org.osid.control.Trigger trigger;
        try {
            trigger = getTrigger(triggerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.triggersByGenus.remove(trigger.getGenusType());

        try (org.osid.type.TypeList types = trigger.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.triggersByRecord.remove(types.getNextType(), trigger);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeTrigger(triggerId);
        return;
    }


    /**
     *  Gets a <code>TriggerList</code> corresponding to the given
     *  trigger genus <code>Type</code> which does not include
     *  triggers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known triggers or an error results. Otherwise,
     *  the returned list may contain only those triggers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  triggerGenusType a trigger genus type 
     *  @return the returned <code>Trigger</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>triggerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.TriggerList getTriggersByGenusType(org.osid.type.Type triggerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.control.trigger.ArrayTriggerList(this.triggersByGenus.get(triggerGenusType)));
    }


    /**
     *  Gets a <code>TriggerList</code> containing the given
     *  trigger record <code>Type</code>. In plenary mode, the
     *  returned list contains all known triggers or an error
     *  results. Otherwise, the returned list may contain only those
     *  triggers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  triggerRecordType a trigger record type 
     *  @return the returned <code>trigger</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>triggerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.TriggerList getTriggersByRecordType(org.osid.type.Type triggerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.control.trigger.ArrayTriggerList(this.triggersByRecord.get(triggerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.triggersByGenus.clear();
        this.triggersByRecord.clear();

        super.close();

        return;
    }
}
