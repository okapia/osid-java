//
// AbstractAssemblyRepositoryQuery.java
//
//     A RepositoryQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.repository.repository.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A RepositoryQuery that stores terms.
 */

public abstract class AbstractAssemblyRepositoryQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.repository.RepositoryQuery,
               org.osid.repository.RepositoryQueryInspector,
               org.osid.repository.RepositorySearchOrder {

    private final java.util.Collection<org.osid.repository.records.RepositoryQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.repository.records.RepositoryQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.repository.records.RepositorySearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyRepositoryQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyRepositoryQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the asset <code> Id </code> for this query. 
     *
     *  @param  assetId an asset <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assetId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAssetId(org.osid.id.Id assetId, boolean match) {
        getAssembler().addIdTerm(getAssetIdColumn(), assetId, match);
        return;
    }


    /**
     *  Clears the asset <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAssetIdTerms() {
        getAssembler().clearTerms(getAssetIdColumn());
        return;
    }


    /**
     *  Gets the asset <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssetIdTerms() {
        return (getAssembler().getIdTerms(getAssetIdColumn()));
    }


    /**
     *  Gets the AssetId column name.
     *
     * @return the column name
     */

    protected String getAssetIdColumn() {
        return ("asset_id");
    }


    /**
     *  Tests if an <code> AssetQuery </code> is available. 
     *
     *  @return <code> true </code> if an asset query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssetQuery() {
        return (false);
    }


    /**
     *  Gets the query for an asset. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the asset query 
     *  @throws org.osid.UnimplementedException <code> supportsAssetQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetQuery getAssetQuery() {
        throw new org.osid.UnimplementedException("supportsAssetQuery() is false");
    }


    /**
     *  Matches repositories that has any asset mapping. 
     *
     *  @param  match <code> true </code> to match repositories with any 
     *          asset, <code> false </code> to match repositories with no 
     *          asset 
     */

    @OSID @Override
    public void matchAnyAsset(boolean match) {
        getAssembler().addIdWildcardTerm(getAssetColumn(), match);
        return;
    }


    /**
     *  Clears the asset terms. 
     */

    @OSID @Override
    public void clearAssetTerms() {
        getAssembler().clearTerms(getAssetColumn());
        return;
    }


    /**
     *  Gets the asset query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.repository.AssetQueryInspector[] getAssetTerms() {
        return (new org.osid.repository.AssetQueryInspector[0]);
    }


    /**
     *  Gets the Asset column name.
     *
     * @return the column name
     */

    protected String getAssetColumn() {
        return ("asset");
    }


    /**
     *  Sets the composition <code> Id </code> for this query. 
     *
     *  @param  compositionId a composition <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> compositionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCompositionId(org.osid.id.Id compositionId, boolean match) {
        getAssembler().addIdTerm(getCompositionIdColumn(), compositionId, match);
        return;
    }


    /**
     *  Clears the composition <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCompositionIdTerms() {
        getAssembler().clearTerms(getCompositionIdColumn());
        return;
    }


    /**
     *  Gets the composition <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCompositionIdTerms() {
        return (getAssembler().getIdTerms(getCompositionIdColumn()));
    }


    /**
     *  Gets the CompositionId column name.
     *
     * @return the column name
     */

    protected String getCompositionIdColumn() {
        return ("composition_id");
    }


    /**
     *  Tests if a <code> CompositionQuery </code> is available. 
     *
     *  @return <code> true </code> if a composition query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompositionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a composition. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the composition query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionQuery getCompositionQuery() {
        throw new org.osid.UnimplementedException("supportsCompositionQuery() is false");
    }


    /**
     *  Matches repositories that has any composition mapping. 
     *
     *  @param  match <code> true </code> to match repositories with any 
     *          composition, <code> false </code> to match repositories with 
     *          no composition 
     */

    @OSID @Override
    public void matchAnyComposition(boolean match) {
        getAssembler().addIdWildcardTerm(getCompositionColumn(), match);
        return;
    }


    /**
     *  Clears the composition terms. 
     */

    @OSID @Override
    public void clearCompositionTerms() {
        getAssembler().clearTerms(getCompositionColumn());
        return;
    }


    /**
     *  Gets the composition query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.repository.CompositionQueryInspector[] getCompositionTerms() {
        return (new org.osid.repository.CompositionQueryInspector[0]);
    }


    /**
     *  Gets the Composition column name.
     *
     * @return the column name
     */

    protected String getCompositionColumn() {
        return ("composition");
    }


    /**
     *  Sets the repository <code> Id </code> for this query to match 
     *  repositories that have the specified repository as an ancestor. 
     *
     *  @param  repositoryId a repository <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorRepositoryId(org.osid.id.Id repositoryId, 
                                          boolean match) {
        getAssembler().addIdTerm(getAncestorRepositoryIdColumn(), repositoryId, match);
        return;
    }


    /**
     *  Clears the ancestor repository <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorRepositoryIdTerms() {
        getAssembler().clearTerms(getAncestorRepositoryIdColumn());
        return;
    }


    /**
     *  Gets the ancestor repository <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorRepositoryIdTerms() {
        return (getAssembler().getIdTerms(getAncestorRepositoryIdColumn()));
    }


    /**
     *  Gets the AncestorRepositoryId column name.
     *
     * @return the column name
     */

    protected String getAncestorRepositoryIdColumn() {
        return ("ancestor_repository_id");
    }


    /**
     *  Tests if a <code> RepositoryQuery </code> is available. 
     *
     *  @return <code> true </code> if a repository query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorRepositoryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a repository. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the repository query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorRepositoryQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.RepositoryQuery getAncestorRepositoryQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorRepositoryQuery() is false");
    }


    /**
     *  Matches repositories with any ancestor. 
     *
     *  @param  match <code> true </code> to match repositories with any 
     *          ancestor, <code> false </code> to match root repositories 
     */

    @OSID @Override
    public void matchAnyAncestorRepository(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorRepositoryColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor repository terms. 
     */

    @OSID @Override
    public void clearAncestorRepositoryTerms() {
        getAssembler().clearTerms(getAncestorRepositoryColumn());
        return;
    }


    /**
     *  Gets the ancestor repository query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.repository.RepositoryQueryInspector[] getAncestorRepositoryTerms() {
        return (new org.osid.repository.RepositoryQueryInspector[0]);
    }


    /**
     *  Gets the AncestorRepository column name.
     *
     * @return the column name
     */

    protected String getAncestorRepositoryColumn() {
        return ("ancestor_repository");
    }


    /**
     *  Sets the repository <code> Id </code> for this query to match 
     *  repositories that have the specified repository as a descendant. 
     *
     *  @param  repositoryId a repository <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantRepositoryId(org.osid.id.Id repositoryId, 
                                            boolean match) {
        getAssembler().addIdTerm(getDescendantRepositoryIdColumn(), repositoryId, match);
        return;
    }


    /**
     *  Clears the descendant repository <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantRepositoryIdTerms() {
        getAssembler().clearTerms(getDescendantRepositoryIdColumn());
        return;
    }


    /**
     *  Gets the descendant repository <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantRepositoryIdTerms() {
        return (getAssembler().getIdTerms(getDescendantRepositoryIdColumn()));
    }


    /**
     *  Gets the DescendantRepositoryId column name.
     *
     * @return the column name
     */

    protected String getDescendantRepositoryIdColumn() {
        return ("descendant_repository_id");
    }


    /**
     *  Tests if a <code> RepositoryQuery </code> is available. 
     *
     *  @return <code> true </code> if a repository query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantRepositoryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a repository. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the repository query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantRepositoryQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.RepositoryQuery getDescendantRepositoryQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantRepositoryQuery() is false");
    }


    /**
     *  Matches repositories with any descendant. 
     *
     *  @param  match <code> true </code> to match repositories with any 
     *          descendant, <code> false </code> to match leaf repositories 
     */

    @OSID @Override
    public void matchAnyDescendantRepository(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantRepositoryColumn(), match);
        return;
    }


    /**
     *  Clears the descendant repository terms. 
     */

    @OSID @Override
    public void clearDescendantRepositoryTerms() {
        getAssembler().clearTerms(getDescendantRepositoryColumn());
        return;
    }


    /**
     *  Gets the descendant repository query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.repository.RepositoryQueryInspector[] getDescendantRepositoryTerms() {
        return (new org.osid.repository.RepositoryQueryInspector[0]);
    }


    /**
     *  Gets the DescendantRepository column name.
     *
     * @return the column name
     */

    protected String getDescendantRepositoryColumn() {
        return ("descendant_repository");
    }


    /**
     *  Tests if this repository supports the given record
     *  <code>Type</code>.
     *
     *  @param  repositoryRecordType a repository record type 
     *  @return <code>true</code> if the repositoryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>repositoryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type repositoryRecordType) {
        for (org.osid.repository.records.RepositoryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(repositoryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  repositoryRecordType the repository record type 
     *  @return the repository query record 
     *  @throws org.osid.NullArgumentException
     *          <code>repositoryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(repositoryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.repository.records.RepositoryQueryRecord getRepositoryQueryRecord(org.osid.type.Type repositoryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.repository.records.RepositoryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(repositoryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(repositoryRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  repositoryRecordType the repository record type 
     *  @return the repository query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>repositoryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(repositoryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.repository.records.RepositoryQueryInspectorRecord getRepositoryQueryInspectorRecord(org.osid.type.Type repositoryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.repository.records.RepositoryQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(repositoryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(repositoryRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param repositoryRecordType the repository record type
     *  @return the repository search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>repositoryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(repositoryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.repository.records.RepositorySearchOrderRecord getRepositorySearchOrderRecord(org.osid.type.Type repositoryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.repository.records.RepositorySearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(repositoryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(repositoryRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this repository. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param repositoryQueryRecord the repository query record
     *  @param repositoryQueryInspectorRecord the repository query inspector
     *         record
     *  @param repositorySearchOrderRecord the repository search order record
     *  @param repositoryRecordType repository record type
     *  @throws org.osid.NullArgumentException
     *          <code>repositoryQueryRecord</code>,
     *          <code>repositoryQueryInspectorRecord</code>,
     *          <code>repositorySearchOrderRecord</code> or
     *          <code>repositoryRecordTyperepository</code> is
     *          <code>null</code>
     */
            
    protected void addRepositoryRecords(org.osid.repository.records.RepositoryQueryRecord repositoryQueryRecord, 
                                      org.osid.repository.records.RepositoryQueryInspectorRecord repositoryQueryInspectorRecord, 
                                      org.osid.repository.records.RepositorySearchOrderRecord repositorySearchOrderRecord, 
                                      org.osid.type.Type repositoryRecordType) {

        addRecordType(repositoryRecordType);

        nullarg(repositoryQueryRecord, "repository query record");
        nullarg(repositoryQueryInspectorRecord, "repository query inspector record");
        nullarg(repositorySearchOrderRecord, "repository search odrer record");

        this.queryRecords.add(repositoryQueryRecord);
        this.queryInspectorRecords.add(repositoryQueryInspectorRecord);
        this.searchOrderRecords.add(repositorySearchOrderRecord);
        
        return;
    }
}
