//
// AbstractIndexedMapCanonicalUnitEnablerLookupSession.java
//
//    A simple framework for providing a CanonicalUnitEnabler lookup service
//    backed by a fixed collection of canonical unit enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.offering.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a CanonicalUnitEnabler lookup service backed by a
 *  fixed collection of canonical unit enablers. The canonical unit enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some canonical unit enablers may be compatible
 *  with more types than are indicated through these canonical unit enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>CanonicalUnitEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapCanonicalUnitEnablerLookupSession
    extends AbstractMapCanonicalUnitEnablerLookupSession
    implements org.osid.offering.rules.CanonicalUnitEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.offering.rules.CanonicalUnitEnabler> canonicalUnitEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.offering.rules.CanonicalUnitEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.offering.rules.CanonicalUnitEnabler> canonicalUnitEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.offering.rules.CanonicalUnitEnabler>());


    /**
     *  Makes a <code>CanonicalUnitEnabler</code> available in this session.
     *
     *  @param  canonicalUnitEnabler a canonical unit enabler
     *  @throws org.osid.NullArgumentException <code>canonicalUnitEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putCanonicalUnitEnabler(org.osid.offering.rules.CanonicalUnitEnabler canonicalUnitEnabler) {
        super.putCanonicalUnitEnabler(canonicalUnitEnabler);

        this.canonicalUnitEnablersByGenus.put(canonicalUnitEnabler.getGenusType(), canonicalUnitEnabler);
        
        try (org.osid.type.TypeList types = canonicalUnitEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.canonicalUnitEnablersByRecord.put(types.getNextType(), canonicalUnitEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }

    
    /**
     *  Makes an array of canonical unit enablers available in this session.
     *
     *  @param  canonicalUnitEnablers an array of canonical unit enablers
     *  @throws org.osid.NullArgumentException <code>canonicalUnitEnablers<code>
     *          is <code>null</code>
     */

    @Override
    protected void putCanonicalUnitEnablers(org.osid.offering.rules.CanonicalUnitEnabler[] canonicalUnitEnablers) {
        for (org.osid.offering.rules.CanonicalUnitEnabler canonicalUnitEnabler : canonicalUnitEnablers) {
            putCanonicalUnitEnabler(canonicalUnitEnabler);
        }

        return;
    }


    /**
     *  Makes a collection of canonical unit enablers available in this session.
     *
     *  @param  canonicalUnitEnablers a collection of canonical unit enablers
     *  @throws org.osid.NullArgumentException <code>canonicalUnitEnablers<code>
     *          is <code>null</code>
     */

    @Override
    protected void putCanonicalUnitEnablers(java.util.Collection<? extends org.osid.offering.rules.CanonicalUnitEnabler> canonicalUnitEnablers) {
        for (org.osid.offering.rules.CanonicalUnitEnabler canonicalUnitEnabler : canonicalUnitEnablers) {
            putCanonicalUnitEnabler(canonicalUnitEnabler);
        }

        return;
    }


    /**
     *  Removes a canonical unit enabler from this session.
     *
     *  @param canonicalUnitEnablerId the <code>Id</code> of the canonical unit enabler
     *  @throws org.osid.NullArgumentException <code>canonicalUnitEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeCanonicalUnitEnabler(org.osid.id.Id canonicalUnitEnablerId) {
        org.osid.offering.rules.CanonicalUnitEnabler canonicalUnitEnabler;
        try {
            canonicalUnitEnabler = getCanonicalUnitEnabler(canonicalUnitEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.canonicalUnitEnablersByGenus.remove(canonicalUnitEnabler.getGenusType());

        try (org.osid.type.TypeList types = canonicalUnitEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.canonicalUnitEnablersByRecord.remove(types.getNextType(), canonicalUnitEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeCanonicalUnitEnabler(canonicalUnitEnablerId);
        return;
    }


    /**
     *  Gets a <code>CanonicalUnitEnablerList</code> corresponding to the given
     *  canonical unit enabler genus <code>Type</code> which does not include
     *  canonical unit enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known canonical unit enablers or an error results. Otherwise,
     *  the returned list may contain only those canonical unit enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  canonicalUnitEnablerGenusType a canonical unit enabler genus type 
     *  @return the returned <code>CanonicalUnitEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerList getCanonicalUnitEnablersByGenusType(org.osid.type.Type canonicalUnitEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.offering.rules.canonicalunitenabler.ArrayCanonicalUnitEnablerList(this.canonicalUnitEnablersByGenus.get(canonicalUnitEnablerGenusType)));
    }


    /**
     *  Gets a <code>CanonicalUnitEnablerList</code> containing the given
     *  canonical unit enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known canonical unit enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  canonical unit enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  canonicalUnitEnablerRecordType a canonical unit enabler record type 
     *  @return the returned <code>canonicalUnitEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerList getCanonicalUnitEnablersByRecordType(org.osid.type.Type canonicalUnitEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.offering.rules.canonicalunitenabler.ArrayCanonicalUnitEnablerList(this.canonicalUnitEnablersByRecord.get(canonicalUnitEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.canonicalUnitEnablersByGenus.clear();
        this.canonicalUnitEnablersByRecord.clear();

        super.close();

        return;
    }
}
