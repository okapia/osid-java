//
// AbstractOrganizationSearchOdrer.java
//
//     Defines an OrganizationSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.personnel.organization.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an {@code OrganizationSearchOrder}.
 */

public abstract class AbstractOrganizationSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractTemporalOsidObjectSearchOrder
    implements org.osid.personnel.OrganizationSearchOrder {

    private final java.util.Collection<org.osid.personnel.records.OrganizationSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by the display 
     *  label. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDisplayLabel(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  organizationRecordType an organization record type 
     *  @return {@code true} if the organizationRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code organizationRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type organizationRecordType) {
        for (org.osid.personnel.records.OrganizationSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(organizationRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  organizationRecordType the organization record type 
     *  @return the organization search order record
     *  @throws org.osid.NullArgumentException
     *          {@code organizationRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(organizationRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.personnel.records.OrganizationSearchOrderRecord getOrganizationSearchOrderRecord(org.osid.type.Type organizationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.personnel.records.OrganizationSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(organizationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(organizationRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this organization. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param organizationRecord the organization search odrer record
     *  @param organizationRecordType organization record type
     *  @throws org.osid.NullArgumentException
     *          {@code organizationRecord} or
     *          {@code organizationRecordTypeorganization} is
     *          {@code null}
     */
            
    protected void addOrganizationRecord(org.osid.personnel.records.OrganizationSearchOrderRecord organizationSearchOrderRecord, 
                                     org.osid.type.Type organizationRecordType) {

        addRecordType(organizationRecordType);
        this.records.add(organizationSearchOrderRecord);
        
        return;
    }
}
