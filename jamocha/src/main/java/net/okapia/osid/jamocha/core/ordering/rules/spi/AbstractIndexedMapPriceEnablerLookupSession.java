//
// AbstractIndexedMapPriceEnablerLookupSession.java
//
//    A simple framework for providing a PriceEnabler lookup service
//    backed by a fixed collection of price enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.ordering.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a PriceEnabler lookup service backed by a
 *  fixed collection of price enablers. The price enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some price enablers may be compatible
 *  with more types than are indicated through these price enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>PriceEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapPriceEnablerLookupSession
    extends AbstractMapPriceEnablerLookupSession
    implements org.osid.ordering.rules.PriceEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.ordering.rules.PriceEnabler> priceEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.ordering.rules.PriceEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.ordering.rules.PriceEnabler> priceEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.ordering.rules.PriceEnabler>());


    /**
     *  Makes a <code>PriceEnabler</code> available in this session.
     *
     *  @param  priceEnabler a price enabler
     *  @throws org.osid.NullArgumentException <code>priceEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putPriceEnabler(org.osid.ordering.rules.PriceEnabler priceEnabler) {
        super.putPriceEnabler(priceEnabler);

        this.priceEnablersByGenus.put(priceEnabler.getGenusType(), priceEnabler);
        
        try (org.osid.type.TypeList types = priceEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.priceEnablersByRecord.put(types.getNextType(), priceEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a price enabler from this session.
     *
     *  @param priceEnablerId the <code>Id</code> of the price enabler
     *  @throws org.osid.NullArgumentException <code>priceEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removePriceEnabler(org.osid.id.Id priceEnablerId) {
        org.osid.ordering.rules.PriceEnabler priceEnabler;
        try {
            priceEnabler = getPriceEnabler(priceEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.priceEnablersByGenus.remove(priceEnabler.getGenusType());

        try (org.osid.type.TypeList types = priceEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.priceEnablersByRecord.remove(types.getNextType(), priceEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removePriceEnabler(priceEnablerId);
        return;
    }


    /**
     *  Gets a <code>PriceEnablerList</code> corresponding to the given
     *  price enabler genus <code>Type</code> which does not include
     *  price enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known price enablers or an error results. Otherwise,
     *  the returned list may contain only those price enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  priceEnablerGenusType a price enabler genus type 
     *  @return the returned <code>PriceEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>priceEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerList getPriceEnablersByGenusType(org.osid.type.Type priceEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.ordering.rules.priceenabler.ArrayPriceEnablerList(this.priceEnablersByGenus.get(priceEnablerGenusType)));
    }


    /**
     *  Gets a <code>PriceEnablerList</code> containing the given
     *  price enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known price enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  price enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  priceEnablerRecordType a price enabler record type 
     *  @return the returned <code>priceEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>priceEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerList getPriceEnablersByRecordType(org.osid.type.Type priceEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.ordering.rules.priceenabler.ArrayPriceEnablerList(this.priceEnablersByRecord.get(priceEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.priceEnablersByGenus.clear();
        this.priceEnablersByRecord.clear();

        super.close();

        return;
    }
}
