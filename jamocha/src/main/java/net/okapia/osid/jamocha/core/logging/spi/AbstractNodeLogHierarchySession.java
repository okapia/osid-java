//
// AbstractNodeLogHierarchySession.java
//
//     Defines a Log hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.logging.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a log hierarchy session for delivering a hierarchy
 *  of logs using the LogNode interface.
 */

public abstract class AbstractNodeLogHierarchySession
    extends net.okapia.osid.jamocha.logging.spi.AbstractLogHierarchySession
    implements org.osid.logging.LogHierarchySession {

    private java.util.Collection<org.osid.logging.LogNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root log <code> Ids </code> in this hierarchy.
     *
     *  @return the root log <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootLogIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.logging.lognode.LogNodeToIdList(this.roots));
    }


    /**
     *  Gets the root logs in the log hierarchy. A node
     *  with no parents is an orphan. While all log <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root logs 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.logging.LogList getRootLogs()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.logging.lognode.LogNodeToLogList(new net.okapia.osid.jamocha.logging.lognode.ArrayLogNodeList(this.roots)));
    }


    /**
     *  Adds a root log node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootLog(org.osid.logging.LogNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root log nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootLogs(java.util.Collection<org.osid.logging.LogNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root log node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootLog(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.logging.LogNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Log </code> has any parents. 
     *
     *  @param  logId a log <code> Id </code> 
     *  @return <code> true </code> if the log has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> logId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> logId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentLogs(org.osid.id.Id logId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getLogNode(logId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  log.
     *
     *  @param  id an <code> Id </code> 
     *  @param  logId the <code> Id </code> of a log 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> logId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> logId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> logId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfLog(org.osid.id.Id id, org.osid.id.Id logId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.logging.LogNodeList parents = getLogNode(logId).getParentLogNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextLogNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given log. 
     *
     *  @param  logId a log <code> Id </code> 
     *  @return the parent <code> Ids </code> of the log 
     *  @throws org.osid.NotFoundException <code> logId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> logId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentLogIds(org.osid.id.Id logId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.logging.log.LogToIdList(getParentLogs(logId)));
    }


    /**
     *  Gets the parents of the given log. 
     *
     *  @param  logId the <code> Id </code> to query 
     *  @return the parents of the log 
     *  @throws org.osid.NotFoundException <code> logId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> logId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.logging.LogList getParentLogs(org.osid.id.Id logId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.logging.lognode.LogNodeToLogList(getLogNode(logId).getParentLogNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  log.
     *
     *  @param  id an <code> Id </code> 
     *  @param  logId the Id of a log 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> logId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> logId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> logId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfLog(org.osid.id.Id id, org.osid.id.Id logId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfLog(id, logId)) {
            return (true);
        }

        try (org.osid.logging.LogList parents = getParentLogs(logId)) {
            while (parents.hasNext()) {
                if (isAncestorOfLog(id, parents.getNextLog().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a log has any children. 
     *
     *  @param  logId a log <code> Id </code> 
     *  @return <code> true </code> if the <code> logId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> logId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> logId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildLogs(org.osid.id.Id logId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getLogNode(logId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  log.
     *
     *  @param  id an <code> Id </code> 
     *  @param logId the <code> Id </code> of a 
     *         log
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> logId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> logId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> logId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfLog(org.osid.id.Id id, org.osid.id.Id logId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfLog(logId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  log.
     *
     *  @param  logId the <code> Id </code> to query 
     *  @return the children of the log 
     *  @throws org.osid.NotFoundException <code> logId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> logId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildLogIds(org.osid.id.Id logId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.logging.log.LogToIdList(getChildLogs(logId)));
    }


    /**
     *  Gets the children of the given log. 
     *
     *  @param  logId the <code> Id </code> to query 
     *  @return the children of the log 
     *  @throws org.osid.NotFoundException <code> logId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> logId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.logging.LogList getChildLogs(org.osid.id.Id logId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.logging.lognode.LogNodeToLogList(getLogNode(logId).getChildLogNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  log.
     *
     *  @param  id an <code> Id </code> 
     *  @param logId the <code> Id </code> of a 
     *         log
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> logId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> logId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> logId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfLog(org.osid.id.Id id, org.osid.id.Id logId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfLog(logId, id)) {
            return (true);
        }

        try (org.osid.logging.LogList children = getChildLogs(logId)) {
            while (children.hasNext()) {
                if (isDescendantOfLog(id, children.getNextLog().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  log.
     *
     *  @param  logId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified log node 
     *  @throws org.osid.NotFoundException <code> logId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> logId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getLogNodeIds(org.osid.id.Id logId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.logging.lognode.LogNodeToNode(getLogNode(logId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given log.
     *
     *  @param  logId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified log node 
     *  @throws org.osid.NotFoundException <code> logId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> logId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.logging.LogNode getLogNodes(org.osid.id.Id logId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getLogNode(logId));
    }


    /**
     *  Closes this <code>LogHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a log node.
     *
     *  @param logId the id of the log node
     *  @throws org.osid.NotFoundException <code>logId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>logId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.logging.LogNode getLogNode(org.osid.id.Id logId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(logId, "log Id");
        for (org.osid.logging.LogNode log : this.roots) {
            if (log.getId().equals(logId)) {
                return (log);
            }

            org.osid.logging.LogNode r = findLog(log, logId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(logId + " is not found");
    }


    protected org.osid.logging.LogNode findLog(org.osid.logging.LogNode node, 
                                               org.osid.id.Id logId) 
	throws org.osid.OperationFailedException {

        try (org.osid.logging.LogNodeList children = node.getChildLogNodes()) {
            while (children.hasNext()) {
                org.osid.logging.LogNode log = children.getNextLogNode();
                if (log.getId().equals(logId)) {
                    return (log);
                }
                
                log = findLog(log, logId);
                if (log != null) {
                    return (log);
                }
            }
        }

        return (null);
    }
}
