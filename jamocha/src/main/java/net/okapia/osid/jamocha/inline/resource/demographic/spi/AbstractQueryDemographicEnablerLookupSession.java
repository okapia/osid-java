//
// AbstractQueryDemographicEnablerLookupSession.java
//
//    An inline adapter that maps a DemographicEnablerLookupSession to
//    a DemographicEnablerQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.resource.demographic.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a DemographicEnablerLookupSession to
 *  a DemographicEnablerQuerySession.
 */

public abstract class AbstractQueryDemographicEnablerLookupSession
    extends net.okapia.osid.jamocha.resource.demographic.spi.AbstractDemographicEnablerLookupSession
    implements org.osid.resource.demographic.DemographicEnablerLookupSession {

    private boolean activeonly    = false;
    private boolean effectiveonly = false;
    private final org.osid.resource.demographic.DemographicEnablerQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryDemographicEnablerLookupSession.
     *
     *  @param querySession the underlying demographic enabler query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryDemographicEnablerLookupSession(org.osid.resource.demographic.DemographicEnablerQuerySession querySession) {
        nullarg(querySession, "demographic enabler query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Bin</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Bin Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBinId() {
        return (this.session.getBinId());
    }


    /**
     *  Gets the <code>Bin</code> associated with this 
     *  session.
     *
     *  @return the <code>Bin</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.Bin getBin()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getBin());
    }


    /**
     *  Tests if this user can perform <code>DemographicEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupDemographicEnablers() {
        return (this.session.canSearchDemographicEnablers());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include demographic enablers in bins which are children
     *  of this bin in the bin hierarchy.
     */

    @OSID @Override
    public void useFederatedBinView() {
        this.session.useFederatedBinView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this bin only.
     */

    @OSID @Override
    public void useIsolatedBinView() {
        this.session.useIsolatedBinView();
        return;
    }
    

    /**
     *  Only active demographic enablers are returned by methods in
     *  this session.
     */
     
    @OSID @Override
    public void useActiveDemographicEnablerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive demographic enablers are returned by
     *  methods in this session.
     */
    
    @OSID @Override
    public void useAnyStatusDemographicEnablerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>DemographicEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>DemographicEnabler</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>DemographicEnabler</code> and retained for
     *  compatibility.
     *
     *  In active mode, demographic enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  demographic enablers are returned.
     *
     *  @param  demographicEnablerId <code>Id</code> of the
     *          <code>DemographicEnabler</code>
     *  @return the demographic enabler
     *  @throws org.osid.NotFoundException <code>demographicEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>demographicEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnabler getDemographicEnabler(org.osid.id.Id demographicEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.demographic.DemographicEnablerQuery query = getQuery();
        query.matchId(demographicEnablerId, true);
        org.osid.resource.demographic.DemographicEnablerList demographicEnablers = this.session.getDemographicEnablersByQuery(query);
        if (demographicEnablers.hasNext()) {
            return (demographicEnablers.getNextDemographicEnabler());
        } 
        
        throw new org.osid.NotFoundException(demographicEnablerId + " not found");
    }


    /**
     *  Gets a <code>DemographicEnablerList</code> corresponding to
     *  the given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  demographicEnablers specified in the <code>Id</code> list, in
     *  the order of the list, including duplicates, or an error
     *  results if an <code>Id</code> in the supplied list is not
     *  found or inaccessible. Otherwise, inaccessible
     *  <code>DemographicEnablers</code> may be omitted from the list
     *  and may present the elements in any order including returning
     *  a unique set.
     *
     *  In active mode, demographic enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  demographic enablers are returned.
     *
     *  @param  demographicEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>DemographicEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>demographicEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerList getDemographicEnablersByIds(org.osid.id.IdList demographicEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.demographic.DemographicEnablerQuery query = getQuery();

        try (org.osid.id.IdList ids = demographicEnablerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getDemographicEnablersByQuery(query));
    }


    /**
     *  Gets a <code>DemographicEnablerList</code> corresponding to
     *  the given demographic enabler genus <code>Type</code> which
     *  does not include demographic enablers of types derived from
     *  the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  demographic enablers or an error results. Otherwise, the
     *  returned list may contain only those demographic enablers that
     *  are accessible through this session. In both cases, the order
     *  of the set is not specified.
     *
     *  In active mode, demographic enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  demographic enablers are returned.
     *
     *  @param  demographicEnablerGenusType a demographicEnabler genus type 
     *  @return the returned <code>DemographicEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>demographicEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerList getDemographicEnablersByGenusType(org.osid.type.Type demographicEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.demographic.DemographicEnablerQuery query = getQuery();
        query.matchGenusType(demographicEnablerGenusType, true);
        return (this.session.getDemographicEnablersByQuery(query));
    }


    /**
     *  Gets a <code>DemographicEnablerList</code> corresponding to
     *  the given demographic enabler genus <code>Type</code> and
     *  include any additional demographic enablers with genus types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  demographic enablers or an error results. Otherwise, the
     *  returned list may contain only those demographic enablers that
     *  are accessible through this session. In both cases, the order
     *  of the set is not specified.
     *
     *  In active mode, demographic enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  demographic enablers are returned.
     *
     *  @param  demographicEnablerGenusType a demographicEnabler genus type 
     *  @return the returned <code>DemographicEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>demographicEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerList getDemographicEnablersByParentGenusType(org.osid.type.Type demographicEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.demographic.DemographicEnablerQuery query = getQuery();
        query.matchParentGenusType(demographicEnablerGenusType, true);
        return (this.session.getDemographicEnablersByQuery(query));
    }


    /**
     *  Gets a <code>DemographicEnablerList</code> containing the
     *  given demographic enabler record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known
     *  demographic enablers or an error results. Otherwise, the
     *  returned list may contain only those demographic enablers that
     *  are accessible through this session. In both cases, the order
     *  of the set is not specified.
     *
     *  In active mode, demographic enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  demographic enablers are returned.
     *
     *  @param  demographicEnablerRecordType a demographicEnabler record type 
     *  @return the returned <code>DemographicEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>demographicEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerList getDemographicEnablersByRecordType(org.osid.type.Type demographicEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.demographic.DemographicEnablerQuery query = getQuery();
        query.matchRecordType(demographicEnablerRecordType, true);
        return (this.session.getDemographicEnablersByQuery(query));
    }


    /**
     *  Gets a <code>DemographicEnablerList</code> effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  demographic enablers or an error results. Otherwise, the
     *  returned list may contain only those demographic enablers that
     *  are accessible through this session.
     *  
     *  In active mode, demographic enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  demographic enablers are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>DemographicEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerList getDemographicEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.demographic.DemographicEnablerQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getDemographicEnablersByQuery(query));
    }
        
    
    /**
     *  Gets all <code>DemographicEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  demographic enablers or an error results. Otherwise, the
     *  returned list may contain only those demographic enablers that
     *  are accessible through this session. In both cases, the order
     *  of the set is not specified.
     *
     *  In active mode, demographic enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  demographic enablers are returned.
     *
     *  @return a list of <code>DemographicEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerList getDemographicEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.demographic.DemographicEnablerQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getDemographicEnablersByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.resource.demographic.DemographicEnablerQuery getQuery() {
        org.osid.resource.demographic.DemographicEnablerQuery query = this.session.getDemographicEnablerQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
