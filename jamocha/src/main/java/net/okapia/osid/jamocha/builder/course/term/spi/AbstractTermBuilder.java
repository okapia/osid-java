//
// AbstractTerm.java
//
//     Defines a Term builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.term.spi;


/**
 *  Defines a <code>Term</code> builder.
 */

public abstract class AbstractTermBuilder<T extends AbstractTermBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.course.term.TermMiter term;


    /**
     *  Constructs a new <code>AbstractTermBuilder</code>.
     *
     *  @param term the term to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractTermBuilder(net.okapia.osid.jamocha.builder.course.term.TermMiter term) {
        super(term);
        this.term = term;
        return;
    }


    /**
     *  Builds the term.
     *
     *  @return the new term
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.course.Term build() {
        (new net.okapia.osid.jamocha.builder.validator.course.term.TermValidator(getValidations())).validate(this.term);
        return (new net.okapia.osid.jamocha.builder.course.term.ImmutableTerm(this.term));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the term miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.course.term.TermMiter getMiter() {
        return (this.term);
    }


    /**
     *  Sets the display label.
     *
     *  @param displayLabel a display label
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>displayLabel</code> is <code>null</code>
     */

    public T displayLabel(org.osid.locale.DisplayText displayLabel) {
        getMiter().setDisplayLabel(displayLabel);
        return (self());
    }


    /**
     *  Sets the open date.
     *
     *  @param date an open date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T openDate(org.osid.calendaring.DateTime date) {
        getMiter().setOpenDate(date);
        return (self());
    }


    /**
     *  Sets the registration start.
     *
     *  @param date a registration start
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T registrationStart(org.osid.calendaring.DateTime date) {
        getMiter().setRegistrationStart(date);
        return (self());
    }


    /**
     *  Sets the registration end.
     *
     *  @param date a registration end
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T registrationEnd(org.osid.calendaring.DateTime date) {
        getMiter().setRegistrationEnd(date);
        return (self());
    }


    /**
     *  Sets the classes start.
     *
     *  @param date a classes start
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T classesStart(org.osid.calendaring.DateTime date) {
        getMiter().setClassesStart(date);
        return (self());
    }


    /**
     *  Sets the classes end.
     *
     *  @param date a classes end
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T classesEnd(org.osid.calendaring.DateTime date) {
        getMiter().setClassesEnd(date);
        return (self());
    }


    /**
     *  Sets the add date.
     *
     *  @param date the add date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T addDate(org.osid.calendaring.DateTime date) {
        getMiter().setAddDate(date);
        return (self());
    }


    /**
     *  Sets the drop date.
     *
     *  @param date the drop date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T dropDate(org.osid.calendaring.DateTime date) {
        getMiter().setDropDate(date);
        return (self());
    }


    /**
     *  Sets the final exam start.
     *
     *  @param date a final exam start
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T finalExamStart(org.osid.calendaring.DateTime date) {
        getMiter().setFinalExamStart(date);
        return (self());
    }


    /**
     *  Sets the final exam end.
     *
     *  @param date a final exam end
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T finalExamEnd(org.osid.calendaring.DateTime date) {
        getMiter().setFinalExamEnd(date);
        return (self());
    }


    /**
     *  Sets the close date.
     *
     *  @param date a close date
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>date</code> is <code>null</code>
     */

    public T closeDate(org.osid.calendaring.DateTime date) {
        getMiter().setCloseDate(date);
        return (self());
    }


    /**
     *  Adds a Term record.
     *
     *  @param record a term record
     *  @param recordType the type of term record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.course.records.TermRecord record, org.osid.type.Type recordType) {
        getMiter().addTermRecord(record, recordType);
        return (self());
    }
}       


