//
// MutableIndexedMapBinLookupSession
//
//    Implements a Bin lookup service backed by a collection of
//    bins indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resource;


/**
 *  Implements a Bin lookup service backed by a collection of
 *  bins. The bins are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some bins may be compatible
 *  with more types than are indicated through these bin
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of bins can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapBinLookupSession
    extends net.okapia.osid.jamocha.core.resource.spi.AbstractIndexedMapBinLookupSession
    implements org.osid.resource.BinLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapBinLookupSession} with no
     *  bins.
     */

    public MutableIndexedMapBinLookupSession() {
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapBinLookupSession} with a
     *  single bin.
     *  
     *  @param  bin a single bin
     *  @throws org.osid.NullArgumentException {@code bin}
     *          is {@code null}
     */

    public MutableIndexedMapBinLookupSession(org.osid.resource.Bin bin) {
        putBin(bin);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapBinLookupSession} using an
     *  array of bins.
     *
     *  @param  bins an array of bins
     *  @throws org.osid.NullArgumentException {@code bins}
     *          is {@code null}
     */

    public MutableIndexedMapBinLookupSession(org.osid.resource.Bin[] bins) {
        putBins(bins);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapBinLookupSession} using a
     *  collection of bins.
     *
     *  @param  bins a collection of bins
     *  @throws org.osid.NullArgumentException {@code bins} is
     *          {@code null}
     */

    public MutableIndexedMapBinLookupSession(java.util.Collection<? extends org.osid.resource.Bin> bins) {
        putBins(bins);
        return;
    }
    

    /**
     *  Makes a {@code Bin} available in this session.
     *
     *  @param  bin a bin
     *  @throws org.osid.NullArgumentException {@code bin{@code  is
     *          {@code null}
     */

    @Override
    public void putBin(org.osid.resource.Bin bin) {
        super.putBin(bin);
        return;
    }


    /**
     *  Makes an array of bins available in this session.
     *
     *  @param  bins an array of bins
     *  @throws org.osid.NullArgumentException {@code bins{@code 
     *          is {@code null}
     */

    @Override
    public void putBins(org.osid.resource.Bin[] bins) {
        super.putBins(bins);
        return;
    }


    /**
     *  Makes collection of bins available in this session.
     *
     *  @param  bins a collection of bins
     *  @throws org.osid.NullArgumentException {@code bin{@code  is
     *          {@code null}
     */

    @Override
    public void putBins(java.util.Collection<? extends org.osid.resource.Bin> bins) {
        super.putBins(bins);
        return;
    }


    /**
     *  Removes a Bin from this session.
     *
     *  @param binId the {@code Id} of the bin
     *  @throws org.osid.NullArgumentException {@code binId{@code  is
     *          {@code null}
     */

    @Override
    public void removeBin(org.osid.id.Id binId) {
        super.removeBin(binId);
        return;
    }    
}
