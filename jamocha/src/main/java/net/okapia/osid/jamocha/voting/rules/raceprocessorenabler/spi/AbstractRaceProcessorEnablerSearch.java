//
// AbstractRaceProcessorEnablerSearch.java
//
//     A template for making a RaceProcessorEnabler Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.rules.raceprocessorenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing race processor enabler searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractRaceProcessorEnablerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.voting.rules.RaceProcessorEnablerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.voting.rules.records.RaceProcessorEnablerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.voting.rules.RaceProcessorEnablerSearchOrder raceProcessorEnablerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of race processor enablers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  raceProcessorEnablerIds list of race processor enablers
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorEnablerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongRaceProcessorEnablers(org.osid.id.IdList raceProcessorEnablerIds) {
        while (raceProcessorEnablerIds.hasNext()) {
            try {
                this.ids.add(raceProcessorEnablerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongRaceProcessorEnablers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of race processor enabler Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getRaceProcessorEnablerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  raceProcessorEnablerSearchOrder race processor enabler search order 
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorEnablerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>raceProcessorEnablerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderRaceProcessorEnablerResults(org.osid.voting.rules.RaceProcessorEnablerSearchOrder raceProcessorEnablerSearchOrder) {
	this.raceProcessorEnablerSearchOrder = raceProcessorEnablerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.voting.rules.RaceProcessorEnablerSearchOrder getRaceProcessorEnablerSearchOrder() {
	return (this.raceProcessorEnablerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given race processor enabler search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a race processor enabler implementing the requested record.
     *
     *  @param raceProcessorEnablerSearchRecordType a race processor enabler search record
     *         type
     *  @return the race processor enabler search record
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(raceProcessorEnablerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.rules.records.RaceProcessorEnablerSearchRecord getRaceProcessorEnablerSearchRecord(org.osid.type.Type raceProcessorEnablerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.voting.rules.records.RaceProcessorEnablerSearchRecord record : this.records) {
            if (record.implementsRecordType(raceProcessorEnablerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(raceProcessorEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this race processor enabler search. 
     *
     *  @param raceProcessorEnablerSearchRecord race processor enabler search record
     *  @param raceProcessorEnablerSearchRecordType raceProcessorEnabler search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRaceProcessorEnablerSearchRecord(org.osid.voting.rules.records.RaceProcessorEnablerSearchRecord raceProcessorEnablerSearchRecord, 
                                           org.osid.type.Type raceProcessorEnablerSearchRecordType) {

        addRecordType(raceProcessorEnablerSearchRecordType);
        this.records.add(raceProcessorEnablerSearchRecord);        
        return;
    }
}
