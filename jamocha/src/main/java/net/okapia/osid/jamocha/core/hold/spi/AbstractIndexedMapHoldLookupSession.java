//
// AbstractIndexedMapHoldLookupSession.java
//
//    A simple framework for providing a Hold lookup service
//    backed by a fixed collection of holds with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.hold.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Hold lookup service backed by a
 *  fixed collection of holds. The holds are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some holds may be compatible
 *  with more types than are indicated through these hold
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Holds</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapHoldLookupSession
    extends AbstractMapHoldLookupSession
    implements org.osid.hold.HoldLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.hold.Hold> holdsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.hold.Hold>());
    private final MultiMap<org.osid.type.Type, org.osid.hold.Hold> holdsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.hold.Hold>());


    /**
     *  Makes a <code>Hold</code> available in this session.
     *
     *  @param  hold a hold
     *  @throws org.osid.NullArgumentException <code>hold<code> is
     *          <code>null</code>
     */

    @Override
    protected void putHold(org.osid.hold.Hold hold) {
        super.putHold(hold);

        this.holdsByGenus.put(hold.getGenusType(), hold);
        
        try (org.osid.type.TypeList types = hold.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.holdsByRecord.put(types.getNextType(), hold);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a hold from this session.
     *
     *  @param holdId the <code>Id</code> of the hold
     *  @throws org.osid.NullArgumentException <code>holdId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeHold(org.osid.id.Id holdId) {
        org.osid.hold.Hold hold;
        try {
            hold = getHold(holdId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.holdsByGenus.remove(hold.getGenusType());

        try (org.osid.type.TypeList types = hold.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.holdsByRecord.remove(types.getNextType(), hold);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeHold(holdId);
        return;
    }


    /**
     *  Gets a <code>HoldList</code> corresponding to the given
     *  hold genus <code>Type</code> which does not include
     *  holds of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known holds or an error results. Otherwise,
     *  the returned list may contain only those holds that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  holdGenusType a hold genus type 
     *  @return the returned <code>Hold</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>holdGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.HoldList getHoldsByGenusType(org.osid.type.Type holdGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.hold.hold.ArrayHoldList(this.holdsByGenus.get(holdGenusType)));
    }


    /**
     *  Gets a <code>HoldList</code> containing the given
     *  hold record <code>Type</code>. In plenary mode, the
     *  returned list contains all known holds or an error
     *  results. Otherwise, the returned list may contain only those
     *  holds that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  holdRecordType a hold record type 
     *  @return the returned <code>hold</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>holdRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.HoldList getHoldsByRecordType(org.osid.type.Type holdRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.hold.hold.ArrayHoldList(this.holdsByRecord.get(holdRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.holdsByGenus.clear();
        this.holdsByRecord.clear();

        super.close();

        return;
    }
}
