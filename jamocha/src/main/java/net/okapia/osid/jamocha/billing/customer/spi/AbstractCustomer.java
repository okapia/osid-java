//
// AbstractCustomer.java
//
//     Defines a Customer.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.customer.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Customer</code>.
 */

public abstract class AbstractCustomer
    extends net.okapia.osid.jamocha.spi.AbstractTemporalOsidObject
    implements org.osid.billing.Customer {

    private org.osid.resource.Resource resource;
    private String customerNumber;
    private org.osid.financials.Activity activity;

    private final java.util.Collection<org.osid.billing.records.CustomerRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the resource <code> Id </code> representing the customer. 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResourceId() {
        return (this.resource.getId());
    }


    /**
     *  Gets the resource. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource()
        throws org.osid.OperationFailedException {

        return (this.resource);
    }


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    protected void setResource(org.osid.resource.Resource resource) {
        nullarg(resource, "resource");
        this.resource = resource;
        return;
    }


    /**
     *  Gets the customer account number. 
     *
     *  @return the customer number 
     */

    @OSID @Override
    public String getCustomerNumber() {
        return (this.customerNumber);
    }


    /**
     *  Sets the customer number.
     *
     *  @param number a customer number
     *  @throws org.osid.NullArgumentException <code>number</code> is
     *          <code>null</code>
     */

    protected void setCustomerNumber(String number) {
        nullarg(number, "customer number");
        this.customerNumber = number;
        return;
    }


    /**
     *  Tests if this customer is linked to a financial activity, 
     *
     *  @return <code> true </code> if this customer has an activity, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasActivity() {
        return (this.activity != null);
    }


    /**
     *  Gets the activity <code> Id. </code> 
     *
     *  @return the activity <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasActivity()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.id.Id getActivityId() {
        if (!hasActivity()) {
            throw new org.osid.IllegalStateException("hasActivity() is false");
        }

        return (this.activity.getId());
    }


    /**
     *  Gets the activity. 
     *
     *  @return the activity 
     *  @throws org.osid.IllegalStateException <code> hasActivity() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.financials.Activity getActivity()
        throws org.osid.OperationFailedException {

        if (!hasActivity()) {
            throw new org.osid.IllegalStateException("hasActivity() is false");
        }

        return (this.activity);
    }


    /**
     *  Sets the activity.
     *
     *  @param activity an activity
     *  @throws org.osid.NullArgumentException <code>activity</code>
     *          is <code>null</code>
     */

    protected void setActivity(org.osid.financials.Activity activity) {
        nullarg(activity, "activity");
        this.activity = activity;
        return;
    }


    /**
     *  Tests if this customer supports the given record
     *  <code>Type</code>.
     *
     *  @param  customerRecordType a customer record type 
     *  @return <code>true</code> if the customerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>customerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type customerRecordType) {
        for (org.osid.billing.records.CustomerRecord record : this.records) {
            if (record.implementsRecordType(customerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Customer</code> record <code>Type</code>.
     *
     *  @param  customerRecordType the customer record type 
     *  @return the customer record 
     *  @throws org.osid.NullArgumentException
     *          <code>customerRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(customerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.records.CustomerRecord getCustomerRecord(org.osid.type.Type customerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.records.CustomerRecord record : this.records) {
            if (record.implementsRecordType(customerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(customerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this customer. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param customerRecord the customer record
     *  @param customerRecordType customer record type
     *  @throws org.osid.NullArgumentException
     *          <code>customerRecord</code> or
     *          <code>customerRecordType</code> is <code>null</code>
     */
            
    protected void addCustomerRecord(org.osid.billing.records.CustomerRecord customerRecord, 
                                     org.osid.type.Type customerRecordType) {

        nullarg(customerRecord, "customer record");
        addRecordType(customerRecordType);
        this.records.add(customerRecord);
        
        return;
    }
}
