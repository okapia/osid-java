//
// AbstractGradingCalculationManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.calculation.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractGradingCalculationManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.grading.calculation.GradingCalculationManager,
               org.osid.grading.calculation.GradingCalculationProxyManager {

    private final Types gradebookColumnCalculationRecordTypes= new TypeRefSet();


    /**
     *  Constructs a new
     *  <code>AbstractGradingCalculationManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractGradingCalculationManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if a gradebook column calculation lookup service is supported. 
     *
     *  @return <code> true </code> if gradebook column calculation lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookColumnCalculationLookup() {
        return (false);
    }


    /**
     *  Tests if a gradebook column calculation administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if gradebook column calculation admin is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookColumnCalculationAdmin() {
        return (false);
    }


    /**
     *  Gets the supported <code> GradebookColumnCalculation </code> record 
     *  types. 
     *
     *  @return a list containing the supported <code> 
     *          GradebookColumnCalculation </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getGradebookColumnCalculationRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.gradebookColumnCalculationRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> GradebookColumnCalculation </code> record 
     *  type is supported. 
     *
     *  @param  gradebookColumnCalculationRecordType a <code> Type </code> 
     *          indicating a <code> GradebookColumnCalculation </code> type 
     *  @return <code> true </code> if the given gradebook column calculation 
     *          record <code> Type </code> is supported, <code> false </code> 
     *          otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          gradebookColumnCalculationRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsGradebookColumnCalculationRecordType(org.osid.type.Type gradebookColumnCalculationRecordType) {
        return (this.gradebookColumnCalculationRecordTypes.contains(gradebookColumnCalculationRecordType));
    }


    /**
     *  Adds support for a gradebook column calculation record type.
     *
     *  @param gradebookColumnCalculationRecordType a gradebook column calculation record type
     *  @throws org.osid.NullArgumentException
     *  <code>gradebookColumnCalculationRecordType</code> is <code>null</code>
     */

    protected void addGradebookColumnCalculationRecordType(org.osid.type.Type gradebookColumnCalculationRecordType) {
        this.gradebookColumnCalculationRecordTypes.add(gradebookColumnCalculationRecordType);
        return;
    }


    /**
     *  Removes support for a gradebook column calculation record type.
     *
     *  @param gradebookColumnCalculationRecordType a gradebook column calculation record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>gradebookColumnCalculationRecordType</code> is <code>null</code>
     */

    protected void removeGradebookColumnCalculationRecordType(org.osid.type.Type gradebookColumnCalculationRecordType) {
        this.gradebookColumnCalculationRecordTypes.remove(gradebookColumnCalculationRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the gradebook 
     *  column calculation lookup service. 
     *
     *  @return a <code> GradebookColumnCalculationLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.calculation.GradebookColumnCalculationLookupSession getGradebookColumnCalculationLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.calculation.GradingCalculationManager.getGradebookColumnCalculationLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the gradebook 
     *  column calculation lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GradebookColumnCalculationLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.calculation.GradebookColumnCalculationLookupSession getGradebookColumnCalculationLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.calculation.GradingCalculationProxyManager.getGradebookColumnCalculationLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the gradebook 
     *  column calculation lookup service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @return <code> a GradebookColumnCalculationLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnCalculationLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.calculation.GradebookColumnCalculationLookupSession getGradebookColumnCalculationLookupSessionForGradebook(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.calculation.GradingCalculationManager.getGradebookColumnCalculationLookupSessionForGradebook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the gradebook 
     *  column calculation lookup service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @param  proxy a proxy 
     *  @return <code> a GradebookColumnCalculationLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnCalculationLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.calculation.GradebookColumnCalculationLookupSession getGradebookColumnCalculationLookupSessionForGradebook(org.osid.id.Id gradebookId, 
                                                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.calculation.GradingCalculationProxyManager.getGradebookColumnCalculationLookupSessionForGradebook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the gradebook 
     *  column calculation administrative service. 
     *
     *  @return a <code> GradebookColumnCalculationAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.calculation.GradebookColumnCalculationAdminSession getGradebookColumnCalculationAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.calculation.GradingCalculationManager.getGradebookColumnCalculationAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the gradebook 
     *  column calculation administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GradebookColumnCalculationAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.calculation.GradebookColumnCalculationAdminSession getGradebookColumnCalculationAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.calculation.GradingCalculationProxyManager.getGradebookColumnCalculationAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the gradebook 
     *  column calculation administrative service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @return <code> a GradebookColumnCalculationAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnCalculationAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.calculation.GradebookColumnCalculationAdminSession getGradebookColumnCalculationAdminSessionForGradebook(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.calculation.GradingCalculationManager.getGradebookColumnCalculationAdminSessionForGradebook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the gradebook 
     *  column calculation administrative service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @param  proxy a proxy 
     *  @return <code> a GradebookColumnCalculationAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnCalculationAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.calculation.GradebookColumnCalculationAdminSession getGradebookColumnCalculationAdminSessionForGradebook(org.osid.id.Id gradebookId, 
                                                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.calculation.GradingCalculationProxyManager.getGradebookColumnCalculationAdminSessionForGradebook not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.gradebookColumnCalculationRecordTypes.clear();
        this.gradebookColumnCalculationRecordTypes.clear();

        return;
    }
}
