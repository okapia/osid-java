//
// AbstractPaymentQueryInspector.java
//
//     A template for making a PaymentQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.payment.payment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for payments.
 */

public abstract class AbstractPaymentQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.billing.payment.PaymentQueryInspector {

    private final java.util.Collection<org.osid.billing.payment.records.PaymentQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the payer <code> Id </code> query terms. 
     *
     *  @return the payer <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPayerIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the payer query terms. 
     *
     *  @return the payer query terms 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerQueryInspector[] getPayerTerms() {
        return (new org.osid.billing.payment.PayerQueryInspector[0]);
    }


    /**
     *  Gets the customer <code> Id </code> query terms. 
     *
     *  @return the customer <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCustomerIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the customer query terms. 
     *
     *  @return the customer query terms 
     */

    @OSID @Override
    public org.osid.billing.CustomerQueryInspector[] getCustomerTerms() {
        return (new org.osid.billing.CustomerQueryInspector[0]);
    }


    /**
     *  Gets the period <code> Id </code> query terms. 
     *
     *  @return the period <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPeriodIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the period query terms. 
     *
     *  @return the period query terms 
     */

    @OSID @Override
    public org.osid.billing.PeriodQueryInspector[] getPeriodTerms() {
        return (new org.osid.billing.PeriodQueryInspector[0]);
    }


    /**
     *  Gets the payment date query terms. 
     *
     *  @return the payment date query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getPaymentDateTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the process date query terms. 
     *
     *  @return the process date query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getProcessDateTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the amount query terms. 
     *
     *  @return the amount query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getAmountTerms() {
        return (new org.osid.search.terms.CurrencyRangeTerm[0]);
    }


    /**
     *  Gets the business <code> Id </code> query terms. 
     *
     *  @return the business <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBusinessIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the business query terms. 
     *
     *  @return the business query terms 
     */

    @OSID @Override
    public org.osid.billing.BusinessQueryInspector[] getBusinessTerms() {
        return (new org.osid.billing.BusinessQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given payment query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a payment implementing the requested record.
     *
     *  @param paymentRecordType a payment record type
     *  @return the payment query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>paymentRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(paymentRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.payment.records.PaymentQueryInspectorRecord getPaymentQueryInspectorRecord(org.osid.type.Type paymentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.payment.records.PaymentQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(paymentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(paymentRecordType + " is not supported");
    }


    /**
     *  Adds a record to this payment query. 
     *
     *  @param paymentQueryInspectorRecord payment query inspector
     *         record
     *  @param paymentRecordType payment record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPaymentQueryInspectorRecord(org.osid.billing.payment.records.PaymentQueryInspectorRecord paymentQueryInspectorRecord, 
                                                   org.osid.type.Type paymentRecordType) {

        addRecordType(paymentRecordType);
        nullarg(paymentRecordType, "payment record type");
        this.records.add(paymentQueryInspectorRecord);        
        return;
    }
}
