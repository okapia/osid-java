//
// AbstractRulesSession.java
//
//    Simple implementation framework for providing a Rules
//    service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.rules.evaluation.spi;

import net.okapia.osid.torrefacto.collect.IdHashMap;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session provides methods to evaluate and execute rules.
 */

public abstract class AbstractRulesSession
    extends net.okapia.osid.jamocha.rules.spi.AbstractRulesSession
    implements org.osid.rules.RulesSession {

    private java.util.Map<org.osid.id.Id, net.okapia.osid.jamocha.assembly.rules.evaluation.EvaluatingRule> rules = new IdHashMap<>();


    /**
     *  Adds a rule.
     *
     *  @param rule
     *  @throws org.osid.NullArgumentException <code>rule</code> is
     *          <code>null</code>
     */

    protected void addRule(net.okapia.osid.jamocha.assembly.rules.evaluation.EvaluatingRule rule) {
        nullarg(rule, "rule");
        this.rules.put(rule.getId(), rule);
        return;
    }


    /**
     *  Removes a rule.
     *
     *  @param rule
     *  @throws org.osid.NullArgumentException <code>rule</code> is
     *          <code>null</code>
     */

    protected void removeRule(net.okapia.osid.jamocha.assembly.rules.evaluation.EvaluatingRule rule) {
        nullarg(rule, "rule");
        this.rules.remove(rule.getId());
        return;
    }


    /**
     *  Evaluates a rule based on an input condition. 
     *
     *  @param  ruleId a rule <code> Id </code> 
     *  @param  condition input conditions 
     *  @return result of the evaluation 
     *  @throws org.osid.NotFoundException an <code> Id was </code> not found 
     *  @throws org.osid.NullArgumentException <code> ruleId </code> or <code> 
     *          condition </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException <code> condition </code> not of 
     *          this service 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    public boolean evaluateRule(org.osid.id.Id ruleId, org.osid.rules.Condition condition)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.assembly.rules.evaluation.EvaluatingRule rule = this.rules.get(ruleId);
        if (rule == null) {
            throw new org.osid.NotFoundException(ruleId + " not found");
        }

        return (rule.eval(condition));  
    }
}
