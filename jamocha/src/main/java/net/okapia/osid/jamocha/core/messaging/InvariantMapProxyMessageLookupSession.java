//
// InvariantMapProxyMessageLookupSession
//
//    Implements a Message lookup service backed by a fixed
//    collection of messages. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.messaging;


/**
 *  Implements a Message lookup service backed by a fixed
 *  collection of messages. The messages are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyMessageLookupSession
    extends net.okapia.osid.jamocha.core.messaging.spi.AbstractMapMessageLookupSession
    implements org.osid.messaging.MessageLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyMessageLookupSession} with no
     *  messages.
     *
     *  @param mailbox the mailbox
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code mailbox} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyMessageLookupSession(org.osid.messaging.Mailbox mailbox,
                                                  org.osid.proxy.Proxy proxy) {
        setMailbox(mailbox);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyMessageLookupSession} with a single
     *  message.
     *
     *  @param mailbox the mailbox
     *  @param message a single message
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code mailbox},
     *          {@code message} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyMessageLookupSession(org.osid.messaging.Mailbox mailbox,
                                                  org.osid.messaging.Message message, org.osid.proxy.Proxy proxy) {

        this(mailbox, proxy);
        putMessage(message);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyMessageLookupSession} using
     *  an array of messages.
     *
     *  @param mailbox the mailbox
     *  @param messages an array of messages
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code mailbox},
     *          {@code messages} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyMessageLookupSession(org.osid.messaging.Mailbox mailbox,
                                                  org.osid.messaging.Message[] messages, org.osid.proxy.Proxy proxy) {

        this(mailbox, proxy);
        putMessages(messages);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyMessageLookupSession} using a
     *  collection of messages.
     *
     *  @param mailbox the mailbox
     *  @param messages a collection of messages
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code mailbox},
     *          {@code messages} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyMessageLookupSession(org.osid.messaging.Mailbox mailbox,
                                                  java.util.Collection<? extends org.osid.messaging.Message> messages,
                                                  org.osid.proxy.Proxy proxy) {

        this(mailbox, proxy);
        putMessages(messages);
        return;
    }
}
