//
// AbstractAdapterReplyLookupSession.java
//
//    A Reply lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.forum.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Reply lookup session adapter.
 */

public abstract class AbstractAdapterReplyLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.forum.ReplyLookupSession {

    private final org.osid.forum.ReplyLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterReplyLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterReplyLookupSession(org.osid.forum.ReplyLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Forum/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Forum Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getForumId() {
        return (this.session.getForumId());
    }


    /**
     *  Gets the {@code Forum} associated with this session.
     *
     *  @return the {@code Forum} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.Forum getForum()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getForum());
    }


    /**
     *  Tests if this user can perform {@code Reply} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupReplies() {
        return (this.session.canLookupReplies());
    }


    /**
     *  A complete view of the {@code Reply} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeReplyView() {
        this.session.useComparativeReplyView();
        return;
    }


    /**
     *  A complete view of the {@code Reply} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryReplyView() {
        this.session.usePlenaryReplyView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include replies in forums which are children
     *  of this forum in the forum hierarchy.
     */

    @OSID @Override
    public void useFederatedForumView() {
        this.session.useFederatedForumView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this forum only.
     */

    @OSID @Override
    public void useIsolatedForumView() {
        this.session.useIsolatedForumView();
        return;
    }
    

    /**
     *  The returns from the lookup methods omit sequestered
     *  replies.
     */

    @OSID @Override
    public void useSequesteredReplyView() {
        this.session.useSequesteredReplyView();
        return;
    }


    /**
     *  All replies are returned including sequestered replies.
     */

    @OSID @Override
    public void useUnsequesteredReplyView() {
        this.session.useUnsequesteredReplyView();
        return;
    }

     
    /**
     *  Gets the {@code Reply} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Reply} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Reply} and
     *  retained for compatibility.
     *
     *  @param replyId {@code Id} of the {@code Reply}
     *  @return the reply
     *  @throws org.osid.NotFoundException {@code replyId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code replyId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.Reply getReply(org.osid.id.Id replyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getReply(replyId));
    }


    /**
     *  Gets a {@code ReplyList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  replies specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Replies} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  replyIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Reply} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code replyIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.ReplyList getRepliesByIds(org.osid.id.IdList replyIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRepliesByIds(replyIds));
    }


    /**
     *  Gets a {@code ReplyList} corresponding to the given
     *  reply genus {@code Type} which does not include
     *  replies of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  replies or an error results. Otherwise, the returned list
     *  may contain only those replies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  replyGenusType a reply genus type 
     *  @return the returned {@code Reply} list
     *  @throws org.osid.NullArgumentException
     *          {@code replyGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.ReplyList getRepliesByGenusType(org.osid.type.Type replyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRepliesByGenusType(replyGenusType));
    }


    /**
     *  Gets a {@code ReplyList} corresponding to the given
     *  reply genus {@code Type} and include any additional
     *  replies with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  replies or an error results. Otherwise, the returned list
     *  may contain only those replies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  replyGenusType a reply genus type 
     *  @return the returned {@code Reply} list
     *  @throws org.osid.NullArgumentException
     *          {@code replyGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.ReplyList getRepliesByParentGenusType(org.osid.type.Type replyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRepliesByParentGenusType(replyGenusType));
    }


    /**
     *  Gets a {@code ReplyList} containing the given
     *  reply record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  replies or an error results. Otherwise, the returned list
     *  may contain only those replies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  replyRecordType a reply record type 
     *  @return the returned {@code Reply} list
     *  @throws org.osid.NullArgumentException
     *          {@code replyRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.ReplyList getRepliesByRecordType(org.osid.type.Type replyRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRepliesByRecordType(replyRecordType));
    }


    /**
     *  Gets a list of all replies corresponding to the given date range 
     *  inclusive. 
     *  
     *  In plenary mode, the returned list contains all known replies
     *  or an error results. Otherwise, the returned list may contain
     *  only those replies that are accessible through this session.
     *  
     *  In sequestered mode, no sequestered replies are returned. In 
     *  unsequestered mode, all replies are returned. 
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code ReplyList} 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code from} or {@code 
     *          to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.forum.ReplyList getRepliesByDate(org.osid.calendaring.DateTime from, 
                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRepliesByDate(from, to));
    }


    /**
     *  Gets a list of all replies corresponding to a post {@code
     *  Id.}
     *  
     *  In plenary mode, the returned list contains all known replies
     *  or an error results. Otherwise, the returned list may contain
     *  only those replies that are accessible through this session.
     *  
     *  In sequestered mode, no sequestered replies are returned. In
     *  unsequestered mode, all replies are returned.
     *
     *  @param  postId the {@code Id} of the post 
     *  @return the returned {@code ReplyList} 
     *  @throws org.osid.NullArgumentException {@code postId} is {@code 
     *          null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.forum.ReplyList getRepliesForPost(org.osid.id.Id postId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRepliesForPost(postId));
    }


    /**
     *  Gets a list of all replies corresponding to post {@code Id} in
     *  the given daterange inclusive.
     *  
     *  In plenary mode, the returned list contains all known replies
     *  or an error results. Otherwise, the returned list may contain
     *  only those replis that are accessible through this session.
     *  
     *  In sequestered mode, no sequestered replies are returned. In
     *  unsequestered mode, all replies are returned.
     *
     *  @param  postId the {@code Id} of the post 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code ReplyList} 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code postId, from} or 
     *          {@code to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.forum.ReplyList getRepliesByDateForPost(org.osid.id.Id postId, 
                                                            org.osid.calendaring.DateTime from, 
                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRepliesByDateForPost(postId, from, to));
    }


    /**
     *  Gets a list of all replies corresponding to a poster. 
     *  
     *  In plenary mode, the returned list contains all known replies or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  replies that are accessible through this session. 
     *  
     *  In sequestered mode, no sequestered replies are returned. In 
     *  unsequestered mode, all replies are returned. 
     *
     *  @param  resourceId the resource {@code Id} 
     *  @return the returned {@code ReplyList} 
     *  @throws org.osid.NullArgumentException {@code resourceId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.forum.ReplyList getRepliesForPoster(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRepliesForPoster(resourceId));
    }


    /**
     *  Gets a list of all replies corresponding to a post {@code Id} 
     *  for the given poster within the date range inclusive. 
     *  
     *  In plenary mode, the returned list contains all known replies or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  replies that are accessible through this session. 
     *  
     *  In sequestered mode, no sequestered replies are returned. In
     *  unsequestered mode, all replies are returned.
     *
     *  @param  resourceId the resource {@code Id} 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code ReplyList} 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code resourceId, from} 
     *          or {@code to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.forum.ReplyList getRepliesByDateForPoster(org.osid.id.Id resourceId, 
                                                              org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRepliesByDateForPoster(resourceId, from, to));
    }


    /**
     *  Gets a list of all replies corresponding to a post {@code Id}
     *  and poster.
     *  
     *  In plenary mode, the returned list contains all known replies
     *  or an error results. Otherwise, the returned list may contain
     *  only those replies that are accessible through this session.
     *  
     *  In sequestered mode, no sequestered replies are returned. In
     *  unsequestered mode, all replies are returned.
     *
     *  @param  postId the {@code Id} of the post 
     *  @param  resourceId the resource {@code Id} 
     *  @return the returned {@code ReplyList} 
     *  @throws org.osid.NullArgumentException {@code postId} or {@code 
     *          resourceId} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.forum.ReplyList getRepliesForPostAndPoster(org.osid.id.Id postId, 
                                                               org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getRepliesForPostAndPoster(postId, resourceId));
    }


    /**
     *  Gets a list of all replies corresponding to a post {@code Id}
     *  and poster within the given daterange incluisve.
     *  
     *  In plenary mode, the returned list contains all known replies
     *  or an error results. Otherwise, the returned list may contain
     *  only those replies that are accessible through this session.
     *  
     *  In sequestered mode, no sequestered replies are returned. In
     *  unsequestered mode, all replies are returned.
     *
     *  @param  postId the {@code Id} of the post 
     *  @param  resourceId the resource {@code Id} 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code ReplyList} 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code postId,
     *         resourceId, from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.forum.ReplyList getRepliesByDateForPostAndPoster(org.osid.id.Id postId, 
                                                                     org.osid.id.Id resourceId, 
                                                                     org.osid.calendaring.DateTime from, 
                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRepliesByDateForPostAndPoster(postId, resourceId, from, to));
    }

    
    /**
     *  Gets all {@code Replies}. 
     *
     *  In plenary mode, the returned list contains all known
     *  replies or an error results. Otherwise, the returned list
     *  may contain only those replies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Replies} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.ReplyList getReplies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getReplies());
    }
}
