//
// InvariantMapAuditLookupSession
//
//    Implements an Audit lookup service backed by a fixed collection of
//    audits.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inquiry;


/**
 *  Implements an Audit lookup service backed by a fixed
 *  collection of audits. The audits are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapAuditLookupSession
    extends net.okapia.osid.jamocha.core.inquiry.spi.AbstractMapAuditLookupSession
    implements org.osid.inquiry.AuditLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapAuditLookupSession</code> with no
     *  audits.
     *  
     *  @param inquest the inquest
     *  @throws org.osid.NullArgumnetException {@code inquest} is
     *          {@code null}
     */

    public InvariantMapAuditLookupSession(org.osid.inquiry.Inquest inquest) {
        setInquest(inquest);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAuditLookupSession</code> with a single
     *  audit.
     *  
     *  @param inquest the inquest
     *  @param audit an single audit
     *  @throws org.osid.NullArgumentException {@code inquest} or
     *          {@code audit} is <code>null</code>
     */

      public InvariantMapAuditLookupSession(org.osid.inquiry.Inquest inquest,
                                               org.osid.inquiry.Audit audit) {
        this(inquest);
        putAudit(audit);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAuditLookupSession</code> using an array
     *  of audits.
     *  
     *  @param inquest the inquest
     *  @param audits an array of audits
     *  @throws org.osid.NullArgumentException {@code inquest} or
     *          {@code audits} is <code>null</code>
     */

      public InvariantMapAuditLookupSession(org.osid.inquiry.Inquest inquest,
                                               org.osid.inquiry.Audit[] audits) {
        this(inquest);
        putAudits(audits);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAuditLookupSession</code> using a
     *  collection of audits.
     *
     *  @param inquest the inquest
     *  @param audits a collection of audits
     *  @throws org.osid.NullArgumentException {@code inquest} or
     *          {@code audits} is <code>null</code>
     */

      public InvariantMapAuditLookupSession(org.osid.inquiry.Inquest inquest,
                                               java.util.Collection<? extends org.osid.inquiry.Audit> audits) {
        this(inquest);
        putAudits(audits);
        return;
    }
}
