//
// AbstractFederatingAuctionProcessorLookupSession.java
//
//     An abstract federating adapter for an AuctionProcessorLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.bidding.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  AuctionProcessorLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingAuctionProcessorLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.bidding.rules.AuctionProcessorLookupSession>
    implements org.osid.bidding.rules.AuctionProcessorLookupSession {

    private boolean parallel = false;
    private org.osid.bidding.AuctionHouse auctionHouse = new net.okapia.osid.jamocha.nil.bidding.auctionhouse.UnknownAuctionHouse();


    /**
     *  Constructs a new <code>AbstractFederatingAuctionProcessorLookupSession</code>.
     */

    protected AbstractFederatingAuctionProcessorLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.bidding.rules.AuctionProcessorLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>AuctionHouse/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>AuctionHouse Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAuctionHouseId() {
        return (this.auctionHouse.getId());
    }


    /**
     *  Gets the <code>AuctionHouse</code> associated with this 
     *  session.
     *
     *  @return the <code>AuctionHouse</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouse getAuctionHouse()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.auctionHouse);
    }


    /**
     *  Sets the <code>AuctionHouse</code>.
     *
     *  @param  auctionHouse the auction house for this session
     *  @throws org.osid.NullArgumentException <code>auctionHouse</code>
     *          is <code>null</code>
     */

    protected void setAuctionHouse(org.osid.bidding.AuctionHouse auctionHouse) {
        nullarg(auctionHouse, "auction house");
        this.auctionHouse = auctionHouse;
        return;
    }


    /**
     *  Tests if this user can perform <code>AuctionProcessor</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAuctionProcessors() {
        for (org.osid.bidding.rules.AuctionProcessorLookupSession session : getSessions()) {
            if (session.canLookupAuctionProcessors()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>AuctionProcessor</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAuctionProcessorView() {
        for (org.osid.bidding.rules.AuctionProcessorLookupSession session : getSessions()) {
            session.useComparativeAuctionProcessorView();
        }

        return;
    }


    /**
     *  A complete view of the <code>AuctionProcessor</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAuctionProcessorView() {
        for (org.osid.bidding.rules.AuctionProcessorLookupSession session : getSessions()) {
            session.usePlenaryAuctionProcessorView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include auction processors in auction houses which are children
     *  of this auction house in the auction house hierarchy.
     */

    @OSID @Override
    public void useFederatedAuctionHouseView() {
        for (org.osid.bidding.rules.AuctionProcessorLookupSession session : getSessions()) {
            session.useFederatedAuctionHouseView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this auction house only.
     */

    @OSID @Override
    public void useIsolatedAuctionHouseView() {
        for (org.osid.bidding.rules.AuctionProcessorLookupSession session : getSessions()) {
            session.useIsolatedAuctionHouseView();
        }

        return;
    }


    /**
     *  Only active auction processors are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveAuctionProcessorView() {
        for (org.osid.bidding.rules.AuctionProcessorLookupSession session : getSessions()) {
            session.useActiveAuctionProcessorView();
        }

        return;
    }


    /**
     *  Active and inactive auction processors are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusAuctionProcessorView() {
        for (org.osid.bidding.rules.AuctionProcessorLookupSession session : getSessions()) {
            session.useAnyStatusAuctionProcessorView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>AuctionProcessor</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>AuctionProcessor</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>AuctionProcessor</code> and
     *  retained for compatibility.
     *
     *  In active mode, auction processors are returned that are currently
     *  active. In any status mode, active and inactive auction processors
     *  are returned.
     *
     *  @param  auctionProcessorId <code>Id</code> of the
     *          <code>AuctionProcessor</code>
     *  @return the auction processor
     *  @throws org.osid.NotFoundException <code>auctionProcessorId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>auctionProcessorId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessor getAuctionProcessor(org.osid.id.Id auctionProcessorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.bidding.rules.AuctionProcessorLookupSession session : getSessions()) {
            try {
                return (session.getAuctionProcessor(auctionProcessorId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(auctionProcessorId + " not found");
    }


    /**
     *  Gets an <code>AuctionProcessorList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  auctionProcessors specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>AuctionProcessors</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, auction processors are returned that are currently
     *  active. In any status mode, active and inactive auction processors
     *  are returned.
     *
     *  @param  auctionProcessorIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>AuctionProcessor</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>auctionProcessorIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorList getAuctionProcessorsByIds(org.osid.id.IdList auctionProcessorIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.bidding.rules.auctionprocessor.MutableAuctionProcessorList ret = new net.okapia.osid.jamocha.bidding.rules.auctionprocessor.MutableAuctionProcessorList();

        try (org.osid.id.IdList ids = auctionProcessorIds) {
            while (ids.hasNext()) {
                ret.addAuctionProcessor(getAuctionProcessor(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>AuctionProcessorList</code> corresponding to the given
     *  auction processor genus <code>Type</code> which does not include
     *  auction processors of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  auction processors or an error results. Otherwise, the returned list
     *  may contain only those auction processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, auction processors are returned that are currently
     *  active. In any status mode, active and inactive auction processors
     *  are returned.
     *
     *  @param  auctionProcessorGenusType an auctionProcessor genus type 
     *  @return the returned <code>AuctionProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionProcessorGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorList getAuctionProcessorsByGenusType(org.osid.type.Type auctionProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.rules.auctionprocessor.FederatingAuctionProcessorList ret = getAuctionProcessorList();

        for (org.osid.bidding.rules.AuctionProcessorLookupSession session : getSessions()) {
            ret.addAuctionProcessorList(session.getAuctionProcessorsByGenusType(auctionProcessorGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AuctionProcessorList</code> corresponding to the given
     *  auction processor genus <code>Type</code> and include any additional
     *  auction processors with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  auction processors or an error results. Otherwise, the returned list
     *  may contain only those auction processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, auction processors are returned that are currently
     *  active. In any status mode, active and inactive auction processors
     *  are returned.
     *
     *  @param  auctionProcessorGenusType an auctionProcessor genus type 
     *  @return the returned <code>AuctionProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionProcessorGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorList getAuctionProcessorsByParentGenusType(org.osid.type.Type auctionProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.rules.auctionprocessor.FederatingAuctionProcessorList ret = getAuctionProcessorList();

        for (org.osid.bidding.rules.AuctionProcessorLookupSession session : getSessions()) {
            ret.addAuctionProcessorList(session.getAuctionProcessorsByParentGenusType(auctionProcessorGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AuctionProcessorList</code> containing the given
     *  auction processor record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  auction processors or an error results. Otherwise, the returned list
     *  may contain only those auction processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, auction processors are returned that are currently
     *  active. In any status mode, active and inactive auction processors
     *  are returned.
     *
     *  @param  auctionProcessorRecordType an auctionProcessor record type 
     *  @return the returned <code>AuctionProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionProcessorRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorList getAuctionProcessorsByRecordType(org.osid.type.Type auctionProcessorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.rules.auctionprocessor.FederatingAuctionProcessorList ret = getAuctionProcessorList();

        for (org.osid.bidding.rules.AuctionProcessorLookupSession session : getSessions()) {
            ret.addAuctionProcessorList(session.getAuctionProcessorsByRecordType(auctionProcessorRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>AuctionProcessors</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  auction processors or an error results. Otherwise, the returned list
     *  may contain only those auction processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, auction processors are returned that are currently
     *  active. In any status mode, active and inactive auction processors
     *  are returned.
     *
     *  @return a list of <code>AuctionProcessors</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorList getAuctionProcessors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.rules.auctionprocessor.FederatingAuctionProcessorList ret = getAuctionProcessorList();

        for (org.osid.bidding.rules.AuctionProcessorLookupSession session : getSessions()) {
            ret.addAuctionProcessorList(session.getAuctionProcessors());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.bidding.rules.auctionprocessor.FederatingAuctionProcessorList getAuctionProcessorList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.bidding.rules.auctionprocessor.ParallelAuctionProcessorList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.bidding.rules.auctionprocessor.CompositeAuctionProcessorList());
        }
    }
}
