//
// AbstractMapRequestLookupSession
//
//    A simple framework for providing a Request lookup service
//    backed by a fixed collection of requests.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Request lookup service backed by a
 *  fixed collection of requests. The requests are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Requests</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapRequestLookupSession
    extends net.okapia.osid.jamocha.provisioning.spi.AbstractRequestLookupSession
    implements org.osid.provisioning.RequestLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.provisioning.Request> requests = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.provisioning.Request>());


    /**
     *  Makes a <code>Request</code> available in this session.
     *
     *  @param  request a request
     *  @throws org.osid.NullArgumentException <code>request<code>
     *          is <code>null</code>
     */

    protected void putRequest(org.osid.provisioning.Request request) {
        this.requests.put(request.getId(), request);
        return;
    }


    /**
     *  Makes an array of requests available in this session.
     *
     *  @param  requests an array of requests
     *  @throws org.osid.NullArgumentException <code>requests<code>
     *          is <code>null</code>
     */

    protected void putRequests(org.osid.provisioning.Request[] requests) {
        putRequests(java.util.Arrays.asList(requests));
        return;
    }


    /**
     *  Makes a collection of requests available in this session.
     *
     *  @param  requests a collection of requests
     *  @throws org.osid.NullArgumentException <code>requests<code>
     *          is <code>null</code>
     */

    protected void putRequests(java.util.Collection<? extends org.osid.provisioning.Request> requests) {
        for (org.osid.provisioning.Request request : requests) {
            this.requests.put(request.getId(), request);
        }

        return;
    }


    /**
     *  Removes a Request from this session.
     *
     *  @param  requestId the <code>Id</code> of the request
     *  @throws org.osid.NullArgumentException <code>requestId<code> is
     *          <code>null</code>
     */

    protected void removeRequest(org.osid.id.Id requestId) {
        this.requests.remove(requestId);
        return;
    }


    /**
     *  Gets the <code>Request</code> specified by its <code>Id</code>.
     *
     *  @param  requestId <code>Id</code> of the <code>Request</code>
     *  @return the request
     *  @throws org.osid.NotFoundException <code>requestId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>requestId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Request getRequest(org.osid.id.Id requestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.provisioning.Request request = this.requests.get(requestId);
        if (request == null) {
            throw new org.osid.NotFoundException("request not found: " + requestId);
        }

        return (request);
    }


    /**
     *  Gets all <code>Requests</code>. In plenary mode, the returned
     *  list contains all known requests or an error
     *  results. Otherwise, the returned list may contain only those
     *  requests that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Requests</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.RequestList getRequests()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.request.ArrayRequestList(this.requests.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.requests.clear();
        super.close();
        return;
    }
}
