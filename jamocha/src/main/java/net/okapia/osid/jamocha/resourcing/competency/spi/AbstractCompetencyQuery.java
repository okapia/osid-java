//
// AbstractCompetencyQuery.java
//
//     A template for making a Competency Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.competency.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for competencies.
 */

public abstract class AbstractCompetencyQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.resourcing.CompetencyQuery {

    private final java.util.Collection<org.osid.resourcing.records.CompetencyQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the learning objective <code> Id </code> for this query. 
     *
     *  @param  objectiveId the learning objective <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> objectiveId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLearningObjectiveId(org.osid.id.Id objectiveId, 
                                         boolean match) {
        return;
    }


    /**
     *  Clears the learning objective <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearLearningObjectiveIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> ObjectiveQuery </code> is available. 
     *
     *  @return <code> true </code> if a learning objective query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLearningObjectiveQuery() {
        return (false);
    }


    /**
     *  Gets the query for a learning objective. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the learning objective query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLearningObjectiveQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQuery getLearningObjectiveQuery() {
        throw new org.osid.UnimplementedException("supportsLearningObjectiveQuery() is false");
    }


    /**
     *  Matches competencies that have any learning objective. 
     *
     *  @param  match <code> true </code> to match competencies with any 
     *          learning objective, <code> false </code> to match competencies 
     *          with no learning objective 
     */

    @OSID @Override
    public void matchAnyLearningObjective(boolean match) {
        return;
    }


    /**
     *  Clears the learning objective query terms. 
     */

    @OSID @Override
    public void clearLearningObjectiveTerms() {
        return;
    }


    /**
     *  Sets the availability <code> Id </code> for this query. 
     *
     *  @param  availabilityId the availability <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> availabilityId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchAvailabilityId(org.osid.id.Id availabilityId, 
                                    boolean match) {
        return;
    }


    /**
     *  Clears the availability <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAvailabilityIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AvailabilityQuery </code> is available. 
     *
     *  @return <code> true </code> if an availability query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilityQuery() {
        return (false);
    }


    /**
     *  Gets the query for an availability. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the availability query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityQuery getAvailabilityQuery() {
        throw new org.osid.UnimplementedException("supportsAvailabilityQuery() is false");
    }


    /**
     *  Matches competencies that are used in any availability. 
     *
     *  @param  match <code> true </code> to match competencies with any 
     *          availability, <code> false </code> to match competencies with 
     *          no availability 
     */

    @OSID @Override
    public void matchAnyAvailability(boolean match) {
        return;
    }


    /**
     *  Clears the availability query terms. 
     */

    @OSID @Override
    public void clearAvailabilityTerms() {
        return;
    }


    /**
     *  Sets the work <code> Id </code> for this query. 
     *
     *  @param  workId the work <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> workId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchWorkId(org.osid.id.Id workId, boolean match) {
        return;
    }


    /**
     *  Clears the work <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearWorkIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> WorkQuery </code> is available. 
     *
     *  @return <code> true </code> if a work query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkQuery() {
        return (false);
    }


    /**
     *  Gets the query for a work. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the work query 
     *  @throws org.osid.UnimplementedException <code> supportsWorkQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkQuery getWorkQuery() {
        throw new org.osid.UnimplementedException("supportsWorkQuery() is false");
    }


    /**
     *  Matches competencies that are used in any work. 
     *
     *  @param  match <code> true </code> to match competencies with any work, 
     *          <code> false </code> to match competencies with no work 
     */

    @OSID @Override
    public void matchAnyWork(boolean match) {
        return;
    }


    /**
     *  Clears the work query terms. 
     */

    @OSID @Override
    public void clearWorkTerms() {
        return;
    }


    /**
     *  Sets the job <code> Id </code> for this query. 
     *
     *  @param  jobId the job <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> jobId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchJobId(org.osid.id.Id jobId, boolean match) {
        return;
    }


    /**
     *  Clears the job <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearJobIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> JobQuery </code> is available. 
     *
     *  @return <code> true </code> if a job query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobQuery() {
        return (false);
    }


    /**
     *  Gets the query for a job. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the job query 
     *  @throws org.osid.UnimplementedException <code> supportsJobQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobQuery getJobQuery() {
        throw new org.osid.UnimplementedException("supportsJobQuery() is false");
    }


    /**
     *  Matches competencies that are used in any job. 
     *
     *  @param  match <code> true </code> to match competencies with any job, 
     *          <code> false </code> to match competencies with no job 
     */

    @OSID @Override
    public void matchAnyJob(boolean match) {
        return;
    }


    /**
     *  Clears the job query terms. 
     */

    @OSID @Override
    public void clearJobTerms() {
        return;
    }


    /**
     *  Sets the foundry <code> Id </code> for this query to match 
     *  competencies assigned to foundries. 
     *
     *  @param  foundryId the foundry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchFoundryId(org.osid.id.Id foundryId, boolean match) {
        return;
    }


    /**
     *  Clears the foundry <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearFoundryIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> FoundryQuery </code> is available. 
     *
     *  @return <code> true </code> if a foundry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFoundryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a foundry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the foundry query 
     *  @throws org.osid.UnimplementedException <code> supportsFoundryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQuery getFoundryQuery() {
        throw new org.osid.UnimplementedException("supportsFoundryQuery() is false");
    }


    /**
     *  Clears the foundry query terms. 
     */

    @OSID @Override
    public void clearFoundryTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given competency query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a competency implementing the requested record.
     *
     *  @param competencyRecordType a competency record type
     *  @return the competency query record
     *  @throws org.osid.NullArgumentException
     *          <code>competencyRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(competencyRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.CompetencyQueryRecord getCompetencyQueryRecord(org.osid.type.Type competencyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.records.CompetencyQueryRecord record : this.records) {
            if (record.implementsRecordType(competencyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(competencyRecordType + " is not supported");
    }


    /**
     *  Adds a record to this competency query. 
     *
     *  @param competencyQueryRecord competency query record
     *  @param competencyRecordType competency record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCompetencyQueryRecord(org.osid.resourcing.records.CompetencyQueryRecord competencyQueryRecord, 
                                          org.osid.type.Type competencyRecordType) {

        addRecordType(competencyRecordType);
        nullarg(competencyQueryRecord, "competency query record");
        this.records.add(competencyQueryRecord);        
        return;
    }
}
