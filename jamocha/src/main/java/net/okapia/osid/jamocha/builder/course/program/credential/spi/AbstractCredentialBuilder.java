//
// AbstractCredential.java
//
//     Defines a Credential builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.program.credential.spi;


/**
 *  Defines a <code>Credential</code> builder.
 */

public abstract class AbstractCredentialBuilder<T extends AbstractCredentialBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.course.program.credential.CredentialMiter credential;


    /**
     *  Constructs a new <code>AbstractCredentialBuilder</code>.
     *
     *  @param credential the credential to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractCredentialBuilder(net.okapia.osid.jamocha.builder.course.program.credential.CredentialMiter credential) {
        super(credential);
        this.credential = credential;
        return;
    }


    /**
     *  Builds the credential.
     *
     *  @return the new credential
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.course.program.Credential build() {
        (new net.okapia.osid.jamocha.builder.validator.course.program.credential.CredentialValidator(getValidations())).validate(this.credential);
        return (new net.okapia.osid.jamocha.builder.course.program.credential.ImmutableCredential(this.credential));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the credential miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.course.program.credential.CredentialMiter getMiter() {
        return (this.credential);
    }


    /**
     *  Sets the lifetime.
     *
     *  @param lifetime a lifetime
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>lifetime</code>
     *          is <code>null</code>
     */

    public T lifetime(org.osid.calendaring.Duration lifetime) {
        getMiter().setLifetime(lifetime);
        return (self());
    }


    /**
     *  Adds a Credential record.
     *
     *  @param record a credential record
     *  @param recordType the type of credential record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.course.program.records.CredentialRecord record, org.osid.type.Type recordType) {
        getMiter().addCredentialRecord(record, recordType);
        return (self());
    }
}       


