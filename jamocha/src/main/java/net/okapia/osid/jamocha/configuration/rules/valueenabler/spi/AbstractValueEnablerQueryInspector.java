//
// AbstractValueEnablerQueryInspector.java
//
//     A template for making a ValueEnablerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.configuration.rules.valueenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for value enablers.
 */

public abstract class AbstractValueEnablerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQueryInspector
    implements org.osid.configuration.rules.ValueEnablerQueryInspector {

    private final java.util.Collection<org.osid.configuration.rules.records.ValueEnablerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the value <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledValueIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.configuration.ValueQueryInspector[] getRuledValueTerms() {
        return (new org.osid.configuration.ValueQueryInspector[0]);
    }


    /**
     *  Gets the configuration <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getConfigurationIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the configuration query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationQueryInspector[] getConfigurationTerms() {
        return (new org.osid.configuration.ConfigurationQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given value enabler query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a value enabler implementing the requested record.
     *
     *  @param valueEnablerRecordType a value enabler record type
     *  @return the value enabler query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>valueEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(valueEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.configuration.rules.records.ValueEnablerQueryInspectorRecord getValueEnablerQueryInspectorRecord(org.osid.type.Type valueEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.configuration.rules.records.ValueEnablerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(valueEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(valueEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this value enabler query. 
     *
     *  @param valueEnablerQueryInspectorRecord value enabler query inspector
     *         record
     *  @param valueEnablerRecordType valueEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addValueEnablerQueryInspectorRecord(org.osid.configuration.rules.records.ValueEnablerQueryInspectorRecord valueEnablerQueryInspectorRecord, 
                                                   org.osid.type.Type valueEnablerRecordType) {

        addRecordType(valueEnablerRecordType);
        nullarg(valueEnablerRecordType, "value enabler record type");
        this.records.add(valueEnablerQueryInspectorRecord);        
        return;
    }
}
