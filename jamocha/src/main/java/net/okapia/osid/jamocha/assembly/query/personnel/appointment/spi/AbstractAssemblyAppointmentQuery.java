//
// AbstractAssemblyAppointmentQuery.java
//
//     An AppointmentQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.personnel.appointment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An AppointmentQuery that stores terms.
 */

public abstract class AbstractAssemblyAppointmentQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.personnel.AppointmentQuery,
               org.osid.personnel.AppointmentQueryInspector,
               org.osid.personnel.AppointmentSearchOrder {

    private final java.util.Collection<org.osid.personnel.records.AppointmentQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.personnel.records.AppointmentQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.personnel.records.AppointmentSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyAppointmentQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyAppointmentQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets a person <code> Id. </code> 
     *
     *  @param  personId a person <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> personId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPersonId(org.osid.id.Id personId, boolean match) {
        getAssembler().addIdTerm(getPersonIdColumn(), personId, match);
        return;
    }


    /**
     *  Clears all person <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPersonIdTerms() {
        getAssembler().clearTerms(getPersonIdColumn());
        return;
    }


    /**
     *  Gets the person <code> Id </code> query terms. 
     *
     *  @return the person <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPersonIdTerms() {
        return (getAssembler().getIdTerms(getPersonIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the person. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPerson(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getPersonColumn(), style);
        return;
    }


    /**
     *  Gets the PersonId column name.
     *
     * @return the column name
     */

    protected String getPersonIdColumn() {
        return ("person_id");
    }


    /**
     *  Tests if a <code> PersonQuery </code> is available. 
     *
     *  @return <code> true </code> if a person query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPersonQuery() {
        return (false);
    }


    /**
     *  Gets the query for a person query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the person query 
     *  @throws org.osid.UnimplementedException <code> supportsPersonQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonQuery getPersonQuery() {
        throw new org.osid.UnimplementedException("supportsPersonQuery() is false");
    }


    /**
     *  Clears all person terms. 
     */

    @OSID @Override
    public void clearPersonTerms() {
        getAssembler().clearTerms(getPersonColumn());
        return;
    }


    /**
     *  Gets the person query terms. 
     *
     *  @return the person terms 
     */

    @OSID @Override
    public org.osid.personnel.PersonQueryInspector[] getPersonTerms() {
        return (new org.osid.personnel.PersonQueryInspector[0]);
    }


    /**
     *  Tests if a person search order is available. 
     *
     *  @return <code> true </code> if a person search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPersonSearchOrder() {
        return (false);
    }


    /**
     *  Gets the person search order. 
     *
     *  @return the person search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPersonSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonSearchOrder getPersonSearchOrder() {
        throw new org.osid.UnimplementedException("supportsPersonSearchOrder() is false");
    }


    /**
     *  Gets the Person column name.
     *
     * @return the column name
     */

    protected String getPersonColumn() {
        return ("person");
    }


    /**
     *  Sets a position <code> Id. </code> 
     *
     *  @param  positionId a position <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> positionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPositionId(org.osid.id.Id positionId, boolean match) {
        getAssembler().addIdTerm(getPositionIdColumn(), positionId, match);
        return;
    }


    /**
     *  Clears all position <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPositionIdTerms() {
        getAssembler().clearTerms(getPositionIdColumn());
        return;
    }


    /**
     *  Gets the position <code> Id </code> query terms. 
     *
     *  @return the position <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPositionIdTerms() {
        return (getAssembler().getIdTerms(getPositionIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the position. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPosition(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getPositionColumn(), style);
        return;
    }


    /**
     *  Gets the PositionId column name.
     *
     * @return the column name
     */

    protected String getPositionIdColumn() {
        return ("position_id");
    }


    /**
     *  Tests if a <code> PositionQuery </code> is available. 
     *
     *  @return <code> true </code> if a position query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPositionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a position query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the position query 
     *  @throws org.osid.UnimplementedException <code> supportsPositionQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionQuery getPositionQuery() {
        throw new org.osid.UnimplementedException("supportsPositionQuery() is false");
    }


    /**
     *  Clears all position terms. 
     */

    @OSID @Override
    public void clearPositionTerms() {
        getAssembler().clearTerms(getPositionColumn());
        return;
    }


    /**
     *  Gets the position query terms. 
     *
     *  @return the position terms 
     */

    @OSID @Override
    public org.osid.personnel.PositionQueryInspector[] getPositionTerms() {
        return (new org.osid.personnel.PositionQueryInspector[0]);
    }


    /**
     *  Tests if a position search order is available. 
     *
     *  @return <code> true </code> if a position search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPositionSearchOrder() {
        return (false);
    }


    /**
     *  Gets the position search order. 
     *
     *  @return the position search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPositionSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionSearchOrder getPositionSearchOrder() {
        throw new org.osid.UnimplementedException("supportsPositionSearchOrder() is false");
    }


    /**
     *  Gets the Position column name.
     *
     * @return the column name
     */

    protected String getPositionColumn() {
        return ("position");
    }


    /**
     *  Matches a commitment between the given range inclusive. 
     *
     *  @param  from a starting range 
     *  @param  to an ending range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     */

    @OSID @Override
    public void matchCommitment(long from, long to, boolean match) {
        getAssembler().addCardinalRangeTerm(getCommitmentColumn(), from, to, match);
        return;
    }


    /**
     *  Matches positions with any low salary. 
     *
     *  @param  match <code> true </code> to match appointments with any 
     *          commitment, <code> false </code> to match appointments with no 
     *          commitment 
     */

    @OSID @Override
    public void matchAnyCommitment(boolean match) {
        getAssembler().addCardinalRangeWildcardTerm(getCommitmentColumn(), match);
        return;
    }


    /**
     *  Clears all commitment terms. 
     */

    @OSID @Override
    public void clearCommitmentTerms() {
        getAssembler().clearTerms(getCommitmentColumn());
        return;
    }


    /**
     *  Gets the commitment query terms. 
     *
     *  @return the commitment terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getCommitmentTerms() {
        return (getAssembler().getCardinalRangeTerms(getCommitmentColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the commitment. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCommitment(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCommitmentColumn(), style);
        return;
    }


    /**
     *  Gets the Commitment column name.
     *
     * @return the column name
     */

    protected String getCommitmentColumn() {
        return ("commitment");
    }


    /**
     *  Matches a title. 
     *
     *  @param  title a title 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> title </code> is not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> title </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchTitle(String title, org.osid.type.Type stringMatchType, 
                           boolean match) {
        getAssembler().addStringTerm(getTitleColumn(), title, stringMatchType, match);
        return;
    }


    /**
     *  Matches persons with any title. 
     *
     *  @param  match <code> true </code> to match appointments with any 
     *          title, <code> false </code> to match appointments with no 
     *          title 
     */

    @OSID @Override
    public void matchAnyTitle(boolean match) {
        getAssembler().addStringWildcardTerm(getTitleColumn(), match);
        return;
    }


    /**
     *  Clears all title terms. 
     */

    @OSID @Override
    public void clearTitleTerms() {
        getAssembler().clearTerms(getTitleColumn());
        return;
    }


    /**
     *  Gets the title query terms. 
     *
     *  @return the title terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getTitleTerms() {
        return (getAssembler().getStringTerms(getTitleColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the title. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTitle(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTitleColumn(), style);
        return;
    }


    /**
     *  Gets the Title column name.
     *
     * @return the column name
     */

    protected String getTitleColumn() {
        return ("title");
    }


    /**
     *  Matches a salary between the given range inclusive. 
     *
     *  @param  from a starting salary range 
     *  @param  to an ending salary range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchSalary(org.osid.financials.Currency from, 
                            org.osid.financials.Currency to, boolean match) {
        getAssembler().addCurrencyRangeTerm(getSalaryColumn(), from, to, match);
        return;
    }


    /**
     *  Matches positions with any salary. 
     *
     *  @param  match <code> true </code> to match appointments with any 
     *          salary, <code> false </code> to match appointments with no 
     *          salary 
     */

    @OSID @Override
    public void matchAnySalary(boolean match) {
        getAssembler().addCurrencyRangeWildcardTerm(getSalaryColumn(), match);
        return;
    }


    /**
     *  Clears all salary terms. 
     */

    @OSID @Override
    public void clearSalaryTerms() {
        getAssembler().clearTerms(getSalaryColumn());
        return;
    }


    /**
     *  Gets the salary query terms. 
     *
     *  @return the salary terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getSalaryTerms() {
        return (getAssembler().getCurrencyRangeTerms(getSalaryColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the salary. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySalary(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSalaryColumn(), style);
        return;
    }


    /**
     *  Gets the Salary column name.
     *
     * @return the column name
     */

    protected String getSalaryColumn() {
        return ("salary");
    }


    /**
     *  Sets the realm <code> Id </code> for this query to match appointments 
     *  assigned to realms. 
     *
     *  @param  realmId a realm <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> realmId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRealmId(org.osid.id.Id realmId, boolean match) {
        getAssembler().addIdTerm(getRealmIdColumn(), realmId, match);
        return;
    }


    /**
     *  Clears all realm <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRealmIdTerms() {
        getAssembler().clearTerms(getRealmIdColumn());
        return;
    }


    /**
     *  Gets the realm <code> Id </code> query terms. 
     *
     *  @return the realm <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRealmIdTerms() {
        return (getAssembler().getIdTerms(getRealmIdColumn()));
    }


    /**
     *  Gets the RealmId column name.
     *
     * @return the column name
     */

    protected String getRealmIdColumn() {
        return ("realm_id");
    }


    /**
     *  Tests if a <code> RealmQuery </code> is available. 
     *
     *  @return <code> true </code> if a realm query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRealmQuery() {
        return (false);
    }


    /**
     *  Gets the query for a realm query. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the realm query 
     *  @throws org.osid.UnimplementedException <code> supportsRealmQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.RealmQuery getRealmQuery() {
        throw new org.osid.UnimplementedException("supportsRealmQuery() is false");
    }


    /**
     *  Clears all realm terms. 
     */

    @OSID @Override
    public void clearRealmTerms() {
        getAssembler().clearTerms(getRealmColumn());
        return;
    }


    /**
     *  Gets the realm query terms. 
     *
     *  @return the realm terms 
     */

    @OSID @Override
    public org.osid.personnel.RealmQueryInspector[] getRealmTerms() {
        return (new org.osid.personnel.RealmQueryInspector[0]);
    }


    /**
     *  Gets the Realm column name.
     *
     * @return the column name
     */

    protected String getRealmColumn() {
        return ("realm");
    }


    /**
     *  Tests if this appointment supports the given record
     *  <code>Type</code>.
     *
     *  @param  appointmentRecordType an appointment record type 
     *  @return <code>true</code> if the appointmentRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>appointmentRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type appointmentRecordType) {
        for (org.osid.personnel.records.AppointmentQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(appointmentRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  appointmentRecordType the appointment record type 
     *  @return the appointment query record 
     *  @throws org.osid.NullArgumentException
     *          <code>appointmentRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(appointmentRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.personnel.records.AppointmentQueryRecord getAppointmentQueryRecord(org.osid.type.Type appointmentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.personnel.records.AppointmentQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(appointmentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(appointmentRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  appointmentRecordType the appointment record type 
     *  @return the appointment query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>appointmentRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(appointmentRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.personnel.records.AppointmentQueryInspectorRecord getAppointmentQueryInspectorRecord(org.osid.type.Type appointmentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.personnel.records.AppointmentQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(appointmentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(appointmentRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param appointmentRecordType the appointment record type
     *  @return the appointment search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>appointmentRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(appointmentRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.personnel.records.AppointmentSearchOrderRecord getAppointmentSearchOrderRecord(org.osid.type.Type appointmentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.personnel.records.AppointmentSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(appointmentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(appointmentRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this appointment. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param appointmentQueryRecord the appointment query record
     *  @param appointmentQueryInspectorRecord the appointment query inspector
     *         record
     *  @param appointmentSearchOrderRecord the appointment search order record
     *  @param appointmentRecordType appointment record type
     *  @throws org.osid.NullArgumentException
     *          <code>appointmentQueryRecord</code>,
     *          <code>appointmentQueryInspectorRecord</code>,
     *          <code>appointmentSearchOrderRecord</code> or
     *          <code>appointmentRecordTypeappointment</code> is
     *          <code>null</code>
     */
            
    protected void addAppointmentRecords(org.osid.personnel.records.AppointmentQueryRecord appointmentQueryRecord, 
                                      org.osid.personnel.records.AppointmentQueryInspectorRecord appointmentQueryInspectorRecord, 
                                      org.osid.personnel.records.AppointmentSearchOrderRecord appointmentSearchOrderRecord, 
                                      org.osid.type.Type appointmentRecordType) {

        addRecordType(appointmentRecordType);

        nullarg(appointmentQueryRecord, "appointment query record");
        nullarg(appointmentQueryInspectorRecord, "appointment query inspector record");
        nullarg(appointmentSearchOrderRecord, "appointment search odrer record");

        this.queryRecords.add(appointmentQueryRecord);
        this.queryInspectorRecords.add(appointmentQueryInspectorRecord);
        this.searchOrderRecords.add(appointmentSearchOrderRecord);
        
        return;
    }
}
