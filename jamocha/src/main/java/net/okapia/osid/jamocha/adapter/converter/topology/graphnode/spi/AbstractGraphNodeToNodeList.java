//
// AbstractGraphNodeToNodeList.java
//
//     Implements an AbstractGraphNodeToNodeList adapter.
//
//
// Tom Coppeto
// Okapia
// 21 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.converter.topology.graphnode.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Implements an {@code AbstractGraphNodeList} adapter to convert
 *  {@code GraphNodes} into hierarchy {@code Nodes}.
 */

public abstract class AbstractGraphNodeToNodeList
    extends net.okapia.osid.jamocha.adapter.hierarchy.node.spi.AbstractAdapterNodeList
    implements org.osid.hierarchy.NodeList {

    private final org.osid.topology.GraphNodeList list;


    /**
     *  Constructs a new {@codeAbstractGraphNodeToNodeList</code> from
     *  a GraphNode.
     *
     *  @param list the list to convert
     *  @throws org.osid.NullArgumentException {@code list} is {@code
     *          null}
     */

    protected AbstractGraphNodeToNodeList(org.osid.topology.GraphNodeList list) {
        super(list);
        this.list = list;
        return;
    }


    /**
     *  Gets the next {@code GraphNode} in this list. 
     *
     *  @return the next {@code GraphNode} in this list. The
     *          {@code hasNext()} method should be used to test
     *          that a next {@code GraphNode} is available
     *          before calling this method.
     *  @throws org.osid.IllegalStateException no more elements
     *          available in this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getNextNode()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.adapter.converter.topology.graphnode.GraphNodeToNode(this.list.getNextGraphNode()));
    }
}
