//
// AbstractIndexedMapParameterProcessorLookupSession.java
//
//    A simple framework for providing a ParameterProcessor lookup service
//    backed by a fixed collection of parameter processors with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.configuration.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a ParameterProcessor lookup service backed by a
 *  fixed collection of parameter processors. The parameter processors are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some parameter processors may be compatible
 *  with more types than are indicated through these parameter processor
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>ParameterProcessors</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapParameterProcessorLookupSession
    extends AbstractMapParameterProcessorLookupSession
    implements org.osid.configuration.rules.ParameterProcessorLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.configuration.rules.ParameterProcessor> parameterProcessorsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.configuration.rules.ParameterProcessor>());
    private final MultiMap<org.osid.type.Type, org.osid.configuration.rules.ParameterProcessor> parameterProcessorsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.configuration.rules.ParameterProcessor>());


    /**
     *  Makes a <code>ParameterProcessor</code> available in this session.
     *
     *  @param  parameterProcessor a parameter processor
     *  @throws org.osid.NullArgumentException <code>parameterProcessor<code> is
     *          <code>null</code>
     */

    @Override
    protected void putParameterProcessor(org.osid.configuration.rules.ParameterProcessor parameterProcessor) {
        super.putParameterProcessor(parameterProcessor);

        this.parameterProcessorsByGenus.put(parameterProcessor.getGenusType(), parameterProcessor);
        
        try (org.osid.type.TypeList types = parameterProcessor.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.parameterProcessorsByRecord.put(types.getNextType(), parameterProcessor);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a parameter processor from this session.
     *
     *  @param parameterProcessorId the <code>Id</code> of the parameter processor
     *  @throws org.osid.NullArgumentException <code>parameterProcessorId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeParameterProcessor(org.osid.id.Id parameterProcessorId) {
        org.osid.configuration.rules.ParameterProcessor parameterProcessor;
        try {
            parameterProcessor = getParameterProcessor(parameterProcessorId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.parameterProcessorsByGenus.remove(parameterProcessor.getGenusType());

        try (org.osid.type.TypeList types = parameterProcessor.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.parameterProcessorsByRecord.remove(types.getNextType(), parameterProcessor);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeParameterProcessor(parameterProcessorId);
        return;
    }


    /**
     *  Gets a <code>ParameterProcessorList</code> corresponding to the given
     *  parameter processor genus <code>Type</code> which does not include
     *  parameter processors of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known parameter processors or an error results. Otherwise,
     *  the returned list may contain only those parameter processors that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  parameterProcessorGenusType a parameter processor genus type 
     *  @return the returned <code>ParameterProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>parameterProcessorGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorList getParameterProcessorsByGenusType(org.osid.type.Type parameterProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.configuration.rules.parameterprocessor.ArrayParameterProcessorList(this.parameterProcessorsByGenus.get(parameterProcessorGenusType)));
    }


    /**
     *  Gets a <code>ParameterProcessorList</code> containing the given
     *  parameter processor record <code>Type</code>. In plenary mode, the
     *  returned list contains all known parameter processors or an error
     *  results. Otherwise, the returned list may contain only those
     *  parameter processors that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  parameterProcessorRecordType a parameter processor record type 
     *  @return the returned <code>parameterProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>parameterProcessorRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorList getParameterProcessorsByRecordType(org.osid.type.Type parameterProcessorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.configuration.rules.parameterprocessor.ArrayParameterProcessorList(this.parameterProcessorsByRecord.get(parameterProcessorRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.parameterProcessorsByGenus.clear();
        this.parameterProcessorsByRecord.clear();

        super.close();

        return;
    }
}
