//
// MutableIndexedMapCredentialLookupSession
//
//    Implements a Credential lookup service backed by a collection of
//    credentials indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.program;


/**
 *  Implements a Credential lookup service backed by a collection of
 *  credentials. The credentials are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some credentials may be compatible
 *  with more types than are indicated through these credential
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of credentials can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapCredentialLookupSession
    extends net.okapia.osid.jamocha.core.course.program.spi.AbstractIndexedMapCredentialLookupSession
    implements org.osid.course.program.CredentialLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapCredentialLookupSession} with no credentials.
     *
     *  @param courseCatalog the course catalog
     *  @throws org.osid.NullArgumentException {@code courseCatalog}
     *          is {@code null}
     */

      public MutableIndexedMapCredentialLookupSession(org.osid.course.CourseCatalog courseCatalog) {
        setCourseCatalog(courseCatalog);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapCredentialLookupSession} with a
     *  single credential.
     *  
     *  @param courseCatalog the course catalog
     *  @param  credential a single credential
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code credential} is {@code null}
     */

    public MutableIndexedMapCredentialLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.course.program.Credential credential) {
        this(courseCatalog);
        putCredential(credential);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapCredentialLookupSession} using an
     *  array of credentials.
     *
     *  @param courseCatalog the course catalog
     *  @param  credentials an array of credentials
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code credentials} is {@code null}
     */

    public MutableIndexedMapCredentialLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.course.program.Credential[] credentials) {
        this(courseCatalog);
        putCredentials(credentials);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapCredentialLookupSession} using a
     *  collection of credentials.
     *
     *  @param courseCatalog the course catalog
     *  @param  credentials a collection of credentials
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code credentials} is {@code null}
     */

    public MutableIndexedMapCredentialLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  java.util.Collection<? extends org.osid.course.program.Credential> credentials) {

        this(courseCatalog);
        putCredentials(credentials);
        return;
    }
    

    /**
     *  Makes a {@code Credential} available in this session.
     *
     *  @param  credential a credential
     *  @throws org.osid.NullArgumentException {@code credential{@code  is
     *          {@code null}
     */

    @Override
    public void putCredential(org.osid.course.program.Credential credential) {
        super.putCredential(credential);
        return;
    }


    /**
     *  Makes an array of credentials available in this session.
     *
     *  @param  credentials an array of credentials
     *  @throws org.osid.NullArgumentException {@code credentials{@code 
     *          is {@code null}
     */

    @Override
    public void putCredentials(org.osid.course.program.Credential[] credentials) {
        super.putCredentials(credentials);
        return;
    }


    /**
     *  Makes collection of credentials available in this session.
     *
     *  @param  credentials a collection of credentials
     *  @throws org.osid.NullArgumentException {@code credential{@code  is
     *          {@code null}
     */

    @Override
    public void putCredentials(java.util.Collection<? extends org.osid.course.program.Credential> credentials) {
        super.putCredentials(credentials);
        return;
    }


    /**
     *  Removes a Credential from this session.
     *
     *  @param credentialId the {@code Id} of the credential
     *  @throws org.osid.NullArgumentException {@code credentialId{@code  is
     *          {@code null}
     */

    @Override
    public void removeCredential(org.osid.id.Id credentialId) {
        super.removeCredential(credentialId);
        return;
    }    
}
