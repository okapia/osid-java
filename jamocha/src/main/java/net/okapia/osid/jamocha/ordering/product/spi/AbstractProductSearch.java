//
// AbstractProductSearch.java
//
//     A template for making a Product Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.product.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing product searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractProductSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.ordering.ProductSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.ordering.records.ProductSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.ordering.ProductSearchOrder productSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of products. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  productIds list of products
     *  @throws org.osid.NullArgumentException
     *          <code>productIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongProducts(org.osid.id.IdList productIds) {
        while (productIds.hasNext()) {
            try {
                this.ids.add(productIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongProducts</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of product Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getProductIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  productSearchOrder product search order 
     *  @throws org.osid.NullArgumentException
     *          <code>productSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>productSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderProductResults(org.osid.ordering.ProductSearchOrder productSearchOrder) {
	this.productSearchOrder = productSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.ordering.ProductSearchOrder getProductSearchOrder() {
	return (this.productSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given product search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a product implementing the requested record.
     *
     *  @param productSearchRecordType a product search record
     *         type
     *  @return the product search record
     *  @throws org.osid.NullArgumentException
     *          <code>productSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(productSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.ProductSearchRecord getProductSearchRecord(org.osid.type.Type productSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.ordering.records.ProductSearchRecord record : this.records) {
            if (record.implementsRecordType(productSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(productSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this product search. 
     *
     *  @param productSearchRecord product search record
     *  @param productSearchRecordType product search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addProductSearchRecord(org.osid.ordering.records.ProductSearchRecord productSearchRecord, 
                                           org.osid.type.Type productSearchRecordType) {

        addRecordType(productSearchRecordType);
        this.records.add(productSearchRecord);        
        return;
    }
}
