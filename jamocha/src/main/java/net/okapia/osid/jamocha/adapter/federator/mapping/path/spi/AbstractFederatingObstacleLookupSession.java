//
// AbstractFederatingObstacleLookupSession.java
//
//     An abstract federating adapter for an ObstacleLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.mapping.path.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  ObstacleLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingObstacleLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.mapping.path.ObstacleLookupSession>
    implements org.osid.mapping.path.ObstacleLookupSession {

    private boolean parallel = false;
    private org.osid.mapping.Map map = new net.okapia.osid.jamocha.nil.mapping.map.UnknownMap();


    /**
     *  Constructs a new <code>AbstractFederatingObstacleLookupSession</code>.
     */

    protected AbstractFederatingObstacleLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.mapping.path.ObstacleLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Map/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Map Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getMapId() {
        return (this.map.getId());
    }


    /**
     *  Gets the <code>Map</code> associated with this 
     *  session.
     *
     *  @return the <code>Map</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.Map getMap()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.map);
    }


    /**
     *  Sets the <code>Map</code>.
     *
     *  @param  map the map for this session
     *  @throws org.osid.NullArgumentException <code>map</code>
     *          is <code>null</code>
     */

    protected void setMap(org.osid.mapping.Map map) {
        nullarg(map, "map");
        this.map = map;
        return;
    }


    /**
     *  Tests if this user can perform <code>Obstacle</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupObstacles() {
        for (org.osid.mapping.path.ObstacleLookupSession session : getSessions()) {
            if (session.canLookupObstacles()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Obstacle</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeObstacleView() {
        for (org.osid.mapping.path.ObstacleLookupSession session : getSessions()) {
            session.useComparativeObstacleView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Obstacle</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryObstacleView() {
        for (org.osid.mapping.path.ObstacleLookupSession session : getSessions()) {
            session.usePlenaryObstacleView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include obstacles in maps which are children
     *  of this map in the map hierarchy.
     */

    @OSID @Override
    public void useFederatedMapView() {
        for (org.osid.mapping.path.ObstacleLookupSession session : getSessions()) {
            session.useFederatedMapView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this map only.
     */

    @OSID @Override
    public void useIsolatedMapView() {
        for (org.osid.mapping.path.ObstacleLookupSession session : getSessions()) {
            session.useIsolatedMapView();
        }

        return;
    }


    /**
     *  Only active obstacles are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveObstacleView() {
        for (org.osid.mapping.path.ObstacleLookupSession session : getSessions()) {
            session.useActiveObstacleView();
        }

        return;
    }


    /**
     *  Active and inactive obstacles are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusObstacleView() {
        for (org.osid.mapping.path.ObstacleLookupSession session : getSessions()) {
            session.useAnyStatusObstacleView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>Obstacle</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Obstacle</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Obstacle</code> and
     *  retained for compatibility.
     *
     *  In active mode, obstacles are returned that are currently
     *  active. In any status mode, active and inactive obstacles
     *  are returned.
     *
     *  @param  obstacleId <code>Id</code> of the
     *          <code>Obstacle</code>
     *  @return the obstacle
     *  @throws org.osid.NotFoundException <code>obstacleId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>obstacleId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.Obstacle getObstacle(org.osid.id.Id obstacleId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.mapping.path.ObstacleLookupSession session : getSessions()) {
            try {
                return (session.getObstacle(obstacleId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(obstacleId + " not found");
    }


    /**
     *  Gets an <code>ObstacleList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  obstacles specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Obstacles</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, obstacles are returned that are currently
     *  active. In any status mode, active and inactive obstacles
     *  are returned.
     *
     *  @param  obstacleIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Obstacle</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>obstacleIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleList getObstaclesByIds(org.osid.id.IdList obstacleIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.mapping.path.obstacle.MutableObstacleList ret = new net.okapia.osid.jamocha.mapping.path.obstacle.MutableObstacleList();

        try (org.osid.id.IdList ids = obstacleIds) {
            while (ids.hasNext()) {
                ret.addObstacle(getObstacle(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>ObstacleList</code> corresponding to the given
     *  obstacle genus <code>Type</code> which does not include
     *  obstacles of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  obstacles or an error results. Otherwise, the returned list
     *  may contain only those obstacles that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, obstacles are returned that are currently
     *  active. In any status mode, active and inactive obstacles
     *  are returned.
     *
     *  @param  obstacleGenusType an obstacle genus type 
     *  @return the returned <code>Obstacle</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>obstacleGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleList getObstaclesByGenusType(org.osid.type.Type obstacleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.mapping.path.obstacle.FederatingObstacleList ret = getObstacleList();

        for (org.osid.mapping.path.ObstacleLookupSession session : getSessions()) {
            ret.addObstacleList(session.getObstaclesByGenusType(obstacleGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>ObstacleList</code> corresponding to the given
     *  obstacle genus <code>Type</code> and include any additional
     *  obstacles with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  obstacles or an error results. Otherwise, the returned list
     *  may contain only those obstacles that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, obstacles are returned that are currently
     *  active. In any status mode, active and inactive obstacles
     *  are returned.
     *
     *  @param  obstacleGenusType an obstacle genus type 
     *  @return the returned <code>Obstacle</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>obstacleGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleList getObstaclesByParentGenusType(org.osid.type.Type obstacleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.mapping.path.obstacle.FederatingObstacleList ret = getObstacleList();

        for (org.osid.mapping.path.ObstacleLookupSession session : getSessions()) {
            ret.addObstacleList(session.getObstaclesByParentGenusType(obstacleGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>ObstacleList</code> containing the given
     *  obstacle record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  obstacles or an error results. Otherwise, the returned list
     *  may contain only those obstacles that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, obstacles are returned that are currently
     *  active. In any status mode, active and inactive obstacles
     *  are returned.
     *
     *  @param  obstacleRecordType an obstacle record type 
     *  @return the returned <code>Obstacle</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>obstacleRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleList getObstaclesByRecordType(org.osid.type.Type obstacleRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.mapping.path.obstacle.FederatingObstacleList ret = getObstacleList();

        for (org.osid.mapping.path.ObstacleLookupSession session : getSessions()) {
            ret.addObstacleList(session.getObstaclesByRecordType(obstacleRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>ObstacleList</code> containing the given path.
     *
     *  In plenary mode, the returned list contains all known
     *  obstacles or an error results. Otherwise, the returned list
     *  may contain only those obstacles that are accessible through
     *  this session.
     *
     *  In active mode, obstacles are returned that are currently
     *  active. In any status mode, active and inactive obstacles are
     *  returned.
     *
     *  @param  pathId a path <code>Id</code>
     *  @return the returned <code>Obstacle</code> list
     *  @throws org.osid.NullArgumentException <code>pathId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleList getObstaclesForPath(org.osid.id.Id pathId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.mapping.path.obstacle.FederatingObstacleList ret = getObstacleList();

        for (org.osid.mapping.path.ObstacleLookupSession session : getSessions()) {
            ret.addObstacleList(session.getObstaclesForPath(pathId));
        }

        ret.noMore();
        return (ret);
    }

    
    /**
     *  Gets an <code>ObstacleList</code> containing the given path
     *  between the given coordinates inclusive.
     *
     *  In plenary mode, the returned list contains all known
     *  obstacles or an error results. Otherwise, the returned list
     *  may contain only those obstacles that are accessible through
     *  this session.
     *
     *  In active mode, obstacles are returned that are currently
     *  active. In any status mode, active and inactive obstacles are
     *  returned.
     *
     *  @param  pathId a path <code>Id</code>
     *  @param  coordinate starting coordinate
     *  @param  distance a distance from coordinate
     *  @return the returned <code>Obstacle</code> list
     *  @throws org.osid.NullArgumentException <code>pathId</code>,
     *          <code>coordinate </code> or <code>distance</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleList getObstaclesForPathAtCoordinate(org.osid.id.Id pathId,
                                                                              org.osid.mapping.Coordinate coordinate,
                                                                              org.osid.mapping.Distance distance)
        throws org.osid.OperationFailedException,
              org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.mapping.path.obstacle.FederatingObstacleList ret = getObstacleList();

        for (org.osid.mapping.path.ObstacleLookupSession session : getSessions()) {
            ret.addObstacleList(session.getObstaclesForPathAtCoordinate(pathId, coordinate, distance));
        }

        ret.noMore();
        return (ret);
    }

        
    /**
     *  Gets all <code>Obstacles</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  obstacles or an error results. Otherwise, the returned list
     *  may contain only those obstacles that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, obstacles are returned that are currently
     *  active. In any status mode, active and inactive obstacles
     *  are returned.
     *
     *  @return a list of <code>Obstacles</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleList getObstacles()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.mapping.path.obstacle.FederatingObstacleList ret = getObstacleList();

        for (org.osid.mapping.path.ObstacleLookupSession session : getSessions()) {
            ret.addObstacleList(session.getObstacles());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.mapping.path.obstacle.FederatingObstacleList getObstacleList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.mapping.path.obstacle.ParallelObstacleList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.mapping.path.obstacle.CompositeObstacleList());
        }
    }
}
