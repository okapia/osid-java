//
// AbstractProcedureQuery.java
//
//     A template for making a Procedure Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recipe.procedure.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for procedures.
 */

public abstract class AbstractProcedureQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.recipe.ProcedureQuery {

    private final java.util.Collection<org.osid.recipe.records.ProcedureQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the objective <code> Id </code> for this query. 
     *
     *  @param  objectiveId the asset <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> objectiveId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLearningObjectiveId(org.osid.id.Id objectiveId, 
                                         boolean match) {
        return;
    }


    /**
     *  Clears the objective <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearLearningObjectiveIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> ObjectiveQuery </code> is available. 
     *
     *  @return <code> true </code> if an objective query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLearningObjectiveQuery() {
        return (false);
    }


    /**
     *  Gets the query for an objective. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the objective query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLearningObjectiveQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQuery getLearningObjectiveQuery() {
        throw new org.osid.UnimplementedException("supportsLearningObjectiveQuery() is false");
    }


    /**
     *  Matches directions with any objective. 
     *
     *  @param  match <code> true </code> to match directions with any 
     *          objective, <code> false </code> to match directions with no 
     *          objectives 
     */

    @OSID @Override
    public void matchAnyLearningObjective(boolean match) {
        return;
    }


    /**
     *  Clears the objective query terms. 
     */

    @OSID @Override
    public void clearLearningObjectiveTerms() {
        return;
    }


    /**
     *  Sets the award <code> Id </code> for this query to match procedures 
     *  assigned to cook books. 
     *
     *  @param  cookbookId a cook book <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCookbookId(org.osid.id.Id cookbookId, boolean match) {
        return;
    }


    /**
     *  Clears the cook book <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCookbookIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> CookbookQuery </code> is available. 
     *
     *  @return <code> true </code> if a cook book query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCookbookQuery() {
        return (false);
    }


    /**
     *  Gets the query for a cook book query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the cook book query 
     *  @throws org.osid.UnimplementedException <code> supportsCookbookQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.CookbookQuery getCookbookQuery() {
        throw new org.osid.UnimplementedException("supportsCookbookQuery() is false");
    }


    /**
     *  Clears the cook book terms. 
     */

    @OSID @Override
    public void clearCookbookTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given procedure query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a procedure implementing the requested record.
     *
     *  @param procedureRecordType a procedure record type
     *  @return the procedure query record
     *  @throws org.osid.NullArgumentException
     *          <code>procedureRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(procedureRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.recipe.records.ProcedureQueryRecord getProcedureQueryRecord(org.osid.type.Type procedureRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recipe.records.ProcedureQueryRecord record : this.records) {
            if (record.implementsRecordType(procedureRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(procedureRecordType + " is not supported");
    }


    /**
     *  Adds a record to this procedure query. 
     *
     *  @param procedureQueryRecord procedure query record
     *  @param procedureRecordType procedure record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addProcedureQueryRecord(org.osid.recipe.records.ProcedureQueryRecord procedureQueryRecord, 
                                          org.osid.type.Type procedureRecordType) {

        addRecordType(procedureRecordType);
        nullarg(procedureQueryRecord, "procedure query record");
        this.records.add(procedureQueryRecord);        
        return;
    }
}
