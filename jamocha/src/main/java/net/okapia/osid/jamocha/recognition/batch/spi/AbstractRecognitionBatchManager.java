//
// AbstractRecognitionBatchManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recognition.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractRecognitionBatchManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.recognition.batch.RecognitionBatchManager,
               org.osid.recognition.batch.RecognitionBatchProxyManager {


    /**
     *  Constructs a new <code>AbstractRecognitionBatchManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractRecognitionBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if bulk administration of conferrals is available. 
     *
     *  @return <code> true </code> if a conferral bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConferralBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of awards is available. 
     *
     *  @return <code> true </code> if an award bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of convocations is available. 
     *
     *  @return <code> true </code> if a convocation bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConvocationBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of academy is available. 
     *
     *  @return <code> true </code> if a academy bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAcademyBatchAdmin() {
        return (false);
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk conferral 
     *  administration service. 
     *
     *  @return a <code> ConferralBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.batch.ConferralBatchAdminSession getConferralBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.batch.RecognitionBatchManager.getConferralBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk conferral 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ConferralBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.batch.ConferralBatchAdminSession getConferralBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.batch.RecognitionBatchProxyManager.getConferralBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk conferral 
     *  administration service for the given academy. 
     *
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @return a <code> ConferralBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> academyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.batch.ConferralBatchAdminSession getConferralBatchAdminSessionForAcademy(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.batch.RecognitionBatchManager.getConferralBatchAdminSessionForAcademy not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk conferral 
     *  administration service for the given academy. 
     *
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ConferralBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> academyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.batch.ConferralBatchAdminSession getConferralBatchAdminSessionForAcademy(org.osid.id.Id academyId, 
                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.batch.RecognitionBatchProxyManager.getConferralBatchAdminSessionForAcademy not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk award 
     *  administration service. 
     *
     *  @return an <code> AwardBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.batch.AwardBatchAdminSession getAwardBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.batch.RecognitionBatchManager.getAwardBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk award 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AwardBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.batch.AwardBatchAdminSession getAwardBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.batch.RecognitionBatchProxyManager.getAwardBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk award 
     *  administration service for the given academy. 
     *
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @return an <code> AwardBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> academyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.batch.AwardBatchAdminSession getAwardBatchAdminSessionForAcademy(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.batch.RecognitionBatchManager.getAwardBatchAdminSessionForAcademy not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk award 
     *  administration service for the given academy. 
     *
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AwardBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> academyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.batch.AwardBatchAdminSession getAwardBatchAdminSessionForAcademy(org.osid.id.Id academyId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.batch.RecognitionBatchProxyManager.getAwardBatchAdminSessionForAcademy not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  convocation administration service. 
     *
     *  @return a <code> ConvocationBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recognition.batch.ConvocationBatchAdminSession getConvocationBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.batch.RecognitionBatchManager.getConvocationBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  convocation administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ConvocationBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recognition.batch.ConvocationBatchAdminSession getConvocationBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.batch.RecognitionBatchProxyManager.getConvocationBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  convocation administration service for the given academy. 
     *
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @return a <code> ConvocationBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> academyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.batch.ConvocationBatchAdminSession getConvocationBatchAdminSessionForAcademy(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.batch.RecognitionBatchManager.getConvocationBatchAdminSessionForAcademy not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  convocation administration service for the given academy. 
     *
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ConvocationBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> academyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.batch.ConvocationBatchAdminSession getConvocationBatchAdminSessionForAcademy(org.osid.id.Id academyId, 
                                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.batch.RecognitionBatchProxyManager.getConvocationBatchAdminSessionForAcademy not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk academy 
     *  administration service. 
     *
     *  @return a <code> AcademyBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAcademyBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.batch.AcademyBatchAdminSession getAcademyBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.batch.RecognitionBatchManager.getAcademyBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk academy 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> AcademyBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAcademyBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.batch.AcademyBatchAdminSession getAcademyBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recognition.batch.RecognitionBatchProxyManager.getAcademyBatchAdminSession not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        return;
    }
}
