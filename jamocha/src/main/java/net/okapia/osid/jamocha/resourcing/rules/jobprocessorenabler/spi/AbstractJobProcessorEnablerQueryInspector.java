//
// AbstractJobProcessorEnablerQueryInspector.java
//
//     A template for making a JobProcessorEnablerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.rules.jobprocessorenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for job processor enablers.
 */

public abstract class AbstractJobProcessorEnablerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQueryInspector
    implements org.osid.resourcing.rules.JobProcessorEnablerQueryInspector {

    private final java.util.Collection<org.osid.resourcing.rules.records.JobProcessorEnablerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the job processor <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledJobProcessorIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the job processor query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorQueryInspector[] getRuledJobProcessorTerms() {
        return (new org.osid.resourcing.rules.JobProcessorQueryInspector[0]);
    }


    /**
     *  Gets the foundry <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFoundryIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the foundry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQueryInspector[] getFoundryTerms() {
        return (new org.osid.resourcing.FoundryQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given job processor enabler query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a job processor enabler implementing the requested record.
     *
     *  @param jobProcessorEnablerRecordType a job processor enabler record type
     *  @return the job processor enabler query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>jobProcessorEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(jobProcessorEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.rules.records.JobProcessorEnablerQueryInspectorRecord getJobProcessorEnablerQueryInspectorRecord(org.osid.type.Type jobProcessorEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.rules.records.JobProcessorEnablerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(jobProcessorEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(jobProcessorEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this job processor enabler query. 
     *
     *  @param jobProcessorEnablerQueryInspectorRecord job processor enabler query inspector
     *         record
     *  @param jobProcessorEnablerRecordType jobProcessorEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addJobProcessorEnablerQueryInspectorRecord(org.osid.resourcing.rules.records.JobProcessorEnablerQueryInspectorRecord jobProcessorEnablerQueryInspectorRecord, 
                                                   org.osid.type.Type jobProcessorEnablerRecordType) {

        addRecordType(jobProcessorEnablerRecordType);
        nullarg(jobProcessorEnablerRecordType, "job processor enabler record type");
        this.records.add(jobProcessorEnablerQueryInspectorRecord);        
        return;
    }
}
