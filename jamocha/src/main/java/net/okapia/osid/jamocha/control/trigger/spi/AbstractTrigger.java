//
// AbstractTrigger.java
//
//     Defines a Trigger.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.trigger.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Trigger</code>.
 */

public abstract class AbstractTrigger
    extends net.okapia.osid.jamocha.spi.AbstractOsidRule
    implements org.osid.control.Trigger {

    private org.osid.control.Controller controller;
    private org.osid.process.State discreetState;

    private boolean turnedOn;
    private boolean turnedOff;
    private boolean changedVariableAmount;
    private boolean changedDiscreetState;

    private java.math.BigDecimal maxThreshold;
    private java.math.BigDecimal minThreshold;

    private final java.util.Collection<org.osid.control.ActionGroup> actionGroups = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.control.Scene> scenes = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.control.Setting> settings = new java.util.LinkedHashSet<>();

    private final java.util.Collection<org.osid.control.records.TriggerRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the controller <code> Id. </code> 
     *
     *  @return the controller <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getControllerId() {
        return (this.controller.getId());
    }


    /**
     *  Gets the controller. 
     *
     *  @return the controller 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.control.Controller getController()
        throws org.osid.OperationFailedException {

        return (this.controller);
    }


    /**
     *  Sets the controller.
     *
     *  @param controller a controller
     *  @throws org.osid.NullArgumentException <code>controller</code>
     *          is <code>null</code>
     */

    protected void setController(org.osid.control.Controller controller) {
        nullarg(controller, "controller");
        this.controller = controller;
        return;
    }


    /**
     *  Tests if this trigger listens to controller ON events. 
     *
     *  @return <code> true </code> if this is an ON event listener, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean turnedOn() {
        return (this.turnedOn);
    }


    /**
     *  Listen to on events.
     *
     *  @param listen {@code true} to listen to on events, {@code
     *         false} otherwise
     */

    protected void setTurnedOn(boolean listen) {
        this.turnedOn = listen;
        return;
    }


    /**
     *  Tests if this trigger listens to controller OFF events. 
     *
     *  @return <code> true </code> if this is an OFF event listener,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean turnedOff() {
        return (this.turnedOff);
    }


    /**
     *  Listen to turned off events.
     *
     *  @param listen {@code true} to listen to off events, {@code
     *         false} otherwise
     */

    protected void setTurnedOff(boolean listen) {
        this.turnedOff = listen;
        return;
    }


    /**
     *  Tests if this trigger listens to changed variable amount
     *  controller events.
     *
     *  @return <code> true </code> if this is a change event listener, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean changedVariableAmount() {
        return (this.changedVariableAmount);
    }


    /**
     *  Sets the changed variable amount.
     *
     *  @param listen {@code true} to listen to changed amount events,
     *         {@code false} otherwise
     */

    protected void setChangedVariableAmount(boolean listen) {
        this.changedVariableAmount = listen;
        return;
    }


    /**
     *  Tests if this trigger listens to events where the variable
     *  amount or percentage increased to the same or above this
     *  value.
     *
     *  @return the max threshold 
     */

    @OSID @Override
    public java.math.BigDecimal changedExceedsVariableAmount() {
        return (this.maxThreshold);
    }


    /**
     *  Listen to changes above the given threshold.
     *
     *  @param max a max threshold
     *  @throws org.osid.NullArgumentException <code>max</code> is
     *          <code>null</code>
     */

    protected void setChangedExceedsVariableAmount(java.math.BigDecimal max) {
        nullarg(max, "max threshold");
        this.maxThreshold = max;
        return;
    }


    /**
     *  Tests if this trigger listens to events where the variable
     *  amount or percentage decreased to the same or below this
     *  value.
     *
     *  @return the max threshold 
     */

    @OSID @Override
    public java.math.BigDecimal changedDeceedsVariableAmount() {
        return (this.minThreshold);
    }


    /**
     *  Listen to changes below the given threshold.
     *
     *  @param min a min threshold
     *  @throws org.osid.NullArgumentException <code>min</code> is
     *          <code>null</code>
     */

    protected void setChangedDeceedsVariableAmount(java.math.BigDecimal min) {
        nullarg(min, "min threshold");
        this.minThreshold = min;
        return;
    }


    /**
     *  Tests if this trigger listens to controller state changes. 
     *
     *  @return <code> true </code> if this is a state change event
     *          listener, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean changedDiscreetState() {
        return (this.changedDiscreetState);
    }


    /**
     *  Listen for state changes.
     *
     *  @param listen {@code true} to listen to state change events,
     *         {@code false} otherwise
     */

    protected void setChangedDiscreetState(boolean listen) {
        this.changedDiscreetState = listen;
        return;
    }


    /**
     *  Tests if this trigger listens to controller events where a
     *  state has changed to a specific state.
     *
     *  @return <code> true </code> if this is a state change event, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean changedToDiscreetState() {
        return (this.discreetState != null);
    }


    /**
     *  Gets the discreet <code> State </code> <code> Id </code> for a changed 
     *  state event. 
     *
     *  @return the state <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> changedToDiscreetState() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getDiscreetStateId() {
        return (this.discreetState.getId());
    }


    /**
     *  Gets the discreet <code> State </code> for a changed state
     *  event.
     *
     *  @return the state 
     *  @throws org.osid.IllegalStateException <code>
     *          changedToDiscreetState() </code> is <code> false
     *          </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.process.State getDiscreetState()
        throws org.osid.OperationFailedException {

        if (!changedToDiscreetState()) {
            throw new org.osid.IllegalStateException("changedToDiscreetState() is false");
        }


        return (this.discreetState);
    }


    /**
     *  Sets the discreet state.
     *
     *  @param state a discreet state
     *  @throws org.osid.NullArgumentException <code>state</code> is
     *          <code>null</code>
     */

    protected void setDiscreetState(org.osid.process.State state) {
        nullarg(state, "state");
        this.discreetState = state;
        return;
    }


    /**
     *  Gets the <code> ActionGroup </code> <code> Ids </code> to
     *  execute in this trigger. Multiple action groups can be
     *  executed in the order of this list.
     *
     *  @return the action group <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getActionGroupIds() {
        try (org.osid.control.ActionGroupList groups = getActionGroups()) {
            return (new net.okapia.osid.jamocha.adapter.converter.control.actiongroup.ActionGroupToIdList(groups));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the <code> ActionGroups </code> to execute in this
     *  trigger.  Multiple action groups can be executed in the order
     *  of this list.
     *
     *  @return the action group 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.control.ActionGroupList getActionGroups()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.control.actiongroup.ArrayActionGroupList(this.actionGroups));
    }


    /**
     *  Adds an action group.
     *
     *  @param actionGroup an action group
     *  @throws org.osid.NullArgumentException
     *          <code>actionGroup</code> is <code>null</code>
     */

    protected void addActionGroup(org.osid.control.ActionGroup actionGroup) {
        nullarg(actionGroup, "action group");
        this.actionGroups.add(actionGroup);
        return;
    }


    /**
     *  Sets all the action groups.
     *
     *  @param actionGroups a collection of action groups
     *  @throws org.osid.NullArgumentException
     *          <code>actionGroups</code> is <code>null</code>
     */

    protected void setActionGroups(java.util.Collection<org.osid.control.ActionGroup> actionGroups) {
        nullarg(actionGroups, "action groups");
        this.actionGroups.clear();
        this.actionGroups.addAll(actionGroups);
        return;
    }


    /**
     *  Gets the <code> Scene </code> <code> Ids </code> to execute in
     *  this trigger. Multiple scenes can be executed in the order of
     *  this list.  This is a shortcut to defining <code> Actions
     *  </code> and <code> ActionGroups </code> which offer more
     *  control in scene execution.
     *
     *  @return the action group <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getSceneIds() {
        try (org.osid.control.SceneList scenes = getScenes()) {
            return (new net.okapia.osid.jamocha.adapter.converter.control.scene.SceneToIdList(scenes));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the <code> Scenes </code> to execute in this
     *  trigger. Multiple scenes can be executed in the order of this
     *  list. This is a shortcut to defining <code> Actions </code>
     *  and <code> ActionGroups </code> which offer more control in
     *  scene execution.
     *
     *  @return the scenes
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.control.SceneList getScenes()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.control.scene.ArraySceneList(this.scenes));
    }


    /**
     *  Adds an scene.
     *
     *  @param scene a Scene
     *  @throws org.osid.NullArgumentException
     *          <code>scene</code> is <code>null</code>
     */

    protected void addScene(org.osid.control.Scene scene) {
        nullarg(scene, "scene");
        this.scenes.add(scene);
        return;
    }


    /**
     *  Sets all the scenes.
     *
     *  @param scenes a collection of scenes
     *  @throws org.osid.NullArgumentException <code>scenes</code> is
     *          <code>null</code>
     */

    protected void setScenes(java.util.Collection<org.osid.control.Scene> scenes) {
        nullarg(scenes, "scenes");
        this.scenes.clear();
        this.scenes.addAll(scenes);
        return;
    }


    /**
     *  Gets the <code> Setting </code> <code> Ids </code> to execute
     *  in this trigger. Multiple settings can be executed in the
     *  order of this list.  This is a shortcut to defining <code>
     *  Settings, </code> <code> Actions </code> and <code>
     *  ActionGroups </code> which offer more control in scene and
     *  setting execution.
     *
     *  @return the action group <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getSettingIds() {
        try (org.osid.control.SettingList settings = getSettings()) {
            return (new net.okapia.osid.jamocha.adapter.converter.control.setting.SettingToIdList(settings));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the <code> Settings </code> to execute in this
     *  trigger. Multiple settings can be executed in the order of
     *  this list. This is a shortcut to defining <code> Settings,
     *  </code> <code> Actions </code> and <code> ActionGroups </code>
     *  which offer more control in scene and setting execution.
     *
     *  @return the settings
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.control.SettingList getSettings()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.control.setting.ArraySettingList(this.settings));
    }


    /**
     *  Adds an setting.
     *
     *  @param setting a setting
     *  @throws org.osid.NullArgumentException
     *          <code>setting</code> is <code>null</code>
     */

    protected void addSetting(org.osid.control.Setting setting) {
        nullarg(setting, "setting");
        this.settings.add(setting);
        return;
    }


    /**
     *  Sets all the settings.
     *
     *  @param settings a collection of settings
     *  @throws org.osid.NullArgumentException <code>settings</code> is
     *          <code>null</code>
     */

    protected void setSettings(java.util.Collection<org.osid.control.Setting> settings) {
        nullarg(settings, "settings");
        this.settings.clear();
        this.settings.addAll(settings);
        return;
    }


    /**
     *  Tests if this trigger supports the given record
     *  <code>Type</code>.
     *
     *  @param  triggerRecordType a trigger record type 
     *  @return <code>true</code> if the triggerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>triggerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type triggerRecordType) {
        for (org.osid.control.records.TriggerRecord record : this.records) {
            if (record.implementsRecordType(triggerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  triggerRecordType the trigger record type 
     *  @return the trigger record 
     *  @throws org.osid.NullArgumentException
     *          <code>triggerRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(triggerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.TriggerRecord getTriggerRecord(org.osid.type.Type triggerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.TriggerRecord record : this.records) {
            if (record.implementsRecordType(triggerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(triggerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this trigger. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param triggerRecord the trigger record
     *  @param triggerRecordType trigger record type
     *  @throws org.osid.NullArgumentException
     *          <code>triggerRecord</code> or
     *          <code>triggerRecordTypetrigger</code> is
     *          <code>null</code>
     */
            
    protected void addTriggerRecord(org.osid.control.records.TriggerRecord triggerRecord, 
                                     org.osid.type.Type triggerRecordType) {

        addRecordType(triggerRecordType);
        this.records.add(triggerRecord);
        
        return;
    }
}
