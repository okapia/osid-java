//
// StockElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.stock.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class StockElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the StockElement Id.
     *
     *  @return the stock element Id
     */

    public static org.osid.id.Id getStockEntityId() {
        return (makeEntityId("osid.inventory.Stock"));
    }


    /**
     *  Gets the SKU element Id.
     *
     *  @return the SKU element Id
     */

    public static org.osid.id.Id getSKU() {
        return (makeElementId("osid.inventory.stock.SKU"));
    }


    /**
     *  Gets the ModelIds element Id.
     *
     *  @return the ModelIds element Id
     */

    public static org.osid.id.Id getModelIds() {
        return (makeElementId("osid.inventory.stock.ModelIds"));
    }


    /**
     *  Gets the Models element Id.
     *
     *  @return the Models element Id
     */

    public static org.osid.id.Id getModels() {
        return (makeElementId("osid.inventory.stock.Models"));
    }


    /**
     *  Gets the LocationDescription element Id.
     *
     *  @return the LocationDescription element Id
     */

    public static org.osid.id.Id getLocationDescription() {
        return (makeElementId("osid.inventory.stock.LocationDescription"));
    }


    /**
     *  Gets the LocationIds element Id.
     *
     *  @return the LocationIds element Id
     */

    public static org.osid.id.Id getLocationIds() {
        return (makeElementId("osid.inventory.stock.LocationIds"));
    }


    /**
     *  Gets the Locations element Id.
     *
     *  @return the Locations element Id
     */

    public static org.osid.id.Id getLocations() {
        return (makeElementId("osid.inventory.stock.Locations"));
    }


    /**
     *  Gets the AncestorStockId element Id.
     *
     *  @return the AncestorStockId element Id
     */

    public static org.osid.id.Id getAncestorStockId() {
        return (makeQueryElementId("osid.inventory.stock.AncestorStockId"));
    }


    /**
     *  Gets the AncestorStock element Id.
     *
     *  @return the AncestorStock element Id
     */

    public static org.osid.id.Id getAncestorStock() {
        return (makeQueryElementId("osid.inventory.stock.AncestorStock"));
    }


    /**
     *  Gets the DescendantStockId element Id.
     *
     *  @return the DescendantStockId element Id
     */

    public static org.osid.id.Id getDescendantStockId() {
        return (makeQueryElementId("osid.inventory.stock.DescendantStockId"));
    }


    /**
     *  Gets the DescendantStock element Id.
     *
     *  @return the DescendantStock element Id
     */

    public static org.osid.id.Id getDescendantStock() {
        return (makeQueryElementId("osid.inventory.stock.DescendantStock"));
    }


    /**
     *  Gets the WarehouseId element Id.
     *
     *  @return the WarehouseId element Id
     */

    public static org.osid.id.Id getWarehouseId() {
        return (makeQueryElementId("osid.inventory.stock.WarehouseId"));
    }


    /**
     *  Gets the Warehouse element Id.
     *
     *  @return the Warehouse element Id
     */

    public static org.osid.id.Id getWarehouse() {
        return (makeQueryElementId("osid.inventory.stock.Warehouse"));
    }


    /**
     *  Gets the LocatinDescription element Id.
     *
     *  @return the LocatinDescription element Id
     */

    public static org.osid.id.Id getLocatinDescription() {
        return (makeElementId("osid.inventory.stock.LocatinDescription"));
    }
}
