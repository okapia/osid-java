//
// AbstractPoolConstrainerSearch.java
//
//     A template for making a PoolConstrainer Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.poolconstrainer.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing pool constrainer searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractPoolConstrainerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.provisioning.rules.PoolConstrainerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.provisioning.rules.records.PoolConstrainerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.provisioning.rules.PoolConstrainerSearchOrder poolConstrainerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of pool constrainers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  poolConstrainerIds list of pool constrainers
     *  @throws org.osid.NullArgumentException
     *          <code>poolConstrainerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongPoolConstrainers(org.osid.id.IdList poolConstrainerIds) {
        while (poolConstrainerIds.hasNext()) {
            try {
                this.ids.add(poolConstrainerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongPoolConstrainers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of pool constrainer Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getPoolConstrainerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  poolConstrainerSearchOrder pool constrainer search order 
     *  @throws org.osid.NullArgumentException
     *          <code>poolConstrainerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>poolConstrainerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderPoolConstrainerResults(org.osid.provisioning.rules.PoolConstrainerSearchOrder poolConstrainerSearchOrder) {
	this.poolConstrainerSearchOrder = poolConstrainerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.provisioning.rules.PoolConstrainerSearchOrder getPoolConstrainerSearchOrder() {
	return (this.poolConstrainerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given pool constrainer search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a pool constrainer implementing the requested record.
     *
     *  @param poolConstrainerSearchRecordType a pool constrainer search record
     *         type
     *  @return the pool constrainer search record
     *  @throws org.osid.NullArgumentException
     *          <code>poolConstrainerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(poolConstrainerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.PoolConstrainerSearchRecord getPoolConstrainerSearchRecord(org.osid.type.Type poolConstrainerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.provisioning.rules.records.PoolConstrainerSearchRecord record : this.records) {
            if (record.implementsRecordType(poolConstrainerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(poolConstrainerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this pool constrainer search. 
     *
     *  @param poolConstrainerSearchRecord pool constrainer search record
     *  @param poolConstrainerSearchRecordType poolConstrainer search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPoolConstrainerSearchRecord(org.osid.provisioning.rules.records.PoolConstrainerSearchRecord poolConstrainerSearchRecord, 
                                           org.osid.type.Type poolConstrainerSearchRecordType) {

        addRecordType(poolConstrainerSearchRecordType);
        this.records.add(poolConstrainerSearchRecord);        
        return;
    }
}
