//
// AbstractMutableAssessmentPart.java
//
//     Defines a mutable AssessmentPart.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.assessment.authoring.assessmentpart.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>AssessmentPart</code>.
 */

public abstract class AbstractMutableAssessmentPart
    extends net.okapia.osid.jamocha.assessment.authoring.assessmentpart.spi.AbstractAssessmentPart
    implements org.osid.assessment.authoring.AssessmentPart,
               net.okapia.osid.jamocha.builder.assessment.authoring.assessmentpart.AssessmentPartMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Sets the sequestered flag.
     *
     *  @param sequestered <code> true </code> if this containable is
     *         sequestered, <code> false </code> if this containable
     *         may appear outside its aggregate
     */

    @Override
    public void setSequestered(boolean sequestered) {
        super.setSequestered(sequestered);
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this assessment part. 
     *
     *  @param record assessment part record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addAssessmentPartRecord(org.osid.assessment.authoring.records.AssessmentPartRecord record, org.osid.type.Type recordType) {
        super.addAssessmentPartRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Enables this assessment part. Enabling an operable overrides any
     *  enabling rule that may exist.
     *  
     *  @param enabled <code>true</code> if enabled, <code>false<code>
     *         otherwise
     */
    
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        return;
    }


    /**
     *  Disables this assessment part. Disabling an operable overrides
     *  any enabling rule that may exist.
     *
     *  @param disabled <code> true </code> if this object is
     *         disabled, <code> false </code> otherwise
     */
    
    public void setDisabled(boolean disabled) {
        super.setDisabled(disabled);
        return;
    }


    /**
     *  Sets the operational flag.
     *
     *  @param operational <code>true</code>if operational,
     *         <code>false</code> if not operational
     */
    
    public void setOperational(boolean operational) {
        super.setOperational(operational);
        return;
    }


    /**
     *  Sets the display name for this assessment part.
     *
     *  @param displayName the name for this assessment part
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this assessment part.
     *
     *  @param description the description of this assessment part
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the assessment.
     *
     *  @param assessment an assessment
     *  @throws org.osid.NullArgumentException
     *          <code>assessment</code> is <code>null</code>
     */

    @Override
    public void setAssessment(org.osid.assessment.Assessment assessment) {
        super.setAssessment(assessment);
        return;
    }


    /**
     *  Sets the assessment part.
     *
     *  @param parent an assessment part
     *  @throws org.osid.NullArgumentException <code>parent</code> is
     *          <code>null</code>
     */

    @Override
    public void setParentAssessmentPart(org.osid.assessment.authoring.AssessmentPart parent) {
        super.setParentAssessmentPart(parent);
        return;
    }


    /**
     *  Sets the section flag.
     *
     *  @param section <code>true</code> if a section,
     *         <code>false</code> otherwise
     */

    @Override
    public void setSection(boolean section) {
        super.setSection(section);
        return;
    }

    
    /**
     *  Sets the weight.
     *
     *  @param weight a weight
     */

    @Override
    public void setWeight(long weight) {
        super.setWeight(weight);
        return;
    }


    /**
     *  Sets the allocated time.
     *
     *  @param time an allocated time
     *  @throws org.osid.NullArgumentException
     *          <code>time</code> is <code>null</code>
     */

    @Override
    public void setAllocatedTime(org.osid.calendaring.Duration time) {
        super.setAllocatedTime(time);
        return;
    }


    /**
     *  Adds a child assessment part.
     *
     *  @param child a child assessment part
     *  @throws org.osid.NullArgumentException
     *          <code>child</code> is <code>null</code>
     */

    @Override
    public void addChildAssessmentPart(org.osid.assessment.authoring.AssessmentPart child) {
        super.addChildAssessmentPart(child);
        return;
    }


    /**
     *  Sets all the child assessment parts.
     *
     *  @param children a collection of child assessment parts
     *  @throws org.osid.NullArgumentException <code>children</code>
     *          is <code>null</code>
     */

    @Override
    public void setChildAssessmentParts(java.util.Collection<org.osid.assessment.authoring.AssessmentPart> children) {
        super.setChildAssessmentParts(children);
        return;
    }
}

