//
// AbstractProfileEntryNotificationSession.java
//
//     A template for making ProfileEntryNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.profile.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code ProfileEntry} objects. This session is
 *  intended for consumers needing to synchronize their state with
 *  this service without the use of polling. Notifications are
 *  cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code ProfileEntry} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for profile entry entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractProfileEntryNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.profile.ProfileEntryNotificationSession {

    private boolean federated = false;
    private boolean implicit  = false;
    private org.osid.profile.Profile profile = new net.okapia.osid.jamocha.nil.profile.profile.UnknownProfile();


    /**
     *  Gets the {@code Profile/code> {@code Id} associated with this
     *  session.
     *
     *  @return the {@code Profile Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getProfileId() {
        return (this.profile.getId());
    }

    
    /**
     *  Gets the {@code Profile} associated with this session.
     *
     *  @return the {@code Profile} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.Profile getProfile()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.profile);
    }


    /**
     *  Sets the {@code Profile}.
     *
     *  @param profile the profile for this session
     *  @throws org.osid.NullArgumentException {@code profile}
     *          is {@code null}
     */

    protected void setProfile(org.osid.profile.Profile profile) {
        nullarg(profile, "profile");
        this.profile = profile;
        return;
    }


    /**
     *  Tests if this user can register for {@code ProfileEntry}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForProfileEntryNotifications() {
        return (true);
    }


    /**
     *  Reliable notifications are desired. In reliable mode,
     *  notifications are to be acknowledged using <code>
     *  acknowledgeProfileEntryNotification() </code>.
     */

    @OSID @Override
    public void reliableProfileEntryNotifications() {
        return;
    }


    /**
     *  Unreliable notifications are desired. In unreliable mode,
     *  notifications do not need to be acknowledged.
     */

    @OSID @Override
    public void unreliableProfileEntryNotifications() {
        return;
    }


    /**
     *  Acknowledge a profile entry notification.
     *
     *  @param  notificationId the <code> Id </code> of the notification
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void acknowledgeProfileEntryNotification(org.osid.id.Id notificationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include entries in profiles which are children of
     *  this profile in the profile hierarchy.
     */

    @OSID @Override
    public void useFederatedProfileView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this profile only.
     */

    @OSID @Override
    public void useIsolatedProfileView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Sets the view for methods in this session to implicit profile
     *  entries.  An implicit view will include profile entries
     *  derived from other profile entries. This method is the
     *  opposite of <code> explicitProfileEntryView()</code>.
     */

    @OSID @Override
    public void useImplicitProfileEntryView() {
        this.implicit = true;
        return;
    }


    /**
     *  Sets the view for methods in this session to explicit profile
     *  entries.  An explicit view includes only those profile entries
     *  that were explicitly defined and not implied. This method is
     *  the opposite of <code> implicitProfileEntryView(). </code>
     */

    @OSID @Override
    public void useExplicitProfileEntryView() {
        this.implicit = false;
        return;
    }

    
    /**
     *  Tests if an implicit or explicit view is set.
     *
     *  @return <code>true</code> if explicit and implicit
     *          profile entries should be returned</code>,
     *          <code>false</code> if only explicit profile entries
     */

    protected boolean isImplicit() {
        return (this.implicit);
    }


    /**
     *  Register for notifications of new profile entries. {@code
     *  ProfileEntryReceiver.newProfileEntry()} is invoked when a new
     *  {@code ProfileEntry} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewProfileEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new profileEntries for the given
     *  resource {@code Id}. {@code
     *  ProfileEntryReceiver.newProfileEntry()} is invoked when a new
     *  {@code ProfileEntry} is created.
     *
     *  @param  resourceId the {@code Id} of the resource to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewProfileEntriesForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /** 
     *  Register for notifications of new profileEntries for the given
     *  profile item {@code Id}. {@code
     *  ProfileEntryReceiver.newProfileEntry()} is invoked when a new
     *  {@code ProfileEntry} is created.
     *
     *  @param  profileItemId the {@code Id} of the profile item to monitor
     *  @throws org.osid.NullArgumentException {@code profileItemId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewProfileEntriesForProfileItem(org.osid.id.Id profileItemId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated profile entries. {@code
     *  ProfileEntryReceiver.changedProfileEntry()} is invoked when a
     *  profile entry is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedProfileEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated profileEntries for the
     *  given resource {@code Id}. {@code
     *  ProfileEntryReceiver.changedProfileEntry()} is invoked when a
     *  {@code ProfileEntry} in this profile is changed.
     *
     *  @param  resourceId the {@code Id} of the resource to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedProfileEntriesForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated profileEntries for the
     *  given profile item {@code Id}. {@code
     *  ProfileEntryReceiver.changedProfileEntry()} is invoked when a
     *  {@code ProfileEntry} in this profile is changed.
     *
     *  @param  profileItemId the {@code Id} of the profile item to monitor
     *  @throws org.osid.NullArgumentException {@code profileItemId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedProfileEntriesForProfileItem(org.osid.id.Id profileItemId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated profile entry. {@code
     *  ProfileEntryReceiver.changedProfileEntry()} is invoked when
     *  the specified profile entry is changed.
     *
     *  @param profileEntryId the {@code Id} of the {@code ProfileEntry} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code profileEntryId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedProfileEntry(org.osid.id.Id profileEntryId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted profile entries. {@code
     *  ProfileEntryReceiver.deletedProfileEntry()} is invoked when a
     *  profile entry is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedProfileEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of deleted profileEntries for the
     *  given resource {@code Id}. {@code
     *  ProfileEntryReceiver.deletedProfileEntry()} is invoked when a
     *  {@code ProfileEntry} is deleted or removed from this profile.
     *
     *  @param  resourceId the {@code Id} of the resource to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */
      
    @OSID @Override
    public void registerForDeletedProfileEntriesForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of deleted profileEntries for the
     *  given profile item {@code Id}. {@code
     *  ProfileEntryReceiver.deletedProfileEntry()} is invoked when a
     *  {@code ProfileEntry} is deleted or removed from this profile.
     *
     *  @param  profileItemId the {@code Id} of the profile item to monitor
     *  @throws org.osid.NullArgumentException {@code profileItemId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForDeletedProfileEntriesForProfileItem(org.osid.id.Id profileItemId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of a deleted profile entry. {@code
     *  ProfileEntryReceiver.deletedProfileEntry()} is invoked when
     *  the specified profile entry is deleted.
     *
     *  @param profileEntryId the {@code Id} of the
     *          {@code ProfileEntry} to monitor
     *  @throws org.osid.NullArgumentException {@code profileEntryId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedProfileEntry(org.osid.id.Id profileEntryId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
