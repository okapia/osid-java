//
// AbstractRealmLookupSession.java
//
//    A starter implementation framework for providing a Realm
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.personnel.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A starter implementation framework for providing a Realm
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getRealms(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractRealmLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.personnel.RealmLookupSession {

    private boolean pedantic = false;
    private org.osid.personnel.Realm realm = new net.okapia.osid.jamocha.nil.personnel.realm.UnknownRealm();
    

    /**
     *  Tests if this user can perform <code>Realm</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRealms() {
        return (true);
    }


    /**
     *  A complete view of the <code>Realm</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRealmView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Realm</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRealmView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }

     
    /**
     *  Gets the <code>Realm</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Realm</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Realm</code> and
     *  retained for compatibility.
     *
     *  @param  realmId <code>Id</code> of the
     *          <code>Realm</code>
     *  @return the realm
     *  @throws org.osid.NotFoundException <code>realmId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>realmId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.Realm getRealm(org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.personnel.RealmList realms = getRealms()) {
            while (realms.hasNext()) {
                org.osid.personnel.Realm realm = realms.getNextRealm();
                if (realm.getId().equals(realmId)) {
                    return (realm);
                }
            }
        } 

        throw new org.osid.NotFoundException(realmId + " not found");
    }


    /**
     *  Gets a <code>RealmList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  realms specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Realms</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getRealms()</code>.
     *
     *  @param  realmIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Realm</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>realmIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.RealmList getRealmsByIds(org.osid.id.IdList realmIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.personnel.Realm> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = realmIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getRealm(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("realm " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.personnel.realm.LinkedRealmList(ret));
    }


    /**
     *  Gets a <code>RealmList</code> corresponding to the given
     *  realm genus <code>Type</code> which does not include
     *  realms of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  realms or an error results. Otherwise, the returned list
     *  may contain only those realms that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getRealms()</code>.
     *
     *  @param  realmGenusType a realm genus type 
     *  @return the returned <code>Realm</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>realmGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.RealmList getRealmsByGenusType(org.osid.type.Type realmGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.personnel.realm.RealmGenusFilterList(getRealms(), realmGenusType));
    }


    /**
     *  Gets a <code>RealmList</code> corresponding to the given
     *  realm genus <code>Type</code> and include any additional
     *  realms with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  realms or an error results. Otherwise, the returned list
     *  may contain only those realms that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getRealms()</code>.
     *
     *  @param  realmGenusType a realm genus type 
     *  @return the returned <code>Realm</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>realmGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.RealmList getRealmsByParentGenusType(org.osid.type.Type realmGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getRealmsByGenusType(realmGenusType));
    }


    /**
     *  Gets a <code>RealmList</code> containing the given
     *  realm record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  realms or an error results. Otherwise, the returned list
     *  may contain only those realms that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getRealms()</code>.
     *
     *  @param  realmRecordType a realm record type 
     *  @return the returned <code>Realm</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>realmRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.RealmList getRealmsByRecordType(org.osid.type.Type realmRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.personnel.realm.RealmRecordFilterList(getRealms(), realmRecordType));
    }


    /**
     *  Gets a <code>RealmList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known realms or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  realms that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Realm</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.personnel.RealmList getRealmsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.personnel.realm.RealmProviderFilterList(getRealms(), resourceId));
    }


    /**
     *  Gets all <code>Realms</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  realms or an error results. Otherwise, the returned list
     *  may contain only those realms that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Realms</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.personnel.RealmList getRealms()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the realm list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of realms
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.personnel.RealmList filterRealmsOnViews(org.osid.personnel.RealmList list)
        throws org.osid.OperationFailedException {

        return (list);
    }
}
