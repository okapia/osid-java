//
// AbstractEndpoint.java
//
//     Defines an Endpoint.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.transport.endpoint.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Endpoint</code>.
 */

public abstract class AbstractEndpoint
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalog
    implements org.osid.transport.Endpoint {

    private final java.util.Collection<org.osid.transport.records.EndpointRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Tests if this endpoint supports the given record
     *  <code>Type</code>.
     *
     *  @param  endpointRecordType an endpoint record type 
     *  @return <code>true</code> if the endpointRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>endpointRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type endpointRecordType) {
        for (org.osid.transport.records.EndpointRecord record : this.records) {
            if (record.implementsRecordType(endpointRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Endpoint</code> record <code>Type</code>.
     *
     *  @param  endpointRecordType the endpoint record type 
     *  @return the endpoint record 
     *  @throws org.osid.NullArgumentException
     *          <code>endpointRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(endpointRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.transport.records.EndpointRecord getEndpointRecord(org.osid.type.Type endpointRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.transport.records.EndpointRecord record : this.records) {
            if (record.implementsRecordType(endpointRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(endpointRecordType + " is not supported");
    }


    /**
     *  Adds a record to this endpoint. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param endpointRecord the endpoint record
     *  @param endpointRecordType endpoint record type
     *  @throws org.osid.NullArgumentException
     *          <code>endpointRecord</code> or
     *          <code>endpointRecordTypeendpoint</code> is
     *          <code>null</code>
     */
            
    protected void addEndpointRecord(org.osid.transport.records.EndpointRecord endpointRecord, 
                                     org.osid.type.Type endpointRecordType) {

        nullarg(endpointRecord, "endpoint record");
        addRecordType(endpointRecordType);
        this.records.add(endpointRecord);
        
        return;
    }
}
