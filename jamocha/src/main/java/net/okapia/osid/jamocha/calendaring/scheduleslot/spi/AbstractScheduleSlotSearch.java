//
// AbstractScheduleSlotSearch.java
//
//     A template for making a ScheduleSlot Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.scheduleslot.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing schedule slot searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractScheduleSlotSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.calendaring.ScheduleSlotSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.calendaring.records.ScheduleSlotSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.calendaring.ScheduleSlotSearchOrder scheduleSlotSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of schedule slots. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  scheduleSlotIds list of schedule slots
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleSlotIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongScheduleSlots(org.osid.id.IdList scheduleSlotIds) {
        while (scheduleSlotIds.hasNext()) {
            try {
                this.ids.add(scheduleSlotIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongScheduleSlots</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of schedule slot Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getScheduleSlotIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  scheduleSlotSearchOrder schedule slot search order 
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleSlotSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>scheduleSlotSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderScheduleSlotResults(org.osid.calendaring.ScheduleSlotSearchOrder scheduleSlotSearchOrder) {
	this.scheduleSlotSearchOrder = scheduleSlotSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.calendaring.ScheduleSlotSearchOrder getScheduleSlotSearchOrder() {
	return (this.scheduleSlotSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given schedule slot search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a schedule slot implementing the requested record.
     *
     *  @param scheduleSlotSearchRecordType a schedule slot search record
     *         type
     *  @return the schedule slot search record
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleSlotSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(scheduleSlotSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.ScheduleSlotSearchRecord getScheduleSlotSearchRecord(org.osid.type.Type scheduleSlotSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.calendaring.records.ScheduleSlotSearchRecord record : this.records) {
            if (record.implementsRecordType(scheduleSlotSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(scheduleSlotSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this schedule slot search. 
     *
     *  @param scheduleSlotSearchRecord schedule slot search record
     *  @param scheduleSlotSearchRecordType scheduleSlot search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addScheduleSlotSearchRecord(org.osid.calendaring.records.ScheduleSlotSearchRecord scheduleSlotSearchRecord, 
                                           org.osid.type.Type scheduleSlotSearchRecordType) {

        addRecordType(scheduleSlotSearchRecordType);
        this.records.add(scheduleSlotSearchRecord);        
        return;
    }
}
