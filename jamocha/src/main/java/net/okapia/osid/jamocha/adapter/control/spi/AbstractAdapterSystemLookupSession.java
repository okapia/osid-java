//
// AbstractAdapterSystemLookupSession.java
//
//    A System lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.control.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A System lookup session adapter.
 */

public abstract class AbstractAdapterSystemLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.control.SystemLookupSession {

    private final org.osid.control.SystemLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterSystemLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterSystemLookupSession(org.osid.control.SystemLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code System} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupSystems() {
        return (this.session.canLookupSystems());
    }


    /**
     *  A complete view of the {@code System} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeSystemView() {
        this.session.useComparativeSystemView();
        return;
    }


    /**
     *  A complete view of the {@code System} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenarySystemView() {
        this.session.usePlenarySystemView();
        return;
    }

     
    /**
     *  Gets the {@code System} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code System} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code System} and
     *  retained for compatibility.
     *
     *  @param systemId {@code Id} of the {@code System}
     *  @return the system
     *  @throws org.osid.NotFoundException {@code systemId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code systemId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.System getSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSystem(systemId));
    }


    /**
     *  Gets a {@code SystemList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  systems specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Systems} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  systemIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code System} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code systemIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SystemList getSystemsByIds(org.osid.id.IdList systemIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSystemsByIds(systemIds));
    }


    /**
     *  Gets a {@code SystemList} corresponding to the given
     *  system genus {@code Type} which does not include
     *  systems of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  systems or an error results. Otherwise, the returned list
     *  may contain only those systems that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  systemGenusType a system genus type 
     *  @return the returned {@code System} list
     *  @throws org.osid.NullArgumentException
     *          {@code systemGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SystemList getSystemsByGenusType(org.osid.type.Type systemGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSystemsByGenusType(systemGenusType));
    }


    /**
     *  Gets a {@code SystemList} corresponding to the given
     *  system genus {@code Type} and include any additional
     *  systems with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  systems or an error results. Otherwise, the returned list
     *  may contain only those systems that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  systemGenusType a system genus type 
     *  @return the returned {@code System} list
     *  @throws org.osid.NullArgumentException
     *          {@code systemGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SystemList getSystemsByParentGenusType(org.osid.type.Type systemGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSystemsByParentGenusType(systemGenusType));
    }


    /**
     *  Gets a {@code SystemList} containing the given
     *  system record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  systems or an error results. Otherwise, the returned list
     *  may contain only those systems that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  systemRecordType a system record type 
     *  @return the returned {@code System} list
     *  @throws org.osid.NullArgumentException
     *          {@code systemRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SystemList getSystemsByRecordType(org.osid.type.Type systemRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSystemsByRecordType(systemRecordType));
    }


    /**
     *  Gets a {@code SystemList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  systems or an error results. Otherwise, the returned list
     *  may contain only those systems that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code System} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.control.SystemList getSystemsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSystemsByProvider(resourceId));
    }


    /**
     *  Gets all {@code Systems}. 
     *
     *  In plenary mode, the returned list contains all known
     *  systems or an error results. Otherwise, the returned list
     *  may contain only those systems that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Systems} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SystemList getSystems()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSystems());
    }
}
