//
// InvariantMapProxyBlockLookupSession
//
//    Implements a Block lookup service backed by a fixed
//    collection of blocks. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.hold;


/**
 *  Implements a Block lookup service backed by a fixed
 *  collection of blocks. The blocks are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyBlockLookupSession
    extends net.okapia.osid.jamocha.core.hold.spi.AbstractMapBlockLookupSession
    implements org.osid.hold.BlockLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyBlockLookupSession} with no
     *  blocks.
     *
     *  @param oubliette the oubliette
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code oubliette} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyBlockLookupSession(org.osid.hold.Oubliette oubliette,
                                                  org.osid.proxy.Proxy proxy) {
        setOubliette(oubliette);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyBlockLookupSession} with a single
     *  block.
     *
     *  @param oubliette the oubliette
     *  @param block a single block
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code oubliette},
     *          {@code block} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyBlockLookupSession(org.osid.hold.Oubliette oubliette,
                                                  org.osid.hold.Block block, org.osid.proxy.Proxy proxy) {

        this(oubliette, proxy);
        putBlock(block);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyBlockLookupSession} using
     *  an array of blocks.
     *
     *  @param oubliette the oubliette
     *  @param blocks an array of blocks
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code oubliette},
     *          {@code blocks} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyBlockLookupSession(org.osid.hold.Oubliette oubliette,
                                                  org.osid.hold.Block[] blocks, org.osid.proxy.Proxy proxy) {

        this(oubliette, proxy);
        putBlocks(blocks);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyBlockLookupSession} using a
     *  collection of blocks.
     *
     *  @param oubliette the oubliette
     *  @param blocks a collection of blocks
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code oubliette},
     *          {@code blocks} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyBlockLookupSession(org.osid.hold.Oubliette oubliette,
                                                  java.util.Collection<? extends org.osid.hold.Block> blocks,
                                                  org.osid.proxy.Proxy proxy) {

        this(oubliette, proxy);
        putBlocks(blocks);
        return;
    }
}
