//
// AbstractFederatingReceiptLookupSession.java
//
//     An abstract federating adapter for a ReceiptLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.messaging.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  ReceiptLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingReceiptLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.messaging.ReceiptLookupSession>
    implements org.osid.messaging.ReceiptLookupSession {

    private boolean parallel = false;
    private org.osid.messaging.Mailbox mailbox = new net.okapia.osid.jamocha.nil.messaging.mailbox.UnknownMailbox();


    /**
     *  Constructs a new <code>AbstractFederatingReceiptLookupSession</code>.
     */

    protected AbstractFederatingReceiptLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.messaging.ReceiptLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Mailbox/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Mailbox Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getMailboxId() {
        return (this.mailbox.getId());
    }


    /**
     *  Gets the <code>Mailbox</code> associated with this 
     *  session.
     *
     *  @return the <code>Mailbox</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.Mailbox getMailbox()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.mailbox);
    }


    /**
     *  Sets the <code>Mailbox</code>.
     *
     *  @param  mailbox the mailbox for this session
     *  @throws org.osid.NullArgumentException <code>mailbox</code>
     *          is <code>null</code>
     */

    protected void setMailbox(org.osid.messaging.Mailbox mailbox) {
        nullarg(mailbox, "mailbox");
        this.mailbox = mailbox;
        return;
    }


    /**
     *  Tests if this user can perform <code>Receipt</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupReceipts() {
        for (org.osid.messaging.ReceiptLookupSession session : getSessions()) {
            if (session.canLookupReceipts()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Receipt</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeReceiptView() {
        for (org.osid.messaging.ReceiptLookupSession session : getSessions()) {
            session.useComparativeReceiptView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Receipt</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryReceiptView() {
        for (org.osid.messaging.ReceiptLookupSession session : getSessions()) {
            session.usePlenaryReceiptView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include receipts in mailboxes which are children
     *  of this mailbox in the mailbox hierarchy.
     */

    @OSID @Override
    public void useFederatedMailboxView() {
        for (org.osid.messaging.ReceiptLookupSession session : getSessions()) {
            session.useFederatedMailboxView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this mailbox only.
     */

    @OSID @Override
    public void useIsolatedMailboxView() {
        for (org.osid.messaging.ReceiptLookupSession session : getSessions()) {
            session.useIsolatedMailboxView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Receipt</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Receipt</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Receipt</code> and
     *  retained for compatibility.
     *
     *  @param  receiptId <code>Id</code> of the
     *          <code>Receipt</code>
     *  @return the receipt
     *  @throws org.osid.NotFoundException <code>receiptId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>receiptId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.Receipt getReceipt(org.osid.id.Id receiptId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.messaging.ReceiptLookupSession session : getSessions()) {
            try {
                return (session.getReceipt(receiptId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(receiptId + " not found");
    }


    /**
     *  Gets a <code>ReceiptList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  receipts specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Receipts</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  receiptIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Receipt</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>receiptIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptList getReceiptsByIds(org.osid.id.IdList receiptIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.messaging.receipt.MutableReceiptList ret = new net.okapia.osid.jamocha.messaging.receipt.MutableReceiptList();

        try (org.osid.id.IdList ids = receiptIds) {
            while (ids.hasNext()) {
                ret.addReceipt(getReceipt(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>ReceiptList</code> corresponding to the given
     *  receipt genus <code>Type</code> which does not include
     *  receipts of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  receipts or an error results. Otherwise, the returned list
     *  may contain only those receipts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  receiptGenusType a receipt genus type 
     *  @return the returned <code>Receipt</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>receiptGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptList getReceiptsByGenusType(org.osid.type.Type receiptGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.messaging.receipt.FederatingReceiptList ret = getReceiptList();

        for (org.osid.messaging.ReceiptLookupSession session : getSessions()) {
            ret.addReceiptList(session.getReceiptsByGenusType(receiptGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ReceiptList</code> corresponding to the given
     *  receipt genus <code>Type</code> and include any additional
     *  receipts with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  receipts or an error results. Otherwise, the returned list
     *  may contain only those receipts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  receiptGenusType a receipt genus type 
     *  @return the returned <code>Receipt</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>receiptGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptList getReceiptsByParentGenusType(org.osid.type.Type receiptGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.messaging.receipt.FederatingReceiptList ret = getReceiptList();

        for (org.osid.messaging.ReceiptLookupSession session : getSessions()) {
            ret.addReceiptList(session.getReceiptsByParentGenusType(receiptGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ReceiptList</code> containing the given
     *  receipt record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  receipts or an error results. Otherwise, the returned list
     *  may contain only those receipts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  receiptRecordType a receipt record type 
     *  @return the returned <code>Receipt</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>receiptRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptList getReceiptsByRecordType(org.osid.type.Type receiptRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.messaging.receipt.FederatingReceiptList ret = getReceiptList();

        for (org.osid.messaging.ReceiptLookupSession session : getSessions()) {
            ret.addReceiptList(session.getReceiptsByRecordType(receiptRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ReceiptList</code> for the given message.  In
     *  plenary mode, the returned list contains all known receipts or
     *  an error results. Otherwise, the returned list may contain
     *  only those receipts that are accessible through this session.
     *
     *  @param  messageId a message <code>Id</code>
     *  @return the returned <code>Receipt</code> list
     *  @throws org.osid.NullArgumentException <code>messageId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.messaging.ReceiptList getReceiptsForMessage(org.osid.id.Id messageId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.messaging.receipt.FederatingReceiptList ret = getReceiptList();

        for (org.osid.messaging.ReceiptLookupSession session : getSessions()) {
            ret.addReceiptList(session.getReceiptsForMessage(messageId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ReceiptList</code> received by the specified
     *  recipient.  In plenary mode, the returned list contains all
     *  known receipts or an error results. Otherwise, the returned
     *  list may contain only those receipts that are accessible
     *  through this session.
     *
     *  @param  resourceId a resource Id
     *  @return the returned <code>Receipt</code> list
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.messaging.ReceiptList getReceiptsForRecipient(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.messaging.receipt.FederatingReceiptList ret = getReceiptList();

        for (org.osid.messaging.ReceiptLookupSession session : getSessions()) {
            ret.addReceiptList(session.getReceiptsForRecipient(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of <code>Receipts</code> for the given message and
     *  recipient. In plenary mode, the returned list contains all
     *  known receipts or an error results. Otherwise, the returned
     *  list may contain only those receipts that are accessible
     *  through this session.
     *
     *  @param  messageId a message <code>Id</code>
     *  @param  resourceId a resource <code>Id</code>
     *  @return the returned <code>Receipt</code>
     *  @throws org.osid.NullArgumentException <code>messageId</code>
     *          or <code>recipientId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.messaging.ReceiptList getReceiptsForMessageAndRecipient(org.osid.id.Id messageId,
                                                                            org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.messaging.receipt.FederatingReceiptList ret = getReceiptList();

        for (org.osid.messaging.ReceiptLookupSession session : getSessions()) {
            ret.addReceiptList(session.getReceiptsForMessageAndRecipient(messageId, resourceId));
        }

        ret.noMore();
        return (ret);
    }
    
    
    /**
     *  Gets all <code>Receipts</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  receipts or an error results. Otherwise, the returned list
     *  may contain only those receipts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Receipts</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptList getReceipts()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.messaging.receipt.FederatingReceiptList ret = getReceiptList();

        for (org.osid.messaging.ReceiptLookupSession session : getSessions()) {
            ret.addReceiptList(session.getReceipts());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.messaging.receipt.FederatingReceiptList getReceiptList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.messaging.receipt.ParallelReceiptList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.messaging.receipt.CompositeReceiptList());
        }
    }
}
