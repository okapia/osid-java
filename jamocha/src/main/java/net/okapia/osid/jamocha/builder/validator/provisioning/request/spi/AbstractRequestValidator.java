//
// AbstractRequestValidator.java
//
//     Validates a Request.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.provisioning.request.spi;


/**
 *  Validates a Request.
 */

public abstract class AbstractRequestValidator
    extends net.okapia.osid.jamocha.builder.validator.spi.AbstractOsidRelationshipValidator {


    /**
     *  Constructs a new <code>AbstractRequestValidator</code>.
     */

    protected AbstractRequestValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AbstractRequestValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractRequestValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates a Request.
     *
     *  @param request a request to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>request</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in
     *          assembly
     */
    
    public void validate(org.osid.provisioning.Request request) {
        super.validate(request);

        testNestedObject(request, "getRequestTransaction");
        testNestedObject(request, "getQueue");
        test(request.getRequestDate(), "getRequestDate()");
        testNestedObject(request, "getRequester");
        testNestedObject(request, "getRequestingAgent");

        testConditionalMethod(request, "getPool", request.hasPool(), "hasPool()");
        if (request.hasPool()) {
            testNestedObject(request, "getPool");
        }

        testNestedObjects(request, "getRequestedProvisionableIds", "getRequestedProvisionables");

        testConditionalMethod(request, "getExchangeProvisionId", request.isExchange(), "isExchange()");
        testConditionalMethod(request, "getExchangeProvision", request.isExchange(), "isExchange()");
        if (request.isExchange()) {
            testNestedObject(request, "getExchangeProvision");
        }
        
        testConditionalMethod(request, "getOriginProvisionId", request.isProvisionResult(), "isProvisionResult()");
        testConditionalMethod(request, "getOriginProvision", request.isProvisionResult(), "isProvisionResult()");
        if (request.isProvisionResult()) {
            testNestedObject(request, "getOriginProvision");
        }

        testConditionalMethod(request, "getPosition", request.hasPosition(), "hasPosition()");
        if (request.hasPosition()) {
            testCardinal(request.getPosition(), "getPosition()");
        }
        
        if (request.hasEWA()) {
            test(request.getEWA(), "getEWA()");
        }

        return;
    }
}
