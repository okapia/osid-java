//
// AbstractAvailabilityEnablerQueryInspector.java
//
//     A template for making an AvailabilityEnablerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.rules.availabilityenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for availability enablers.
 */

public abstract class AbstractAvailabilityEnablerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQueryInspector
    implements org.osid.resourcing.rules.AvailabilityEnablerQueryInspector {

    private final java.util.Collection<org.osid.resourcing.rules.records.AvailabilityEnablerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the availability <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledAvailabilityIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the availability query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityQueryInspector[] getRuledAvailabilityTerms() {
        return (new org.osid.resourcing.AvailabilityQueryInspector[0]);
    }


    /**
     *  Gets the foundry <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFoundryIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the foundry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQueryInspector[] getFoundryTerms() {
        return (new org.osid.resourcing.FoundryQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given availability enabler query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an availability enabler implementing the requested record.
     *
     *  @param availabilityEnablerRecordType an availability enabler record type
     *  @return the availability enabler query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>availabilityEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(availabilityEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.rules.records.AvailabilityEnablerQueryInspectorRecord getAvailabilityEnablerQueryInspectorRecord(org.osid.type.Type availabilityEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.rules.records.AvailabilityEnablerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(availabilityEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(availabilityEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this availability enabler query. 
     *
     *  @param availabilityEnablerQueryInspectorRecord availability enabler query inspector
     *         record
     *  @param availabilityEnablerRecordType availabilityEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAvailabilityEnablerQueryInspectorRecord(org.osid.resourcing.rules.records.AvailabilityEnablerQueryInspectorRecord availabilityEnablerQueryInspectorRecord, 
                                                   org.osid.type.Type availabilityEnablerRecordType) {

        addRecordType(availabilityEnablerRecordType);
        nullarg(availabilityEnablerRecordType, "availability enabler record type");
        this.records.add(availabilityEnablerQueryInspectorRecord);        
        return;
    }
}
