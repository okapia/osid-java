//
// AbstractCommitmentEnablerQueryInspector.java
//
//     A template for making a CommitmentEnablerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.rules.commitmentenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for commitment enablers.
 */

public abstract class AbstractCommitmentEnablerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQueryInspector
    implements org.osid.calendaring.rules.CommitmentEnablerQueryInspector {

    private final java.util.Collection<org.osid.calendaring.rules.records.CommitmentEnablerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the commitment <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledCommitmentIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the commitment query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentQueryInspector[] getRuledCommitmentTerms() {
        return (new org.osid.calendaring.CommitmentQueryInspector[0]);
    }


    /**
     *  Gets the calendar <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCalendarIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the calendar query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQueryInspector[] getCalendarTerms() {
        return (new org.osid.calendaring.CalendarQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given commitment enabler query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a commitment enabler implementing the requested record.
     *
     *  @param commitmentEnablerRecordType a commitment enabler record type
     *  @return the commitment enabler query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(commitmentEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.rules.records.CommitmentEnablerQueryInspectorRecord getCommitmentEnablerQueryInspectorRecord(org.osid.type.Type commitmentEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.rules.records.CommitmentEnablerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(commitmentEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(commitmentEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this commitment enabler query. 
     *
     *  @param commitmentEnablerQueryInspectorRecord commitment enabler query inspector
     *         record
     *  @param commitmentEnablerRecordType commitmentEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCommitmentEnablerQueryInspectorRecord(org.osid.calendaring.rules.records.CommitmentEnablerQueryInspectorRecord commitmentEnablerQueryInspectorRecord, 
                                                   org.osid.type.Type commitmentEnablerRecordType) {

        addRecordType(commitmentEnablerRecordType);
        nullarg(commitmentEnablerRecordType, "commitment enabler record type");
        this.records.add(commitmentEnablerQueryInspectorRecord);        
        return;
    }
}
