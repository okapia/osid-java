//
// AbstractPosition.java
//
//     Defines a Position builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.personnel.position.spi;


/**
 *  Defines a <code>Position</code> builder.
 */

public abstract class AbstractPositionBuilder<T extends AbstractPositionBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractTemporalOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.personnel.position.PositionMiter position;


    /**
     *  Constructs a new <code>AbstractPositionBuilder</code>.
     *
     *  @param position the position to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractPositionBuilder(net.okapia.osid.jamocha.builder.personnel.position.PositionMiter position) {
        super(position);
        this.position = position;
        return;
    }


    /**
     *  Builds the position.
     *
     *  @return the new position
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.personnel.Position build() {
        (new net.okapia.osid.jamocha.builder.validator.personnel.position.PositionValidator(getValidations())).validate(this.position);
        return (new net.okapia.osid.jamocha.builder.personnel.position.ImmutablePosition(this.position));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the position miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.personnel.position.PositionMiter getMiter() {
        return (this.position);
    }


    /**
     *  Sets the organization.
     *
     *  @param organization an organization
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>organization</code> is <code>null</code>
     */

    public T organization(org.osid.personnel.Organization organization) {
        getMiter().setOrganization(organization);
        return (self());
    }


    /**
     *  Sets the title.
     *
     *  @param title a title
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>title</code> is
     *          <code>null</code>
     */

    public T title(org.osid.locale.DisplayText title) {
        getMiter().setTitle(title);
        return (self());
    }


    /**
     *  Sets the level.
     *
     *  @param level a level
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>level</code> is
     *          <code>null</code>
     */

    public T level(org.osid.grading.Grade level) {
        getMiter().setLevel(level);
        return (self());
    }


    /**
     *  Adds a qualification.
     *
     *  @param qualification a qualification
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>qualification</code> is <code>null</code>
     */

    public T qualification(org.osid.learning.Objective qualification) {
        getMiter().addQualification(qualification);
        return (self());
    }


    /**
     *  Sets all the qualifications.
     *
     *  @param qualifications a collection of qualifications
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>qualifications</code> is <code>null</code>
     */

    public T qualifications(java.util.Collection<org.osid.learning.Objective> qualifications) {
        getMiter().setQualifications(qualifications);
        return (self());
    }


    /**
     *  Sets the target appointments.
     *
     *  @param appointments a target number ofappointments
     *  @return the builder
     *  @throws org.osid.InvalidArgumentException
     *          <code>appointment</code> is negative
     */

    public T targetAppointments(long appointments) {
        getMiter().setTargetAppointments(appointments);
        return (self());
    }


    /**
     *  Sets the required commitment.
     *
     *  @param percentage a required commitment
     *  @return the builder
     *  @throws org.osid.InvalidArgumentException
     *          <code>percentage</code> is out of range
     */

    public T requiredCommitment(long percentage) {
        getMiter().setRequiredCommitment(percentage);
        return (self());
    }


    /**
     *  Sets the low salary range.
     *
     *  @param salary a low salary range
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>salary</code> is
     *          <code>null</code>
     */

    public T lowSalaryRange(org.osid.financials.Currency salary) {
        getMiter().setLowSalaryRange(salary);
        return (self());
    }


    /**
     *  Sets the midpoint salary range.
     *
     *  @param salary a midpoint salary range
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>salary</code> is
     *          <code>null</code>
     */

    public T midpointSalaryRange(org.osid.financials.Currency salary) {
        getMiter().setMidpointSalaryRange(salary);
        return (self());
    }


    /**
     *  Sets the high salary range.
     *
     *  @param salary a high salary range
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>salary</code> is
     *          <code>null</code>
     */

    public T highSalaryRange(org.osid.financials.Currency salary) {
        getMiter().setHighSalaryRange(salary);
        return (self());
    }


    /**
     *  Sets the compensation frequency.
     *
     *  @param frequency a compensation frequency
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>frequency</code>
     *          is <code>null</code>
     */

    public T compensationFrequency(org.osid.calendaring.Duration frequency) {
        getMiter().setCompensationFrequency(frequency);
        return (self());
    }

    
    /**
     *  Sets the exempt flag.
     *
     *  @return the builder
     */

    public T exempt() {
        getMiter().setExempt(true);
        return (self());
    }


    /**
     *  Unsets the exempt flag.
     *
     *  @return the builder
     */

    public T nonExempt() {
        getMiter().setExempt(false);
        return (self());
    }


    /**
     *  Sets the benefits type.
     *
     *  @param benefitsType a benefits type
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>benefitsType</code> is <code>null</code>
     */

    public T benefitsType(org.osid.type.Type benefitsType) {
        getMiter().setBenefitsType(benefitsType);
        return (self());
    }


    /**
     *  Adds a Position record.
     *
     *  @param record a position record
     *  @param recordType the type of position record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.personnel.records.PositionRecord record, org.osid.type.Type recordType) {
        getMiter().addPositionRecord(record, recordType);
        return (self());
    }
}       


