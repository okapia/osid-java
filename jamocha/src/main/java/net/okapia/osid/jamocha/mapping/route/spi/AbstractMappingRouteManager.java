//
// AbstractMappingRouteManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.route.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractMappingRouteManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.mapping.route.MappingRouteManager,
               org.osid.mapping.route.MappingRouteProxyManager {

    private final Types routeRecordTypes                   = new TypeRefSet();
    private final Types routeSearchRecordTypes             = new TypeRefSet();

    private final Types routeSegmentRecordTypes            = new TypeRefSet();
    private final Types routeProgressRecordTypes           = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractMappingRouteManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractMappingRouteManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any map federation is exposed. Federation is exposed when a 
     *  specific map may be identified, selected and used to create a lookup 
     *  or admin session. Federation is not exposed when a set of maps appears 
     *  as a single map. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if a location routing service is supported. 
     *
     *  @return <code> true </code> if a location routing service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationRouting() {
        return (false);
    }


    /**
     *  Tests if a location service is supported for the current agent. 
     *
     *  @return <code> true </code> if my location is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMyLocation() {
        return (false);
    }


    /**
     *  Tests if looking up routes is supported. 
     *
     *  @return <code> true </code> if route lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRouteLookup() {
        return (false);
    }


    /**
     *  Tests if querying routes is supported. 
     *
     *  @return <code> true </code> if route query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRouteQuery() {
        return (false);
    }


    /**
     *  Tests if searching routes is supported. 
     *
     *  @return <code> true </code> if route search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRouteSearch() {
        return (false);
    }


    /**
     *  Tests if a route <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if route administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRouteAdmin() {
        return (false);
    }


    /**
     *  Tests if a route <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if route notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRouteNotification() {
        return (false);
    }


    /**
     *  Tests if a route map lookup service is supported. 
     *
     *  @return <code> true </code> if a route map lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRouteMap() {
        return (false);
    }


    /**
     *  Tests if a route map service is supported. 
     *
     *  @return <code> true </code> if a route to map assignment service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRouteMapAssignment() {
        return (false);
    }


    /**
     *  Tests if a route smart map service is supported. 
     *
     *  @return <code> true </code> if a route smart map lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRouteSmartMap() {
        return (false);
    }


    /**
     *  Tests if a resource route service is supported. 
     *
     *  @return <code> true </code> if a resource route service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceRoute() {
        return (false);
    }


    /**
     *  Tests if a resource route update service is supported. 
     *
     *  @return <code> true </code> if a resource route update service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceRouteAssignment() {
        return (false);
    }


    /**
     *  Tests if a resource route notification service is supported. 
     *
     *  @return <code> true </code> if a resource route notification service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceRouteNotification() {
        return (false);
    }


    /**
     *  Tests if a route service is supported for the current agent. 
     *
     *  @return <code> true </code> if my route is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMyRoute() {
        return (false);
    }


    /**
     *  Gets the supported <code> Route </code> record types. 
     *
     *  @return a list containing the supported <code> Route </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRouteRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.routeRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Route </code> record type is supported. 
     *
     *  @param  routeRecordType a <code> Type </code> indicating a <code> 
     *          Route </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> routeRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRouteRecordType(org.osid.type.Type routeRecordType) {
        return (this.routeRecordTypes.contains(routeRecordType));
    }


    /**
     *  Adds support for a route record type.
     *
     *  @param routeRecordType a route record type
     *  @throws org.osid.NullArgumentException
     *  <code>routeRecordType</code> is <code>null</code>
     */

    protected void addRouteRecordType(org.osid.type.Type routeRecordType) {
        this.routeRecordTypes.add(routeRecordType);
        return;
    }


    /**
     *  Removes support for a route record type.
     *
     *  @param routeRecordType a route record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>routeRecordType</code> is <code>null</code>
     */

    protected void removeRouteRecordType(org.osid.type.Type routeRecordType) {
        this.routeRecordTypes.remove(routeRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Route </code> search record types. 
     *
     *  @return a list containing the supported <code> Route </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRouteSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.routeSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Route </code> search record type is 
     *  supported. 
     *
     *  @param  routeSearchRecordType a <code> Type </code> indicating a 
     *          <code> Route </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> routeSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRouteSearchRecordType(org.osid.type.Type routeSearchRecordType) {
        return (this.routeSearchRecordTypes.contains(routeSearchRecordType));
    }


    /**
     *  Adds support for a route search record type.
     *
     *  @param routeSearchRecordType a route search record type
     *  @throws org.osid.NullArgumentException
     *  <code>routeSearchRecordType</code> is <code>null</code>
     */

    protected void addRouteSearchRecordType(org.osid.type.Type routeSearchRecordType) {
        this.routeSearchRecordTypes.add(routeSearchRecordType);
        return;
    }


    /**
     *  Removes support for a route search record type.
     *
     *  @param routeSearchRecordType a route search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>routeSearchRecordType</code> is <code>null</code>
     */

    protected void removeRouteSearchRecordType(org.osid.type.Type routeSearchRecordType) {
        this.routeSearchRecordTypes.remove(routeSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> RouteSegment </code> record types. 
     *
     *  @return a list containing the supported <code> RouteSegment </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRouteSegmentRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.routeSegmentRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> RouteSegment </code> record type is 
     *  supported. 
     *
     *  @param  routeSegmentRecordType a <code> Type </code> indicating a 
     *          <code> RouteSegment </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          supportsRouteSegmentRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRouteSegmentRecordType(org.osid.type.Type routeSegmentRecordType) {
        return (this.routeSegmentRecordTypes.contains(routeSegmentRecordType));
    }


    /**
     *  Adds support for a route segment record type.
     *
     *  @param routeSegmentRecordType a route segment record type
     *  @throws org.osid.NullArgumentException
     *  <code>routeSegmentRecordType</code> is <code>null</code>
     */

    protected void addRouteSegmentRecordType(org.osid.type.Type routeSegmentRecordType) {
        this.routeSegmentRecordTypes.add(routeSegmentRecordType);
        return;
    }


    /**
     *  Removes support for a route segment record type.
     *
     *  @param routeSegmentRecordType a route segment record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>routeSegmentRecordType</code> is <code>null</code>
     */

    protected void removeRouteSegmentRecordType(org.osid.type.Type routeSegmentRecordType) {
        this.routeSegmentRecordTypes.remove(routeSegmentRecordType);
        return;
    }


    /**
     *  Gets the supported <code> RouteProgress </code> record types. 
     *
     *  @return a list containing the supported <code> RouteProgress </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRouteProgressRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.routeProgressRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> RouteProgress </code> record type is 
     *  supported. 
     *
     *  @param  routeProgressRecordType a <code> Type </code> indicating a 
     *          <code> RouteProgress </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          supportsRouteProgressRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRouteProgressRecordType(org.osid.type.Type routeProgressRecordType) {
        return (this.routeProgressRecordTypes.contains(routeProgressRecordType));
    }


    /**
     *  Adds support for a route progress record type.
     *
     *  @param routeProgressRecordType a route progress record type
     *  @throws org.osid.NullArgumentException
     *  <code>routeProgressRecordType</code> is <code>null</code>
     */

    protected void addRouteProgressRecordType(org.osid.type.Type routeProgressRecordType) {
        this.routeProgressRecordTypes.add(routeProgressRecordType);
        return;
    }


    /**
     *  Removes support for a route progress record type.
     *
     *  @param routeProgressRecordType a route progress record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>routeProgressRecordType</code> is <code>null</code>
     */

    protected void removeRouteProgressRecordType(org.osid.type.Type routeProgressRecordType) {
        this.routeProgressRecordTypes.remove(routeProgressRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  routing service. 
     *
     *  @return a <code> RoutingSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRouting() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RoutingSession getRoutingSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteManager.getRoutingSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  routing service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RoutingSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRouting() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RoutingSession getRoutingSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteProxyManager.getRoutingSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  routing service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> RoutingSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRouting() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RoutingSession getRoutingSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteManager.getRoutingSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  routing service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RoutingSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRouting() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RoutingSession getRoutingSessionForMap(org.osid.id.Id mapId, 
                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteProxyManager.getRoutingSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the route lookup 
     *  service. 
     *
     *  @return a <code> RouteLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRouteLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteLookupSession getRouteLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteManager.getRouteLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the route lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RouteLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRouteLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteLookupSession getRouteLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteProxyManager.getRouteLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the route lookup 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the map 
     *  @return a <code> RouteLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRouteLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteLookupSession getRouteLookupSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteManager.getRouteLookupSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the route lookup 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the map 
     *  @param  proxy a proxy 
     *  @return a <code> RouteLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRouteLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteLookupSession getRouteLookupSessionForMap(org.osid.id.Id mapId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteProxyManager.getRouteLookupSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the route query 
     *  service. 
     *
     *  @return a <code> RouteQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRouteQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteQuerySession getRouteQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteManager.getRouteQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the route query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RouteQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRouteQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteQuerySession getRouteQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteProxyManager.getRouteQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the route query 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the map 
     *  @return a <code> RouteQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRouteQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteQuerySession getRouteQuerySessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteManager.getRouteQuerySessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the route query 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the map 
     *  @param  proxy a proxy 
     *  @return a <code> RouteQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRouteQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteQuerySession getRouteQuerySessionForMap(org.osid.id.Id mapId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteProxyManager.getRouteQuerySessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the route search 
     *  service. 
     *
     *  @return a <code> RouteSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRouteSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteSearchSession getRouteSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteManager.getRouteSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the route search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RouteSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRouteSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteSearchSession getRouteSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteProxyManager.getRouteSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the route search 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> RouteSearchSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRouteSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteSearchSession getRouteSearchSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteManager.getRouteSearchSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the route search 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RouteSearchSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRouteSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteSearchSession getRouteSearchSessionForMap(org.osid.id.Id mapId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteProxyManager.getRouteSearchSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the route 
     *  administration service. 
     *
     *  @return a <code> RouteAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRouteAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteAdminSession getRouteAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteManager.getRouteAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the route 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RouteAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRouteAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteAdminSession getRouteAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteProxyManager.getRouteAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the route 
     *  administration service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> RouteAdminSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRouteAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteAdminSession getRouteAdminSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteManager.getRouteAdminSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the route 
     *  administration service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RouteAdminSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRouteAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteAdminSession getRouteAdminSessionForMap(org.osid.id.Id mapId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteProxyManager.getRouteAdminSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the route 
     *  notification service. 
     *
     *  @param  routeReceiver the notification callback 
     *  @return a <code> RouteNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> routeReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRouteNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteNotificationSession getRouteNotificationSession(org.osid.mapping.route.RouteReceiver routeReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteManager.getRouteNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the route 
     *  notification service. 
     *
     *  @param  routeReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> RouteNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> routeReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRouteNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteNotificationSession getRouteNotificationSession(org.osid.mapping.route.RouteReceiver routeReceiver, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteProxyManager.getRouteNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the route 
     *  notification service for the given map. 
     *
     *  @param  routeReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> RouteNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> routeReceiver </code> or 
     *          <code> mapId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRouteNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteNotificationSession getRouteNotificationSessionForMap(org.osid.mapping.route.RouteReceiver routeReceiver, 
                                                                                             org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteManager.getRouteNotificationSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the route 
     *  notification service for the given map. 
     *
     *  @param  routeReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RouteNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> routeReceiver, mapId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRouteNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteNotificationSession getRouteNotificationSessionForMap(org.osid.mapping.route.RouteReceiver routeReceiver, 
                                                                                             org.osid.id.Id mapId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteProxyManager.getRouteNotificationSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup route/map mappings. 
     *
     *  @return a <code> RouteMapSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRouteMap() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteMapSession getRouteMapSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteManager.getRouteMapSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup route/map mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RouteMapSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRouteMap() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteMapSession getRouteMapSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteProxyManager.getRouteMapSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning routes 
     *  to maps. 
     *
     *  @return a <code> LocationMapAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRouteMapAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteMapAssignmentSession getRouteMapAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteManager.getRouteMapAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning routes 
     *  to maps. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LocationMapAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRouteMapAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteMapAssignmentSession getRouteMapAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteProxyManager.getRouteMapAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage route smart maps. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> RouteSmartMapSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRouteSmartMap() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteSmartMapSession getRouteSmartMapSession(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteManager.getRouteSmartMapSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage route smart maps. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RouteSmartMapSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRouteSmartMap() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteSmartMapSession getRouteSmartMapSession(org.osid.id.Id mapId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteProxyManager.getRouteSmartMapSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource route 
     *  service. 
     *
     *  @return a <code> ResourceRouteSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResourceRoute() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.ResourceRouteSession getResourceRouteSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteManager.getResourceRouteSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource route 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ResourceRouteSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResourceRoute() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.ResourceRouteSession getResourceRouteSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteProxyManager.getResourceRouteSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource route 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> ResourceRouteSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResourceRoute() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.ResourceRouteSession getResourceRouteSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteManager.getResourceRouteSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource route 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ResourceRouteSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResourceRoute() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.ResourceRouteSession getResourceRouteSessionForMap(org.osid.id.Id mapId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteProxyManager.getResourceRouteSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource route 
     *  assignment service. 
     *
     *  @return a <code> ResourceRouteAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRouteAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.ResourceRouteAssignmentSession getResourceRouteAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteManager.getResourceRouteAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource route 
     *  assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ResourceRouteAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRouteAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.ResourceRouteAssignmentSession getResourceRouteAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteProxyManager.getResourceRouteAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource route 
     *  assignment service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> ResourceRouteAssignmentSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRouteAssignment() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.ResourceRouteAssignmentSession getResourceRouteAssignmentSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteManager.getResourceRouteAssignmentSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource route 
     *  assignment service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ResourceRouteAssignmentSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRouteAssignment() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.ResourceRouteAssignmentSession getResourceRouteAssignmentSessionForMap(org.osid.id.Id mapId, 
                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteProxyManager.getResourceRouteAssignmentSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource route 
     *  notification service. 
     *
     *  @param  resourceRouteReceiver the notification callback 
     *  @return a <code> ResourceRouteNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> resourceRouteReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRouteNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.ResourceRouteNotificationSession getResourceRouteNotificationSession(org.osid.mapping.route.ResourceRouteReceiver resourceRouteReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteManager.getResourceRouteNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource route 
     *  notification service. 
     *
     *  @param  resourceRouteReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> ResourceRouteNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> resourceRouteReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRouteNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.ResourceRouteNotificationSession getResourceRouteNotificationSession(org.osid.mapping.route.ResourceRouteReceiver resourceRouteReceiver, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteProxyManager.getResourceRouteNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource route 
     *  notification service for the given map. 
     *
     *  @param  resourceRouteReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> ResourceRouteNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> resourceRouteReceiver 
     *          </code> or <code> mapId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRouteNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.ResourceRouteNotificationSession getResourceRouteNotificationSessionForMap(org.osid.mapping.route.ResourceRouteReceiver resourceRouteReceiver, 
                                                                                                             org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteManager.getResourceRouteNotificationSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource route 
     *  notification service for the given map. 
     *
     *  @param  resourceRouteReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ResourceRouteNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> resourceRouteReceiver, 
     *          mapId </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRouteNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.ResourceRouteNotificationSession getResourceRouteNotificationSessionForMap(org.osid.mapping.route.ResourceRouteReceiver resourceRouteReceiver, 
                                                                                                             org.osid.id.Id mapId, 
                                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteProxyManager.getResourceRouteNotificationSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my route 
     *  service. 
     *
     *  @return a <code> MyRouteLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyRouteLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.MyRouteSession getMyRouteSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteManager.getMyRouteSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my route 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MyRouteLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyRouteLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.MyRouteSession getMyRouteSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteProxyManager.getMyRouteSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my route 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the map 
     *  @return a <code> MyRouteLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyRouteLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.MyRouteSession getMyRouteSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteManager.getMyRouteSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my route 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the map 
     *  @param  proxy a proxy 
     *  @return a <code> MyRouteLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyRouteLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.MyRouteSession getMyRouteSessionForMap(org.osid.id.Id mapId, 
                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.route.MappingRouteProxyManager.getMyRouteSessionForMap not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.routeRecordTypes.clear();
        this.routeRecordTypes.clear();

        this.routeSearchRecordTypes.clear();
        this.routeSearchRecordTypes.clear();

        this.routeSegmentRecordTypes.clear();
        this.routeProgressRecordTypes.clear();

        return;
    }
}
