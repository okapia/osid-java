//
// AbstractRoomBatchManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractRoomBatchManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.room.batch.RoomBatchManager,
               org.osid.room.batch.RoomBatchProxyManager {


    /**
     *  Constructs a new <code>AbstractRoomBatchManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractRoomBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is
     *          supported, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if bulk administration of rooms is available. 
     *
     *  @return <code> true </code> if a room bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRoomBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of floors is available. 
     *
     *  @return <code> true </code> if a floor bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFloorBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of buildings is available. 
     *
     *  @return <code> true </code> if a building bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBuildingBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of campuses is available. 
     *
     *  @return <code> true </code> if an campus bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCampusBatchAdmin() {
        return (false);
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk room 
     *  administration service. 
     *
     *  @return a <code> RoomBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRoomBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.batch.RoomBatchAdminSession getRoomBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.batch.RoomBatchManager.getRoomBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk room 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RoomBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRoomBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.batch.RoomBatchAdminSession getRoomBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.batch.RoomBatchProxyManager.getRoomBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk room 
     *  administration service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @return a <code> RoomBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRoomBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.batch.RoomBatchAdminSession getRoomBatchAdminSessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.batch.RoomBatchManager.getRoomBatchAdminSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk room 
     *  administration service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RoomBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRoomBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.batch.RoomBatchAdminSession getRoomBatchAdminSessionForCampus(org.osid.id.Id campusId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.batch.RoomBatchProxyManager.getRoomBatchAdminSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk floor 
     *  administration service. 
     *
     *  @return a <code> FloorBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFloorBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.batch.FloorBatchAdminSession getFloorBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.batch.RoomBatchManager.getFloorBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk room 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FloorBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFloorBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.batch.FloorBatchAdminSession getFloorBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.batch.RoomBatchProxyManager.getFloorBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk floor 
     *  administration service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @return a <code> FloorBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFloorBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.batch.FloorBatchAdminSession getFloorBatchAdminSessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.batch.RoomBatchManager.getFloorBatchAdminSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk floor 
     *  administration service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> FloorBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFloorBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.batch.FloorBatchAdminSession getFloorBatchAdminSessionForCampus(org.osid.id.Id campusId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.batch.RoomBatchProxyManager.getFloorBatchAdminSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk building 
     *  administration service. 
     *
     *  @return a <code> BuildingBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBuildingBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.batch.BuildingBatchAdminSession getBuildingBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.batch.RoomBatchManager.getBuildingBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk building 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BuildingBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBuildingBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.batch.BuildingBatchAdminSession getBuildingBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.batch.RoomBatchProxyManager.getBuildingBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk building 
     *  administration service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @return a <code> BuildingBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBuildingBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.batch.BuildingBatchAdminSession getBuildingBatchAdminSessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.batch.RoomBatchManager.getBuildingBatchAdminSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk building 
     *  administration service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BuildingBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBuildingBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.batch.BuildingBatchAdminSession getBuildingBatchAdminSessionForCampus(org.osid.id.Id campusId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.batch.RoomBatchProxyManager.getBuildingBatchAdminSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk campus 
     *  administration service. 
     *
     *  @return a <code> CampusBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCampusBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.batch.CampusBatchAdminSession getCampusBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.batch.RoomBatchManager.getCampusBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk campus 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CampusBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCampusBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.batch.CampusBatchAdminSession getCampusBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.batch.RoomBatchProxyManager.getCampusBatchAdminSession not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        return;
    }
}
