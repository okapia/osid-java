//
// AbstractQueryPersonLookupSession.java
//
//    An inline adapter that maps a PersonLookupSession to
//    a PersonQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.personnel.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a PersonLookupSession to
 *  a PersonQuerySession.
 */

public abstract class AbstractQueryPersonLookupSession
    extends net.okapia.osid.jamocha.personnel.spi.AbstractPersonLookupSession
    implements org.osid.personnel.PersonLookupSession {

    private final org.osid.personnel.PersonQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryPersonLookupSession.
     *
     *  @param querySession the underlying person query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryPersonLookupSession(org.osid.personnel.PersonQuerySession querySession) {
        nullarg(querySession, "person query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Realm</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Realm Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getRealmId() {
        return (this.session.getRealmId());
    }


    /**
     *  Gets the <code>Realm</code> associated with this 
     *  session.
     *
     *  @return the <code>Realm</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.Realm getRealm()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getRealm());
    }


    /**
     *  Tests if this user can perform <code>Person</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPersons() {
        return (this.session.canSearchPersons());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include persons in realms which are children
     *  of this realm in the realm hierarchy.
     */

    @OSID @Override
    public void useFederatedRealmView() {
        this.session.useFederatedRealmView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this realm only.
     */

    @OSID @Override
    public void useIsolatedRealmView() {
        this.session.useIsolatedRealmView();
        return;
    }
    
     
    /**
     *  Gets the <code>Person</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Person</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Person</code> and
     *  retained for compatibility.
     *
     *  @param  personId <code>Id</code> of the
     *          <code>Person</code>
     *  @return the person
     *  @throws org.osid.NotFoundException <code>personId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>personId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.Person getPerson(org.osid.id.Id personId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.personnel.PersonQuery query = getQuery();
        query.matchId(personId, true);
        org.osid.personnel.PersonList persons = this.session.getPersonsByQuery(query);
        if (persons.hasNext()) {
            return (persons.getNextPerson());
        } 
        
        throw new org.osid.NotFoundException(personId + " not found");
    }


    /**
     *  Gets a <code>PersonList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  persons specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Persons</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  personIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Person</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>personIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.PersonList getPersonsByIds(org.osid.id.IdList personIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.personnel.PersonQuery query = getQuery();

        try (org.osid.id.IdList ids = personIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getPersonsByQuery(query));
    }


    /**
     *  Gets a <code>PersonList</code> corresponding to the given
     *  person genus <code>Type</code> which does not include
     *  persons of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  persons or an error results. Otherwise, the returned list
     *  may contain only those persons that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  personGenusType a person genus type 
     *  @return the returned <code>Person</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>personGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.PersonList getPersonsByGenusType(org.osid.type.Type personGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.personnel.PersonQuery query = getQuery();
        query.matchGenusType(personGenusType, true);
        return (this.session.getPersonsByQuery(query));
    }


    /**
     *  Gets a <code>PersonList</code> corresponding to the given
     *  person genus <code>Type</code> and include any additional
     *  persons with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  persons or an error results. Otherwise, the returned list
     *  may contain only those persons that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  personGenusType a person genus type 
     *  @return the returned <code>Person</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>personGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.PersonList getPersonsByParentGenusType(org.osid.type.Type personGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.personnel.PersonQuery query = getQuery();
        query.matchParentGenusType(personGenusType, true);
        return (this.session.getPersonsByQuery(query));
    }


    /**
     *  Gets a <code>PersonList</code> containing the given
     *  person record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  persons or an error results. Otherwise, the returned list
     *  may contain only those persons that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  personRecordType a person record type 
     *  @return the returned <code>Person</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>personRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.PersonList getPersonsByRecordType(org.osid.type.Type personRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.personnel.PersonQuery query = getQuery();
        query.matchRecordType(personRecordType, true);
        return (this.session.getPersonsByQuery(query));
    }

    
    /**
     *  Gets all <code>Persons</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  persons or an error results. Otherwise, the returned list
     *  may contain only those persons that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Persons</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.PersonList getPersons()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.personnel.PersonQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getPersonsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.personnel.PersonQuery getQuery() {
        org.osid.personnel.PersonQuery query = this.session.getPersonQuery();
        return (query);
    }
}
