//
// InvariantMapProxyProgramEntryLookupSession
//
//    Implements a ProgramEntry lookup service backed by a fixed
//    collection of programEntries. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.chronicle;


/**
 *  Implements a ProgramEntry lookup service backed by a fixed
 *  collection of program entries. The program entries are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyProgramEntryLookupSession
    extends net.okapia.osid.jamocha.core.course.chronicle.spi.AbstractMapProgramEntryLookupSession
    implements org.osid.course.chronicle.ProgramEntryLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyProgramEntryLookupSession} with no
     *  program entries.
     *
     *  @param courseCatalog the course catalog
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyProgramEntryLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.proxy.Proxy proxy) {
        setCourseCatalog(courseCatalog);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyProgramEntryLookupSession} with a single
     *  program entry.
     *
     *  @param courseCatalog the course catalog
     *  @param programEntry a single program entry
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code programEntry} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyProgramEntryLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.course.chronicle.ProgramEntry programEntry, org.osid.proxy.Proxy proxy) {

        this(courseCatalog, proxy);
        putProgramEntry(programEntry);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyProgramEntryLookupSession} using
     *  an array of program entries.
     *
     *  @param courseCatalog the course catalog
     *  @param programEntries an array of program entries
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code programEntries} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyProgramEntryLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.course.chronicle.ProgramEntry[] programEntries, org.osid.proxy.Proxy proxy) {

        this(courseCatalog, proxy);
        putProgramEntries(programEntries);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyProgramEntryLookupSession} using a
     *  collection of program entries.
     *
     *  @param courseCatalog the course catalog
     *  @param programEntries a collection of program entries
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code programEntries} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyProgramEntryLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  java.util.Collection<? extends org.osid.course.chronicle.ProgramEntry> programEntries,
                                                  org.osid.proxy.Proxy proxy) {

        this(courseCatalog, proxy);
        putProgramEntries(programEntries);
        return;
    }
}
