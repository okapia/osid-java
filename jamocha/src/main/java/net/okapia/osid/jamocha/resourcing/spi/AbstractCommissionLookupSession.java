//
// AbstractCommissionLookupSession.java
//
//    A starter implementation framework for providing a Commission
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Commission
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getCommissions(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractCommissionLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.resourcing.CommissionLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.resourcing.Foundry foundry = new net.okapia.osid.jamocha.nil.resourcing.foundry.UnknownFoundry();
    

    /**
     *  Gets the <code>Foundry/code> <code>Id</code> associated with
     *  this session.
     *
     *  @return the <code>Foundry Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFoundryId() {
        return (this.foundry.getId());
    }


    /**
     *  Gets the <code>Foundry</code> associated with this session.
     *
     *  @return the <code>Foundry</code> associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Foundry getFoundry()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.foundry);
    }


    /**
     *  Sets the <code>Foundry</code>.
     *
     *  @param  foundry the foundry for this session
     *  @throws org.osid.NullArgumentException <code>foundry</code>
     *          is <code>null</code>
     */

    protected void setFoundry(org.osid.resourcing.Foundry foundry) {
        nullarg(foundry, "foundry");
        this.foundry = foundry;
        return;
    }


    /**
     *  Tests if this user can perform <code>Commission</code>
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCommissions() {
        return (true);
    }


    /**
     *  A complete view of the <code>Commission</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCommissionView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Commission</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCommissionView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include commissions in foundries which are children
     *  of this foundry in the foundry hierarchy.
     */

    @OSID @Override
    public void useFederatedFoundryView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this foundry only.
     */

    @OSID @Override
    public void useIsolatedFoundryView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only commissions whose effective dates are current are
     *  returned by methods in this session.
     */

    @OSID @Override
    public void useEffectiveCommissionView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All commissions of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveCommissionView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Commission</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Commission</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Commission</code> and
     *  retained for compatibility.
     *
     *  In effective mode, commissions are returned that are currently
     *  effective.  In any effective mode, effective commissions and
     *  those currently expired are returned.
     *
     *  @param  commissionId <code>Id</code> of the
     *          <code>Commission</code>
     *  @return the commission
     *  @throws org.osid.NotFoundException <code>commissionId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>commissionId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Commission getCommission(org.osid.id.Id commissionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.resourcing.CommissionList commissions = getCommissions()) {
            while (commissions.hasNext()) {
                org.osid.resourcing.Commission commission = commissions.getNextCommission();
                if (commission.getId().equals(commissionId)) {
                    return (commission);
                }
            }
        } 

        throw new org.osid.NotFoundException(commissionId + " not found");
    }


    /**
     *  Gets a <code>CommissionList</code> corresponding to the given
     *  <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  commissions specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Commissions</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, commissions are returned that are currently
     *  effective.  In any effective mode, effective commissions and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getCommissions()</code>.
     *
     *  @param commissionIds the list of <code>Ids</code> to retrieve
     *  @return the returned <code>Commission</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>commissionIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionList getCommissionsByIds(org.osid.id.IdList commissionIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.resourcing.Commission> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = commissionIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getCommission(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("commission " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.resourcing.commission.LinkedCommissionList(ret));
    }


    /**
     *  Gets a <code>CommissionList</code> corresponding to the given
     *  commission genus <code>Type</code> which does not include
     *  commissions of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  commissions or an error results. Otherwise, the returned list
     *  may contain only those commissions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, commissions are returned that are currently
     *  effective.  In any effective mode, effective commissions and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getCommissions()</code>.
     *
     *  @param commissionGenusType a commission genus type
     *  @return the returned <code>Commission</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commissionGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionList getCommissionsByGenusType(org.osid.type.Type commissionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.commission.CommissionGenusFilterList(getCommissions(), commissionGenusType));
    }


    /**
     *  Gets a <code>CommissionList</code> corresponding to the given
     *  commission genus <code>Type</code> and include any additional
     *  commissions with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  commissions or an error results. Otherwise, the returned list
     *  may contain only those commissions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, commissions are returned that are currently
     *  effective.  In any effective mode, effective commissions and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCommissions()</code>.
     *
     *  @param commissionGenusType a commission genus type
     *  @return the returned <code>Commission</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commissionGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionList getCommissionsByParentGenusType(org.osid.type.Type commissionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getCommissionsByGenusType(commissionGenusType));
    }


    /**
     *  Gets a <code>CommissionList</code> containing the given
     *  commission record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known
     *  commissions or an error results. Otherwise, the returned list
     *  may contain only those commissions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, commissions are returned that are currently
     *  effective.  In any effective mode, effective commissions and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCommissions()</code>.
     *
     *  @param commissionRecordType a commission record type
     *  @return the returned <code>Commission</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commissionRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionList getCommissionsByRecordType(org.osid.type.Type commissionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.commission.CommissionRecordFilterList(getCommissions(), commissionRecordType));
    }


    /**
     *  Gets a <code>CommissionList</code> effective during the entire
     *  given date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  commissions or an error results. Otherwise, the returned list
     *  may contain only those commissions that are accessible through
     *  this session.
     *  
     *  In active mode, commissions are returned that are currently
     *  active. In any status mode, active and inactive commissions
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Commission</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.resourcing.CommissionList getCommissionsOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.commission.TemporalCommissionFilterList(getCommissions(), from, to));
    }
        

    /**
     *  Gets a list of commissions corresponding to a resource
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  commissions or an error results. Otherwise, the returned list
     *  may contain only those commissions that are accessible through
     *  this session.
     *
     *  In effective mode, commissions are returned that are currently
     *  effective.  In any effective mode, effective commissions and
     *  those currently expired are returned.
     *
     *  @param resourceId the <code>Id</code> of the resource
     *  @return the returned <code>CommissionList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.resourcing.CommissionList getCommissionsForResource(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.commission.CommissionFilterList(new ResourceFilter(resourceId), getCommissions()));
    }


    /**
     *  Gets a list of commissions corresponding to a resource
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  commissions or an error results. Otherwise, the returned list
     *  may contain only those commissions that are accessible through
     *  this session.
     *
     *  In effective mode, commissions are returned that are currently
     *  effective.  In any effective mode, effective commissions and
     *  those currently expired are returned.
     *
     *  @param resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>CommissionList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.CommissionList getCommissionsForResourceOnDate(org.osid.id.Id resourceId,
                                                                      org.osid.calendaring.DateTime from,
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.commission.TemporalCommissionFilterList(getCommissionsForResource(resourceId), from, to));
    }


    /**
     *  Gets a list of commissions corresponding to a work
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  commissions or an error results. Otherwise, the returned list
     *  may contain only those commissions that are accessible through
     *  this session.
     *
     *  In effective mode, commissions are returned that are currently
     *  effective.  In any effective mode, effective commissions and
     *  those currently expired are returned.
     *
     *  @param workId the <code>Id</code> of the work
     *  @return the returned <code>CommissionList</code>
     *  @throws org.osid.NullArgumentException <code>workId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.resourcing.CommissionList getCommissionsForWork(org.osid.id.Id workId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.commission.CommissionFilterList(new WorkFilter(workId), getCommissions()));
    }


    /**
     *  Gets a list of commissions corresponding to a work
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  commissions or an error results. Otherwise, the returned list
     *  may contain only those commissions that are accessible through
     *  this session.
     *
     *  In effective mode, commissions are returned that are currently
     *  effective.  In any effective mode, effective commissions and
     *  those currently expired are returned.
     *
     *  @param workId the <code>Id</code> of the work
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>CommissionList</code>
     *  @throws org.osid.NullArgumentException <code>workId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.CommissionList getCommissionsForWorkOnDate(org.osid.id.Id workId,
                                                                      org.osid.calendaring.DateTime from,
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.commission.TemporalCommissionFilterList(getCommissionsForWork(workId), from, to));
    }


    /**
     *  Gets a list of commissions corresponding to resource and work
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  commissions or an error results. Otherwise, the returned list
     *  may contain only those commissions that are accessible through
     *  this session.
     *
     *  In effective mode, commissions are returned that are currently
     *  effective.  In any effective mode, effective commissions and
     *  those currently expired are returned.
     *
     *  @param resourceId the <code>Id</code> of the resource
     *  @param  workId the <code>Id</code> of the work
     *  @return the returned <code>CommissionList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>workId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.resourcing.CommissionList getCommissionsForResourceAndWork(org.osid.id.Id resourceId,
                                                                        org.osid.id.Id workId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.commission.CommissionFilterList(new WorkFilter(workId), getCommissionsForResource(resourceId)));
    }


    /**
     *  Gets a list of commissions corresponding to resource and work
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  commissions or an error results. Otherwise, the returned list
     *  may contain only those commissions that are accessible through
     *  this session.
     *
     *  In effective mode, commissions are returned that are currently
     *  effective.  In any effective mode, effective commissions and
     *  those currently expired are returned.
     *
     *  @param workId the <code>Id</code> of the work
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>CommissionList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>workId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.CommissionList getCommissionsForResourceAndWorkOnDate(org.osid.id.Id resourceId,
                                                                              org.osid.id.Id workId,
                                                                              org.osid.calendaring.DateTime from,
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.commission.TemporalCommissionFilterList(getCommissionsForResourceAndWork(resourceId, workId), from, to));
    }


    /**
     *  Gets all <code>Commissions</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  commissions or an error results. Otherwise, the returned list
     *  may contain only those commissions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, commissions are returned that are currently
     *  effective.  In any effective mode, effective commissions and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Commissions</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.resourcing.CommissionList getCommissions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the commission list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of commissions
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.resourcing.CommissionList filterCommissionsOnViews(org.osid.resourcing.CommissionList list)
        throws org.osid.OperationFailedException {

        org.osid.resourcing.CommissionList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.resourcing.commission.EffectiveCommissionFilterList(ret);
        }

        return (ret);
    }


    public static class ResourceFilter
        implements net.okapia.osid.jamocha.inline.filter.resourcing.commission.CommissionFilter {
         
        private final org.osid.id.Id resourceId;
         
         
        /**
         *  Constructs a new <code>ResourceFilter</code>.
         *
         *  @param resourceId the resource to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */
        
        public ResourceFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "resource Id");
            this.resourceId = resourceId;
            return;
        }

         
        /**
         *  Used by the CommissionFilterList to filter the 
         *  commission list based on resource.
         *
         *  @param commission the commission
         *  @return <code>true</code> to pass the commission,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.resourcing.Commission commission) {
            return (commission.getResourceId().equals(this.resourceId));
        }
    }


    public static class WorkFilter
        implements net.okapia.osid.jamocha.inline.filter.resourcing.commission.CommissionFilter {
         
        private final org.osid.id.Id workId;
         
         
        /**
         *  Constructs a new <code>WorkFilter</code>.
         *
         *  @param workId the work to filter
         *  @throws org.osid.NullArgumentException
         *          <code>workId</code> is <code>null</code>
         */
        
        public WorkFilter(org.osid.id.Id workId) {
            nullarg(workId, "work Id");
            this.workId = workId;
            return;
        }

         
        /**
         *  Used by the CommissionFilterList to filter the 
         *  commission list based on work.
         *
         *  @param commission the commission
         *  @return <code>true</code> to pass the commission,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.resourcing.Commission commission) {
            return (commission.getWorkId().equals(this.workId));
        }
    }
}
