//
// AbstractSyllabusLookupSession.java
//
//    A starter implementation framework for providing a Syllabus
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.syllabus.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Syllabus
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getSyllabi(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractSyllabusLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.course.syllabus.SyllabusLookupSession {

    private boolean pedantic      = false;
    private boolean federated     = false;
    private org.osid.course.CourseCatalog courseCatalog = new net.okapia.osid.jamocha.nil.course.coursecatalog.UnknownCourseCatalog();
    

    /**
     *  Gets the <code>CourseCatalog/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.courseCatalog.getId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.courseCatalog);
    }


    /**
     *  Sets the <code>CourseCatalog</code>.
     *
     *  @param  courseCatalog the course catalog for this session
     *  @throws org.osid.NullArgumentException <code>courseCatalog</code>
     *          is <code>null</code>
     */

    protected void setCourseCatalog(org.osid.course.CourseCatalog courseCatalog) {
        nullarg(courseCatalog, "course catalog");
        this.courseCatalog = courseCatalog;
        return;
    }


    /**
     *  Tests if this user can perform <code>Syllabus</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupSyllabi() {
        return (true);
    }


    /**
     *  A complete view of the <code>Syllabus</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeSyllabusView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Syllabus</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenarySyllabusView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include syllabi in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>Syllabus</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Syllabus</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Syllabus</code> and
     *  retained for compatibility.
     *
     *  @param  syllabusId <code>Id</code> of the
     *          <code>Syllabus</code>
     *  @return the syllabus
     *  @throws org.osid.NotFoundException <code>syllabusId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>syllabusId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.Syllabus getSyllabus(org.osid.id.Id syllabusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.course.syllabus.SyllabusList syllabi = getSyllabi()) {
            while (syllabi.hasNext()) {
                org.osid.course.syllabus.Syllabus syllabus = syllabi.getNextSyllabus();
                if (syllabus.getId().equals(syllabusId)) {
                    return (syllabus);
                }
            }
        } 

        throw new org.osid.NotFoundException(syllabusId + " not found");
    }


    /**
     *  Gets a <code>SyllabusList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  syllabi specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Syllabi</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getSyllabi()</code>.
     *
     *  @param  syllabusIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Syllabus</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>syllabusIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusList getSyllabiByIds(org.osid.id.IdList syllabusIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.course.syllabus.Syllabus> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = syllabusIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getSyllabus(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("syllabus " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.course.syllabus.syllabus.LinkedSyllabusList(ret));
    }


    /**
     *  Gets a <code>SyllabusList</code> corresponding to the given
     *  syllabus genus <code>Type</code> which does not include
     *  syllabi of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  syllabi or an error results. Otherwise, the returned list
     *  may contain only those syllabi that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getSyllabi()</code>.
     *
     *  @param  syllabusGenusType a syllabus genus type 
     *  @return the returned <code>Syllabus</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>syllabusGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusList getSyllabiByGenusType(org.osid.type.Type syllabusGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.syllabus.syllabus.SyllabusGenusFilterList(getSyllabi(), syllabusGenusType));
    }


    /**
     *  Gets a <code>SyllabusList</code> corresponding to the given
     *  syllabus genus <code>Type</code> and include any additional
     *  syllabi with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  syllabi or an error results. Otherwise, the returned list
     *  may contain only those syllabi that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getSyllabi()</code>.
     *
     *  @param  syllabusGenusType a syllabus genus type 
     *  @return the returned <code>Syllabus</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>syllabusGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusList getSyllabiByParentGenusType(org.osid.type.Type syllabusGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getSyllabiByGenusType(syllabusGenusType));
    }


    /**
     *  Gets a <code>SyllabusList</code> containing the given
     *  syllabus record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  syllabi or an error results. Otherwise, the returned list
     *  may contain only those syllabi that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getSyllabi()</code>.
     *
     *  @param  syllabusRecordType a syllabus record type 
     *  @return the returned <code>Syllabus</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>syllabusRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusList getSyllabiByRecordType(org.osid.type.Type syllabusRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.syllabus.syllabus.SyllabusRecordFilterList(getSyllabi(), syllabusRecordType));
    }


    /**
     *  Gets a <code> SyllabusList </code> for the given course <code>
     *  .  </code> In plenary mode, the returned list contains all
     *  known syllabi or an error results. Otherwise, the returned
     *  list may contain only those syllabi that are accessible
     *  through this session.
     *
     *  @param  courseId a course <code> Id </code> 
     *  @return the returned <code> Syllabus </code> list 
     *  @throws org.osid.NullArgumentException <code> courseId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusList getSyllabiForCourse(org.osid.id.Id courseId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.syllabus.syllabus.SyllabusFilterList(new CourseFilter(courseId), getSyllabi()));
    }        

    /**
     *  Gets all <code>Syllabi</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  syllabi or an error results. Otherwise, the returned list
     *  may contain only those syllabi that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Syllabi</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.course.syllabus.SyllabusList getSyllabi()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the syllabus list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of syllabi
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.course.syllabus.SyllabusList filterSyllabiOnViews(org.osid.course.syllabus.SyllabusList list)
        throws org.osid.OperationFailedException {

        return (list);
    }


    public static class CourseFilter
        implements net.okapia.osid.jamocha.inline.filter.course.syllabus.syllabus.SyllabusFilter {
         
        private final org.osid.id.Id courseId;
         
         
        /**
         *  Constructs a new <code>CourseFilter</code>.
         *
         *  @param courseId the course to filter
         *  @throws org.osid.NullArgumentException
         *          <code>courseId</code> is <code>null</code>
         */
        
        public CourseFilter(org.osid.id.Id courseId) {
            nullarg(courseId, "course Id");
            this.courseId = courseId;
            return;
        }

         
        /**
         *  Used by the SyllabusFilterList to filter the 
         *  syllabus list based on course.
         *
         *  @param syllabus the syllabus
         *  @return <code>true</code> to pass the syllabus,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.syllabus.Syllabus syllabus) {
            return (syllabus.getCourseId().equals(this.courseId));
        }
    }
}
