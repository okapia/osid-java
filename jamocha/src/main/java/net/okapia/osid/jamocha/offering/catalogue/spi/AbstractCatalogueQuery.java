//
// AbstractCatalogueQuery.java
//
//     A template for making a Catalogue Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.catalogue.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for catalogues.
 */

public abstract class AbstractCatalogueQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.offering.CatalogueQuery {

    private final java.util.Collection<org.osid.offering.records.CatalogueQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the canonical unit <code> Id </code> for this query to match 
     *  canonical units assigned to catalogues. 
     *
     *  @param  canonicalUnitId a canonical unit <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> canonicalUnitId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCanonicalUnitId(org.osid.id.Id canonicalUnitId, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears all canonical unit <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCanonicalUnitIdTerms() {
        return;
    }


    /**
     *  Tests if a canonical unit query is available. 
     *
     *  @return <code> true </code> if a canonical unit query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitQuery() {
        return (false);
    }


    /**
     *  Gets the query for a catalogue. 
     *
     *  @return the canonical unit query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitQuery getCanonicalUnitQuery() {
        throw new org.osid.UnimplementedException("supportsCanonicalUnitQuery() is false");
    }


    /**
     *  Matches catalogues with any canonical unit. 
     *
     *  @param  match <code> true </code> to match catalogues with any 
     *          canonical unit, <code> false </code> to match catalogues with 
     *          no canonical units 
     */

    @OSID @Override
    public void matchAnyCanonicalUnit(boolean match) {
        return;
    }


    /**
     *  Clears all canonical unit terms. 
     */

    @OSID @Override
    public void clearCanonicalUnitTerms() {
        return;
    }


    /**
     *  Sets the offering <code> Id </code> for this query to match offerings 
     *  assigned to catalogues. 
     *
     *  @param  offeringId an offering <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> offeringId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOfferingId(org.osid.id.Id offeringId, boolean match) {
        return;
    }


    /**
     *  Clears all offering <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearOfferingIdTerms() {
        return;
    }


    /**
     *  Tests if an offering query is available. 
     *
     *  @return <code> true </code> if an offering query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingQuery() {
        return (false);
    }


    /**
     *  Gets the query for an offering. 
     *
     *  @return the offering query 
     *  @throws org.osid.UnimplementedException <code> supportsOfferingQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingQuery getOfferingQuery() {
        throw new org.osid.UnimplementedException("supportsOfferingQuery() is false");
    }


    /**
     *  Matches catalogues with any offering. 
     *
     *  @param  match <code> true </code> to match catalogues with any 
     *          offering, <code> false </code> to match catalogues with no 
     *          offerings 
     */

    @OSID @Override
    public void matchAnyOffering(boolean match) {
        return;
    }


    /**
     *  Clears all offering terms. 
     */

    @OSID @Override
    public void clearOfferingTerms() {
        return;
    }


    /**
     *  Sets the participant <code> Id </code> for this query to match 
     *  participants assigned to catalogues. 
     *
     *  @param  participantId a participant <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> participantId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchParticipantId(org.osid.id.Id participantId, boolean match) {
        return;
    }


    /**
     *  Clears all participant <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearParticipantIdTerms() {
        return;
    }


    /**
     *  Tests if a participant query is available. 
     *
     *  @return <code> true </code> if a participant query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParticipantQuery() {
        return (false);
    }


    /**
     *  Gets the query for a participant. 
     *
     *  @return the participant query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantQuery getParticipantQuery() {
        throw new org.osid.UnimplementedException("supportsParticipantQuery() is false");
    }


    /**
     *  Matches catalogues with any participant. 
     *
     *  @param  match <code> true </code> to match catalogues with any 
     *          participant, <code> false </code> to match catalogues with no 
     *          participants 
     */

    @OSID @Override
    public void matchAnyParticipant(boolean match) {
        return;
    }


    /**
     *  Clears all participant terms. 
     */

    @OSID @Override
    public void clearParticipantTerms() {
        return;
    }


    /**
     *  Sets the result <code> Id </code> for this query to match results. 
     *
     *  @param  resultId an result <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resultId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResultId(org.osid.id.Id resultId, boolean match) {
        return;
    }


    /**
     *  Clears all result <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearResultIdTerms() {
        return;
    }


    /**
     *  Tests if an result query is available. 
     *
     *  @return <code> true </code> if an result query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResultQuery() {
        return (false);
    }


    /**
     *  Gets the query for an result. 
     *
     *  @return the result query 
     *  @throws org.osid.UnimplementedException <code> supportsResultQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ResultQuery getResultQuery() {
        throw new org.osid.UnimplementedException("supportsResultQuery() is false");
    }


    /**
     *  Matches catalogues with any result. 
     *
     *  @param  match <code> true </code> to match catalogues with any result, 
     *          <code> false </code> to match catalogues with no results 
     */

    @OSID @Override
    public void matchAnyResult(boolean match) {
        return;
    }


    /**
     *  Clears all result terms. 
     */

    @OSID @Override
    public void clearResultTerms() {
        return;
    }


    /**
     *  Sets the catalogue <code> Id </code> for this query to match 
     *  catalogues that have the specified catalogue as an ancestor. 
     *
     *  @param  catalogueId a catalogue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorCatalogueId(org.osid.id.Id catalogueId, 
                                         boolean match) {
        return;
    }


    /**
     *  Clears all ancestor catalogue <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorCatalogueIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CatalogueQuery </code> is available. 
     *
     *  @return <code> true </code> if a catalogue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorCatalogueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a catalogue. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the catalogue query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorCatalogueQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.CatalogueQuery getAncestorCatalogueQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorCatalogueQuery() is false");
    }


    /**
     *  Matches catalogues with any ancestor. 
     *
     *  @param  match <code> true </code> to match catalogues with any 
     *          ancestor, <code> false </code> to match root catalogues 
     */

    @OSID @Override
    public void matchAnyAncestorCatalogue(boolean match) {
        return;
    }


    /**
     *  Clears all ancestor catalogue terms. 
     */

    @OSID @Override
    public void clearAncestorCatalogueTerms() {
        return;
    }


    /**
     *  Sets the catalogue <code> Id </code> for this query to match 
     *  catalogues that have the specified catalogue as a descendant. 
     *
     *  @param  catalogueId a catalogue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantCatalogueId(org.osid.id.Id catalogueId, 
                                           boolean match) {
        return;
    }


    /**
     *  Clears all descendant catalogue <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantCatalogueIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CatalogueQuery </code> is available. 
     *
     *  @return <code> true </code> if a catalogue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantCatalogueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a catalogue. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the catalogue query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantCatalogueQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.CatalogueQuery getDescendantCatalogueQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantCatalogueQuery() is false");
    }


    /**
     *  Matches catalogues with any descendant. 
     *
     *  @param  match <code> true </code> to match catalogues with any 
     *          descendant, <code> false </code> to match leaf catalogues 
     */

    @OSID @Override
    public void matchAnyDescendantCatalogue(boolean match) {
        return;
    }


    /**
     *  Clears all descendant catalogue terms. 
     */

    @OSID @Override
    public void clearDescendantCatalogueTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given catalogue query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a catalogue implementing the requested record.
     *
     *  @param catalogueRecordType a catalogue record type
     *  @return the catalogue query record
     *  @throws org.osid.NullArgumentException
     *          <code>catalogueRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(catalogueRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.records.CatalogueQueryRecord getCatalogueQueryRecord(org.osid.type.Type catalogueRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.records.CatalogueQueryRecord record : this.records) {
            if (record.implementsRecordType(catalogueRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(catalogueRecordType + " is not supported");
    }


    /**
     *  Adds a record to this catalogue query. 
     *
     *  @param catalogueQueryRecord catalogue query record
     *  @param catalogueRecordType catalogue record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCatalogueQueryRecord(org.osid.offering.records.CatalogueQueryRecord catalogueQueryRecord, 
                                          org.osid.type.Type catalogueRecordType) {

        addRecordType(catalogueRecordType);
        nullarg(catalogueQueryRecord, "catalogue query record");
        this.records.add(catalogueQueryRecord);        
        return;
    }
}
