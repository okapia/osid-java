//
// ProfileEntryEnablerElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.profile.rules.profileentryenablerpi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ProfileEntryEnablerElements
    extends net.okapia.osid.jamocha.spi.OsidEnablerElements {


    /**
     *  Gets the ProfileEntryEnablerElement Id.
     *
     *  @return the profile entry enabler element Id
     */

    public static org.osid.id.Id getProfileEntryEnablerEntityId() {
        return (makeEntityId("osid.profile.rules.ProfileEntryEnabler"));
    }


    /**
     *  Gets the RuledProfileEntryId element Id.
     *
     *  @return the RuledProfileEntryId element Id
     */

    public static org.osid.id.Id getRuledProfileEntryId() {
        return (makeQueryElementId("osid.profile.rules.profileentryenabler.RuledProfileEntryId"));
    }


    /**
     *  Gets the RuledProfileEntry element Id.
     *
     *  @return the RuledProfileEntry element Id
     */

    public static org.osid.id.Id getRuledProfileEntry() {
        return (makeQueryElementId("osid.profile.rules.profileentryenabler.RuledProfileEntry"));
    }


    /**
     *  Gets the ProfileId element Id.
     *
     *  @return the ProfileId element Id
     */

    public static org.osid.id.Id getProfileId() {
        return (makeQueryElementId("osid.profile.rules.profileentryenabler.ProfileId"));
    }


    /**
     *  Gets the Profile element Id.
     *
     *  @return the Profile element Id
     */

    public static org.osid.id.Id getProfile() {
        return (makeQueryElementId("osid.profile.rules.profileentryenabler.Profile"));
    }
}
