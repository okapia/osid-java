//
// AbstractMapJournalEntryLookupSession
//
//    A simple framework for providing a JournalEntry lookup service
//    backed by a fixed collection of journal entries.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.journaling.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a JournalEntry lookup service backed by a
 *  fixed collection of journal entries. The journal entries are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>JournalEntries</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapJournalEntryLookupSession
    extends net.okapia.osid.jamocha.journaling.spi.AbstractJournalEntryLookupSession
    implements org.osid.journaling.JournalEntryLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.journaling.JournalEntry> journalEntries = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.journaling.JournalEntry>());


    /**
     *  Makes a <code>JournalEntry</code> available in this session.
     *
     *  @param  journalEntry a journal entry
     *  @throws org.osid.NullArgumentException <code>journalEntry<code>
     *          is <code>null</code>
     */

    protected void putJournalEntry(org.osid.journaling.JournalEntry journalEntry) {
        this.journalEntries.put(journalEntry.getId(), journalEntry);
        return;
    }


    /**
     *  Makes an array of journal entries available in this session.
     *
     *  @param  journalEntries an array of journal entries
     *  @throws org.osid.NullArgumentException <code>journalEntries<code>
     *          is <code>null</code>
     */

    protected void putJournalEntries(org.osid.journaling.JournalEntry[] journalEntries) {
        putJournalEntries(java.util.Arrays.asList(journalEntries));
        return;
    }


    /**
     *  Makes a collection of journal entries available in this session.
     *
     *  @param  journalEntries a collection of journal entries
     *  @throws org.osid.NullArgumentException <code>journalEntries<code>
     *          is <code>null</code>
     */

    protected void putJournalEntries(java.util.Collection<? extends org.osid.journaling.JournalEntry> journalEntries) {
        for (org.osid.journaling.JournalEntry journalEntry : journalEntries) {
            this.journalEntries.put(journalEntry.getId(), journalEntry);
        }

        return;
    }


    /**
     *  Removes a JournalEntry from this session.
     *
     *  @param  journalEntryId the <code>Id</code> of the journal entry
     *  @throws org.osid.NullArgumentException <code>journalEntryId<code> is
     *          <code>null</code>
     */

    protected void removeJournalEntry(org.osid.id.Id journalEntryId) {
        this.journalEntries.remove(journalEntryId);
        return;
    }


    /**
     *  Gets the <code>JournalEntry</code> specified by its <code>Id</code>.
     *
     *  @param  journalEntryId <code>Id</code> of the <code>JournalEntry</code>
     *  @return the journalEntry
     *  @throws org.osid.NotFoundException <code>journalEntryId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>journalEntryId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntry getJournalEntry(org.osid.id.Id journalEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.journaling.JournalEntry journalEntry = this.journalEntries.get(journalEntryId);
        if (journalEntry == null) {
            throw new org.osid.NotFoundException("journalEntry not found: " + journalEntryId);
        }

        return (journalEntry);
    }


    /**
     *  Gets all <code>JournalEntries</code>. In plenary mode, the returned
     *  list contains all known journalEntries or an error
     *  results. Otherwise, the returned list may contain only those
     *  journalEntries that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>JournalEntries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.journaling.journalentry.ArrayJournalEntryList(this.journalEntries.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.journalEntries.clear();
        super.close();
        return;
    }
}
