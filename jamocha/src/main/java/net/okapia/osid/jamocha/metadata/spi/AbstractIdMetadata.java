//
// AbstractIdMetadata.java
//
//     Defines an Id Metadata.
//
//
// Tom Coppeto
// Okapia
// 15 March 2013
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metadata.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an Id Metadata.
 */

public abstract class AbstractIdMetadata
    extends AbstractMetadata
    implements org.osid.Metadata {

    private final java.util.Collection<org.osid.id.Id> set = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.id.Id> defvals  = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.id.Id> existing = new java.util.LinkedHashSet<>();    
    

    /**
     *  Constructs a new {@code AbstractIdMetadata}.
     *
     *  @param elementId the Id of the element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    protected AbstractIdMetadata(org.osid.id.Id elementId) {
        super(org.osid.Syntax.ID, elementId);
        return;
    }


    /**
     *  Constructs a new {@code AbstractIdMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @param isLinked {@code true} if the element is linked to
     *         another element, {@code false} otherwise
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    protected AbstractIdMetadata(org.osid.id.Id elementId, boolean isArray, boolean isLinked) {
        super(org.osid.Syntax.ID, elementId, isArray, isLinked);
        return;
    }


    /**
     *  Gets the set of acceptable Id values. 
     *
     *  @return a set of Ids or an empty array if not restricted 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          ID </code>
     */

    @OSID @Override
    public org.osid.id.Id[] getIdSet() {
        return (this.set.toArray(new org.osid.id.Id[this.set.size()]));
    }

    
    /**
     *  Sets the Id set.
     *
     *  @param values a collection of accepted Id values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setIdSet(java.util.Collection<org.osid.id.Id> values) {
        this.set.clear();
        addToIdSet(values);
        return;
    }


    /**
     *  Adds a collection of values to the Id set.
     *
     *  @param values a collection of accepted Id values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addToIdSet(java.util.Collection<org.osid.id.Id> values) {
        nullarg(values, "Id set");
        this.set.addAll(values);
        return;
    }


    /**
     *  Adds a value to the Id set.
     *
     *  @param value an Id value
     *  @throws org.osid.InvalidArgumentException value is negative
     */

    protected void addToIdSet(org.osid.id.Id value) {
        nullarg(value, "id value");
        this.set.add(value);
        return;
    }


    /**
     *  Removes a value from the Id set.
     *
     *  @param value an Id value
     *  @throws org.osid.InvalidArgumentException value is negative
     */

    protected void removeFromIdSet(org.osid.id.Id value) {
        nullarg(value, "id value");
        this.set.remove(value);
        return;
    }


    /**
     *  Clears the Id set.
     */

    protected void clearIdSet() {
        this.set.clear();
        return;
    }


    /**
     *  Gets the default Id values. These are the values used
     *  if the element value is not provided or is cleared. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the default Id values 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          ID </code> or <code> isRequired() </code> is
     *          <code> true </code>
     */

    @OSID @Override
    public org.osid.id.Id[] getDefaultIdValues() {
        return (this.defvals.toArray(new org.osid.id.Id[this.defvals.size()]));
    }


    /**
     *  Sets the default Id set.
     *
     *  @param values a collection of default Id values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setDefaultIdValues(java.util.Collection<org.osid.id.Id> values) {
        clearDefaultIdValues();
        addDefaultIdValues(values);
        return;
    }


    /**
     *  Adds a collection of default Id values.
     *
     *  @param values a collection of default Id values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addDefaultIdValues(java.util.Collection<org.osid.id.Id> values) {
        nullarg(values, "default Id values");
        this.defvals.addAll(values);
        return;
    }


    /**
     *  Adds a default Id value.
     *
     *  @param value an Id value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void addDefaultIdValue(org.osid.id.Id value) {
        nullarg(value, "default Id value");
        this.defvals.add(value);
        return;
    }


    /**
     *  Removes a default Id value.
     *
     *  @param value an Id value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void removeDefaultIdValue(org.osid.id.Id value) {
        nullarg(value, "default Id value");
        this.defvals.remove(value);
        return;
    }


    /**
     *  Clears the default Id values.
     */

    protected void clearDefaultIdValues() {
        this.defvals.clear();
        return;
    }


    /**
     *  Gets the existing Id values. If <code> hasValue()
     *  </code> and <code> isRequired() </code> are <code> false,
     *  </code> then these values are the default values. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the existing Id values 
     *  @throws org.osid.IllegalStateException syntax is not a <code> 
     *          ID </code> or <code> isValueKnown() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.id.Id[] getExistingIdValues() {
        return (this.existing.toArray(new org.osid.id.Id[this.existing.size()]));
    }


    /**
     *  Sets the existing Id set.
     *
     *  @param values a collection of existing Id values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setExistingIdValues(java.util.Collection<org.osid.id.Id> values) {
        clearExistingIdValues();
        addExistingIdValues(values);
        return;
    }


    /**
     *  Adds a collection of existing Id values.
     *
     *  @param values a collection of existing Id values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addExistingIdValues(java.util.Collection<org.osid.id.Id> values) {
        nullarg(values, "existing Id values");

        this.existing.addAll(values);
        setValueKnown(true);

        return;
    }


    /**
     *  Adds a existing Id value.
     *
     *  @param value an Id value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void addExistingIdValue(org.osid.id.Id value) {
        nullarg(value, "existing Id value");

        this.existing.add(value);
        setValueKnown(true);

        return;
    }


    /**
     *  Removes a existing Id value.
     *
     *  @param value an Id value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void removeExistingIdValue(org.osid.id.Id value) {
        nullarg(value, "existing Id value");
        this.existing.remove(value);
        return;
    }


    /**
     *  Clears the existing Id values.
     */

    protected void clearExistingIdValues() {
        this.existing.clear();
        setValueKnown(false);
        return;
    }        
}