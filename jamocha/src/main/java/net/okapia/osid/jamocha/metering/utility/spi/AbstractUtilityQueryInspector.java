//
// AbstractUtilityQueryInspector.java
//
//     A template for making an UtilityQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metering.utility.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for utilities.
 */

public abstract class AbstractUtilityQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.metering.UtilityQueryInspector {

    private final java.util.Collection<org.osid.metering.records.UtilityQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the meter <code> Id </code> terms. 
     *
     *  @return the meter <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getMeterIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the meter terms. 
     *
     *  @return the meter terms 
     */

    @OSID @Override
    public org.osid.metering.MeterQueryInspector[] getMeterTerms() {
        return (new org.osid.metering.MeterQueryInspector[0]);
    }


    /**
     *  Gets the ancestor utility <code> Id </code> terms. 
     *
     *  @return the utility <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorUtilityIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor utility terms. 
     *
     *  @return the utility terms 
     */

    @OSID @Override
    public org.osid.metering.UtilityQueryInspector[] getAncestorUtilityTerms() {
        return (new org.osid.metering.UtilityQueryInspector[0]);
    }


    /**
     *  Gets the descendant utility <code> Id </code> terms. 
     *
     *  @return the utility <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantUtilityIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant utility terms. 
     *
     *  @return the utility terms 
     */

    @OSID @Override
    public org.osid.metering.UtilityQueryInspector[] getDescendantUtilityTerms() {
        return (new org.osid.metering.UtilityQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given utility query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an utility implementing the requested record.
     *
     *  @param utilityRecordType an utility record type
     *  @return the utility query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>utilityRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(utilityRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.metering.records.UtilityQueryInspectorRecord getUtilityQueryInspectorRecord(org.osid.type.Type utilityRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.metering.records.UtilityQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(utilityRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(utilityRecordType + " is not supported");
    }


    /**
     *  Adds a record to this utility query. 
     *
     *  @param utilityQueryInspectorRecord utility query inspector
     *         record
     *  @param utilityRecordType utility record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addUtilityQueryInspectorRecord(org.osid.metering.records.UtilityQueryInspectorRecord utilityQueryInspectorRecord, 
                                                   org.osid.type.Type utilityRecordType) {

        addRecordType(utilityRecordType);
        nullarg(utilityRecordType, "utility record type");
        this.records.add(utilityQueryInspectorRecord);        
        return;
    }
}
