//
// AbstractMapRaceProcessorEnablerLookupSession
//
//    A simple framework for providing a RaceProcessorEnabler lookup service
//    backed by a fixed collection of race processor enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a RaceProcessorEnabler lookup service backed by a
 *  fixed collection of race processor enablers. The race processor enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>RaceProcessorEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapRaceProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.voting.rules.spi.AbstractRaceProcessorEnablerLookupSession
    implements org.osid.voting.rules.RaceProcessorEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.voting.rules.RaceProcessorEnabler> raceProcessorEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.voting.rules.RaceProcessorEnabler>());


    /**
     *  Makes a <code>RaceProcessorEnabler</code> available in this session.
     *
     *  @param  raceProcessorEnabler a race processor enabler
     *  @throws org.osid.NullArgumentException <code>raceProcessorEnabler<code>
     *          is <code>null</code>
     */

    protected void putRaceProcessorEnabler(org.osid.voting.rules.RaceProcessorEnabler raceProcessorEnabler) {
        this.raceProcessorEnablers.put(raceProcessorEnabler.getId(), raceProcessorEnabler);
        return;
    }


    /**
     *  Makes an array of race processor enablers available in this session.
     *
     *  @param  raceProcessorEnablers an array of race processor enablers
     *  @throws org.osid.NullArgumentException <code>raceProcessorEnablers<code>
     *          is <code>null</code>
     */

    protected void putRaceProcessorEnablers(org.osid.voting.rules.RaceProcessorEnabler[] raceProcessorEnablers) {
        putRaceProcessorEnablers(java.util.Arrays.asList(raceProcessorEnablers));
        return;
    }


    /**
     *  Makes a collection of race processor enablers available in this session.
     *
     *  @param  raceProcessorEnablers a collection of race processor enablers
     *  @throws org.osid.NullArgumentException <code>raceProcessorEnablers<code>
     *          is <code>null</code>
     */

    protected void putRaceProcessorEnablers(java.util.Collection<? extends org.osid.voting.rules.RaceProcessorEnabler> raceProcessorEnablers) {
        for (org.osid.voting.rules.RaceProcessorEnabler raceProcessorEnabler : raceProcessorEnablers) {
            this.raceProcessorEnablers.put(raceProcessorEnabler.getId(), raceProcessorEnabler);
        }

        return;
    }


    /**
     *  Removes a RaceProcessorEnabler from this session.
     *
     *  @param  raceProcessorEnablerId the <code>Id</code> of the race processor enabler
     *  @throws org.osid.NullArgumentException <code>raceProcessorEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeRaceProcessorEnabler(org.osid.id.Id raceProcessorEnablerId) {
        this.raceProcessorEnablers.remove(raceProcessorEnablerId);
        return;
    }


    /**
     *  Gets the <code>RaceProcessorEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  raceProcessorEnablerId <code>Id</code> of the <code>RaceProcessorEnabler</code>
     *  @return the raceProcessorEnabler
     *  @throws org.osid.NotFoundException <code>raceProcessorEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>raceProcessorEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnabler getRaceProcessorEnabler(org.osid.id.Id raceProcessorEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.voting.rules.RaceProcessorEnabler raceProcessorEnabler = this.raceProcessorEnablers.get(raceProcessorEnablerId);
        if (raceProcessorEnabler == null) {
            throw new org.osid.NotFoundException("raceProcessorEnabler not found: " + raceProcessorEnablerId);
        }

        return (raceProcessorEnabler);
    }


    /**
     *  Gets all <code>RaceProcessorEnablers</code>. In plenary mode, the returned
     *  list contains all known raceProcessorEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  raceProcessorEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>RaceProcessorEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerList getRaceProcessorEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.voting.rules.raceprocessorenabler.ArrayRaceProcessorEnablerList(this.raceProcessorEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.raceProcessorEnablers.clear();
        super.close();
        return;
    }
}
