//
// AbstractQueryEdgeLookupSession.java
//
//    An inline adapter that maps an EdgeLookupSession to
//    an EdgeQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.topology.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an EdgeLookupSession to
 *  an EdgeQuerySession.
 */

public abstract class AbstractQueryEdgeLookupSession
    extends net.okapia.osid.jamocha.topology.spi.AbstractEdgeLookupSession
    implements org.osid.topology.EdgeLookupSession {

    private boolean effectiveonly = false;
    private final org.osid.topology.EdgeQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryEdgeLookupSession.
     *
     *  @param querySession the underlying edge query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryEdgeLookupSession(org.osid.topology.EdgeQuerySession querySession) {
        nullarg(querySession, "edge query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Graph</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Graph Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getGraphId() {
        return (this.session.getGraphId());
    }


    /**
     *  Gets the <code>Graph</code> associated with this 
     *  session.
     *
     *  @return the <code>Graph</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.Graph getGraph()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getGraph());
    }


    /**
     *  Tests if this user can perform <code>Edge</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupEdges() {
        return (this.session.canSearchEdges());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include edges in graphs which are children
     *  of this graph in the graph hierarchy.
     */

    @OSID @Override
    public void useFederatedGraphView() {
        this.session.useFederatedGraphView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this graph only.
     */

    @OSID @Override
    public void useIsolatedGraphView() {
        this.session.useIsolatedGraphView();
        return;
    }
    

    /**
     *  Only edges whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveEdgeView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All edges of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveEdgeView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Edge</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Edge</code> may have a different <code>Id</code> than
     *  requested, such as the case where a duplicate <code>Id</code>
     *  was assigned to a <code>Edge</code> and retained for
     *  compatibility.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param  edgeId <code>Id</code> of the
     *          <code>Edge</code>
     *  @return the edge
     *  @throws org.osid.NotFoundException <code>edgeId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>edgeId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.Edge getEdge(org.osid.id.Id edgeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.EdgeQuery query = getQuery();
        query.matchId(edgeId, true);
        org.osid.topology.EdgeList edges = this.session.getEdgesByQuery(query);
        if (edges.hasNext()) {
            return (edges.getNextEdge());
        } 
        
        throw new org.osid.NotFoundException(edgeId + " not found");
    }


    /**
     *  Gets an <code>EdgeList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the edges
     *  specified in the <code>Id</code> list, in the order of the
     *  list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Edges</code> may
     *  be omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param  edgeIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Edge</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>edgeIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByIds(org.osid.id.IdList edgeIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.EdgeQuery query = getQuery();

        try (org.osid.id.IdList ids = edgeIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getEdgesByQuery(query));
    }


    /**
     *  Gets an <code>EdgeList</code> corresponding to the given
     *  edge genus <code>Type</code> which does not include
     *  edges of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  edges or an error results. Otherwise, the returned list
     *  may contain only those edges that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param  edgeGenusType an edge genus type 
     *  @return the returned <code>Edge</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>edgeGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByGenusType(org.osid.type.Type edgeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.EdgeQuery query = getQuery();
        query.matchGenusType(edgeGenusType, true);
        return (this.session.getEdgesByQuery(query));
    }


    /**
     *  Gets an <code>EdgeList</code> corresponding to the given
     *  edge genus <code>Type</code> and include any additional
     *  edges with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  edges or an error results. Otherwise, the returned list
     *  may contain only those edges that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and
     *  those currently expired are returned.
     *
     *  @param  edgeGenusType an edge genus type 
     *  @return the returned <code>Edge</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>edgeGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByParentGenusType(org.osid.type.Type edgeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.EdgeQuery query = getQuery();
        query.matchParentGenusType(edgeGenusType, true);
        return (this.session.getEdgesByQuery(query));
    }


    /**
     *  Gets an <code>EdgeList</code> containing the given
     *  edge record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  edges or an error results. Otherwise, the returned list
     *  may contain only those edges that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and
     *  those currently expired are returned.
     *
     *  @param  edgeRecordType an edge record type 
     *  @return the returned <code>Edge</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>edgeRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByRecordType(org.osid.type.Type edgeRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.EdgeQuery query = getQuery();
        query.matchRecordType(edgeRecordType, true);
        return (this.session.getEdgesByQuery(query));
    }


    /**
     *  Gets an <code>EdgeList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  edges or an error results. Otherwise, the returned list
     *  may contain only those edges that are accessible
     *  through this session.
     *  
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and
     *  those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Edge</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.topology.EdgeList getEdgesOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.EdgeQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getEdgesByQuery(query));
    }
        

    /**
     *  Gets an <code> EdgeList </code> of a genus type effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session.
     *  
     *  In effective mode, edges are returned that are currently
     *  effective. In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param  edgeGenusType an edge genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code> Edge </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> edgeGenusType,
     *          from, </code> or <code> to </code> is <code> null
     *          </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByGenusTypeOnDate(org.osid.type.Type edgeGenusType, 
                                                                org.osid.calendaring.DateTime from, 
                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.EdgeQuery query = getQuery();
        query.matchGenusType(edgeGenusType, true);
        query.matchDate(from, to, true);
        return (this.session.getEdgesByQuery(query));
    }


    /**
     *  Gets a list of edges corresponding to a source node
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param  sourceNodeId the <code>Id</code> of the source node
     *  @return the returned <code>EdgeList</code>
     *  @throws org.osid.NullArgumentException <code>sourceNodeId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.topology.EdgeList getEdgesForSourceNode(org.osid.id.Id sourceNodeId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.topology.EdgeQuery query = getQuery();
        query.matchSourceNodeId(sourceNodeId, true);
        return (this.session.getEdgesByQuery(query));
    }


    /**
     *  Gets a list of edges corresponding to a source node
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param  sourceNodeId the <code>Id</code> of the source node
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>EdgeList</code>
     *  @throws org.osid.NullArgumentException <code>sourceNodeId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesForSourceNodeOnDate(org.osid.id.Id sourceNodeId,
                                                                  org.osid.calendaring.DateTime from,
                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.EdgeQuery query = getQuery();
        query.matchSourceNodeId(sourceNodeId, true);
        query.matchDate(from, to, true);
        return (this.session.getEdgesByQuery(query));
    }


    /**
     *  Gets an <code> EdgeList </code> corresponding to the given
     *  source node <code> Id </code> and edge genus <code> Type
     *  </code> and includes any genus types derived from the given
     *  genus type.
     *  
     *  In plenary mode, the returned list contains all of the edges
     *  corresponding to the given peer, including duplicates, or an
     *  error results if an edge is inaccessible. Otherwise,
     *  inaccessible <code> Edges </code> may be omitted from the list
     *  and may present the elements in any order including returning
     *  a unique set.
     *  
     *  In effective mode, edges are returned that are currently
     *  effective. In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param  sourceNodeId a node <code> Id </code> 
     *  @param  edgeGenusType an edge genus type 
     *  @return a list of edges 
     *  @throws org.osid.NullArgumentException <code> sourceNodeId </code> or 
     *          <code> edgeGenusType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByGenusTypeForSourceNode(org.osid.id.Id sourceNodeId, 
                                                                       org.osid.type.Type edgeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.EdgeQuery query = getQuery();
        query.matchSourceNodeId(sourceNodeId, true);
        query.matchGenusType(edgeGenusType, true);
        return (this.session.getEdgesByQuery(query));
    }


    /**
     *  Gets an <code> EdgeList </code> with the given genus type and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session.
     *  
     *  In effective mode, edges are returned that are currently
     *  effective. In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param  nodeId a node <code> Id </code> 
     *  @param  edgeGenusType an edge genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return a list of edges 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> sourceNodeId,
     *          edgeGenusType, from </code> or <code> to </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByGenusTypeForSourceNodeOnDate(org.osid.id.Id nodeId, 
                                                                             org.osid.type.Type edgeGenusType, 
                                                                             org.osid.calendaring.DateTime from, 
                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.EdgeQuery query = getQuery();
        query.matchSourceNodeId(nodeId, true);
        query.matchGenusType(edgeGenusType, true);
        query.matchDate(from, to, true);
        return (this.session.getEdgesByQuery(query));
    }


    /**
     *  Gets a list of edges corresponding to a destination node
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session.
     *
     *  In effective mode, edges are returned that are
     *  currently effective.  In any effective mode, effective
     *  edges and those currently expired are returned.
     *
     *  @param destinationNodeId the <code>Id</code> of the
     *         destination node
     *  @return the returned <code>EdgeList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>destinationNodeId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.topology.EdgeList getEdgesForDestinationNode(org.osid.id.Id destinationNodeId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.topology.EdgeQuery query = getQuery();
        query.matchDestinationNodeId(destinationNodeId, true);
        return (this.session.getEdgesByQuery(query));
    }


    /**
     *  Gets a list of edges corresponding to a destination node
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param destinationNodeId the <code>Id</code> of the
     *         destination node
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>EdgeList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>destinationNodeId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesForDestinationNodeOnDate(org.osid.id.Id destinationNodeId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.EdgeQuery query = getQuery();
        query.matchDestinationNodeId(destinationNodeId, true);
        query.matchDate(from, to, true);
        return (this.session.getEdgesByQuery(query));
    }


    /**
     *  Gets an <code> EdgeList </code> corresponding to the given
     *  destination node <code> Id </code> and edge genus <code> Type
     *  </code> and includes any genus types derived from the given
     *  genus type.
     *  
     *  In plenary mode, the returned list contains all of the edges
     *  corresponding to the given peer, including duplicates, or an
     *  error results if an edge is inaccessible. Otherwise,
     *  inaccessible <code> Edges </code> may be omitted from the list
     *  and may present the elements in any order including returning
     *  a unique set.
     *  
     *  In effective mode, edges are returned that are currently
     *  effective. In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param  destinationNodeId a node <code> Id </code> 
     *  @param  edgeGenusType an edge genus type 
     *  @return a list of edges 
     *  @throws org.osid.NullArgumentException <code>
     *          destinationNodeId </code> or <code> edgeGenusType
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByGenusTypeForDestinationNode(org.osid.id.Id destinationNodeId, 
                                                                            org.osid.type.Type edgeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.EdgeQuery query = getQuery();
        query.matchDestinationNodeId(destinationNodeId, true);
        query.matchGenusType(edgeGenusType, true);
        return (this.session.getEdgesByQuery(query));
    }


    /**
     *  Gets an <code> EdgeList </code> with the given genus type and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range. 
     *  
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session.
     *  
     *  In effective mode, edges are returned that are currently
     *  effective. In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param  destinationNodeId a node <code> Id </code> 
     *  @param  edgeGenusType an edge genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return a list of edges 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code>
     *          destinationNodeId, edgeGenusType, from </code> or
     *          <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByGenusTypeForDestinationNodeOnDate(org.osid.id.Id destinationNodeId, 
                                                                                  org.osid.type.Type edgeGenusType, 
                                                                                  org.osid.calendaring.DateTime from, 
                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.EdgeQuery query = getQuery();
        query.matchDestinationNodeId(destinationNodeId, true);
        query.matchGenusType(edgeGenusType, true);
        query.matchDate(from, to, true);
        return (this.session.getEdgesByQuery(query));
    }


    /**
     *  Gets a list of edges corresponding to source node and
     *  destination node <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  edges or an error results. Otherwise, the returned list
     *  may contain only those edges that are accessible
     *  through this session.
     *
     *  In effective mode, edges are returned that are
     *  currently effective.  In any effective mode, effective
     *  edges and those currently expired are returned.
     *
     *  @param sourceNodeId the <code>Id</code> of the source node
     *  @param destinationNodeId the <code>Id</code> of the
     *         destination node
     *  @return the returned <code>EdgeList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>sourceNodeId</code>,
     *          <code>destinationNodeId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.topology.EdgeList getEdgesForNodes(org.osid.id.Id sourceNodeId,
                                                       org.osid.id.Id destinationNodeId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.topology.EdgeQuery query = getQuery();
        query.matchSourceNodeId(sourceNodeId, true);
        query.matchDestinationNodeId(destinationNodeId, true);
        return (this.session.getEdgesByQuery(query));
    }


    /**
     *  Gets a list of edges corresponding to source node and
     *  destination node <code>Ids</code> and effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param sourceNodeId the <code>Id</code> of the source node
     *  @param destinationNodeId the <code>Id</code> of the
     *         destination node
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>EdgeList</code>
     *  @throws org.osid.NullArgumentException <code>sourceNodeId</code>,
     *          <code>destinationNodeId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesForNodesOnDate(org.osid.id.Id sourceNodeId,
                                                             org.osid.id.Id destinationNodeId,
                                                             org.osid.calendaring.DateTime from,
                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.topology.EdgeQuery query = getQuery();
        query.matchSourceNodeId(sourceNodeId, true);
        query.matchDestinationNodeId(destinationNodeId, true);
        query.matchDate(from, to, true);
        return (this.session.getEdgesByQuery(query));
    }


    /**
     *  Gets an EdgeList corresponding to the given peer <code> Ids
     *  </code> and edge genus type and includes and genus types
     *  derived from the given genus type.
     *  
     *  In plenary mode, the returned list contains all of the edges
     *  corresponding to the given peer, including duplicates, or an
     *  error results if an edge is inaccessible. Otherwise,
     *  inaccessible <code> Edges </code> may be omitted from the list
     *  and may present the elements in any order including returning
     *  a unique set.
     *  
     *  In effective mode, edges are returned that are currently
     *  effective. In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param  sourceNodeId a node <code> Id </code> 
     *  @param  destinationNodeId a node <code> Id </code> 
     *  @param  edgeGenusType an edge genus type 
     *  @return the edges 
     *  @throws org.osid.NullArgumentException <code> sourceNodeId,
     *          destinationNodeId, </code> or <code> edgeGenusType
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByGenusTypeForNodes(org.osid.id.Id sourceNodeId, 
                                                                  org.osid.id.Id destinationNodeId, 
                                                                  org.osid.type.Type edgeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.EdgeQuery query = getQuery();
        query.matchSourceNodeId(sourceNodeId, true);
        query.matchDestinationNodeId(destinationNodeId, true);
        query.matchGenusType(edgeGenusType, true);
        return (this.session.getEdgesByQuery(query));
    }

    
    /**
     *  Gets an <code> EdgeList </code> of the given genus type
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session.
     *  
     *  In effective mode, edges are returned that are currently
     *  effective. In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param  sourceNodeId a node <code> Id </code> 
     *  @param  destinationNodeId a node <code> Id </code> 
     *  @param  edgeGenusType an edge genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return a list of edges 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> sourceNodeId,
     *          destinationNodeId, edgeGenusType, from </code> or
     *          <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByGenusTypeForNodesOnDate(org.osid.id.Id sourceNodeId, 
                                                                        org.osid.id.Id destinationNodeId, 
                                                                        org.osid.type.Type edgeGenusType, 
                                                                        org.osid.calendaring.DateTime from, 
                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.EdgeQuery query = getQuery();
        query.matchSourceNodeId(sourceNodeId, true);
        query.matchDestinationNodeId(destinationNodeId, true);
        query.matchGenusType(edgeGenusType, true);
        query.matchDate(from, to, true);
        return (this.session.getEdgesByQuery(query));
    }


    /**
     *  Gets all <code>Edges</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  edges or an error results. Otherwise, the returned list
     *  may contain only those edges that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Edges</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdges()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.EdgeQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getEdgesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.topology.EdgeQuery getQuery() {
        org.osid.topology.EdgeQuery query = this.session.getEdgeQuery();
        
        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }

        return (query);
    }
}
