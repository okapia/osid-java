//
// AbstractRaceSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.race.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractRaceSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.voting.RaceSearchResults {

    private org.osid.voting.RaceList races;
    private final org.osid.voting.RaceQueryInspector inspector;
    private final java.util.Collection<org.osid.voting.records.RaceSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractRaceSearchResults.
     *
     *  @param races the result set
     *  @param raceQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>races</code>
     *          or <code>raceQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractRaceSearchResults(org.osid.voting.RaceList races,
                                            org.osid.voting.RaceQueryInspector raceQueryInspector) {
        nullarg(races, "races");
        nullarg(raceQueryInspector, "race query inspectpr");

        this.races = races;
        this.inspector = raceQueryInspector;

        return;
    }


    /**
     *  Gets the race list resulting from a search.
     *
     *  @return a race list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.voting.RaceList getRaces() {
        if (this.races == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.voting.RaceList races = this.races;
        this.races = null;
	return (races);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.voting.RaceQueryInspector getRaceQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  race search record <code> Type. </code> This method must
     *  be used to retrieve a race implementing the requested
     *  record.
     *
     *  @param raceSearchRecordType a race search 
     *         record type 
     *  @return the race search
     *  @throws org.osid.NullArgumentException
     *          <code>raceSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(raceSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.records.RaceSearchResultsRecord getRaceSearchResultsRecord(org.osid.type.Type raceSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.voting.records.RaceSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(raceSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(raceSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record race search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addRaceRecord(org.osid.voting.records.RaceSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "race record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
