//
// TodoMiter.java
//
//     Defines a Todo miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.checklist.todo;


/**
 *  Defines a <code>Todo</code> miter for use with the builders.
 */

public interface TodoMiter
    extends net.okapia.osid.jamocha.builder.spi.TemporalOsidObjectMiter,
            net.okapia.osid.jamocha.builder.spi.ContainableMiter,
            org.osid.checklist.Todo {


    /**
     *  Sets the complete flag.
     *
     *  @param complete <code> true </code> if this todo is complete,
     *         <code> false </code> if not complete
     */

    public void setComplete(boolean complete);


    /**
     *  Sets the priority.
     *
     *  @param priority a priority
     *  @throws org.osid.NullArgumentException <code>priority</code>
     *          is <code>null</code>
     */

    public void setPriority(org.osid.type.Type priority);


    /**
     *  Sets the due date.
     *
     *  @param date a due date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setDueDate(org.osid.calendaring.DateTime date);


    /**
     *  Adds a dependency.
     *
     *  @param dependency a dependency
     *  @throws org.osid.NullArgumentException <code>dependency</code>
     *          is <code>null</code>
     */

    public void addDependency(org.osid.checklist.Todo dependency);


    /**
     *  Sets all the dependencies.
     *
     *  @param dependencies a collection of dependencies
     *  @throws org.osid.NullArgumentException
     *          <code>dependencies</code> is <code>null</code>
     */

    public void setDependencies(java.util.Collection<org.osid.checklist.Todo> dependencies);


    /**
     *  Adds a Todo record.
     *
     *  @param record a todo record
     *  @param recordType the type of todo record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addTodoRecord(org.osid.checklist.records.TodoRecord record, org.osid.type.Type recordType);
}       


