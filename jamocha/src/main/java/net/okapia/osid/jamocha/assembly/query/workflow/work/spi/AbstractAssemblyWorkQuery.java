//
// AbstractAssemblyWorkQuery.java
//
//     A WorkQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.workflow.work.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A WorkQuery that stores terms.
 */

public abstract class AbstractAssemblyWorkQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.workflow.WorkQuery,
               org.osid.workflow.WorkQueryInspector,
               org.osid.workflow.WorkSearchOrder {

    private final java.util.Collection<org.osid.workflow.records.WorkQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.workflow.records.WorkQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.workflow.records.WorkSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyWorkQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyWorkQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches suspended work. 
     *
     *  @param  match <code> true </code> to match suspended work, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public void matchSuspended(boolean match) {
        getAssembler().addBooleanTerm(getSuspendedColumn(), match);
        return;
    }


    /**
     *  Clears the suspended query terms. 
     */

    @OSID @Override
    public void clearSuspendedTerms() {
        getAssembler().clearTerms(getSuspendedColumn());
        return;
    }


    /**
     *  Gets the suspended query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getSuspendedTerms() {
        return (getAssembler().getBooleanTerms(getSuspendedColumn()));
    }


    /**
     *  Gets the Suspended column name.
     *
     * @return the column name
     */

    protected String getSuspendedColumn() {
        return ("suspended");
    }


    /**
     *  Sets the process <code> Id </code> for this query. 
     *
     *  @param  processId the process <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> processId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProcessId(org.osid.id.Id processId, boolean match) {
        getAssembler().addIdTerm(getProcessIdColumn(), processId, match);
        return;
    }


    /**
     *  Clears the process <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearProcessIdTerms() {
        getAssembler().clearTerms(getProcessIdColumn());
        return;
    }


    /**
     *  Gets the process <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProcessIdTerms() {
        return (getAssembler().getIdTerms(getProcessIdColumn()));
    }


    /**
     *  Gets the ProcessId column name.
     *
     * @return the column name
     */

    protected String getProcessIdColumn() {
        return ("process_id");
    }


    /**
     *  Tests if a <code> ProcessQuery </code> is available. 
     *
     *  @return <code> true </code> if a process query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a process. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the process query 
     *  @throws org.osid.UnimplementedException <code> supportsProcessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessQuery getProcessQuery() {
        throw new org.osid.UnimplementedException("supportsProcessQuery() is false");
    }


    /**
     *  Clears the process query terms. 
     */

    @OSID @Override
    public void clearProcessTerms() {
        getAssembler().clearTerms(getProcessColumn());
        return;
    }


    /**
     *  Gets the process query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.workflow.ProcessQueryInspector[] getProcessTerms() {
        return (new org.osid.workflow.ProcessQueryInspector[0]);
    }


    /**
     *  Gets the Process column name.
     *
     * @return the column name
     */

    protected String getProcessColumn() {
        return ("process");
    }


    /**
     *  Sets the step <code> Id </code> for this query. 
     *
     *  @param  stepId the step <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stepId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchStepId(org.osid.id.Id stepId, boolean match) {
        getAssembler().addIdTerm(getStepIdColumn(), stepId, match);
        return;
    }


    /**
     *  Clears the step <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearStepIdTerms() {
        getAssembler().clearTerms(getStepIdColumn());
        return;
    }


    /**
     *  Gets the step <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStepIdTerms() {
        return (getAssembler().getIdTerms(getStepIdColumn()));
    }


    /**
     *  Gets the StepId column name.
     *
     * @return the column name
     */

    protected String getStepIdColumn() {
        return ("step_id");
    }


    /**
     *  Tests if a <code> StepQuery </code> is available. 
     *
     *  @return <code> true </code> if a step query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepQuery() {
        return (false);
    }


    /**
     *  Gets the query for a step Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the step query 
     *  @throws org.osid.UnimplementedException <code> supportsStepyQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepQuery getStepQuery() {
        throw new org.osid.UnimplementedException("supportsStepQuery() is false");
    }


    /**
     *  Matches processes that have any step. 
     *
     *  @param  match <code> true </code> to match processes with any step, 
     *          <code> false </code> to match processes with no step 
     */

    @OSID @Override
    public void matchAnyStep(boolean match) {
        getAssembler().addIdWildcardTerm(getStepColumn(), match);
        return;
    }


    /**
     *  Clears the step query terms. 
     */

    @OSID @Override
    public void clearStepTerms() {
        getAssembler().clearTerms(getStepColumn());
        return;
    }


    /**
     *  Gets the step query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.workflow.StepQueryInspector[] getStepTerms() {
        return (new org.osid.workflow.StepQueryInspector[0]);
    }


    /**
     *  Gets the Step column name.
     *
     * @return the column name
     */

    protected String getStepColumn() {
        return ("step");
    }


    /**
     *  Sets the office <code> Id </code> for this query to match works 
     *  assigned to offices. 
     *
     *  @param  officeId the office <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOfficeId(org.osid.id.Id officeId, boolean match) {
        getAssembler().addIdTerm(getOfficeIdColumn(), officeId, match);
        return;
    }


    /**
     *  Clears the office <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearOfficeIdTerms() {
        getAssembler().clearTerms(getOfficeIdColumn());
        return;
    }


    /**
     *  Gets the office <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOfficeIdTerms() {
        return (getAssembler().getIdTerms(getOfficeIdColumn()));
    }


    /**
     *  Gets the OfficeId column name.
     *
     * @return the column name
     */

    protected String getOfficeIdColumn() {
        return ("office_id");
    }


    /**
     *  Tests if a <code> OfficeQuery </code> is available. 
     *
     *  @return <code> true </code> if a office query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfficeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a office. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the office query 
     *  @throws org.osid.UnimplementedException <code> supportsOfficeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.OfficeQuery getOfficeQuery() {
        throw new org.osid.UnimplementedException("supportsOfficeQuery() is false");
    }


    /**
     *  Clears the office query terms. 
     */

    @OSID @Override
    public void clearOfficeTerms() {
        getAssembler().clearTerms(getOfficeColumn());
        return;
    }


    /**
     *  Gets the office query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.workflow.OfficeQueryInspector[] getOfficeTerms() {
        return (new org.osid.workflow.OfficeQueryInspector[0]);
    }


    /**
     *  Gets the Office column name.
     *
     * @return the column name
     */

    protected String getOfficeColumn() {
        return ("office");
    }


    /**
     *  Tests if this work supports the given record
     *  <code>Type</code>.
     *
     *  @param  workRecordType a work record type 
     *  @return <code>true</code> if the workRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>workRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type workRecordType) {
        for (org.osid.workflow.records.WorkQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(workRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  workRecordType the work record type 
     *  @return the work query record 
     *  @throws org.osid.NullArgumentException
     *          <code>workRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(workRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.records.WorkQueryRecord getWorkQueryRecord(org.osid.type.Type workRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.workflow.records.WorkQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(workRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(workRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  workRecordType the work record type 
     *  @return the work query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>workRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(workRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.records.WorkQueryInspectorRecord getWorkQueryInspectorRecord(org.osid.type.Type workRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.workflow.records.WorkQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(workRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(workRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param workRecordType the work record type
     *  @return the work search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>workRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(workRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.records.WorkSearchOrderRecord getWorkSearchOrderRecord(org.osid.type.Type workRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.workflow.records.WorkSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(workRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(workRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this work. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param workQueryRecord the work query record
     *  @param workQueryInspectorRecord the work query inspector
     *         record
     *  @param workSearchOrderRecord the work search order record
     *  @param workRecordType work record type
     *  @throws org.osid.NullArgumentException
     *          <code>workQueryRecord</code>,
     *          <code>workQueryInspectorRecord</code>,
     *          <code>workSearchOrderRecord</code> or
     *          <code>workRecordTypework</code> is
     *          <code>null</code>
     */
            
    protected void addWorkRecords(org.osid.workflow.records.WorkQueryRecord workQueryRecord, 
                                      org.osid.workflow.records.WorkQueryInspectorRecord workQueryInspectorRecord, 
                                      org.osid.workflow.records.WorkSearchOrderRecord workSearchOrderRecord, 
                                      org.osid.type.Type workRecordType) {

        addRecordType(workRecordType);

        nullarg(workQueryRecord, "work query record");
        nullarg(workQueryInspectorRecord, "work query inspector record");
        nullarg(workSearchOrderRecord, "work search odrer record");

        this.queryRecords.add(workQueryRecord);
        this.queryInspectorRecords.add(workQueryInspectorRecord);
        this.searchOrderRecords.add(workSearchOrderRecord);
        
        return;
    }
}
