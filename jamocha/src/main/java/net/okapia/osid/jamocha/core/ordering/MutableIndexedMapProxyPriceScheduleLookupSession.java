//
// MutableIndexedMapProxyPriceScheduleLookupSession
//
//    Implements a PriceSchedule lookup service backed by a collection of
//    priceSchedules indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.ordering;


/**
 *  Implements a PriceSchedule lookup service backed by a collection of
 *  priceSchedules. The price schedules are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some priceSchedules may be compatible
 *  with more types than are indicated through these priceSchedule
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of price schedules can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyPriceScheduleLookupSession
    extends net.okapia.osid.jamocha.core.ordering.spi.AbstractIndexedMapPriceScheduleLookupSession
    implements org.osid.ordering.PriceScheduleLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyPriceScheduleLookupSession} with
     *  no price schedule.
     *
     *  @param store the store
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code store} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyPriceScheduleLookupSession(org.osid.ordering.Store store,
                                                       org.osid.proxy.Proxy proxy) {
        setStore(store);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyPriceScheduleLookupSession} with
     *  a single price schedule.
     *
     *  @param store the store
     *  @param  priceSchedule an price schedule
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code store},
     *          {@code priceSchedule}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyPriceScheduleLookupSession(org.osid.ordering.Store store,
                                                       org.osid.ordering.PriceSchedule priceSchedule, org.osid.proxy.Proxy proxy) {

        this(store, proxy);
        putPriceSchedule(priceSchedule);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyPriceScheduleLookupSession} using
     *  an array of price schedules.
     *
     *  @param store the store
     *  @param  priceSchedules an array of price schedules
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code store},
     *          {@code priceSchedules}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyPriceScheduleLookupSession(org.osid.ordering.Store store,
                                                       org.osid.ordering.PriceSchedule[] priceSchedules, org.osid.proxy.Proxy proxy) {

        this(store, proxy);
        putPriceSchedules(priceSchedules);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyPriceScheduleLookupSession} using
     *  a collection of price schedules.
     *
     *  @param store the store
     *  @param  priceSchedules a collection of price schedules
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code store},
     *          {@code priceSchedules}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyPriceScheduleLookupSession(org.osid.ordering.Store store,
                                                       java.util.Collection<? extends org.osid.ordering.PriceSchedule> priceSchedules,
                                                       org.osid.proxy.Proxy proxy) {
        this(store, proxy);
        putPriceSchedules(priceSchedules);
        return;
    }

    
    /**
     *  Makes a {@code PriceSchedule} available in this session.
     *
     *  @param  priceSchedule a price schedule
     *  @throws org.osid.NullArgumentException {@code priceSchedule{@code 
     *          is {@code null}
     */

    @Override
    public void putPriceSchedule(org.osid.ordering.PriceSchedule priceSchedule) {
        super.putPriceSchedule(priceSchedule);
        return;
    }


    /**
     *  Makes an array of price schedules available in this session.
     *
     *  @param  priceSchedules an array of price schedules
     *  @throws org.osid.NullArgumentException {@code priceSchedules{@code 
     *          is {@code null}
     */

    @Override
    public void putPriceSchedules(org.osid.ordering.PriceSchedule[] priceSchedules) {
        super.putPriceSchedules(priceSchedules);
        return;
    }


    /**
     *  Makes collection of price schedules available in this session.
     *
     *  @param  priceSchedules a collection of price schedules
     *  @throws org.osid.NullArgumentException {@code priceSchedule{@code 
     *          is {@code null}
     */

    @Override
    public void putPriceSchedules(java.util.Collection<? extends org.osid.ordering.PriceSchedule> priceSchedules) {
        super.putPriceSchedules(priceSchedules);
        return;
    }


    /**
     *  Removes a PriceSchedule from this session.
     *
     *  @param priceScheduleId the {@code Id} of the price schedule
     *  @throws org.osid.NullArgumentException {@code priceScheduleId{@code  is
     *          {@code null}
     */

    @Override
    public void removePriceSchedule(org.osid.id.Id priceScheduleId) {
        super.removePriceSchedule(priceScheduleId);
        return;
    }    
}
