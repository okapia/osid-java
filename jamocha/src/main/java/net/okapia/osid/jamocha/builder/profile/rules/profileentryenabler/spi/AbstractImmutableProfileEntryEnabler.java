//
// AbstractImmutableProfileEntryEnabler.java
//
//     Wraps a mutable ProfileEntryEnabler to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.profile.rules.profileentryenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>ProfileEntryEnabler</code> to hide modifiers. This
 *  wrapper provides an immutized ProfileEntryEnabler from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying profileEntryEnabler whose state changes are visible.
 */

public abstract class AbstractImmutableProfileEntryEnabler
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidEnabler
    implements org.osid.profile.rules.ProfileEntryEnabler {

    private final org.osid.profile.rules.ProfileEntryEnabler profileEntryEnabler;


    /**
     *  Constructs a new <code>AbstractImmutableProfileEntryEnabler</code>.
     *
     *  @param profileEntryEnabler the profile entry enabler to immutablize
     *  @throws org.osid.NullArgumentException <code>profileEntryEnabler</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableProfileEntryEnabler(org.osid.profile.rules.ProfileEntryEnabler profileEntryEnabler) {
        super(profileEntryEnabler);
        this.profileEntryEnabler = profileEntryEnabler;
        return;
    }


    /**
     *  Gets the profile entry enabler record corresponding to the given 
     *  <code> ProfileEntryEnabler </code> record <code> Type. </code> This 
     *  method is used to retrieve an object implementing the requested 
     *  record. The <code> profileEntryEnablerRecordType </code> may be the 
     *  <code> Type </code> returned in <code> getRecordTypes() </code> or any 
     *  of its parents in a <code> Type </code> hierarchy where <code> 
     *  hasRecordType(profileEntryEnablerRecordType) </code> is <code> true 
     *  </code> . 
     *
     *  @param  profileEntryEnablerRecordType the type of profile entry 
     *          enabler record to retrieve 
     *  @return the profile entry enabler record 
     *  @throws org.osid.NullArgumentException <code> 
     *          profileEntryEnablerRecordType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(profileEntryEnablerRecordType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.profile.rules.records.ProfileEntryEnablerRecord getProfileEntryEnablerRecord(org.osid.type.Type profileEntryEnablerRecordType)
        throws org.osid.OperationFailedException {

        return (this.profileEntryEnabler.getProfileEntryEnablerRecord(profileEntryEnablerRecordType));
    }
}

