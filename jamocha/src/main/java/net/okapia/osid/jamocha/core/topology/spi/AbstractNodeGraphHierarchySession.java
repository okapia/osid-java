//
// AbstractNodeGraphHierarchySession.java
//
//     Defines a Graph hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.topology.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a graph hierarchy session for delivering a hierarchy
 *  of graphs using the GraphNode interface.
 */

public abstract class AbstractNodeGraphHierarchySession
    extends net.okapia.osid.jamocha.topology.spi.AbstractGraphHierarchySession
    implements org.osid.topology.GraphHierarchySession {

    private java.util.Collection<org.osid.topology.GraphNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root graph <code> Ids </code> in this hierarchy.
     *
     *  @return the root graph <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootGraphIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.topology.graphnode.GraphNodeToIdList(this.roots));
    }


    /**
     *  Gets the root graphs in the graph hierarchy. A node
     *  with no parents is an orphan. While all graph <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root graphs 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.GraphList getRootGraphs()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.topology.graphnode.GraphNodeToGraphList(new net.okapia.osid.jamocha.topology.graphnode.ArrayGraphNodeList(this.roots)));
    }


    /**
     *  Adds a root graph node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootGraph(org.osid.topology.GraphNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root graph nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootGraphs(java.util.Collection<org.osid.topology.GraphNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root graph node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootGraph(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.topology.GraphNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Graph </code> has any parents. 
     *
     *  @param  graphId a graph <code> Id </code> 
     *  @return <code> true </code> if the graph has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> graphId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> graphId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentGraphs(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getGraphNode(graphId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  graph.
     *
     *  @param  id an <code> Id </code> 
     *  @param  graphId the <code> Id </code> of a graph 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> graphId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> graphId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> graphId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfGraph(org.osid.id.Id id, org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.topology.GraphNodeList parents = getGraphNode(graphId).getParentGraphNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextGraphNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given graph. 
     *
     *  @param  graphId a graph <code> Id </code> 
     *  @return the parent <code> Ids </code> of the graph 
     *  @throws org.osid.NotFoundException <code> graphId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> graphId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentGraphIds(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.topology.graph.GraphToIdList(getParentGraphs(graphId)));
    }


    /**
     *  Gets the parents of the given graph. 
     *
     *  @param  graphId the <code> Id </code> to query 
     *  @return the parents of the graph 
     *  @throws org.osid.NotFoundException <code> graphId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> graphId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.GraphList getParentGraphs(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.topology.graphnode.GraphNodeToGraphList(getGraphNode(graphId).getParentGraphNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  graph.
     *
     *  @param  id an <code> Id </code> 
     *  @param  graphId the Id of a graph 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> graphId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> graphId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> graphId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfGraph(org.osid.id.Id id, org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfGraph(id, graphId)) {
            return (true);
        }

        try (org.osid.topology.GraphList parents = getParentGraphs(graphId)) {
            while (parents.hasNext()) {
                if (isAncestorOfGraph(id, parents.getNextGraph().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a graph has any children. 
     *
     *  @param  graphId a graph <code> Id </code> 
     *  @return <code> true </code> if the <code> graphId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> graphId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> graphId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildGraphs(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getGraphNode(graphId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  graph.
     *
     *  @param  id an <code> Id </code> 
     *  @param graphId the <code> Id </code> of a 
     *         graph
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> graphId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> graphId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> graphId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfGraph(org.osid.id.Id id, org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfGraph(graphId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  graph.
     *
     *  @param  graphId the <code> Id </code> to query 
     *  @return the children of the graph 
     *  @throws org.osid.NotFoundException <code> graphId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> graphId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildGraphIds(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.topology.graph.GraphToIdList(getChildGraphs(graphId)));
    }


    /**
     *  Gets the children of the given graph. 
     *
     *  @param  graphId the <code> Id </code> to query 
     *  @return the children of the graph 
     *  @throws org.osid.NotFoundException <code> graphId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> graphId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.GraphList getChildGraphs(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.topology.graphnode.GraphNodeToGraphList(getGraphNode(graphId).getChildGraphNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  graph.
     *
     *  @param  id an <code> Id </code> 
     *  @param graphId the <code> Id </code> of a 
     *         graph
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> graphId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> graphId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> graphId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfGraph(org.osid.id.Id id, org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfGraph(graphId, id)) {
            return (true);
        }

        try (org.osid.topology.GraphList children = getChildGraphs(graphId)) {
            while (children.hasNext()) {
                if (isDescendantOfGraph(id, children.getNextGraph().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  graph.
     *
     *  @param  graphId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified graph node 
     *  @throws org.osid.NotFoundException <code> graphId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> graphId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getGraphNodeIds(org.osid.id.Id graphId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.topology.graphnode.GraphNodeToNode(getGraphNode(graphId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given graph.
     *
     *  @param  graphId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified graph node 
     *  @throws org.osid.NotFoundException <code> graphId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> graphId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.GraphNode getGraphNodes(org.osid.id.Id graphId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getGraphNode(graphId));
    }


    /**
     *  Closes this <code>GraphHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a graph node.
     *
     *  @param graphId the id of the graph node
     *  @throws org.osid.NotFoundException <code>graphId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>graphId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.topology.GraphNode getGraphNode(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(graphId, "graph Id");
        for (org.osid.topology.GraphNode graph : this.roots) {
            if (graph.getId().equals(graphId)) {
                return (graph);
            }

            org.osid.topology.GraphNode r = findGraph(graph, graphId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(graphId + " is not found");
    }


    protected org.osid.topology.GraphNode findGraph(org.osid.topology.GraphNode node, 
                                                    org.osid.id.Id graphId) 
	throws org.osid.OperationFailedException {

        try (org.osid.topology.GraphNodeList children = node.getChildGraphNodes()) {
            while (children.hasNext()) {
                org.osid.topology.GraphNode graph = children.getNextGraphNode();
                if (graph.getId().equals(graphId)) {
                    return (graph);
                }
                
                graph = findGraph(graph, graphId);
                if (graph != null) {
                    return (graph);
                }
            }
        }

        return (null);
    }
}
