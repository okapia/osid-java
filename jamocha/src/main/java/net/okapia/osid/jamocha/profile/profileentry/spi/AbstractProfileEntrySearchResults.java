//
// AbstractProfileEntrySearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.profile.profileentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractProfileEntrySearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.profile.ProfileEntrySearchResults {

    private org.osid.profile.ProfileEntryList profileEntries;
    private final org.osid.profile.ProfileEntryQueryInspector inspector;
    private final java.util.Collection<org.osid.profile.records.ProfileEntrySearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractProfileEntrySearchResults.
     *
     *  @param profileEntries the result set
     *  @param profileEntryQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>profileEntries</code>
     *          or <code>profileEntryQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractProfileEntrySearchResults(org.osid.profile.ProfileEntryList profileEntries,
                                            org.osid.profile.ProfileEntryQueryInspector profileEntryQueryInspector) {
        nullarg(profileEntries, "profile entries");
        nullarg(profileEntryQueryInspector, "profile entry query inspectpr");

        this.profileEntries = profileEntries;
        this.inspector = profileEntryQueryInspector;

        return;
    }


    /**
     *  Gets the profile entry list resulting from a search.
     *
     *  @return a profile entry list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntries() {
        if (this.profileEntries == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.profile.ProfileEntryList profileEntries = this.profileEntries;
        this.profileEntries = null;
	return (profileEntries);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.profile.ProfileEntryQueryInspector getProfileEntryQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  profile entry search record <code> Type. </code> This method must
     *  be used to retrieve a profileEntry implementing the requested
     *  record.
     *
     *  @param profileEntrySearchRecordType a profileEntry search 
     *         record type 
     *  @return the profile entry search
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntrySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(profileEntrySearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.profile.records.ProfileEntrySearchResultsRecord getProfileEntrySearchResultsRecord(org.osid.type.Type profileEntrySearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.profile.records.ProfileEntrySearchResultsRecord record : this.records) {
            if (record.implementsRecordType(profileEntrySearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(profileEntrySearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record profile entry search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addProfileEntryRecord(org.osid.profile.records.ProfileEntrySearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "profile entry record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
