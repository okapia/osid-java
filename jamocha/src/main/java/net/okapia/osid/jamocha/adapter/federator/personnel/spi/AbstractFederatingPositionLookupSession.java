//
// AbstractFederatingPositionLookupSession.java
//
//     An abstract federating adapter for a PositionLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.personnel.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  PositionLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingPositionLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.personnel.PositionLookupSession>
    implements org.osid.personnel.PositionLookupSession {

    private boolean parallel = false;
    private org.osid.personnel.Realm realm = new net.okapia.osid.jamocha.nil.personnel.realm.UnknownRealm();


    /**
     *  Constructs a new <code>AbstractFederatingPositionLookupSession</code>.
     */

    protected AbstractFederatingPositionLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.personnel.PositionLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Realm/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Realm Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getRealmId() {
        return (this.realm.getId());
    }


    /**
     *  Gets the <code>Realm</code> associated with this 
     *  session.
     *
     *  @return the <code>Realm</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.Realm getRealm()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.realm);
    }


    /**
     *  Sets the <code>Realm</code>.
     *
     *  @param  realm the realm for this session
     *  @throws org.osid.NullArgumentException <code>realm</code>
     *          is <code>null</code>
     */

    protected void setRealm(org.osid.personnel.Realm realm) {
        nullarg(realm, "realm");
        this.realm = realm;
        return;
    }


    /**
     *  Tests if this user can perform <code>Position</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPositions() {
        for (org.osid.personnel.PositionLookupSession session : getSessions()) {
            if (session.canLookupPositions()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Position</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePositionView() {
        for (org.osid.personnel.PositionLookupSession session : getSessions()) {
            session.useComparativePositionView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Position</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPositionView() {
        for (org.osid.personnel.PositionLookupSession session : getSessions()) {
            session.usePlenaryPositionView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include positions in realms which are children
     *  of this realm in the realm hierarchy.
     */

    @OSID @Override
    public void useFederatedRealmView() {
        for (org.osid.personnel.PositionLookupSession session : getSessions()) {
            session.useFederatedRealmView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this realm only.
     */

    @OSID @Override
    public void useIsolatedRealmView() {
        for (org.osid.personnel.PositionLookupSession session : getSessions()) {
            session.useIsolatedRealmView();
        }

        return;
    }


    /**
     *  Only positions whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectivePositionView() {
        for (org.osid.personnel.PositionLookupSession session : getSessions()) {
            session.useEffectivePositionView();
        }

        return;
    }


    /**
     *  All positions of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectivePositionView() {
        for (org.osid.personnel.PositionLookupSession session : getSessions()) {
            session.useAnyEffectivePositionView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Position</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Position</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Position</code> and
     *  retained for compatibility.
     *
     *  In effective mode, positions are returned that are currently
     *  effective.  In any effective mode, effective positions and
     *  those currently expired are returned.
     *
     *  @param  positionId <code>Id</code> of the
     *          <code>Position</code>
     *  @return the position
     *  @throws org.osid.NotFoundException <code>positionId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>positionId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.Position getPosition(org.osid.id.Id positionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.personnel.PositionLookupSession session : getSessions()) {
            try {
                return (session.getPosition(positionId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(positionId + " not found");
    }


    /**
     *  Gets a <code>PositionList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  positions specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Positions</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, positions are returned that are currently effective.
     *  In any effective mode, effective positions and those currently expired
     *  are returned.
     *
     *  @param  positionIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Position</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>positionIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.PositionList getPositionsByIds(org.osid.id.IdList positionIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.personnel.position.MutablePositionList ret = new net.okapia.osid.jamocha.personnel.position.MutablePositionList();

        try (org.osid.id.IdList ids = positionIds) {
            while (ids.hasNext()) {
                ret.addPosition(getPosition(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>PositionList</code> corresponding to the given
     *  position genus <code>Type</code> which does not include
     *  positions of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  positions or an error results. Otherwise, the returned list
     *  may contain only those positions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, positions are returned that are currently effective.
     *  In any effective mode, effective positions and those currently expired
     *  are returned.
     *
     *  @param  positionGenusType a position genus type 
     *  @return the returned <code>Position</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>positionGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.PositionList getPositionsByGenusType(org.osid.type.Type positionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.personnel.position.FederatingPositionList ret = getPositionList();

        for (org.osid.personnel.PositionLookupSession session : getSessions()) {
            ret.addPositionList(session.getPositionsByGenusType(positionGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PositionList</code> corresponding to the given
     *  position genus <code>Type</code> and include any additional
     *  positions with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  positions or an error results. Otherwise, the returned list
     *  may contain only those positions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, positions are returned that are currently
     *  effective.  In any effective mode, effective positions and
     *  those currently expired are returned.
     *
     *  @param  positionGenusType a position genus type 
     *  @return the returned <code>Position</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>positionGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.PositionList getPositionsByParentGenusType(org.osid.type.Type positionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.personnel.position.FederatingPositionList ret = getPositionList();

        for (org.osid.personnel.PositionLookupSession session : getSessions()) {
            ret.addPositionList(session.getPositionsByParentGenusType(positionGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PositionList</code> containing the given
     *  position record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  positions or an error results. Otherwise, the returned list
     *  may contain only those positions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, positions are returned that are currently
     *  effective.  In any effective mode, effective positions and
     *  those currently expired are returned.
     *
     *  @param  positionRecordType a position record type 
     *  @return the returned <code>Position</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>positionRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.PositionList getPositionsByRecordType(org.osid.type.Type positionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.personnel.position.FederatingPositionList ret = getPositionList();

        for (org.osid.personnel.PositionLookupSession session : getSessions()) {
            ret.addPositionList(session.getPositionsByRecordType(positionRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PositionList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  positions or an error results. Otherwise, the returned list
     *  may contain only those positions that are accessible
     *  through this session.
     *  
     *  In effective mode, positions are returned that are currently
     *  effective.  In any effective mode, effective positions and
     *  those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Position</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.personnel.PositionList getPositionsOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.personnel.position.FederatingPositionList ret = getPositionList();

        for (org.osid.personnel.PositionLookupSession session : getSessions()) {
            ret.addPositionList(session.getPositionsOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a <code>PositionList</code> for the given organization.
     *
     *  In plenary mode, the returned list contains all known
     *  positions or an error results. Otherwise, the returned list
     *  may contain only those positions that are accessible through
     *  this session.
     *
     *  In effective mode, positions are returned that are currently
     *  effective.  In any effective mode, effective positions and
     *  those currently expired are returned.
     *
     *  @param  organizationId an organization <code>Id</code>
     *  @return the returned <code>PositionList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>organizationId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.personnel.PositionList getPositionsForOrganization(org.osid.id.Id organizationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.personnel.position.FederatingPositionList ret = getPositionList();

        for (org.osid.personnel.PositionLookupSession session : getSessions()) {
            ret.addPositionList(session.getPositionsForOrganization(organizationId));
        }

        ret.noMore();
        return (ret);
    }

    
    /**
     *  Gets a <code>PositionList</code> for the given organization
     *  and effective during the entire given date range inclusive but
     *  not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  positions or an error results. Otherwise, the returned list
     *  may contain only those positions that are accessible through
     *  this session.
     *
     *  In effective mode, positions are returned that are currently
     *  effective.  In any effective mode, effective positions and
     *  those currently expired are returned.
     *
     *  @param  organizationId an organization <code>Id</code>
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code>PositionList</code>
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>organizationId</code>, <code>from</code>, or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.personnel.PositionList getPositionsForOrganizationOnDate(org.osid.id.Id organizationId,
                                                                             org.osid.calendaring.DateTime from,
                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.personnel.position.FederatingPositionList ret = getPositionList();

        for (org.osid.personnel.PositionLookupSession session : getSessions()) {
            ret.addPositionList(session.getPositionsForOrganizationOnDate(organizationId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Positions</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  positions or an error results. Otherwise, the returned list
     *  may contain only those positions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, positions are returned that are currently
     *  effective.  In any effective mode, effective positions and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Positions</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.PositionList getPositions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.personnel.position.FederatingPositionList ret = getPositionList();

        for (org.osid.personnel.PositionLookupSession session : getSessions()) {
            ret.addPositionList(session.getPositions());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.personnel.position.FederatingPositionList getPositionList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.personnel.position.ParallelPositionList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.personnel.position.CompositePositionList());
        }
    }
}
