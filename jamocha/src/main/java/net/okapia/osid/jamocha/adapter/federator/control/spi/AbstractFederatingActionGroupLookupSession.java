//
// AbstractFederatingActionGroupLookupSession.java
//
//     An abstract federating adapter for an ActionGroupLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.control.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  ActionGroupLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingActionGroupLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.control.ActionGroupLookupSession>
    implements org.osid.control.ActionGroupLookupSession {

    private boolean parallel = false;
    private org.osid.control.System system = new net.okapia.osid.jamocha.nil.control.system.UnknownSystem();


    /**
     *  Constructs a new <code>AbstractFederatingActionGroupLookupSession</code>.
     */

    protected AbstractFederatingActionGroupLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.control.ActionGroupLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>System/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>System Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getSystemId() {
        return (this.system.getId());
    }


    /**
     *  Gets the <code>System</code> associated with this 
     *  session.
     *
     *  @return the <code>System</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.System getSystem()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.system);
    }


    /**
     *  Sets the <code>System</code>.
     *
     *  @param  system the system for this session
     *  @throws org.osid.NullArgumentException <code>system</code>
     *          is <code>null</code>
     */

    protected void setSystem(org.osid.control.System system) {
        nullarg(system, "system");
        this.system = system;
        return;
    }


    /**
     *  Tests if this user can perform <code>ActionGroup</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupActionGroups() {
        for (org.osid.control.ActionGroupLookupSession session : getSessions()) {
            if (session.canLookupActionGroups()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>ActionGroup</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeActionGroupView() {
        for (org.osid.control.ActionGroupLookupSession session : getSessions()) {
            session.useComparativeActionGroupView();
        }

        return;
    }


    /**
     *  A complete view of the <code>ActionGroup</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryActionGroupView() {
        for (org.osid.control.ActionGroupLookupSession session : getSessions()) {
            session.usePlenaryActionGroupView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include action groups in systems which are children
     *  of this system in the system hierarchy.
     */

    @OSID @Override
    public void useFederatedSystemView() {
        for (org.osid.control.ActionGroupLookupSession session : getSessions()) {
            session.useFederatedSystemView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this system only.
     */

    @OSID @Override
    public void useIsolatedSystemView() {
        for (org.osid.control.ActionGroupLookupSession session : getSessions()) {
            session.useIsolatedSystemView();
        }

        return;
    }

     
    /**
     *  Gets the <code>ActionGroup</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>ActionGroup</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>ActionGroup</code> and
     *  retained for compatibility.
     *
     *  @param  actionGroupId <code>Id</code> of the
     *          <code>ActionGroup</code>
     *  @return the action group
     *  @throws org.osid.NotFoundException <code>actionGroupId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>actionGroupId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.ActionGroup getActionGroup(org.osid.id.Id actionGroupId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.control.ActionGroupLookupSession session : getSessions()) {
            try {
                return (session.getActionGroup(actionGroupId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(actionGroupId + " not found");
    }


    /**
     *  Gets an <code>ActionGroupList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  actionGroups specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>ActionGroups</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getActionGroups()</code>.
     *
     *  @param  actionGroupIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>ActionGroup</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>actionGroupIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.ActionGroupList getActionGroupsByIds(org.osid.id.IdList actionGroupIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.control.actiongroup.MutableActionGroupList ret = new net.okapia.osid.jamocha.control.actiongroup.MutableActionGroupList();

        try (org.osid.id.IdList ids = actionGroupIds) {
            while (ids.hasNext()) {
                ret.addActionGroup(getActionGroup(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>ActionGroupList</code> corresponding to the given
     *  action group genus <code>Type</code> which does not include
     *  action groups of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  action groups or an error results. Otherwise, the returned list
     *  may contain only those action groups that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getActionGroups()</code>.
     *
     *  @param  actionGroupGenusType an actionGroup genus type 
     *  @return the returned <code>ActionGroup</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>actionGroupGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.ActionGroupList getActionGroupsByGenusType(org.osid.type.Type actionGroupGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.control.actiongroup.FederatingActionGroupList ret = getActionGroupList();

        for (org.osid.control.ActionGroupLookupSession session : getSessions()) {
            ret.addActionGroupList(session.getActionGroupsByGenusType(actionGroupGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>ActionGroupList</code> corresponding to the given
     *  action group genus <code>Type</code> and include any additional
     *  action groups with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  action groups or an error results. Otherwise, the returned list
     *  may contain only those action groups that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getActionGroups()</code>.
     *
     *  @param  actionGroupGenusType an actionGroup genus type 
     *  @return the returned <code>ActionGroup</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>actionGroupGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.ActionGroupList getActionGroupsByParentGenusType(org.osid.type.Type actionGroupGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.control.actiongroup.FederatingActionGroupList ret = getActionGroupList();

        for (org.osid.control.ActionGroupLookupSession session : getSessions()) {
            ret.addActionGroupList(session.getActionGroupsByParentGenusType(actionGroupGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>ActionGroupList</code> containing the given
     *  action group record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  action groups or an error results. Otherwise, the returned list
     *  may contain only those action groups that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getActionGroups()</code>.
     *
     *  @param  actionGroupRecordType an actionGroup record type 
     *  @return the returned <code>ActionGroup</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>actionGroupRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.ActionGroupList getActionGroupsByRecordType(org.osid.type.Type actionGroupRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.control.actiongroup.FederatingActionGroupList ret = getActionGroupList();

        for (org.osid.control.ActionGroupLookupSession session : getSessions()) {
            ret.addActionGroupList(session.getActionGroupsByRecordType(actionGroupRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code> ActionGroupList </code> containing of the given <code>
     *  Action. </code>
     *
     *  In plenary mode, the returned list contains all known action
     *  groups or an error results. Otherwise, the returned list may
     *  contain only those action groups that are accessible through
     *  this session.
     *
     *  @param  actionId an action <code> Id </code>
     *  @return the returned <code> ActionGroup </code> list
     *  @throws org.osid.NullArgumentException <code> actionId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.control.ActionGroupList getActionGroupsByAction(org.osid.id.Id actionId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.control.actiongroup.FederatingActionGroupList ret = getActionGroupList();

        for (org.osid.control.ActionGroupLookupSession session : getSessions()) {
            ret.addActionGroupList(session.getActionGroupsByAction(actionId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>ActionGroups</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  action groups or an error results. Otherwise, the returned list
     *  may contain only those action groups that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>ActionGroups</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.ActionGroupList getActionGroups()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.control.actiongroup.FederatingActionGroupList ret = getActionGroupList();

        for (org.osid.control.ActionGroupLookupSession session : getSessions()) {
            ret.addActionGroupList(session.getActionGroups());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.control.actiongroup.FederatingActionGroupList getActionGroupList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.control.actiongroup.ParallelActionGroupList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.control.actiongroup.CompositeActionGroupList());
        }
    }
}
