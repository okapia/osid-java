//
// AbstractMutableLesson.java
//
//     Defines a mutable Lesson.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.plan.lesson.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Lesson</code>.
 */

public abstract class AbstractMutableLesson
    extends net.okapia.osid.jamocha.course.plan.lesson.spi.AbstractLesson
    implements org.osid.course.plan.Lesson,
               net.okapia.osid.jamocha.builder.course.plan.lesson.LessonMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this lesson. 
     *
     *  @param record lesson record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addLessonRecord(org.osid.course.plan.records.LessonRecord record, org.osid.type.Type recordType) {
        super.addLessonRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the start date for when this lesson is effective.
     *
     *  @param date the start date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setStartDate(org.osid.calendaring.DateTime date) {
        super.setStartDate(date);
        return;
    }


    /**
     *  Sets the end date for when this lesson ceases to be
     *  effective.
     *
     *  @param date the end date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setEndDate(org.osid.calendaring.DateTime date) {
        super.setEndDate(date);
        return;
    }


    /**
     *  Sets the display name for this lesson.
     *
     *  @param displayName the name for this lesson
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this lesson.
     *
     *  @param description the description of this lesson
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the end state for the expiration of this lesson
     *  relationship.
     *
     *  @param state the end state
     *  @throws org.osid.NullArgumentException <code>state</code> is
     *          <code>null</code>
     */

    @Override
    public void setEndReason(org.osid.process.State state) {
        super.setEndReason(state);
        return;
    }


    /**
     *  Sets the plan.
     *
     *  @param plan a plan
     *  @throws org.osid.NullArgumentException <code>plan</code> is
     *          <code>null</code>
     */

    @Override
    public void setPlan(org.osid.course.plan.Plan plan) {
        super.setPlan(plan);
        return;
    }


    /**
     *  Sets the docet.
     *
     *  @param docet a docet
     *  @throws org.osid.NullArgumentException <code>docet</code> is
     *          <code>null</code>
     */

    @Override
    public void setDocet(org.osid.course.syllabus.Docet docet) {
        super.setDocet(docet);
        return;
    }


    /**
     *  Adds an activity.
     *
     *  @param activity an activity
     *  @throws org.osid.NullArgumentException <code>activity</code>
     *          is <code>null</code>
     */

    @Override
    public void addActivity(org.osid.course.Activity activity) {
        super.addActivity(activity);
        return;
    }


    /**
     *  Sets all the activities.
     *
     *  @param activities a collection of activities
     *  @throws org.osid.NullArgumentException
     *          <code>activities</code> is <code>null</code>
     */

    @Override
    public void setActivities(java.util.Collection<org.osid.course.Activity> activities) {
        super.setActivities(activities);
        return;
    }


    /**
     *  Sets the planned start time.
     *
     *  @param time a planned start time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    @Override
    public void setPlannedStartTime(org.osid.calendaring.Duration time) {
        super.setPlannedStartTime(time);
        return;
    }


    /**
     *  Sets the actual start time.
     *
     *  @param time an actual start time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    @Override
    public void setActualStartTime(org.osid.calendaring.Duration time) {
        super.setActualStartTime(time);
        return;
    }


    /**
     *  Sets the actual starting activity.
     *
     *  @param activity an actual starting activity
     *  @throws org.osid.NullArgumentException <code>activity</code>
     *          is <code>null</code>
     */

    @Override
    public void setActualStartingActivity(org.osid.course.Activity activity) {
        super.setActualStartingActivity(activity);
        return;
    }


    /**
     *  Sets the complete flag.
     *
     *  @param complete <code>true</code> if this lesson is complete,
     *         <code>false</code> if not completed
     */

    @Override
    public void setComplete(boolean complete) {
        super.setComplete(complete);
        return;
    }


    /**
     *  Sets the skipped flag.
     *
     *  @param skipped <code>true</code> if this lesson is skipped,
     *         <code>false</code> if not completed
     */

    @Override
    public void setSkipped(boolean skipped) {
        super.setSkipped(skipped);
        return;
    }


    /**
     *  Sets the actual end time.
     *
     *  @param time an actual end time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    @Override
    public void setActualEndTime(org.osid.calendaring.Duration time) {
        super.setActualEndTime(time);
        return;
    }


    /**
     *  Sets the actual ending activity.
     *
     *  @param activity an actual ending activity
     *  @throws org.osid.NullArgumentException <code>activity</code>
     *          is <code>null</code>
     */

    @Override
    public void setActualEndingActivity(org.osid.course.Activity activity) {
        super.setActualEndingActivity(activity);
        return;
    }


    /**
     *  Sets the actual time spent.
     *
     *  @param timeSpent an actual time spent
     *  @throws org.osid.NullArgumentException <code>timeSpent</code>
     *          is <code>null</code>
     */

    @Override
    public void setActualTimeSpent(org.osid.calendaring.Duration timeSpent) {
        super.setActualTimeSpent(timeSpent);
        return;
    }
}

