//
// AbstractTermQuery.java
//
//     A template for making a Term Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.term.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for terms.
 */

public abstract class AbstractTermQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.course.TermQuery {

    private final java.util.Collection<org.osid.course.records.TermQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Adds a display label for this query. 
     *
     *  @param  label label string to match 
     *  @param  stringMatchType the label match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> label </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> label </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchDisplayLabel(String label, 
                                  org.osid.type.Type stringMatchType, 
                                  boolean match) {
        return;
    }


    /**
     *  Matches a display label that has any value. 
     *
     *  @param  match <code> true </code> to match courses with any label, 
     *          <code> false </code> to match assets with no title 
     */

    @OSID @Override
    public void matchAnyDisplayLabel(boolean match) {
        return;
    }


    /**
     *  Clears the display label query terms. 
     */

    @OSID @Override
    public void clearDisplayLabelTerms() {
        return;
    }


    /**
     *  Matches the open date between the given range inclusive. 
     *
     *  @param  low low date range 
     *  @param  high high date range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchOpenDate(org.osid.calendaring.DateTime low, 
                              org.osid.calendaring.DateTime high, 
                              boolean match) {
        return;
    }


    /**
     *  Matches a term that has any open date assigned. 
     *
     *  @param  match <code> true </code> to match terms with any open date, 
     *          <code> false </code> to match terms with no open date 
     */

    @OSID @Override
    public void matchAnyOpenDate(boolean match) {
        return;
    }


    /**
     *  Clears the open date query terms. 
     */

    @OSID @Override
    public void clearOpenDateTerms() {
        return;
    }


    /**
     *  Matches the registration start date between the given range inclusive. 
     *
     *  @param  low low date range 
     *  @param  high high date range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchRegistrationStart(org.osid.calendaring.DateTime low, 
                                       org.osid.calendaring.DateTime high, 
                                       boolean match) {
        return;
    }


    /**
     *  Matches a term that has any registration start date assigned. 
     *
     *  @param  match <code> true </code> to match terms with any registration 
     *          start date, <code> false </code> to match terms with no 
     *          registration start date 
     */

    @OSID @Override
    public void matchAnyRegistrationStart(boolean match) {
        return;
    }


    /**
     *  Clears the registration start date query terms. 
     */

    @OSID @Override
    public void clearRegistrationStartTerms() {
        return;
    }


    /**
     *  Matches the registration end date between the given range inclusive. 
     *
     *  @param  low low date range 
     *  @param  high high date range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchRegistrationEnd(org.osid.calendaring.DateTime low, 
                                     org.osid.calendaring.DateTime high, 
                                     boolean match) {
        return;
    }


    /**
     *  Matches a term that has any registration end date assigned. 
     *
     *  @param  match <code> true </code> to match terms with any registration 
     *          end date, <code> false </code> to match terms with no 
     *          registration end date 
     */

    @OSID @Override
    public void matchAnyRegistrationEnd(boolean match) {
        return;
    }


    /**
     *  Clears the registration end date query terms. 
     */

    @OSID @Override
    public void clearRegistrationEndTerms() {
        return;
    }


    /**
     *  Matches the registration period that incudes the given date. 
     *
     *  @param  date a date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchRegistrationPeriod(org.osid.calendaring.DateTime date, 
                                        boolean match) {
        return;
    }


    /**
     *  Clears the registration period query terms. 
     */

    @OSID @Override
    public void clearRegistrationPeriodTerms() {
        return;
    }


    /**
     *  Matches the registration period duration between the given range 
     *  inclusive. 
     *
     *  @param  low low duration range 
     *  @param  high high duration range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchRegistrationDuration(org.osid.calendaring.Duration low, 
                                          org.osid.calendaring.Duration high, 
                                          boolean match) {
        return;
    }


    /**
     *  Clears the registration period duration query terms. 
     */

    @OSID @Override
    public void clearRegistrationDurationTerms() {
        return;
    }


    /**
     *  Matches the class start date between the given range inclusive. 
     *
     *  @param  low low date range 
     *  @param  high high date range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchClassesStart(org.osid.calendaring.DateTime low, 
                                  org.osid.calendaring.DateTime high, 
                                  boolean match) {
        return;
    }


    /**
     *  Matches a term that has any class start date assigned. 
     *
     *  @param  match <code> true </code> to match terms with any class start 
     *          date, <code> false </code> to match terms with no class start 
     *          date 
     */

    @OSID @Override
    public void matchAnyClassesStart(boolean match) {
        return;
    }


    /**
     *  Clears the class start date query terms. 
     */

    @OSID @Override
    public void clearClassesStartTerms() {
        return;
    }


    /**
     *  Matches the class end date between the given range inclusive. 
     *
     *  @param  low low date range 
     *  @param  high high date range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchClassesEnd(org.osid.calendaring.DateTime low, 
                                org.osid.calendaring.DateTime high, 
                                boolean match) {
        return;
    }


    /**
     *  Matches a term that has any class end date assigned. 
     *
     *  @param  match <code> true </code> to match terms with any class end 
     *          date, <code> false </code> to match terms with no class end 
     *          date 
     */

    @OSID @Override
    public void matchAnyClassesEnd(boolean match) {
        return;
    }


    /**
     *  Clears the class end date query terms. 
     */

    @OSID @Override
    public void clearClassesEndTerms() {
        return;
    }


    /**
     *  Matches the class period that incudes the given date. 
     *
     *  @param  date a date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchClassesPeriod(org.osid.calendaring.DateTime date, 
                                   boolean match) {
        return;
    }


    /**
     *  Clears the class period query terms. 
     */

    @OSID @Override
    public void clearClassesPeriodTerms() {
        return;
    }


    /**
     *  Matches the classes period duration between the given range inclusive. 
     *
     *  @param  low low duration range 
     *  @param  high high duration range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchClassesDuration(org.osid.calendaring.Duration low, 
                                     org.osid.calendaring.Duration high, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the classes period duration query terms. 
     */

    @OSID @Override
    public void clearClassesDurationTerms() {
        return;
    }


    /**
     *  Matches the add date between the given range inclusive. 
     *
     *  @param  low low date range 
     *  @param  high high date range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchAddDate(org.osid.calendaring.DateTime low, 
                             org.osid.calendaring.DateTime high, 
                             boolean match) {
        return;
    }


    /**
     *  Matches a term that has any add date assigned. 
     *
     *  @param  match <code> true </code> to match terms with any add 
     *          date, <code> false </code> to match terms with no add
     *          date 
     */

    @OSID @Override
    public void matchAnyAddDate(boolean match) {
        return;
    }


    /**
     *  Clears the add date query terms. 
     */

    @OSID @Override
    public void clearAddDateTerms() {
        return;
    }


    /**
     *  Matches the drop date between the given range inclusive. 
     *
     *  @param  low low date range 
     *  @param  high high date range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchDropDate(org.osid.calendaring.DateTime low, 
                             org.osid.calendaring.DateTime high, 
                             boolean match) {
        return;
    }


    /**
     *  Matches a term that has any drop date assigned. 
     *
     *  @param  match <code> true </code> to match terms with any drop 
     *          date, <code> false </code> to match terms with no drop
     *          date 
     */

    @OSID @Override
    public void matchAnyDropDate(boolean match) {
        return;
    }


    /**
     *  Clears the drop date query terms. 
     */

    @OSID @Override
    public void clearDropDateTerms() {
        return;
    }


    /**
     *  Matches the final exam period start date between the given range 
     *  inclusive. 
     *
     *  @param  low low date range 
     *  @param  high high date range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchFinalExamStart(org.osid.calendaring.DateTime low, 
                                    org.osid.calendaring.DateTime high, 
                                    boolean match) {
        return;
    }


    /**
     *  Matches a term that has any final exam period start date assigned. 
     *
     *  @param  match <code> true </code> to match terms with any final exam 
     *          start date, <code> false </code> to match terms with no final 
     *          exam start date 
     */

    @OSID @Override
    public void matchAnyFinalExamStart(boolean match) {
        return;
    }


    /**
     *  Clears the final exam start date query terms. 
     */

    @OSID @Override
    public void clearFinalExamStartTerms() {
        return;
    }


    /**
     *  Matches the final exam period end date between the given range 
     *  inclusive. 
     *
     *  @param  low low date range 
     *  @param  high high date range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchFinalExamEnd(org.osid.calendaring.DateTime low, 
                                  org.osid.calendaring.DateTime high, 
                                  boolean match) {
        return;
    }


    /**
     *  Matches a term that has any final exam period end date assigned. 
     *
     *  @param  match <code> true </code> to match terms with any final exam 
     *          end date, <code> false </code> to match terms with no final 
     *          exam end date 
     */

    @OSID @Override
    public void matchAnyFinalExamEnd(boolean match) {
        return;
    }


    /**
     *  Clears the final exam period end date query terms. 
     */

    @OSID @Override
    public void clearFinalExamEndTerms() {
        return;
    }


    /**
     *  Matches the final exam period that incudes the given date. 
     *
     *  @param  date a date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchFinalExamPeriod(org.osid.calendaring.DateTime date, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the final exam period query terms. 
     */

    @OSID @Override
    public void clearFinalExamPeriodTerms() {
        return;
    }


    /**
     *  Matches the final exam period duration between the given range 
     *  inclusive. 
     *
     *  @param  low low duration range 
     *  @param  high high duration range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchFinalExamDuration(org.osid.calendaring.Duration low, 
                                       org.osid.calendaring.Duration high, 
                                       boolean match) {
        return;
    }


    /**
     *  Clears the final exam period duration query terms. 
     */

    @OSID @Override
    public void clearFinalExamDurationTerms() {
        return;
    }


    /**
     *  Matches the close date between the given range inclusive. 
     *
     *  @param  low low date range 
     *  @param  high high date range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchCloseDate(org.osid.calendaring.DateTime low, 
                               org.osid.calendaring.DateTime high, 
                               boolean match) {
        return;
    }


    /**
     *  Matches a term that has any close date assigned. 
     *
     *  @param  match <code> true </code> to match terms with any close date, 
     *          <code> false </code> to match terms with no close date 
     */

    @OSID @Override
    public void matchAnyCloseDate(boolean match) {
        return;
    }


    /**
     *  Clears the close date query terms. 
     */

    @OSID @Override
    public void clearCloseDateTerms() {
        return;
    }


    /**
     *  Sets the term <code> Id </code> for this query to match terms that 
     *  have the specified term as an ancestor. 
     *
     *  @param  termId a term <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> termId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchAncestorTermId(org.osid.id.Id termId, boolean match) {
        return;
    }


    /**
     *  Clears the ancestor term <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorTermIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> TermQuery </code> is available. 
     *
     *  @return <code> true </code> if a termency query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorTermQuery() {
        return (false);
    }


    /**
     *  Gets the query for a term. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the term query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorTermQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermQuery getAncestorTermQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorTermQuery() is false");
    }


    /**
     *  Matches terms with any ancestor. 
     *
     *  @param  match <code> true </code> to match terms with any ancestor, 
     *          <code> false </code> to match root terms 
     */

    @OSID @Override
    public void matchAnyAncestorTerm(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor term query terms. 
     */

    @OSID @Override
    public void clearAncestorTermTerms() {
        return;
    }


    /**
     *  Sets the term <code> Id </code> for this query to match terms that 
     *  have the specified term as a descendant. 
     *
     *  @param  termId a term <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> termId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchDescendantTermId(org.osid.id.Id termId, boolean match) {
        return;
    }


    /**
     *  Clears the descendant term <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantTermIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> TermQuery </code> is available. 
     *
     *  @return <code> true </code> if a termency query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantTermQuery() {
        return (false);
    }


    /**
     *  Gets the query for a term. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the term query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantTermQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermQuery getDescendantTermQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantTermQuery() is false");
    }


    /**
     *  Matches terms with any descendant. 
     *
     *  @param  match <code> true </code> to match terms with any Ddscendant, 
     *          <code> false </code> to match leaf terms 
     */

    @OSID @Override
    public void matchAnyDescendantTerm(boolean match) {
        return;
    }


    /**
     *  Clears the descendant term query terms. 
     */

    @OSID @Override
    public void clearDescendantTermTerms() {
        return;
    }


    /**
     *  Sets the course offering <code> Id </code> for this query to match 
     *  terms assigned to course offerings. 
     *
     *  @param  courseOfferingId the course offering <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseOfferingId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseOfferingId(org.osid.id.Id courseOfferingId, 
                                      boolean match) {
        return;
    }


    /**
     *  Clears the course offering <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCourseOfferingIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CourseOfferingQuery </code> is available. 
     *
     *  @return <code> true </code> if a course offering query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseOfferingQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course offering. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course offering query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingQuery getCourseOfferingQuery() {
        throw new org.osid.UnimplementedException("supportsCourseOfferingQuery() is false");
    }


    /**
     *  Matches courses that have any course offering. 
     *
     *  @param  match <code> true </code> to match terms with any course 
     *          offering, <code> false </code> to match terms with no course 
     *          offerings 
     */

    @OSID @Override
    public void matchAnyCourseOffering(boolean match) {
        return;
    }


    /**
     *  Clears the course offering query terms. 
     */

    @OSID @Override
    public void clearCourseOfferingTerms() {
        return;
    }


    /**
     *  Sets the course catalog <code> Id </code> for this query to match 
     *  terms assigned to course catalogs. 
     *
     *  @param  courseCatalogId the course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog query terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given term query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a term implementing the requested record.
     *
     *  @param termRecordType a term record type
     *  @return the term query record
     *  @throws org.osid.NullArgumentException
     *          <code>termRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(termRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.TermQueryRecord getTermQueryRecord(org.osid.type.Type termRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.records.TermQueryRecord record : this.records) {
            if (record.implementsRecordType(termRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(termRecordType + " is not supported");
    }


    /**
     *  Adds a record to this term query. 
     *
     *  @param termQueryRecord term query record
     *  @param termRecordType term record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addTermQueryRecord(org.osid.course.records.TermQueryRecord termQueryRecord, 
                                          org.osid.type.Type termRecordType) {

        addRecordType(termRecordType);
        nullarg(termQueryRecord, "term query record");
        this.records.add(termQueryRecord);        
        return;
    }
}
