//
// AbstractAssessmentPart.java
//
//     Defines an AssessmentPart.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.authoring.assessmentpart.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>AssessmentPart</code>.
 */

public abstract class AbstractAssessmentPart
    extends net.okapia.osid.jamocha.spi.AbstractOperableOsidObject
    implements org.osid.assessment.authoring.AssessmentPart {

    private org.osid.assessment.Assessment assessment;
    private org.osid.assessment.authoring.AssessmentPart parent;
    private boolean section = false;
    private long weight = 1;
    private org.osid.calendaring.Duration duration;

    private final java.util.Collection<org.osid.assessment.authoring.AssessmentPart> children = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.assessment.authoring.records.AssessmentPartRecord> records = new java.util.LinkedHashSet<>();

    private final Containable containable = new Containable();


    /**
     *  Tests if this <code> Containable </code> is sequestered in
     *  that it should not appear outside of its aggregated
     *  composition.
     *
     *  @return <code> true </code> if this containable is
     *          sequestered, <code> false </code> if this containable
     *          may appear outside its aggregate
     */

    @OSID @Override
    public boolean isSequestered() {
        return (this.containable.isSequestered());
    }


    /**
     *  Sets the sequestered flag.
     *
     *  @param sequestered <code> true </code> if this containable is
     *         sequestered, <code> false </code> if this containable
     *         may appear outside its aggregate
     */

    protected void setSequestered(boolean sequestered) {
        this.containable.setSequestered(sequestered);
        return;
    }


    /**
     *  Gets the assessment <code> Id </code> to which this rule
     *  belongs.
     *
     *  @return <code> Id </code> of an assessment 
     */

    @OSID @Override
    public org.osid.id.Id getAssessmentId() {
        return (this.assessment.getId());
    }


    /**
     *  Gets the assessment to which this rule belongs. 
     *
     *  @return an assessment 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.assessment.Assessment getAssessment()
        throws org.osid.OperationFailedException {

        return (this.assessment);
    }


    /**
     *  Sets the assessment.
     *
     *  @param assessment an assessment
     *  @throws org.osid.NullArgumentException <code>assessment</code>
     *          is <code>null</code>
     */

    protected void setAssessment(org.osid.assessment.Assessment assessment) {
        nullarg(assessment, "assessment");
        this.assessment = assessment;
        return;
    }


    /**
     *  Tests if this assessment part belongs to a parent assessment part. 
     *
     *  @return <code> true </code> if this part has a parent, <code>
     *          false </code> if a root
     */

    @OSID @Override
    public boolean hasParentPart() {
        return (this.parent != null);
    }


    /**
     *  Gets the parent assessment <code> Id. </code> 
     *
     *  @return <code> Id </code> of an assessment 
     *  @throws org.osid.IllegalStateException <code> hasParentPart() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAssessmentPartId() {
        if (!hasParentPart()) {
            throw new org.osid.IllegalStateException("hasParentPart() is false");
        }

        return (this.parent.getId());
    }


    /**
     *  Gets the parent assessment. 
     *
     *  @return the parent assessment part 
     *  @throws org.osid.IllegalStateException <code> hasParentPart()
     *          </code> is <code> false </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPart getAssessmentPart()
        throws org.osid.OperationFailedException {

        if (!hasParentPart()) {
            throw new org.osid.IllegalStateException("hasParentPart() is false");
        }

        return (this.parent);
    }


    /**
     *  Sets the parent assessment part.
     *
     *  @param parent an assessment part
     *  @throws org.osid.NullArgumentException <code>parent</code> is
     *          <code>null</code>
     */

    protected void setParentAssessmentPart(org.osid.assessment.authoring.AssessmentPart parent) {
        nullarg(parent, "parent assessment part");
        this.parent = parent;
        return;
    }


    /**
     *  Tests if this part should be visible as a section in an assessment. If 
     *  visible, this part will appear to the user as a separate section of 
     *  the assessment. Typically, a section may not be under a non-sectioned 
     *  part. 
     *
     *  @return <code> true </code> if this part is a section, <code>
     *          false </code> otherwise
     */

    @OSID @Override
    public boolean isSection() {
        return (this.section);
    }


    /**
     *  Sets the section.
     *
     *  @param section <code>true</code> if a section,
     *         <code>false</code> otherwise
     */

    protected void setSection(boolean section) {
        this.section = section;
        return;
    }


    /**
     *  Gets an integral weight factor for this assessment part used
     *  for scoring. The percentage weight for this part is this
     *  weight divided by the sum total of all the weights in the
     *  assessment.
     *
     *  @return the weight 
     */

    @OSID @Override
    public long getWeight() {
        return (this.weight);
    }


    /**
     *  Sets the weight.
     *
     *  @param weight a weight
     */

    protected void setWeight(long weight) {
        cardinalarg(weight, "weight");
        this.weight = weight;
        return;
    }


    /**
     *  Gets the allocated time for this part. The allocated time may
     *  be used to assign fixed time limits to each item or can be
     *  used to estimate the total assessment time.
     *
     *  @return allocated time 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getAllocatedTime() {
        return (this.duration);
    }


    /**
     *  Sets the allocated time.
     *
     *  @param duration an allocated time
     *  @throws org.osid.NullArgumentException <code>duration</code>
     *          is <code>null</code>
     */

    protected void setAllocatedTime(org.osid.calendaring.Duration duration) {
        nullarg(duration, "duration");
        this.duration = duration;
        return;
    }


    /**
     *  Gets any child assessment part <code> Ids. </code> 
     *
     *  @return <code> Ids </code> of the child assessment parts 
     */

    @OSID @Override
    public org.osid.id.IdList getChildAssessmentPartIds() {
        try {
            org.osid.assessment.authoring.AssessmentPartList children = getChildAssessmentParts();
            return (new net.okapia.osid.jamocha.adapter.converter.assessment.authoring.assessmentpart.AssessmentPartToIdList(children));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets any child assessment parts. 
     *
     *  @return the child assessment parts 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartList getChildAssessmentParts()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.assessment.authoring.assessmentpart.ArrayAssessmentPartList(this.children));
    }


    /**
     *  Adds a child assessment part.
     *
     *  @param child a child assessment part
     *  @throws org.osid.NullArgumentException <code>child</code> is
     *          <code>null</code>
     */

    protected void addChildAssessmentPart(org.osid.assessment.authoring.AssessmentPart child) {
        nullarg(child, "child assessment part");
        this.children.add(child);
        return;
    }


    /**
     *  Sets all the child assessment parts.
     *
     *  @param children a collection of child assessment parts
     *  @throws org.osid.NullArgumentException <code>children</code>
     *          is <code>null</code>
     */

    protected void setChildAssessmentParts(java.util.Collection<org.osid.assessment.authoring.AssessmentPart> children) {
        nullarg(children, "chil assessment parts");

        this.children.clear();
        this.children.addAll(children);

        return;
    }


    /**
     *  Tests if this assessment part supports the given record
     *  <code>Type</code>.
     *
     *  @param assessmentPartRecordType an assessment part record type
     *  @return <code>true</code> if the assessmentPartRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentPartRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type assessmentPartRecordType) {
        for (org.osid.assessment.authoring.records.AssessmentPartRecord record : this.records) {
            if (record.implementsRecordType(assessmentPartRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>AssessmentPart</code> record <code>Type</code>.
     *
     *  @param  assessmentPartRecordType the assessment part record type 
     *  @return the assessment part record 
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentPartRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assessmentPartRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.authoring.records.AssessmentPartRecord getAssessmentPartRecord(org.osid.type.Type assessmentPartRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.authoring.records.AssessmentPartRecord record : this.records) {
            if (record.implementsRecordType(assessmentPartRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assessmentPartRecordType + " is not supported");
    }


    /**
     *  Adds a record to this assessment part. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param assessmentPartRecord the assessment part record
     *  @param assessmentPartRecordType assessment part record type
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentPartRecord</code> or
     *          <code>assessmentPartRecordType</code> is
     *          <code>null</code>
     */
            
    protected void addAssessmentPartRecord(org.osid.assessment.authoring.records.AssessmentPartRecord assessmentPartRecord, 
                                           org.osid.type.Type assessmentPartRecordType) {

        nullarg(assessmentPartRecord, "assessment part record");
        addRecordType(assessmentPartRecordType);
        this.records.add(assessmentPartRecord);
        
        return;
    }


    protected class Containable
        extends net.okapia.osid.jamocha.spi.AbstractContainable
        implements org.osid.Containable {

        
        /**
         *  Sets the sequestered flag.
         *
         *  @param sequestered <code> true </code> if this containable is
         *         sequestered, <code> false </code> if this containable
         *         may appear outside its aggregate
         */
        
        @Override
        protected void setSequestered(boolean sequestered) {
            super.setSequestered(sequestered);
            return;
        }
    }
}
