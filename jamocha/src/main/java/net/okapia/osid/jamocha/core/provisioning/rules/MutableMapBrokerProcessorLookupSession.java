//
// MutableMapBrokerProcessorLookupSession
//
//    Implements a BrokerProcessor lookup service backed by a collection of
//    brokerProcessors that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.rules;


/**
 *  Implements a BrokerProcessor lookup service backed by a collection of
 *  broker processors. The broker processors are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of broker processors can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapBrokerProcessorLookupSession
    extends net.okapia.osid.jamocha.core.provisioning.rules.spi.AbstractMapBrokerProcessorLookupSession
    implements org.osid.provisioning.rules.BrokerProcessorLookupSession {


    /**
     *  Constructs a new {@code MutableMapBrokerProcessorLookupSession}
     *  with no broker processors.
     *
     *  @param distributor the distributor
     *  @throws org.osid.NullArgumentException {@code distributor} is
     *          {@code null}
     */

      public MutableMapBrokerProcessorLookupSession(org.osid.provisioning.Distributor distributor) {
        setDistributor(distributor);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapBrokerProcessorLookupSession} with a
     *  single brokerProcessor.
     *
     *  @param distributor the distributor  
     *  @param brokerProcessor a broker processor
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code brokerProcessor} is {@code null}
     */

    public MutableMapBrokerProcessorLookupSession(org.osid.provisioning.Distributor distributor,
                                           org.osid.provisioning.rules.BrokerProcessor brokerProcessor) {
        this(distributor);
        putBrokerProcessor(brokerProcessor);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapBrokerProcessorLookupSession}
     *  using an array of broker processors.
     *
     *  @param distributor the distributor
     *  @param brokerProcessors an array of broker processors
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code brokerProcessors} is {@code null}
     */

    public MutableMapBrokerProcessorLookupSession(org.osid.provisioning.Distributor distributor,
                                           org.osid.provisioning.rules.BrokerProcessor[] brokerProcessors) {
        this(distributor);
        putBrokerProcessors(brokerProcessors);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapBrokerProcessorLookupSession}
     *  using a collection of broker processors.
     *
     *  @param distributor the distributor
     *  @param brokerProcessors a collection of broker processors
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code brokerProcessors} is {@code null}
     */

    public MutableMapBrokerProcessorLookupSession(org.osid.provisioning.Distributor distributor,
                                           java.util.Collection<? extends org.osid.provisioning.rules.BrokerProcessor> brokerProcessors) {

        this(distributor);
        putBrokerProcessors(brokerProcessors);
        return;
    }

    
    /**
     *  Makes a {@code BrokerProcessor} available in this session.
     *
     *  @param brokerProcessor a broker processor
     *  @throws org.osid.NullArgumentException {@code brokerProcessor{@code  is
     *          {@code null}
     */

    @Override
    public void putBrokerProcessor(org.osid.provisioning.rules.BrokerProcessor brokerProcessor) {
        super.putBrokerProcessor(brokerProcessor);
        return;
    }


    /**
     *  Makes an array of broker processors available in this session.
     *
     *  @param brokerProcessors an array of broker processors
     *  @throws org.osid.NullArgumentException {@code brokerProcessors{@code 
     *          is {@code null}
     */

    @Override
    public void putBrokerProcessors(org.osid.provisioning.rules.BrokerProcessor[] brokerProcessors) {
        super.putBrokerProcessors(brokerProcessors);
        return;
    }


    /**
     *  Makes collection of broker processors available in this session.
     *
     *  @param brokerProcessors a collection of broker processors
     *  @throws org.osid.NullArgumentException {@code brokerProcessors{@code  is
     *          {@code null}
     */

    @Override
    public void putBrokerProcessors(java.util.Collection<? extends org.osid.provisioning.rules.BrokerProcessor> brokerProcessors) {
        super.putBrokerProcessors(brokerProcessors);
        return;
    }


    /**
     *  Removes a BrokerProcessor from this session.
     *
     *  @param brokerProcessorId the {@code Id} of the broker processor
     *  @throws org.osid.NullArgumentException {@code brokerProcessorId{@code 
     *          is {@code null}
     */

    @Override
    public void removeBrokerProcessor(org.osid.id.Id brokerProcessorId) {
        super.removeBrokerProcessor(brokerProcessorId);
        return;
    }    
}
