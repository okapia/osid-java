//
// AbstractAccountList
//
//     Implements a filter for an AccountList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.financials.account.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for an AccountList. Subclasses call this
 *  constructor and implement the <code>pass()</code> method. This
 *  filter is synchronous but can be wrapped in a BufferedAccountList
 *  to improve performance.
 */

public abstract class AbstractAccountFilterList
    extends net.okapia.osid.jamocha.financials.account.spi.AbstractAccountList
    implements org.osid.financials.AccountList,
               net.okapia.osid.jamocha.inline.filter.financials.account.AccountFilter {

    private org.osid.financials.Account account;
    private final org.osid.financials.AccountList list;
    private org.osid.OsidException error;


    /**
     *  Creates a new <code>AbstractAccountFilterList</code>.
     *
     *  @param accountList an <code>AccountList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>accountList</code> is <code>null</code>
     */

    protected AbstractAccountFilterList(org.osid.financials.AccountList accountList) {
        nullarg(accountList, "account list");
        this.list = accountList;
        return;
    }    

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in
     *          this list, <code> false </code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (hasError()) {
            return (true);
        }

        prime();

        if (this.account == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code> Account </code> in this list. 
     *
     *  @return the next <code> Account </code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code> Account </code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.financials.Account getNextAccount()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        if (hasNext()) {
            org.osid.financials.Account account = this.account;
            this.account = null;
            return (account);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in account list");
        }
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.account = null;
        this.list.close();
        return;
    }

    
    /**
     *  Filters Accounts.
     *
     *  @param account the account to filter
     *  @return <code>true</code> if the account passes the filter,
     *          <code>false</code> if the account should be filtered
     */

    public abstract boolean pass(org.osid.financials.Account account);


    protected void prime() {
        if (this.account != null) {
            return;
        }

        org.osid.financials.Account account = null;

        while (this.list.hasNext()) {
            try {
                account = this.list.getNextAccount();
            } catch (org.osid.OsidException oe) {
                error(oe);
                return;
            }

            if (pass(account)) {
                this.account = account;
                return;
            }
        }

        return;
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    protected void error(org.osid.OsidException error) {
        this.error = error;
        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    protected boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }
}
