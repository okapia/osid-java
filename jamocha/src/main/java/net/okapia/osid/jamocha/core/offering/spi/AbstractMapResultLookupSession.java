//
// AbstractMapResultLookupSession
//
//    A simple framework for providing a Result lookup service
//    backed by a fixed collection of results.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.offering.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Result lookup service backed by a
 *  fixed collection of results. The results are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Results</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapResultLookupSession
    extends net.okapia.osid.jamocha.offering.spi.AbstractResultLookupSession
    implements org.osid.offering.ResultLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.offering.Result> results = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.offering.Result>());


    /**
     *  Makes a <code>Result</code> available in this session.
     *
     *  @param  result a result
     *  @throws org.osid.NullArgumentException <code>result<code>
     *          is <code>null</code>
     */

    protected void putResult(org.osid.offering.Result result) {
        this.results.put(result.getId(), result);
        return;
    }


    /**
     *  Makes an array of results available in this session.
     *
     *  @param  results an array of results
     *  @throws org.osid.NullArgumentException <code>results<code>
     *          is <code>null</code>
     */

    protected void putResults(org.osid.offering.Result[] results) {
        putResults(java.util.Arrays.asList(results));
        return;
    }


    /**
     *  Makes a collection of results available in this session.
     *
     *  @param  results a collection of results
     *  @throws org.osid.NullArgumentException <code>results<code>
     *          is <code>null</code>
     */

    protected void putResults(java.util.Collection<? extends org.osid.offering.Result> results) {
        for (org.osid.offering.Result result : results) {
            this.results.put(result.getId(), result);
        }

        return;
    }


    /**
     *  Removes a Result from this session.
     *
     *  @param  resultId the <code>Id</code> of the result
     *  @throws org.osid.NullArgumentException <code>resultId<code> is
     *          <code>null</code>
     */

    protected void removeResult(org.osid.id.Id resultId) {
        this.results.remove(resultId);
        return;
    }


    /**
     *  Gets the <code>Result</code> specified by its <code>Id</code>.
     *
     *  @param  resultId <code>Id</code> of the <code>Result</code>
     *  @return the result
     *  @throws org.osid.NotFoundException <code>resultId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>resultId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Result getResult(org.osid.id.Id resultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.offering.Result result = this.results.get(resultId);
        if (result == null) {
            throw new org.osid.NotFoundException("result not found: " + resultId);
        }

        return (result);
    }


    /**
     *  Gets all <code>Results</code>. In plenary mode, the returned
     *  list contains all known results or an error
     *  results. Otherwise, the returned list may contain only those
     *  results that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Results</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ResultList getResults()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.offering.result.ArrayResultList(this.results.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.results.clear();
        super.close();
        return;
    }
}
