//
// AbstractStatistic.java
//
//     Defines a Statistic builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.metering.statistic.spi;


/**
 *  Defines a <code>Statistic</code> builder.
 */

public abstract class AbstractStatisticBuilder<T extends AbstractStatisticBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidCompendiumBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.metering.statistic.StatisticMiter statistic;


    /**
     *  Constructs a new <code>AbstractStatisticBuilder</code>.
     *
     *  @param statistic the statistic to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractStatisticBuilder(net.okapia.osid.jamocha.builder.metering.statistic.StatisticMiter statistic) {
        super(statistic);
        this.statistic = statistic;
        return;
    }


    /**
     *  Builds the statistic.
     *
     *  @return the new statistic
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.metering.Statistic build() {
        (new net.okapia.osid.jamocha.builder.validator.metering.statistic.StatisticValidator(getValidations())).validate(this.statistic);
        return (new net.okapia.osid.jamocha.builder.metering.statistic.ImmutableStatistic(this.statistic));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the statistic miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.metering.statistic.StatisticMiter getMiter() {
        return (this.statistic);
    }


    /**
     *  Sets the meter.
     *
     *  @param meter the meter
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>meter</code> is <code>null</code>
     */

    public T meter(org.osid.metering.Meter meter) {
        getMiter().setMeter(meter);
        return (self());
    }


    /**
     *  Sets the metered object id.
     *
     *  @param objectId the metered object id
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>objectId</code>
     *          is <code>null</code>
     */

    public T meteredObjectId(org.osid.id.Id objectId) {
        getMiter().setMeteredObjectId(objectId);
        return (self());
    }


    /**
     *  Sets the sum.
     *
     *  @param sum th sum
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>sum</code> is
     *          <code>null</code>
     */

    public T sum(java.math.BigDecimal sum) {
        getMiter().setSum(sum);
        return (self());
    }


    /**
     *  Sets the mean.
     *
     *  @param mean the mean
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>mean</code> is
     *          <code>null</code>
     */

    public T mean(java.math.BigDecimal mean) {
        getMiter().setMean(mean);
        return (self());
    }


    /**
     *  Sets the median.
     *
     *  @param median the median
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>median</code> is
     *          <code>null</code>
     */

    public T median(java.math.BigDecimal median) {
        getMiter().setMedian(median);
        return (self());
    }


    /**
     *  Sets the mode.
     *
     *  @param mode the mode
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>mode</code> is
     *          <code>null</code>
     */

    public T mode(java.math.BigDecimal mode) {
        getMiter().setMode(mode);
        return (self());
    }


    /**
     *  Sets the standard deviation.
     *
     *  @param standardDeviation a standard deviation
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>standardDeviation</code> is <code>null</code>
     */

    public T standardDeviation(java.math.BigDecimal standardDeviation) {
        getMiter().setStandardDeviation(standardDeviation);
        return (self());
    }


    /**
     *  Sets the root mean square.
     *
     *  @param rms the rms
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>rms</code> is
     *          <code>null</code>
     */

    public T rms(java.math.BigDecimal rms) {
        getMiter().setRMS(rms);
        return (self());
    }


    /**
     *  Sets the delta.
     *
     *  @param delta the delta
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>delta</code> is
     *          <code>null</code>
     */

    public T delta(java.math.BigDecimal delta) {
        getMiter().setDelta(delta);
        return (self());
    }


    /**
     *  Sets the percent change.
     *
     *  @param percent a percent change
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>percent</code> is
     *          <code>null</code>
     */

    public T percentChange(java.math.BigDecimal percent) {
        getMiter().setPercentChange(percent);
        return (self());
    }


    /**
     *  Sets the average rate.
     *
     *  @param rate the average rate
     *  @param units the time units
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>rate</code> or
     *          <code>units</code> is <code>null</code>
     */

    public T averageRate(java.math.BigDecimal rate, org.osid.calendaring.DateTimeResolution units) {
        getMiter().setAverageRate(rate, units);
        return (self());
    }


    /**
     *  Adds a Statistic record.
     *
     *  @param record a statistic record
     *  @param recordType the type of statistic record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.metering.records.StatisticRecord record, org.osid.type.Type recordType) {
        getMiter().addStatisticRecord(record, recordType);
        return (self());
    }
}       


