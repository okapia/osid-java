//
// AbstractImmutableChain.java
//
//     Wraps a mutable Chain to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.sequencing.chain.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Chain</code> to hide modifiers. This
 *  wrapper provides an immutized Chain from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying chain whose state changes are visible.
 */

public abstract class AbstractImmutableChain
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.sequencing.Chain {

    private final org.osid.sequencing.Chain chain;


    /**
     *  Constructs a new <code>AbstractImmutableChain</code>.
     *
     *  @param chain the chain to immutablize
     *  @throws org.osid.NullArgumentException <code>chain</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableChain(org.osid.sequencing.Chain chain) {
        super(chain);
        this.chain = chain;
        return;
    }


    /**
     *  Tests if elements are appended to the end of the chain. 
     *
     *  @return <code> true </code> if elements are appended to the end of the 
     *          chain, <code> false </code> if elements are added to the top 
     *          of the chain 
     */

    @OSID @Override
    public boolean isFifo() {
        return (this.chain.isFifo());
    }


    /**
     *  Gets the chain record corresponding to the given <code> Chain </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> chainRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(chainRecordType) </code> is <code> true </code> . 
     *
     *  @param  chainRecordType the type of chain record to retrieve 
     *  @return the chain record 
     *  @throws org.osid.NullArgumentException <code> chainRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(chainRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.records.ChainRecord getChainRecord(org.osid.type.Type chainRecordType)
        throws org.osid.OperationFailedException {

        return (this.chain.getChainRecord(chainRecordType));
    }
}

