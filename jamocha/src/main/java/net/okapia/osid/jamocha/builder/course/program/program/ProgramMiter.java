//
// ProgramMiter.java
//
//     Defines a Program miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.program.program;


/**
 *  Defines a <code>Program</code> miter for use with the builders.
 */

public interface ProgramMiter
    extends net.okapia.osid.jamocha.builder.spi.OperableOsidObjectMiter,
            org.osid.course.program.Program {


    /**
     *  Sets the title.
     *
     *  @param title a title
     *  @throws org.osid.NullArgumentException <code>title</code> is
     *          <code>null</code>
     */

    public void setTitle(org.osid.locale.DisplayText title);


    /**
     *  Sets the number.
     *
     *  @param number a number
     *  @throws org.osid.NullArgumentException <code>number</code> is
     *          <code>null</code>
     */

    public void setNumber(String number);


    /**
     *  Adds a sponsor.
     *
     *  @param sponsor a sponsor
     *  @throws org.osid.NullArgumentException <code>sponsor</code> is
     *          <code>null</code>
     */

    public void addSponsor(org.osid.resource.Resource sponsor);


    /**
     *  Sets all the sponsors.
     *
     *  @param sponsors a collection of sponsors
     *  @throws org.osid.NullArgumentException <code>sponsors</code>
     *          is <code>null</code>
     */

    public void setSponsors(java.util.Collection<org.osid.resource.Resource> sponsors);


    /**
     *  Sets the completion requirements info.
     *
     *  @param info a completion requirements info
     *  @throws org.osid.NullArgumentException <code>info</code> is
     *          <code>null</code>
     */

    public void setCompletionRequirementsInfo(org.osid.locale.DisplayText info);


    /**
     *  Adds a completion requirement.
     *
     *  @param requirement a completion requirement
     *  @throws org.osid.NullArgumentException
     *          <code>requirement</code> is <code>null</code>
     */

    public void addCompletionRequirement(org.osid.course.requisite.Requisite requirement);


    /**
     *  Sets all the completion requirements.
     *
     *  @param requirements a collection of completion requirements
     *  @throws org.osid.NullArgumentException
     *          <code>requirements</code> is <code>null</code>
     */

    public void setCompletionRequirements(java.util.Collection<org.osid.course.requisite.Requisite> requirements);


    /**
     *  Adds a credential.
     *
     *  @param credential a credential
     *  @throws org.osid.NullArgumentException <code>credential</code>
     *          is <code>null</code>
     */

    public void addCredential(org.osid.course.program.Credential credential);


    /**
     *  Sets all the credentials.
     *
     *  @param credentials a collection of credentials
     *  @throws org.osid.NullArgumentException
     *          <code>credentials</code> is <code>null</code>
     */

    public void setCredentials(java.util.Collection<org.osid.course.program.Credential> credentials);


    /**
     *  Adds a Program record.
     *
     *  @param record a program record
     *  @param recordType the type of program record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addProgramRecord(org.osid.course.program.records.ProgramRecord record, org.osid.type.Type recordType);
}       


