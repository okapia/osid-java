//
// AbstractQueryAddressBookLookupSession.java
//
//    An AddressBookQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.contact.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An AddressBookQuerySession adapter.
 */

public abstract class AbstractAdapterAddressBookQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.contact.AddressBookQuerySession {

    private final org.osid.contact.AddressBookQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterAddressBookQuerySession.
     *
     *  @param session the underlying address book query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterAddressBookQuerySession(org.osid.contact.AddressBookQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@codeAddressBook</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchAddressBooks() {
        return (this.session.canSearchAddressBooks());
    }

      
    /**
     *  Gets an address book query. The returned query will not have an
     *  extension query.
     *
     *  @return the address book query 
     */
      
    @OSID @Override
    public org.osid.contact.AddressBookQuery getAddressBookQuery() {
        return (this.session.getAddressBookQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  addressBookQuery the address book query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code addressBookQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code addressBookQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.contact.AddressBookList getAddressBooksByQuery(org.osid.contact.AddressBookQuery addressBookQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getAddressBooksByQuery(addressBookQuery));
    }
}
