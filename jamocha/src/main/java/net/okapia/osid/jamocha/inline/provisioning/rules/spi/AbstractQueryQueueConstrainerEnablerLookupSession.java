//
// AbstractQueryQueueConstrainerEnablerLookupSession.java
//
//    An inline adapter that maps a QueueConstrainerEnablerLookupSession to
//    a QueueConstrainerEnablerQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a QueueConstrainerEnablerLookupSession to
 *  a QueueConstrainerEnablerQuerySession.
 */

public abstract class AbstractQueryQueueConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.provisioning.rules.spi.AbstractQueueConstrainerEnablerLookupSession
    implements org.osid.provisioning.rules.QueueConstrainerEnablerLookupSession {

    private boolean activeonly    = false;
    private boolean effectiveonly = false;
    private final org.osid.provisioning.rules.QueueConstrainerEnablerQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryQueueConstrainerEnablerLookupSession.
     *
     *  @param querySession the underlying queue constrainer enabler query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryQueueConstrainerEnablerLookupSession(org.osid.provisioning.rules.QueueConstrainerEnablerQuerySession querySession) {
        nullarg(querySession, "queue constrainer enabler query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Distributor</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Distributor Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.session.getDistributorId());
    }


    /**
     *  Gets the <code>Distributor</code> associated with this 
     *  session.
     *
     *  @return the <code>Distributor</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getDistributor());
    }


    /**
     *  Tests if this user can perform <code>QueueConstrainerEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupQueueConstrainerEnablers() {
        return (this.session.canSearchQueueConstrainerEnablers());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include queue constrainer enablers in distributors which are children
     *  of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        this.session.useFederatedDistributorView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        this.session.useIsolatedDistributorView();
        return;
    }
    

    /**
     *  Only active queue constrainer enablers are returned by methods
     *  in this session.
     */
     
    @OSID @Override
    public void useActiveQueueConstrainerEnablerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive queue constrainer enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusQueueConstrainerEnablerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>QueueConstrainerEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>QueueConstrainerEnabler</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>QueueConstrainerEnabler</code> and retained for
     *  compatibility.
     *
     *  In active mode, queue constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  queue constrainer enablers are returned.
     *
     *  @param  queueConstrainerEnablerId <code>Id</code> of the
     *          <code>QueueConstrainerEnabler</code>
     *  @return the queue constrainer enabler
     *  @throws org.osid.NotFoundException <code>queueConstrainerEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>queueConstrainerEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerEnabler getQueueConstrainerEnabler(org.osid.id.Id queueConstrainerEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.QueueConstrainerEnablerQuery query = getQuery();
        query.matchId(queueConstrainerEnablerId, true);
        org.osid.provisioning.rules.QueueConstrainerEnablerList queueConstrainerEnablers = this.session.getQueueConstrainerEnablersByQuery(query);
        if (queueConstrainerEnablers.hasNext()) {
            return (queueConstrainerEnablers.getNextQueueConstrainerEnabler());
        } 
        
        throw new org.osid.NotFoundException(queueConstrainerEnablerId + " not found");
    }


    /**
     *  Gets a <code>QueueConstrainerEnablerList</code> corresponding
     *  to the given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  queueConstrainerEnablers specified in the <code>Id</code>
     *  list, in the order of the list, including duplicates, or an
     *  error results if an <code>Id</code> in the supplied list is
     *  not found or inaccessible. Otherwise, inaccessible
     *  <code>QueueConstrainerEnablers</code> may be omitted from the
     *  list and may present the elements in any order including
     *  returning a unique set.
     *
     *  In active mode, queue constrainer enablers are returned that
     *  are currently active. In anqy status mode, active and inactive
     *  queue constrainer enablers are returned.
     *
     *  @param  queueConstrainerEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>QueueConstrainerEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerEnablerList getQueueConstrainerEnablersByIds(org.osid.id.IdList queueConstrainerEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.QueueConstrainerEnablerQuery query = getQuery();

        try (org.osid.id.IdList ids = queueConstrainerEnablerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getQueueConstrainerEnablersByQuery(query));
    }


    /**
     *  Gets a <code>QueueConstrainerEnablerList</code> corresponding
     *  to the given queue constrainer enabler genus <code>Type</code>
     *  which does not include queue constrainer enablers of types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known queue
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those queue constrainer
     *  enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, queue constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  queue constrainer enablers are returned.
     *
     *  @param  queueConstrainerEnablerGenusType a queueConstrainerEnabler genus type 
     *  @return the returned <code>QueueConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerEnablerList getQueueConstrainerEnablersByGenusType(org.osid.type.Type queueConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.QueueConstrainerEnablerQuery query = getQuery();
        query.matchGenusType(queueConstrainerEnablerGenusType, true);
        return (this.session.getQueueConstrainerEnablersByQuery(query));
    }


    /**
     *  Gets a <code>QueueConstrainerEnablerList</code> corresponding
     *  to the given queue constrainer enabler genus <code>Type</code>
     *  and include any additional queue constrainer enablers with
     *  genus types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known queue
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those queue constrainer
     *  enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, queue constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  queue constrainer enablers are returned.
     *
     *  @param  queueConstrainerEnablerGenusType a queueConstrainerEnabler genus type 
     *  @return the returned <code>QueueConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerEnablerList getQueueConstrainerEnablersByParentGenusType(org.osid.type.Type queueConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.QueueConstrainerEnablerQuery query = getQuery();
        query.matchParentGenusType(queueConstrainerEnablerGenusType, true);
        return (this.session.getQueueConstrainerEnablersByQuery(query));
    }


    /**
     *  Gets a <code>QueueConstrainerEnablerList</code> containing the
     *  given queue constrainer enabler record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known queue
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those queue constrainer
     *  enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, queue constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  queue constrainer enablers are returned.
     *
     *  @param  queueConstrainerEnablerRecordType a queueConstrainerEnabler record type 
     *  @return the returned <code>QueueConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerEnablerList getQueueConstrainerEnablersByRecordType(org.osid.type.Type queueConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.QueueConstrainerEnablerQuery query = getQuery();
        query.matchRecordType(queueConstrainerEnablerRecordType, true);
        return (this.session.getQueueConstrainerEnablersByQuery(query));
    }


    /**
     *  Gets a <code>QueueConstrainerEnablerList</code> effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known queue
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those queue constrainer
     *  enablers that are accessible through this session.
     *  
     *  In active mode, queue constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  queue constrainer enablers are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>QueueConstrainerEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerEnablerList getQueueConstrainerEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.QueueConstrainerEnablerQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getQueueConstrainerEnablersByQuery(query));
    }
        
    
    /**
     *  Gets all <code>QueueConstrainerEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known queue
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those queue constrainer
     *  enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, queue constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  queue constrainer enablers are returned.
     *
     *  @return a list of <code>QueueConstrainerEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerEnablerList getQueueConstrainerEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.QueueConstrainerEnablerQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getQueueConstrainerEnablersByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.provisioning.rules.QueueConstrainerEnablerQuery getQuery() {
        org.osid.provisioning.rules.QueueConstrainerEnablerQuery query = this.session.getQueueConstrainerEnablerQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
