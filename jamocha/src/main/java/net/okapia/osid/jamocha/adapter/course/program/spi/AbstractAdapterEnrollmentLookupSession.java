//
// AbstractAdapterEnrollmentLookupSession.java
//
//    An Enrollment lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.course.program.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Enrollment lookup session adapter.
 */

public abstract class AbstractAdapterEnrollmentLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.course.program.EnrollmentLookupSession {

    private final org.osid.course.program.EnrollmentLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterEnrollmentLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterEnrollmentLookupSession(org.osid.course.program.EnrollmentLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code CourseCatalog/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code CourseCatalog Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.session.getCourseCatalogId());
    }


    /**
     *  Gets the {@code CourseCatalog} associated with this session.
     *
     *  @return the {@code CourseCatalog} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCourseCatalog());
    }


    /**
     *  Tests if this user can perform {@code Enrollment} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupEnrollments() {
        return (this.session.canLookupEnrollments());
    }


    /**
     *  A complete view of the {@code Enrollment} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeEnrollmentView() {
        this.session.useComparativeEnrollmentView();
        return;
    }


    /**
     *  A complete view of the {@code Enrollment} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryEnrollmentView() {
        this.session.usePlenaryEnrollmentView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include enrollments in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.session.useFederatedCourseCatalogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.session.useIsolatedCourseCatalogView();
        return;
    }
    

    /**
     *  Only enrollments whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveEnrollmentView() {
        this.session.useEffectiveEnrollmentView();
        return;
    }
    

    /**
     *  All enrollments of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveEnrollmentView() {
        this.session.useAnyEffectiveEnrollmentView();
        return;
    }

     
    /**
     *  Gets the {@code Enrollment} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Enrollment} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Enrollment} and
     *  retained for compatibility.
     *
     *  In effective mode, enrollments are returned that are currently
     *  effective.  In any effective mode, effective enrollments and
     *  those currently expired are returned.
     *
     *  @param enrollmentId {@code Id} of the {@code Enrollment}
     *  @return the enrollment
     *  @throws org.osid.NotFoundException {@code enrollmentId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code enrollmentId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.Enrollment getEnrollment(org.osid.id.Id enrollmentId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEnrollment(enrollmentId));
    }


    /**
     *  Gets an {@code EnrollmentList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  enrollments specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Enrollments} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, enrollments are returned that are currently
     *  effective.  In any effective mode, effective enrollments and
     *  those currently expired are returned.
     *
     *  @param  enrollmentIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Enrollment} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code enrollmentIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsByIds(org.osid.id.IdList enrollmentIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEnrollmentsByIds(enrollmentIds));
    }


    /**
     *  Gets an {@code EnrollmentList} corresponding to the given
     *  enrollment genus {@code Type} which does not include
     *  enrollments of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, enrollments are returned that are currently
     *  effective.  In any effective mode, effective enrollments and
     *  those currently expired are returned.
     *
     *  @param  enrollmentGenusType an enrollment genus type 
     *  @return the returned {@code Enrollment} list
     *  @throws org.osid.NullArgumentException
     *          {@code enrollmentGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsByGenusType(org.osid.type.Type enrollmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEnrollmentsByGenusType(enrollmentGenusType));
    }


    /**
     *  Gets an {@code EnrollmentList} corresponding to the given
     *  enrollment genus {@code Type} and include any additional
     *  enrollments with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, enrollments are returned that are currently
     *  effective.  In any effective mode, effective enrollments and
     *  those currently expired are returned.
     *
     *  @param  enrollmentGenusType an enrollment genus type 
     *  @return the returned {@code Enrollment} list
     *  @throws org.osid.NullArgumentException
     *          {@code enrollmentGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsByParentGenusType(org.osid.type.Type enrollmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEnrollmentsByParentGenusType(enrollmentGenusType));
    }


    /**
     *  Gets an {@code EnrollmentList} containing the given
     *  enrollment record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, enrollments are returned that are currently
     *  effective.  In any effective mode, effective enrollments and
     *  those currently expired are returned.
     *
     *  @param  enrollmentRecordType an enrollment record type 
     *  @return the returned {@code Enrollment} list
     *  @throws org.osid.NullArgumentException
     *          {@code enrollmentRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsByRecordType(org.osid.type.Type enrollmentRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEnrollmentsByRecordType(enrollmentRecordType));
    }


    /**
     *  Gets an {@code EnrollmentList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible
     *  through this session.
     *  
     *  In active mode, enrollments are returned that are currently
     *  active. In any status mode, active and inactive enrollments
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Enrollment} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsOnDate(org.osid.calendaring.DateTime from, 
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEnrollmentsOnDate(from, to));
    }
        

    /**
     *  Gets a list of enrollments corresponding to a program offering
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible through
     *  this session.
     *
     *  In effective mode, enrollments are returned that are
     *  currently effective.  In any effective mode, effective
     *  enrollments and those currently expired are returned.
     *
     *  @param  programOfferingId the {@code Id} of the program offering
     *  @return the returned {@code EnrollmentList}
     *  @throws org.osid.NullArgumentException {@code programOfferingId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsForProgramOffering(org.osid.id.Id programOfferingId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEnrollmentsForProgramOffering(programOfferingId));
    }


    /**
     *  Gets a list of enrollments corresponding to a program offering
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible
     *  through this session.
     *
     *  In effective mode, enrollments are returned that are
     *  currently effective.  In any effective mode, effective
     *  enrollments and those currently expired are returned.
     *
     *  @param  programOfferingId the {@code Id} of the program offering
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code EnrollmentList}
     *  @throws org.osid.NullArgumentException {@code programOfferingId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsForProgramOfferingOnDate(org.osid.id.Id programOfferingId,
                                                                                         org.osid.calendaring.DateTime from,
                                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEnrollmentsForProgramOfferingOnDate(programOfferingId, from, to));
    }


    /**
     *  Gets a list of enrollments corresponding to a student
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible
     *  through this session.
     *
     *  In effective mode, enrollments are returned that are
     *  currently effective.  In any effective mode, effective
     *  enrollments and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the student
     *  @return the returned {@code EnrollmentList}
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsForStudent(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEnrollmentsForStudent(resourceId));
    }


    /**
     *  Gets a list of enrollments corresponding to a student
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible
     *  through this session.
     *
     *  In effective mode, enrollments are returned that are
     *  currently effective.  In any effective mode, effective
     *  enrollments and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the student
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code EnrollmentList}
     *  @throws org.osid.NullArgumentException {@code resourceId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsForStudentOnDate(org.osid.id.Id resourceId,
                                                                                 org.osid.calendaring.DateTime from,
                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEnrollmentsForStudentOnDate(resourceId, from, to));
    }


    /**
     *  Gets a list of enrollments corresponding to program offering and student
     *  {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible
     *  through this session.
     *
     *  In effective mode, enrollments are returned that are
     *  currently effective.  In any effective mode, effective
     *  enrollments and those currently expired are returned.
     *
     *  @param  programOfferingId the {@code Id} of the program offering
     *  @param  resourceId the {@code Id} of the student
     *  @return the returned {@code EnrollmentList}
     *  @throws org.osid.NullArgumentException {@code programOfferingId},
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsForProgramOfferingAndStudent(org.osid.id.Id programOfferingId,
                                                                                             org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEnrollmentsForProgramOfferingAndStudent(programOfferingId, resourceId));
    }


    /**
     *  Gets a list of enrollments corresponding to program offering and student
     *  {@code Ids} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible
     *  through this session.
     *
     *  In effective mode, enrollments are returned that are currently
     *  effective. In any effective mode, effective enrollments and
     *  those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the student
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code EnrollmentList}
     *  @throws org.osid.NullArgumentException {@code programOfferingId},
     *          {@code resourceId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsForProgramOfferingAndStudentOnDate(org.osid.id.Id programOfferingId,
                                                                                                   org.osid.id.Id resourceId,
                                                                                                   org.osid.calendaring.DateTime from,
                                                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEnrollmentsForProgramOfferingAndStudentOnDate(programOfferingId, resourceId, from, to));
    }


    /**
     *  Gets a list of enrollments corresponding to a program {@code
     *  Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible through
     *  this session.
     *
     *  In effective mode, enrollments are returned that are currently
     *  effective.  In any effective mode, effective enrollments and
     *  those currently expired are returned.
     *
     *  @param  programId the {@code Id} of the program 
     *  @return the returned {@code EnrollmentList}
     *  @throws org.osid.NullArgumentException {@code programId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsForProgram(org.osid.id.Id programId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEnrollmentsForProgram(programId));
    }


    /**
     *  Gets a list of enrollments corresponding to a program {@code
     *  Id} and effective during the entire given date range inclusive
     *  but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible through
     *  this session.
     *
     *  In effective mode, enrollments are returned that are currently
     *  effective.  In any effective mode, effective enrollments and
     *  those currently expired are returned.
     *
     *  @param  programId the {@code Id} of the program 
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code EnrollmentList}
     *  @throws org.osid.NullArgumentException {@code programId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsForProgramOnDate(org.osid.id.Id programId,
                                                                                 org.osid.calendaring.DateTime from,
                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEnrollmentsForProgramOnDate(programId, from, to));
    }


    /**
     *  Gets a list of enrollments corresponding to program and
     *  student {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible through
     *  this session.
     *
     *  In effective mode, enrollments are returned that are currently
     *  effective.  In any effective mode, effective enrollments and
     *  those currently expired are returned.
     *
     *  @param  programId the {@code Id} of the program 
     *  @param  resourceId the {@code Id} of the student
     *  @return the returned {@code EnrollmentList}
     *  @throws org.osid.NullArgumentException {@code programId},
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsForProgramAndStudent(org.osid.id.Id programId,
                                                                                     org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEnrollmentsForProgramAndStudent(programId, resourceId));
    }


    /**
     *  Gets a list of enrollments corresponding to program and
     *  student {@code Ids} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible through
     *  this session.
     *
     *  In effective mode, enrollments are returned that are currently
     *  effective. In any effective mode, effective enrollments and
     *  those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the student
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code EnrollmentList}
     *  @throws org.osid.NullArgumentException {@code programId},
     *          {@code resourceId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsForProgramAndStudentOnDate(org.osid.id.Id programId,
                                                                                           org.osid.id.Id resourceId,
                                                                                           org.osid.calendaring.DateTime from,
                                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEnrollmentsForProgramAndStudentOnDate(programId, resourceId, from, to));
    }

    
    /**
     *  Gets all {@code Enrollments}. 
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, enrollments are returned that are currently
     *  effective.  In any effective mode, effective enrollments and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Enrollments} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollments()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEnrollments());
    }
}
