//
// AbstractProfileEntryEnabler.java
//
//     Defines a ProfileEntryEnabler builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.profile.rules.profileentryenabler.spi;


/**
 *  Defines a <code>ProfileEntryEnabler</code> builder.
 */

public abstract class AbstractProfileEntryEnablerBuilder<T extends AbstractProfileEntryEnablerBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidEnablerBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.profile.rules.profileentryenabler.ProfileEntryEnablerMiter profileEntryEnabler;


    /**
     *  Constructs a new <code>AbstractProfileEntryEnablerBuilder</code>.
     *
     *  @param profileEntryEnabler the profile entry enabler to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractProfileEntryEnablerBuilder(net.okapia.osid.jamocha.builder.profile.rules.profileentryenabler.ProfileEntryEnablerMiter profileEntryEnabler) {
        super(profileEntryEnabler);
        this.profileEntryEnabler = profileEntryEnabler;
        return;
    }


    /**
     *  Builds the profile entry enabler.
     *
     *  @return the new profile entry enabler
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.profile.rules.ProfileEntryEnabler build() {
        (new net.okapia.osid.jamocha.builder.validator.profile.rules.profileentryenabler.ProfileEntryEnablerValidator(getValidations())).validate(this.profileEntryEnabler);
        return (new net.okapia.osid.jamocha.builder.profile.rules.profileentryenabler.ImmutableProfileEntryEnabler(this.profileEntryEnabler));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the profile entry enabler miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.profile.rules.profileentryenabler.ProfileEntryEnablerMiter getMiter() {
        return (this.profileEntryEnabler);
    }


    /**
     *  Adds a ProfileEntryEnabler record.
     *
     *  @param record a profile entry enabler record
     *  @param recordType the type of profile entry enabler record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.profile.rules.records.ProfileEntryEnablerRecord record, org.osid.type.Type recordType) {
        getMiter().addProfileEntryEnablerRecord(record, recordType);
        return (self());
    }
}       


