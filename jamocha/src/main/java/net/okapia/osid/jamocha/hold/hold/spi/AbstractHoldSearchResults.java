//
// AbstractHoldSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.hold.hold.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractHoldSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.hold.HoldSearchResults {

    private org.osid.hold.HoldList holds;
    private final org.osid.hold.HoldQueryInspector inspector;
    private final java.util.Collection<org.osid.hold.records.HoldSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractHoldSearchResults.
     *
     *  @param holds the result set
     *  @param holdQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>holds</code>
     *          or <code>holdQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractHoldSearchResults(org.osid.hold.HoldList holds,
                                            org.osid.hold.HoldQueryInspector holdQueryInspector) {
        nullarg(holds, "holds");
        nullarg(holdQueryInspector, "hold query inspectpr");

        this.holds = holds;
        this.inspector = holdQueryInspector;

        return;
    }


    /**
     *  Gets the hold list resulting from a search.
     *
     *  @return a hold list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.hold.HoldList getHolds() {
        if (this.holds == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.hold.HoldList holds = this.holds;
        this.holds = null;
	return (holds);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.hold.HoldQueryInspector getHoldQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  hold search record <code> Type. </code> This method must
     *  be used to retrieve a hold implementing the requested
     *  record.
     *
     *  @param holdSearchRecordType a hold search 
     *         record type 
     *  @return the hold search
     *  @throws org.osid.NullArgumentException
     *          <code>holdSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(holdSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.hold.records.HoldSearchResultsRecord getHoldSearchResultsRecord(org.osid.type.Type holdSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.hold.records.HoldSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(holdSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(holdSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record hold search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addHoldRecord(org.osid.hold.records.HoldSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "hold record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
