//
// AgencyNodeToIdList.java
//
//     Implements an AgencyNode IdList. 
//
//
// Tom Coppeto
// Okapia
// 15 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.converter.authentication.agencynode;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  Implements an IdList converting the underlying AgencyNodeList to a
 *  list of Ids.
 */

public final class AgencyNodeToIdList
    extends net.okapia.osid.jamocha.adapter.id.id.spi.AbstractAdapterIdList
    implements org.osid.id.IdList {

    private final org.osid.authentication.AgencyNodeList agencyNodeList;


    /**
     *  Creates a new {@code AgencyNodeToIdList}.
     *
     *  @param agencyNodeList an {@code AgencyNodeList}
     *  @throws org.osid.NullArgumentException {@code agencyNodeList}
     *          is {@code null}
     */

    public AgencyNodeToIdList(org.osid.authentication.AgencyNodeList agencyNodeList) {
        super(agencyNodeList);
        this.agencyNodeList = agencyNodeList;
        return;
    }


    /**
     *  Creates a new {@code AgencyNodeToIdList}.
     *
     *  @param agencyNodes a collection of AgencyNodes
     *  @throws org.osid.NullArgumentException {@code agencyNodes}
     *          is {@code null}
     */

    public AgencyNodeToIdList(java.util.Collection<org.osid.authentication.AgencyNode> agencyNodes) {
        this(new net.okapia.osid.jamocha.authentication.agencynode.ArrayAgencyNodeList(agencyNodes)); 
        return;
    }


    /**
     *  Gets the next {@code Id} in this list. 
     *
     *  @return the next {@code Id} in this list. The {@code
     *          hasNext()} method should be used to test that a next
     *          {@code Id} is available before calling this method.
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.id.Id getNextId()
        throws org.osid.OperationFailedException {

        return (this.agencyNodeList.getNextAgencyNode().getId());
    }


    /**
     *  Closes this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.agencyNodeList.close();
        return;
    }
}
