//
// AbstractNodeStoreHierarchySession.java
//
//     Defines a Store hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.ordering.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a store hierarchy session for delivering a hierarchy
 *  of stores using the StoreNode interface.
 */

public abstract class AbstractNodeStoreHierarchySession
    extends net.okapia.osid.jamocha.ordering.spi.AbstractStoreHierarchySession
    implements org.osid.ordering.StoreHierarchySession {

    private java.util.Collection<org.osid.ordering.StoreNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root store <code> Ids </code> in this hierarchy.
     *
     *  @return the root store <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootStoreIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.ordering.storenode.StoreNodeToIdList(this.roots));
    }


    /**
     *  Gets the root stores in the store hierarchy. A node
     *  with no parents is an orphan. While all store <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root stores 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.StoreList getRootStores()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.ordering.storenode.StoreNodeToStoreList(new net.okapia.osid.jamocha.ordering.storenode.ArrayStoreNodeList(this.roots)));
    }


    /**
     *  Adds a root store node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootStore(org.osid.ordering.StoreNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root store nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootStores(java.util.Collection<org.osid.ordering.StoreNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root store node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootStore(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.ordering.StoreNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Store </code> has any parents. 
     *
     *  @param  storeId a store <code> Id </code> 
     *  @return <code> true </code> if the store has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> storeId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> storeId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentStores(org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getStoreNode(storeId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  store.
     *
     *  @param  id an <code> Id </code> 
     *  @param  storeId the <code> Id </code> of a store 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> storeId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> storeId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> storeId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfStore(org.osid.id.Id id, org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.ordering.StoreNodeList parents = getStoreNode(storeId).getParentStoreNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextStoreNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given store. 
     *
     *  @param  storeId a store <code> Id </code> 
     *  @return the parent <code> Ids </code> of the store 
     *  @throws org.osid.NotFoundException <code> storeId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> storeId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentStoreIds(org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.ordering.store.StoreToIdList(getParentStores(storeId)));
    }


    /**
     *  Gets the parents of the given store. 
     *
     *  @param  storeId the <code> Id </code> to query 
     *  @return the parents of the store 
     *  @throws org.osid.NotFoundException <code> storeId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> storeId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.StoreList getParentStores(org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.ordering.storenode.StoreNodeToStoreList(getStoreNode(storeId).getParentStoreNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  store.
     *
     *  @param  id an <code> Id </code> 
     *  @param  storeId the Id of a store 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> storeId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> storeId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> storeId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfStore(org.osid.id.Id id, org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfStore(id, storeId)) {
            return (true);
        }

        try (org.osid.ordering.StoreList parents = getParentStores(storeId)) {
            while (parents.hasNext()) {
                if (isAncestorOfStore(id, parents.getNextStore().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a store has any children. 
     *
     *  @param  storeId a store <code> Id </code> 
     *  @return <code> true </code> if the <code> storeId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> storeId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> storeId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildStores(org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getStoreNode(storeId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  store.
     *
     *  @param  id an <code> Id </code> 
     *  @param storeId the <code> Id </code> of a 
     *         store
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> storeId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> storeId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> storeId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfStore(org.osid.id.Id id, org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfStore(storeId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  store.
     *
     *  @param  storeId the <code> Id </code> to query 
     *  @return the children of the store 
     *  @throws org.osid.NotFoundException <code> storeId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> storeId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildStoreIds(org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.ordering.store.StoreToIdList(getChildStores(storeId)));
    }


    /**
     *  Gets the children of the given store. 
     *
     *  @param  storeId the <code> Id </code> to query 
     *  @return the children of the store 
     *  @throws org.osid.NotFoundException <code> storeId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> storeId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.StoreList getChildStores(org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.ordering.storenode.StoreNodeToStoreList(getStoreNode(storeId).getChildStoreNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  store.
     *
     *  @param  id an <code> Id </code> 
     *  @param storeId the <code> Id </code> of a 
     *         store
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> storeId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> storeId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> storeId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfStore(org.osid.id.Id id, org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfStore(storeId, id)) {
            return (true);
        }

        try (org.osid.ordering.StoreList children = getChildStores(storeId)) {
            while (children.hasNext()) {
                if (isDescendantOfStore(id, children.getNextStore().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  store.
     *
     *  @param  storeId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified store node 
     *  @throws org.osid.NotFoundException <code> storeId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> storeId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getStoreNodeIds(org.osid.id.Id storeId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.ordering.storenode.StoreNodeToNode(getStoreNode(storeId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given store.
     *
     *  @param  storeId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified store node 
     *  @throws org.osid.NotFoundException <code> storeId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> storeId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.StoreNode getStoreNodes(org.osid.id.Id storeId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getStoreNode(storeId));
    }


    /**
     *  Closes this <code>StoreHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a store node.
     *
     *  @param storeId the id of the store node
     *  @throws org.osid.NotFoundException <code>storeId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>storeId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.ordering.StoreNode getStoreNode(org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(storeId, "store Id");
        for (org.osid.ordering.StoreNode store : this.roots) {
            if (store.getId().equals(storeId)) {
                return (store);
            }

            org.osid.ordering.StoreNode r = findStore(store, storeId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(storeId + " is not found");
    }


    protected org.osid.ordering.StoreNode findStore(org.osid.ordering.StoreNode node, 
                                                    org.osid.id.Id storeId) 
	throws org.osid.OperationFailedException {

        try (org.osid.ordering.StoreNodeList children = node.getChildStoreNodes()) {
            while (children.hasNext()) {
                org.osid.ordering.StoreNode store = children.getNextStoreNode();
                if (store.getId().equals(storeId)) {
                    return (store);
                }
                
                store = findStore(store, storeId);
                if (store != null) {
                    return (store);
                }
            }
        }

        return (null);
    }
}
