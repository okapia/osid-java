//
// AbstractQueryBranchLookupSession.java
//
//    An inline adapter that maps a BranchLookupSession to
//    a BranchQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.journaling.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a BranchLookupSession to
 *  a BranchQuerySession.
 */

public abstract class AbstractQueryBranchLookupSession
    extends net.okapia.osid.jamocha.journaling.spi.AbstractBranchLookupSession
    implements org.osid.journaling.BranchLookupSession {

    private boolean activeonly    = false;
    private final org.osid.journaling.BranchQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryBranchLookupSession.
     *
     *  @param querySession the underlying branch query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryBranchLookupSession(org.osid.journaling.BranchQuerySession querySession) {
        nullarg(querySession, "branch query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Journal</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Journal Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getJournalId() {
        return (this.session.getJournalId());
    }


    /**
     *  Gets the <code>Journal</code> associated with this 
     *  session.
     *
     *  @return the <code>Journal</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.Journal getJournal()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getJournal());
    }


    /**
     *  Tests if this user can perform <code>Branch</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupBranches() {
        return (this.session.canSearchBranches());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include branches in journals which are children
     *  of this journal in the journal hierarchy.
     */

    @OSID @Override
    public void useFederatedJournalView() {
        this.session.useFederatedJournalView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this journal only.
     */

    @OSID @Override
    public void useIsolatedJournalView() {
        this.session.useIsolatedJournalView();
        return;
    }
    

    /**
     *  Only active branches are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveBranchView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive branches are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusBranchView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>Branch</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Branch</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Branch</code> and
     *  retained for compatibility.
     *
     *  In active mode, branches are returned that are currently
     *  active. In any status mode, active and inactive branches
     *  are returned.
     *
     *  @param  branchId <code>Id</code> of the
     *          <code>Branch</code>
     *  @return the branch
     *  @throws org.osid.NotFoundException <code>branchId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>branchId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.Branch getBranch(org.osid.id.Id branchId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.journaling.BranchQuery query = getQuery();
        query.matchId(branchId, true);
        org.osid.journaling.BranchList branches = this.session.getBranchesByQuery(query);
        if (branches.hasNext()) {
            return (branches.getNextBranch());
        } 
        
        throw new org.osid.NotFoundException(branchId + " not found");
    }


    /**
     *  Gets a <code>BranchList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  branches specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Branches</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, branches are returned that are currently
     *  active. In any status mode, active and inactive branches
     *  are returned.
     *
     *  @param  branchIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Branch</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>branchIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.BranchList getBranchesByIds(org.osid.id.IdList branchIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.journaling.BranchQuery query = getQuery();

        try (org.osid.id.IdList ids = branchIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getBranchesByQuery(query));
    }


    /**
     *  Gets a <code>BranchList</code> corresponding to the given
     *  branch genus <code>Type</code> which does not include
     *  branches of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  branches or an error results. Otherwise, the returned list
     *  may contain only those branches that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, branches are returned that are currently
     *  active. In any status mode, active and inactive branches
     *  are returned.
     *
     *  @param  branchGenusType a branch genus type 
     *  @return the returned <code>Branch</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>branchGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.BranchList getBranchesByGenusType(org.osid.type.Type branchGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.journaling.BranchQuery query = getQuery();
        query.matchGenusType(branchGenusType, true);
        return (this.session.getBranchesByQuery(query));
    }


    /**
     *  Gets a <code>BranchList</code> corresponding to the given
     *  branch genus <code>Type</code> and include any additional
     *  branches with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  branches or an error results. Otherwise, the returned list
     *  may contain only those branches that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, branches are returned that are currently
     *  active. In any status mode, active and inactive branches
     *  are returned.
     *
     *  @param  branchGenusType a branch genus type 
     *  @return the returned <code>Branch</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>branchGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.BranchList getBranchesByParentGenusType(org.osid.type.Type branchGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.journaling.BranchQuery query = getQuery();
        query.matchParentGenusType(branchGenusType, true);
        return (this.session.getBranchesByQuery(query));
    }


    /**
     *  Gets a <code>BranchList</code> containing the given
     *  branch record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  branches or an error results. Otherwise, the returned list
     *  may contain only those branches that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, branches are returned that are currently
     *  active. In any status mode, active and inactive branches
     *  are returned.
     *
     *  @param  branchRecordType a branch record type 
     *  @return the returned <code>Branch</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>branchRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.BranchList getBranchesByRecordType(org.osid.type.Type branchRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.journaling.BranchQuery query = getQuery();
        query.matchRecordType(branchRecordType, true);
        return (this.session.getBranchesByQuery(query));
    }

    
    /**
     *  Gets all <code>Branches</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  branches or an error results. Otherwise, the returned list
     *  may contain only those branches that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, branches are returned that are currently
     *  active. In any status mode, active and inactive branches
     *  are returned.
     *
     *  @return a list of <code>Branches</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.BranchList getBranches()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.journaling.BranchQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getBranchesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.journaling.BranchQuery getQuery() {
        org.osid.journaling.BranchQuery query = this.session.getBranchQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
