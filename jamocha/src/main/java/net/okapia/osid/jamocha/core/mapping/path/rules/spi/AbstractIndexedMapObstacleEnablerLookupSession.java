//
// AbstractIndexedMapObstacleEnablerLookupSession.java
//
//    A simple framework for providing an ObstacleEnabler lookup service
//    backed by a fixed collection of obstacle enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.path.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an ObstacleEnabler lookup service backed by a
 *  fixed collection of obstacle enablers. The obstacle enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some obstacle enablers may be compatible
 *  with more types than are indicated through these obstacle enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>ObstacleEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapObstacleEnablerLookupSession
    extends AbstractMapObstacleEnablerLookupSession
    implements org.osid.mapping.path.rules.ObstacleEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.mapping.path.rules.ObstacleEnabler> obstacleEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.mapping.path.rules.ObstacleEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.mapping.path.rules.ObstacleEnabler> obstacleEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.mapping.path.rules.ObstacleEnabler>());


    /**
     *  Makes an <code>ObstacleEnabler</code> available in this session.
     *
     *  @param  obstacleEnabler an obstacle enabler
     *  @throws org.osid.NullArgumentException <code>obstacleEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putObstacleEnabler(org.osid.mapping.path.rules.ObstacleEnabler obstacleEnabler) {
        super.putObstacleEnabler(obstacleEnabler);

        this.obstacleEnablersByGenus.put(obstacleEnabler.getGenusType(), obstacleEnabler);
        
        try (org.osid.type.TypeList types = obstacleEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.obstacleEnablersByRecord.put(types.getNextType(), obstacleEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an obstacle enabler from this session.
     *
     *  @param obstacleEnablerId the <code>Id</code> of the obstacle enabler
     *  @throws org.osid.NullArgumentException <code>obstacleEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeObstacleEnabler(org.osid.id.Id obstacleEnablerId) {
        org.osid.mapping.path.rules.ObstacleEnabler obstacleEnabler;
        try {
            obstacleEnabler = getObstacleEnabler(obstacleEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.obstacleEnablersByGenus.remove(obstacleEnabler.getGenusType());

        try (org.osid.type.TypeList types = obstacleEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.obstacleEnablersByRecord.remove(types.getNextType(), obstacleEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeObstacleEnabler(obstacleEnablerId);
        return;
    }


    /**
     *  Gets an <code>ObstacleEnablerList</code> corresponding to the given
     *  obstacle enabler genus <code>Type</code> which does not include
     *  obstacle enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known obstacle enablers or an error results. Otherwise,
     *  the returned list may contain only those obstacle enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  obstacleEnablerGenusType an obstacle enabler genus type 
     *  @return the returned <code>ObstacleEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>obstacleEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerList getObstacleEnablersByGenusType(org.osid.type.Type obstacleEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.mapping.path.rules.obstacleenabler.ArrayObstacleEnablerList(this.obstacleEnablersByGenus.get(obstacleEnablerGenusType)));
    }


    /**
     *  Gets an <code>ObstacleEnablerList</code> containing the given
     *  obstacle enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known obstacle enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  obstacle enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  obstacleEnablerRecordType an obstacle enabler record type 
     *  @return the returned <code>obstacleEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>obstacleEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerList getObstacleEnablersByRecordType(org.osid.type.Type obstacleEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.mapping.path.rules.obstacleenabler.ArrayObstacleEnablerList(this.obstacleEnablersByRecord.get(obstacleEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.obstacleEnablersByGenus.clear();
        this.obstacleEnablersByRecord.clear();

        super.close();

        return;
    }
}
