//
// AbstractEnrollmentSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.program.enrollment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractEnrollmentSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.course.program.EnrollmentSearchResults {

    private org.osid.course.program.EnrollmentList enrollments;
    private final org.osid.course.program.EnrollmentQueryInspector inspector;
    private final java.util.Collection<org.osid.course.program.records.EnrollmentSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractEnrollmentSearchResults.
     *
     *  @param enrollments the result set
     *  @param enrollmentQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>enrollments</code>
     *          or <code>enrollmentQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractEnrollmentSearchResults(org.osid.course.program.EnrollmentList enrollments,
                                            org.osid.course.program.EnrollmentQueryInspector enrollmentQueryInspector) {
        nullarg(enrollments, "enrollments");
        nullarg(enrollmentQueryInspector, "enrollment query inspectpr");

        this.enrollments = enrollments;
        this.inspector = enrollmentQueryInspector;

        return;
    }


    /**
     *  Gets the enrollment list resulting from a search.
     *
     *  @return an enrollment list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollments() {
        if (this.enrollments == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.course.program.EnrollmentList enrollments = this.enrollments;
        this.enrollments = null;
	return (enrollments);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.course.program.EnrollmentQueryInspector getEnrollmentQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  enrollment search record <code> Type. </code> This method must
     *  be used to retrieve an enrollment implementing the requested
     *  record.
     *
     *  @param enrollmentSearchRecordType an enrollment search 
     *         record type 
     *  @return the enrollment search
     *  @throws org.osid.NullArgumentException
     *          <code>enrollmentSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(enrollmentSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.program.records.EnrollmentSearchResultsRecord getEnrollmentSearchResultsRecord(org.osid.type.Type enrollmentSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.course.program.records.EnrollmentSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(enrollmentSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(enrollmentSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record enrollment search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addEnrollmentRecord(org.osid.course.program.records.EnrollmentSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "enrollment record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
