//
// AbstractSubscriptionNotificationSession.java
//
//     A template for making SubscriptionNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.subscription.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Subscription} objects. This session is
 *  intended for consumers needing to synchronize their state with
 *  this service without the use of polling. Notifications are
 *  cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Subscription} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for subscription entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractSubscriptionNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.subscription.SubscriptionNotificationSession {

    private boolean federated = false;
    private org.osid.subscription.Publisher publisher = new net.okapia.osid.jamocha.nil.subscription.publisher.UnknownPublisher();


    /**
     *  Gets the {@code Publisher/code> {@code Id} associated with
     *  this session.
     *
     *  @return the {@code Publisher Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getPublisherId() {
        return (this.publisher.getId());
    }

    
    /**
     *  Gets the {@code Publisher} associated with this session.
     *
     *  @return the {@code Publisher} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.Publisher getPublisher()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.publisher);
    }


    /**
     *  Sets the {@code Publisher}.
     *
     *  @param publisher the publisher for this session
     *  @throws org.osid.NullArgumentException {@code publisher}
     *          is {@code null}
     */

    protected void setPublisher(org.osid.subscription.Publisher publisher) {
        nullarg(publisher, "publisher");
        this.publisher = publisher;
        return;
    }


    /**
     *  Tests if this user can register for {@code Subscription}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForSubscriptionNotifications() {
        return (true);
    }


    /**
     *  Reliable notifications are desired. In reliable mode,
     *  notifications are to be acknowledged using <code>
     *  acknowledgeSubscriptionNotification()</code>.
     */

    @OSID @Override
    public void reliableSubscriptionNotifications() {
        return;
    }


    /**
     *  Unreliable notifications are desired. In unreliable mode, 
     *  notifications do not need to be acknowledged. 
     */

    @OSID @Override
    public void unreliableSubscriptionNotifications() {
        return;
    }


    /**
     *  Acknowledge a subscription notification. 
     *
     *  @param  notificationId the <code> Id </code> of the notification 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void acknowledgeSubscriptionNotification(org.osid.id.Id notificationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include subscription in publishers which are
     *  children of this publisher in the publisher hierarchy.
     */

    @OSID @Override
    public void useFederatedPublisherView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this publisher only.
     */

    @OSID @Override
    public void useIsolatedPublisherView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return {@codetrue</code> if federated view,
     *          {@codefalse</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new subscriptions. {@code
     *  SubscriptionReceiver.newSubscription()} is invoked when a new
     *  {@code Subscription} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewSubscriptions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new subscriptions of the given
     *  genus type. {@code SubscriptionReceiver.newSubscription()} is
     *  invoked when a new {@code Subscription} is created.
     *
     *  @param  subscriptionGenusType a subscription genus type 
     *  @throws org.osid.NullArgumentException {@code
     *         subscriptionGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForNewSubscriptionsByGenusType(org.osid.type.Type subscriptionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new subscriptions for the given
     *  subscriber {@code Id}. {@code
     *  SubscriptionReceiver.newSubscription()} is invoked when a new
     *  {@code Subscription} is created.
     *
     *  @param  resourceId the {@code Id} of the subscriber to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewSubscriptionsForSubscriber(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /** 
     *  Register for notifications of new subscriptions for the given
     *  dispatch {@code Id}. {@code
     *  SubscriptionReceiver.newSubscription()} is invoked when a new
     *  {@code Subscription} is created.
     *
     *  @param  dispatchId the {@code Id} of the dispatch to monitor
     *  @throws org.osid.NullArgumentException {@code dispatchId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewSubscriptionsForDispatch(org.osid.id.Id dispatchId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated subscriptions. {@code
     *  SubscriptionReceiver.changedSubscription()} is invoked when a
     *  subscription is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedSubscriptions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated subscriptions of the
     *  given genus type. {@code
     *  SubscriptionReceiver.changedSubscription()} is invoked when a
     *  subscription is changed.
     *
     *  @param  subscriptionGenusType a subscription genus type 
     *  @throws org.osid.NullArgumentException {@code
     *         subscriptionGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedSubscriptionsByGenusType(org.osid.type.Type subscriptionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of updated subscriptions for the
     *  given subscriber {@code Id}. {@code
     *  SubscriptionReceiver.changedSubscription()} is invoked when a
     *  {@code Subscription} in this publisher is changed.
     *
     *  @param  resourceId the {@code Id} of the subscriber to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedSubscriptionsForSubscriber(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated subscriptions for the
     *  given dispatch {@code Id}. {@code
     *  SubscriptionReceiver.changedSubscription()} is invoked when a
     *  {@code Subscription} in this publisher is changed.
     *
     *  @param  dispatchId the {@code Id} of the dispatch to monitor
     *  @throws org.osid.NullArgumentException {@code dispatchId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedSubscriptionsForDispatch(org.osid.id.Id dispatchId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated subscription. {@code
     *  SubscriptionReceiver.changedSubscription()} is invoked when
     *  the specified subscription is changed.
     *
     *  @param subscriptionId the {@code Id} of the {@code Subscription} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code subscriptionId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedSubscription(org.osid.id.Id subscriptionId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted subscriptions. {@code
     *  SubscriptionReceiver.deletedSubscription()} is invoked when a
     *  subscription is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedSubscriptions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of deleted subscriptions of the
     *  given genus type. {@code
     *  SubscriptionReceiver.deletedSubscription()} is invoked when a
     *  subscription is deleted.
     *
     *  @param  subscriptionGenusType a subscription genus type 
     *  @throws org.osid.NullArgumentException {@code
     *         subscriptionGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForDeletedSubscriptionsByGenusType(org.osid.type.Type subscriptionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of deleted subscriptions for the
     *  given subscriber {@code Id}. {@code
     *  SubscriptionReceiver.deletedSubscription()} is invoked when a
     *  {@code Subscription} is deleted or removed from this
     *  publisher.
     *
     *  @param  resourceId the {@code Id} of the subscriber to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */
      
    @OSID @Override
    public void registerForDeletedSubscriptionsForSubscriber(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of deleted subscriptions for the
     *  given dispatch {@code Id}. {@code
     *  SubscriptionReceiver.deletedSubscription()} is invoked when a
     *  {@code Subscription} is deleted or removed from this
     *  publisher.
     *
     *  @param  dispatchId the {@code Id} of the dispatch to monitor
     *  @throws org.osid.NullArgumentException {@code dispatchId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForDeletedSubscriptionsForDispatch(org.osid.id.Id dispatchId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of a deleted subscription. {@code
     *  SubscriptionReceiver.deletedSubscription()} is invoked when
     *  the specified subscription is deleted.
     *
     *  @param subscriptionId the {@code Id} of the
     *          {@code Subscription} to monitor
     *  @throws org.osid.NullArgumentException {@code subscriptionId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedSubscription(org.osid.id.Id subscriptionId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
