//
// AbstractCatalogNodeToNode.java
//
//     Defines a CatalogNode to Node converter.
//
//
// Tom Coppeto
// Okapia
// 20 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.converter.cataloging.catalognode.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code CatalogNode} to hierarchy {@code Node}
 *  converter.
 */

public abstract class AbstractCatalogNodeToNode
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterIdentifiable
    implements org.osid.hierarchy.Node {

    private final org.osid.cataloging.CatalogNode node;


    /**
     *  Constructs a new {@code AbstractCatalogNodeToNode} from a
     *  catalog node.
     *
     *  @param node the catalog node node to convert
     *  @throws org.osid.NullArgumentException {@code node} is
     *          {@code null}
     */

    protected AbstractCatalogNodeToNode(org.osid.cataloging.CatalogNode node) {
        super(node);
        this.node = node;
        return;
    }
    
    
    /**
     *  Gets the underlying catalog node.
     *
     *  @return the catalog node
     */

    protected org.osid.cataloging.CatalogNode getCatalogNode() {
        return (this.node);
    }


    /**
     *  Tests if this {@code Containable} is sequestered in that it 
     *  should not appear outside of its aggregated composition. 
     *
     *  @return {@code true} if this containable is sequestered,
     *          {@code false} if this containable may appear outside
     *          its aggregate
     */
     
    @OSID @Override
    public boolean isSequestered() {
        return (true);
    }


    /**
     *  Tests if this node is a root in the hierarchy (has no
     *  parents). A node may have no more parents available in this
     *  node structure but is not a root in the hierarchy. If both
     *  {@code isRoot()} and {@code hasParents()} is false, the
     *  parents of this node may be accessed thorugh another node
     *  structure retrieval.
     *
     *  @return {@code true} if this node is a root in the hierarchy,
     *          {@code false} otherwise
     */

    @OSID @Override
    public boolean isRoot() {
        return (this.node.isRoot());
    }


    /**
     *  Tests if any parents are available in this node
     *  structure. There may be no more parents in this node structure
     *  however there may be parents that exist in the hierarchy.
     *
     *  @return {@code true} if this node has parents, {@code false}
     *          otherwise
     */

    @OSID @Override
    public boolean hasParents() {
        return (this.node.hasParents());
    }


    /**
     *  Gets the parents of this node. 
     *
     *  @return the parents of this node 
     */

    @OSID @Override
    public org.osid.id.IdList getParentIds() {
        return (this.node.getParentIds());
    }


    /**
     *  Tests if this node is a leaf in the hierarchy (has no
     *  children). A node may have no more children available in this
     *  node structure but is not a leaf in the hierarchy. If both
     *  {@code isLeaf()} and {@code hasChildren()} is false, the
     *  children of this node may be accessed thorugh another node
     *  structure retrieval.
     *
     *  @return {@code true} if this node is a leaf in the hierarchy,
     *          {@code false} otherwise
     */

    @OSID @Override
    public boolean isLeaf() {
        return (this.node.isLeaf());
    }


    /**
     *  Tests if any children are available in this node
     *  structure. There may be no more children available in this
     *  node structure but this node may have children in the
     *  hierarchy.
     *
     *  @return {@code true} if this node has children, {@code false}
     *           otherwise
     */

    @OSID @Override
    public boolean hasChildren() {
        return (this.node.hasChildren());
    }


    /**
     *  Gets the children of this node. 
     *
     *  @return the children of this node 
     */

    @OSID @Override
    public org.osid.id.IdList getChildIds() {
        return (this.node.getChildIds());
    }


    /**
     *  Gets the parents of this node. 
     *
     *  @return the parents of this node 
     */

    @OSID @Override
    public org.osid.hierarchy.NodeList getParents() {
        return (new net.okapia.osid.jamocha.adapter.converter.cataloging.catalognode.CatalogNodeToNodeList(this.node.getParentCatalogNodes()));
    }


    /**
     *  Gets the children of this node. 
     *
     *  @return the children of this node 
     */

    @OSID @Override
    public org.osid.hierarchy.NodeList getChildren() {
        return (new net.okapia.osid.jamocha.adapter.converter.cataloging.catalognode.CatalogNodeToNodeList(this.node.getChildCatalogNodes()));
    }
}
