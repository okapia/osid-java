//
// AbstractJob.java
//
//     Defines a Job.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.job.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Job</code>.
 */

public abstract class AbstractJob
    extends net.okapia.osid.jamocha.spi.AbstractOsidGovernator
    implements org.osid.resourcing.Job {

    private final java.util.Collection<org.osid.resourcing.Competency> competencies = new java.util.LinkedHashSet<>();

    private final java.util.Collection<org.osid.resourcing.records.JobRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Ids </code> of the competencies. 
     *
     *  @return the competency <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getCompetencyIds() {
        try {
            org.osid.resourcing.CompetencyList competencies = getCompetencies();
            return (new net.okapia.osid.jamocha.adapter.converter.resourcing.competency.CompetencyToIdList(competencies));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the competencies. 
     *
     *  @return the competencies 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyList getCompetencies()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.resourcing.competency.ArrayCompetencyList(this.competencies));
    }


    /**
     *  Adds a competency.
     *
     *  @param competency a competency
     *  @throws org.osid.NullArgumentException
     *          <code>competency</code> is <code>null</code>
     */

    protected void addCompetency(org.osid.resourcing.Competency competency) {
        nullarg(competency, "competency");
        this.competencies.add(competency);
        return;
    }


    /**
     *  Sets all the competencies.
     *
     *  @param competencies a collection of competencies
     *  @throws org.osid.NullArgumentException
     *          <code>competencies</code> is <code>null</code>
     */

    protected void setCompetencies(java.util.Collection<org.osid.resourcing.Competency> competencies) {
        nullarg(competencies, "competencies");
        this.competencies.clear();
        this.competencies.addAll(competencies);
        return;
    }


    /**
     *  Tests if this job supports the given record
     *  <code>Type</code>.
     *
     *  @param  jobRecordType a job record type 
     *  @return <code>true</code> if the jobRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>jobRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type jobRecordType) {
        for (org.osid.resourcing.records.JobRecord record : this.records) {
            if (record.implementsRecordType(jobRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>Job</code>
     *  record <code>Type</code>.
     *
     *  @param  jobRecordType the job record type 
     *  @return the job record 
     *  @throws org.osid.NullArgumentException
     *          <code>jobRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(jobRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.JobRecord getJobRecord(org.osid.type.Type jobRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.records.JobRecord record : this.records) {
            if (record.implementsRecordType(jobRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(jobRecordType + " is not supported");
    }


    /**
     *  Adds a record to this job. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param jobRecord the job record
     *  @param jobRecordType job record type
     *  @throws org.osid.NullArgumentException
     *          <code>jobRecord</code> or
     *          <code>jobRecordTypejob</code> is
     *          <code>null</code>
     */
            
    protected void addJobRecord(org.osid.resourcing.records.JobRecord jobRecord, 
                                org.osid.type.Type jobRecordType) {
        
        nullarg(jobRecord, "job record");
        addRecordType(jobRecordType);
        this.records.add(jobRecord);
        
        return;
    }
}
