//
// AbstractImmutableAddress.java
//
//     Wraps a mutable Address to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.contact.address.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Address</code> to hide modifiers. This
 *  wrapper provides an immutized Address from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying address whose state changes are visible.
 */

public abstract class AbstractImmutableAddress
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.contact.Address {

    private final org.osid.contact.Address address;


    /**
     *  Constructs a new <code>AbstractImmutableAddress</code>.
     *
     *  @param address the address to immutablize
     *  @throws org.osid.NullArgumentException <code>address</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableAddress(org.osid.contact.Address address) {
        super(address);
        this.address = address;
        return;
    }


    /**
     *  Gets the resource <code> Id </code> to which this address belongs. All 
     *  addresses belong to one resource but may be used for contacts among 
     *  multiple other resources. 
     *
     *  @return a resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResourceId() {
        return (this.address.getResourceId());
    }


    /**
     *  Gets the resource to which this address belongs. All addresses belong 
     *  to one resource but may be used for contacts among multiple other 
     *  resources. 
     *
     *  @return a resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource()
        throws org.osid.OperationFailedException {

        return (this.address.getResource());
    }


    /**
     *  Gets the textual representation of this address. 
     *
     *  @return the address 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getAddressText() {
        return (this.address.getAddressText());
    }


    /**
     *  Gets the address record corresponding to the given <code> Address 
     *  </code> record <code> Type. </code> This method must be used to 
     *  retrieve an object implementing the requested record. The <code> 
     *  addressRecordType </code> may be the <code> Type </code> returned in 
     *  <code> getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(addressRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  addressRecordType the type of address record to retrieve 
     *  @return the address record 
     *  @throws org.osid.NullArgumentException <code> addressRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(addressRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.contact.records.AddressRecord getAddressRecord(org.osid.type.Type addressRecordType)
        throws org.osid.OperationFailedException {

        return (this.address.getAddressRecord(addressRecordType));
    }
}

