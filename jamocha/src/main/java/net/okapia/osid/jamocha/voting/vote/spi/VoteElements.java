//
// VoteElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.vote.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class VoteElements
    extends net.okapia.osid.jamocha.spi.OsidRelationshipElements {


    /**
     *  Gets the VoteElement Id.
     *
     *  @return the vote element Id
     */

    public static org.osid.id.Id getVoteEntityId() {
        return (makeEntityId("osid.voting.Vote"));
    }


    /**
     *  Gets the CandidateId element Id.
     *
     *  @return the CandidateId element Id
     */

    public static org.osid.id.Id getCandidateId() {
        return (makeElementId("osid.voting.vote.CandidateId"));
    }


    /**
     *  Gets the Candidate element Id.
     *
     *  @return the Candidate element Id
     */

    public static org.osid.id.Id getCandidate() {
        return (makeElementId("osid.voting.vote.Candidate"));
    }


    /**
     *  Gets the VoterId element Id.
     *
     *  @return the VoterId element Id
     */

    public static org.osid.id.Id getVoterId() {
        return (makeElementId("osid.voting.vote.VoterId"));
    }


    /**
     *  Gets the Voter element Id.
     *
     *  @return the Voter element Id
     */

    public static org.osid.id.Id getVoter() {
        return (makeElementId("osid.voting.vote.Voter"));
    }


    /**
     *  Gets the VotingAgentId element Id.
     *
     *  @return the VotingAgentId element Id
     */

    public static org.osid.id.Id getVotingAgentId() {
        return (makeElementId("osid.voting.vote.VotingAgentId"));
    }


    /**
     *  Gets the VotingAgent element Id.
     *
     *  @return the VotingAgent element Id
     */

    public static org.osid.id.Id getVotingAgent() {
        return (makeElementId("osid.voting.vote.VotingAgent"));
    }


    /**
     *  Gets the Votes element Id.
     *
     *  @return the Votes element Id
     */

    public static org.osid.id.Id getVotes() {
        return (makeElementId("osid.voting.vote.Votes"));
    }


    /**
     *  Gets the MinimumVotes element Id.
     *
     *  @return the MinimumVotes element Id
     */

    public static org.osid.id.Id getMinimumVotes() {
        return (makeQueryElementId("osid.voting.vote.MinimumVotes"));
    }


    /**
     *  Gets the PollsId element Id.
     *
     *  @return the PollsId element Id
     */

    public static org.osid.id.Id getPollsId() {
        return (makeQueryElementId("osid.voting.vote.PollsId"));
    }


    /**
     *  Gets the Polls element Id.
     *
     *  @return the Polls element Id
     */

    public static org.osid.id.Id getPolls() {
        return (makeQueryElementId("osid.voting.vote.Polls"));
    }
}
