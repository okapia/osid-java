//
// AbstractAdapterHierarchyLookupSession.java
//
//    A Hierarchy lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.hierarchy.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Hierarchy lookup session adapter.
 */

public abstract class AbstractAdapterHierarchyLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.hierarchy.HierarchyLookupSession {

    private final org.osid.hierarchy.HierarchyLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterHierarchyLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterHierarchyLookupSession(org.osid.hierarchy.HierarchyLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Hierarchy} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupHierarchies() {
        return (this.session.canLookupHierarchies());
    }


    /**
     *  A complete view of the {@code Hierarchy} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeHierarchyView() {
        this.session.useComparativeHierarchyView();
        return;
    }


    /**
     *  A complete view of the {@code Hierarchy} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryHierarchyView() {
        this.session.usePlenaryHierarchyView();
        return;
    }

     
    /**
     *  Gets the {@code Hierarchy} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Hierarchy} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Hierarchy} and
     *  retained for compatibility.
     *
     *  @param hierarchyId {@code Id} of the {@code Hierarchy}
     *  @return the hierarchy
     *  @throws org.osid.NotFoundException {@code hierarchyId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code hierarchyId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Hierarchy getHierarchy(org.osid.id.Id hierarchyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getHierarchy(hierarchyId));
    }


    /**
     *  Gets a {@code HierarchyList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  hierarchies specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Hierarchies} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  hierarchyIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Hierarchy} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code hierarchyIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyList getHierarchiesByIds(org.osid.id.IdList hierarchyIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getHierarchiesByIds(hierarchyIds));
    }


    /**
     *  Gets a {@code HierarchyList} corresponding to the given
     *  hierarchy genus {@code Type} which does not include
     *  hierarchies of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  hierarchies or an error results. Otherwise, the returned list
     *  may contain only those hierarchies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  hierarchyGenusType a hierarchy genus type 
     *  @return the returned {@code Hierarchy} list
     *  @throws org.osid.NullArgumentException
     *          {@code hierarchyGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyList getHierarchiesByGenusType(org.osid.type.Type hierarchyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getHierarchiesByGenusType(hierarchyGenusType));
    }


    /**
     *  Gets a {@code HierarchyList} corresponding to the given
     *  hierarchy genus {@code Type} and include any additional
     *  hierarchies with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  hierarchies or an error results. Otherwise, the returned list
     *  may contain only those hierarchies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  hierarchyGenusType a hierarchy genus type 
     *  @return the returned {@code Hierarchy} list
     *  @throws org.osid.NullArgumentException
     *          {@code hierarchyGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyList getHierarchiesByParentGenusType(org.osid.type.Type hierarchyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getHierarchiesByParentGenusType(hierarchyGenusType));
    }


    /**
     *  Gets a {@code HierarchyList} containing the given
     *  hierarchy record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  hierarchies or an error results. Otherwise, the returned list
     *  may contain only those hierarchies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  hierarchyRecordType a hierarchy record type 
     *  @return the returned {@code Hierarchy} list
     *  @throws org.osid.NullArgumentException
     *          {@code hierarchyRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyList getHierarchiesByRecordType(org.osid.type.Type hierarchyRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getHierarchiesByRecordType(hierarchyRecordType));
    }


    /**
     *  Gets a {@code HierarchyList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  hierarchies or an error results. Otherwise, the returned list
     *  may contain only those hierarchies that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Hierarchy} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyList getHierarchiesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getHierarchiesByProvider(resourceId));
    }


    /**
     *  Gets all {@code Hierarchies}. 
     *
     *  In plenary mode, the returned list contains all known
     *  hierarchies or an error results. Otherwise, the returned list
     *  may contain only those hierarchies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Hierarchies} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyList getHierarchies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getHierarchies());
    }
}
