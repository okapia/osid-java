//
// AbstractGrade.java
//
//     Defines a Grade.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.grade.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Grade</code>.
 */

public abstract class AbstractGrade
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.grading.Grade {

    private org.osid.grading.GradeSystem gradeSystem;
    private java.math.BigDecimal inputScoreStartRange;
    private java.math.BigDecimal inputScoreEndRange;
    private java.math.BigDecimal outputScore;

    private final java.util.Collection<org.osid.grading.records.GradeRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> GradeSystem Id </code> in which this grade belongs. 
     *
     *  @return the grade system <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getGradeSystemId() {
        return (this.gradeSystem.getId());
    }


    /**
     *  Gets the <code> GradeSystem </code> in which this grade belongs. 
     *
     *  @return the grade system 
     */

    @OSID @Override
    public org.osid.grading.GradeSystem getGradeSystem() {
        return (this.gradeSystem);
    }


    /**
     *  Sets the grade system.
     *
     *  @param system a grade system
     *  @throws org.osid.NullArgumentException
     *          <code>system</code> is <code>null</code>
     */

    protected void setGradeSystem(org.osid.grading.GradeSystem system) {
        nullarg(system, "grade system");
        this.gradeSystem = system;
        return;
    }


    /**
     *  Gets the low end of the input score range equivalent to this grade. 
     *
     *  @return the start range 
     */

    @OSID @Override
    public java.math.BigDecimal getInputScoreStartRange() {
        return (this.inputScoreStartRange);
    }


    /**
     *  Sets the input score start range.
     *
     *  @param start an input score start range
     *  @throws org.osid.NullArgumentException <code>start</code> is
     *          <code>null</code>
     */

    protected void setInputScoreStartRange(java.math.BigDecimal start) {
        nullarg(start, "input score start range");
        this.inputScoreStartRange = start;
        return;
    }


    /**
     *  Gets the high end of the input score range equivalent to this grade. 
     *
     *  @return the end range 
     */

    @OSID @Override
    public java.math.BigDecimal getInputScoreEndRange() {
        return (this.inputScoreEndRange);
    }


    /**
     *  Sets the input score end range.
     *
     *  @param end an input score end range
     *  @throws org.osid.NullArgumentException <code>end</code> is
     *          <code>null</code>
     */

    protected void setInputScoreEndRange(java.math.BigDecimal end) {
        nullarg(end, "input score end range");
        this.inputScoreEndRange = end;
        return;
    }


    /**
     *  Gets the output score for this grade used for calculating cumultives 
     *  or performing articulation. 
     *
     *  @return the output score 
     */

    @OSID @Override
    public java.math.BigDecimal getOutputScore() {
        return (this.outputScore);
    }


    /**
     *  Sets the output score.
     *
     *  @param score an output score
     *  @throws org.osid.NullArgumentException
     *          <code>score</code> is <code>null</code>
     */

    protected void setOutputScore(java.math.BigDecimal score) {
        nullarg(score, "score");
        this.outputScore = score;
        return;
    }


    /**
     *  Tests if this grade supports the given record
     *  <code>Type</code>.
     *
     *  @param  gradeRecordType a grade record type 
     *  @return <code>true</code> if the gradeRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>gradeRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type gradeRecordType) {
        for (org.osid.grading.records.GradeRecord record : this.records) {
            if (record.implementsRecordType(gradeRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Grade</code> record <code>Type</code>.
     *
     *  @param  gradeRecordType the grade record type 
     *  @return the grade record 
     *  @throws org.osid.NullArgumentException
     *          <code>gradeRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(gradeRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.records.GradeRecord getGradeRecord(org.osid.type.Type gradeRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.grading.records.GradeRecord record : this.records) {
            if (record.implementsRecordType(gradeRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradeRecordType + " is not supported");
    }


    /**
     *  Adds a record to this grade. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param gradeRecord the grade record
     *  @param gradeRecordType grade record type
     *  @throws org.osid.NullArgumentException
     *          <code>gradeRecord</code> or
     *          <code>gradeRecordTypegrade</code> is
     *          <code>null</code>
     */
            
    protected void addGradeRecord(org.osid.grading.records.GradeRecord gradeRecord, 
                                  org.osid.type.Type gradeRecordType) {
        
        nullarg(gradeRecord, "grade record");
        addRecordType(gradeRecordType);
        this.records.add(gradeRecord);
        
        return;
    }
}
