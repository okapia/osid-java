//
// SettingMiter.java
//
//     Defines a Setting miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.control.setting;


/**
 *  Defines a <code>Setting</code> miter for use with the builders.
 */

public interface SettingMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.control.Setting {


    /**
     *  Sets the controller.
     *
     *  @param controller a controller
     *  @throws org.osid.NullArgumentException <code>controller</code>
     *          is <code>null</code>
     */

    public void setController(org.osid.control.Controller controller);


    /**
     *  Sets the on flag.
     *
     *  @param on {@code true} is on, {@code false} if off or unknown
     */

    public void setOn(boolean on);


    /**
     *  Sets the off flag.
     *
     *  @param off {@code true} is off, {@code false} if on or unknown
     */

    public void setOff(boolean off);


    /**
     *  Sets the variable amount.
     *
     *  @param amount a variable amount
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    public void setVariableAmount(java.math.BigDecimal amount);

    
    /**
     *  Sets the discreet state.
     *
     *  @param state a discreet state
     *  @throws org.osid.NullArgumentException <code>state</code> is
     *          <code>null</code>
     */

    public void setDiscreetState(org.osid.process.State state);


    /**
     *  Sets the ramp rate.
     *
     *  @param rate a ramp rate
     *  @throws org.osid.NullArgumentException <code>rate</code> is
     *          <code>null</code>
     */

    public void setRampRate(org.osid.calendaring.Duration rate);


    /**
     *  Adds a Setting record.
     *
     *  @param record a setting record
     *  @param recordType the type of setting record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addSettingRecord(org.osid.control.records.SettingRecord record, org.osid.type.Type recordType);
}       


