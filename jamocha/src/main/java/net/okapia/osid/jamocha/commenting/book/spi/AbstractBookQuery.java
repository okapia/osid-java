//
// AbstractBookQuery.java
//
//     A template for making a Book Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.commenting.book.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for books.
 */

public abstract class AbstractBookQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.commenting.BookQuery {

    private final java.util.Collection<org.osid.commenting.records.BookQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the comment <code> Id </code> for this query to match comments 
     *  assigned to books. 
     *
     *  @param  commentId a comment <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> commentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCommentId(org.osid.id.Id commentId, boolean match) {
        return;
    }


    /**
     *  Clears the comment <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCommentIdTerms() {
        return;
    }


    /**
     *  Tests if a comment query is available. 
     *
     *  @return <code> true </code> if a comment query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommentQuery() {
        return (false);
    }


    /**
     *  Gets the query for a comment. 
     *
     *  @return the comment query 
     *  @throws org.osid.UnimplementedException <code> supportsCommentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentQuery getCommentQuery() {
        throw new org.osid.UnimplementedException("supportsCommentQuery() is false");
    }


    /**
     *  Matches books with any comment. 
     *
     *  @param  match <code> true </code> to match books with any comment, 
     *          <code> false </code> to match books with no comments 
     */

    @OSID @Override
    public void matchAnyComment(boolean match) {
        return;
    }


    /**
     *  Clears the comment terms. 
     */

    @OSID @Override
    public void clearCommentTerms() {
        return;
    }


    /**
     *  Sets the book <code> Id </code> for this query to match books that 
     *  have the specified book as an ancestor. 
     *
     *  @param  bookId a book <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, a <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> bookId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchAncestorBookId(org.osid.id.Id bookId, boolean match) {
        return;
    }


    /**
     *  Clears the ancestor book <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorBookIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BookQuery </code> is available. 
     *
     *  @return <code> true </code> if a book query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorBookQuery() {
        return (false);
    }


    /**
     *  Gets the query for a book. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the book query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorBookQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.BookQuery getAncestorBookQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorBookQuery() is false");
    }


    /**
     *  Matches books with any ancestor. 
     *
     *  @param  match <code> true </code> to match books with any ancestor, 
     *          <code> false </code> to match root books 
     */

    @OSID @Override
    public void matchAnyAncestorBook(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor book terms. 
     */

    @OSID @Override
    public void clearAncestorBookTerms() {
        return;
    }


    /**
     *  Sets the book <code> Id </code> for this query to match books that 
     *  have the specified book as a descendant. 
     *
     *  @param  bookId a book <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> bookId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchDescendantBookId(org.osid.id.Id bookId, boolean match) {
        return;
    }


    /**
     *  Clears the descendant book <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantBookIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BookQuery </code> is available. 
     *
     *  @return <code> true </code> if a book query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantBookQuery() {
        return (false);
    }


    /**
     *  Gets the query for a book. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the book query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantBookQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.BookQuery getDescendantBookQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantBookQuery() is false");
    }


    /**
     *  Matches books with any descendant. 
     *
     *  @param  match <code> true </code> to match books with any descendant, 
     *          <code> false </code> to match leaf books 
     */

    @OSID @Override
    public void matchAnyDescendantBook(boolean match) {
        return;
    }


    /**
     *  Clears the descendant book terms. 
     */

    @OSID @Override
    public void clearDescendantBookTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given book query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a book implementing the requested record.
     *
     *  @param bookRecordType a book record type
     *  @return the book query record
     *  @throws org.osid.NullArgumentException
     *          <code>bookRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(bookRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.commenting.records.BookQueryRecord getBookQueryRecord(org.osid.type.Type bookRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.commenting.records.BookQueryRecord record : this.records) {
            if (record.implementsRecordType(bookRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(bookRecordType + " is not supported");
    }


    /**
     *  Adds a record to this book query. 
     *
     *  @param bookQueryRecord book query record
     *  @param bookRecordType book record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBookQueryRecord(org.osid.commenting.records.BookQueryRecord bookQueryRecord, 
                                          org.osid.type.Type bookRecordType) {

        addRecordType(bookRecordType);
        nullarg(bookQueryRecord, "book query record");
        this.records.add(bookQueryRecord);        
        return;
    }
}
