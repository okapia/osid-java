//
// AbstractMapParameterProcessorEnablerLookupSession
//
//    A simple framework for providing a ParameterProcessorEnabler lookup service
//    backed by a fixed collection of parameter processor enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.configuration.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a ParameterProcessorEnabler lookup service backed by a
 *  fixed collection of parameter processor enablers. The parameter processor enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>ParameterProcessorEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapParameterProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.configuration.rules.spi.AbstractParameterProcessorEnablerLookupSession
    implements org.osid.configuration.rules.ParameterProcessorEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.configuration.rules.ParameterProcessorEnabler> parameterProcessorEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.configuration.rules.ParameterProcessorEnabler>());


    /**
     *  Makes a <code>ParameterProcessorEnabler</code> available in this session.
     *
     *  @param  parameterProcessorEnabler a parameter processor enabler
     *  @throws org.osid.NullArgumentException <code>parameterProcessorEnabler<code>
     *          is <code>null</code>
     */

    protected void putParameterProcessorEnabler(org.osid.configuration.rules.ParameterProcessorEnabler parameterProcessorEnabler) {
        this.parameterProcessorEnablers.put(parameterProcessorEnabler.getId(), parameterProcessorEnabler);
        return;
    }


    /**
     *  Makes an array of parameter processor enablers available in this session.
     *
     *  @param  parameterProcessorEnablers an array of parameter processor enablers
     *  @throws org.osid.NullArgumentException <code>parameterProcessorEnablers<code>
     *          is <code>null</code>
     */

    protected void putParameterProcessorEnablers(org.osid.configuration.rules.ParameterProcessorEnabler[] parameterProcessorEnablers) {
        putParameterProcessorEnablers(java.util.Arrays.asList(parameterProcessorEnablers));
        return;
    }


    /**
     *  Makes a collection of parameter processor enablers available in this session.
     *
     *  @param  parameterProcessorEnablers a collection of parameter processor enablers
     *  @throws org.osid.NullArgumentException <code>parameterProcessorEnablers<code>
     *          is <code>null</code>
     */

    protected void putParameterProcessorEnablers(java.util.Collection<? extends org.osid.configuration.rules.ParameterProcessorEnabler> parameterProcessorEnablers) {
        for (org.osid.configuration.rules.ParameterProcessorEnabler parameterProcessorEnabler : parameterProcessorEnablers) {
            this.parameterProcessorEnablers.put(parameterProcessorEnabler.getId(), parameterProcessorEnabler);
        }

        return;
    }


    /**
     *  Removes a ParameterProcessorEnabler from this session.
     *
     *  @param  parameterProcessorEnablerId the <code>Id</code> of the parameter processor enabler
     *  @throws org.osid.NullArgumentException <code>parameterProcessorEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeParameterProcessorEnabler(org.osid.id.Id parameterProcessorEnablerId) {
        this.parameterProcessorEnablers.remove(parameterProcessorEnablerId);
        return;
    }


    /**
     *  Gets the <code>ParameterProcessorEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  parameterProcessorEnablerId <code>Id</code> of the <code>ParameterProcessorEnabler</code>
     *  @return the parameterProcessorEnabler
     *  @throws org.osid.NotFoundException <code>parameterProcessorEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>parameterProcessorEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnabler getParameterProcessorEnabler(org.osid.id.Id parameterProcessorEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.configuration.rules.ParameterProcessorEnabler parameterProcessorEnabler = this.parameterProcessorEnablers.get(parameterProcessorEnablerId);
        if (parameterProcessorEnabler == null) {
            throw new org.osid.NotFoundException("parameterProcessorEnabler not found: " + parameterProcessorEnablerId);
        }

        return (parameterProcessorEnabler);
    }


    /**
     *  Gets all <code>ParameterProcessorEnablers</code>. In plenary mode, the returned
     *  list contains all known parameterProcessorEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  parameterProcessorEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>ParameterProcessorEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerList getParameterProcessorEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.configuration.rules.parameterprocessorenabler.ArrayParameterProcessorEnablerList(this.parameterProcessorEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.parameterProcessorEnablers.clear();
        super.close();
        return;
    }
}
