//
// AbstractAssessmentPart.java
//
//     Defines an AssessmentPart builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.assessment.authoring.assessmentpart.spi;


/**
 *  Defines an <code>AssessmentPart</code> builder.
 */

public abstract class AbstractAssessmentPartBuilder<T extends AbstractAssessmentPartBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOperableOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.assessment.authoring.assessmentpart.AssessmentPartMiter assessmentPart;


    /**
     *  Constructs a new <code>AbstractAssessmentPartBuilder</code>.
     *
     *  @param assessmentPart the assessment part to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractAssessmentPartBuilder(net.okapia.osid.jamocha.builder.assessment.authoring.assessmentpart.AssessmentPartMiter assessmentPart) {
        super(assessmentPart);
        this.assessmentPart = assessmentPart;
        return;
    }


    /**
     *  Builds the assessment part.
     *
     *  @return the new assessment part
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.assessment.authoring.AssessmentPart build() {
        (new net.okapia.osid.jamocha.builder.validator.assessment.authoring.assessmentpart.AssessmentPartValidator(getValidations())).validate(this.assessmentPart);
        return (new net.okapia.osid.jamocha.builder.assessment.authoring.assessmentpart.ImmutableAssessmentPart(this.assessmentPart));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the assessment part miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.assessment.authoring.assessmentpart.AssessmentPartMiter getMiter() {
        return (this.assessmentPart);
    }


    /**
     *  Sets the sequestered flag.
     */

    public T sequestered() {
        getMiter().setSequestered(true);
        return (self());
    }


    /**
     *  Sets the unsequestered flag.
     */

    public T unsequestered() {
        getMiter().setSequestered(false);
        return (self());
    }


    /**
     *  Sets the assessment.
     *
     *  @param assessment an assessment
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>assessment</code> is <code>null</code>
     */

    public T assessment(org.osid.assessment.Assessment assessment) {
        getMiter().setAssessment(assessment);
        return (self());
    }


    /**
     *  Sets the assessment part.
     *
     *  @param parent an assessment part
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>parent</code> is
     *          <code>null</code>
     */

    public T parent(org.osid.assessment.authoring.AssessmentPart parent) {
        getMiter().setParentAssessmentPart(parent);
        return (self());
    }


    /**
     *  Sets the section flag.
     *
     *  @return the builder
     */

    public T section() {
        getMiter().setSection(true);
        return (self());
    }


    /**
     *  Unets the section flag.
     *
     *  @return the builder
     */

    public T nosection() {
        getMiter().setSection(false);
        return (self());
    }


    /**
     *  Sets the weight.
     *
     *  @param weight a weight
     *  @return the builder
     */

    public T weight(long weight) {
        getMiter().setWeight(weight);
        return (self());
    }


    /**
     *  Sets the allocated time.
     *
     *  @param time an allocated time
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    public T allocatedTime(org.osid.calendaring.Duration time) {
        getMiter().setAllocatedTime(time);
        return (self());
    }


    /**
     *  Adds a child assessment part.
     *
     *  @param child a child assessment part
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>child</code> is
     *          <code>null</code>
     */

    public T child(org.osid.assessment.authoring.AssessmentPart child) {
        getMiter().addChildAssessmentPart(child);
        return (self());
    }


    /**
     *  Sets all the child assessment parts.
     *
     *  @param children a collection of child assessment parts
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>children</code>
     *          is <code>null</code>
     */

    public T children(java.util.Collection<org.osid.assessment.authoring.AssessmentPart> children) {
        getMiter().setChildAssessmentParts(children);
        return (self());
    }


    /**
     *  Adds an AssessmentPart record.
     *
     *  @param record an assessment part record
     *  @param recordType the type of assessment part record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.assessment.authoring.records.AssessmentPartRecord record, org.osid.type.Type recordType) {
        getMiter().addAssessmentPartRecord(record, recordType);
        return (self());
    }
}       


