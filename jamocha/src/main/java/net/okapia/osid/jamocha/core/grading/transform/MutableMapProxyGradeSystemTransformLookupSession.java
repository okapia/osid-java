//
// MutableMapProxyGradeSystemTransformLookupSession
//
//    Implements a GradeSystemTransform lookup service backed by a collection of
//    gradeSystemTransforms that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.grading.transform;


/**
 *  Implements a GradeSystemTransform lookup service backed by a collection of
 *  gradeSystemTransforms. The gradeSystemTransforms are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of grade system transforms can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyGradeSystemTransformLookupSession
    extends net.okapia.osid.jamocha.core.grading.transform.spi.AbstractMapGradeSystemTransformLookupSession
    implements org.osid.grading.transform.GradeSystemTransformLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyGradeSystemTransformLookupSession}
     *  with no grade system transforms.
     *
     *  @param gradebook the gradebook
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code gradebook} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyGradeSystemTransformLookupSession(org.osid.grading.Gradebook gradebook,
                                                  org.osid.proxy.Proxy proxy) {
        setGradebook(gradebook);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyGradeSystemTransformLookupSession} with a
     *  single grade system transform.
     *
     *  @param gradebook the gradebook
     *  @param gradeSystemTransform a grade system transform
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code gradebook},
     *          {@code gradeSystemTransform}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyGradeSystemTransformLookupSession(org.osid.grading.Gradebook gradebook,
                                                org.osid.grading.transform.GradeSystemTransform gradeSystemTransform, org.osid.proxy.Proxy proxy) {
        this(gradebook, proxy);
        putGradeSystemTransform(gradeSystemTransform);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyGradeSystemTransformLookupSession} using an
     *  array of grade system transforms.
     *
     *  @param gradebook the gradebook
     *  @param gradeSystemTransforms an array of grade system transforms
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code gradebook},
     *          {@code gradeSystemTransforms}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyGradeSystemTransformLookupSession(org.osid.grading.Gradebook gradebook,
                                                org.osid.grading.transform.GradeSystemTransform[] gradeSystemTransforms, org.osid.proxy.Proxy proxy) {
        this(gradebook, proxy);
        putGradeSystemTransforms(gradeSystemTransforms);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyGradeSystemTransformLookupSession} using a
     *  collection of grade system transforms.
     *
     *  @param gradebook the gradebook
     *  @param gradeSystemTransforms a collection of grade system transforms
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code gradebook},
     *          {@code gradeSystemTransforms}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyGradeSystemTransformLookupSession(org.osid.grading.Gradebook gradebook,
                                                java.util.Collection<? extends org.osid.grading.transform.GradeSystemTransform> gradeSystemTransforms,
                                                org.osid.proxy.Proxy proxy) {
   
        this(gradebook, proxy);
        setSessionProxy(proxy);
        putGradeSystemTransforms(gradeSystemTransforms);
        return;
    }

    
    /**
     *  Makes a {@code GradeSystemTransform} available in this session.
     *
     *  @param gradeSystemTransform an grade system transform
     *  @throws org.osid.NullArgumentException {@code gradeSystemTransform{@code 
     *          is {@code null}
     */

    @Override
    public void putGradeSystemTransform(org.osid.grading.transform.GradeSystemTransform gradeSystemTransform) {
        super.putGradeSystemTransform(gradeSystemTransform);
        return;
    }


    /**
     *  Makes an array of gradeSystemTransforms available in this session.
     *
     *  @param gradeSystemTransforms an array of grade system transforms
     *  @throws org.osid.NullArgumentException {@code gradeSystemTransforms{@code 
     *          is {@code null}
     */

    @Override
    public void putGradeSystemTransforms(org.osid.grading.transform.GradeSystemTransform[] gradeSystemTransforms) {
        super.putGradeSystemTransforms(gradeSystemTransforms);
        return;
    }


    /**
     *  Makes collection of grade system transforms available in this session.
     *
     *  @param gradeSystemTransforms
     *  @throws org.osid.NullArgumentException {@code gradeSystemTransform{@code 
     *          is {@code null}
     */

    @Override
    public void putGradeSystemTransforms(java.util.Collection<? extends org.osid.grading.transform.GradeSystemTransform> gradeSystemTransforms) {
        super.putGradeSystemTransforms(gradeSystemTransforms);
        return;
    }


    /**
     *  Removes a GradeSystemTransform from this session.
     *
     *  @param gradeSystemTransformId the {@code Id} of the grade system transform
     *  @throws org.osid.NullArgumentException {@code gradeSystemTransformId{@code  is
     *          {@code null}
     */

    @Override
    public void removeGradeSystemTransform(org.osid.id.Id gradeSystemTransformId) {
        super.removeGradeSystemTransform(gradeSystemTransformId);
        return;
    }    
}
