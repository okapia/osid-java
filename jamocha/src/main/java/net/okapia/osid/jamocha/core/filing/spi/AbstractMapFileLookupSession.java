//
// AbstractMapFileLookupSession
//
//    A simple framework for providing a File lookup service
//    backed by a fixed collection of files.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.filing.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a File lookup service backed by a
 *  fixed collection of files. The files are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Files</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapFileLookupSession
    extends net.okapia.osid.jamocha.filing.spi.AbstractFileLookupSession
    implements org.osid.filing.FileLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.filing.File> files = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.filing.File>());


    /**
     *  Makes a <code>File</code> available in this session.
     *
     *  @param  file a file
     *  @throws org.osid.NullArgumentException <code>file<code>
     *          is <code>null</code>
     */

    protected void putFile(org.osid.filing.File file) {
        this.files.put(file.getId(), file);
        return;
    }


    /**
     *  Makes an array of files available in this session.
     *
     *  @param  files an array of files
     *  @throws org.osid.NullArgumentException <code>files<code>
     *          is <code>null</code>
     */

    protected void putFiles(org.osid.filing.File[] files) {
        putFiles(java.util.Arrays.asList(files));
        return;
    }


    /**
     *  Makes a collection of files available in this session.
     *
     *  @param  files a collection of files
     *  @throws org.osid.NullArgumentException <code>files<code>
     *          is <code>null</code>
     */

    protected void putFiles(java.util.Collection<? extends org.osid.filing.File> files) {
        for (org.osid.filing.File file : files) {
            this.files.put(file.getId(), file);
        }

        return;
    }


    /**
     *  Removes a File from this session.
     *
     *  @param  fileId the <code>Id</code> of the file
     *  @throws org.osid.NullArgumentException <code>fileId<code> is
     *          <code>null</code>
     */

    protected void removeFile(org.osid.id.Id fileId) {
        this.files.remove(fileId);
        return;
    }


    /**
     *  Gets the <code>File</code> specified by its <code>Id</code>.
     *
     *  @param  fileId <code>Id</code> of the <code>File</code>
     *  @return the file
     *  @throws org.osid.NotFoundException <code>fileId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>fileId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.File getFile(org.osid.id.Id fileId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.filing.File file = this.files.get(fileId);
        if (file == null) {
            throw new org.osid.NotFoundException("file not found: " + fileId);
        }

        return (file);
    }


    /**
     *  Gets all <code>Files</code>. In plenary mode, the returned
     *  list contains all known files or an error
     *  results. Otherwise, the returned list may contain only those
     *  files that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Files</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.FileList getFiles()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.filing.file.ArrayFileList(this.files.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.files.clear();
        super.close();
        return;
    }
}
