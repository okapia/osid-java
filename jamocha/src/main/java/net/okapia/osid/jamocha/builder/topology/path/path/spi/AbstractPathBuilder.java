//
// AbstractPath.java
//
//     Defines a Path builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.topology.path.path.spi;


/**
 *  Defines a <code>Path</code> builder.
 */

public abstract class AbstractPathBuilder<T extends AbstractPathBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.topology.path.path.PathMiter path;


    /**
     *  Constructs a new <code>AbstractPathBuilder</code>.
     *
     *  @param path the path to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractPathBuilder(net.okapia.osid.jamocha.builder.topology.path.path.PathMiter path) {
        super(path);
        this.path = path;
        return;
    }


    /**
     *  Builds the path.
     *
     *  @return the new path
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.topology.path.Path build() {
        (new net.okapia.osid.jamocha.builder.validator.topology.path.path.PathValidator(getValidations())).validate(this.path);
        return (new net.okapia.osid.jamocha.builder.topology.path.path.ImmutablePath(this.path));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the path miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.topology.path.path.PathMiter getMiter() {
        return (this.path);
    }


    /**
     *  Sets the starting node.
     *
     *  @param node a starting node
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>node</code> is
     *          <code>null</code>
     */

    public T startingNode(org.osid.topology.Node node) {
        getMiter().setStartingNode(node);
        return (self());
    }


    /**
     *  Sets the ending node.
     *
     *  @param node an ending node
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>node</code> is
     *          <code>null</code>
     */

    public T endingNode(org.osid.topology.Node node) {
        getMiter().setEndingNode(node);
        return (self());
    }


    /**
     *  Sets the hops.
     *
     *  @param hop a hops
     *  @return the builder
     *  @throws org.osid.InvalidArgumentException <code>hop</code> is
     *          negative
     */

    public T hop(long hop) {
        getMiter().setHops(hop);
        return (self());
    }


    /**
     *  Sets the distance.
     *
     *  @param distance a distance
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>distance</code>
     *          is <code>null</code>
     */

    public T distance(java.math.BigDecimal distance) {
        getMiter().setDistance(distance);
        return (self());
    }


    /**
     *  Sets the cost.
     *
     *  @param cost a cost
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>cost</code> is
     *          <code>null</code>
     */

    public T cost(java.math.BigDecimal cost) {
        getMiter().setCost(cost);
        return (self());
    }


    /**
     *  Adds an edge.
     *
     *  @param edge an edge
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>edge</code> is
     *          <code>null</code>
     */

    public T edge(org.osid.topology.Edge edge) {
        getMiter().addEdge(edge);
        return (self());
    }


    /**
     *  Sets all the edges.
     *
     *  @param edges a collection of edges
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>edges</code> is
     *          <code>null</code>
     */

    public T edges(java.util.Collection<org.osid.topology.Edge> edges) {
        getMiter().setEdges(edges);
        return (self());
    }


    /**
     *  Adds a Path record.
     *
     *  @param record a path record
     *  @param recordType the type of path record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.topology.path.records.PathRecord record, org.osid.type.Type recordType) {
        getMiter().addPathRecord(record, recordType);
        return (self());
    }
}       


