//
// AbstractFoundryLookupSession.java
//
//    A starter implementation framework for providing a Foundry
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A starter implementation framework for providing a Foundry lookup
 *  service.
 *
 *  Although this abstract class requires only the implementation of
 *  getFoundries(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractFoundryLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.resourcing.FoundryLookupSession {

    private boolean pedantic = false;
    private org.osid.resourcing.Foundry foundry = new net.okapia.osid.jamocha.nil.resourcing.foundry.UnknownFoundry();
    


    /**
     *  Tests if this user can perform <code>Foundry</code> lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupFoundries() {
        return (true);
    }


    /**
     *  A complete view of the <code>Foundry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeFoundryView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Foundry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryFoundryView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }

     
    /**
     *  Gets the <code>Foundry</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Foundry</code> may have a different <code>Id</code> than
     *  requested, such as the case where a duplicate <code>Id</code>
     *  was assigned to a <code>Foundry</code> and retained for
     *  compatibility.
     *
     *  @param  foundryId <code>Id</code> of the
     *          <code>Foundry</code>
     *  @return the foundry
     *  @throws org.osid.NotFoundException <code>foundryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>foundryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Foundry getFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.resourcing.FoundryList foundries = getFoundries()) {
            while (foundries.hasNext()) {
                org.osid.resourcing.Foundry foundry = foundries.getNextFoundry();
                if (foundry.getId().equals(foundryId)) {
                    return (foundry);
                }
            }
        } 

        throw new org.osid.NotFoundException(foundryId + " not found");
    }


    /**
     *  Gets a <code>FoundryList</code> corresponding to the given
     *  <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  foundries specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Foundries</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getFoundries()</code>.
     *
     *  @param  foundryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Foundry</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>foundryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryList getFoundriesByIds(org.osid.id.IdList foundryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.resourcing.Foundry> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = foundryIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getFoundry(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("foundry " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.resourcing.foundry.LinkedFoundryList(ret));
    }


    /**
     *  Gets a <code>FoundryList</code> corresponding to the given
     *  foundry genus <code>Type</code> which does not include
     *  foundries of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  foundries or an error results. Otherwise, the returned list
     *  may contain only those foundries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getFoundries()</code>.
     *
     *  @param  foundryGenusType a foundry genus type 
     *  @return the returned <code>Foundry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>foundryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryList getFoundriesByGenusType(org.osid.type.Type foundryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.foundry.FoundryGenusFilterList(getFoundries(), foundryGenusType));
    }


    /**
     *  Gets a <code>FoundryList</code> corresponding to the given
     *  foundry genus <code>Type</code> and include any additional
     *  foundries with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  foundries or an error results. Otherwise, the returned list
     *  may contain only those foundries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getFoundries()</code>.
     *
     *  @param  foundryGenusType a foundry genus type 
     *  @return the returned <code>Foundry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>foundryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryList getFoundriesByParentGenusType(org.osid.type.Type foundryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getFoundriesByGenusType(foundryGenusType));
    }


    /**
     *  Gets a <code>FoundryList</code> containing the given foundry
     *  record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known
     *  foundries or an error results. Otherwise, the returned list
     *  may contain only those foundries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getFoundries()</code>.
     *
     *  @param  foundryRecordType a foundry record type 
     *  @return the returned <code>Foundry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>foundryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryList getFoundriesByRecordType(org.osid.type.Type foundryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.foundry.FoundryRecordFilterList(getFoundries(), foundryRecordType));
    }


    /**
     *  Gets a <code>FoundryList</code> from the given provider.
     *  
     *  In plenary mode, the returned list contains all known
     *  foundries or an error results. Otherwise, the returned list
     *  may contain only those foundries that are accessible through
     *  this session.
     *
     *  @param resourceId a resource <code>Id</code>
     *  @return the returned <code>Foundry</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryList getFoundriesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.resourcing.foundry.FoundryProviderFilterList(getFoundries(), resourceId));
    }


    /**
     *  Gets all <code>Foundries</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  foundries or an error results. Otherwise, the returned list
     *  may contain only those foundries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Foundries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.resourcing.FoundryList getFoundries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the foundry list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of foundries
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.resourcing.FoundryList filterFoundriesOnViews(org.osid.resourcing.FoundryList list)
        throws org.osid.OperationFailedException {

        return (list);
    }
}
