//
// PersonMiter.java
//
//     Defines a Person miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.personnel.person;


/**
 *  Defines a <code>Person</code> miter for use with the builders.
 */

public interface PersonMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.personnel.Person {


    /**
     *  Sets the salutation.
     *
     *  @param salutation a salutation
     *  @throws org.osid.NullArgumentException <code>salutation</code>
     *          is <code>null</code>
     */

    public void setSalutation(org.osid.locale.DisplayText salutation);


    /**
     *  Sets the given name.
     *
     *  @param givenName a given name
     *  @throws org.osid.NullArgumentException <code>givenName</code>
     *          is <code>null</code>
     */

    public void setGivenName(org.osid.locale.DisplayText givenName);


    /**
     *  Sets the preferred name.
     *
     *  @param preferredName a preferred name
     *  @throws org.osid.NullArgumentException
     *          <code>preferredName</code> is <code>null</code>
     */

    public void setPreferredName(org.osid.locale.DisplayText preferredName);


    /**
     *  Adds a forename alias.
     *
     *  @param forenameAlias a forename alias
     *  @throws org.osid.NullArgumentException
     *          <code>forenameAlias</code> is <code>null</code>
     */

    public void addForenameAlias(org.osid.locale.DisplayText forenameAlias);


    /**
     *  Sets all the forename aliases.
     *
     *  @param forenameAliases a forename aliases
     *  @throws org.osid.NullArgumentException
     *          <code>forenameAliases</code> is <code>null</code>
     */

    public void setForenameAliases(java.util.Collection<org.osid.locale.DisplayText> forenameAliases);


    /**
     *  Adds a middle name.
     *
     *  @param middleName a middle name
     *  @throws org.osid.NullArgumentException <code>middleName</code>
     *          is <code>null</code>
     */

    public void addMiddleName(org.osid.locale.DisplayText middleName);


    /**
     *  Sets all the middle names.
     *
     *  @param middleNames a collection of middle names
     *  @throws org.osid.NullArgumentException
     *          <code>middleNames</code> is <code>null</code>
     */

    public void setMiddleNames(java.util.Collection<org.osid.locale.DisplayText> middleNames);


    /**
     *  Sets the surname.
     *
     *  @param surname a surname
     *  @throws org.osid.NullArgumentException <code>surname</code> is
     *          <code>null</code>
     */

    public void setSurname(org.osid.locale.DisplayText surname);


    /**
     *  Adds a surname alias.
     *
     *  @param surnameAlias a surname alias
     *  @throws org.osid.NullArgumentException
     *          <code>surnameAlias</code> is <code>null</code>
     */

    public void addSurnameAlias(org.osid.locale.DisplayText surnameAlias);


    /**
     *  Sets all the surname aliases.
     *
     *  @param surnameAliases a collection of surname aliases
     *  @throws org.osid.NullArgumentException
     *          <code>surnameAliases</code> is <code>null</code>
     */

    public void setSurnameAliases(java.util.Collection<org.osid.locale.DisplayText> surnameAliases);


    /**
     *  Sets the generation qualifier.
     *
     *  @param generationQualifier a generation qualifier
     *  @throws org.osid.NullArgumentException
     *          <code>generationQualifier</code> is <code>null</code>
     */

    public void setGenerationQualifier(org.osid.locale.DisplayText generationQualifier);


    /**
     *  Sets the qualification suffix.
     *
     *  @param qualificationSuffix a qualification suffix
     *  @throws org.osid.NullArgumentException
     *          <code>qualificationSuffix</code> is <code>null</code>
     */

    public void setQualificationSuffix(org.osid.locale.DisplayText qualificationSuffix);


    /**
     *  Sets the birth date.
     *
     *  @param date a birth date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setBirthDate(org.osid.calendaring.DateTime date);


    /**
     *  Sets the death date.
     *
     *  @param date a death date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setDeathDate(org.osid.calendaring.DateTime date);


    /**
     *  Sets the institutional identifier.
     *
     *  @param identifier an institutional identifier
     *  @throws org.osid.NullArgumentException <code>identifier</code>
     *          is <code>null</code>
     */

    public void setInstitutionalIdentifier(String identifier);


    /**
     *  Adds a Person record.
     *
     *  @param record a person record
     *  @param recordType the type of person record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addPersonRecord(org.osid.personnel.records.PersonRecord record, org.osid.type.Type recordType);
}       


