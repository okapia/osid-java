//
// AbstractTerm.java
//
//    An abstract class for an evaluating rule term.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.rules.evaluation.spi;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract class for an evaluating term.
 */

public abstract class AbstractTerm
    implements net.okapia.osid.jamocha.assembly.rules.evaluation.Term {

    private java.util.Collection<net.okapia.osid.jamocha.assembly.rules.evaluation.Term> children = new java.util.ArrayList<>();


    /**
     *  Evaluates a term.
     *
     *  @param condition 
     *  @return <code>true</code> for a positive result,
     *          <code>false</code> otherwise
     */

    @Override
    public boolean eval(org.osid.rules.Condition condition) {
        for (net.okapia.osid.jamocha.assembly.rules.evaluation.Term t : this.children) {
            if (!t.eval(condition)) {
                return (false);
            }
        }

        return (true);
    }


    /**
     *  Adds a term as a child to this term.
     *
     *  @param term
     *  @throws org.osid.NullArgumentException <code>term</code> 
     *          is <code>null</code>
     */

    public void addTerm(net.okapia.osid.jamocha.assembly.rules.evaluation.Term term) {
        nullarg(term, "term");
        this.children.add(term);
        return;
    }


    protected java.util.Collection<net.okapia.osid.jamocha.assembly.rules.evaluation.Term> getTerms() {
        return (this.children);
    }
}


