//
// MutableIndexedMapProxyCredentialEntryLookupSession
//
//    Implements a CredentialEntry lookup service backed by a collection of
//    credentialEntries indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.chronicle;


/**
 *  Implements a CredentialEntry lookup service backed by a collection of
 *  credentialEntries. The credential entries are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some credentialEntries may be compatible
 *  with more types than are indicated through these credentialEntry
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of credential entries can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyCredentialEntryLookupSession
    extends net.okapia.osid.jamocha.core.course.chronicle.spi.AbstractIndexedMapCredentialEntryLookupSession
    implements org.osid.course.chronicle.CredentialEntryLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyCredentialEntryLookupSession} with
     *  no credential entry.
     *
     *  @param courseCatalog the course catalog
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyCredentialEntryLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                       org.osid.proxy.Proxy proxy) {
        setCourseCatalog(courseCatalog);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyCredentialEntryLookupSession} with
     *  a single credential entry.
     *
     *  @param courseCatalog the course catalog
     *  @param  credentialEntry an credential entry
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code credentialEntry}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyCredentialEntryLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                       org.osid.course.chronicle.CredentialEntry credentialEntry, org.osid.proxy.Proxy proxy) {

        this(courseCatalog, proxy);
        putCredentialEntry(credentialEntry);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyCredentialEntryLookupSession} using
     *  an array of credential entries.
     *
     *  @param courseCatalog the course catalog
     *  @param  credentialEntries an array of credential entries
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code credentialEntries}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyCredentialEntryLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                       org.osid.course.chronicle.CredentialEntry[] credentialEntries, org.osid.proxy.Proxy proxy) {

        this(courseCatalog, proxy);
        putCredentialEntries(credentialEntries);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyCredentialEntryLookupSession} using
     *  a collection of credential entries.
     *
     *  @param courseCatalog the course catalog
     *  @param  credentialEntries a collection of credential entries
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code credentialEntries}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyCredentialEntryLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                       java.util.Collection<? extends org.osid.course.chronicle.CredentialEntry> credentialEntries,
                                                       org.osid.proxy.Proxy proxy) {
        this(courseCatalog, proxy);
        putCredentialEntries(credentialEntries);
        return;
    }

    
    /**
     *  Makes a {@code CredentialEntry} available in this session.
     *
     *  @param  credentialEntry a credential entry
     *  @throws org.osid.NullArgumentException {@code credentialEntry{@code 
     *          is {@code null}
     */

    @Override
    public void putCredentialEntry(org.osid.course.chronicle.CredentialEntry credentialEntry) {
        super.putCredentialEntry(credentialEntry);
        return;
    }


    /**
     *  Makes an array of credential entries available in this session.
     *
     *  @param  credentialEntries an array of credential entries
     *  @throws org.osid.NullArgumentException {@code credentialEntries{@code 
     *          is {@code null}
     */

    @Override
    public void putCredentialEntries(org.osid.course.chronicle.CredentialEntry[] credentialEntries) {
        super.putCredentialEntries(credentialEntries);
        return;
    }


    /**
     *  Makes collection of credential entries available in this session.
     *
     *  @param  credentialEntries a collection of credential entries
     *  @throws org.osid.NullArgumentException {@code credentialEntry{@code 
     *          is {@code null}
     */

    @Override
    public void putCredentialEntries(java.util.Collection<? extends org.osid.course.chronicle.CredentialEntry> credentialEntries) {
        super.putCredentialEntries(credentialEntries);
        return;
    }


    /**
     *  Removes a CredentialEntry from this session.
     *
     *  @param credentialEntryId the {@code Id} of the credential entry
     *  @throws org.osid.NullArgumentException {@code credentialEntryId{@code  is
     *          {@code null}
     */

    @Override
    public void removeCredentialEntry(org.osid.id.Id credentialEntryId) {
        super.removeCredentialEntry(credentialEntryId);
        return;
    }    
}
