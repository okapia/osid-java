//
// MutableMapProxyValueLookupSession
//
//    Implements a Value lookup service backed by a collection of
//    values that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.configuration;


/**
 *  Implements a Value lookup service backed by a collection of
 *  values. The values are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of values can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyValueLookupSession
    extends net.okapia.osid.jamocha.core.configuration.spi.AbstractMapValueLookupSession
    implements org.osid.configuration.ValueLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyValueLookupSession}
     *  with no values.
     *
     *  @param configuration the configuration
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code configuration} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyValueLookupSession(org.osid.configuration.Configuration configuration,
                                                  org.osid.proxy.Proxy proxy) {
        setConfiguration(configuration);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyValueLookupSession} with a
     *  single value.
     *
     *  @param configuration the configuration
     *  @param value a value
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code configuration},
     *          {@code value}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyValueLookupSession(org.osid.configuration.Configuration configuration,
                                                org.osid.configuration.Value value, org.osid.proxy.Proxy proxy) {
        this(configuration, proxy);
        putValue(value);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyValueLookupSession} using an
     *  array of values.
     *
     *  @param configuration the configuration
     *  @param values an array of values
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code configuration},
     *          {@code values}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyValueLookupSession(org.osid.configuration.Configuration configuration,
                                                org.osid.configuration.Value[] values, org.osid.proxy.Proxy proxy) {
        this(configuration, proxy);
        putValues(values);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyValueLookupSession} using a
     *  collection of values.
     *
     *  @param configuration the configuration
     *  @param values a collection of values
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code configuration},
     *          {@code values}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyValueLookupSession(org.osid.configuration.Configuration configuration,
                                                java.util.Collection<? extends org.osid.configuration.Value> values,
                                                org.osid.proxy.Proxy proxy) {
   
        this(configuration, proxy);
        setSessionProxy(proxy);
        putValues(values);
        return;
    }

    
    /**
     *  Makes a {@code Value} available in this session.
     *
     *  @param value an value
     *  @throws org.osid.NullArgumentException {@code value{@code 
     *          is {@code null}
     */

    @Override
    public void putValue(org.osid.configuration.Value value) {
        super.putValue(value);
        return;
    }


    /**
     *  Makes an array of values available in this session.
     *
     *  @param values an array of values
     *  @throws org.osid.NullArgumentException {@code values{@code 
     *          is {@code null}
     */

    @Override
    public void putValues(org.osid.configuration.Value[] values) {
        super.putValues(values);
        return;
    }


    /**
     *  Makes collection of values available in this session.
     *
     *  @param values
     *  @throws org.osid.NullArgumentException {@code value{@code 
     *          is {@code null}
     */

    @Override
    public void putValues(java.util.Collection<? extends org.osid.configuration.Value> values) {
        super.putValues(values);
        return;
    }


    /**
     *  Removes a Value from this session.
     *
     *  @param valueId the {@code Id} of the value
     *  @throws org.osid.NullArgumentException {@code valueId{@code  is
     *          {@code null}
     */

    @Override
    public void removeValue(org.osid.id.Id valueId) {
        super.removeValue(valueId);
        return;
    }    
}
