//
// AbstractQueryRenovationLookupSession.java
//
//    An inline adapter that maps a RenovationLookupSession to
//    a RenovationQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.room.construction.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a RenovationLookupSession to
 *  a RenovationQuerySession.
 */

public abstract class AbstractQueryRenovationLookupSession
    extends net.okapia.osid.jamocha.room.construction.spi.AbstractRenovationLookupSession
    implements org.osid.room.construction.RenovationLookupSession {

    private boolean effectiveonly = false;
    private final org.osid.room.construction.RenovationQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryRenovationLookupSession.
     *
     *  @param querySession the underlying renovation query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryRenovationLookupSession(org.osid.room.construction.RenovationQuerySession querySession) {
        nullarg(querySession, "renovation query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Campus</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Campus Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCampusId() {
        return (this.session.getCampusId());
    }


    /**
     *  Gets the <code>Campus</code> associated with this 
     *  session.
     *
     *  @return the <code>Campus</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.Campus getCampus()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCampus());
    }


    /**
     *  Tests if this user can perform <code>Renovation</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRenovations() {
        return (this.session.canSearchRenovations());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include renovations in campuses which are children
     *  of this campus in the campus hierarchy.
     */

    @OSID @Override
    public void useFederatedCampusView() {
        this.session.useFederatedCampusView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this campus only.
     */

    @OSID @Override
    public void useIsolatedCampusView() {
        this.session.useIsolatedCampusView();
        return;
    }
    

    /**
     *  Only renovations whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveRenovationView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All renovations of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveRenovationView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Renovation</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Renovation</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Renovation</code> and
     *  retained for compatibility.
     *
     *  In effective mode, renovations are returned that are currently
     *  effective.  In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  renovationId <code>Id</code> of the
     *          <code>Renovation</code>
     *  @return the renovation
     *  @throws org.osid.NotFoundException <code>renovationId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>renovationId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.Renovation getRenovation(org.osid.id.Id renovationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.construction.RenovationQuery query = getQuery();
        query.matchId(renovationId, true);
        org.osid.room.construction.RenovationList renovations = this.session.getRenovationsByQuery(query);
        if (renovations.hasNext()) {
            return (renovations.getNextRenovation());
        } 
        
        throw new org.osid.NotFoundException(renovationId + " not found");
    }


    /**
     *  Gets a <code>RenovationList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  renovations specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Renovations</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, renovations are returned that are currently effective.
     *  In any effective mode, effective renovations and those currently expired
     *  are returned.
     *
     *  @param  renovationIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Renovation</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>renovationIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByIds(org.osid.id.IdList renovationIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.construction.RenovationQuery query = getQuery();

        try (org.osid.id.IdList ids = renovationIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getRenovationsByQuery(query));
    }


    /**
     *  Gets a <code>RenovationList</code> corresponding to the given
     *  renovation genus <code>Type</code> which does not include
     *  renovations of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, renovations are returned that are currently effective.
     *  In any effective mode, effective renovations and those currently expired
     *  are returned.
     *
     *  @param  renovationGenusType a renovation genus type 
     *  @return the returned <code>Renovation</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>renovationGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByGenusType(org.osid.type.Type renovationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.construction.RenovationQuery query = getQuery();
        query.matchGenusType(renovationGenusType, true);
        return (this.session.getRenovationsByQuery(query));
    }


    /**
     *  Gets a <code>RenovationList</code> corresponding to the given
     *  renovation genus <code>Type</code> and include any additional
     *  renovations with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, renovations are returned that are currently
     *  effective.  In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  renovationGenusType a renovation genus type 
     *  @return the returned <code>Renovation</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>renovationGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByParentGenusType(org.osid.type.Type renovationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.construction.RenovationQuery query = getQuery();
        query.matchParentGenusType(renovationGenusType, true);
        return (this.session.getRenovationsByQuery(query));
    }


    /**
     *  Gets a <code>RenovationList</code> containing the given
     *  renovation record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, renovations are returned that are currently
     *  effective.  In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  renovationRecordType a renovation record type 
     *  @return the returned <code>Renovation</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>renovationRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByRecordType(org.osid.type.Type renovationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.construction.RenovationQuery query = getQuery();
        query.matchRecordType(renovationRecordType, true);
        return (this.session.getRenovationsByQuery(query));
    }


    /**
     *  Gets a <code>RenovationList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible
     *  through this session.
     *  
     *  In effective mode, renovations are returned that are currently
     *  effective.  In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Renovation</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsOnDate(org.osid.calendaring.DateTime from, 
                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.construction.RenovationQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getRenovationsByQuery(query));
    }
        
    
    /**
     *  Gets a list of all renovations with a genus type and effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known renovations
     *  or an error results. Otherwise, the returned list may contain
     *  only those renovations that are accessible through this session.
     *  
     *  In effective mode, renovations are returned that are currently
     *  effective.  In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  renovationGenusType a renovation genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code> Renovation </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code>
     *          renovationGenusType, from </code> or <code> to </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByGenusTypeOnDate(org.osid.type.Type renovationGenusType, 
                                                                                     org.osid.calendaring.DateTime from, 
                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.construction.RenovationQuery query = getQuery();
        query.matchGenusType(renovationGenusType, true);
        query.matchDate(from, to, true);
        return (this.session.getRenovationsByQuery(query));
    }


    /**
     *  Gets a <code> RenovationList </code> containing the given
     *  room.
     *  
     *  In plenary mode, the returned list contains all known renovations
     *  or an error results. Otherwise, the returned list may contain
     *  only those renovations that are accessible through this session.
     *  
     *  In effective mode, renovations are returned that are currently
     *  effective.  In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  roomId a room <code> Id </code> 
     *  @return the returned <code> Renovation </code> list 
     *  @throws org.osid.NullArgumentException <code> roomId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsForRoom(org.osid.id.Id roomId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.construction.RenovationQuery query = getQuery();
        query.matchRoomId(roomId, true);
        return (this.session.getRenovationsByQuery(query));
    }

    
    /**
     *  Gets a <code> RenovationList </code> containing the given
     *  room and genus type.
     *  
     *  In plenary mode, the returned list contains all known renovations
     *  or an error results. Otherwise, the returned list may contain
     *  only those renovations that are accessible through this session.
     *  
     *  In effective mode, renovations are returned that are currently
     *  effective.  In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  roomId a room <code> Id </code> 
     *  @param  renovationGenusType a renovation genus type 
     *  @return the returned <code> Renovation </code> list 
     *  @throws org.osid.NullArgumentException <code> roomId
     *          </code> or <code> renovationGenusType </code> is <code>
     *          null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByGenusTypeForRoom(org.osid.id.Id roomId, 
                                                                                      org.osid.type.Type renovationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.construction.RenovationQuery query = getQuery();
        query.matchRoomId(roomId, true);
        query.matchGenusType(renovationGenusType, true);
        return (this.session.getRenovationsByQuery(query));
    }


    /**
     *  Gets a list of all renovations for a room effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known renovations
     *  or an error results. Otherwise, the returned list may contain
     *  only those renovations that are accessible through this session.
     *  
     *  In effective mode, renovations are returned that are currently
     *  effective.  In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  roomId a room <code> Id </code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code> Renovation </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> roomId, from
     *          </code> or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsForRoomOnDate(org.osid.id.Id roomId, 
                                                                                 org.osid.calendaring.DateTime from, 
                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.construction.RenovationQuery query = getQuery();
        query.matchRoomId(roomId, true);
        query.matchDate(from, to, true);
        return (this.session.getRenovationsByQuery(query));
    }


    /**
     *  Gets a list of all renovations for a room with a genus type
     *  and effective during the entire given date range inclusive but
     *  not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known renovations
     *  or an error results. Otherwise, the returned list may contain
     *  only those renovations that are accessible through this session.
     *  
     *  In effective mode, renovations are returned that are currently
     *  effective.  In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  roomId a room <code> Id </code> 
     *  @param  renovationGenusType a renovation genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code> Renovation </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> roomId,
     *          renovationGenusType, from </code> or <code> to </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByGenusTypeForRoomOnDate(org.osid.id.Id roomId, 
                                                                                            org.osid.type.Type renovationGenusType, 
                                                                                            org.osid.calendaring.DateTime from, 
                                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.construction.RenovationQuery query = getQuery();
        query.matchRoomId(roomId, true);
        query.matchGenusType(renovationGenusType, true);
        query.matchDate(from, to, true);
        return (this.session.getRenovationsByQuery(query));
    }


    /**
     *  Gets a <code> RenovationList </code> containing the given
     *  floor.
     *  
     *  In plenary mode, the returned list contains all known renovations
     *  or an error results. Otherwise, the returned list may contain
     *  only those renovations that are accessible through this session.
     *  
     *  In effective mode, renovations are returned that are currently
     *  effective.  In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  floorId a floor <code> Id </code> 
     *  @return the returned <code> Renovation </code> list 
     *  @throws org.osid.NullArgumentException <code> floorId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsForFloor(org.osid.id.Id floorId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.construction.RenovationQuery query = getQuery();
        if (query.supportsRoomQuery()) {
            query.getRoomQuery().matchFloorId(floorId, true);            
            return (this.session.getRenovationsByQuery(query));
        }

        return (super.getRenovationsForFloor(floorId));
    }

    
    /**
     *  Gets a <code> RenovationList </code> containing the given
     *  floor and genus type.
     *  
     *  In plenary mode, the returned list contains all known renovations
     *  or an error results. Otherwise, the returned list may contain
     *  only those renovations that are accessible through this session.
     *  
     *  In effective mode, renovations are returned that are currently
     *  effective.  In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  floorId a floor <code> Id </code> 
     *  @param  renovationGenusType a renovation genus type 
     *  @return the returned <code> Renovation </code> list 
     *  @throws org.osid.NullArgumentException <code> floorId
     *          </code> or <code> renovationGenusType </code> is <code>
     *          null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByGenusTypeForFloor(org.osid.id.Id floorId, 
                                                                                       org.osid.type.Type renovationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.construction.RenovationQuery query = getQuery();
        if (query.supportsRoomQuery()) {
            query.getRoomQuery().matchFloorId(floorId, true);
            query.matchGenusType(renovationGenusType, true);
            return (this.session.getRenovationsByQuery(query));
        }

        return (super.getRenovationsByGenusTypeForFloor(floorId, renovationGenusType));
    }


    /**
     *  Gets a list of all renovations for a floor effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known renovations
     *  or an error results. Otherwise, the returned list may contain
     *  only those renovations that are accessible through this session.
     *  
     *  In effective mode, renovations are returned that are currently
     *  effective.  In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  floorId a floor <code> Id </code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code> Renovation </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> floorId, from
     *          </code> or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsForFloorOnDate(org.osid.id.Id floorId, 
                                                                                  org.osid.calendaring.DateTime from, 
                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.construction.RenovationQuery query = getQuery();
        if (query.supportsRoomQuery()) {
            query.getRoomQuery().matchFloorId(floorId, true);            
            query.matchDate(from, to, true);
            return (this.session.getRenovationsByQuery(query));
        }

        return (super.getRenovationsForFloorOnDate(floorId, from, to));
    }


    /**
     *  Gets a list of all renovations for a floor with a genus type
     *  and effective during the entire given date range inclusive but
     *  not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known renovations
     *  or an error results. Otherwise, the returned list may contain
     *  only those renovations that are accessible through this session.
     *  
     *  In effective mode, renovations are returned that are currently
     *  effective.  In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  floorId a floor <code> Id </code> 
     *  @param  renovationGenusType a renovation genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code> Renovation </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> floorId,
     *          renovationGenusType, from </code> or <code> to </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByGenusTypeForFloorOnDate(org.osid.id.Id floorId, 
                                                                                             org.osid.type.Type renovationGenusType, 
                                                                                             org.osid.calendaring.DateTime from, 
                                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.construction.RenovationQuery query = getQuery();
        if (query.supportsRoomQuery()) {
            query.getRoomQuery().matchFloorId(floorId, true);            
            query.matchGenusType(renovationGenusType, true);
            query.matchDate(from, to, true);
            return (this.session.getRenovationsByQuery(query));
        }

        return (super.getRenovationsByGenusTypeForFloorOnDate(floorId, renovationGenusType, from, to));
    }


    /**
     *  Gets a <code> RenovationList </code> containing the given
     *  building.
     *  
     *  In plenary mode, the returned list contains all known renovations
     *  or an error results. Otherwise, the returned list may contain
     *  only those renovations that are accessible through this session.
     *  
     *  In effective mode, renovations are returned that are currently
     *  effective.  In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  buildingId a building <code> Id </code> 
     *  @return the returned <code> Renovation </code> list 
     *  @throws org.osid.NullArgumentException <code> buildingId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsForBuilding(org.osid.id.Id buildingId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.construction.RenovationQuery query = getQuery();
        if (query.supportsRoomQuery()) {
            org.osid.room.RoomQuery roomQuery = query.getRoomQuery();
            if (roomQuery.supportsFloorQuery()) {
                roomQuery.getFloorQuery().matchBuildingId(buildingId, true);
                return (this.session.getRenovationsByQuery(query));
            }
        }

        return (super.getRenovationsForBuilding(buildingId));
    }

    
    /**
     *  Gets a <code> RenovationList </code> containing the given
     *  building and genus type.
     *  
     *  In plenary mode, the returned list contains all known renovations
     *  or an error results. Otherwise, the returned list may contain
     *  only those renovations that are accessible through this session.
     *  
     *  In effective mode, renovations are returned that are currently
     *  effective.  In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  buildingId a building <code> Id </code> 
     *  @param  renovationGenusType a renovation genus type 
     *  @return the returned <code> Renovation </code> list 
     *  @throws org.osid.NullArgumentException <code> buildingId
     *          </code> or <code> renovationGenusType </code> is <code>
     *          null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByGenusTypeForBuilding(org.osid.id.Id buildingId, 
                                                                                          org.osid.type.Type renovationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.construction.RenovationQuery query = getQuery();

        if (query.supportsRoomQuery()) {
            org.osid.room.RoomQuery roomQuery = query.getRoomQuery();
            if (roomQuery.supportsFloorQuery()) {
                roomQuery.getFloorQuery().matchBuildingId(buildingId, true);
                return (this.session.getRenovationsByQuery(query));
            }
        }

        query.matchGenusType(renovationGenusType, true);
        return (super.getRenovationsByGenusTypeForBuilding(buildingId, renovationGenusType));
    }


    /**
     *  Gets a list of all renovations for a building effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known renovations
     *  or an error results. Otherwise, the returned list may contain
     *  only those renovations that are accessible through this session.
     *  
     *  In effective mode, renovations are returned that are currently
     *  effective.  In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  buildingId a building <code> Id </code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code> Renovation </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> buildingId, from
     *          </code> or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsForBuildingOnDate(org.osid.id.Id buildingId, 
                                                                                     org.osid.calendaring.DateTime from, 
                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.construction.RenovationQuery query = getQuery();
        if (query.supportsRoomQuery()) {
            org.osid.room.RoomQuery roomQuery = query.getRoomQuery();
            if (roomQuery.supportsFloorQuery()) {
                roomQuery.getFloorQuery().matchBuildingId(buildingId, true);
                query.matchDate(from, to, true);
                return (this.session.getRenovationsByQuery(query));
            }
        }

        return (super.getRenovationsForBuildingOnDate(buildingId, from, to));
    }


    /**
     *  Gets a list of all renovations for a building with a genus type
     *  and effective during the entire given date range inclusive but
     *  not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known renovations
     *  or an error results. Otherwise, the returned list may contain
     *  only those renovations that are accessible through this session.
     *  
     *  In effective mode, renovations are returned that are currently
     *  effective.  In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  buildingId a building <code> Id </code> 
     *  @param  renovationGenusType a renovation genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code> Renovation </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> buildingId,
     *          renovationGenusType, from </code> or <code> to </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByGenusTypeForBuildingOnDate(org.osid.id.Id buildingId, 
                                                                                                org.osid.type.Type renovationGenusType, 
                                                                                                org.osid.calendaring.DateTime from, 
                                                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.construction.RenovationQuery query = getQuery();
        if (query.supportsRoomQuery()) {
            org.osid.room.RoomQuery roomQuery = query.getRoomQuery();
            if (roomQuery.supportsFloorQuery()) {
                roomQuery.getFloorQuery().matchBuildingId(buildingId, true);
                query.matchGenusType(renovationGenusType, true);
                query.matchDate(from, to, true);
                return (this.session.getRenovationsByQuery(query));
            }
        }

        return (super.getRenovationsByGenusTypeForBuildingOnDate(buildingId, 
                                                                 renovationGenusType, from, to));
    }

        
    /**
     *  Gets all <code>Renovations</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, renovations are returned that are currently
     *  effective.  In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Renovations</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.room.construction.RenovationQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getRenovationsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.room.construction.RenovationQuery getQuery() {
        org.osid.room.construction.RenovationQuery query = this.session.getRenovationQuery();
        
        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }

        return (query);
    }
}
