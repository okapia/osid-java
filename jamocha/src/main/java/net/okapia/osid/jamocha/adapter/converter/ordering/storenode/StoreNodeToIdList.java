//
// StoreNodeToIdList.java
//
//     Implements a StoreNode IdList. 
//
//
// Tom Coppeto
// Okapia
// 15 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.converter.ordering.storenode;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  Implements an IdList converting the underlying StoreNodeList to a
 *  list of Ids.
 */

public final class StoreNodeToIdList
    extends net.okapia.osid.jamocha.adapter.id.id.spi.AbstractAdapterIdList
    implements org.osid.id.IdList {

    private final org.osid.ordering.StoreNodeList storeNodeList;


    /**
     *  Creates a new {@code StoreNodeToIdList}.
     *
     *  @param storeNodeList a {@code StoreNodeList}
     *  @throws org.osid.NullArgumentException {@code storeNodeList}
     *          is {@code null}
     */

    public StoreNodeToIdList(org.osid.ordering.StoreNodeList storeNodeList) {
        super(storeNodeList);
        this.storeNodeList = storeNodeList;
        return;
    }


    /**
     *  Creates a new {@code StoreNodeToIdList}.
     *
     *  @param storeNodes a collection of StoreNodes
     *  @throws org.osid.NullArgumentException {@code storeNodes}
     *          is {@code null}
     */

    public StoreNodeToIdList(java.util.Collection<org.osid.ordering.StoreNode> storeNodes) {
        this(new net.okapia.osid.jamocha.ordering.storenode.ArrayStoreNodeList(storeNodes)); 
        return;
    }


    /**
     *  Gets the next {@code Id} in this list. 
     *
     *  @return the next {@code Id} in this list. The {@code
     *          hasNext()} method should be used to test that a next
     *          {@code Id} is available before calling this method.
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.id.Id getNextId()
        throws org.osid.OperationFailedException {

        return (this.storeNodeList.getNextStoreNode().getId());
    }


    /**
     *  Closes this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.storeNodeList.close();
        return;
    }
}
