//
// AbstractImmutableAssessmentRequirement.java
//
//     Wraps a mutable AssessmentRequirement to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.requisite.assessmentrequirement.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>AssessmentRequirement</code> to hide modifiers. This
 *  wrapper provides an immutized AssessmentRequirement from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying assessmentRequirement whose state changes are visible.
 */

public abstract class AbstractImmutableAssessmentRequirement
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRule
    implements org.osid.course.requisite.AssessmentRequirement {

    private final org.osid.course.requisite.AssessmentRequirement assessmentRequirement;


    /**
     *  Constructs a new <code>AbstractImmutableAssessmentRequirement</code>.
     *
     *  @param assessmentRequirement the assessment requirement to immutablize
     *  @throws org.osid.NullArgumentException <code>assessmentRequirement</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableAssessmentRequirement(org.osid.course.requisite.AssessmentRequirement assessmentRequirement) {
        super(assessmentRequirement);
        this.assessmentRequirement = assessmentRequirement;
        return;
    }


    /**
     *  Gets any <code> Requisites </code> that may be substituted in place of 
     *  this <code> AssessmentRequirement. </code> All <code> Requisites 
     *  </code> must be satisifed to be a substitute for this assessment 
     *  requirement. Inactive <code> Requisites </code> are not evaluated but 
     *  if no applicable requisite exists, then the alternate requisite is not 
     *  satisifed. 
     *
     *  @return the alternate requisites 
     */

    @OSID @Override
    public org.osid.course.requisite.Requisite[] getAltRequisites() {
        return (this.assessmentRequirement.getAltRequisites());
    }


    /**
     *  Gets the <code> Id </code> of the <code> Assessment. </code> 
     *
     *  @return the assessment <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAssessmentId() {
        return (this.assessmentRequirement.getAssessmentId());
    }


    /**
     *  Gets the <code> Assessment. </code> 
     *
     *  @return the assessment 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.assessment.Assessment getAssessment()
        throws org.osid.OperationFailedException {

        return (this.assessmentRequirement.getAssessment());
    }


    /**
     *  Tests if the assessment must be completed within the required 
     *  duration. 
     *
     *  @return <code> true </code> if the assessment has to be completed 
     *          within a required time, <code> false </code> if it could have 
     *          been completed at any time in the past 
     */

    @OSID @Override
    public boolean hasTimeframe() {
        return (this.assessmentRequirement.hasTimeframe());
    }


    /**
     *  Gets the timeframe in which the assessment has to be completed. A 
     *  negative duration indicates the assessment had to be completed within 
     *  the specified amount of time in the past. A posiitive duration 
     *  indicates the assessment must be completed within the specified amount 
     *  of time in the future. A zero duration indicates the assessment must 
     *  be completed in the current term. 
     *
     *  @return the time frame 
     *  @throws org.osid.IllegalStateException <code> hasTimeframe() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getTimeframe() {
        return (this.assessmentRequirement.getTimeframe());
    }


    /**
     *  Tests if a minimum grade above passing is required in the completion 
     *  of the assessment. 
     *
     *  @return <code> true </code> if a minimum grade is required, <code> 
     *          false </code> if the course just has to be passed 
     */

    @OSID @Override
    public boolean hasMinimumGrade() {
        return (this.assessmentRequirement.hasMinimumGrade());
    }


    /**
     *  Gets the minimum grade <code> Id. </code> 
     *
     *  @return the minimum grade <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasMinimumGrade() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getMinimumGradeId() {
        return (this.assessmentRequirement.getMinimumGradeId());
    }


    /**
     *  Gets the minimum grade. 
     *
     *  @return the minimum grade 
     *  @throws org.osid.IllegalStateException <code> hasMinimumGrade() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.Grade getMinimumGrade()
        throws org.osid.OperationFailedException {

        return (this.assessmentRequirement.getMinimumGrade());
    }


    /**
     *  Tests if a minimum score above passing is required in the completion 
     *  of the assessment. 
     *
     *  @return <code> true </code> if a minimum score is required, <code> 
     *          false </code> if the course just has to be passed 
     */

    @OSID @Override
    public boolean hasMinimumScore() {
        return (this.assessmentRequirement.hasMinimumScore());
    }


    /**
     *  Gets the scoring system <code> Id </code> for the minimum score. 
     *
     *  @return the scoring system <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasMinimumScore() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getMinimumScoreSystemId() {
        return (this.assessmentRequirement.getMinimumScoreSystemId());
    }


    /**
     *  Gets the scoring system for the minimum score. 
     *
     *  @return the scoring system 
     *  @throws org.osid.IllegalStateException <code> hasMinimumScore() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeSystem getMinimumScoreSystem()
        throws org.osid.OperationFailedException {

        return (this.assessmentRequirement.getMinimumScoreSystem());
    }


    /**
     *  Gets the minimum score. 
     *
     *  @return the minimum score 
     *  @throws org.osid.IllegalStateException <code> hasMinimumScore() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getMinimumScore() {
        return (this.assessmentRequirement.getMinimumScore());
    }


    /**
     *  Gets the assessment requirement record corresponding to the given 
     *  <code> AssessmentRequirement </code> record <code> Type. </code> This 
     *  method is used to retrieve an object implementing the requested 
     *  record. The <code> assessmentRequirementRecordType </code> may be the 
     *  <code> Type </code> returned in <code> getRecordTypes() </code> or any 
     *  of its parents in a <code> Type </code> hierarchy where <code> 
     *  hasRecordType(assessmentRequirementRecordType) </code> is <code> true 
     *  </code> . 
     *
     *  @param  assessmentRequirementRecordType the type of assessment 
     *          requirement record to retrieve 
     *  @return the assessment requirement record 
     *  @throws org.osid.NullArgumentException <code> 
     *          assessmentRequirementRecordType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(assessmentRequirementRecordType) </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.records.AssessmentRequirementRecord getAssessmentRequirementRecord(org.osid.type.Type assessmentRequirementRecordType)
        throws org.osid.OperationFailedException {

        return (this.assessmentRequirement.getAssessmentRequirementRecord(assessmentRequirementRecordType));
    }
}

