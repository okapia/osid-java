//
// AbstractIndexedMapPositionLookupSession.java
//
//    A simple framework for providing a Position lookup service
//    backed by a fixed collection of positions with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.personnel.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Position lookup service backed by a
 *  fixed collection of positions. The positions are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some positions may be compatible
 *  with more types than are indicated through these position
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Positions</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapPositionLookupSession
    extends AbstractMapPositionLookupSession
    implements org.osid.personnel.PositionLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.personnel.Position> positionsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.personnel.Position>());
    private final MultiMap<org.osid.type.Type, org.osid.personnel.Position> positionsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.personnel.Position>());


    /**
     *  Makes a <code>Position</code> available in this session.
     *
     *  @param  position a position
     *  @throws org.osid.NullArgumentException <code>position<code> is
     *          <code>null</code>
     */

    @Override
    protected void putPosition(org.osid.personnel.Position position) {
        super.putPosition(position);

        this.positionsByGenus.put(position.getGenusType(), position);
        
        try (org.osid.type.TypeList types = position.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.positionsByRecord.put(types.getNextType(), position);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a position from this session.
     *
     *  @param positionId the <code>Id</code> of the position
     *  @throws org.osid.NullArgumentException <code>positionId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removePosition(org.osid.id.Id positionId) {
        org.osid.personnel.Position position;
        try {
            position = getPosition(positionId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.positionsByGenus.remove(position.getGenusType());

        try (org.osid.type.TypeList types = position.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.positionsByRecord.remove(types.getNextType(), position);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removePosition(positionId);
        return;
    }


    /**
     *  Gets a <code>PositionList</code> corresponding to the given
     *  position genus <code>Type</code> which does not include
     *  positions of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known positions or an error results. Otherwise,
     *  the returned list may contain only those positions that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  positionGenusType a position genus type 
     *  @return the returned <code>Position</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>positionGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.PositionList getPositionsByGenusType(org.osid.type.Type positionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.personnel.position.ArrayPositionList(this.positionsByGenus.get(positionGenusType)));
    }


    /**
     *  Gets a <code>PositionList</code> containing the given
     *  position record <code>Type</code>. In plenary mode, the
     *  returned list contains all known positions or an error
     *  results. Otherwise, the returned list may contain only those
     *  positions that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  positionRecordType a position record type 
     *  @return the returned <code>position</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>positionRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.PositionList getPositionsByRecordType(org.osid.type.Type positionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.personnel.position.ArrayPositionList(this.positionsByRecord.get(positionRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.positionsByGenus.clear();
        this.positionsByRecord.clear();

        super.close();

        return;
    }
}
