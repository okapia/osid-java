//
// AbstractAdapterShipmentLookupSession.java
//
//    A Shipment lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.inventory.shipment.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Shipment lookup session adapter.
 */

public abstract class AbstractAdapterShipmentLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.inventory.shipment.ShipmentLookupSession {

    private final org.osid.inventory.shipment.ShipmentLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterShipmentLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterShipmentLookupSession(org.osid.inventory.shipment.ShipmentLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Warehouse/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Warehouse Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getWarehouseId() {
        return (this.session.getWarehouseId());
    }


    /**
     *  Gets the {@code Warehouse} associated with this session.
     *
     *  @return the {@code Warehouse} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Warehouse getWarehouse()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getWarehouse());
    }


    /**
     *  Tests if this user can perform {@code Shipment} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupShipments() {
        return (this.session.canLookupShipments());
    }


    /**
     *  A complete view of the {@code Shipment} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeShipmentView() {
        this.session.useComparativeShipmentView();
        return;
    }


    /**
     *  A complete view of the {@code Shipment} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryShipmentView() {
        this.session.usePlenaryShipmentView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include shipments in warehouses which are children
     *  of this warehouse in the warehouse hierarchy.
     */

    @OSID @Override
    public void useFederatedWarehouseView() {
        this.session.useFederatedWarehouseView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this warehouse only.
     */

    @OSID @Override
    public void useIsolatedWarehouseView() {
        this.session.useIsolatedWarehouseView();
        return;
    }
    
     
    /**
     *  Gets the {@code Shipment} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Shipment} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Shipment} and
     *  retained for compatibility.
     *
     *  @param shipmentId {@code Id} of the {@code Shipment}
     *  @return the shipment
     *  @throws org.osid.NotFoundException {@code shipmentId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code shipmentId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.shipment.Shipment getShipment(org.osid.id.Id shipmentId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getShipment(shipmentId));
    }


    /**
     *  Gets a {@code ShipmentList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  shipments specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Shipments} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  shipmentIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Shipment} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code shipmentIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentList getShipmentsByIds(org.osid.id.IdList shipmentIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getShipmentsByIds(shipmentIds));
    }


    /**
     *  Gets a {@code ShipmentList} corresponding to the given
     *  shipment genus {@code Type} which does not include
     *  shipments of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  shipments or an error results. Otherwise, the returned list
     *  may contain only those shipments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  shipmentGenusType a shipment genus type 
     *  @return the returned {@code Shipment} list
     *  @throws org.osid.NullArgumentException
     *          {@code shipmentGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentList getShipmentsByGenusType(org.osid.type.Type shipmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getShipmentsByGenusType(shipmentGenusType));
    }


    /**
     *  Gets a {@code ShipmentList} corresponding to the given
     *  shipment genus {@code Type} and include any additional
     *  shipments with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  shipments or an error results. Otherwise, the returned list
     *  may contain only those shipments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  shipmentGenusType a shipment genus type 
     *  @return the returned {@code Shipment} list
     *  @throws org.osid.NullArgumentException
     *          {@code shipmentGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentList getShipmentsByParentGenusType(org.osid.type.Type shipmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getShipmentsByParentGenusType(shipmentGenusType));
    }


    /**
     *  Gets a {@code ShipmentList} containing the given
     *  shipment record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  shipments or an error results. Otherwise, the returned list
     *  may contain only those shipments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  shipmentRecordType a shipment record type 
     *  @return the returned {@code Shipment} list
     *  @throws org.osid.NullArgumentException
     *          {@code shipmentRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentList getShipmentsByRecordType(org.osid.type.Type shipmentRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getShipmentsByRecordType(shipmentRecordType));
    }


    /**
     *  Gets a {@code ShipmentList} received between the given date
     *  range inclusive.
     *  
     *  In plenary mode, the returned list contains all known
     *  shipments or an error results. Otherwise, the returned list
     *  may contain only those shipments that are accessible through
     *  this session.
     *
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned {@code ShipmentList} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code from} or {@code
     *          to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentList getShipmentsOnDate(org.osid.calendaring.DateTime from, 
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getShipmentsOnDate(from, to));
    }


    /**
     *  Gets a {@code ShipmentList} for to the given stock. 
     *  
     *  In plenary mode, the returned list contains all known
     *  shipments or an error results. Otherwise, the returned list
     *  may contain only those shipments that are accessible through
     *  this session.
     *
     *  @param  stockId a stock {@code Id} 
     *  @return the returned {@code ShipmentList} list 
     *  @throws org.osid.NullArgumentException {@code stockId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentList getShipmentsForStock(org.osid.id.Id stockId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getShipmentsForStock(stockId));
    }


    /**
     *  Gets a {@code ShipmentList} for the given stock and received
     *  between the given date range inclusive.
     *  
     *  In plenary mode, the returned list contains all known
     *  shipments or an error results. Otherwise, the returned list
     *  may contain only those shipments that are accessible through
     *  this session.
     *
     *  @param  stockId a stock {@code Id} 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned {@code ShipmentList} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code stockId, from,} 
     *          or {@code to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentList getShipmentsForStockOnDate(org.osid.id.Id stockId, 
                                                                               org.osid.calendaring.DateTime from, 
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getShipmentsForStockOnDate(stockId, from, to));
    }


    /**
     *  Gets a {@code ShipmentList} from to the given source. 
     *  
     *  In plenary mode, the returned list contains all known
     *  shipments or an error results. Otherwise, the returned list
     *  may contain only those shipments that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code ShipmentList} list 
     *  @throws org.osid.NullArgumentException {@code resourceId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentList getShipmentsBySource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getShipmentsBySource(resourceId));
    }


    /**
     *  Gets a {@code ShipmentList} from the given source and received
     *  between the given date range inclusive.
     *  
     *  In plenary mode, the returned list contains all known
     *  shipments or an error results. Otherwise, the returned list
     *  may contain only those shipments that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned {@code ShipmentList} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code resourceId,
     *         from}, or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentList getShipmentsBySourceOnDate(org.osid.id.Id resourceId, 
                                                                               org.osid.calendaring.DateTime from, 
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getShipmentsBySourceOnDate(resourceId, from, to));
    }

    
    /**
     *  Gets all {@code Shipments}. 
     *
     *  In plenary mode, the returned list contains all known
     *  shipments or an error results. Otherwise, the returned list
     *  may contain only those shipments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Shipments} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentList getShipments()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getShipments());
    }
}
