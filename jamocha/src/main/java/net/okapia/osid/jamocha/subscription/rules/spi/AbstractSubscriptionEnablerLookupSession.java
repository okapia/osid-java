//
// AbstractSubscriptionEnablerLookupSession.java
//
//    A starter implementation framework for providing a SubscriptionEnabler
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.subscription.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a SubscriptionEnabler
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getSubscriptionEnablers(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractSubscriptionEnablerLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.subscription.rules.SubscriptionEnablerLookupSession {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean federated     = false;
    private org.osid.subscription.Publisher publisher = new net.okapia.osid.jamocha.nil.subscription.publisher.UnknownPublisher();
    

    /**
     *  Gets the <code>Publisher/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Publisher Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getPublisherId() {
        return (this.publisher.getId());
    }


    /**
     *  Gets the <code>Publisher</code> associated with this 
     *  session.
     *
     *  @return the <code>Publisher</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.Publisher getPublisher()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.publisher);
    }


    /**
     *  Sets the <code>Publisher</code>.
     *
     *  @param  publisher the publisher for this session
     *  @throws org.osid.NullArgumentException <code>publisher</code>
     *          is <code>null</code>
     */

    protected void setPublisher(org.osid.subscription.Publisher publisher) {
        nullarg(publisher, "publisher");
        this.publisher = publisher;
        return;
    }


    /**
     *  Tests if this user can perform <code>SubscriptionEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupSubscriptionEnablers() {
        return (true);
    }


    /**
     *  A complete view of the <code>SubscriptionEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeSubscriptionEnablerView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>SubscriptionEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenarySubscriptionEnablerView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include subscription enablers in publishers which are children
     *  of this publisher in the publisher hierarchy.
     */

    @OSID @Override
    public void useFederatedPublisherView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this publisher only.
     */

    @OSID @Override
    public void useIsolatedPublisherView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active subscription enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveSubscriptionEnablerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive subscription enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusSubscriptionEnablerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>SubscriptionEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>SubscriptionEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>SubscriptionEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, subscription enablers are returned that are currently
     *  active. In any status mode, active and inactive subscription enablers
     *  are returned.
     *
     *  @param  subscriptionEnablerId <code>Id</code> of the
     *          <code>SubscriptionEnabler</code>
     *  @return the subscription enabler
     *  @throws org.osid.NotFoundException <code>subscriptionEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>subscriptionEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnabler getSubscriptionEnabler(org.osid.id.Id subscriptionEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.subscription.rules.SubscriptionEnablerList subscriptionEnablers = getSubscriptionEnablers()) {
            while (subscriptionEnablers.hasNext()) {
                org.osid.subscription.rules.SubscriptionEnabler subscriptionEnabler = subscriptionEnablers.getNextSubscriptionEnabler();
                if (subscriptionEnabler.getId().equals(subscriptionEnablerId)) {
                    return (subscriptionEnabler);
                }
            }
        } 

        throw new org.osid.NotFoundException(subscriptionEnablerId + " not found");
    }


    /**
     *  Gets a <code>SubscriptionEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  subscriptionEnablers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>SubscriptionEnablers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, subscription enablers are returned that are currently
     *  active. In any status mode, active and inactive subscription enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getSubscriptionEnablers()</code>.
     *
     *  @param  subscriptionEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>SubscriptionEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
*               found
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnablerList getSubscriptionEnablersByIds(org.osid.id.IdList subscriptionEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.subscription.rules.SubscriptionEnabler> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = subscriptionEnablerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getSubscriptionEnabler(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("subscription enabler " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.subscription.rules.subscriptionenabler.LinkedSubscriptionEnablerList(ret));
    }


    /**
     *  Gets a <code>SubscriptionEnablerList</code> corresponding to the given
     *  subscription enabler genus <code>Type</code> which does not include
     *  subscription enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  subscription enablers or an error results. Otherwise, the returned list
     *  may contain only those subscription enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, subscription enablers are returned that are currently
     *  active. In any status mode, active and inactive subscription enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getSubscriptionEnablers()</code>.
     *
     *  @param  subscriptionEnablerGenusType a subscriptionEnabler genus type 
     *  @return the returned <code>SubscriptionEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnablerList getSubscriptionEnablersByGenusType(org.osid.type.Type subscriptionEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.subscription.rules.subscriptionenabler.SubscriptionEnablerGenusFilterList(getSubscriptionEnablers(), subscriptionEnablerGenusType));
    }


    /**
     *  Gets a <code>SubscriptionEnablerList</code> corresponding to the given
     *  subscription enabler genus <code>Type</code> and include any additional
     *  subscription enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  subscription enablers or an error results. Otherwise, the returned list
     *  may contain only those subscription enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, subscription enablers are returned that are currently
     *  active. In any status mode, active and inactive subscription enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getSubscriptionEnablers()</code>.
     *
     *  @param  subscriptionEnablerGenusType a subscriptionEnabler genus type 
     *  @return the returned <code>SubscriptionEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnablerList getSubscriptionEnablersByParentGenusType(org.osid.type.Type subscriptionEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getSubscriptionEnablersByGenusType(subscriptionEnablerGenusType));
    }


    /**
     *  Gets a <code>SubscriptionEnablerList</code> containing the given
     *  subscription enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  subscription enablers or an error results. Otherwise, the returned list
     *  may contain only those subscription enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *

     *  In active mode, subscription enablers are returned that are currently
     *  active. In any status mode, active and inactive subscription enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getSubscriptionEnablers()</code>.
     *
     *  @param  subscriptionEnablerRecordType a subscriptionEnabler record type 
     *  @return the returned <code>SubscriptionEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnablerList getSubscriptionEnablersByRecordType(org.osid.type.Type subscriptionEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.subscription.rules.subscriptionenabler.SubscriptionEnablerRecordFilterList(getSubscriptionEnablers(), subscriptionEnablerRecordType));
    }


    /**
     *  Gets a <code>SubscriptionEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  subscription enablers or an error results. Otherwise, the returned list
     *  may contain only those subscription enablers that are accessible
     *  through this session.
     *  
     *  In active mode, subscription enablers are returned that are currently
     *  active in addition to being effective during the given date
     *  range. In any status mode, active and inactive subscription enablers are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>SubscriptionEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnablerList getSubscriptionEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.subscription.rules.subscriptionenabler.TemporalSubscriptionEnablerFilterList(getSubscriptionEnablers(), from, to));
    }
        

    /**
     *  Gets a <code>SubscriptionEnablerList </code> which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  subscription enablers or an error results. Otherwise, the returned list
     *  may contain only those subscription enablers that are accessible
     *  through this session.
     *
     *  In active mode, subscription enablers are returned that are currently
     *  active in addition to being effective during the date
     *  range. In any status mode, active and inactive subscription enablers are
     *  returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>SubscriptionEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnablerList getSubscriptionEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (getSubscriptionEnablersOnDate(from, to));
    }


    /**
     *  Gets all <code>SubscriptionEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  subscription enablers or an error results. Otherwise, the returned list
     *  may contain only those subscription enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, subscription enablers are returned that are currently
     *  active. In any status mode, active and inactive subscription enablers
     *  are returned.
     *
     *  @return a list of <code>SubscriptionEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.subscription.rules.SubscriptionEnablerList getSubscriptionEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the subscription enabler list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of subscription enablers
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.subscription.rules.SubscriptionEnablerList filterSubscriptionEnablersOnViews(org.osid.subscription.rules.SubscriptionEnablerList list)
        throws org.osid.OperationFailedException {

        org.osid.subscription.rules.SubscriptionEnablerList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.subscription.rules.subscriptionenabler.ActiveSubscriptionEnablerFilterList(ret);
        }

        return (ret);
    }
}
