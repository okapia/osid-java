//
// AbstractFilingManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.filing.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractFilingManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.filing.FilingManager,
               org.osid.filing.FilingProxyManager {

    private final Types fileRecordTypes                    = new TypeRefSet();
    private final Types fileSearchRecordTypes              = new TypeRefSet();

    private final Types directoryRecordTypes               = new TypeRefSet();
    private final Types directorySearchRecordTypes         = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractFilingManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractFilingManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if directory federation is exposed. Federation is exposed when a 
     *  specific directory may be identified, selected and used to access a 
     *  session. Federation is not exposed when a set of directories appears 
     *  as a single directory. 
     *
     *  @return <code> true </code> if federation is visible <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if a <code> FileSystemSession </code> is supported. 
     *
     *  @return <code> true </code> if a <code> FileSystemSession </code> is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFileSystem() {
        return (false);
    }


    /**
     *  Tests if a <code> FileSystemManagementSession </code> is supported. 
     *
     *  @return <code> true </code> if a <code> FileSystemManagementSession 
     *          </code> is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFileSystemManagement() {
        return (false);
    }


    /**
     *  Tests if a <code> FileContentSession </code> is supported. 
     *
     *  @return <code> true </code> if a <code> FileContentSession </code> is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFileContent() {
        return (false);
    }


    /**
     *  Tests if file lookup is supported. 
     *
     *  @return <code> true </code> if a <code> FileLookupSession </code> is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFileLookup() {
        return (false);
    }


    /**
     *  Tests if file querying is supported. 
     *
     *  @return <code> true </code> if a <code> FileQuerySession </code> is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFileQuery() {
        return (false);
    }


    /**
     *  Tests if file searching is supported. 
     *
     *  @return <code> true </code> if a <code> FileSearchSession </code> is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFileSearch() {
        return (false);
    }


    /**
     *  Tests if file notification is supported. 
     *
     *  @return <code> true </code> if a <code> FileNotificationSession 
     *          </code> is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFileNotification() {
        return (false);
    }


    /**
     *  Tests if managing smart directories is supported. 
     *
     *  @return <code> true </code> if a <code> FileSmartDirectorySession 
     *          </code> is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFileSmartDirectory() {
        return (false);
    }


    /**
     *  Tests if a <code> DirectoryLookupSession </code> is supported. 
     *
     *  @return <code> true </code> if a <code> DirectoryLookupSession </code> 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDirectoryLookup() {
        return (false);
    }


    /**
     *  Tests if directory querying is supported. 
     *
     *  @return <code> true </code> if a <code> DirectoryQuerySession </code> 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDirectoryQuery() {
        return (false);
    }


    /**
     *  Tests if directory searching is supported. 
     *
     *  @return <code> true </code> if a <code> DirectorySearchSession </code> 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDirectorySearch() {
        return (false);
    }


    /**
     *  Tests if directory administration is supported. 
     *
     *  @return <code> true </code> if a <code> DirectoryAdminSession </code> 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDirectoryAdmin() {
        return (false);
    }


    /**
     *  Tests if a directory <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if a <code> DirectoryNotificationSession 
     *          </code> is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDirectoryNotification() {
        return (false);
    }


    /**
     *  Tests if a file management service is supported. 
     *
     *  @return <code> true </code> if a <code> FileManagementSession </code> 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFilingManagement() {
        return (false);
    }


    /**
     *  Tests if a filing allocation service is supported. 
     *
     *  @return <code> true </code> if a <code> FilingAllocationManager 
     *          </code> is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFilingAllocation() {
        return (false);
    }


    /**
     *  Gets the supported file record types. 
     *
     *  @return a list containing the supported <code> File </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getFileRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.fileRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given file record type is supported. 
     *
     *  @param  fileRecordType a <code> Type </code> indicating a file record 
     *          type 
     *  @return <code> true </code> if the given record <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> fileRecordType </code> 
     *          is null 
     */

    @OSID @Override
    public boolean supportsFileRecordType(org.osid.type.Type fileRecordType) {
        return (this.fileRecordTypes.contains(fileRecordType));
    }


    /**
     *  Adds support for a file record type.
     *
     *  @param fileRecordType a file record type
     *  @throws org.osid.NullArgumentException
     *  <code>fileRecordType</code> is <code>null</code>
     */

    protected void addFileRecordType(org.osid.type.Type fileRecordType) {
        this.fileRecordTypes.add(fileRecordType);
        return;
    }


    /**
     *  Removes support for a file record type.
     *
     *  @param fileRecordType a file record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>fileRecordType</code> is <code>null</code>
     */

    protected void removeFileRecordType(org.osid.type.Type fileRecordType) {
        this.fileRecordTypes.remove(fileRecordType);
        return;
    }


    /**
     *  Gets the supported file search record types. 
     *
     *  @return a list containing the supported <code> File </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getFileSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.fileSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given file search record type is supported. 
     *
     *  @param  fileSearchRecordType a <code> Type </code> indicating a file 
     *          search record type 
     *  @return <code> true </code> if the given search record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> fileSearchRecordType 
     *          </code> is null 
     */

    @OSID @Override
    public boolean supportsFileSearchRecordType(org.osid.type.Type fileSearchRecordType) {
        return (this.fileSearchRecordTypes.contains(fileSearchRecordType));
    }


    /**
     *  Adds support for a file search record type.
     *
     *  @param fileSearchRecordType a file search record type
     *  @throws org.osid.NullArgumentException
     *  <code>fileSearchRecordType</code> is <code>null</code>
     */

    protected void addFileSearchRecordType(org.osid.type.Type fileSearchRecordType) {
        this.fileSearchRecordTypes.add(fileSearchRecordType);
        return;
    }


    /**
     *  Removes support for a file search record type.
     *
     *  @param fileSearchRecordType a file search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>fileSearchRecordType</code> is <code>null</code>
     */

    protected void removeFileSearchRecordType(org.osid.type.Type fileSearchRecordType) {
        this.fileSearchRecordTypes.remove(fileSearchRecordType);
        return;
    }


    /**
     *  Gets the supported directory record types. 
     *
     *  @return a list containing the supported <code> Directory </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDirectoryRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.directoryRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given directory record type is supported. 
     *
     *  @param  directoryRecordType a <code> Type </code> indicating a 
     *          directory record type 
     *  @return <code> true </code> if the given record <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> directoryRecordType 
     *          </code> is null 
     */

    @OSID @Override
    public boolean supportsDirectoryRecordType(org.osid.type.Type directoryRecordType) {
        return (this.directoryRecordTypes.contains(directoryRecordType));
    }


    /**
     *  Adds support for a directory record type.
     *
     *  @param directoryRecordType a directory record type
     *  @throws org.osid.NullArgumentException
     *  <code>directoryRecordType</code> is <code>null</code>
     */

    protected void addDirectoryRecordType(org.osid.type.Type directoryRecordType) {
        this.directoryRecordTypes.add(directoryRecordType);
        return;
    }


    /**
     *  Removes support for a directory record type.
     *
     *  @param directoryRecordType a directory record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>directoryRecordType</code> is <code>null</code>
     */

    protected void removeDirectoryRecordType(org.osid.type.Type directoryRecordType) {
        this.directoryRecordTypes.remove(directoryRecordType);
        return;
    }


    /**
     *  Gets the supported directory search record types. 
     *
     *  @return a list containing the supported <code> Directory </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDirectorySearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.directorySearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given directory search record type is supported. 
     *
     *  @param  directorySearchRecordType a <code> Type </code> indicating a 
     *          directory search record type 
     *  @return <code> true </code> if the given search record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          directorySearchRecordType </code> is null 
     */

    @OSID @Override
    public boolean supportsDirectorySearchRecordType(org.osid.type.Type directorySearchRecordType) {
        return (this.directorySearchRecordTypes.contains(directorySearchRecordType));
    }


    /**
     *  Adds support for a directory search record type.
     *
     *  @param directorySearchRecordType a directory search record type
     *  @throws org.osid.NullArgumentException
     *  <code>directorySearchRecordType</code> is <code>null</code>
     */

    protected void addDirectorySearchRecordType(org.osid.type.Type directorySearchRecordType) {
        this.directorySearchRecordTypes.add(directorySearchRecordType);
        return;
    }


    /**
     *  Removes support for a directory search record type.
     *
     *  @param directorySearchRecordType a directory search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>directorySearchRecordType</code> is <code>null</code>
     */

    protected void removeDirectorySearchRecordType(org.osid.type.Type directorySearchRecordType) {
        this.directorySearchRecordTypes.remove(directorySearchRecordType);
        return;
    }


    /**
     *  Gets the session for examining file systems. 
     *
     *  @return a <code> FileSystemSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileSystem() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileSystemSession getFileSystemSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingManager.getFileSystemSession not implemented");
    }


    /**
     *  Gets the session for examining file systems. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FileSystemSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileSystem() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileSystemSession getFileSystemSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingProxyManager.getFileSystemSession not implemented");
    }


    /**
     *  Gets the session for exmaning file systems for the given path. 
     *
     *  @param  path a path 
     *  @return a <code> FileSystemSession </code> 
     *  @throws org.osid.NotFoundException <code> path </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> path </code> is 
     *          null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileSystem() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileSystemSession getFileSystemSessionForPath(String path)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingManager.getFileSystemSessionForPath not implemented");
    }



    /**
     *  Gets the session for exmaning file systems for the given directory. 
     *
     *  @param  directoryId the <code> Id </code> of a directory 
     *  @return a <code> FileSystemSession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryId </code> is 
     *          null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileSystem() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileSystemSession getFileSystemSessionForDirectory(org.osid.id.Id directoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingManager.getFileSystemSessionForDirectory not implemented");
    }


    /**
     *  Gets the session for exmaning file systems for the given path. 
     *
     *  @param  path a path
     *  @param  proxy a proxy 
     *  @return a <code> FileSystemSession </code> 
     *  @throws org.osid.NotFoundException <code> path </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> path </code> or 
     *          <code> proxy </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileSystem() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileSystemSession getFileSystemSessionForPath(String path, 
                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingProxyManager.getFileSystemSessionForPath not implemented");
    }


    /**
     *  Gets the session for exmaning file systems for the given directory. 
     *
     *  @param  directoryId the <code> Id </code> of a directory 
     *  @param  proxy a proxy 
     *  @return a <code> FileSystemSession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryId </code> or 
     *          <code> proxy </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileSystem() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileSystemSession getFileSystemSessionForDirectory(org.osid.id.Id directoryId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingProxyManager.getFileSystemSessionForDirectory not implemented");
    }


    /**
     *  Gets the session for manipulating file systems. 
     *
     *  @return a <code> FileSystemManagementSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFileSystemManagement() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileSystemManagementSession getFileSystemManagementSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingManager.getFileSystemManagementSession not implemented");
    }


    /**
     *  Gets the session for manipulating file systems. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FileSystemManagementSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFileSystemManagement() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileSystemManagementSession getFileSystemManagementSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingProxyManager.getFileSystemManagementSession not implemented");
    }


    /**
     *  Gets the session for manipulating files for the given path. 
     *
     *  @param  path a path 
     *  @return a <code> FileSystemManagementSession </code> 
     *  @throws org.osid.NotFoundException <code> path </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> path </code> is 
     *          null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFileSystemManagement() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileSystemManagementSession getFileSystemManagementSessionForPath(String path)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingManager.getFileSystemManagementSessionForPath not implemented");
    }


    /**
     *  Gets the session for manipulating files for the given path. 
     *
     *  @param  directoryId the <code> Id </code> of a directory 
     *  @return a <code> FileSystemManagementSession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryId </code> is 
     *          null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFileSystemManagement() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileSystemManagementSession getFileSystemManagementSessionForDirectory(org.osid.id.Id directoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingManager.getFileSystemManagementSessionForDirectory not implemented");
    }


    /**
     *  Gets the session for manipulating files for the given path. 
     *
     *  @param  path a path 
     *  @param  proxy a proxy 
     *  @return a <code> FileSystemManagementSession </code> 
     *  @throws org.osid.NotFoundException <code> path </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> path </code> or 
     *          <code> proxy </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFileSystemManagement() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileSystemManagementSession getFileSystemManagementSessionForPath(String path, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingProxyManager.getFileSystemManagementSessionForPath not implemented");
    }


    /**
     *  Gets the session for manipulating files for the given directory. 
     *
     *  @param  directoryId the <code> Id </code> of a directory 
     *  @param  proxy a proxy 
     *  @return a <code> FileSystemManagementSession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryId </code> or 
     *          <code> proxy </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFileSystemManagement() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileSystemManagementSession getFileSystemManagementSessionForDirectory(org.osid.id.Id directoryId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingProxyManager.getFileSystemManagementSessionForDirectory not implemented");
    }


    /**
     *  Gets the session for reading and writing files. 
     *
     *  @return a <code> FileContentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileContent() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileContentSession getFileContentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingManager.getFileContentSession not implemented");
    }


    /**
     *  Gets the session for reading and writing files. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FileContentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileContent() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileContentSession getFileContentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingProxyManager.getFileContentSession not implemented");
    }


    /**
     *  Gets the session for reading and writing files for the given path. 
     *
     *  @param  path a path
     *  @return a <code> FileContentSession </code> 
     *  @throws org.osid.NotFoundException <code> path </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> path </code> is 
     *          null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileContent() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileContentSession getFileContentSessionForPath(String path)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingManager.getFileContentSessionForPath not implemented");
    }


    /**
     *  Gets the session for reading and writing files for the given path. 
     *
     *  @param  directoryId the <code> Id </code> of a directory 
     *  @return a <code> FileContentSession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryId </code> is 
     *          null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileContent() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileContentSession getFileContentSessionForDirectory(org.osid.id.Id directoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingManager.getFileContentSessionForDirectory not implemented");
    }


    /**
     *  Gets the session for reading and writing files for the given
     *  path.
     *
     *  @param  path a path
     *  @param  proxy a proxy 
     *  @return a <code> FileContentSession </code> 
     *  @throws org.osid.NotFoundException <code> path </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> path </code> or 
     *          <code> proxy </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileContent() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileContentSession getFileContentSessionForPath(String path, 
                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingProxyManager.getFileContentSessionForPath not implemented");
    }


    /**
     *  Gets the session for reading and writing files for the given path. 
     *
     *  @param  directoryId the <code> Id </code> of a directory 
     *  @param  proxy a proxy 
     *  @return a <code> FileContentSession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryId </code> or 
     *          <code> proxy </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileContent() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileContentSession getFileContentSessionForDirectory(org.osid.id.Id directoryId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingProxyManager.getFileContentSessionForDirectory not implemented");
    }


    /**
     *  Gets the session for looking up files. 
     *
     *  @return the <code> FileLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileLookupSession getFileLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingManager.getFileLookupSession not implemented");
    }


    /**
     *  Gets the session for looking up files. 
     *
     *  @param  proxy a proxy 
     *  @return the <code> FileLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileLookupSession getFileLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingProxyManager.getFileLookupSession not implemented");
    }


    /**
     *  Gets the session for manipulating files for the given path. 
     *
     *  @param  directoryId the <code> Id </code> of the directory 
     *  @return a <code> FileLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryId </code> is 
     *          null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileLookupSession getFileLookupSessionForDirectory(org.osid.id.Id directoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingManager.getFileLookupSessionForDirectory not implemented");
    }


    /**
     *  Gets a file lookup session for the specified directory. 
     *
     *  @param  directoryId the <code> Id </code> to the directory 
     *  @param  proxy a proxy 
     *  @return a <code> FileLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryId </code> or 
     *          <code> proxy </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileLookupSession getFileLookupSessionForDirectory(org.osid.id.Id directoryId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingProxyManager.getFileLookupSessionForDirectory not implemented");
    }


    /**
     *  Gets the session for querying files. 
     *
     *  @return the <code> FileQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileQuerySession getFileQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingManager.getFileQuerySession not implemented");
    }


    /**
     *  Gets the session for querying files. 
     *
     *  @param  proxy a proxy 
     *  @return the <code> FileQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileQuerySession getFileQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingProxyManager.getFileQuerySession not implemented");
    }


    /**
     *  Gets a file query session for the specified directory. 
     *
     *  @param  directoryId the <code> Id </code> of the directory 
     *  @return a <code> FileQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryId </code> is 
     *          null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileQuerySession getFileQuerySessionForDirectory(org.osid.id.Id directoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingManager.getFileQuerySessionForDirectory not implemented");
    }


    /**
     *  Gets a file query session for the specified directory. 
     *
     *  @param  directoryId the <code> Id </code> to the directory 
     *  @param  proxy a proxy 
     *  @return a <code> FileQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryId </code> or 
     *          <code> proxy </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileQuerySession getFileQuerySessionForDirectory(org.osid.id.Id directoryId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingProxyManager.getFileQuerySessionForDirectory not implemented");
    }


    /**
     *  Gets the session for searching for files. 
     *
     *  @return the <code> FileSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileSearchSession getFileSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingManager.getFileSearchSession not implemented");
    }


    /**
     *  Gets the session for searching for files. 
     *
     *  @param  proxy a proxy 
     *  @return the <code> FileSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileSearchSession getFileSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingProxyManager.getFileSearchSession not implemented");
    }


    /**
     *  Gets a file search session for the specified directory. 
     *
     *  @param  directoryId the <code> Id </code> of the directory 
     *  @return a <code> FileSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryId </code> is 
     *          null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileSearchSession getFileSearchSessionForDirectory(org.osid.id.Id directoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingManager.getFileSearchSessionForDirectory not implemented");
    }


    /**
     *  Gets a file search session for the specified directory. 
     *
     *  @param  directoryId the <code> Id </code> to the directory 
     *  @param  proxy a proxy 
     *  @return a <code> FileSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryId </code> or 
     *          <code> proxy </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileSearchSession getFileSearchSessionForDirectory(org.osid.id.Id directoryId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingProxyManager.getFileSearchSessionForDirectory not implemented");
    }


    /**
     *  Gets the session for receiving messages about changes to files. 
     *
     *  @param  fileReceiver the notification callback 
     *  @return <code> a FileNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> fileReceiver </code> is 
     *          null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFileNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileNotificationSession getFileNotificationSession(org.osid.filing.FileReceiver fileReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingManager.getFileNotificationSession not implemented");
    }


    /**
     *  Gets the session for receiving messages about changes to files. 
     *
     *  @param  fileReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return <code> a FileNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> fileReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFileNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileNotificationSession getFileNotificationSession(org.osid.filing.FileReceiver fileReceiver, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingProxyManager.getFileNotificationSession not implemented");
    }


    /**
     *  Gets a file notification session for the specified directory. 
     *
     *  @param  fileReceiver the notification callback 
     *  @param  directoryId the <code> Id </code> of the directory 
     *  @return a <code> FileNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> fileReceiver </code> or 
     *          <code> directoryId </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFileNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileNotificationSession getFileNotificationSessionForDirectory(org.osid.filing.FileReceiver fileReceiver, 
                                                                                          org.osid.id.Id directoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingManager.getFileNotificationSessionForDirectory not implemented");
    }


    /**
     *  Gets a file notification session for the specified directory. 
     *
     *  @param  fileReceiver the notification callback 
     *  @param  directoryId the <code> Id </code> to the directory 
     *  @param  proxy a proxy 
     *  @return a <code> FileNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> directoryPath </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> fileReceiver, 
     *          directoryId, </code> or <code> proxy </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFileNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileNotificationSession getFileNotificationSessionForDirectory(org.osid.filing.FileReceiver fileReceiver, 
                                                                                          org.osid.id.Id directoryId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingProxyManager.getFileNotificationSessionForDirectory not implemented");
    }


    /**
     *  Gets the session for managing dynamic directories. 
     *
     *  @param  directoryId the <code> Id </code> of the directory 
     *  @return a <code> FileSmartDirectorySession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryId </code> is 
     *          null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFileSmartDirectory() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileSmartDirectorySession getFileSmartDirectorySession(org.osid.id.Id directoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingManager.getFileSmartDirectorySession not implemented");
    }


    /**
     *  Gets the session for managing dynamic diectories. 
     *
     *  @param  directoryId the <code> Id </code> to the directory 
     *  @param  proxy a proxy 
     *  @return a <code> FileSmartDirectorySession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId is not found 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> directoryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFileSmartDirectory() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileSmartDirectorySession getFileSmartDirectorySession(org.osid.id.Id directoryId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingProxyManager.getFileSmartDirectorySession not implemented");
    }


    /**
     *  Gets the session for examining directories. 
     *
     *  @return a <code> DirectoryLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectoryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectoryLookupSession getDirectoryLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingManager.getDirectoryLookupSession not implemented");
    }


    /**
     *  Gets the session for examining directories. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DirectoryLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectoryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectoryLookupSession getDirectoryLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingProxyManager.getDirectoryLookupSession not implemented");
    }


    /**
     *  Gets the session for examining a given directory. 
     *
     *  @param  directoryId the <code> Id </code> of the directory 
     *  @return a <code> DirectoryLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryId </code> is 
     *          null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectoryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectoryLookupSession getDirectoryLookupSessionForDirectory(org.osid.id.Id directoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingManager.getDirectoryLookupSessionForDirectory not implemented");
    }


    /**
     *  Gets the session for examining a given directory. If the path is an 
     *  alias, the target directory is used. The path indicates the file alias 
     *  and the real path indicates the target directory. 
     *
     *  @param  directoryId the <code> Id </code> to the directory 
     *  @param  proxy a proxy 
     *  @return a <code> DirectoryLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId is not found 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> directoryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectoryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectoryLookupSession getDirectoryLookupSessionForDirectory(org.osid.id.Id directoryId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingProxyManager.getDirectoryLookupSessionForDirectory not implemented");
    }


    /**
     *  Gets the session for querying directories. 
     *
     *  @return a <code> DirectoryQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectoryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectoryQuerySession getDirectoryQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingManager.getDirectoryQuerySession not implemented");
    }


    /**
     *  Gets the session for querying directories. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DirectoryQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectoryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectoryQuerySession getDirectoryQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingProxyManager.getDirectoryQuerySession not implemented");
    }


    /**
     *  Gets the session for querying directories within a given directory. 
     *
     *  @param  directoryId the <code> Id </code> of the directory 
     *  @return a <code> DirectoryQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryId </code> is 
     *          null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectoryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectoryQuerySession getDirectoryQuerySessionForDirectory(org.osid.id.Id directoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingManager.getDirectoryQuerySessionForDirectory not implemented");
    }


    /**
     *  Gets the session for querying directories within a given directory. If 
     *  the path is an alias, the target directory is used. The path indicates 
     *  the file alias and the real path indicates the target directory. 
     *
     *  @param  directoryId the <code> Id </code> to the directory 
     *  @param  proxy a proxy 
     *  @return a <code> DirectoryQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId is not found 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> directoryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectoryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectoryQuerySession getDirectoryQuerySessionForDirectory(org.osid.id.Id directoryId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingProxyManager.getDirectoryQuerySessionForDirectory not implemented");
    }


    /**
     *  Gets the session for searching for directories. 
     *
     *  @return a <code> DirectorySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectorySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectorySearchSession getDirectorySearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingManager.getDirectorySearchSession not implemented");
    }


    /**
     *  Gets the session for searching for directories. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DirectorySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectorySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectorySearchSession getDirectorySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingProxyManager.getDirectorySearchSession not implemented");
    }


    /**
     *  Gets the session for searching for directories within a given 
     *  directory. 
     *
     *  @param  directoryId the <code> Id </code> of the directory 
     *  @return a <code> DirectorySearchSession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryId </code> is 
     *          null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectorySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectorySearchSession getDirectorySearchSessionForDirectory(org.osid.id.Id directoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingManager.getDirectorySearchSessionForDirectory not implemented");
    }


    /**
     *  Gets the session for searching for directories within a given 
     *  directory. If the path is an alias, the target directory is used. The 
     *  path indicates the file alias and the real path indicates the target 
     *  directory. 
     *
     *  @param  directoryId the <code> Id </code> to the directory 
     *  @param  proxy a proxy 
     *  @return a <code> DirectorySearchSession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId is not found 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> directoryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectorySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectorySearchSession getDirectorySearchSessionForDirectory(org.osid.id.Id directoryId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingProxyManager.getDirectorySearchSessionForDirectory not implemented");
    }


    /**
     *  Gets the session for creating and removing files. 
     *
     *  @return a <code> DirectoryAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectoryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectoryAdminSession getDirectoryAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingManager.getDirectoryAdminSession not implemented");
    }


    /**
     *  Gets the session for creating and removing files. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DirectoryAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectoryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectoryAdminSession getDirectoryAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingProxyManager.getDirectoryAdminSession not implemented");
    }


    /**
     *  Gets the session for searching for creating and removing files in the 
     *  given directory. 
     *
     *  @param  directoryId the <code> Id </code> of the directory 
     *  @return a <code> DirectoryAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryId </code> is 
     *          null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectoryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectoryAdminSession getDirectoryAdminSessionForDirectory(org.osid.id.Id directoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingManager.getDirectoryAdminSessionForDirectory not implemented");
    }


    /**
     *  Gets the session for searching for creating and removing files in the 
     *  given directory. If the path is an alias, the target directory is 
     *  used. The path indicates the file alias and the real path indicates 
     *  the target directory. 
     *
     *  @param  directoryId the <code> Id </code> to the directory 
     *  @param  proxy a proxy 
     *  @return a <code> DirectoryAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId is not found 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> directoryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectoryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectoryAdminSession getDirectoryAdminSessionForDirectory(org.osid.id.Id directoryId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingProxyManager.getDirectoryAdminSessionForDirectory not implemented");
    }


    /**
     *  Gets the session for receiving messages about changes to directories. 
     *
     *  @param  directoryReceiver the notification callback 
     *  @return a <code> DirectoryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> directoryReceiver 
     *          </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectoryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectoryNotificationSession getDirectoryNotificationSession(org.osid.filing.DirectoryReceiver directoryReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingManager.getDirectoryNotificationSession not implemented");
    }


    /**
     *  Gets the session for receiving messages about changes to directories. 
     *
     *  @param  directoryReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> DirectoryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> directoryReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectoryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectoryNotificationSession getDirectoryNotificationSession(org.osid.filing.DirectoryReceiver directoryReceiver, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingProxyManager.getDirectoryNotificationSession not implemented");
    }


    /**
     *  Gets the session for receiving messages about changes to directories 
     *  in the given directory. 
     *
     *  @param  directoryReceiver the notification callback 
     *  @param  directoryId the <code> Id </code> of the directory 
     *  @return a <code> DirectoryNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryReceiver 
     *          </code> or <code> directoryId </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectoryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectoryNotificationSession getDirectoryNotificationSessionForDirectory(org.osid.filing.DirectoryReceiver directoryReceiver, 
                                                                                                    org.osid.id.Id directoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingManager.getDirectoryNotificationSessionForDirectory not implemented");
    }


    /**
     *  Gets the session for receiving messages about changes to directories 
     *  in the given directory. If the path is an alias, the target directory 
     *  is used. The path indicates the file alias and the real path indicates 
     *  the target directory. 
     *
     *  @param  directoryReceiver the notification callback 
     *  @param  directoryId the <code> Id </code> to the directory 
     *  @param  proxy a proxy 
     *  @return a <code> DirectoryNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> directoryPath </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryReceiver, 
     *          directoryId, </code> or <code> proxy </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectoryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectoryNotificationSession getDirectoryNotificationSessionForDirectory(org.osid.filing.DirectoryReceiver directoryReceiver, 
                                                                                                    org.osid.id.Id directoryId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingProxyManager.getDirectoryNotificationSessionForDirectory not implemented");
    }


    /**
     *  Gets the <code> FilingAllocationManager. </code> 
     *
     *  @return a <code> FilingAllocationManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFilingAllocation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.allocation.FilingAllocationManager getFilingAllocationManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingManager.getFilingAllocationManager not implemented");
    }


    /**
     *  Gets the <code> FilingAllocationProxyManager. </code> 
     *
     *  @return a <code> FilingAllocationProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFilingAllocation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.allocation.FilingAllocationProxyManager getFilingAllocationProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.FilingProxyManager.getFilingAllocationProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.fileRecordTypes.clear();
        this.fileRecordTypes.clear();

        this.fileSearchRecordTypes.clear();
        this.fileSearchRecordTypes.clear();

        this.directoryRecordTypes.clear();
        this.directoryRecordTypes.clear();

        this.directorySearchRecordTypes.clear();
        this.directorySearchRecordTypes.clear();

        return;
    }
}
