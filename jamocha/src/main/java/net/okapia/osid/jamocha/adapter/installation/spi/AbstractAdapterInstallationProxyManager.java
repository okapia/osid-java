//
// AbstractInstallationProxyManager.java
//
//     An adapter for a InstallationProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.installation.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a InstallationProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterInstallationProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.installation.InstallationProxyManager>
    implements org.osid.installation.InstallationProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterInstallationProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterInstallationProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterInstallationProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterInstallationProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if an installation lookup service is supported. 
     *
     *  @return true if installation lookup is supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsInstallationLookup() {
        return (getAdapteeManager().supportsInstallationLookup());
    }


    /**
     *  Tests if an installation query service is supported. 
     *
     *  @return true if installation query is supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsInstallationQuery() {
        return (getAdapteeManager().supportsInstallationQuery());
    }


    /**
     *  Tests if an installation search service is supported. 
     *
     *  @return <code> true </code> if installation search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstallationSearch() {
        return (getAdapteeManager().supportsInstallationSearch());
    }


    /**
     *  Tests if an installation management service is supported. 
     *
     *  @return <code> true </code> if package management is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstallationManagement() {
        return (getAdapteeManager().supportsInstallationManagement());
    }


    /**
     *  Tests if an installation update service is supported. 
     *
     *  @return <code> true </code> if package update is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstallationUpdate() {
        return (getAdapteeManager().supportsInstallationUpdate());
    }


    /**
     *  Tests if installation notification is supported. Messages may be sent 
     *  when installations are installed or removed. 
     *
     *  @return <code> true </code> if installation notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstallationNotification() {
        return (getAdapteeManager().supportsInstallationNotification());
    }


    /**
     *  Tests if a site lookup service is supported. 
     *
     *  @return true if site lookup is supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsSiteLookup() {
        return (getAdapteeManager().supportsSiteLookup());
    }


    /**
     *  Tests if a package lookup service is supported. A package lookup 
     *  service defines methods to access packages. 
     *
     *  @return true if package lookup is supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsPackageLookup() {
        return (getAdapteeManager().supportsPackageLookup());
    }


    /**
     *  Tests if querying packages is supported. 
     *
     *  @return <code> true </code> if packages query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPackageQuery() {
        return (getAdapteeManager().supportsPackageQuery());
    }


    /**
     *  Tests if a package search service is supported. 
     *
     *  @return <code> true </code> if package search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPackageSearch() {
        return (getAdapteeManager().supportsPackageSearch());
    }


    /**
     *  Tests if a package administrative service is supported. 
     *
     *  @return <code> true </code> if package admin is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPackageAdmin() {
        return (getAdapteeManager().supportsPackageAdmin());
    }


    /**
     *  Tests if package notification is supported. Messages may be sent when 
     *  packages are created, modified, or deleted. 
     *
     *  @return <code> true </code> if package notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPackageNotification() {
        return (getAdapteeManager().supportsPackageNotification());
    }


    /**
     *  Tests if a package to depot lookup session is available. 
     *
     *  @return <code> true </code> if package depot lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPackageDepot() {
        return (getAdapteeManager().supportsPackageDepot());
    }


    /**
     *  Tests if a package to depot assignment session is available. 
     *
     *  @return <code> true </code> if package depot assignment is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPackageDepotAssignment() {
        return (getAdapteeManager().supportsPackageDepotAssignment());
    }


    /**
     *  Tests if package smart depots are available. 
     *
     *  @return <code> true </code> if package smart depots are supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPackageSmartDepot() {
        return (getAdapteeManager().supportsPackageSmartDepot());
    }


    /**
     *  Tests if a depot lookup service is supported. 
     *
     *  @return <code> true </code> if depot lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDepotLookup() {
        return (getAdapteeManager().supportsDepotLookup());
    }


    /**
     *  Tests if a depot query service is supported. 
     *
     *  @return <code> true </code> if depot query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDepotQuery() {
        return (getAdapteeManager().supportsDepotQuery());
    }


    /**
     *  Tests if a depot search service is supported. 
     *
     *  @return <code> true </code> if depot search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDepotSearch() {
        return (getAdapteeManager().supportsDepotSearch());
    }


    /**
     *  Tests if a depot administrative service is supported. 
     *
     *  @return <code> true </code> if depot admin is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDepotAdmin() {
        return (getAdapteeManager().supportsDepotAdmin());
    }


    /**
     *  Tests if depot notification is supported. Messages may be sent when 
     *  depots are created, modified, or deleted. 
     *
     *  @return <code> true </code> if depot notification is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDepotNotification() {
        return (getAdapteeManager().supportsDepotNotification());
    }


    /**
     *  Tests if a depot hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if a depot hierarchy traversal is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDepotHierarchy() {
        return (getAdapteeManager().supportsDepotHierarchy());
    }


    /**
     *  Tests if depot hierarchy design is supported. 
     *
     *  @return <code> true </code> if a depot hierarchy design is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDepotHierarchyDesign() {
        return (getAdapteeManager().supportsDepotHierarchyDesign());
    }


    /**
     *  Tests if an installation batch service is supported. 
     *
     *  @return <code> true </code> if an installation batch service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstallationBatch() {
        return (getAdapteeManager().supportsInstallationBatch());
    }


    /**
     *  Gets the supported <code> Installation </code> record types. 
     *
     *  @return a list containing the supported <code> Installation </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getInstallationRecordTypes() {
        return (getAdapteeManager().getInstallationRecordTypes());
    }


    /**
     *  Tests if the given <code> Installation </code> record type is 
     *  supported. 
     *
     *  @param  installationRecordType a <code> Type </code> indicating an 
     *          <code> Installation </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> installationRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsInstallationRecordType(org.osid.type.Type installationRecordType) {
        return (getAdapteeManager().supportsInstallationRecordType(installationRecordType));
    }


    /**
     *  Gets the supported <code> Installation </code> search record types. 
     *
     *  @return a list containing the supported <code> Installation </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getInstallationSearchRecordTypes() {
        return (getAdapteeManager().getInstallationSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Installation </code> search record type is 
     *  supported. 
     *
     *  @param  installationSearchRecordType a <code> Type </code> indicating 
     *          an <code> Installation </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          installationSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsInstallationSearchRecordType(org.osid.type.Type installationSearchRecordType) {
        return (getAdapteeManager().supportsInstallationSearchRecordType(installationSearchRecordType));
    }


    /**
     *  Gets the supported <code> Site </code> record types. 
     *
     *  @return a list containing the supported <code> Site </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSiteRecordTypes() {
        return (getAdapteeManager().getSiteRecordTypes());
    }


    /**
     *  Tests if the given <code> Site </code> record type is supported. 
     *
     *  @param  siteRecordType a <code> Type </code> indicating a <code> Site 
     *          </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> siteRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSiteRecordType(org.osid.type.Type siteRecordType) {
        return (getAdapteeManager().supportsSiteRecordType(siteRecordType));
    }


    /**
     *  Gets the supported <code> Package </code> record types. 
     *
     *  @return a list containing the supported <code> Package </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPackageRecordTypes() {
        return (getAdapteeManager().getPackageRecordTypes());
    }


    /**
     *  Tests if the given <code> Package </code> record type is supported. 
     *
     *  @param  packageRecordType a <code> Type </code> indicating a <code> 
     *          Package </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> packageRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPackageRecordType(org.osid.type.Type packageRecordType) {
        return (getAdapteeManager().supportsPackageRecordType(packageRecordType));
    }


    /**
     *  Gets the supported <code> Package </code> search record types. 
     *
     *  @return a list containing the supported <code> Package </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPackageSearchRecordTypes() {
        return (getAdapteeManager().getPackageSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Package </code> search record type is 
     *  supported. 
     *
     *  @param  packageSearchRecordType a <code> Type </code> indicating a 
     *          <code> Package </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> packageSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPackageSearchRecordType(org.osid.type.Type packageSearchRecordType) {
        return (getAdapteeManager().supportsPackageSearchRecordType(packageSearchRecordType));
    }


    /**
     *  Gets the supported <code> InstallationContent </code> record types. 
     *
     *  @return a list containing the supported <code> InstallationContent 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getInstallationContentRecordTypes() {
        return (getAdapteeManager().getInstallationContentRecordTypes());
    }


    /**
     *  Tests if the given <code> InstallationContent </code> record type is 
     *  supported. 
     *
     *  @param  installationContentRecordType a <code> Type </code> indicating 
     *          an <code> InstallationContent </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          installationContentRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsInstallationContentRecordType(org.osid.type.Type installationContentRecordType) {
        return (getAdapteeManager().supportsInstallationContentRecordType(installationContentRecordType));
    }


    /**
     *  Gets the supported <code> Depot </code> record types. 
     *
     *  @return a list containing the supported <code> Depot </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDepotRecordTypes() {
        return (getAdapteeManager().getDepotRecordTypes());
    }


    /**
     *  Tests if the given <code> Depot </code> record type is supported. 
     *
     *  @param  depotRecordType a <code> Type </code> indicating a <code> 
     *          Depot </code> type 
     *  @return <code> true </code> if the given depot record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> depotRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDepotRecordType(org.osid.type.Type depotRecordType) {
        return (getAdapteeManager().supportsDepotRecordType(depotRecordType));
    }


    /**
     *  Gets the supported depot search record types. 
     *
     *  @return a list containing the supported <code> Depot </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDepotSearchRecordTypes() {
        return (getAdapteeManager().getDepotSearchRecordTypes());
    }


    /**
     *  Tests if the given depot search record type is supported. 
     *
     *  @param  depotSearchRecordType a <code> Type </code> indicating a 
     *          <code> Depot </code> search record type 
     *  @return <code> true </code> if the given search record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> depotSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDepotSearchRecordType(org.osid.type.Type depotSearchRecordType) {
        return (getAdapteeManager().supportsDepotSearchRecordType(depotSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the installation 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InstallationLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationLookupSession getInstallationLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInstallationLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the installation 
     *  lookup service for the given site. 
     *
     *  @param  siteId the <code> Id </code> of the site 
     *  @param  proxy a proxy 
     *  @return <code> an InstallationLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> siteId </code> not found 
     *  @throws org.osid.NullArgumentException <code> siteId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationLookupSession getInstallationLookupSessionForSite(org.osid.id.Id siteId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInstallationLookupSessionForSite(siteId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the installation 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InstallationQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationQuerySession[] getInstallationQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInstallationQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the installation 
     *  query service for the given site. 
     *
     *  @param  siteId the <code> Id </code> of the site 
     *  @param  proxy a proxy 
     *  @return <code> an InstallationQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> siteId </code> not found 
     *  @throws org.osid.NullArgumentException <code> siteId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationQuerySession[] getInstallationQuerySessionForSite(org.osid.id.Id siteId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInstallationQuerySessionForSite(siteId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the installation 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InstallationSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationSearchSession getInstallationSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInstallationSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the installation 
     *  search service for the given site. 
     *
     *  @param  siteId the <code> Id </code> of the site 
     *  @param  proxy a proxy 
     *  @return <code> an InstallationSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> siteId </code> not found 
     *  @throws org.osid.NullArgumentException <code> siteId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationSearchSession getInstallationSearchSessionForSite(org.osid.id.Id siteId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInstallationSearchSessionForSite(siteId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the installation 
     *  management service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InstallationAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationManagement() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationManagementSession getInstallationManagementSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInstallationManagementSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the installation 
     *  management service for the given site. 
     *
     *  @param  siteId the <code> Id </code> of the site 
     *  @param  proxy a proxy 
     *  @return <code> an InstallationAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> siteId </code> not found 
     *  @throws org.osid.NullArgumentException <code> siteId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationManagement() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationManagementSession getInstallationManagementSessionForSite(org.osid.id.Id siteId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInstallationManagementSessionForSite(siteId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the installation 
     *  update service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InstallationAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationUpdate() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationUpdateSession getInstallationUpdateSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInstallationUpdateSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the installation 
     *  update service for the given site. 
     *
     *  @param  siteId the <code> Id </code> of the site 
     *  @param  proxy a proxy 
     *  @return <code> an InstallationUpdateSession </code> 
     *  @throws org.osid.NotFoundException <code> siteId </code> not found 
     *  @throws org.osid.NullArgumentException <code> siteId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationUpdate() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationUpdateSession getInstallationUpdateSessionForSite(org.osid.id.Id siteId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInstallationUpdateSessionForSite(siteId, proxy));
    }


    /**
     *  Gets the notification session for notifications pertaining to 
     *  installation changes. 
     *
     *  @param  installationReceiver the installation receiver 
     *  @param  proxy a proxy 
     *  @return an <code> InstallationNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> installationReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationNotificationSession getInstallationNotificationSession(org.osid.installation.InstallationReceiver installationReceiver, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInstallationNotificationSession(installationReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the installation 
     *  notification service for the given site. 
     *
     *  @param  installationReceiver the installation receiver 
     *  @param  siteId the <code> Id </code> of the site 
     *  @param  proxy a proxy 
     *  @return <code> an InstallationNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> siteId </code> not found 
     *  @throws org.osid.NullArgumentException <code> installationReceiver, 
     *          siteId </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationNotificationSession getInstallationNotificationSessionForSite(org.osid.installation.InstallationReceiver installationReceiver, 
                                                                                                           org.osid.id.Id siteId, 
                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInstallationNotificationSessionForSite(installationReceiver, siteId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the site lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SiteLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSiteLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.SiteLookupSession getSiteLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSiteLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the package lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PackageLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPackageLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageLookupSession getPackageLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPackageLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the package lookup 
     *  service for the given depot. 
     *
     *  @param  depotId the <code> Id </code> of the depot 
     *  @param  proxy a proxy 
     *  @return <code> a PackageLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> depotId </code> not found 
     *  @throws org.osid.NullArgumentException <code> depotId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsPackageLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageLookupSession getPackageLookupSessionForDepot(org.osid.id.Id depotId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPackageLookupSessionForDepot(depotId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the package query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PackageQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPackageQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageQuerySession getPackageQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPackageQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the package query 
     *  service for the given depot. 
     *
     *  @param  depotId the <code> Id </code> of the <code> Depot </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PackageQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Depot </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> depotId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPackageQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageQuerySession getPackageQuerySessionForDepot(org.osid.id.Id depotId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPackageQuerySessionForDepot(depotId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the package search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PackageSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPackageSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageSearchSession getPackageSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPackageSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the package search 
     *  service for the given depot. 
     *
     *  @param  depotId the <code> Id </code> of the depot 
     *  @param  proxy a proxy 
     *  @return <code> a PackageSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> depotId </code> not found 
     *  @throws org.osid.NullArgumentException <code> depotId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsPackageSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageSearchSession getPackageSearchSessionForDepot(org.osid.id.Id depotId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPackageSearchSessionForDepot(depotId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the package 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PackageAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPackageAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageAdminSession getPackageAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPackageAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the package admin 
     *  service for the given depot. 
     *
     *  @param  depotId the <code> Id </code> of the depot 
     *  @param  proxy a proxy 
     *  @return <code> a PackageAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> depotId </code> not found 
     *  @throws org.osid.NullArgumentException <code> depotId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsPackageAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageAdminSession getPackageAdminSessionForDepot(org.osid.id.Id depotId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPackageAdminSessionForDepot(depotId, proxy));
    }


    /**
     *  Gets the notification session for notifications pertaining to package 
     *  changes. 
     *
     *  @param  packageReceiver the package receiver 
     *  @param  proxy a proxy 
     *  @return a <code> PackageNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> packageReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPackageNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageNotificationSession getPackageNotificationSession(org.osid.installation.PackageReceiver packageReceiver, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPackageNotificationSession(packageReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the package 
     *  notification service for the given depot. 
     *
     *  @param  packageReceiver the package receiver 
     *  @param  depotId the <code> Id </code> of the depot 
     *  @param  proxy a proxy 
     *  @return <code> a PackageNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> depotId </code> not found 
     *  @throws org.osid.NullArgumentException <code> packageReceiver, depotId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPackageNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageNotificationSession getPackageNotificationSessionForDepot(org.osid.installation.PackageReceiver packageReceiver, 
                                                                                                  org.osid.id.Id depotId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPackageNotificationSessionForDepot(packageReceiver, depotId, proxy));
    }


    /**
     *  Gets the session for retrieving package to depot mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PackageDepotSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPackageDepot() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageDepotSession getPackageDepotSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPackageDepotSession(proxy));
    }


    /**
     *  Gets the session for assigning package to depot mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PackageDepotAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPackageDepotAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageDepotSession getPackageDepotAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPackageDepotAssignmentSession(proxy));
    }


    /**
     *  Gets the session for managing dynamic package depot. 
     *
     *  @param  depotId the <code> Id </code> of the depot 
     *  @param  proxy a proxy 
     *  @return a <code> PackageSmartDepotSession </code> 
     *  @throws org.osid.NotFoundException <code> depotId </code> not found 
     *  @throws org.osid.NullArgumentException <code> depotId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPackageSmartDepot() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageSmartDepotSession getPackageSmartDepotSession(org.osid.id.Id depotId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPackageSmartDepotSession(depotId, proxy));
    }


    /**
     *  Gets the OsidSession associated with the depot lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DepotLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDepotLookup() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.installation.DepotLookupSession getDepotLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDepotLookupSession(proxy));
    }


    /**
     *  Gets the depot query session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DepotQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDepotQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.DepotQuerySession getDepotQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDepotQuerySession(proxy));
    }


    /**
     *  Gets the OsidSession associated with the depot search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DepotSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDepotSearch() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.installation.DepotSearchSession getDepotSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDepotSearchSession(proxy));
    }


    /**
     *  Gets the OsidSession associated with the depot administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DepotAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDepotAdmin() is 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.installation.DepotAdminSession getDepotAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDepotAdminSession(proxy));
    }


    /**
     *  Gets the notification session for notifications pertaining to depot 
     *  service changes. 
     *
     *  @param  depotReceiver the depot receiver 
     *  @param  proxy a proxy 
     *  @return a <code> DepotNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> depotReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDepotNotification() is false </code> 
     */

    @OSID @Override
    public org.osid.installation.DepotNotificationSession getDepotNotificationSession(org.osid.installation.DepotReceiver depotReceiver, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDepotNotificationSession(depotReceiver, proxy));
    }


    /**
     *  Gets the session traversing depot hierarchies. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DepotHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDepotHierarchy() is false </code> 
     */

    @OSID @Override
    public org.osid.installation.DepotHierarchySession getDepotHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDepotHierarchySession(proxy));
    }


    /**
     *  Gets the session designing depot hierarchies. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DepotHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDepotHierarchyDesign() is false </code> 
     */

    @OSID @Override
    public org.osid.installation.DepotHierarchyDesignSession getDepotHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDepotHierarchyDesignSession(proxy));
    }


    /**
     *  Gets an <code> InstallationBatchProxyManager. </code> 
     *
     *  @return an <code> InstallationBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationBatch() is false </code> 
     */

    @OSID @Override
    public org.osid.installation.batch.InstallationBatchProxyManager getInstallationBatchProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInstallationBatchProxyManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
