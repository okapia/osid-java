//
// AbstractPrice.java
//
//     Defines a Price builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.ordering.price.spi;


/**
 *  Defines a <code>Price</code> builder.
 */

public abstract class AbstractPriceBuilder<T extends AbstractPriceBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRuleBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.ordering.price.PriceMiter price;


    /**
     *  Constructs a new <code>AbstractPriceBuilder</code>.
     *
     *  @param price the price to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractPriceBuilder(net.okapia.osid.jamocha.builder.ordering.price.PriceMiter price) {
        super(price);
        this.price = price;
        return;
    }


    /**
     *  Builds the price.
     *
     *  @return the new price
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.ordering.Price build() {
        (new net.okapia.osid.jamocha.builder.validator.ordering.price.PriceValidator(getValidations())).validate(this.price);
        return (new net.okapia.osid.jamocha.builder.ordering.price.ImmutablePrice(this.price));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the price miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.ordering.price.PriceMiter getMiter() {
        return (this.price);
    }


    /**
     *  Sets the price schedule.
     *
     *  @param schedule a price schedule
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>schedule</code>
     *          is <code>null</code>
     */

    public T priceSchedule(org.osid.ordering.PriceSchedule schedule) {
        getMiter().setPriceSchedule(schedule);
        return (self());
    }


    /**
     *  Sets the minimum quantity.
     *
     *  @param quantity a minimum quantity
     *  @return the builder
     *  @throws org.osid.InvalidArgumentException
     *          <code>quantity</code> is negative
     */

    public T minimumQuantity(long quantity) {
        getMiter().setMinimumQuantity(quantity);
        return (self());
    }


    /**
     *  Sets the maximum quantity.
     *
     *  @param quantity a maximum quantity
     *  @return the builder
     *  @throws org.osid.InvalidArgumentException
     *          <code>quantity</code> is negative
     */

    public T maximumQuantity(long quantity) {
        getMiter().setMaximumQuantity(quantity);
        return (self());
    }


    /**
     *  Sets the demographic.
     *
     *  @param demographic a demographic
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>demographic</code> is <code>null</code>
     */

    public T demographic(org.osid.resource.Resource demographic) {
        getMiter().setDemographic(demographic);
        return (self());
    }


    /**
     *  Sets the amount.
     *
     *  @param amount an amount
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    public T amount(org.osid.financials.Currency amount) {
        getMiter().setAmount(amount);
        return (self());
    }


    /**
     *  Sets the recurring interval.
     *
     *  @param interval a recurring interval
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>interval</code>
     *          is <code>null</code>
     */

    public T recurringInterval(org.osid.calendaring.Duration interval) {
        getMiter().setRecurringInterval(interval);
        return (self());
    }


    /**
     *  Adds a Price record.
     *
     *  @param record a price record
     *  @param recordType the type of price record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.ordering.records.PriceRecord record, org.osid.type.Type recordType) {
        getMiter().addPriceRecord(record, recordType);
        return (self());
    }
}       


