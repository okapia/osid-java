//
// SingleRouteList.java
//
//     Implements a RouteList based on a single Route.
//
//
// Tom Coppeto
// OnTapSolutions
// 29 June 2008
//
//
// Copyright (c) 2008, 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.route.route;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a ObjectList based on a single Route.
 */

public final class SingleRouteList
    extends net.okapia.osid.jamocha.mapping.route.route.spi.AbstractRouteList
    implements org.osid.mapping.route.RouteList {

    private final org.osid.mapping.route.Route route;
    private boolean read = false;


    /**
     *  Creates a new <code>SingleRouteList</code>.
     *
     *  @param route a route
     *  @throws org.osid.NullArgumentException <code>route</code>
     *          is <code>null</code>
     */

    public SingleRouteList(org.osid.mapping.route.Route route) {
        nullarg(route, "route");
        this.route = route;
        return;
    }


    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code>true</code> if more elements are available in
     *          this list, <code>false</code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (this.read) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code>Route</code> in this list. 
     *
     *  @return the next <code>Route</code> in this list. The
     *          <code> hasNext() </code> method should be used to test
     *          that a next <code>Route</code> is available before
     *          calling this method.
     *  @throws org.osid.IllegalStateException no more elements
     *          available in this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.mapping.route.Route getNextRoute()
        throws org.osid.OperationFailedException {

        if (hasNext()) {
            this.read = true;
            return (this.route);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in route list");
        }
    }
}
