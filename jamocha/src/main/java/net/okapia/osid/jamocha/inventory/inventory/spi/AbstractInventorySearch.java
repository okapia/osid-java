//
// AbstractInventorySearch.java
//
//     A template for making an Inventory Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.inventory.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing inventory searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractInventorySearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.inventory.InventorySearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.inventory.records.InventorySearchRecord> records = new java.util.ArrayList<>();
    private org.osid.inventory.InventorySearchOrder inventorySearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of inventories. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  inventoryIds list of inventories
     *  @throws org.osid.NullArgumentException
     *          <code>inventoryIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongInventories(org.osid.id.IdList inventoryIds) {
        while (inventoryIds.hasNext()) {
            try {
                this.ids.add(inventoryIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongInventories</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of inventory Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getInventoryIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  inventorySearchOrder inventory search order 
     *  @throws org.osid.NullArgumentException
     *          <code>inventorySearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>inventorySearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderInventoryResults(org.osid.inventory.InventorySearchOrder inventorySearchOrder) {
	this.inventorySearchOrder = inventorySearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.inventory.InventorySearchOrder getInventorySearchOrder() {
	return (this.inventorySearchOrder);
    }


    /**
     *  Gets the record corresponding to the given inventory search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an inventory implementing the requested record.
     *
     *  @param inventorySearchRecordType an inventory search record
     *         type
     *  @return the inventory search record
     *  @throws org.osid.NullArgumentException
     *          <code>inventorySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(inventorySearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.records.InventorySearchRecord getInventorySearchRecord(org.osid.type.Type inventorySearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.inventory.records.InventorySearchRecord record : this.records) {
            if (record.implementsRecordType(inventorySearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(inventorySearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this inventory search. 
     *
     *  @param inventorySearchRecord inventory search record
     *  @param inventorySearchRecordType inventory search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addInventorySearchRecord(org.osid.inventory.records.InventorySearchRecord inventorySearchRecord, 
                                           org.osid.type.Type inventorySearchRecordType) {

        addRecordType(inventorySearchRecordType);
        this.records.add(inventorySearchRecord);        
        return;
    }
}
