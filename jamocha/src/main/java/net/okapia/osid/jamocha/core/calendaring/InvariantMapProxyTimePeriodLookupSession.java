//
// InvariantMapProxyTimePeriodLookupSession
//
//    Implements a TimePeriod lookup service backed by a fixed
//    collection of timePeriods. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring;


/**
 *  Implements a TimePeriod lookup service backed by a fixed
 *  collection of time periods. The time periods are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyTimePeriodLookupSession
    extends net.okapia.osid.jamocha.core.calendaring.spi.AbstractMapTimePeriodLookupSession
    implements org.osid.calendaring.TimePeriodLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyTimePeriodLookupSession} with no
     *  time periods.
     *
     *  @param calendar the calendar
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyTimePeriodLookupSession(org.osid.calendaring.Calendar calendar,
                                                  org.osid.proxy.Proxy proxy) {
        setCalendar(calendar);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyTimePeriodLookupSession} with a single
     *  time period.
     *
     *  @param calendar the calendar
     *  @param timePeriod a single time period
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code timePeriod} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyTimePeriodLookupSession(org.osid.calendaring.Calendar calendar,
                                                  org.osid.calendaring.TimePeriod timePeriod, org.osid.proxy.Proxy proxy) {

        this(calendar, proxy);
        putTimePeriod(timePeriod);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyTimePeriodLookupSession} using
     *  an array of time periods.
     *
     *  @param calendar the calendar
     *  @param timePeriods an array of time periods
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code timePeriods} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyTimePeriodLookupSession(org.osid.calendaring.Calendar calendar,
                                                  org.osid.calendaring.TimePeriod[] timePeriods, org.osid.proxy.Proxy proxy) {

        this(calendar, proxy);
        putTimePeriods(timePeriods);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyTimePeriodLookupSession} using a
     *  collection of time periods.
     *
     *  @param calendar the calendar
     *  @param timePeriods a collection of time periods
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code timePeriods} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyTimePeriodLookupSession(org.osid.calendaring.Calendar calendar,
                                                  java.util.Collection<? extends org.osid.calendaring.TimePeriod> timePeriods,
                                                  org.osid.proxy.Proxy proxy) {

        this(calendar, proxy);
        putTimePeriods(timePeriods);
        return;
    }
}
