//
// AbstractFederatingLocationLookupSession.java
//
//     An abstract federating adapter for a LocationLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.mapping.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  LocationLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingLocationLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.mapping.LocationLookupSession>
    implements org.osid.mapping.LocationLookupSession {

    private boolean parallel = false;
    private org.osid.mapping.Map map = new net.okapia.osid.jamocha.nil.mapping.map.UnknownMap();


    /**
     *  Constructs a new <code>AbstractFederatingLocationLookupSession</code>.
     */

    protected AbstractFederatingLocationLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.mapping.LocationLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Map/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Map Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getMapId() {
        return (this.map.getId());
    }


    /**
     *  Gets the <code>Map</code> associated with this 
     *  session.
     *
     *  @return the <code>Map</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.Map getMap()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.map);
    }


    /**
     *  Sets the <code>Map</code>.
     *
     *  @param  map the map for this session
     *  @throws org.osid.NullArgumentException <code>map</code>
     *          is <code>null</code>
     */

    protected void setMap(org.osid.mapping.Map map) {
        nullarg(map, "map");
        this.map = map;
        return;
    }


    /**
     *  Tests if this user can perform <code>Location</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupLocations() {
        for (org.osid.mapping.LocationLookupSession session : getSessions()) {
            if (session.canLookupLocations()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Location</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeLocationView() {
        for (org.osid.mapping.LocationLookupSession session : getSessions()) {
            session.useComparativeLocationView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Location</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryLocationView() {
        for (org.osid.mapping.LocationLookupSession session : getSessions()) {
            session.usePlenaryLocationView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include locations in maps which are children
     *  of this map in the map hierarchy.
     */

    @OSID @Override
    public void useFederatedMapView() {
        for (org.osid.mapping.LocationLookupSession session : getSessions()) {
            session.useFederatedMapView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this map only.
     */

    @OSID @Override
    public void useIsolatedMapView() {
        for (org.osid.mapping.LocationLookupSession session : getSessions()) {
            session.useIsolatedMapView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Location</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Location</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Location</code> and
     *  retained for compatibility.
     *
     *  @param  locationId <code>Id</code> of the
     *          <code>Location</code>
     *  @return the location
     *  @throws org.osid.NotFoundException <code>locationId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>locationId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.Location getLocation(org.osid.id.Id locationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.mapping.LocationLookupSession session : getSessions()) {
            try {
                return (session.getLocation(locationId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(locationId + " not found");
    }


    /**
     *  Gets a <code>LocationList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  locations specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Locations</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  locationIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Location</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>locationIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.LocationList getLocationsByIds(org.osid.id.IdList locationIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.mapping.location.MutableLocationList ret = new net.okapia.osid.jamocha.mapping.location.MutableLocationList();

        try (org.osid.id.IdList ids = locationIds) {
            while (ids.hasNext()) {
                ret.addLocation(getLocation(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>LocationList</code> corresponding to the given
     *  location genus <code>Type</code> which does not include
     *  locations of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  locations or an error results. Otherwise, the returned list
     *  may contain only those locations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  locationGenusType a location genus type 
     *  @return the returned <code>Location</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>locationGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.LocationList getLocationsByGenusType(org.osid.type.Type locationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.mapping.location.FederatingLocationList ret = getLocationList();

        for (org.osid.mapping.LocationLookupSession session : getSessions()) {
            ret.addLocationList(session.getLocationsByGenusType(locationGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>LocationList</code> corresponding to the given
     *  location genus <code>Type</code> and include any additional
     *  locations with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  locations or an error results. Otherwise, the returned list
     *  may contain only those locations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  locationGenusType a location genus type 
     *  @return the returned <code>Location</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>locationGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.LocationList getLocationsByParentGenusType(org.osid.type.Type locationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.mapping.location.FederatingLocationList ret = getLocationList();

        for (org.osid.mapping.LocationLookupSession session : getSessions()) {
            ret.addLocationList(session.getLocationsByParentGenusType(locationGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>LocationList</code> containing the given
     *  location record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  locations or an error results. Otherwise, the returned list
     *  may contain only those locations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  locationRecordType a location record type 
     *  @return the returned <code>Location</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>locationRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.LocationList getLocationsByRecordType(org.osid.type.Type locationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.mapping.location.FederatingLocationList ret = getLocationList();

        for (org.osid.mapping.LocationLookupSession session : getSessions()) {
            ret.addLocationList(session.getLocationsByRecordType(locationRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Locations</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  locations or an error results. Otherwise, the returned list
     *  may contain only those locations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Locations</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.LocationList getLocations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.mapping.location.FederatingLocationList ret = getLocationList();

        for (org.osid.mapping.LocationLookupSession session : getSessions()) {
            ret.addLocationList(session.getLocations());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.mapping.location.FederatingLocationList getLocationList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.mapping.location.ParallelLocationList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.mapping.location.CompositeLocationList());
        }
    }
}
