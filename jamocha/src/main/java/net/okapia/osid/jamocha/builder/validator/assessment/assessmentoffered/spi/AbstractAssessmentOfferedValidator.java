//
// AbstractAssessmentOfferedValidator.java
//
//     Validates an AssessmentOffered.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.assessment.assessmentoffered.spi;


/**
 *  Validates an AssessmentOffered.
 */

public abstract class AbstractAssessmentOfferedValidator
    extends net.okapia.osid.jamocha.builder.validator.spi.AbstractOsidObjectValidator {


    /**
     *  Constructs a new <code>AbstractAssessmentOfferedValidator</code>.
     */

    protected AbstractAssessmentOfferedValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AbstractAssessmentOfferedValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractAssessmentOfferedValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates an AssessmentOffered.
     *
     *  @param assessmentOffered an assessment offered to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>assessmentOffered</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in
     *          assembly
     */
    
    public void validate(org.osid.assessment.AssessmentOffered assessmentOffered) {
        super.validate(assessmentOffered);

        testNestedObject(assessmentOffered, "getAssessment");
        testNestedObject(assessmentOffered, "getLevel");

        testConditionalMethod(assessmentOffered, "getStartTime", assessmentOffered.hasStartTime(), "hasStartTime()");
        testConditionalMethod(assessmentOffered, "getDeadline", assessmentOffered.hasDeadline(), "hasDeadline()");
        testConditionalMethod(assessmentOffered, "getDuration", assessmentOffered.hasDuration(), "hasDuration()");

        testConditionalMethod(assessmentOffered, "getScoreSystemId", assessmentOffered.isScored(), "isScored()");
        testConditionalMethod(assessmentOffered, "getScoreSystem", assessmentOffered.isScored(), "isScored()");
        if (assessmentOffered.isScored()) {
            testNestedObject(assessmentOffered, "getScoreSystem");
        }

        testConditionalMethod(assessmentOffered, "getGradeSystemId", assessmentOffered.isGraded(), "isGraded()");
        testConditionalMethod(assessmentOffered, "getGradeSystem", assessmentOffered.isGraded(), "isGraded()");
        if (assessmentOffered.isGraded()) {
            testNestedObject(assessmentOffered, "getGradeSystem");
        }


        testConditionalMethod(assessmentOffered, "getRubricId", assessmentOffered.hasRubric(), "hasRubric()");
        testConditionalMethod(assessmentOffered, "getRubric", assessmentOffered.hasRubric(), "hasRubric()");
        if (assessmentOffered.hasRubric()) {
            testNestedObject(assessmentOffered, "getRubric");
        }

        return;
    }
}
