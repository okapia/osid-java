//
// AbstractImmutableGradeMap.java
//
//     Wraps a mutable GradeMap to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.grading.transform.grademap.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>GradeMap</code> to hide modifiers. This
 *  wrapper provides an immutized GradeMap from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying grade map whose state changes are visible.
 */

public abstract class AbstractImmutableGradeMap
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutable
    implements org.osid.grading.transform.GradeMap {

    private final org.osid.grading.transform.GradeMap gradeMap;


    /**
     *  Constructs a new <code>AbstractImmutableGradeMap</code>.
     *
     *  @param gradeMap the grade map to immutablize
     *  @throws org.osid.NullArgumentException <code>gradeMap</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableGradeMap(org.osid.grading.transform.GradeMap gradeMap) {
        super(gradeMap);
        this.gradeMap = gradeMap;
        return;
    }


    /**
     *  Gets the source <code> Grade Id. </code> 
     *
     *  @return the source grade <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSourceGradeId() {
        return (this.gradeMap.getSourceGradeId());
    }


    /**
     *  Gets the source <code> Grade. </code> 
     *
     *  @return the source grade 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.Grade getSourceGrade()
        throws org.osid.OperationFailedException {

        return (this.gradeMap.getSourceGrade());
    }


    /**
     *  Gets the target <code> Grade Id. </code> 
     *
     *  @return the target grade <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getTargetGradeId() {
        return (this.gradeMap.getTargetGradeId());
    }


    /**
     *  Gets the target <code> Grade. </code> 
     *
     *  @return the target grade 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.Grade getTargetGrade()
        throws org.osid.OperationFailedException {
        
        return (this.gradeMap.getTargetGrade());
    }
}

