//
// TimePeriodElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.timeperiod.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class TimePeriodElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the TimePeriodElement Id.
     *
     *  @return the time period element Id
     */

    public static org.osid.id.Id getTimePeriodEntityId() {
        return (makeEntityId("osid.calendaring.TimePeriod"));
    }


    /**
     *  Gets the Start element Id.
     *
     *  @return the Start element Id
     */

    public static org.osid.id.Id getStart() {
        return (makeElementId("osid.calendaring.timeperiod.Start"));
    }


    /**
     *  Gets the End element Id.
     *
     *  @return the End element Id
     */

    public static org.osid.id.Id getEnd() {
        return (makeElementId("osid.calendaring.timeperiod.End"));
    }


    /**
     *  Gets the ExceptionIds element Id.
     *
     *  @return the ExceptionIds element Id
     */

    public static org.osid.id.Id getExceptionIds() {
        return (makeElementId("osid.calendaring.timeperiod.ExceptionIds"));
    }


    /**
     *  Gets the Exceptions element Id.
     *
     *  @return the Exceptions element Id
     */

    public static org.osid.id.Id getExceptions() {
        return (makeElementId("osid.calendaring.timeperiod.Exceptions"));
    }


    /**
     *  Gets the Time element Id.
     *
     *  @return the Time element Id
     */

    public static org.osid.id.Id getTime() {
        return (makeQueryElementId("osid.calendaring.timeperiod.Time"));
    }


    /**
     *  Gets the TimeInclusive element Id.
     *
     *  @return the TimeInclusive element Id
     */

    public static org.osid.id.Id getTimeInclusive() {
        return (makeQueryElementId("osid.calendaring.timeperiod.TimeInclusive"));
    }


    /**
     *  Gets the Duration element Id.
     *
     *  @return the Duration element Id
     */

    public static org.osid.id.Id getDuration() {
        return (makeQueryElementId("osid.calendaring.timeperiod.Duration"));
    }


    /**
     *  Gets the EventId element Id.
     *
     *  @return the EventId element Id
     */

    public static org.osid.id.Id getEventId() {
        return (makeQueryElementId("osid.calendaring.timeperiod.EventId"));
    }


    /**
     *  Gets the Event element Id.
     *
     *  @return the Event element Id
     */

    public static org.osid.id.Id getEvent() {
        return (makeQueryElementId("osid.calendaring.timeperiod.Event"));
    }


    /**
     *  Gets the CalendarId element Id.
     *
     *  @return the CalendarId element Id
     */

    public static org.osid.id.Id getCalendarId() {
        return (makeQueryElementId("osid.calendaring.timeperiod.CalendarId"));
    }


    /**
     *  Gets the Calendar element Id.
     *
     *  @return the Calendar element Id
     */

    public static org.osid.id.Id getCalendar() {
        return (makeQueryElementId("osid.calendaring.timeperiod.Calendar"));
    }
}
