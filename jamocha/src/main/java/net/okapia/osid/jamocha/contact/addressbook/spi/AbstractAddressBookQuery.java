//
// AbstractAddressBookQuery.java
//
//     A template for making an AddressBook Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.contact.addressbook.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for address books.
 */

public abstract class AbstractAddressBookQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.contact.AddressBookQuery {

    private final java.util.Collection<org.osid.contact.records.AddressBookQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the contact <code> Id </code> for this query to match contacts 
     *  assigned to address books. 
     *
     *  @param  contactId a contact <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> contactId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchContactId(org.osid.id.Id contactId, boolean match) {
        return;
    }


    /**
     *  Clears the contact <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearContactIdTerms() {
        return;
    }


    /**
     *  Tests if a contact query is available. 
     *
     *  @return <code> true </code> if a contact query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContactQuery() {
        return (false);
    }


    /**
     *  Gets the query for an address book. 
     *
     *  @return the contact query 
     *  @throws org.osid.UnimplementedException <code> supportsContactQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.ContactQuery getContactQuery() {
        throw new org.osid.UnimplementedException("supportsContactQuery() is false");
    }


    /**
     *  Matches address books with any contact. 
     *
     *  @param  match <code> true </code> to match address books with any 
     *          contact, <code> false </code> to match address books with no 
     *          contacts 
     */

    @OSID @Override
    public void matchAnyContact(boolean match) {
        return;
    }


    /**
     *  Clears the contact terms. 
     */

    @OSID @Override
    public void clearContactTerms() {
        return;
    }


    /**
     *  Sets the address <code> Id </code> for this query to match contacts 
     *  assigned to addresses. 
     *
     *  @param  addressId an address <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> addressId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAddressId(org.osid.id.Id addressId, boolean match) {
        return;
    }


    /**
     *  Clears the address <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAddressIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AddressQuery </code> is available. 
     *
     *  @return <code> true </code> if an address query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAddressQuery() {
        return (false);
    }


    /**
     *  Gets the query for an address query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the address query 
     *  @throws org.osid.UnimplementedException <code> supportsAddressQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressQuery getAddressQuery() {
        throw new org.osid.UnimplementedException("supportsAddressQuery() is false");
    }


    /**
     *  Matches address books with any address. 
     *
     *  @param  match <code> true </code> to match address books with any 
     *          address, <code> false </code> to match address books with no 
     *          addresses 
     */

    @OSID @Override
    public void matchAnyAddress(boolean match) {
        return;
    }


    /**
     *  Clears the address terms. 
     */

    @OSID @Override
    public void clearAddressTerms() {
        return;
    }


    /**
     *  Sets the address book <code> Id </code> for this query to match 
     *  address books that have the specified address book as an ancestor. 
     *
     *  @param  addressBookId an address book <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> addressBookId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorAddressBookId(org.osid.id.Id addressBookId, 
                                           boolean match) {
        return;
    }


    /**
     *  Clears the ancestor address book <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorAddressBookIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AddressBookQuery </code> is available. 
     *
     *  @return <code> true </code> if an address book query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorAddressBookQuery() {
        return (false);
    }


    /**
     *  Gets the query for an address book. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the address book query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorAddressBookQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressBookQuery getAncestorAddressBookQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorAddressBookQuery() is false");
    }


    /**
     *  Matches address books with any ancestor. 
     *
     *  @param  match <code> true </code> to match address books with any 
     *          ancestor, <code> false </code> to match root address books 
     */

    @OSID @Override
    public void matchAnyAncestorAddressBook(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor address book terms. 
     */

    @OSID @Override
    public void clearAncestorAddressBookTerms() {
        return;
    }


    /**
     *  Sets the address book <code> Id </code> for this query to match 
     *  address books that have the specified address book as a descendant. 
     *
     *  @param  addressBookId an address book <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> addressBookId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantAddressBookId(org.osid.id.Id addressBookId, 
                                             boolean match) {
        return;
    }


    /**
     *  Clears the descendant address book <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantAddressBookIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AddressBookQuery </code> is available. 
     *
     *  @return <code> true </code> if an address book query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantAddressBookQuery() {
        return (false);
    }


    /**
     *  Gets the query for an address book. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the address book query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantAddressBookQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressBookQuery getDescendantAddressBookQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantAddressBookQuery() is false");
    }


    /**
     *  Matches address books with any descendant. 
     *
     *  @param  match <code> true </code> to match address books with any 
     *          descendant, <code> false </code> to match leaf address books 
     */

    @OSID @Override
    public void matchAnyDescendantAddressBook(boolean match) {
        return;
    }


    /**
     *  Clears the descendant address book terms. 
     */

    @OSID @Override
    public void clearDescendantAddressBookTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given address book query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an address book implementing the requested record.
     *
     *  @param addressBookRecordType an address book record type
     *  @return the address book query record
     *  @throws org.osid.NullArgumentException
     *          <code>addressBookRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(addressBookRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.contact.records.AddressBookQueryRecord getAddressBookQueryRecord(org.osid.type.Type addressBookRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.contact.records.AddressBookQueryRecord record : this.records) {
            if (record.implementsRecordType(addressBookRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(addressBookRecordType + " is not supported");
    }


    /**
     *  Adds a record to this address book query. 
     *
     *  @param addressBookQueryRecord address book query record
     *  @param addressBookRecordType addressBook record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAddressBookQueryRecord(org.osid.contact.records.AddressBookQueryRecord addressBookQueryRecord, 
                                          org.osid.type.Type addressBookRecordType) {

        addRecordType(addressBookRecordType);
        nullarg(addressBookQueryRecord, "address book query record");
        this.records.add(addressBookQueryRecord);        
        return;
    }
}
