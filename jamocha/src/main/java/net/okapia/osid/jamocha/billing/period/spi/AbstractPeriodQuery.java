//
// AbstractPeriodQuery.java
//
//     A template for making a Period Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.period.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for periods.
 */

public abstract class AbstractPeriodQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.billing.PeriodQuery {

    private final java.util.Collection<org.osid.billing.records.PeriodQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Adds a display label for this query. 
     *
     *  @param  label label string to match 
     *  @param  stringMatchType the label match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> label </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> label </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchDisplayLabel(String label, 
                                  org.osid.type.Type stringMatchType, 
                                  boolean match) {
        return;
    }


    /**
     *  Matches a display label that has any value. 
     *
     *  @param  match <code> true </code> to match customers with any label, 
     *          <code> false </code> to match assets with no title 
     */

    @OSID @Override
    public void matchAnyDisplayLabel(boolean match) {
        return;
    }


    /**
     *  Clears the display label query terms. 
     */

    @OSID @Override
    public void clearDisplayLabelTerms() {
        return;
    }


    /**
     *  Matches the open date between the given range inclusive. 
     *
     *  @param  low low date range 
     *  @param  high high date range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchOpenDate(org.osid.calendaring.DateTime low, 
                              org.osid.calendaring.DateTime high, 
                              boolean match) {
        return;
    }


    /**
     *  Matches a period that has any open date assigned. 
     *
     *  @param  match <code> true </code> to match periods with any open date, 
     *          <code> false </code> to match events with no open date 
     */

    @OSID @Override
    public void matchAnyOpenDate(boolean match) {
        return;
    }


    /**
     *  Clears the open date query terms. 
     */

    @OSID @Override
    public void clearOpenDateTerms() {
        return;
    }


    /**
     *  Matches the close date between the given range inclusive. 
     *
     *  @param  low low date range 
     *  @param  high high date range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchCloseDate(org.osid.calendaring.DateTime low, 
                               org.osid.calendaring.DateTime high, 
                               boolean match) {
        return;
    }


    /**
     *  Matches a period that has any close date assigned. 
     *
     *  @param  match <code> true </code> to match periods with any close 
     *          date, <code> false </code> to match events with no close date 
     */

    @OSID @Override
    public void matchAnyCloseDate(boolean match) {
        return;
    }


    /**
     *  Clears the close date query terms. 
     */

    @OSID @Override
    public void clearCloseDateTerms() {
        return;
    }


    /**
     *  Matches the billing date between the given range inclusive. 
     *
     *  @param  low low date range 
     *  @param  high high date range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchBillingDate(org.osid.calendaring.DateTime low, 
                                 org.osid.calendaring.DateTime high, 
                                 boolean match) {
        return;
    }


    /**
     *  Matches a period that has any billing date assigned. 
     *
     *  @param  match <code> true </code> to match periods with any billing 
     *          date, <code> false </code> to match events with no billing 
     *          date 
     */

    @OSID @Override
    public void matchAnyBillingDate(boolean match) {
        return;
    }


    /**
     *  Clears the billing date query terms. 
     */

    @OSID @Override
    public void clearBillingDateTerms() {
        return;
    }


    /**
     *  Matches the due date between the given range inclusive. 
     *
     *  @param  low low date range 
     *  @param  high high date range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchDueDate(org.osid.calendaring.DateTime low, 
                             org.osid.calendaring.DateTime high, boolean match) {
        return;
    }


    /**
     *  Matches a period that has any due date assigned. 
     *
     *  @param  match <code> true </code> to match periods with any due date, 
     *          <code> false </code> to match events with no due date 
     */

    @OSID @Override
    public void matchAnyDueDate(boolean match) {
        return;
    }


    /**
     *  Clears the due date query terms. 
     */

    @OSID @Override
    public void clearDueDateTerms() {
        return;
    }


    /**
     *  Sets the business <code> Id </code> for this query to match periods 
     *  assigned to businesses. 
     *
     *  @param  businessId the business <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBusinessId(org.osid.id.Id businessId, boolean match) {
        return;
    }


    /**
     *  Clears the business <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearBusinessIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BusinessQuery </code> is available. 
     *
     *  @return <code> true </code> if a business query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a business. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the business query 
     *  @throws org.osid.UnimplementedException <code> supportsBusinessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.BusinessQuery getBusinessQuery() {
        throw new org.osid.UnimplementedException("supportsBusinessQuery() is false");
    }


    /**
     *  Clears the business query terms. 
     */

    @OSID @Override
    public void clearBusinessTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given period query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a period implementing the requested record.
     *
     *  @param periodRecordType a period record type
     *  @return the period query record
     *  @throws org.osid.NullArgumentException
     *          <code>periodRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(periodRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.records.PeriodQueryRecord getPeriodQueryRecord(org.osid.type.Type periodRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.records.PeriodQueryRecord record : this.records) {
            if (record.implementsRecordType(periodRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(periodRecordType + " is not supported");
    }


    /**
     *  Adds a record to this period query. 
     *
     *  @param periodQueryRecord period query record
     *  @param periodRecordType period record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPeriodQueryRecord(org.osid.billing.records.PeriodQueryRecord periodQueryRecord, 
                                          org.osid.type.Type periodRecordType) {

        addRecordType(periodRecordType);
        nullarg(periodQueryRecord, "period query record");
        this.records.add(periodQueryRecord);        
        return;
    }
}
