//
// AbstractImmutableProxy.java
//
//     Wraps a mutable Proxy to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.proxy.proxy.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Proxy</code> to hide modifiers. This
 *  wrapper provides an immutized Proxy from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying proxy whose state changes are visible.
 */

public abstract class AbstractImmutableProxy
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidResult
    implements org.osid.proxy.Proxy {

    private final org.osid.proxy.Proxy proxy;


    /**
     *  Constructs a new <code>AbstractImmutableProxy</code>.
     *
     *  @param proxy the proxy to immutablize
     *  @throws org.osid.NullArgumentException <code>proxy</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableProxy(org.osid.proxy.Proxy proxy) {
        super(proxy);
        this.proxy = proxy;
        return;
    }


    /**
     *  Tests if an authentication is available. 
     *
     *  @return <code> true </code> if an <code> Authentication </code> is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasAuthentication() {
        return (this.proxy.hasAuthentication());
    }


    /**
     *  Gets the <code> Authentication </code> for this proxy. 
     *
     *  @return the authentication 
     *  @throws org.osid.IllegalStateException <code> hasAuthentication() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.process.Authentication getAuthentication() {
        return (this.proxy.getAuthentication());
    }


    /**
     *  Tests if an effective agent is available. 
     *
     *  @return <code> true </code> if an effective agent is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasEffectiveAgent() {
        return (this.proxy.hasEffectiveAgent());
    }


    /**
     *  Gets the effective <code> Agent Id </code> for this proxy. 
     *
     *  @return the effective agent <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasEffectiveAgent() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getEffectiveAgentId() {
        return (this.proxy.getEffectiveAgentId());
    }


    /**
     *  Gets the effective <code> Agent </code> for this proxy. 
     *
     *  @return the effective agent
     *  @throws org.osid.IllegalStateException <code> hasEffectiveAgent() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request
     */

    @OSID @Override
    public org.osid.authentication.Agent getEffectiveAgent()
        throws org.osid.OperationFailedException {

        return (this.proxy.getEffectiveAgent());
    }
    

    /**
     *  Tests if an effective date is available. 
     *
     *  @return <code> true </code> if an effective date is available,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean hasEffectiveDate() {
        return (this.proxy.hasEffectiveDate());
    }


    /**
     *  Gets the effective date. 
     *
     *  @return the effective date 
     *  @throws org.osid.IllegalStateException <code> hasEffectiveDate() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public java.util.Date getEffectiveDate() {
        return (this.proxy.getEffectiveDate());
    }


    /**
     *  Gets the rate of the clock. 
     *
     *  @return the rate 
     *  @throws org.osid.IllegalStateException <code> hasEffectiveDate() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getEffectiveClockRate() {
        return (this.proxy.getEffectiveClockRate());
    }

    
    /**
     *  Gets the locale. 
     *
     *  @return a locale 
     */

    @OSID @Override
    public org.osid.locale.Locale getLocale() {
        return (this.proxy.getLocale());
    }


    /**
     *  Tests if a <code> DisplayText </code> format <code> Type
     *  </code> is available.
     *
     *  @return <code> true </code> if a format type is available, <code> 
     *          false </code> otherwise 
      */

    @OSID @Override
    public boolean hasFormatType() {
        return (this.proxy.hasFormatType());
    }


    /**
     *  Gets the <code> DisplayText </code> format <code>
     *  Type. </code>
     *
     *  @return the format <code> Type </code> 
     *  @throws org.osid.IllegalStateException <code> hasFormatType()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.type.Type getFormatType() {
        return (this.proxy.getFormatType());
    }


    /**
     *  Gets the proxy record corresponding to the given <code> Proxy </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> proxyRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(proxyRecordType) </code> is <code> true </code> . 
     *
     *  @param  proxyRecordType the type of the record to retrieve 
     *  @return the proxy record 
     *  @throws org.osid.NullArgumentException <code> proxyRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(proxyRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.proxy.records.ProxyRecord getProxyRecord(org.osid.type.Type proxyRecordType)
        throws org.osid.OperationFailedException {

        return (this.proxy.getProxyRecord(proxyRecordType));
    }
}

