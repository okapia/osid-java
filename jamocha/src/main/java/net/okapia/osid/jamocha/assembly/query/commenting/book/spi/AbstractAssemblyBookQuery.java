//
// AbstractAssemblyBookQuery.java
//
//     A BookQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.commenting.book.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A BookQuery that stores terms.
 */

public abstract class AbstractAssemblyBookQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.commenting.BookQuery,
               org.osid.commenting.BookQueryInspector,
               org.osid.commenting.BookSearchOrder {

    private final java.util.Collection<org.osid.commenting.records.BookQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.commenting.records.BookQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.commenting.records.BookSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyBookQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyBookQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the comment <code> Id </code> for this query to match comments 
     *  assigned to books. 
     *
     *  @param  commentId a comment <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> commentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCommentId(org.osid.id.Id commentId, boolean match) {
        getAssembler().addIdTerm(getCommentIdColumn(), commentId, match);
        return;
    }


    /**
     *  Clears the comment <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCommentIdTerms() {
        getAssembler().clearTerms(getCommentIdColumn());
        return;
    }


    /**
     *  Gets the comment <code> Id </code> terms. 
     *
     *  @return the comment <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCommentIdTerms() {
        return (getAssembler().getIdTerms(getCommentIdColumn()));
    }


    /**
     *  Gets the CommentId column name.
     *
     * @return the column name
     */

    protected String getCommentIdColumn() {
        return ("comment_id");
    }


    /**
     *  Tests if a comment query is available. 
     *
     *  @return <code> true </code> if a comment query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommentQuery() {
        return (false);
    }


    /**
     *  Gets the query for a comment. 
     *
     *  @return the comment query 
     *  @throws org.osid.UnimplementedException <code> supportsCommentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentQuery getCommentQuery() {
        throw new org.osid.UnimplementedException("supportsCommentQuery() is false");
    }


    /**
     *  Matches books with any comment. 
     *
     *  @param  match <code> true </code> to match books with any comment, 
     *          <code> false </code> to match books with no comments 
     */

    @OSID @Override
    public void matchAnyComment(boolean match) {
        getAssembler().addIdWildcardTerm(getCommentColumn(), match);
        return;
    }


    /**
     *  Clears the comment terms. 
     */

    @OSID @Override
    public void clearCommentTerms() {
        getAssembler().clearTerms(getCommentColumn());
        return;
    }


    /**
     *  Gets the comment terms. 
     *
     *  @return the comment terms 
     */

    @OSID @Override
    public org.osid.commenting.CommentQueryInspector[] getCommentTerms() {
        return (new org.osid.commenting.CommentQueryInspector[0]);
    }


    /**
     *  Gets the Comment column name.
     *
     * @return the column name
     */

    protected String getCommentColumn() {
        return ("comment");
    }


    /**
     *  Sets the book <code> Id </code> for this query to match books that 
     *  have the specified book as an ancestor. 
     *
     *  @param  bookId a book <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, a <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> bookId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchAncestorBookId(org.osid.id.Id bookId, boolean match) {
        getAssembler().addIdTerm(getAncestorBookIdColumn(), bookId, match);
        return;
    }


    /**
     *  Clears the ancestor book <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorBookIdTerms() {
        getAssembler().clearTerms(getAncestorBookIdColumn());
        return;
    }


    /**
     *  Gets the ancestor book <code> Id </code> terms. 
     *
     *  @return the ancestor book <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorBookIdTerms() {
        return (getAssembler().getIdTerms(getAncestorBookIdColumn()));
    }


    /**
     *  Gets the AncestorBookId column name.
     *
     * @return the column name
     */

    protected String getAncestorBookIdColumn() {
        return ("ancestor_book_id");
    }


    /**
     *  Tests if a <code> BookQuery </code> is available. 
     *
     *  @return <code> true </code> if a book query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorBookQuery() {
        return (false);
    }


    /**
     *  Gets the query for a book. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the book query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorBookQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.BookQuery getAncestorBookQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorBookQuery() is false");
    }


    /**
     *  Matches books with any ancestor. 
     *
     *  @param  match <code> true </code> to match books with any ancestor, 
     *          <code> false </code> to match root books 
     */

    @OSID @Override
    public void matchAnyAncestorBook(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorBookColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor book terms. 
     */

    @OSID @Override
    public void clearAncestorBookTerms() {
        getAssembler().clearTerms(getAncestorBookColumn());
        return;
    }


    /**
     *  Gets the ancestor book terms. 
     *
     *  @return the ancestor book terms 
     */

    @OSID @Override
    public org.osid.commenting.BookQueryInspector[] getAncestorBookTerms() {
        return (new org.osid.commenting.BookQueryInspector[0]);
    }


    /**
     *  Gets the AncestorBook column name.
     *
     * @return the column name
     */

    protected String getAncestorBookColumn() {
        return ("ancestor_book");
    }


    /**
     *  Sets the book <code> Id </code> for this query to match books that 
     *  have the specified book as a descendant. 
     *
     *  @param  bookId a book <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> bookId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchDescendantBookId(org.osid.id.Id bookId, boolean match) {
        getAssembler().addIdTerm(getDescendantBookIdColumn(), bookId, match);
        return;
    }


    /**
     *  Clears the descendant book <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantBookIdTerms() {
        getAssembler().clearTerms(getDescendantBookIdColumn());
        return;
    }


    /**
     *  Gets the descendant book <code> Id </code> terms. 
     *
     *  @return the descendant book <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantBookIdTerms() {
        return (getAssembler().getIdTerms(getDescendantBookIdColumn()));
    }


    /**
     *  Gets the DescendantBookId column name.
     *
     * @return the column name
     */

    protected String getDescendantBookIdColumn() {
        return ("descendant_book_id");
    }


    /**
     *  Tests if a <code> BookQuery </code> is available. 
     *
     *  @return <code> true </code> if a book query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantBookQuery() {
        return (false);
    }


    /**
     *  Gets the query for a book. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the book query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantBookQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.BookQuery getDescendantBookQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantBookQuery() is false");
    }


    /**
     *  Matches books with any descendant. 
     *
     *  @param  match <code> true </code> to match books with any descendant, 
     *          <code> false </code> to match leaf books 
     */

    @OSID @Override
    public void matchAnyDescendantBook(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantBookColumn(), match);
        return;
    }


    /**
     *  Clears the descendant book terms. 
     */

    @OSID @Override
    public void clearDescendantBookTerms() {
        getAssembler().clearTerms(getDescendantBookColumn());
        return;
    }


    /**
     *  Gets the descendant book terms. 
     *
     *  @return the descendant book terms 
     */

    @OSID @Override
    public org.osid.commenting.BookQueryInspector[] getDescendantBookTerms() {
        return (new org.osid.commenting.BookQueryInspector[0]);
    }


    /**
     *  Gets the DescendantBook column name.
     *
     * @return the column name
     */

    protected String getDescendantBookColumn() {
        return ("descendant_book");
    }


    /**
     *  Tests if this book supports the given record
     *  <code>Type</code>.
     *
     *  @param  bookRecordType a book record type 
     *  @return <code>true</code> if the bookRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>bookRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type bookRecordType) {
        for (org.osid.commenting.records.BookQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(bookRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  bookRecordType the book record type 
     *  @return the book query record 
     *  @throws org.osid.NullArgumentException
     *          <code>bookRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(bookRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.commenting.records.BookQueryRecord getBookQueryRecord(org.osid.type.Type bookRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.commenting.records.BookQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(bookRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(bookRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  bookRecordType the book record type 
     *  @return the book query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>bookRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(bookRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.commenting.records.BookQueryInspectorRecord getBookQueryInspectorRecord(org.osid.type.Type bookRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.commenting.records.BookQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(bookRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(bookRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param bookRecordType the book record type
     *  @return the book search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>bookRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(bookRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.commenting.records.BookSearchOrderRecord getBookSearchOrderRecord(org.osid.type.Type bookRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.commenting.records.BookSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(bookRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(bookRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this book. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param bookQueryRecord the book query record
     *  @param bookQueryInspectorRecord the book query inspector
     *         record
     *  @param bookSearchOrderRecord the book search order record
     *  @param bookRecordType book record type
     *  @throws org.osid.NullArgumentException
     *          <code>bookQueryRecord</code>,
     *          <code>bookQueryInspectorRecord</code>,
     *          <code>bookSearchOrderRecord</code> or
     *          <code>bookRecordTypebook</code> is
     *          <code>null</code>
     */
            
    protected void addBookRecords(org.osid.commenting.records.BookQueryRecord bookQueryRecord, 
                                      org.osid.commenting.records.BookQueryInspectorRecord bookQueryInspectorRecord, 
                                      org.osid.commenting.records.BookSearchOrderRecord bookSearchOrderRecord, 
                                      org.osid.type.Type bookRecordType) {

        addRecordType(bookRecordType);

        nullarg(bookQueryRecord, "book query record");
        nullarg(bookQueryInspectorRecord, "book query inspector record");
        nullarg(bookSearchOrderRecord, "book search odrer record");

        this.queryRecords.add(bookQueryRecord);
        this.queryInspectorRecords.add(bookQueryInspectorRecord);
        this.searchOrderRecords.add(bookSearchOrderRecord);
        
        return;
    }
}
