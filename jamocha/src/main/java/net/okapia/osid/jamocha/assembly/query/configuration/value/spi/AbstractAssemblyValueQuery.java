//
// AbstractAssemblyValueQuery.java
//
//     A ValueQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.configuration.value.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ValueQuery that stores terms.
 */

public abstract class AbstractAssemblyValueQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOperableOsidObjectQuery
    implements org.osid.configuration.ValueQuery,
               org.osid.configuration.ValueQueryInspector,
               org.osid.configuration.ValueSearchOrder {

    private final java.util.Collection<org.osid.configuration.records.ValueQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.configuration.records.ValueQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.configuration.records.ValueSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyValueQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyValueQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Adds a priority match. Multiple ranges can be added to perform a 
     *  boolean <code> OR </code> among them. 
     *
     *  @param  low start priority value 
     *  @param  high end priority value 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchPriority(long low, long high, boolean match) {
        getAssembler().addCardinalRangeTerm(getPriorityColumn(), low, high, match);
        return;
    }


    /**
     *  Matches values with any priority. 
     *
     *  @param  match <code> true </code> if to match values with any 
     *          priority, <code> false </code> to match values with no 
     *          priority 
     */

    @OSID @Override
    public void matchAnyPriority(boolean match) {
        getAssembler().addCardinalRangeWildcardTerm(getPriorityColumn(), match);
        return;
    }


    /**
     *  Clears the priority terms. 
     */

    @OSID @Override
    public void clearPriorityTerms() {
        getAssembler().clearTerms(getPriorityColumn());
        return;
    }


    /**
     *  Gets the priority query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getPriorityTerms() {
        return (getAssembler().getCardinalRangeTerms(getPriorityColumn()));
    }


    /**
     *  Specifies a preference for ordering the results by the value
     *  priority.
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is
     *          <code> null </code>
     */

    @OSID @Override
    public void orderByPriority(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getPriorityColumn(), style);
        return;
    }


    /**
     *  Gets the Priority column name.
     *
     * @return the column name
     */

    protected String getPriorityColumn() {
        return ("priority");
    }


    /**
     *  Specifies a preference for ordering the results by the value.
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByValue(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getValueColumn(), style);
        return;
    }


    /**
     *  Gets the Value column name.
     *
     * @return the column name
     */

    protected String getValueColumn() {
        return ("value");
    }


    /**
     *  Adds a boolean match. 
     *
     *  @param  value a boolean value 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchBooleanValue(boolean value, boolean match) {
        getAssembler().addBooleanTerm(getBooleanValueColumn(), match ? value : !value);
        return;
    }


    /**
     *  Clears the boolean value terms. 
     */

    @OSID @Override
    public void clearBooleanValueTerms() {
        getAssembler().clearTerms(getBooleanValueColumn());
        return;
    }


    /**
     *  Gets the boolean value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getBooleanValueTerms() {
        return (getAssembler().getBooleanTerms(getBooleanValueColumn()));
    }


    /**
     *  Gets the BooleanValue column name.
     *
     * @return the column name
     */

    protected String getBooleanValueColumn() {
        return ("boolean_value");
    }


    /**
     *  Adds a byte string match. Multiple byte arrays can be added to perform 
     *  a boolean <code> OR </code> among them. 
     *
     *  @param  value a byte value 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @param  partial <code> true </code> if for a partial match, <code> 
     *          false </code> for complete match 
     */

    @OSID @Override
    public void matchBytesValue(byte[] value, boolean match, boolean partial) {
        getAssembler().addBytesTerm(getBytesValueColumn(), value, match, partial);
        return;
    }


    /**
     *  Clears the bytes value terms. 
     */

    @OSID @Override
    public void clearBytesValueTerms() {
        getAssembler().clearTerms(getBytesValueColumn());
        return;
    }


    /**
     *  Gets the bytes value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BytesTerm[] getBytesValueTerms() {
        return (getAssembler().getBytesTerms(getBytesValueColumn()));
    }


    /**
     *  Gets the BytesValue column name.
     *
     * @return the column name
     */

    protected String getBytesValueColumn() {
        return ("bytes_value");
    }


    /**
     *  Adds a cardinal match within the given range inclusive. Multiple 
     *  ranges can be added to perform a boolean <code> OR </code> among them. 
     *
     *  @param  low start cardinal value 
     *  @param  high end cardinal value 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchCardinalValue(long low, long high, boolean match) {
        getAssembler().addCardinalRangeTerm(getCardinalValueColumn(), low, high, match);
        return;
    }


    /**
     *  Clears the cardinal value terms. 
     */

    @OSID @Override
    public void clearCardinalValueTerms() {
        getAssembler().clearTerms(getCardinalValueColumn());
        return;
    }


    /**
     *  Gets the cardinal value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getCardinalValueTerms() {
        return (getAssembler().getCardinalRangeTerms(getCardinalValueColumn()));
    }


    /**
     *  Gets the CardinalValue column name.
     *
     * @return the column name
     */

    protected String getCardinalValueColumn() {
        return ("cardinal_value");
    }


    /**
     *  Adds a coordinate match for coordinates inside the specified 
     *  coordinate. Multiple ranges can be added to perform a boolean <code> 
     *  OR </code> among them. 
     *
     *  @param  coordinate a coordinate value 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> coordinate </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCoordinateValue(org.osid.mapping.Coordinate coordinate, 
                                     boolean match) {
        getAssembler().addCoordinateTerm(getCoordinateValueColumn(), coordinate, match);
        return;
    }


    /**
     *  Clears the coordinate value terms. 
     */

    @OSID @Override
    public void clearCoordinateValueTerms() {
        getAssembler().clearTerms(getCoordinateValueColumn());
        return;
    }


    /**
     *  Gets the coordinate value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CoordinateTerm[] getCoordinateValueTerms() {
        return (getAssembler().getCoordinateTerms(getCoordinateValueColumn()));
    }


    /**
     *  Gets the CoordinateValue column name.
     *
     * @return the column name
     */

    protected String getCoordinateValueColumn() {
        return ("coordinate_value");
    }


    /**
     *  Adds a curency match within the given range inclusive. Multiple ranges 
     *  can be added to perform a boolean <code> OR </code> among them. 
     *
     *  @param  low start currency value 
     *  @param  high a currency value 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     *  @throws org.osid.NullArgumentException <code> low </code> or <code> 
     *          high </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchCurrencyValue(org.osid.financials.Currency low, 
                                   org.osid.financials.Currency high, 
                                   boolean match) {
        getAssembler().addCurrencyRangeTerm(getCurrencyValueColumn(), low, high, match);
        return;
    }


    /**
     *  Clears the currency value terms. 
     */

    @OSID @Override
    public void clearCurrencyValueTerms() {
        getAssembler().clearTerms(getCurrencyValueColumn());
        return;
    }


    /**
     *  Gets the currency value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyTerm[] getCurrencyValueTerms() {
        return (getAssembler().getCurrencyTerms(getCurrencyValueColumn()));
    }


    /**
     *  Gets the CurrencyValue column name.
     *
     * @return the column name
     */

    protected String getCurrencyValueColumn() {
        return ("currency_value");
    }


    /**
     *  Adds a <code> DateTime </code> range match within the given range 
     *  inclusive. Multiple ranges can be added to perform a boolean <code> OR 
     *  </code> among them. 
     *
     *  @param  low start datetime value 
     *  @param  high end datetime value 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     *  @throws org.osid.NullArgumentException <code> low </code> or <code> 
     *          high </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDateTimeValue(org.osid.calendaring.DateTime low, 
                                   org.osid.calendaring.DateTime high, 
                                   boolean match) {
        getAssembler().addDateTimeRangeTerm(getDateTimeValueColumn(), low, high, match);
        return;
    }


    /**
     *  Clears the date time value terms. 
     */

    @OSID @Override
    public void clearDateTimeValueTerms() {
        getAssembler().clearTerms(getDateTimeValueColumn());
        return;
    }


    /**
     *  Gets the date time value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getDateTimeValueTerms() {
        return (getAssembler().getDateTimeRangeTerms(getDateTimeValueColumn()));
    }


    /**
     *  Gets the DateTimeValue column name.
     *
     * @return the column name
     */

    protected String getDateTimeValueColumn() {
        return ("date_time_value");
    }


    /**
     *  Adds a decimal match within the given range inclusive. Multiple ranges 
     *  can be added to perform a boolean <code> OR </code> among them. 
     *
     *  @param  low start decimal value 
     *  @param  high end decimal value 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchDecimalValue(java.math.BigDecimal low, 
                                  java.math.BigDecimal high, boolean match) {
        getAssembler().addDecimalRangeTerm(getDecimalValueColumn(), low, high, match);
        return;
    }


    /**
     *  Clears the decimal value terms. 
     */

    @OSID @Override
    public void clearDecimalValueTerms() {
        getAssembler().clearTerms(getDecimalValueColumn());
        return;
    }


    /**
     *  Gets the decimal value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getDecimalValueTerms() {
        return (getAssembler().getDecimalRangeTerms(getDecimalValueColumn()));
    }


    /**
     *  Gets the DecimalValue column name.
     *
     * @return the column name
     */

    protected String getDecimalValueColumn() {
        return ("decimal_value");
    }


    /**
     *  Adds a <code> Distance </code> range match within the given range 
     *  inclusive. Multiple ranges can be added to perform a boolean <code> OR 
     *  </code> among them. 
     *
     *  @param  low start distance value 
     *  @param  high end distance value 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     *  @throws org.osid.NullArgumentException <code> low </code> or <code> 
     *          high </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDistanceValue(org.osid.mapping.Distance low, 
                                   org.osid.mapping.Distance high, 
                                   boolean match) {
        getAssembler().addDistanceRangeTerm(getDistanceValueColumn(), low, high, match);
        return;
    }


    /**
     *  Clears the distance value terms. 
     */

    @OSID @Override
    public void clearDistanceValueTerms() {
        getAssembler().clearTerms(getDistanceValueColumn());
        return;
    }


    /**
     *  Gets the distance value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DistanceTerm[] getDistanceValueTerms() {
        return (getAssembler().getDistanceTerms(getDistanceValueColumn()));
    }


    /**
     *  Gets the DistanceValue column name.
     *
     * @return the column name
     */

    protected String getDistanceValueColumn() {
        return ("distance_value");
    }


    /**
     *  Adds a <code> Duration </code> range match within the given range 
     *  inclusive. Multiple ranges can be added to perform a boolean <code> OR 
     *  </code> among them. 
     *
     *  @param  low start duration value 
     *  @param  high end duration value 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     *  @throws org.osid.NullArgumentException <code> low </code> or <code> 
     *          high </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDurationValue(org.osid.calendaring.Duration low, 
                                   org.osid.calendaring.Duration high, 
                                   boolean match) {
        getAssembler().addDurationRangeTerm(getDurationValueColumn(), low, high, match);
        return;
    }


    /**
     *  Clears the duration value terms. 
     */

    @OSID @Override
    public void clearDurationValueTerms() {
        getAssembler().clearTerms(getDurationValueColumn());
        return;
    }


    /**
     *  Gets the duration value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationTerm[] getDurationValueTerms() {
        return (getAssembler().getDurationTerms(getDurationValueColumn()));
    }


    /**
     *  Gets the DurationValue column name.
     *
     * @return the column name
     */

    protected String getDurationValueColumn() {
        return ("duration_value");
    }


    /**
     *  Adds a <code> Heading </code> range match within the given range 
     *  inclusive. Multiple ranges can be added to perform a boolean <code> OR 
     *  </code> among them. 
     *
     *  @param  low start heading value 
     *  @param  high end heading value 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     *  @throws org.osid.NullArgumentException <code> low </code> or <code> 
     *          high </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchHeadingValue(org.osid.mapping.Heading low, 
                                  org.osid.mapping.Heading high, boolean match) {
        getAssembler().addHeadingRangeTerm(getHeadingValueColumn(), low, high, match);
        return;
    }


    /**
     *  Clears the heading value terms. 
     */

    @OSID @Override
    public void clearHeadingValueTerms() {
        getAssembler().clearTerms(getHeadingValueColumn());
        return;
    }


    /**
     *  Gets the heading value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.HeadingTerm[] getHeadingValueTerms() {
        return (getAssembler().getHeadingTerms(getHeadingValueColumn()));
    }


    /**
     *  Gets the HeadingValue column name.
     *
     * @return the column name
     */

    protected String getHeadingValueColumn() {
        return ("heading_value");
    }


    /**
     *  Adds an <code> Id </code> to match. Multiple <code> Ids </code> can be 
     *  added to perform a boolean <code> OR </code> among them. 
     *
     *  @param  value an <code> Id </code> value 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> value </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchIdValue(org.osid.id.Id value, boolean match) {
        getAssembler().addIdTerm(getIdValueColumn(), value, match);
        return;
    }


    /**
     *  Clears the Id value terms. 
     */

    @OSID @Override
    public void clearIdValueTerms() {
        getAssembler().clearTerms(getIdValueColumn());
        return;
    }


    /**
     *  Gets the Id value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getIdValueTerms() {
        return (getAssembler().getIdTerms(getIdValueColumn()));
    }


    /**
     *  Gets the IdValue column name.
     *
     * @return the column name
     */

    protected String getIdValueColumn() {
        return ("id_value");
    }


    /**
     *  Adds an integer match within the given range inclusive. Multiple 
     *  ranges can be added to perform a boolean <code> OR </code> among them. 
     *
     *  @param  low start integer value 
     *  @param  high end integer value 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchIntegerValue(long low, long high, boolean match) {
        getAssembler().addIntegerRangeTerm(getIntegerValueColumn(), low, high, match);
        return;
    }


    /**
     *  Clears the integer value terms. 
     */

    @OSID @Override
    public void clearIntegerValueTerms() {
        getAssembler().clearTerms(getIntegerValueColumn());
        return;
    }


    /**
     *  Gets the integer value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IntegerRangeTerm[] getIntegerValueTerms() {
        return (getAssembler().getIntegerRangeTerms(getIntegerValueColumn()));
    }


    /**
     *  Gets the IntegerValue column name.
     *
     * @return the column name
     */

    protected String getIntegerValueColumn() {
        return ("integer_value");
    }


    /**
     *  Adds a spatial unit match within the given spatial unit inclusive. 
     *  Multiple ranges can be added to perform a boolean <code> OR </code> 
     *  among them. 
     *
     *  @param  value a spatial unit 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> value </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchSpatialUnitValue(org.osid.mapping.SpatialUnit value, 
                                      boolean match) {
        getAssembler().addSpatialUnitTerm(getSpatialUnitValueColumn(), value, match);
        return;
    }


    /**
     *  Clears the spatial unit value terms. 
     */

    @OSID @Override
    public void clearSpatialUnitValueTerms() {
        getAssembler().clearTerms(getSpatialUnitValueColumn());
        return;
    }


    /**
     *  Gets the spatial unit value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.SpatialUnitTerm[] getSpatialUnitValueTerms() {
        return (getAssembler().getSpatialUnitTerms(getSpatialUnitValueColumn()));
    }


    /**
     *  Gets the SpatialUnitValue column name.
     *
     * @return the column name
     */

    protected String getSpatialUnitValueColumn() {
        return ("spatial_unit_value");
    }


    /**
     *  Adds a speed match within the given range inclusive. Multiple ranges 
     *  can be added to perform a boolean <code> OR </code> among them. 
     *
     *  @param  low start speed value 
     *  @param  high end speed value 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     *  @throws org.osid.NullArgumentException <code> low </code> or <code> 
     *          high </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchSpeedValue(org.osid.mapping.Speed low, 
                                org.osid.mapping.Speed high, boolean match) {
        getAssembler().addSpeedRangeTerm(getSpeedValueColumn(), low, high, match);
        return;
    }


    /**
     *  Clears the speed value terms. 
     */

    @OSID @Override
    public void clearSpeedValueTerms() {
        getAssembler().clearTerms(getSpeedValueColumn());
        return;
    }


    /**
     *  Gets the speed value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.SpeedTerm[] getSpeedValueTerms() {
        return (getAssembler().getSpeedTerms(getSpeedValueColumn()));
    }


    /**
     *  Gets the SpeedValue column name.
     *
     * @return the column name
     */

    protected String getSpeedValueColumn() {
        return ("speed_value");
    }


    /**
     *  Adds a string match. Multiple strings can be added to perform a 
     *  boolean <code> OR </code> among them. 
     *
     *  @param  value string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> value is </code> not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> value </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchStringValue(String value, 
                                 org.osid.type.Type stringMatchType, 
                                 boolean match) {
        getAssembler().addStringTerm(getStringValueColumn(), value, stringMatchType, match);
        return;
    }


    /**
     *  Clears the string value terms. 
     */

    @OSID @Override
    public void clearStringValueTerms() {
        getAssembler().clearTerms(getStringValueColumn());
        return;
    }


    /**
     *  Gets the string value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getStringValueTerms() {
        return (getAssembler().getStringTerms(getStringValueColumn()));
    }


    /**
     *  Gets the StringValue column name.
     *
     * @return the column name
     */

    protected String getStringValueColumn() {
        return ("string_value");
    }


    /**
     *  Adds a time match within the given range inclusive. Multiple ranges 
     *  can be added to perform a boolean <code> OR </code> among them. 
     *
     *  @param  low start time value 
     *  @param  high end time value 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     *  @throws org.osid.NullArgumentException <code> low </code> or <code> 
     *          high </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchTimeValue(org.osid.calendaring.Time low, 
                               org.osid.calendaring.Time high, boolean match) {
        getAssembler().addTimeRangeTerm(getTimeValueColumn(), low, high, match);
        return;
    }


    /**
     *  Clears the time value terms. 
     */

    @OSID @Override
    public void clearTimeValueTerms() {
        getAssembler().clearTerms(getTimeValueColumn());
        return;
    }


    /**
     *  Gets the time value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.TimeTerm[] getTimeValueTerms() {
        return (getAssembler().getTimeTerms(getTimeValueColumn()));
    }


    /**
     *  Gets the TimeValue column name.
     *
     * @return the column name
     */

    protected String getTimeValueColumn() {
        return ("time_value");
    }


    /**
     *  Adds a <code> Type </code> match. Multiple types can be added to 
     *  perform a boolean <code> OR </code> among them. 
     *
     *  @param  value type to match 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> value </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchTypeValue(org.osid.type.Type value, boolean match) {
        getAssembler().addTypeTerm(getTypeValueColumn(), value, match);
        return;
    }


    /**
     *  Clears the type value terms. 
     */

    @OSID @Override
    public void clearTypeValueTerms() {
        getAssembler().clearTerms(getTypeValueColumn());
        return;
    }


    /**
     *  Gets the type value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getTypeValueTerms() {
        return (getAssembler().getTypeTerms(getTypeValueColumn()));
    }


    /**
     *  Gets the TypeValue column name.
     *
     * @return the column name
     */

    protected String getTypeValueColumn() {
        return ("type_value");
    }


    /**
     *  Adds a <code> Version </code> match within the given range inclusive. 
     *  Multiple queries can be added to perform a boolean <code> OR </code> 
     *  among them. 
     *
     *  @param  low start version value 
     *  @param  high end version value 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     *  @throws org.osid.NullArgumentException <code> low </code> or <code> 
     *          high </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchVersionValue(org.osid.installation.Version low, 
                                  org.osid.installation.Version high, 
                                  boolean match) {
        getAssembler().addVersionRangeTerm(getVersionValueColumn(), low, high, match);
        return;
    }


    /**
     *  Clears the version value terms. 
     */

    @OSID @Override
    public void clearVersionValueTerms() {
        getAssembler().clearTerms(getVersionValueColumn());
        return;
    }


    /**
     *  Gets the version value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.VersionRangeTerm[] getVersionValueTerms() {
        return (getAssembler().getVersionRangeTerms(getVersionValueColumn()));
    }


    /**
     *  Gets the VersionValue column name.
     *
     * @return the column name
     */

    protected String getVersionValueColumn() {
        return ("version_value");
    }


    /**
     *  Adds a <code> Type </code> to match on the type of object. Multiple 
     *  types can be added to perform a boolean <code> OR </code> among them. 
     *
     *  @param  objectType type to match 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> objectType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchObjectValueType(org.osid.type.Type objectType, 
                                     boolean match) {
        getAssembler().addTypeTerm(getObjectValueTypeColumn(), objectType, match);
        return;
    }


    /**
     *  Clears the object value type value terms. 
     */

    @OSID @Override
    public void clearObjectValueTypeTerms() {
        getAssembler().clearTerms(getObjectValueTypeColumn());
        return;
    }


    /**
     *  Gets the object value type query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getObjectValueTypeTerms() {
        return (getAssembler().getTypeTerms(getObjectValueTypeColumn()));
    }


    /**
     *  Gets the ObjectValueType column name.
     *
     * @return the column name
     */

    protected String getObjectValueTypeColumn() {
        return ("object_value_type");
    }


    /**
     *  Adds an object match. Multiple objects can be added to perform a 
     *  boolean <code> OR </code> among them. 
     *
     *  @param  object object to match 
     *  @param  objectType type of object 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> object </code> or <code> 
     *          objectType </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchObjectValue(java.lang.Object object, 
                                 org.osid.type.Type objectType, boolean match) {
        getAssembler().addObjectTerm(getObjectValueColumn(), object, objectType, match);
        return;
    }


    /**
     *  Clears the object value terms. 
     */

    @OSID @Override
    public void clearObjectValueTerms() {
        getAssembler().clearTerms(getObjectValueColumn());
        return;
    }


    /**
     *  Gets the object value query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.ObjectTerm[] getObjectValueTerms() {
        return (getAssembler().getObjectTerms(getObjectValueColumn()));
    }


    /**
     *  Gets the ObjectValue column name.
     *
     * @return the column name
     */

    protected String getObjectValueColumn() {
        return ("object_value");
    }


    /**
     *  Adds a parameter <code> Id </code> for this query. 
     *
     *  @param  parameterId a parameter <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> parameterId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchParameterId(org.osid.id.Id parameterId, boolean match) {
        getAssembler().addIdTerm(getParameterIdColumn(), parameterId, match);
        return;
    }


    /**
     *  Clears the parameter <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearParameterIdTerms() {
        getAssembler().clearTerms(getParameterIdColumn());
        return;
    }


    /**
     *  Gets the parameter <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getParameterIdTerms() {
        return (getAssembler().getIdTerms(getParameterIdColumn()));
    }


    /**
     *  Gets the ParameterId column name.
     *
     * @return the column name
     */

    protected String getParameterIdColumn() {
        return ("parameter_id");
    }


    /**
     *  Tests if a <code> ParameterQuery </code> is available. 
     *
     *  @return <code> true </code> if a parameter query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterQuery() {
        return (false);
    }


    /**
     *  Gets the query for a parameter. 
     *
     *  @return the parameter query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterQuery getParameterQuery() {
        throw new org.osid.UnimplementedException("supportsParameterQuery() is false");
    }


    /**
     *  Clears the parameter terms. 
     */

    @OSID @Override
    public void clearParameterTerms() {
        getAssembler().clearTerms(getParameterColumn());
        return;
    }


    /**
     *  Gets the parameter query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.configuration.ParameterQueryInspector[] getParameterTerms() {
        return (new org.osid.configuration.ParameterQueryInspector[0]);
    }


    /**
     *  Tests if a <code> ParameterSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a parameter search order interface is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterSearchOrder() {
        return (false);
    }


    /**
     *  Gets the parameter search order. 
     *
     *  @return the parameter search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterSearchOrder getParameterSearchOrder() {
        throw new org.osid.UnimplementedException("supportsParameterSearchOrder() is false");
    }


    /**
     *  Gets the Parameter column name.
     *
     * @return the column name
     */

    protected String getParameterColumn() {
        return ("parameter");
    }


    /**
     *  Sets the configuration <code> Id </code> for this query. 
     *
     *  @param  configurationId a configuration <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchConfigurationId(org.osid.id.Id configurationId, 
                                     boolean match) {
        getAssembler().addIdTerm(getConfigurationIdColumn(), configurationId, match);
        return;
    }


    /**
     *  Clears the configuration <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearConfigurationIdTerms() {
        getAssembler().clearTerms(getConfigurationIdColumn());
        return;
    }


    /**
     *  Gets the configuration <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getConfigurationIdTerms() {
        return (getAssembler().getIdTerms(getConfigurationIdColumn()));
    }


    /**
     *  Gets the ConfigurationId column name.
     *
     * @return the column name
     */

    protected String getConfigurationIdColumn() {
        return ("configuration_id");
    }


    /**
     *  Tests if a <code> ConfigurationQuery </code> is available. 
     *
     *  @return <code> true </code> if a configuration query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConfigurationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a configuration. 
     *
     *  @return the configuration query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConfigurationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationQuery getConfigurationQuery() {
        throw new org.osid.UnimplementedException("supportsConfigurationQuery() is false");
    }


    /**
     *  Clears the configuration <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearConfigurationTerms() {
        getAssembler().clearTerms(getConfigurationColumn());
        return;
    }


    /**
     *  Gets the configuration query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationQueryInspector[] getConfigurationTerms() {
        return (new org.osid.configuration.ConfigurationQueryInspector[0]);
    }


    /**
     *  Gets the Configuration column name.
     *
     * @return the column name
     */

    protected String getConfigurationColumn() {
        return ("configuration");
    }


    /**
     *  Tests if this value supports the given record
     *  <code>Type</code>.
     *
     *  @param  valueRecordType a value record type 
     *  @return <code>true</code> if the valueRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>valueRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type valueRecordType) {
        for (org.osid.configuration.records.ValueQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(valueRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  valueRecordType the value record type 
     *  @return the value query record 
     *  @throws org.osid.NullArgumentException
     *          <code>valueRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(valueRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.configuration.records.ValueQueryRecord getValueQueryRecord(org.osid.type.Type valueRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.configuration.records.ValueQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(valueRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(valueRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  valueRecordType the value record type 
     *  @return the value query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>valueRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(valueRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.configuration.records.ValueQueryInspectorRecord getValueQueryInspectorRecord(org.osid.type.Type valueRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.configuration.records.ValueQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(valueRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(valueRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param valueRecordType the value record type
     *  @return the value search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>valueRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(valueRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.configuration.records.ValueSearchOrderRecord getValueSearchOrderRecord(org.osid.type.Type valueRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.configuration.records.ValueSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(valueRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(valueRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this value. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param valueQueryRecord the value query record
     *  @param valueQueryInspectorRecord the value query inspector
     *         record
     *  @param valueSearchOrderRecord the value search order record
     *  @param valueRecordType value record type
     *  @throws org.osid.NullArgumentException
     *          <code>valueQueryRecord</code>,
     *          <code>valueQueryInspectorRecord</code>,
     *          <code>valueSearchOrderRecord</code> or
     *          <code>valueRecordTypevalue</code> is
     *          <code>null</code>
     */
            
    protected void addValueRecords(org.osid.configuration.records.ValueQueryRecord valueQueryRecord, 
                                   org.osid.configuration.records.ValueQueryInspectorRecord valueQueryInspectorRecord, 
                                   org.osid.configuration.records.ValueSearchOrderRecord valueSearchOrderRecord, 
                                   org.osid.type.Type valueRecordType) {

        addRecordType(valueRecordType);

        nullarg(valueQueryRecord, "value query record");
        nullarg(valueQueryInspectorRecord, "value query inspector record");
        nullarg(valueSearchOrderRecord, "value search odrer record");

        this.queryRecords.add(valueQueryRecord);
        this.queryInspectorRecords.add(valueQueryInspectorRecord);
        this.searchOrderRecords.add(valueSearchOrderRecord);
        
        return;
    }
}
