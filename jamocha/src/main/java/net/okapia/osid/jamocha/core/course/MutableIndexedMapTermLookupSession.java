//
// MutableIndexedMapTermLookupSession
//
//    Implements a Term lookup service backed by a collection of
//    terms indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course;


/**
 *  Implements a Term lookup service backed by a collection of
 *  terms. The terms are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some terms may be compatible
 *  with more types than are indicated through these term
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of terms can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapTermLookupSession
    extends net.okapia.osid.jamocha.core.course.spi.AbstractIndexedMapTermLookupSession
    implements org.osid.course.TermLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapTermLookupSession} with no terms.
     *
     *  @param courseCatalog the course catalog
     *  @throws org.osid.NullArgumentException {@code courseCatalog}
     *          is {@code null}
     */

      public MutableIndexedMapTermLookupSession(org.osid.course.CourseCatalog courseCatalog) {
        setCourseCatalog(courseCatalog);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapTermLookupSession} with a
     *  single term.
     *  
     *  @param courseCatalog the course catalog
     *  @param  term a single term
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code term} is {@code null}
     */

    public MutableIndexedMapTermLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.course.Term term) {
        this(courseCatalog);
        putTerm(term);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapTermLookupSession} using an
     *  array of terms.
     *
     *  @param courseCatalog the course catalog
     *  @param  terms an array of terms
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code terms} is {@code null}
     */

    public MutableIndexedMapTermLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.course.Term[] terms) {
        this(courseCatalog);
        putTerms(terms);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapTermLookupSession} using a
     *  collection of terms.
     *
     *  @param courseCatalog the course catalog
     *  @param  terms a collection of terms
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code terms} is {@code null}
     */

    public MutableIndexedMapTermLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  java.util.Collection<? extends org.osid.course.Term> terms) {

        this(courseCatalog);
        putTerms(terms);
        return;
    }
    

    /**
     *  Makes a {@code Term} available in this session.
     *
     *  @param  term a term
     *  @throws org.osid.NullArgumentException {@code term{@code  is
     *          {@code null}
     */

    @Override
    public void putTerm(org.osid.course.Term term) {
        super.putTerm(term);
        return;
    }


    /**
     *  Makes an array of terms available in this session.
     *
     *  @param  terms an array of terms
     *  @throws org.osid.NullArgumentException {@code terms{@code 
     *          is {@code null}
     */

    @Override
    public void putTerms(org.osid.course.Term[] terms) {
        super.putTerms(terms);
        return;
    }


    /**
     *  Makes collection of terms available in this session.
     *
     *  @param  terms a collection of terms
     *  @throws org.osid.NullArgumentException {@code term{@code  is
     *          {@code null}
     */

    @Override
    public void putTerms(java.util.Collection<? extends org.osid.course.Term> terms) {
        super.putTerms(terms);
        return;
    }


    /**
     *  Removes a Term from this session.
     *
     *  @param termId the {@code Id} of the term
     *  @throws org.osid.NullArgumentException {@code termId{@code  is
     *          {@code null}
     */

    @Override
    public void removeTerm(org.osid.id.Id termId) {
        super.removeTerm(termId);
        return;
    }    
}
