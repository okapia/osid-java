//
// AbstractAuthorizationEnablerQuery.java
//
//     A template for making an AuthorizationEnabler Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authorization.rules.authorizationenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for authorization enablers.
 */

public abstract class AbstractAuthorizationEnablerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.authorization.rules.AuthorizationEnablerQuery {

    private final java.util.Collection<org.osid.authorization.rules.records.AuthorizationEnablerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches enablers mapped to an authorization. 
     *
     *  @param  authorizationId the authorization <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> authorizationId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledAuthorizationId(org.osid.id.Id authorizationId, 
                                          boolean match) {
        return;
    }


    /**
     *  Clears the authorization <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledAuthorizationIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AuthorizationQuery </code> is available. 
     *
     *  @return <code> true </code> if an authorization query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledAuthorizationQuery() {
        return (false);
    }


    /**
     *  Gets the query for an authorization. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the authorization query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledAuthorizationQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationQuery getRuledAuthorizationQuery() {
        throw new org.osid.UnimplementedException("supportsRuledAuthorizationQuery() is false");
    }


    /**
     *  Matches enablers mapped to any authorization. 
     *
     *  @param  match <code> true </code> for enablers mapped to any 
     *          authorization, <code> false </code> to match enablers mapped 
     *          to no authorizations 
     */

    @OSID @Override
    public void matchAnyRuledAuthorization(boolean match) {
        return;
    }


    /**
     *  Clears the authorization query terms. 
     */

    @OSID @Override
    public void clearRuledAuthorizationTerms() {
        return;
    }


    /**
     *  Matches enablers mapped to the vault. 
     *
     *  @param  vaultId the vault <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchVaultId(org.osid.id.Id vaultId, boolean match) {
        return;
    }


    /**
     *  Clears the vault <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearVaultIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> VaultQuery </code> is available. 
     *
     *  @return <code> true </code> if a vault query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVaultQuery() {
        return (false);
    }


    /**
     *  Gets the query for a vault. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the vault query 
     *  @throws org.osid.UnimplementedException <code> supportsVaultQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.VaultQuery getVaultQuery() {
        throw new org.osid.UnimplementedException("supportsVaultQuery() is false");
    }


    /**
     *  Clears the vault query terms. 
     */

    @OSID @Override
    public void clearVaultTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given authorization enabler query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an authorization enabler implementing the requested record.
     *
     *  @param authorizationEnablerRecordType an authorization enabler record type
     *  @return the authorization enabler query record
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(authorizationEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.authorization.rules.records.AuthorizationEnablerQueryRecord getAuthorizationEnablerQueryRecord(org.osid.type.Type authorizationEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authorization.rules.records.AuthorizationEnablerQueryRecord record : this.records) {
            if (record.implementsRecordType(authorizationEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(authorizationEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this authorization enabler query. 
     *
     *  @param authorizationEnablerQueryRecord authorization enabler query record
     *  @param authorizationEnablerRecordType authorizationEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAuthorizationEnablerQueryRecord(org.osid.authorization.rules.records.AuthorizationEnablerQueryRecord authorizationEnablerQueryRecord, 
                                          org.osid.type.Type authorizationEnablerRecordType) {

        addRecordType(authorizationEnablerRecordType);
        nullarg(authorizationEnablerQueryRecord, "authorization enabler query record");
        this.records.add(authorizationEnablerQueryRecord);        
        return;
    }
}
