//
// InvariantNodeDictionaryHierarchySession.java
//
//     Defines a Dictionary hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.dictionary;


/**
 *  Defines a repository hierarchy session for delivering a hierarchy of
 *  repositories using the DictionaryNode interface.
 */

public final class InvariantNodeDictionaryHierarchySession
    extends net.okapia.osid.jamocha.core.dictionary.spi.AbstractNodeDictionaryHierarchySession
    implements org.osid.dictionary.DictionaryHierarchySession {


    /**
     *  Constructs a new
     *  <code>InvariantNodeDictionaryHierarchySession</code> with no
     *  nodes.
     *
     *  @param hierarchy the hierarchy for this session
     *  @throws org.osid.NullArgumentException <code>hierarchy</code> 
     *          is <code>null</code>
     */

    public InvariantNodeDictionaryHierarchySession(org.osid.hierarchy.Hierarchy hierarchy) {
        setHierarchy(hierarchy);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantNodeDictionaryHierarchySession</code> from a
     *  single root node also used to identify the hierarchy.
     *
     *  @param root a root node
     *  @throws org.osid.NullArgumentException <code>root</code> 
     *          is <code>null</code>
     */

    public InvariantNodeDictionaryHierarchySession(org.osid.dictionary.DictionaryNode root) {
        setHierarchy(new net.okapia.osid.jamocha.builder.hierarchy.hierarchy.HierarchyBuilder()
                     .id(root.getId())
                     .displayName(root.getDictionary().getDisplayName())
                     .description(root.getDictionary().getDescription())
                     .build());

        addRootDictionary(root);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantNodeDictionaryHierarchySession</code> using a
     *  single node as the root.
     *
     *  @param hierarchy the hierarchy for this session
     *  @param root a root node
     *  @throws org.osid.NullArgumentException <code>hierarchy</code>
     *          or <code>root</code> is <code>null</code>
     */

    public InvariantNodeDictionaryHierarchySession(org.osid.hierarchy.Hierarchy hierarchy, 
                                                   org.osid.dictionary.DictionaryNode root) {
        setHierarchy(hierarchy);
        addRootDictionary(root);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantNodeDictionaryHierarchySession</code> using a
     *  collection of nodes as the root.
     *
     *  @param hierarchy the hierarchy for this session
     *  @param roots a collection of root roots
     *  @throws org.osid.NullArgumentException <code>hierarchy</code>
     *          or <code>roots</code> is <code>null</code>
     */

    public InvariantNodeDictionaryHierarchySession(org.osid.hierarchy.Hierarchy hierarchy, 
                                                   java.util.Collection<org.osid.dictionary.DictionaryNode> roots) {
        setHierarchy(hierarchy);
        addRootDictionaries(roots);
        return;
    }
}
