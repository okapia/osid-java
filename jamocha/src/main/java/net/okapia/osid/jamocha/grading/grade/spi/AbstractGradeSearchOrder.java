//
// AbstractGradeSearchOdrer.java
//
//     Defines a GradeSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.grade.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code GradeSearchOrder}.
 */

public abstract class AbstractGradeSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectSearchOrder
    implements org.osid.grading.GradeSearchOrder {

    private final java.util.Collection<org.osid.grading.records.GradeSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specified a preference for ordering results by the grade system. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByGradeSystem(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a <code> GradeSystemSearchOrder </code> interface is 
     *  available for grade systems. 
     *
     *  @return <code> true </code> if a grade system search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSystemSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a grade system. 
     *
     *  @return the grade system search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemSearchOrder getGradeSystemSearchOrder() {
        throw new org.osid.UnimplementedException("supportsGradeSystemSearchOrder() is false");
    }


    /**
     *  Specified a preference for ordering results by start of the input 
     *  score range. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByInputScoreStartRange(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by end of the input score 
     *  range. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByInputScoreEndRange(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the output score. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByOutputScore(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  gradeRecordType a grade record type 
     *  @return {@code true} if the gradeRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code gradeRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type gradeRecordType) {
        for (org.osid.grading.records.GradeSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(gradeRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  gradeRecordType the grade record type 
     *  @return the grade search order record
     *  @throws org.osid.NullArgumentException
     *          {@code gradeRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(gradeRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.grading.records.GradeSearchOrderRecord getGradeSearchOrderRecord(org.osid.type.Type gradeRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.grading.records.GradeSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(gradeRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradeRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this grade. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param gradeRecord the grade search odrer record
     *  @param gradeRecordType grade record type
     *  @throws org.osid.NullArgumentException
     *          {@code gradeRecord} or
     *          {@code gradeRecordTypegrade} is
     *          {@code null}
     */
            
    protected void addGradeRecord(org.osid.grading.records.GradeSearchOrderRecord gradeSearchOrderRecord, 
                                     org.osid.type.Type gradeRecordType) {

        addRecordType(gradeRecordType);
        this.records.add(gradeSearchOrderRecord);
        
        return;
    }
}
