//
// AbstractQueryVoteLookupSession.java
//
//    An inline adapter that maps a VoteLookupSession to
//    a VoteQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.voting.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a VoteLookupSession to
 *  a VoteQuerySession.
 */

public abstract class AbstractQueryVoteLookupSession
    extends net.okapia.osid.jamocha.voting.spi.AbstractVoteLookupSession
    implements org.osid.voting.VoteLookupSession {

    private boolean effectiveonly = false;
    private final org.osid.voting.VoteQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryVoteLookupSession.
     *
     *  @param querySession the underlying vote query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryVoteLookupSession(org.osid.voting.VoteQuerySession querySession) {
        nullarg(querySession, "vote query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Polls</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Polls Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getPollsId() {
        return (this.session.getPollsId());
    }


    /**
     *  Gets the <code>Polls</code> associated with this 
     *  session.
     *
     *  @return the <code>Polls</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Polls getPolls()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getPolls());
    }


    /**
     *  Tests if this user can perform <code>Vote</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupVotes() {
        return (this.session.canSearchVotes());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include votes in pollses which are children
     *  of this polls in the polls hierarchy.
     */

    @OSID @Override
    public void useFederatedPollsView() {
        this.session.useFederatedPollsView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this polls only.
     */

    @OSID @Override
    public void useIsolatedPollsView() {
        this.session.useIsolatedPollsView();
        return;
    }
    

    /**
     *  Only votes whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveVoteView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All votes of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveVoteView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Vote</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Vote</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Vote</code> and
     *  retained for compatibility.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and
     *  those currently expired are returned.
     *
     *  @param  voteId <code>Id</code> of the
     *          <code>Vote</code>
     *  @return the vote
     *  @throws org.osid.NotFoundException <code>voteId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>voteId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Vote getVote(org.osid.id.Id voteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.VoteQuery query = getQuery();
        query.matchId(voteId, true);
        org.osid.voting.VoteList votes = this.session.getVotesByQuery(query);
        if (votes.hasNext()) {
            return (votes.getNextVote());
        } 
        
        throw new org.osid.NotFoundException(voteId + " not found");
    }


    /**
     *  Gets a <code>VoteList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  votes specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Votes</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, votes are returned that are currently effective.
     *  In any effective mode, effective votes and those currently expired
     *  are returned.
     *
     *  @param  voteIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Vote</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>voteIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesByIds(org.osid.id.IdList voteIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.VoteQuery query = getQuery();

        try (org.osid.id.IdList ids = voteIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getVotesByQuery(query));
    }


    /**
     *  Gets a <code>VoteList</code> corresponding to the given
     *  vote genus <code>Type</code> which does not include
     *  votes of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  votes or an error results. Otherwise, the returned list
     *  may contain only those votes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and those
     *  currently expired are returned.
     *
     *  @param  voteGenusType a vote genus type 
     *  @return the returned <code>Vote</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>voteGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesByGenusType(org.osid.type.Type voteGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.VoteQuery query = getQuery();
        query.matchGenusType(voteGenusType, true);
        return (this.session.getVotesByQuery(query));
    }


    /**
     *  Gets a <code>VoteList</code> corresponding to the given
     *  vote genus <code>Type</code> and include any additional
     *  votes with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  votes or an error results. Otherwise, the returned list
     *  may contain only those votes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and
     *  those currently expired are returned.
     *
     *  @param  voteGenusType a vote genus type 
     *  @return the returned <code>Vote</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>voteGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesByParentGenusType(org.osid.type.Type voteGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.VoteQuery query = getQuery();
        query.matchParentGenusType(voteGenusType, true);
        return (this.session.getVotesByQuery(query));
    }


    /**
     *  Gets a <code>VoteList</code> containing the given
     *  vote record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  votes or an error results. Otherwise, the returned list
     *  may contain only those votes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and
     *  those currently expired are returned.
     *
     *  @param  voteRecordType a vote record type 
     *  @return the returned <code>Vote</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>voteRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesByRecordType(org.osid.type.Type voteRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.VoteQuery query = getQuery();
        query.matchRecordType(voteRecordType, true);
        return (this.session.getVotesByQuery(query));
    }


    /**
     *  Gets a <code>VoteList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  votes or an error results. Otherwise, the returned list
     *  may contain only those votes that are accessible
     *  through this session.
     *  
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and
     *  those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Vote</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.voting.VoteList getVotesOnDate(org.osid.calendaring.DateTime from, 
                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.VoteQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getVotesByQuery(query));
    }
        

    /**
     *  Gets a list of votes corresponding to a voter
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  votes or an error results. Otherwise, the returned list
     *  may contain only those votes that are accessible
     *  through this session.
     *
     *  In effective mode, votes are returned that are
     *  currently effective.  In any effective mode, effective
     *  votes and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the voter
     *  @return the returned <code>VoteList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.voting.VoteList getVotesForVoter(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.voting.VoteQuery query = getQuery();
        query.matchVoterId(resourceId, true);
        return (this.session.getVotesByQuery(query));
    }


    /**
     *  Gets a list of votes corresponding to a voter
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  votes or an error results. Otherwise, the returned list
     *  may contain only those votes that are accessible
     *  through this session.
     *
     *  In effective mode, votes are returned that are
     *  currently effective.  In any effective mode, effective
     *  votes and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the voter
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>VoteList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesForVoterOnDate(org.osid.id.Id resourceId,
                                                           org.osid.calendaring.DateTime from,
                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.VoteQuery query = getQuery();
        query.matchVoterId(resourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getVotesByQuery(query));
    }


    /**
     *  Gets a list of votes corresponding to a candidate
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  votes or an error results. Otherwise, the returned list
     *  may contain only those votes that are accessible
     *  through this session.
     *
     *  In effective mode, votes are returned that are
     *  currently effective.  In any effective mode, effective
     *  votes and those currently expired are returned.
     *
     *  @param  candidateId the <code>Id</code> of the candidate
     *  @return the returned <code>VoteList</code>
     *  @throws org.osid.NullArgumentException <code>candidateId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.voting.VoteList getVotesForCandidate(org.osid.id.Id candidateId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.voting.VoteQuery query = getQuery();
        query.matchCandidateId(candidateId, true);
        return (this.session.getVotesByQuery(query));
    }


    /**
     *  Gets a list of votes corresponding to a candidate
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  votes or an error results. Otherwise, the returned list
     *  may contain only those votes that are accessible
     *  through this session.
     *
     *  In effective mode, votes are returned that are
     *  currently effective.  In any effective mode, effective
     *  votes and those currently expired are returned.
     *
     *  @param  candidateId the <code>Id</code> of the candidate
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>VoteList</code>
     *  @throws org.osid.NullArgumentException <code>candidateId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesForCandidateOnDate(org.osid.id.Id candidateId,
                                                               org.osid.calendaring.DateTime from,
                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.VoteQuery query = getQuery();
        query.matchCandidateId(candidateId, true);
        query.matchDate(from, to, true);
        return (this.session.getVotesByQuery(query));
    }


    /**
     *  Gets a list of votes corresponding to voter and candidate
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  votes or an error results. Otherwise, the returned list
     *  may contain only those votes that are accessible
     *  through this session.
     *
     *  In effective mode, votes are returned that are
     *  currently effective.  In any effective mode, effective
     *  votes and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the voter
     *  @param  candidateId the <code>Id</code> of the candidate
     *  @return the returned <code>VoteList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>candidateId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesForVoterAndCandidate(org.osid.id.Id resourceId,
                                                                 org.osid.id.Id candidateId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.VoteQuery query = getQuery();
        query.matchVoterId(resourceId, true);
        query.matchCandidateId(candidateId, true);
        return (this.session.getVotesByQuery(query));
    }


    /**
     *  Gets a list of votes corresponding to voter and candidate
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  votes or an error results. Otherwise, the returned list
     *  may contain only those votes that are accessible
     *  through this session.
     *
     *  In effective mode, votes are returned that are
     *  currently effective.  In any effective mode, effective
     *  votes and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the voter
     *  @param  candidateId the <code>Id</code> of the candidate
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>VoteList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>candidateId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesForVoterAndCandidateOnDate(org.osid.id.Id resourceId,
                                                                       org.osid.id.Id candidateId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.voting.VoteQuery query = getQuery();
        query.matchVoterId(resourceId, true);
        query.matchCandidateId(candidateId, true);
        query.matchDate(from, to, true);
        return (this.session.getVotesByQuery(query));
    }


    /**
     *  Gets a list of votes corresponding to a race
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  votes or an error results. Otherwise, the returned list
     *  may contain only those votes that are accessible
     *  through this session.
     *
     *  In effective mode, votes are returned that are
     *  currently effective.  In any effective mode, effective
     *  votes and those currently expired are returned.
     *
     *  @param  raceId the <code>Id</code> of the race
     *  @return the returned <code>VoteList</code>
     *  @throws org.osid.NullArgumentException <code>raceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.voting.VoteList getVotesForRace(org.osid.id.Id raceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.voting.VoteQuery query = getQuery();
        if (query.supportsCandidateQuery()) {
            query.getCandidateQuery().matchRaceId(raceId, true);
            return (this.session.getVotesByQuery(query));
        }
        
        return (super.getVotesForRace(raceId));
    }


    /**
     *  Gets a list of votes corresponding to a race
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  votes or an error results. Otherwise, the returned list
     *  may contain only those votes that are accessible
     *  through this session.
     *
     *  In effective mode, votes are returned that are
     *  currently effective.  In any effective mode, effective
     *  votes and those currently expired are returned.
     *
     *  @param  raceId the <code>Id</code> of the race
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>VoteList</code>
     *  @throws org.osid.NullArgumentException <code>raceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesForRaceOnDate(org.osid.id.Id raceId,
                                                               org.osid.calendaring.DateTime from,
                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {


        org.osid.voting.VoteQuery query = getQuery();
        if (query.supportsCandidateQuery()) {
            query.getCandidateQuery().matchRaceId(raceId, true);
            query.matchDate(from, to, true);
            return (this.session.getVotesByQuery(query));
        }
        
        return (super.getVotesForRace(raceId));
    }


    /**
     *  Gets a list of votes corresponding to voter and race
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  votes or an error results. Otherwise, the returned list
     *  may contain only those votes that are accessible
     *  through this session.
     *
     *  In effective mode, votes are returned that are
     *  currently effective.  In any effective mode, effective
     *  votes and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the voter
     *  @param  raceId the <code>Id</code> of the race
     *  @return the returned <code>VoteList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>raceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesForVoterAndRace(org.osid.id.Id resourceId,
                                                                 org.osid.id.Id raceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.VoteQuery query = getQuery();
        if (query.supportsCandidateQuery()) {
            query.getCandidateQuery().matchRaceId(raceId, true);
            query.matchVoterId(resourceId, true);
            return (this.session.getVotesByQuery(query));
        }
        
        return (super.getVotesForRace(raceId));
    }


    /**
     *  Gets a list of votes corresponding to voter and race
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  votes or an error results. Otherwise, the returned list
     *  may contain only those votes that are accessible
     *  through this session.
     *
     *  In effective mode, votes are returned that are
     *  currently effective.  In any effective mode, effective
     *  votes and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the voter
     *  @param  raceId the <code>Id</code> of the race
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>VoteList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>raceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesForVoterAndRaceOnDate(org.osid.id.Id resourceId,
                                                                  org.osid.id.Id raceId,
                                                                  org.osid.calendaring.DateTime from,
                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.voting.VoteQuery query = getQuery();
        if (query.supportsCandidateQuery()) {
            query.getCandidateQuery().matchRaceId(raceId, true);
            query.matchVoterId(resourceId, true);
            query.matchDate(from, to, true);
            return (this.session.getVotesByQuery(query));
        }

        return (super.getVotesForRace(raceId));
    }


    /**
     *  Gets all <code>Votes</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  votes or an error results. Otherwise, the returned list
     *  may contain only those votes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Votes</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotes()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.VoteQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getVotesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.voting.VoteQuery getQuery() {
        org.osid.voting.VoteQuery query = this.session.getVoteQuery();
        
        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }

        return (query);
    }
}
