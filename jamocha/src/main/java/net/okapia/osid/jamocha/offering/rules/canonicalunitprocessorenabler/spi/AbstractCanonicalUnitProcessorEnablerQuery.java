//
// AbstractCanonicalUnitProcessorEnablerQuery.java
//
//     A template for making a CanonicalUnitProcessorEnabler Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.rules.canonicalunitprocessorenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for canonical unit processor enablers.
 */

public abstract class AbstractCanonicalUnitProcessorEnablerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.offering.rules.CanonicalUnitProcessorEnablerQuery {

    private final java.util.Collection<org.osid.offering.rules.records.CanonicalUnitProcessorEnablerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches enablers mapped to the canonical unit processor. 
     *
     *  @param  canonicalUnitProcessorId the canonical unit <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> canonicalUnitProcessorId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledCanonicalUnitProcessorId(org.osid.id.Id canonicalUnitProcessorId, 
                                                   boolean match) {
        return;
    }


    /**
     *  Clears the canonical unit processor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledCanonicalUnitProcessorIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CanonicalUnitProcessorQuery </code> is available. 
     *
     *  @return <code> true </code> if a canonical unit processor query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledCanonicalUnitProcessorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a canonical unit processor. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the canonical unit processor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledCanonicalUnitProcessorQuery() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorQuery getRuledCanonicalUnitProcessorQuery() {
        throw new org.osid.UnimplementedException("supportsRuledCanonicalUnitProcessorQuery() is false");
    }


    /**
     *  Matches enablers mapped to any canonical unit processor. 
     *
     *  @param  match <code> true </code> for enablers mapped to any canonical 
     *          unit processor, <code> false </code> to match enablers mapped 
     *          to no canonical unit processors 
     */

    @OSID @Override
    public void matchAnyRuledCanonicalUnitProcessor(boolean match) {
        return;
    }


    /**
     *  Clears the canonical unit processor query terms. 
     */

    @OSID @Override
    public void clearRuledCanonicalUnitProcessorTerms() {
        return;
    }


    /**
     *  Matches enablers mapped to the catalogue. 
     *
     *  @param  catalogueId the catalogue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCatalogueId(org.osid.id.Id catalogueId, boolean match) {
        return;
    }


    /**
     *  Clears the catalogue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCatalogueIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CatalogueQuery </code> is available. 
     *
     *  @return <code> true </code> if a catalogue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a catalogue. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the catalogue query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogueQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CatalogueQuery getCatalogueQuery() {
        throw new org.osid.UnimplementedException("supportsCatalogueQuery() is false");
    }


    /**
     *  Clears the catalogue query terms. 
     */

    @OSID @Override
    public void clearCatalogueTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given canonical unit processor enabler query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a canonical unit processor enabler implementing the requested record.
     *
     *  @param canonicalUnitProcessorEnablerRecordType a canonical unit processor enabler record type
     *  @return the canonical unit processor enabler query record
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(canonicalUnitProcessorEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.rules.records.CanonicalUnitProcessorEnablerQueryRecord getCanonicalUnitProcessorEnablerQueryRecord(org.osid.type.Type canonicalUnitProcessorEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.rules.records.CanonicalUnitProcessorEnablerQueryRecord record : this.records) {
            if (record.implementsRecordType(canonicalUnitProcessorEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(canonicalUnitProcessorEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this canonical unit processor enabler query. 
     *
     *  @param canonicalUnitProcessorEnablerQueryRecord canonical unit processor enabler query record
     *  @param canonicalUnitProcessorEnablerRecordType canonicalUnitProcessorEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCanonicalUnitProcessorEnablerQueryRecord(org.osid.offering.rules.records.CanonicalUnitProcessorEnablerQueryRecord canonicalUnitProcessorEnablerQueryRecord, 
                                          org.osid.type.Type canonicalUnitProcessorEnablerRecordType) {

        addRecordType(canonicalUnitProcessorEnablerRecordType);
        nullarg(canonicalUnitProcessorEnablerQueryRecord, "canonical unit processor enabler query record");
        this.records.add(canonicalUnitProcessorEnablerQueryRecord);        
        return;
    }
}
