//
// AbstractIndexedMapAntimatroidLookupSession.java
//
//    A simple framework for providing an Antimatroid lookup service
//    backed by a fixed collection of antimatroids with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.sequencing.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an Antimatroid lookup service backed by a
 *  fixed collection of antimatroids. The antimatroids are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some antimatroids may be compatible
 *  with more types than are indicated through these antimatroid
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Antimatroids</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapAntimatroidLookupSession
    extends AbstractMapAntimatroidLookupSession
    implements org.osid.sequencing.AntimatroidLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.sequencing.Antimatroid> antimatroidsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.sequencing.Antimatroid>());
    private final MultiMap<org.osid.type.Type, org.osid.sequencing.Antimatroid> antimatroidsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.sequencing.Antimatroid>());


    /**
     *  Makes an <code>Antimatroid</code> available in this session.
     *
     *  @param  antimatroid an antimatroid
     *  @throws org.osid.NullArgumentException <code>antimatroid<code> is
     *          <code>null</code>
     */

    @Override
    protected void putAntimatroid(org.osid.sequencing.Antimatroid antimatroid) {
        super.putAntimatroid(antimatroid);

        this.antimatroidsByGenus.put(antimatroid.getGenusType(), antimatroid);
        
        try (org.osid.type.TypeList types = antimatroid.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.antimatroidsByRecord.put(types.getNextType(), antimatroid);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }

    
    /**
     *  Makes an array of antimatroids available in this session.
     *
     *  @param  antimatroids an array of antimatroids
     *  @throws org.osid.NullArgumentException <code>antimatroids<code>
     *          is <code>null</code>
     */

    @Override
    protected void putAntimatroids(org.osid.sequencing.Antimatroid[] antimatroids) {
        for (org.osid.sequencing.Antimatroid antimatroid : antimatroids) {
            putAntimatroid(antimatroid);
        }

        return;
    }


    /**
     *  Makes a collection of antimatroids available in this session.
     *
     *  @param  antimatroids a collection of antimatroids
     *  @throws org.osid.NullArgumentException <code>antimatroids<code>
     *          is <code>null</code>
     */

    @Override
    protected void putAntimatroids(java.util.Collection<? extends org.osid.sequencing.Antimatroid> antimatroids) {
        for (org.osid.sequencing.Antimatroid antimatroid : antimatroids) {
            putAntimatroid(antimatroid);
        }

        return;
    }


    /**
     *  Removes an antimatroid from this session.
     *
     *  @param antimatroidId the <code>Id</code> of the antimatroid
     *  @throws org.osid.NullArgumentException <code>antimatroidId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeAntimatroid(org.osid.id.Id antimatroidId) {
        org.osid.sequencing.Antimatroid antimatroid;
        try {
            antimatroid = getAntimatroid(antimatroidId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.antimatroidsByGenus.remove(antimatroid.getGenusType());

        try (org.osid.type.TypeList types = antimatroid.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.antimatroidsByRecord.remove(types.getNextType(), antimatroid);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeAntimatroid(antimatroidId);
        return;
    }


    /**
     *  Gets an <code>AntimatroidList</code> corresponding to the given
     *  antimatroid genus <code>Type</code> which does not include
     *  antimatroids of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known antimatroids or an error results. Otherwise,
     *  the returned list may contain only those antimatroids that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  antimatroidGenusType an antimatroid genus type 
     *  @return the returned <code>Antimatroid</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>antimatroidGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidList getAntimatroidsByGenusType(org.osid.type.Type antimatroidGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.sequencing.antimatroid.ArrayAntimatroidList(this.antimatroidsByGenus.get(antimatroidGenusType)));
    }


    /**
     *  Gets an <code>AntimatroidList</code> containing the given
     *  antimatroid record <code>Type</code>. In plenary mode, the
     *  returned list contains all known antimatroids or an error
     *  results. Otherwise, the returned list may contain only those
     *  antimatroids that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  antimatroidRecordType an antimatroid record type 
     *  @return the returned <code>antimatroid</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>antimatroidRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidList getAntimatroidsByRecordType(org.osid.type.Type antimatroidRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.sequencing.antimatroid.ArrayAntimatroidList(this.antimatroidsByRecord.get(antimatroidRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.antimatroidsByGenus.clear();
        this.antimatroidsByRecord.clear();

        super.close();

        return;
    }
}
