//
// MutableMapProxyActionGroupLookupSession
//
//    Implements an ActionGroup lookup service backed by a collection of
//    actionGroups that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control;


/**
 *  Implements an ActionGroup lookup service backed by a collection of
 *  actionGroups. The actionGroups are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of action groups can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyActionGroupLookupSession
    extends net.okapia.osid.jamocha.core.control.spi.AbstractMapActionGroupLookupSession
    implements org.osid.control.ActionGroupLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyActionGroupLookupSession}
     *  with no action groups.
     *
     *  @param system the system
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyActionGroupLookupSession(org.osid.control.System system,
                                                  org.osid.proxy.Proxy proxy) {
        setSystem(system);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyActionGroupLookupSession} with a
     *  single action group.
     *
     *  @param system the system
     *  @param actionGroup an action group
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code system},
     *          {@code actionGroup}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyActionGroupLookupSession(org.osid.control.System system,
                                                org.osid.control.ActionGroup actionGroup, org.osid.proxy.Proxy proxy) {
        this(system, proxy);
        putActionGroup(actionGroup);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyActionGroupLookupSession} using an
     *  array of action groups.
     *
     *  @param system the system
     *  @param actionGroups an array of action groups
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code system},
     *          {@code actionGroups}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyActionGroupLookupSession(org.osid.control.System system,
                                                org.osid.control.ActionGroup[] actionGroups, org.osid.proxy.Proxy proxy) {
        this(system, proxy);
        putActionGroups(actionGroups);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyActionGroupLookupSession} using a
     *  collection of action groups.
     *
     *  @param system the system
     *  @param actionGroups a collection of action groups
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code system},
     *          {@code actionGroups}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyActionGroupLookupSession(org.osid.control.System system,
                                                java.util.Collection<? extends org.osid.control.ActionGroup> actionGroups,
                                                org.osid.proxy.Proxy proxy) {
   
        this(system, proxy);
        setSessionProxy(proxy);
        putActionGroups(actionGroups);
        return;
    }

    
    /**
     *  Makes a {@code ActionGroup} available in this session.
     *
     *  @param actionGroup an action group
     *  @throws org.osid.NullArgumentException {@code actionGroup{@code 
     *          is {@code null}
     */

    @Override
    public void putActionGroup(org.osid.control.ActionGroup actionGroup) {
        super.putActionGroup(actionGroup);
        return;
    }


    /**
     *  Makes an array of actionGroups available in this session.
     *
     *  @param actionGroups an array of action groups
     *  @throws org.osid.NullArgumentException {@code actionGroups{@code 
     *          is {@code null}
     */

    @Override
    public void putActionGroups(org.osid.control.ActionGroup[] actionGroups) {
        super.putActionGroups(actionGroups);
        return;
    }


    /**
     *  Makes collection of action groups available in this session.
     *
     *  @param actionGroups
     *  @throws org.osid.NullArgumentException {@code actionGroup{@code 
     *          is {@code null}
     */

    @Override
    public void putActionGroups(java.util.Collection<? extends org.osid.control.ActionGroup> actionGroups) {
        super.putActionGroups(actionGroups);
        return;
    }


    /**
     *  Removes a ActionGroup from this session.
     *
     *  @param actionGroupId the {@code Id} of the action group
     *  @throws org.osid.NullArgumentException {@code actionGroupId{@code  is
     *          {@code null}
     */

    @Override
    public void removeActionGroup(org.osid.id.Id actionGroupId) {
        super.removeActionGroup(actionGroupId);
        return;
    }    
}
