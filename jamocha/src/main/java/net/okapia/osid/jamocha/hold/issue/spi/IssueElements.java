//
// IssueElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.hold.issue.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class IssueElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the IssueElement Id.
     *
     *  @return the issue element Id
     */

    public static org.osid.id.Id getIssueEntityId() {
        return (makeEntityId("osid.hold.Issue"));
    }


    /**
     *  Gets the BureauId element Id.
     *
     *  @return the BureauId element Id
     */

    public static org.osid.id.Id getBureauId() {
        return (makeElementId("osid.hold.issue.BureauId"));
    }


    /**
     *  Gets the Bureau element Id.
     *
     *  @return the Bureau element Id
     */

    public static org.osid.id.Id getBureau() {
        return (makeElementId("osid.hold.issue.Bureau"));
    }


    /**
     *  Gets the BlockId element Id.
     *
     *  @return the BlockId element Id
     */

    public static org.osid.id.Id getBlockId() {
        return (makeQueryElementId("osid.hold.issue.BlockId"));
    }


    /**
     *  Gets the Block element Id.
     *
     *  @return the Block element Id
     */

    public static org.osid.id.Id getBlock() {
        return (makeQueryElementId("osid.hold.issue.Block"));
    }


    /**
     *  Gets the HoldId element Id.
     *
     *  @return the HoldId element Id
     */

    public static org.osid.id.Id getHoldId() {
        return (makeQueryElementId("osid.hold.issue.HoldId"));
    }


    /**
     *  Gets the Hold element Id.
     *
     *  @return the Hold element Id
     */

    public static org.osid.id.Id getHold() {
        return (makeQueryElementId("osid.hold.issue.Hold"));
    }


    /**
     *  Gets the OublietteId element Id.
     *
     *  @return the OublietteId element Id
     */

    public static org.osid.id.Id getOublietteId() {
        return (makeQueryElementId("osid.hold.issue.OublietteId"));
    }


    /**
     *  Gets the Oubliette element Id.
     *
     *  @return the Oubliette element Id
     */

    public static org.osid.id.Id getOubliette() {
        return (makeQueryElementId("osid.hold.issue.Oubliette"));
    }
}
