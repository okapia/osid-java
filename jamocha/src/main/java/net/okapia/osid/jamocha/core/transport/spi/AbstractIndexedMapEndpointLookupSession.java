//
// AbstractIndexedMapEndpointLookupSession.java
//
//    A simple framework for providing an Endpoint lookup service
//    backed by a fixed collection of endpoints with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.transport.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an Endpoint lookup service backed by a
 *  fixed collection of endpoints. The endpoints are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some endpoints may be compatible
 *  with more types than are indicated through these endpoint
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Endpoints</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapEndpointLookupSession
    extends AbstractMapEndpointLookupSession
    implements org.osid.transport.EndpointLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.transport.Endpoint> endpointsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.transport.Endpoint>());
    private final MultiMap<org.osid.type.Type, org.osid.transport.Endpoint> endpointsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.transport.Endpoint>());


    /**
     *  Makes an <code>Endpoint</code> available in this session.
     *
     *  @param  endpoint an endpoint
     *  @throws org.osid.NullArgumentException <code>endpoint<code> is
     *          <code>null</code>
     */

    @Override
    protected void putEndpoint(org.osid.transport.Endpoint endpoint) {
        super.putEndpoint(endpoint);

        this.endpointsByGenus.put(endpoint.getGenusType(), endpoint);
        
        try (org.osid.type.TypeList types = endpoint.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.endpointsByRecord.put(types.getNextType(), endpoint);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an endpoint from this session.
     *
     *  @param endpointId the <code>Id</code> of the endpoint
     *  @throws org.osid.NullArgumentException <code>endpointId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeEndpoint(org.osid.id.Id endpointId) {
        org.osid.transport.Endpoint endpoint;
        try {
            endpoint = getEndpoint(endpointId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.endpointsByGenus.remove(endpoint.getGenusType());

        try (org.osid.type.TypeList types = endpoint.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.endpointsByRecord.remove(types.getNextType(), endpoint);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeEndpoint(endpointId);
        return;
    }


    /**
     *  Gets an <code>EndpointList</code> corresponding to the given
     *  endpoint genus <code>Type</code> which does not include
     *  endpoints of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known endpoints or an error results. Otherwise,
     *  the returned list may contain only those endpoints that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  endpointGenusType an endpoint genus type 
     *  @return the returned <code>Endpoint</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>endpointGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.transport.EndpointList getEndpointsByGenusType(org.osid.type.Type endpointGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.transport.endpoint.ArrayEndpointList(this.endpointsByGenus.get(endpointGenusType)));
    }


    /**
     *  Gets an <code>EndpointList</code> containing the given
     *  endpoint record <code>Type</code>. In plenary mode, the
     *  returned list contains all known endpoints or an error
     *  results. Otherwise, the returned list may contain only those
     *  endpoints that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  endpointRecordType an endpoint record type 
     *  @return the returned <code>endpoint</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>endpointRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.transport.EndpointList getEndpointsByRecordType(org.osid.type.Type endpointRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.transport.endpoint.ArrayEndpointList(this.endpointsByRecord.get(endpointRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.endpointsByGenus.clear();
        this.endpointsByRecord.clear();

        super.close();

        return;
    }
}
