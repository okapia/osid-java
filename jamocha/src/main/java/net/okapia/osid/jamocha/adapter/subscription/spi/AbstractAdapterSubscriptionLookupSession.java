//
// AbstractAdapterSubscriptionLookupSession.java
//
//    A Subscription lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.subscription.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Subscription lookup session adapter.
 */

public abstract class AbstractAdapterSubscriptionLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.subscription.SubscriptionLookupSession {

    private final org.osid.subscription.SubscriptionLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterSubscriptionLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterSubscriptionLookupSession(org.osid.subscription.SubscriptionLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Publisher/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Publisher Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getPublisherId() {
        return (this.session.getPublisherId());
    }


    /**
     *  Gets the {@code Publisher} associated with this session.
     *
     *  @return the {@code Publisher} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.Publisher getPublisher()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getPublisher());
    }


    /**
     *  Tests if this user can perform {@code Subscription} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupSubscriptions() {
        return (this.session.canLookupSubscriptions());
    }


    /**
     *  A complete view of the {@code Subscription} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeSubscriptionView() {
        this.session.useComparativeSubscriptionView();
        return;
    }


    /**
     *  A complete view of the {@code Subscription} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenarySubscriptionView() {
        this.session.usePlenarySubscriptionView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include subscriptions in publishers which are children
     *  of this publisher in the publisher hierarchy.
     */

    @OSID @Override
    public void useFederatedPublisherView() {
        this.session.useFederatedPublisherView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this publisher only.
     */

    @OSID @Override
    public void useIsolatedPublisherView() {
        this.session.useIsolatedPublisherView();
        return;
    }
    

    /**
     *  Only subscriptions whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveSubscriptionView() {
        this.session.useEffectiveSubscriptionView();
        return;
    }
    

    /**
     *  All subscriptions of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveSubscriptionView() {
        this.session.useAnyEffectiveSubscriptionView();
        return;
    }

     
    /**
     *  Gets the {@code Subscription} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Subscription} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Subscription} and
     *  retained for compatibility.
     *
     *  In effective mode, subscriptions are returned that are currently
     *  effective.  In any effective mode, effective subscriptions and
     *  those currently expired are returned.
     *
     *  @param subscriptionId {@code Id} of the {@code Subscription}
     *  @return the subscription
     *  @throws org.osid.NotFoundException {@code subscriptionId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code subscriptionId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.Subscription getSubscription(org.osid.id.Id subscriptionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSubscription(subscriptionId));
    }


    /**
     *  Gets a {@code SubscriptionList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  subscriptions specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Subscriptions} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, subscriptions are returned that are currently
     *  effective.  In any effective mode, effective subscriptions and
     *  those currently expired are returned.
     *
     *  @param  subscriptionIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Subscription} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code subscriptionIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsByIds(org.osid.id.IdList subscriptionIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSubscriptionsByIds(subscriptionIds));
    }


    /**
     *  Gets a {@code SubscriptionList} corresponding to the given
     *  subscription genus {@code Type} which does not include
     *  subscriptions of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned list
     *  may contain only those subscriptions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, subscriptions are returned that are currently
     *  effective.  In any effective mode, effective subscriptions and
     *  those currently expired are returned.
     *
     *  @param  subscriptionGenusType a subscription genus type 
     *  @return the returned {@code Subscription} list
     *  @throws org.osid.NullArgumentException
     *          {@code subscriptionGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsByGenusType(org.osid.type.Type subscriptionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSubscriptionsByGenusType(subscriptionGenusType));
    }


    /**
     *  Gets a {@code SubscriptionList} corresponding to the given
     *  subscription genus {@code Type} and include any additional
     *  subscriptions with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned list
     *  may contain only those subscriptions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, subscriptions are returned that are currently
     *  effective.  In any effective mode, effective subscriptions and
     *  those currently expired are returned.
     *
     *  @param  subscriptionGenusType a subscription genus type 
     *  @return the returned {@code Subscription} list
     *  @throws org.osid.NullArgumentException
     *          {@code subscriptionGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsByParentGenusType(org.osid.type.Type subscriptionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSubscriptionsByParentGenusType(subscriptionGenusType));
    }


    /**
     *  Gets a {@code SubscriptionList} containing the given
     *  subscription record {@code Type}.
     * 
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those subscriptions that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, subscriptions are returned that are
     *  currently effective.  In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param  subscriptionRecordType a subscription record type 
     *  @return the returned {@code Subscription} list
     *  @throws org.osid.NullArgumentException
     *          {@code subscriptionRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsByRecordType(org.osid.type.Type subscriptionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSubscriptionsByRecordType(subscriptionRecordType));
    }


    /**
     *  Gets a {@code SubscriptionList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned list
     *  may contain only those subscriptions that are accessible
     *  through this session.
     *  
     *  In active mode, subscriptions are returned that are currently
     *  active. In any status mode, active and inactive subscriptions
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Subscription} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsOnDate(org.osid.calendaring.DateTime from, 
                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSubscriptionsOnDate(from, to));
    }
        

    /**
     *  Gets a {@code SubscriptionList} of a subscription genus type
     *  and effective during the entire given date range inclusive but
     *  not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those subscriptions that are accessible
     *  through this session.
     *  
     *  In effective mode, subscriptions are returned that are
     *  currently effective. In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param  subscriptionGenusType a subscription genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned {@code Subscriptions} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code subscriptionGenusType, 
     *          from,} or {@code to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsByGenusTypeOnDate(org.osid.type.Type subscriptionGenusType, 
                                                                                    org.osid.calendaring.DateTime from, 
                                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSubscriptionsByGenusTypeOnDate(subscriptionGenusType, from, to));
    }


    /**
     *  Gets a list of subscriptions corresponding to a subscriber
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned list
     *  may contain only those subscriptions that are accessible through
     *  this session.
     *
     *  In effective mode, subscriptions are returned that are
     *  currently effective.  In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the subscriber
     *  @return the returned {@code SubscriptionList}
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsForSubscriber(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSubscriptionsForSubscriber(resourceId));
    }


    /**
     *  Gets a list of subscriptions corresponding to a subscriber
     *  {@code Id} and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those subscriptions that are accessible
     *  through this session.
     *
     *  In effective mode, subscriptions are returned that are
     *  currently effective.  In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the subscriber
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code SubscriptionList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsForSubscriberOnDate(org.osid.id.Id resourceId,
                                                                                      org.osid.calendaring.DateTime from,
                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSubscriptionsForSubscriberOnDate(resourceId, from, to));
    }


    /**
     *  Gets a list of subscriptions of the given genus type
     *  corresponding to a subscriber {@code Id.}
     *  
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those entries that are accessible
     *  through this session.
     *  
     *  In effective mode, subscriptions are returned that are
     *  currently effective. In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource 
     *  @param  subscriptionGenusType a subscription genus type 
     *  @return the returned {@code SubscriptionList} 
     *  @throws org.osid.NullArgumentException {@code resourceId} or
     *          {@code subscriptionGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsByGenusTypeForSubscriber(org.osid.id.Id resourceId, 
                                                                                           org.osid.type.Type subscriptionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getSubscriptionsByGenusTypeForSubscriber(resourceId, subscriptionGenusType));
    }


    /**
     *  Gets a list of all subscriptions of the given genus type
     *  corresponding to a subscriber {@code Id} and effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those entries that are accessible
     *  through this session.
     *  
     *  In effective mode, subscriptions are returned that are
     *  currently effective. In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @param  subscriptionGenusType a subscription genus type 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned {@code SubscriptionList} 
     *  @throws org.osid.InvalidArgumentException {@code to} is less
     *          than {@code from}
     *  @throws org.osid.NullArgumentException {@code resourceId, 
     *          subscriptionGenusType, from} or {@code to} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsByGenusTypeForSubscriberOnDate(org.osid.id.Id resourceId, 
                                                                                                 org.osid.type.Type subscriptionGenusType, 
                                                                                                 org.osid.calendaring.DateTime from, 
                                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSubscriptionsByGenusTypeForSubscriberOnDate(resourceId, subscriptionGenusType, from, to));
    }


    /**
     *  Gets a list of subscriptions corresponding to a dispatch
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned list
     *  may contain only those subscriptions that are accessible
     *  through this session.
     *
     *  In effective mode, subscriptions are returned that are
     *  currently effective.  In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param  dispatchId the {@code Id} of the dispatch
     *  @return the returned {@code SubscriptionList}
     *  @throws org.osid.NullArgumentException {@code dispatchId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsForDispatch(org.osid.id.Id dispatchId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSubscriptionsForDispatch(dispatchId));
    }


    /**
     *  Gets a list of subscriptions corresponding to a dispatch
     *  {@code Id} and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those subscriptions that are accessible
     *  through this session.
     *
     *  In effective mode, subscriptions are returned that are
     *  currently effective.  In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param  dispatchId the {@code Id} of the dispatch
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code SubscriptionList}
     *  @throws org.osid.NullArgumentException {@code dispatchId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsForDispatchOnDate(org.osid.id.Id dispatchId,
                                                                                    org.osid.calendaring.DateTime from,
                                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSubscriptionsForDispatchOnDate(dispatchId, from, to));
    }


    /**
     *  Gets a list of all subscriptions of the given genus type
     *  corresponding to a dispatch {@code Id}.
     *  
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those entries that are accessible
     *  through this session.
     *  
     *  In effective mode, subscriptions are returned that are
     *  currently effective. In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param  dispatchId the {@code Id} of the dispatch 
     *  @param  subscriptionGenusType a subscription genus type 
     *  @return the returned {@code SubscriptionList} 
     *  @throws org.osid.NullArgumentException {@code dispatchId} or 
     *          {@code subscriptionGenusType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsByGenusTypeForDispatch(org.osid.id.Id dispatchId, 
                                                                                         org.osid.type.Type subscriptionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSubscriptionsByGenusTypeForDispatch(dispatchId, subscriptionGenusType));
    }


    /**
     *  Gets a list of all subscriptions of the given genus type
     *  corresponding to a dispatch {@code Id} and effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those entries that are accessible
     *  through this session.
     *  
     *  In effective mode, subscriptions are returned that are
     *  currently effective. In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param  dispatchId a dispatch {@code Id} 
     *  @param  subscriptionGenusType a subscription genus type 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned {@code SubscriptionList} 
     *  @throws org.osid.InvalidArgumentException {@code to} is less 
     *          than {@code from} 
     *  @throws org.osid.NullArgumentException {@code dispatchId, 
     *          subscriptionGenusType, from} or {@code to} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsByGenusTypeForDispatchOnDate(org.osid.id.Id dispatchId, 
                                                                                               org.osid.type.Type subscriptionGenusType, 
                                                                                               org.osid.calendaring.DateTime from, 
                                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSubscriptionsByGenusTypeForDispatchOnDate(dispatchId, subscriptionGenusType, from, to));
    }


    /**
     *  Gets a list of subscriptions corresponding to subscriber and
     *  dispatch {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those subscriptions that are accessible
     *  through this session.
     *
     *  In effective mode, subscriptions are returned that are
     *  currently effective.  In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the subscriber
     *  @param  dispatchId the {@code Id} of the dispatch
     *  @return the returned {@code SubscriptionList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code dispatchId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsForSubscriberAndDispatch(org.osid.id.Id resourceId,
                                                                                           org.osid.id.Id dispatchId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSubscriptionsForSubscriberAndDispatch(resourceId, dispatchId));
    }


    /**
     *  Gets a list of subscriptions corresponding to subscriber and
     *  dispatch {@code Ids} and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those subscriptions that are accessible
     *  through this session.
     *
     *  In effective mode, subscriptions are returned that are
     *  currently effective. In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param  dispatchId the {@code Id} of the dispatch
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code SubscriptionList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code dispatchId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsForSubscriberAndDispatchOnDate(org.osid.id.Id resourceId,
                                                                                                 org.osid.id.Id dispatchId,
                                                                                                 org.osid.calendaring.DateTime from,
                                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSubscriptionsForSubscriberAndDispatchOnDate(resourceId, dispatchId, from, to));
    }


    /**
     *  Gets a list of all subscriptions of the given genus type
     *  corresponding to a susbcriber and dispatch {@code Id}.
     *  
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those entries that are accessible
     *  through this session.
     *  
     *  In effective mode, subscriptions are returned that are
     *  currently effective. In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource 
     *  @param  dispatchId the {@code Id} of the dispatch 
     *  @param  subscriptionGenusType a subscription genus type 
     *  @return the returned {@code SubscriptionList} 
     *  @throws org.osid.NullArgumentException {@code resourceId,
     *         dispatchId} or {@code subscriptionGenusType} is {@code
     *         null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsByGenusTypeForSubscriberAndDispatch(org.osid.id.Id resourceId, 
                                                                                                      org.osid.id.Id dispatchId, 
                                                                                                      org.osid.type.Type subscriptionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSubscriptionsByGenusTypeForSubscriberAndDispatch(resourceId, dispatchId, subscriptionGenusType));
    }


    /**
     *  Gets a list of all subscriptions of the given genus type
     *  corresponding to a subscriber and dispatch {@code Id} and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned
     *  list may contain only those entries that are accessible
     *  through this session.
     *  
     *  In effective mode, subscriptions are returned that are
     *  currently effective. In any effective mode, effective
     *  subscriptions and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource 
     *  @param  dispatchId a dispatch {@code Id} 
     *  @param  subscriptionGenusType a subscription genus type 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned {@code SubscriptionList} 
     *  @throws org.osid.InvalidArgumentException {@code to} is less 
     *          than {@code from} 
     *  @throws org.osid.NullArgumentException {@code resourceId, dispatch, 
     *          subscriptionGenusType, from} or {@code to} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsByGenusTypeForSubscriberAndDispatchOnDate(org.osid.id.Id resourceId, 
                                                                                                            org.osid.id.Id dispatchId, 
                                                                                                            org.osid.type.Type subscriptionGenusType, 
                                                                                                            org.osid.calendaring.DateTime from, 
                                                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSubscriptionsByGenusTypeForSubscriberAndDispatchOnDate(resourceId, dispatchId, subscriptionGenusType, from, to));
    }


    /**
     *  Gets all {@code Subscriptions}. 
     *
     *  In plenary mode, the returned list contains all known
     *  subscriptions or an error results. Otherwise, the returned list
     *  may contain only those subscriptions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, subscriptions are returned that are currently
     *  effective.  In any effective mode, effective subscriptions and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Subscriptions} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSubscriptions());
    }
}
