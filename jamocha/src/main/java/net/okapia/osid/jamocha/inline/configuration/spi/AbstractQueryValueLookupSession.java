//
// AbstractQueryValueLookupSession.java
//
//    An inline adapter that maps a ValueLookupSession to
//    a ValueQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.configuration.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a ValueLookupSession to
 *  a ValueQuerySession.
 */

public abstract class AbstractQueryValueLookupSession
    extends net.okapia.osid.jamocha.configuration.spi.AbstractValueLookupSession
    implements org.osid.configuration.ValueLookupSession {

      private boolean activeonly    = false;

    private final org.osid.configuration.ValueQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryValueLookupSession.
     *
     *  @param querySession the underlying value query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryValueLookupSession(org.osid.configuration.ValueQuerySession querySession) {
        nullarg(querySession, "value query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Configuration</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Configuration Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getConfigurationId() {
        return (this.session.getConfigurationId());
    }


    /**
     *  Gets the <code>Configuration</code> associated with this 
     *  session.
     *
     *  @return the <code>Configuration</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.Configuration getConfiguration()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getConfiguration());
    }


    /**
     *  Tests if this user can perform <code>Value</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupValues() {
        return (this.session.canSearchValues());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include values in configurations which are children
     *  of this configuration in the configuration hierarchy.
     */

    @OSID @Override
    public void useFederatedConfigurationView() {
        this.session.useFederatedConfigurationView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this configuration only.
     */

    @OSID @Override
    public void useIsolatedConfigurationView() {
        this.session.useIsolatedConfigurationView();
        return;
    }
    

    /**
     *  Only active values are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveValueView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive values are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusValueView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>Value</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Value</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Value</code> and
     *  retained for compatibility.
     *
     *  In active mode, values are returned that are currently
     *  active. In any status mode, active and inactive values
     *  are returned.
     *
     *  @param  valueId <code>Id</code> of the
     *          <code>Value</code>
     *  @return the value
     *  @throws org.osid.NotFoundException <code>valueId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>valueId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.Value getValue(org.osid.id.Id valueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.configuration.ValueQuery query = getQuery();
        query.matchId(valueId, true);
        org.osid.configuration.ValueList values = this.session.getValuesByQuery(query);
        if (values.hasNext()) {
            return (values.getNextValue());
        } 
        
        throw new org.osid.NotFoundException(valueId + " not found");
    }


    /**
     *  Gets a <code>ValueList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  values specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Values</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, values are returned that are currently
     *  active. In any status mode, active and inactive values
     *  are returned.
     *
     *  @param  valueIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Value</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>valueIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ValueList getValuesByIds(org.osid.id.IdList valueIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.configuration.ValueQuery query = getQuery();

        try (org.osid.id.IdList ids = valueIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getValuesByQuery(query));
    }


    /**
     *  Gets a <code>ValueList</code> corresponding to the given
     *  value genus <code>Type</code> which does not include
     *  values of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  values or an error results. Otherwise, the returned list
     *  may contain only those values that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, values are returned that are currently
     *  active. In any status mode, active and inactive values
     *  are returned.
     *
     *  @param  valueGenusType a value genus type 
     *  @return the returned <code>Value</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>valueGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ValueList getValuesByGenusType(org.osid.type.Type valueGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.configuration.ValueQuery query = getQuery();
        query.matchGenusType(valueGenusType, true);
        return (this.session.getValuesByQuery(query));
    }


    /**
     *  Gets a <code>ValueList</code> corresponding to the given
     *  value genus <code>Type</code> and include any additional
     *  values with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  values or an error results. Otherwise, the returned list
     *  may contain only those values that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, values are returned that are currently
     *  active. In any status mode, active and inactive values
     *  are returned.
     *
     *  @param  valueGenusType a value genus type 
     *  @return the returned <code>Value</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>valueGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ValueList getValuesByParentGenusType(org.osid.type.Type valueGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.configuration.ValueQuery query = getQuery();
        query.matchParentGenusType(valueGenusType, true);
        return (this.session.getValuesByQuery(query));
    }


    /**
     *  Gets a <code>ValueList</code> containing the given
     *  value record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  values or an error results. Otherwise, the returned list
     *  may contain only those values that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, values are returned that are currently
     *  active. In any status mode, active and inactive values
     *  are returned.
     *
     *  @param  valueRecordType a value record type 
     *  @return the returned <code>Value</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>valueRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ValueList getValuesByRecordType(org.osid.type.Type valueRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.configuration.ValueQuery query = getQuery();
        query.matchRecordType(valueRecordType, true);
        return (this.session.getValuesByQuery(query));
    }

    
    /**
     *  Gets all <code>Values</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  values or an error results. Otherwise, the returned list
     *  may contain only those values that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, values are returned that are currently
     *  active. In any status mode, active and inactive values
     *  are returned.
     *
     *  @return a list of <code>Values</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ValueList getValues()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {


        org.osid.configuration.ValueQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getValuesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.configuration.ValueQuery getQuery() {
        org.osid.configuration.ValueQuery query = this.session.getValueQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
