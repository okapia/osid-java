//
// AbstractCanonicalUnitSearch.java
//
//     A template for making a CanonicalUnit Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.canonicalunit.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing canonical unit searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractCanonicalUnitSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.offering.CanonicalUnitSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.offering.records.CanonicalUnitSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.offering.CanonicalUnitSearchOrder canonicalUnitSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of canonical units. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  canonicalUnitIds list of canonical units
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongCanonicalUnits(org.osid.id.IdList canonicalUnitIds) {
        while (canonicalUnitIds.hasNext()) {
            try {
                this.ids.add(canonicalUnitIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongCanonicalUnits</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of canonical unit Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getCanonicalUnitIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  canonicalUnitSearchOrder canonical unit search order 
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>canonicalUnitSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderCanonicalUnitResults(org.osid.offering.CanonicalUnitSearchOrder canonicalUnitSearchOrder) {
	this.canonicalUnitSearchOrder = canonicalUnitSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.offering.CanonicalUnitSearchOrder getCanonicalUnitSearchOrder() {
	return (this.canonicalUnitSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given canonical unit search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a canonical unit implementing the requested record.
     *
     *  @param canonicalUnitSearchRecordType a canonical unit search record
     *         type
     *  @return the canonical unit search record
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(canonicalUnitSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.records.CanonicalUnitSearchRecord getCanonicalUnitSearchRecord(org.osid.type.Type canonicalUnitSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.offering.records.CanonicalUnitSearchRecord record : this.records) {
            if (record.implementsRecordType(canonicalUnitSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(canonicalUnitSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this canonical unit search. 
     *
     *  @param canonicalUnitSearchRecord canonical unit search record
     *  @param canonicalUnitSearchRecordType canonicalUnit search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCanonicalUnitSearchRecord(org.osid.offering.records.CanonicalUnitSearchRecord canonicalUnitSearchRecord, 
                                           org.osid.type.Type canonicalUnitSearchRecordType) {

        addRecordType(canonicalUnitSearchRecordType);
        this.records.add(canonicalUnitSearchRecord);        
        return;
    }
}
