//
// AbstractProfileManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.profile.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractProfileManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.profile.ProfileManager,
               org.osid.profile.ProfileProxyManager {

    private final Types profileEntryRecordTypes            = new TypeRefSet();
    private final Types profileEntrySearchRecordTypes      = new TypeRefSet();

    private final Types profileItemRecordTypes             = new TypeRefSet();
    private final Types profileItemSearchRecordTypes       = new TypeRefSet();

    private final Types profileRecordTypes                 = new TypeRefSet();
    private final Types profileSearchRecordTypes           = new TypeRefSet();

    private final Types profileEntryConditionRecordTypes   = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractProfileManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractProfileManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests for the availability of a profile service which is the basic 
     *  service for checking profiles. 
     *
     *  @return <code> true </code> if profiling is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfiling() {
        return (false);
    }


    /**
     *  Tests if a profile entry lookup service is supported. A profile entry 
     *  lookup service defines methods to access profile entries. 
     *
     *  @return true if profile entry lookup is supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsProfileEntryLookup() {
        return (false);
    }


    /**
     *  Tests if profile entry query is supported. 
     *
     *  @return <code> true </code> if profile entry query is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileEntryQuery() {
        return (false);
    }


    /**
     *  Tests if a profile entry search service is supported. 
     *
     *  @return <code> true </code> if profile entry search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileEntrySearch() {
        return (false);
    }


    /**
     *  Tests if a profile entry administrative service is supported. 
     *
     *  @return <code> true </code> if profile entry admin is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileEntryAdmin() {
        return (false);
    }


    /**
     *  Tests if profile entry notification is supported. Messages may be sent 
     *  when peofile entries are created, modified, or deleted. 
     *
     *  @return <code> true </code> if profile entry notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileEntryNotification() {
        return (false);
    }


    /**
     *  Tests if rerieving mappings of profile entries and profile is 
     *  supported. 
     *
     *  @return <code> true </code> if profile entry profile mapping retrieval 
     *          is supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileEntryProfile() {
        return (false);
    }


    /**
     *  Tests if managing mappings of profile entries and profile is 
     *  supported. 
     *
     *  @return <code> true </code> if profile entry profile assignment is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileEntryProfileAssignment() {
        return (false);
    }


    /**
     *  Tests if profile entry smart profiles are available. 
     *
     *  @return <code> true </code> if profile entry smart profiles are 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileEntryRelationshpSmartProfile() {
        return (false);
    }


    /**
     *  Tests if a profile item lookup service is supported. A profile item 
     *  lookup service defines methods to access profile items. 
     *
     *  @return <code> true </code> if profile item lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileItemLookup() {
        return (false);
    }


    /**
     *  Tests if profile item query is supported. 
     *
     *  @return <code> true </code> if profile item query is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileItemQuery() {
        return (false);
    }


    /**
     *  Tests if a profile item search service is supported. 
     *
     *  @return <code> true </code> if profile item search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileItemSearch() {
        return (false);
    }


    /**
     *  Tests if a profile item administrative service is supported. 
     *
     *  @return <code> true </code> if profile item admin is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileItemAdmin() {
        return (false);
    }


    /**
     *  Tests if profile item notification is supported. Messages may be sent 
     *  when profile items are created, modified, or deleted. 
     *
     *  @return <code> true </code> if profile item notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileItemNotification() {
        return (false);
    }


    /**
     *  Tests if a profile item to profile lookup session is available. 
     *
     *  @return <code> true </code> if profile item profile lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileItemProfile() {
        return (false);
    }


    /**
     *  Tests if a profile item to profile assignment session is available. 
     *
     *  @return <code> true </code> if profile item profile assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileItemProfileAssignment() {
        return (false);
    }


    /**
     *  Tests if profile item smart profiles are available. 
     *
     *  @return <code> true </code> if profile item smart profiles are 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileItemSmartProfile() {
        return (false);
    }


    /**
     *  Tests if a profile lookup service is supported. A profile lookup 
     *  service defines methods to access profiles. 
     *
     *  @return <code> true </code> if profile lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileLookup() {
        return (false);
    }


    /**
     *  Tests if profile query is supported. 
     *
     *  @return <code> true </code> if profile query is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileQuery() {
        return (false);
    }


    /**
     *  Tests if a profile search service is supported. 
     *
     *  @return <code> true </code> if profile search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileSearch() {
        return (false);
    }


    /**
     *  Tests if a profile administrative service is supported. 
     *
     *  @return <code> true </code> if profile admin is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileAdmin() {
        return (false);
    }


    /**
     *  Tests if profile notification is supported. Messages may be sent when 
     *  profiles are created, modified, or deleted. 
     *
     *  @return <code> true </code> if profile notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileNotification() {
        return (false);
    }


    /**
     *  Tests if a profile hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if a profile hierarchy traversal is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileHierarchy() {
        return (false);
    }


    /**
     *  Tests if profile hierarchy design is supported. 
     *
     *  @return <code> true </code> if a profile hierarchy design is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests if a profile batch service is supported. 
     *
     *  @return <code> true </code> if a profile batch service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileBatch() {
        return (false);
    }


    /**
     *  Tests if a profile rules service is supported. 
     *
     *  @return <code> true </code> if a profile rules service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileRules() {
        return (false);
    }


    /**
     *  Gets the supported <code> ProfileEntry </code> record types. 
     *
     *  @return a list containing the supported profile entry record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProfileEntryRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.profileEntryRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given profile entry record type is supported. 
     *
     *  @param  profileEntryRecordType a <code> Type </code> indicating a 
     *          profile entry record type 
     *  @return <code> true </code> if the given record Type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> profileEntryRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProfileEntryRecordType(org.osid.type.Type profileEntryRecordType) {
        return (this.profileEntryRecordTypes.contains(profileEntryRecordType));
    }


    /**
     *  Adds support for a profile entry record type.
     *
     *  @param profileEntryRecordType a profile entry record type
     *  @throws org.osid.NullArgumentException
     *  <code>profileEntryRecordType</code> is <code>null</code>
     */

    protected void addProfileEntryRecordType(org.osid.type.Type profileEntryRecordType) {
        this.profileEntryRecordTypes.add(profileEntryRecordType);
        return;
    }


    /**
     *  Removes support for a profile entry record type.
     *
     *  @param profileEntryRecordType a profile entry record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>profileEntryRecordType</code> is <code>null</code>
     */

    protected void removeProfileEntryRecordType(org.osid.type.Type profileEntryRecordType) {
        this.profileEntryRecordTypes.remove(profileEntryRecordType);
        return;
    }


    /**
     *  Gets the supported <code> ProfileEntry </code> search record types. 
     *
     *  @return a list containing the supported profile entry search record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProfileEntrySearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.profileEntrySearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given profile entry search record type is supported. 
     *
     *  @param  profileEntrySearchRecordType a <code> Type </code> indicating 
     *          a profile entry search record type 
     *  @return <code> true </code> if the given search record Type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          profileEntrySearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProfileEntrySearchRecordType(org.osid.type.Type profileEntrySearchRecordType) {
        return (this.profileEntrySearchRecordTypes.contains(profileEntrySearchRecordType));
    }


    /**
     *  Adds support for a profile entry search record type.
     *
     *  @param profileEntrySearchRecordType a profile entry search record type
     *  @throws org.osid.NullArgumentException
     *  <code>profileEntrySearchRecordType</code> is <code>null</code>
     */

    protected void addProfileEntrySearchRecordType(org.osid.type.Type profileEntrySearchRecordType) {
        this.profileEntrySearchRecordTypes.add(profileEntrySearchRecordType);
        return;
    }


    /**
     *  Removes support for a profile entry search record type.
     *
     *  @param profileEntrySearchRecordType a profile entry search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>profileEntrySearchRecordType</code> is <code>null</code>
     */

    protected void removeProfileEntrySearchRecordType(org.osid.type.Type profileEntrySearchRecordType) {
        this.profileEntrySearchRecordTypes.remove(profileEntrySearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> ProfileItem </code> record types. 
     *
     *  @return a list containing the supported <code> ProfileItem </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProfileItemRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.profileItemRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> ProfileItem </code> record type is 
     *  supported. 
     *
     *  @param  profileItemRecordType a <code> Type </code> indicating a 
     *          <code> ProfileItem </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> profileItemRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProfileItemRecordType(org.osid.type.Type profileItemRecordType) {
        return (this.profileItemRecordTypes.contains(profileItemRecordType));
    }


    /**
     *  Adds support for a profile item record type.
     *
     *  @param profileItemRecordType a profile item record type
     *  @throws org.osid.NullArgumentException
     *  <code>profileItemRecordType</code> is <code>null</code>
     */

    protected void addProfileItemRecordType(org.osid.type.Type profileItemRecordType) {
        this.profileItemRecordTypes.add(profileItemRecordType);
        return;
    }


    /**
     *  Removes support for a profile item record type.
     *
     *  @param profileItemRecordType a profile item record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>profileItemRecordType</code> is <code>null</code>
     */

    protected void removeProfileItemRecordType(org.osid.type.Type profileItemRecordType) {
        this.profileItemRecordTypes.remove(profileItemRecordType);
        return;
    }


    /**
     *  Gets the supported <code> ProfileItem </code> search record types. 
     *
     *  @return a list containing the supported <code> ProfileItem </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProfileItemSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.profileItemSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> ProfileItem </code> search record type is 
     *  supported. 
     *
     *  @param  profileItemSearchRecordType a <code> Type </code> indicating a 
     *          <code> ProfileItem </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          profileItemSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProfileItemSearchRecordType(org.osid.type.Type profileItemSearchRecordType) {
        return (this.profileItemSearchRecordTypes.contains(profileItemSearchRecordType));
    }


    /**
     *  Adds support for a profile item search record type.
     *
     *  @param profileItemSearchRecordType a profile item search record type
     *  @throws org.osid.NullArgumentException
     *  <code>profileItemSearchRecordType</code> is <code>null</code>
     */

    protected void addProfileItemSearchRecordType(org.osid.type.Type profileItemSearchRecordType) {
        this.profileItemSearchRecordTypes.add(profileItemSearchRecordType);
        return;
    }


    /**
     *  Removes support for a profile item search record type.
     *
     *  @param profileItemSearchRecordType a profile item search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>profileItemSearchRecordType</code> is <code>null</code>
     */

    protected void removeProfileItemSearchRecordType(org.osid.type.Type profileItemSearchRecordType) {
        this.profileItemSearchRecordTypes.remove(profileItemSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Profile </code> record types. 
     *
     *  @return a list containing the supported <code> Profile </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProfileRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.profileRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Profile </code> record type is supported. 
     *
     *  @param  profileRecordType a <code> Type </code> indicating a <code> 
     *          Profile </code> type 
     *  @return <code> true </code> if the given profile record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> profileRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProfileRecordType(org.osid.type.Type profileRecordType) {
        return (this.profileRecordTypes.contains(profileRecordType));
    }


    /**
     *  Adds support for a profile record type.
     *
     *  @param profileRecordType a profile record type
     *  @throws org.osid.NullArgumentException
     *  <code>profileRecordType</code> is <code>null</code>
     */

    protected void addProfileRecordType(org.osid.type.Type profileRecordType) {
        this.profileRecordTypes.add(profileRecordType);
        return;
    }


    /**
     *  Removes support for a profile record type.
     *
     *  @param profileRecordType a profile record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>profileRecordType</code> is <code>null</code>
     */

    protected void removeProfileRecordType(org.osid.type.Type profileRecordType) {
        this.profileRecordTypes.remove(profileRecordType);
        return;
    }


    /**
     *  Gets the supported profile search record types. 
     *
     *  @return a list containing the supported <code> Profile </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProfileSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.profileSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given profile search record type is supported. 
     *
     *  @param  profileSearchRecordType a <code> Type </code> indicating a 
     *          <code> Profile </code> search record type 
     *  @return <code> true </code> if the given search record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> profileSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProfileSearchRecordType(org.osid.type.Type profileSearchRecordType) {
        return (this.profileSearchRecordTypes.contains(profileSearchRecordType));
    }


    /**
     *  Adds support for a profile search record type.
     *
     *  @param profileSearchRecordType a profile search record type
     *  @throws org.osid.NullArgumentException
     *  <code>profileSearchRecordType</code> is <code>null</code>
     */

    protected void addProfileSearchRecordType(org.osid.type.Type profileSearchRecordType) {
        this.profileSearchRecordTypes.add(profileSearchRecordType);
        return;
    }


    /**
     *  Removes support for a profile search record type.
     *
     *  @param profileSearchRecordType a profile search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>profileSearchRecordType</code> is <code>null</code>
     */

    protected void removeProfileSearchRecordType(org.osid.type.Type profileSearchRecordType) {
        this.profileSearchRecordTypes.remove(profileSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> ProfileEntryCondition </code> record types. 
     *
     *  @return a list containing the supported <code> ProfileEntryCondition 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProfileEntryConditionRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.profileEntryConditionRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> ProfileEntryCondition </code> record type is 
     *  supported. 
     *
     *  @param  profileEntryConditionRecordType a <code> Type </code> 
     *          indicating an <code> ProfileEntryCondition </code> record type 
     *  @return <code> true </code> if the given profile entry condition 
     *          record <code> Type </code> is supported, <code> false </code> 
     *          otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          profileEntryConditionRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProfileEntryConditionRecordType(org.osid.type.Type profileEntryConditionRecordType) {
        return (this.profileEntryConditionRecordTypes.contains(profileEntryConditionRecordType));
    }


    /**
     *  Adds support for a profile entry condition record type.
     *
     *  @param profileEntryConditionRecordType a profile entry condition record type
     *  @throws org.osid.NullArgumentException
     *  <code>profileEntryConditionRecordType</code> is <code>null</code>
     */

    protected void addProfileEntryConditionRecordType(org.osid.type.Type profileEntryConditionRecordType) {
        this.profileEntryConditionRecordTypes.add(profileEntryConditionRecordType);
        return;
    }


    /**
     *  Removes support for a profile entry condition record type.
     *
     *  @param profileEntryConditionRecordType a profile entry condition record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>profileEntryConditionRecordType</code> is <code>null</code>
     */

    protected void removeProfileEntryConditionRecordType(org.osid.type.Type profileEntryConditionRecordType) {
        this.profileEntryConditionRecordTypes.remove(profileEntryConditionRecordType);
        return;
    }


    /**
     *  Gets a <code> ProfilingSession </code> which is responsible for 
     *  performing profile checks. 
     *
     *  @return a profiling session for this service 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProfiling() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfilingSession getProfilingSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileManager.getProfilingSession not implemented");
    }


    /**
     *  Gets a <code> ProfilingSession </code> which is responsible for 
     *  performing profile checks. 
     *
     *  @param  proxy a proxy 
     *  @return a profiling session for this service 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProfiling() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfilingSession getProfilingSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileProxyManager.getProfilingSession not implemented");
    }


    /**
     *  Gets a <code> ProfilingSession </code> which is responsible for 
     *  performing profile checks for the given profile. 
     *
     *  @param  profileId the <code> Id </code> of the profile 
     *  @return <code> a ProfilingSession </code> 
     *  @throws org.osid.NotFoundException <code> profileId </code> 
     *  @throws org.osid.NullArgumentException <code> profileId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsProfiling() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfilingSession getProfilingSessionForProfile(org.osid.id.Id profileId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileManager.getProfilingSessionForProfile not implemented");
    }


    /**
     *  Gets a <code> ProfilingSession </code> which is responsible for 
     *  performing profile checks for the given profile. 
     *
     *  @param  profileId the <code> Id </code> of the profile 
     *  @param  proxy a proxy 
     *  @return <code> a ProfilingSession </code> 
     *  @throws org.osid.NotFoundException <code> profileId </code> not found 
     *  @throws org.osid.NullArgumentException <code> profileId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsProfiling() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfilingSession getProfilingSessionForProfile(org.osid.id.Id profileId, 
                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileProxyManager.getProfilingSessionForProfile not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  lookup service. 
     *
     *  @return a <code> ProfileEntryLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryLookupSession getProfileEntryLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileManager.getProfileEntryLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProfileEntryLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryLookupSession getProfileEntryLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileProxyManager.getProfileEntryLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  lookup service for the given profile. 
     *
     *  @param  profileId the <code> Id </code> of the profile 
     *  @return <code> a ProfileEntryLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> profileId </code> not found 
     *  @throws org.osid.NullArgumentException <code> profileId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryLookupSession getProfileEntryLookupSessionForProfile(org.osid.id.Id profileId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileManager.getProfileEntryLookupSessionForProfile not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  lookup service for the given profile. 
     *
     *  @param  profileId the <code> Id </code> of the profile 
     *  @param  proxy a proxy 
     *  @return <code> a ProfileEntryLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> profileId </code> not found 
     *  @throws org.osid.NullArgumentException <code> profileId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryLookupSession getProfileEntryLookupSessionForProfile(org.osid.id.Id profileId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileProxyManager.getProfileEntryLookupSessionForProfile not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  query service. 
     *
     *  @return a <code> ProfileEntryQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryQuerySession getProfileEntryQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileManager.getProfileEntryQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProfileEntryQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryQuerySession getProfileEntryQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileProxyManager.getProfileEntryQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  query service for the given profile. 
     *
     *  @param  profileId the <code> Id </code> of the <code> Profile </code> 
     *  @return a <code> ProfileEntryQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Profile </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> profileId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryQuerySession getProfileEntryQuerySessionForProfile(org.osid.id.Id profileId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileManager.getProfileEntryQuerySessionForProfile not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  query service for the given profile. 
     *
     *  @param  profileId the <code> Id </code> of the <code> Profile </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProfileEntryQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Profile </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> profileId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryQuerySession getProfileEntryQuerySessionForProfile(org.osid.id.Id profileId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileProxyManager.getProfileEntryQuerySessionForProfile not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  search service. 
     *
     *  @return a <code> ProfileEntrySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntrySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntrySearchSession getProfileEntrySearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileManager.getProfileEntrySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProfileEntrySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntrySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntrySearchSession getProfileEntrySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileProxyManager.getProfileEntrySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  search service for the given profile. 
     *
     *  @param  profileId the <code> Id </code> of the profile 
     *  @return <code> a ProfileEntrySearchSession </code> 
     *  @throws org.osid.NotFoundException <code> profileId </code> not found 
     *  @throws org.osid.NullArgumentException <code> profileId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntrySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntrySearchSession getProfileEntrySearchSessionForProfile(org.osid.id.Id profileId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileManager.getProfileEntrySearchSessionForProfile not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  search service for the given profile. 
     *
     *  @param  profileId the <code> Id </code> of the profile 
     *  @param  proxy a proxy 
     *  @return <code> a ProfileEntrySearchSession </code> 
     *  @throws org.osid.NotFoundException <code> profileId </code> not found 
     *  @throws org.osid.NullArgumentException <code> profileId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntrySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntrySearchSession getProfileEntrySearchSessionForProfile(org.osid.id.Id profileId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileProxyManager.getProfileEntrySearchSessionForProfile not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  administration service. 
     *
     *  @return a <code> ProfileEntryAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryAdminSession getProfileEntryAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileManager.getProfileEntryAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProfileEntryAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryAdminSession getProfileEntryAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileProxyManager.getProfileEntryAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  admin service for the given profile. 
     *
     *  @param  profileId the <code> Id </code> of the profile 
     *  @return <code> a ProfileEntryAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> profileId </code> not found 
     *  @throws org.osid.NullArgumentException <code> profileId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryAdminSession getProfileEntryAdminSessionForProfile(org.osid.id.Id profileId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileManager.getProfileEntryAdminSessionForProfile not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  admin service for the given profile. 
     *
     *  @param  profileId the <code> Id </code> of the profile 
     *  @param  proxy a proxy 
     *  @return <code> a ProfileEntryAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> profileId </code> not found 
     *  @throws org.osid.NullArgumentException <code> profileId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryAdminSession getProfileEntryAdminSessionForProfile(org.osid.id.Id profileId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileProxyManager.getProfileEntryAdminSessionForProfile not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to profile 
     *  entry changes. 
     *
     *  @param  profileEntryReceiver the profile entry receiver 
     *  @return a <code> ProfileEntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> profileEntryReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryNotificationSession getProfileEntryNotificationSession(org.osid.profile.ProfileEntryReceiver profileEntryReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileManager.getProfileEntryNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to profile 
     *  entry changes. 
     *
     *  @param  profileEntryReceiver the profile entry receiver 
     *  @param  proxy a proxy 
     *  @return a <code> ProfileEntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> profileEntryReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryNotificationSession getProfileEntryNotificationSession(org.osid.profile.ProfileEntryReceiver profileEntryReceiver, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileProxyManager.getProfileEntryNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  notification service for the given profile. 
     *
     *  @param  profileEntryReceiver the profile entry receiver 
     *  @param  profileId the <code> Id </code> of the profile 
     *  @return <code> a ProfileEntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> profileId </code> not found 
     *  @throws org.osid.NullArgumentException <code> profileEntryReceiver 
     *          </code> or <code> profileId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryNotificationSession getProfileEntryNotificationSessionForProfile(org.osid.profile.ProfileEntryReceiver profileEntryReceiver, 
                                                                                                         org.osid.id.Id profileId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileManager.getProfileEntryNotificationSessionForProfile not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile entry 
     *  notification service for the given profile. 
     *
     *  @param  profileEntryReceiver the profile entry receiver interface 
     *  @param  profileId the profile entry receiver 
     *  @param  proxy a proxy 
     *  @return <code> a ProfileEntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> profileId </code> not found 
     *  @throws org.osid.NullArgumentException <code> profileEntryReceiver, 
     *          profileId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryNotificationSession getProfileEntryNotificationSessionForProfile(org.osid.profile.ProfileEntryReceiver profileEntryReceiver, 
                                                                                                         org.osid.id.Id profileId, 
                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileProxyManager.getProfileEntryNotificationSessionForProfile not implemented");
    }


    /**
     *  Gets the session for retrieving profile entry to profile mappings. 
     *
     *  @return a <code> ProfileEntryProfileSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryProfile() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryProfileSession getProfileEntryProfileSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileManager.getProfileEntryProfileSession not implemented");
    }


    /**
     *  Gets the session for retrieving profile entry to profile mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProfileEntryProfileSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryProfile() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryProfileSession getProfileEntryProfileSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileProxyManager.getProfileEntryProfileSession not implemented");
    }


    /**
     *  Gets the session for assigning profile entries to profile mappings. 
     *
     *  @return a <code> ProfileEntryProfileAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryProfileAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryProfileAssignmentSession getProfileEntryProfileAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileManager.getProfileEntryProfileAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning profile entry to profile mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProfileEntryProfileAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryProfileAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryProfileAssignmentSession getProfileEntryProfileAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileProxyManager.getProfileEntryProfileAssignmentSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic profile entry profiles. 
     *
     *  @param  profileId the <code> Id </code> of the profile 
     *  @return a <code> ProfileEntrySmartProfileSession </code> 
     *  @throws org.osid.NotFoundException <code> profileId </code> not found 
     *  @throws org.osid.NullArgumentException <code> profileId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntrySmartProfile() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntrySmartProfileSession getProfileEntrySmartProfileSession(org.osid.id.Id profileId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileManager.getProfileEntrySmartProfileSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic profile entry profile. 
     *
     *  @param  profileId the <code> Id </code> of the profile 
     *  @param  proxy a proxy 
     *  @return a <code> ProfileEntrySmartProfileSession </code> 
     *  @throws org.osid.NotFoundException <code> profileId </code> not found 
     *  @throws org.osid.NullArgumentException <code> profileId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntrySmartProfile() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntrySmartProfileSession getProfileEntrySmartProfileSession(org.osid.id.Id profileId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileProxyManager.getProfileEntrySmartProfileSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile item 
     *  lookup service. 
     *
     *  @return a <code> ProfileItemLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemLookupSession getProfileItemLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileManager.getProfileItemLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile item 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProfileItemLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> profileId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemLookupSession getProfileItemLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileProxyManager.getProfileItemLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile item 
     *  lookup service for the given profile. 
     *
     *  @param  profileId the <code> Id </code> of the profile 
     *  @return <code> a ProfileItemLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> profileId </code> not found 
     *  @throws org.osid.NullArgumentException <code> profileId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemLookupSession getProfileItemLookupSessionForProfile(org.osid.id.Id profileId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileManager.getProfileItemLookupSessionForProfile not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile item 
     *  lookup service for the given profile. 
     *
     *  @param  profileId the <code> Id </code> of the profile 
     *  @param  proxy a proxy 
     *  @return <code> a ProfileItemLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> profileId </code> not found 
     *  @throws org.osid.NullArgumentException <code> profileId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemLookupSession getProfileItemLookupSessionForProfile(org.osid.id.Id profileId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileProxyManager.getProfileItemLookupSessionForProfile not implemented");
    }


    /**
     *  Gets a profile item query session. 
     *
     *  @return <code> a ProfileItemQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemQuerySession getProfileItemQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileManager.getProfileItemQuerySession not implemented");
    }


    /**
     *  Gets a profile item query session. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a ProfileItemQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemQuerySession getProfileItemQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileProxyManager.getProfileItemQuerySession not implemented");
    }


    /**
     *  Gets a profile item query session for the given profile. 
     *
     *  @param  profileId the <code> Id </code> of the profile 
     *  @return <code> a ProfileItemQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> profileId </code> not found 
     *  @throws org.osid.NullArgumentException <code> profileId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemQuerySession getProfileItemQuerySessionForProfile(org.osid.id.Id profileId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileManager.getProfileItemQuerySessionForProfile not implemented");
    }


    /**
     *  Gets a profile item query session for the given profile. 
     *
     *  @param  profileId the <code> Id </code> of the profile 
     *  @param  proxy a proxy 
     *  @return <code> a ProfileItemQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> profileId </code> not found 
     *  @throws org.osid.NullArgumentException <code> profileId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemQuerySession getProfileItemQuerySessionForProfile(org.osid.id.Id profileId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileProxyManager.getProfileItemQuerySessionForProfile not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile item 
     *  search service. 
     *
     *  @return a <code> ProfileItemSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemSearchSession getProfileItemSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileManager.getProfileItemSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile item 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProfileItemSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemSearchSession getProfileItemSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileProxyManager.getProfileItemSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile item 
     *  search service for the given profile. 
     *
     *  @param  profileId the <code> Id </code> of the profile 
     *  @return a <code> ProfileItemSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> profileId </code> not found 
     *  @throws org.osid.NullArgumentException <code> profileId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemSearchSession getProfileItemSearchSessionForProfile(org.osid.id.Id profileId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileManager.getProfileItemSearchSessionForProfile not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile item 
     *  search service for the given profile. 
     *
     *  @param  profileId the <code> Id </code> of the profile 
     *  @param  proxy a proxy 
     *  @return a <code> ProfileItemSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> profileId </code> not found 
     *  @throws org.osid.NullArgumentException <code> profileId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemSearchSession getProfileItemSearchSessionForProfile(org.osid.id.Id profileId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileProxyManager.getProfileItemSearchSessionForProfile not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile item 
     *  administration service. 
     *
     *  @return a <code> ProfileItemAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemAdminSession getProfileItemAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileManager.getProfileItemAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile item 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProfileItemAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemAdminSession getProfileItemAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileProxyManager.getProfileItemAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile item 
     *  admin service for the given profile. 
     *
     *  @param  profileId the <code> Id </code> of the profile 
     *  @return <code> a ProfileItemAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> profileId </code> not found 
     *  @throws org.osid.NullArgumentException <code> profileId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemAdminSession getProfileItemAdminSessionForProfile(org.osid.id.Id profileId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileManager.getProfileItemAdminSessionForProfile not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile item 
     *  admin service for the given profile. 
     *
     *  @param  profileId the <code> Id </code> of the profile 
     *  @param  proxy a proxy 
     *  @return <code> a ProfileItemAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> profileId </code> not found 
     *  @throws org.osid.NullArgumentException <code> profileId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemAdminSession getProfileItemAdminSessionForProfile(org.osid.id.Id profileId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileProxyManager.getProfileItemAdminSessionForProfile not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to profile 
     *  item changes. 
     *
     *  @param  profileItemReceiver the profile item receiver 
     *  @return a <code> ProfileItemNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> profileItemReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemNotificationSession getProfileItemNotificationSession(org.osid.profile.ProfileItemReceiver profileItemReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileManager.getProfileItemNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to profile 
     *  item changes. 
     *
     *  @param  profileItemReceiver the profile item receiver 
     *  @param  proxy a proxy 
     *  @return a <code> ProfileItemNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> profileItemReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemNotificationSession getProfileItemNotificationSession(org.osid.profile.ProfileItemReceiver profileItemReceiver, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileProxyManager.getProfileItemNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile item 
     *  notification service for the given profile. 
     *
     *  @param  profileItemReceiver the profile item receiver 
     *  @param  profileId the <code> Id </code> of the profile 
     *  @return <code> a ProfileItemNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> profileId </code> not found 
     *  @throws org.osid.NullArgumentException <code> profileItemReceiver 
     *          </code> or <code> profileId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemNotificationSession getProfileItemNotificationSessionForProfile(org.osid.profile.ProfileItemReceiver profileItemReceiver, 
                                                                                                       org.osid.id.Id profileId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileManager.getProfileItemNotificationSessionForProfile not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the profile item 
     *  notification service for the given profile. 
     *
     *  @param  profileItemReceiver the profile item receiver 
     *  @param  profileId the <code> Id </code> of the profile 
     *  @param  proxy a proxy 
     *  @return <code> a ProfileItemNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> profileId </code> not found 
     *  @throws org.osid.NullArgumentException <code> profileItemReceiver, 
     *          profileId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemNotificationSession getProfileItemNotificationSessionForProfile(org.osid.profile.ProfileItemReceiver profileItemReceiver, 
                                                                                                       org.osid.id.Id profileId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileProxyManager.getProfileItemNotificationSessionForProfile not implemented");
    }


    /**
     *  Gets the session for retrieving profile item to profile mappings. 
     *
     *  @return a <code> ProfileItemProfileSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemProfile() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemProfileSession getProfileItemProfileSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileManager.getProfileItemProfileSession not implemented");
    }


    /**
     *  Gets the session for retrieving profile item to profile mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProfileItemProfileSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemProfile() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemProfileSession getProfileItemProfileSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileProxyManager.getProfileItemProfileSession not implemented");
    }


    /**
     *  Gets the session for assigning profile item to profile mappings. 
     *
     *  @return a <code> ProfileItemProfileAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemProfileAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemProfileSession getProfileItemProfileAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileManager.getProfileItemProfileAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning profile item to profile mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProfileItemProfileAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemProfileAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemProfileSession getProfileItemProfileAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileProxyManager.getProfileItemProfileAssignmentSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic profile item profile. 
     *
     *  @param  profileId the <code> Id </code> of the profile 
     *  @return a <code> ProfileItemSmartProfileSession </code> 
     *  @throws org.osid.NotFoundException <code> profileId </code> not found 
     *  @throws org.osid.NullArgumentException <code> profileId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemSmartProfile() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemSmartProfileSession getProfileItemSmartProfileSession(org.osid.id.Id profileId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileManager.getProfileItemSmartProfileSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic profile item profile. 
     *
     *  @param  profileId the <code> Id </code> of the profile 
     *  @param  proxy a proxy 
     *  @return a <code> ProfileItemSmartProfileSession </code> 
     *  @throws org.osid.NotFoundException <code> profileId </code> not found 
     *  @throws org.osid.NullArgumentException <code> profileId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemSmartProfile() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemSmartProfileSession getProfileItemSmartProfileSession(org.osid.id.Id profileId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileProxyManager.getProfileItemSmartProfileSession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the profile lookup service. 
     *
     *  @return a <code> ProfileLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProfileLookup() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileLookupSession getProfileLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileManager.getProfileLookupSession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the profile lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProfileLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProfileLookup() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileLookupSession getProfileLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileProxyManager.getProfileLookupSession not implemented");
    }


    /**
     *  Gets the profile query session. 
     *
     *  @return a <code> ProfileQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProfileQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileQuerySession getProfileQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileManager.getProfileQuerySession not implemented");
    }


    /**
     *  Gets the profile query session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProfileQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProfileQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileQuerySession getProfileQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileProxyManager.getProfileQuerySession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the profile search service. 
     *
     *  @return a <code> ProfileSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProfileSearch() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileSearchSession getProfileSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileManager.getProfileSearchSession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the profile search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProfileSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProfileSearch() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileSearchSession getProfileSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileProxyManager.getProfileSearchSession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the profile administration 
     *  service. 
     *
     *  @return a <code> ProfileAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProfileAdmin() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileAdminSession getProfileAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileManager.getProfileAdminSession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the profile administration 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProfileAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProfileAdmin() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileAdminSession getProfileAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileProxyManager.getProfileAdminSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to profile 
     *  service changes. 
     *
     *  @param  profileReceiver the profile receiver 
     *  @return a <code> ProfileNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> profileReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileNotification() is false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileNotificationSession getProfileNotificationSession(org.osid.profile.ProfileReceiver profileReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileManager.getProfileNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to profile 
     *  service changes. 
     *
     *  @param  profileReceiver the profile receiver 
     *  @param  proxy a proxy 
     *  @return a <code> ProfileNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> profileReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileNotification() is false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileNotificationSession getProfileNotificationSession(org.osid.profile.ProfileReceiver profileReceiver, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileProxyManager.getProfileNotificationSession not implemented");
    }


    /**
     *  Gets the session traversing profile hierarchies. 
     *
     *  @return a <code> ProfileHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileHierarchy() is false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileHierarchySession getProfileHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileManager.getProfileHierarchySession not implemented");
    }


    /**
     *  Gets the session traversing profile hierarchies. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProfileHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileHierarchy() is false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileHierarchySession getProfileHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileProxyManager.getProfileHierarchySession not implemented");
    }


    /**
     *  Gets the session designing profile hierarchies. 
     *
     *  @return a <code> ProfileHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileHierarchyDesign() is false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileHierarchyDesignSession getProfileHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileManager.getProfileHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the session designing profile hierarchies. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProfileHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileHierarchyDesign() is false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileHierarchyDesignSession getProfileHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileProxyManager.getProfileHierarchyDesignSession not implemented");
    }


    /**
     *  Gets a <code> ProfileBatchManager. </code> 
     *
     *  @return a <code> ProfileBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProfileBatch() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.profile.batch.ProfileBatchManager getProfileBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileManager.getProfileBatchManager not implemented");
    }


    /**
     *  Gets a <code> ProfileBatchProxyManager. </code> 
     *
     *  @return a <code> ProfileBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProfileBatch() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.profile.batch.ProfileBatchProxyManager getProfileBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileProxyManager.getProfileBatchProxyManager not implemented");
    }


    /**
     *  Gets a <code> ProfileRulesManager. </code> 
     *
     *  @return a <code> ProfileRulesManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProfileRules() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileRulesManager getProfileRulesManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileManager.getProfileRulesManager not implemented");
    }


    /**
     *  Gets a <code> ProfileRulesProxyManager. </code> 
     *
     *  @return a <code> ProfileRulesProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProfileRules() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.profile.rules.ProfileRulesProxyManager getProfileRulesProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.profile.ProfileProxyManager.getProfileRulesProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.profileEntryRecordTypes.clear();
        this.profileEntryRecordTypes.clear();

        this.profileEntrySearchRecordTypes.clear();
        this.profileEntrySearchRecordTypes.clear();

        this.profileItemRecordTypes.clear();
        this.profileItemRecordTypes.clear();

        this.profileItemSearchRecordTypes.clear();
        this.profileItemSearchRecordTypes.clear();

        this.profileRecordTypes.clear();
        this.profileRecordTypes.clear();

        this.profileSearchRecordTypes.clear();
        this.profileSearchRecordTypes.clear();

        this.profileEntryConditionRecordTypes.clear();
        this.profileEntryConditionRecordTypes.clear();

        return;
    }
}
