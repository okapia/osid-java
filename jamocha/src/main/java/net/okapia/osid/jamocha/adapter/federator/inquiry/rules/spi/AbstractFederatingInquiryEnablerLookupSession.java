//
// AbstractFederatingInquiryEnablerLookupSession.java
//
//     An abstract federating adapter for an InquiryEnablerLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.inquiry.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  InquiryEnablerLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingInquiryEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.inquiry.rules.InquiryEnablerLookupSession>
    implements org.osid.inquiry.rules.InquiryEnablerLookupSession {

    private boolean parallel = false;
    private org.osid.inquiry.Inquest inquest = new net.okapia.osid.jamocha.nil.inquiry.inquest.UnknownInquest();


    /**
     *  Constructs a new <code>AbstractFederatingInquiryEnablerLookupSession</code>.
     */

    protected AbstractFederatingInquiryEnablerLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.inquiry.rules.InquiryEnablerLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Inquest/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Inquest Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getInquestId() {
        return (this.inquest.getId());
    }


    /**
     *  Gets the <code>Inquest</code> associated with this 
     *  session.
     *
     *  @return the <code>Inquest</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.Inquest getInquest()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.inquest);
    }


    /**
     *  Sets the <code>Inquest</code>.
     *
     *  @param  inquest the inquest for this session
     *  @throws org.osid.NullArgumentException <code>inquest</code>
     *          is <code>null</code>
     */

    protected void setInquest(org.osid.inquiry.Inquest inquest) {
        nullarg(inquest, "inquest");
        this.inquest = inquest;
        return;
    }


    /**
     *  Tests if this user can perform <code>InquiryEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupInquiryEnablers() {
        for (org.osid.inquiry.rules.InquiryEnablerLookupSession session : getSessions()) {
            if (session.canLookupInquiryEnablers()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>InquiryEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeInquiryEnablerView() {
        for (org.osid.inquiry.rules.InquiryEnablerLookupSession session : getSessions()) {
            session.useComparativeInquiryEnablerView();
        }

        return;
    }


    /**
     *  A complete view of the <code>InquiryEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryInquiryEnablerView() {
        for (org.osid.inquiry.rules.InquiryEnablerLookupSession session : getSessions()) {
            session.usePlenaryInquiryEnablerView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include inquiry enablers in inquests which are children
     *  of this inquest in the inquest hierarchy.
     */

    @OSID @Override
    public void useFederatedInquestView() {
        for (org.osid.inquiry.rules.InquiryEnablerLookupSession session : getSessions()) {
            session.useFederatedInquestView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this inquest only.
     */

    @OSID @Override
    public void useIsolatedInquestView() {
        for (org.osid.inquiry.rules.InquiryEnablerLookupSession session : getSessions()) {
            session.useIsolatedInquestView();
        }

        return;
    }


    /**
     *  Only active inquiry enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveInquiryEnablerView() {
        for (org.osid.inquiry.rules.InquiryEnablerLookupSession session : getSessions()) {
            session.useActiveInquiryEnablerView();
        }

        return;
    }


    /**
     *  Active and inactive inquiry enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusInquiryEnablerView() {
        for (org.osid.inquiry.rules.InquiryEnablerLookupSession session : getSessions()) {
            session.useAnyStatusInquiryEnablerView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>InquiryEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>InquiryEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>InquiryEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, inquiry enablers are returned that are currently
     *  active. In any status mode, active and inactive inquiry enablers
     *  are returned.
     *
     *  @param  inquiryEnablerId <code>Id</code> of the
     *          <code>InquiryEnabler</code>
     *  @return the inquiry enabler
     *  @throws org.osid.NotFoundException <code>inquiryEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>inquiryEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnabler getInquiryEnabler(org.osid.id.Id inquiryEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.inquiry.rules.InquiryEnablerLookupSession session : getSessions()) {
            try {
                return (session.getInquiryEnabler(inquiryEnablerId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(inquiryEnablerId + " not found");
    }


    /**
     *  Gets an <code>InquiryEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  inquiryEnablers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>InquiryEnablers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, inquiry enablers are returned that are currently
     *  active. In any status mode, active and inactive inquiry enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getInquiryEnablers()</code>.
     *
     *  @param  inquiryEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>InquiryEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>inquiryEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerList getInquiryEnablersByIds(org.osid.id.IdList inquiryEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.inquiry.rules.inquiryenabler.MutableInquiryEnablerList ret = new net.okapia.osid.jamocha.inquiry.rules.inquiryenabler.MutableInquiryEnablerList();

        try (org.osid.id.IdList ids = inquiryEnablerIds) {
            while (ids.hasNext()) {
                ret.addInquiryEnabler(getInquiryEnabler(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>InquiryEnablerList</code> corresponding to the given
     *  inquiry enabler genus <code>Type</code> which does not include
     *  inquiry enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  inquiry enablers or an error results. Otherwise, the returned list
     *  may contain only those inquiry enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, inquiry enablers are returned that are currently
     *  active. In any status mode, active and inactive inquiry enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getInquiryEnablers()</code>.
     *
     *  @param  inquiryEnablerGenusType an inquiryEnabler genus type 
     *  @return the returned <code>InquiryEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>inquiryEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerList getInquiryEnablersByGenusType(org.osid.type.Type inquiryEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inquiry.rules.inquiryenabler.FederatingInquiryEnablerList ret = getInquiryEnablerList();

        for (org.osid.inquiry.rules.InquiryEnablerLookupSession session : getSessions()) {
            ret.addInquiryEnablerList(session.getInquiryEnablersByGenusType(inquiryEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>InquiryEnablerList</code> corresponding to the given
     *  inquiry enabler genus <code>Type</code> and include any additional
     *  inquiry enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  inquiry enablers or an error results. Otherwise, the returned list
     *  may contain only those inquiry enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, inquiry enablers are returned that are currently
     *  active. In any status mode, active and inactive inquiry enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getInquiryEnablers()</code>.
     *
     *  @param  inquiryEnablerGenusType an inquiryEnabler genus type 
     *  @return the returned <code>InquiryEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>inquiryEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerList getInquiryEnablersByParentGenusType(org.osid.type.Type inquiryEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inquiry.rules.inquiryenabler.FederatingInquiryEnablerList ret = getInquiryEnablerList();

        for (org.osid.inquiry.rules.InquiryEnablerLookupSession session : getSessions()) {
            ret.addInquiryEnablerList(session.getInquiryEnablersByParentGenusType(inquiryEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>InquiryEnablerList</code> containing the given
     *  inquiry enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  inquiry enablers or an error results. Otherwise, the returned list
     *  may contain only those inquiry enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, inquiry enablers are returned that are currently
     *  active. In any status mode, active and inactive inquiry enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getInquiryEnablers()</code>.
     *
     *  @param  inquiryEnablerRecordType an inquiryEnabler record type 
     *  @return the returned <code>InquiryEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>inquiryEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerList getInquiryEnablersByRecordType(org.osid.type.Type inquiryEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inquiry.rules.inquiryenabler.FederatingInquiryEnablerList ret = getInquiryEnablerList();

        for (org.osid.inquiry.rules.InquiryEnablerLookupSession session : getSessions()) {
            ret.addInquiryEnablerList(session.getInquiryEnablersByRecordType(inquiryEnablerRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>InquiryEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  inquiry enablers or an error results. Otherwise, the returned list
     *  may contain only those inquiry enablers that are accessible
     *  through this session.
     *  
     *  In active mode, inquiry enablers are returned that are currently
     *  active. In any status mode, active and inactive inquiry enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>InquiryEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerList getInquiryEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inquiry.rules.inquiryenabler.FederatingInquiryEnablerList ret = getInquiryEnablerList();

        for (org.osid.inquiry.rules.InquiryEnablerLookupSession session : getSessions()) {
            ret.addInquiryEnablerList(session.getInquiryEnablersOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets an <code>InquiryEnablerList </code> which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  inquiry enablers or an error results. Otherwise, the returned list
     *  may contain only those inquiry enablers that are accessible
     *  through this session.
     *
     *  In active mode, inquiry enablers are returned that are currently
     *  active. In any status mode, active and inactive inquiry enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>InquiryEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerList getInquiryEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                                       org.osid.calendaring.DateTime from,
                                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inquiry.rules.inquiryenabler.FederatingInquiryEnablerList ret = getInquiryEnablerList();

        for (org.osid.inquiry.rules.InquiryEnablerLookupSession session : getSessions()) {
            ret.addInquiryEnablerList(session.getInquiryEnablersOnDateWithAgent(agentId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>InquiryEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  inquiry enablers or an error results. Otherwise, the returned list
     *  may contain only those inquiry enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, inquiry enablers are returned that are currently
     *  active. In any status mode, active and inactive inquiry enablers
     *  are returned.
     *
     *  @return a list of <code>InquiryEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerList getInquiryEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inquiry.rules.inquiryenabler.FederatingInquiryEnablerList ret = getInquiryEnablerList();

        for (org.osid.inquiry.rules.InquiryEnablerLookupSession session : getSessions()) {
            ret.addInquiryEnablerList(session.getInquiryEnablers());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.inquiry.rules.inquiryenabler.FederatingInquiryEnablerList getInquiryEnablerList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.inquiry.rules.inquiryenabler.ParallelInquiryEnablerList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.inquiry.rules.inquiryenabler.CompositeInquiryEnablerList());
        }
    }
}
