//
// AwardRequirementElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.requisite.awardrequirement.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class AwardRequirementElements
    extends net.okapia.osid.jamocha.spi.OsidRuleElements {


    /**
     *  Gets the AwardRequirementElement Id.
     *
     *  @return the award requirement element Id
     */

    public static org.osid.id.Id getAwardRequirementEntityId() {
        return (makeEntityId("osid.course.requisite.AwardRequirement"));
    }


    /**
     *  Gets the AltRequisites element Id.
     *
     *  @return the AltRequisites element Id
     */

    public static org.osid.id.Id getAltRequisites() {
        return (makeElementId("osid.course.requisite.awardrequirement.AltRequisites"));
    }


    /**
     *  Gets the AwardId element Id.
     *
     *  @return the AwardId element Id
     */

    public static org.osid.id.Id getAwardId() {
        return (makeElementId("osid.course.requisite.awardrequirement.AwardId"));
    }


    /**
     *  Gets the Award element Id.
     *
     *  @return the Award element Id
     */

    public static org.osid.id.Id getAward() {
        return (makeElementId("osid.course.requisite.awardrequirement.Award"));
    }


    /**
     *  Gets the Timeframe element Id.
     *
     *  @return the Timeframe element Id
     */

    public static org.osid.id.Id getTimeframe() {
        return (makeElementId("osid.course.requisite.awardrequirement.Timeframe"));
    }
}
