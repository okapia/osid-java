//
// AbstractIndexedMapCourseOfferingLookupSession.java
//
//    A simple framework for providing a CourseOffering lookup service
//    backed by a fixed collection of course offerings with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a CourseOffering lookup service backed by a
 *  fixed collection of course offerings. The course offerings are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some course offerings may be compatible
 *  with more types than are indicated through these course offering
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>CourseOfferings</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapCourseOfferingLookupSession
    extends AbstractMapCourseOfferingLookupSession
    implements org.osid.course.CourseOfferingLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.course.CourseOffering> courseOfferingsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.CourseOffering>());
    private final MultiMap<org.osid.type.Type, org.osid.course.CourseOffering> courseOfferingsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.CourseOffering>());


    /**
     *  Makes a <code>CourseOffering</code> available in this session.
     *
     *  @param  courseOffering a course offering
     *  @throws org.osid.NullArgumentException <code>courseOffering<code> is
     *          <code>null</code>
     */

    @Override
    protected void putCourseOffering(org.osid.course.CourseOffering courseOffering) {
        super.putCourseOffering(courseOffering);

        this.courseOfferingsByGenus.put(courseOffering.getGenusType(), courseOffering);
        
        try (org.osid.type.TypeList types = courseOffering.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.courseOfferingsByRecord.put(types.getNextType(), courseOffering);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a course offering from this session.
     *
     *  @param courseOfferingId the <code>Id</code> of the course offering
     *  @throws org.osid.NullArgumentException <code>courseOfferingId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeCourseOffering(org.osid.id.Id courseOfferingId) {
        org.osid.course.CourseOffering courseOffering;
        try {
            courseOffering = getCourseOffering(courseOfferingId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.courseOfferingsByGenus.remove(courseOffering.getGenusType());

        try (org.osid.type.TypeList types = courseOffering.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.courseOfferingsByRecord.remove(types.getNextType(), courseOffering);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeCourseOffering(courseOfferingId);
        return;
    }


    /**
     *  Gets a <code>CourseOfferingList</code> corresponding to the given
     *  course offering genus <code>Type</code> which does not include
     *  course offerings of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known course offerings or an error results. Otherwise,
     *  the returned list may contain only those course offerings that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  courseOfferingGenusType a course offering genus type 
     *  @return the returned <code>CourseOffering</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>courseOfferingGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingList getCourseOfferingsByGenusType(org.osid.type.Type courseOfferingGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.courseoffering.ArrayCourseOfferingList(this.courseOfferingsByGenus.get(courseOfferingGenusType)));
    }


    /**
     *  Gets a <code>CourseOfferingList</code> containing the given
     *  course offering record <code>Type</code>. In plenary mode, the
     *  returned list contains all known course offerings or an error
     *  results. Otherwise, the returned list may contain only those
     *  course offerings that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  courseOfferingRecordType a course offering record type 
     *  @return the returned <code>courseOffering</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>courseOfferingRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingList getCourseOfferingsByRecordType(org.osid.type.Type courseOfferingRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.courseoffering.ArrayCourseOfferingList(this.courseOfferingsByRecord.get(courseOfferingRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.courseOfferingsByGenus.clear();
        this.courseOfferingsByRecord.clear();

        super.close();

        return;
    }
}
