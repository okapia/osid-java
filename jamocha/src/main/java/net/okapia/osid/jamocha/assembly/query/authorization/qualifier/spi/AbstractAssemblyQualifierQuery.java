//
// AbstractAssemblyQualifierQuery.java
//
//     A QualifierQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.authorization.qualifier.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A QualifierQuery that stores terms.
 */

public abstract class AbstractAssemblyQualifierQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.authorization.QualifierQuery,
               org.osid.authorization.QualifierQueryInspector,
               org.osid.authorization.QualifierSearchOrder {

    private final java.util.Collection<org.osid.authorization.records.QualifierQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.authorization.records.QualifierQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.authorization.records.QualifierSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyQualifierQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyQualifierQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the qualifier hierarchy <code> Id </code> for this query. 
     *
     *  @param  qualifierHierarchyId a hierarchy <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> qualifierHierarchyId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchQualifierHierarchyId(org.osid.id.Id qualifierHierarchyId, 
                                          boolean match) {
        getAssembler().addIdTerm(getQualifierHierarchyIdColumn(), qualifierHierarchyId, match);
        return;
    }


    /**
     *  Clears the qualifier hierarchy <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearQualifierHierarchyIdTerms() {
        getAssembler().clearTerms(getQualifierHierarchyIdColumn());
        return;
    }


    /**
     *  Gets the qualifier hierarchy <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getQualifierHierarchyIdTerms() {
        return (getAssembler().getIdTerms(getQualifierHierarchyIdColumn()));
    }


    /**
     *  Gets the QualifierHierarchyId column name.
     *
     * @return the column name
     */

    protected String getQualifierHierarchyIdColumn() {
        return ("qualifier_hierarchy_id");
    }


    /**
     *  Tests if a <code> HierarchyQuery </code> is available. 
     *
     *  @return <code> true </code> if a qualifier hierarchy query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQualifierHierarchyQuery() {
        return (false);
    }


    /**
     *  Gets the query for a qualifier hierarchy. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the qualifier hierarchy query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierHierarchyQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyQuery getQualifierHierarchyQuery() {
        throw new org.osid.UnimplementedException("supportsQualifierHierarchyQuery() is false");
    }


    /**
     *  Clears the qualifier hierarchy query terms. 
     */

    @OSID @Override
    public void clearQualifierHierarchyTerms() {
        getAssembler().clearTerms(getQualifierHierarchyColumn());
        return;
    }


    /**
     *  Gets the qualifier hierarchy query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyQueryInspector[] getQualifierHierarchyTerms() {
        return (new org.osid.hierarchy.HierarchyQueryInspector[0]);
    }


    /**
     *  Gets the QualifierHierarchy column name.
     *
     * @return the column name
     */

    protected String getQualifierHierarchyColumn() {
        return ("qualifier_hierarchy");
    }


    /**
     *  Sets the authorization <code> Id </code> for this query. 
     *
     *  @param  authorizationId an authorization <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> authorizationId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchAuthorizationId(org.osid.id.Id authorizationId, 
                                     boolean match) {
        getAssembler().addIdTerm(getAuthorizationIdColumn(), authorizationId, match);
        return;
    }


    /**
     *  Clears the authorization <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAuthorizationIdTerms() {
        getAssembler().clearTerms(getAuthorizationIdColumn());
        return;
    }


    /**
     *  Gets the authorization <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAuthorizationIdTerms() {
        return (getAssembler().getIdTerms(getAuthorizationIdColumn()));
    }


    /**
     *  Gets the AuthorizationId column name.
     *
     * @return the column name
     */

    protected String getAuthorizationIdColumn() {
        return ("authorization_id");
    }


    /**
     *  Tests if an <code> AuthorizationQuery </code> is available. 
     *
     *  @return <code> true </code> if an authorization query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizationQuery() {
        return (false);
    }


    /**
     *  Gets the query for an authorization. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the authorization query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationQuery getAuthorizationQuery() {
        throw new org.osid.UnimplementedException("supportsAuthorizationQuery() is false");
    }


    /**
     *  Matches qualifiers that have any authorization mapping. 
     *
     *  @param  match <code> true </code> to match qualifiers with any 
     *          authorization mapping, <code> false </code> to match 
     *          qualifiers with no authorization mapping 
     */

    @OSID @Override
    public void matchAnyAuthorization(boolean match) {
        getAssembler().addIdWildcardTerm(getAuthorizationColumn(), match);
        return;
    }


    /**
     *  Clears the authorization query terms. 
     */

    @OSID @Override
    public void clearAuthorizationTerms() {
        getAssembler().clearTerms(getAuthorizationColumn());
        return;
    }


    /**
     *  Gets the authorization query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationQueryInspector[] getAuthorizationTerms() {
        return (new org.osid.authorization.AuthorizationQueryInspector[0]);
    }


    /**
     *  Gets the Authorization column name.
     *
     * @return the column name
     */

    protected String getAuthorizationColumn() {
        return ("authorization");
    }


    /**
     *  Sets the qualifier <code> Id </code> for this query to match 
     *  qualifiers that have the specified qualifier as an ancestor. 
     *
     *  @param  qualifierId a qualifier <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> qualifierId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorQualifierId(org.osid.id.Id qualifierId, 
                                         boolean match) {
        getAssembler().addIdTerm(getAncestorQualifierIdColumn(), qualifierId, match);
        return;
    }


    /**
     *  Clears the ancestor qualifier <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorQualifierIdTerms() {
        getAssembler().clearTerms(getAncestorQualifierIdColumn());
        return;
    }


    /**
     *  Gets the ancestor qualifier <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorQualifierIdTerms() {
        return (getAssembler().getIdTerms(getAncestorQualifierIdColumn()));
    }


    /**
     *  Gets the AncestorQualifierId column name.
     *
     * @return the column name
     */

    protected String getAncestorQualifierIdColumn() {
        return ("ancestor_qualifier_id");
    }


    /**
     *  Tests if a <code> QualifierQuery </code> is available. 
     *
     *  @return <code> true </code> if a qualifier query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorQualifierQuery() {
        return (false);
    }


    /**
     *  Gets the query for a qualifier. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the qualifier query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorQualifierQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierQuery getAncestorQualifierQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorQualifierQuery() is false");
    }


    /**
     *  Matches qualifiers that have any ancestor. 
     *
     *  @param  match <code> true </code> to match qualifiers with any 
     *          ancestor, <code> false </code> to match root qualifiers 
     */

    @OSID @Override
    public void matchAnyAncestorQualifier(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorQualifierColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor qualifier query terms. 
     */

    @OSID @Override
    public void clearAncestorQualifierTerms() {
        getAssembler().clearTerms(getAncestorQualifierColumn());
        return;
    }


    /**
     *  Gets the ancestor qualifier query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authorization.FunctionQueryInspector[] getAncestorQualifierTerms() {
        return (new org.osid.authorization.FunctionQueryInspector[0]);
    }


    /**
     *  Gets the AncestorQualifier column name.
     *
     * @return the column name
     */

    protected String getAncestorQualifierColumn() {
        return ("ancestor_qualifier");
    }


    /**
     *  Sets the qualifier <code> Id </code> for this query to match 
     *  qualifiers that have the specified qualifier as a descendant. 
     *
     *  @param  qualifierId a qualifier <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> qualifierId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantQualifierId(org.osid.id.Id qualifierId, 
                                           boolean match) {
        getAssembler().addIdTerm(getDescendantQualifierIdColumn(), qualifierId, match);
        return;
    }


    /**
     *  Clears the descendant qualifier <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantQualifierIdTerms() {
        getAssembler().clearTerms(getDescendantQualifierIdColumn());
        return;
    }


    /**
     *  Gets the descendant qualifier <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantQualifierIdTerms() {
        return (getAssembler().getIdTerms(getDescendantQualifierIdColumn()));
    }


    /**
     *  Gets the DescendantQualifierId column name.
     *
     * @return the column name
     */

    protected String getDescendantQualifierIdColumn() {
        return ("descendant_qualifier_id");
    }


    /**
     *  Tests if a <code> QualifierQuery </code> is available. 
     *
     *  @return <code> true </code> if a qualifier query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantQualifierQuery() {
        return (false);
    }


    /**
     *  Gets the query for a qualifier. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the qualifier query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantQualifierQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierQuery getDescendantQualifierQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantQualifierQuery() is false");
    }


    /**
     *  Matches qualifiers that have any ancestor. 
     *
     *  @param  match <code> true </code> to match qualifiers with any 
     *          ancestor, <code> false </code> to match leaf qualifiers 
     */

    @OSID @Override
    public void matchAnyDescendantQualifier(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantQualifierColumn(), match);
        return;
    }


    /**
     *  Clears the descendant qualifier query terms. 
     */

    @OSID @Override
    public void clearDescendantQualifierTerms() {
        getAssembler().clearTerms(getDescendantQualifierColumn());
        return;
    }


    /**
     *  Gets the descendant qualifier query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authorization.FunctionQueryInspector[] getDescendantQualifierTerms() {
        return (new org.osid.authorization.FunctionQueryInspector[0]);
    }


    /**
     *  Gets the DescendantQualifier column name.
     *
     * @return the column name
     */

    protected String getDescendantQualifierColumn() {
        return ("descendant_qualifier");
    }


    /**
     *  Sets the vault <code> Id </code> for this query. 
     *
     *  @param  vaultId a vault <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchVaultId(org.osid.id.Id vaultId, boolean match) {
        getAssembler().addIdTerm(getVaultIdColumn(), vaultId, match);
        return;
    }


    /**
     *  Clears the vault <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearVaultIdTerms() {
        getAssembler().clearTerms(getVaultIdColumn());
        return;
    }


    /**
     *  Gets the vault <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getVaultIdTerms() {
        return (getAssembler().getIdTerms(getVaultIdColumn()));
    }


    /**
     *  Gets the VaultId column name.
     *
     * @return the column name
     */

    protected String getVaultIdColumn() {
        return ("vault_id");
    }


    /**
     *  Tests if a <code> VaultQuery </code> is available. 
     *
     *  @return <code> true </code> if a vault query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVaultQuery() {
        return (false);
    }


    /**
     *  Gets the query for a vault. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the vault query 
     *  @throws org.osid.UnimplementedException <code> supportsVaultQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.VaultQuery getVaultQuery() {
        throw new org.osid.UnimplementedException("supportsVaultQuery() is false");
    }


    /**
     *  Clears the vault query terms. 
     */

    @OSID @Override
    public void clearVaultTerms() {
        getAssembler().clearTerms(getVaultColumn());
        return;
    }


    /**
     *  Gets the vault query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authorization.VaultQueryInspector[] getVaultTerms() {
        return (new org.osid.authorization.VaultQueryInspector[0]);
    }


    /**
     *  Gets the Vault column name.
     *
     * @return the column name
     */

    protected String getVaultColumn() {
        return ("vault");
    }


    /**
     *  Tests if this qualifier supports the given record
     *  <code>Type</code>.
     *
     *  @param  qualifierRecordType a qualifier record type 
     *  @return <code>true</code> if the qualifierRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>qualifierRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type qualifierRecordType) {
        for (org.osid.authorization.records.QualifierQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(qualifierRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  qualifierRecordType the qualifier record type 
     *  @return the qualifier query record 
     *  @throws org.osid.NullArgumentException
     *          <code>qualifierRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(qualifierRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.authorization.records.QualifierQueryRecord getQualifierQueryRecord(org.osid.type.Type qualifierRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authorization.records.QualifierQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(qualifierRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(qualifierRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  qualifierRecordType the qualifier record type 
     *  @return the qualifier query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>qualifierRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(qualifierRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.authorization.records.QualifierQueryInspectorRecord getQualifierQueryInspectorRecord(org.osid.type.Type qualifierRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authorization.records.QualifierQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(qualifierRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(qualifierRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param qualifierRecordType the qualifier record type
     *  @return the qualifier search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>qualifierRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(qualifierRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.authorization.records.QualifierSearchOrderRecord getQualifierSearchOrderRecord(org.osid.type.Type qualifierRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authorization.records.QualifierSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(qualifierRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(qualifierRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this qualifier. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param qualifierQueryRecord the qualifier query record
     *  @param qualifierQueryInspectorRecord the qualifier query inspector
     *         record
     *  @param qualifierSearchOrderRecord the qualifier search order record
     *  @param qualifierRecordType qualifier record type
     *  @throws org.osid.NullArgumentException
     *          <code>qualifierQueryRecord</code>,
     *          <code>qualifierQueryInspectorRecord</code>,
     *          <code>qualifierSearchOrderRecord</code> or
     *          <code>qualifierRecordTypequalifier</code> is
     *          <code>null</code>
     */
            
    protected void addQualifierRecords(org.osid.authorization.records.QualifierQueryRecord qualifierQueryRecord, 
                                      org.osid.authorization.records.QualifierQueryInspectorRecord qualifierQueryInspectorRecord, 
                                      org.osid.authorization.records.QualifierSearchOrderRecord qualifierSearchOrderRecord, 
                                      org.osid.type.Type qualifierRecordType) {

        addRecordType(qualifierRecordType);

        nullarg(qualifierQueryRecord, "qualifier query record");
        nullarg(qualifierQueryInspectorRecord, "qualifier query inspector record");
        nullarg(qualifierSearchOrderRecord, "qualifier search odrer record");

        this.queryRecords.add(qualifierQueryRecord);
        this.queryInspectorRecords.add(qualifierQueryInspectorRecord);
        this.searchOrderRecords.add(qualifierSearchOrderRecord);
        
        return;
    }
}
