//
// AbstractIndexedMapResponseLookupSession.java
//
//    A simple framework for providing a Response lookup service
//    backed by a fixed collection of responses with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inquiry.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Response lookup service backed by a
 *  fixed collection of responses. The responses are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some responses may be compatible
 *  with more types than are indicated through these response
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Responses</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapResponseLookupSession
    extends AbstractMapResponseLookupSession
    implements org.osid.inquiry.ResponseLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.inquiry.Response> responsesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.inquiry.Response>());
    private final MultiMap<org.osid.type.Type, org.osid.inquiry.Response> responsesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.inquiry.Response>());


    /**
     *  Makes a <code>Response</code> available in this session.
     *
     *  @param  response a response
     *  @throws org.osid.NullArgumentException <code>response<code> is
     *          <code>null</code>
     */

    @Override
    protected void putResponse(org.osid.inquiry.Response response) {
        super.putResponse(response);

        this.responsesByGenus.put(response.getGenusType(), response);
        
        try (org.osid.type.TypeList types = response.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.responsesByRecord.put(types.getNextType(), response);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }

    
    /**
     *  Makes an array of responses available in this session.
     *
     *  @param  responses an array of responses
     *  @throws org.osid.NullArgumentException <code>responses<code>
     *          is <code>null</code>
     */

    @Override
    protected void putResponses(org.osid.inquiry.Response[] responses) {
        for (org.osid.inquiry.Response response : responses) {
            putResponse(response);
        }

        return;
    }


    /**
     *  Makes a collection of responses available in this session.
     *
     *  @param  responses a collection of responses
     *  @throws org.osid.NullArgumentException <code>responses<code>
     *          is <code>null</code>
     */

    @Override
    protected void putResponses(java.util.Collection<? extends org.osid.inquiry.Response> responses) {
        for (org.osid.inquiry.Response response : responses) {
            putResponse(response);
        }

        return;
    }


    /**
     *  Removes a response from this session.
     *
     *  @param responseId the <code>Id</code> of the response
     *  @throws org.osid.NullArgumentException <code>responseId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeResponse(org.osid.id.Id responseId) {
        org.osid.inquiry.Response response;
        try {
            response = getResponse(responseId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.responsesByGenus.remove(response.getGenusType());

        try (org.osid.type.TypeList types = response.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.responsesByRecord.remove(types.getNextType(), response);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeResponse(responseId);
        return;
    }


    /**
     *  Gets a <code>ResponseList</code> corresponding to the given
     *  response genus <code>Type</code> which does not include
     *  responses of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known responses or an error results. Otherwise,
     *  the returned list may contain only those responses that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  responseGenusType a response genus type 
     *  @return the returned <code>Response</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>responseGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseList getResponsesByGenusType(org.osid.type.Type responseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inquiry.response.ArrayResponseList(this.responsesByGenus.get(responseGenusType)));
    }


    /**
     *  Gets a <code>ResponseList</code> containing the given
     *  response record <code>Type</code>. In plenary mode, the
     *  returned list contains all known responses or an error
     *  results. Otherwise, the returned list may contain only those
     *  responses that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  responseRecordType a response record type 
     *  @return the returned <code>response</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>responseRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseList getResponsesByRecordType(org.osid.type.Type responseRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inquiry.response.ArrayResponseList(this.responsesByRecord.get(responseRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.responsesByGenus.clear();
        this.responsesByRecord.clear();

        super.close();

        return;
    }
}
