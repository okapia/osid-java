//
// AbstractCompositionEnabler.java
//
//     Defines a CompositionEnabler builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.repository.rules.compositionenabler.spi;


/**
 *  Defines a <code>CompositionEnabler</code> builder.
 */

public abstract class AbstractCompositionEnablerBuilder<T extends AbstractCompositionEnablerBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidEnablerBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.repository.rules.compositionenabler.CompositionEnablerMiter compositionEnabler;


    /**
     *  Constructs a new <code>AbstractCompositionEnablerBuilder</code>.
     *
     *  @param compositionEnabler the composition enabler to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractCompositionEnablerBuilder(net.okapia.osid.jamocha.builder.repository.rules.compositionenabler.CompositionEnablerMiter compositionEnabler) {
        super(compositionEnabler);
        this.compositionEnabler = compositionEnabler;
        return;
    }


    /**
     *  Builds the composition enabler.
     *
     *  @return the new composition enabler
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.repository.rules.CompositionEnabler build() {
        (new net.okapia.osid.jamocha.builder.validator.repository.rules.compositionenabler.CompositionEnablerValidator(getValidations())).validate(this.compositionEnabler);
        return (new net.okapia.osid.jamocha.builder.repository.rules.compositionenabler.ImmutableCompositionEnabler(this.compositionEnabler));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the composition enabler miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.repository.rules.compositionenabler.CompositionEnablerMiter getMiter() {
        return (this.compositionEnabler);
    }


    /**
     *  Adds a CompositionEnabler record.
     *
     *  @param record a composition enabler record
     *  @param recordType the type of composition enabler record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.repository.rules.records.CompositionEnablerRecord record, org.osid.type.Type recordType) {
        getMiter().addCompositionEnablerRecord(record, recordType);
        return (self());
    }
}       


