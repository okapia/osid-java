//
// MutableMapProxyAssessmentPartLookupSession
//
//    Implements an AssessmentPart lookup service backed by a collection of
//    assessmentParts that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.assessment.authoring;


/**
 *  Implements an AssessmentPart lookup service backed by a collection of
 *  assessmentParts. The assessmentParts are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of assessment parts can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyAssessmentPartLookupSession
    extends net.okapia.osid.jamocha.core.assessment.authoring.spi.AbstractMapAssessmentPartLookupSession
    implements org.osid.assessment.authoring.AssessmentPartLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyAssessmentPartLookupSession}
     *  with no assessment parts.
     *
     *  @param bank the bank
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bank} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyAssessmentPartLookupSession(org.osid.assessment.Bank bank,
                                                  org.osid.proxy.Proxy proxy) {
        setBank(bank);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyAssessmentPartLookupSession} with a
     *  single assessment part.
     *
     *  @param bank the bank
     *  @param assessmentPart an assessment part
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bank},
     *          {@code assessmentPart}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyAssessmentPartLookupSession(org.osid.assessment.Bank bank,
                                                org.osid.assessment.authoring.AssessmentPart assessmentPart, org.osid.proxy.Proxy proxy) {
        this(bank, proxy);
        putAssessmentPart(assessmentPart);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyAssessmentPartLookupSession} using an
     *  array of assessment parts.
     *
     *  @param bank the bank
     *  @param assessmentParts an array of assessment parts
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bank},
     *          {@code assessmentParts}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyAssessmentPartLookupSession(org.osid.assessment.Bank bank,
                                                org.osid.assessment.authoring.AssessmentPart[] assessmentParts, org.osid.proxy.Proxy proxy) {
        this(bank, proxy);
        putAssessmentParts(assessmentParts);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyAssessmentPartLookupSession} using a
     *  collection of assessment parts.
     *
     *  @param bank the bank
     *  @param assessmentParts a collection of assessment parts
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bank},
     *          {@code assessmentParts}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyAssessmentPartLookupSession(org.osid.assessment.Bank bank,
                                                java.util.Collection<? extends org.osid.assessment.authoring.AssessmentPart> assessmentParts,
                                                org.osid.proxy.Proxy proxy) {
   
        this(bank, proxy);
        setSessionProxy(proxy);
        putAssessmentParts(assessmentParts);
        return;
    }

    
    /**
     *  Makes a {@code AssessmentPart} available in this session.
     *
     *  @param assessmentPart an assessment part
     *  @throws org.osid.NullArgumentException {@code assessmentPart{@code 
     *          is {@code null}
     */

    @Override
    public void putAssessmentPart(org.osid.assessment.authoring.AssessmentPart assessmentPart) {
        super.putAssessmentPart(assessmentPart);
        return;
    }


    /**
     *  Makes an array of assessmentParts available in this session.
     *
     *  @param assessmentParts an array of assessment parts
     *  @throws org.osid.NullArgumentException {@code assessmentParts{@code 
     *          is {@code null}
     */

    @Override
    public void putAssessmentParts(org.osid.assessment.authoring.AssessmentPart[] assessmentParts) {
        super.putAssessmentParts(assessmentParts);
        return;
    }


    /**
     *  Makes collection of assessment parts available in this session.
     *
     *  @param assessmentParts
     *  @throws org.osid.NullArgumentException {@code assessmentPart{@code 
     *          is {@code null}
     */

    @Override
    public void putAssessmentParts(java.util.Collection<? extends org.osid.assessment.authoring.AssessmentPart> assessmentParts) {
        super.putAssessmentParts(assessmentParts);
        return;
    }


    /**
     *  Removes a AssessmentPart from this session.
     *
     *  @param assessmentPartId the {@code Id} of the assessment part
     *  @throws org.osid.NullArgumentException {@code assessmentPartId{@code  is
     *          {@code null}
     */

    @Override
    public void removeAssessmentPart(org.osid.id.Id assessmentPartId) {
        super.removeAssessmentPart(assessmentPartId);
        return;
    }    
}
