//
// AbstractRecipeBatchManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recipe.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractRecipeBatchManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.recipe.batch.RecipeBatchManager,
               org.osid.recipe.batch.RecipeBatchProxyManager {


    /**
     *  Constructs a new <code>AbstractRecipeBatchManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractRecipeBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if bulk administration of recipes is available. 
     *
     *  @return <code> true </code> if a recipe bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipeBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of directions is available. 
     *
     *  @return <code> true </code> if an direction bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDirectionBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of procedures is available. 
     *
     *  @return <code> true </code> if a procedure bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcedureBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of cookbook is available. 
     *
     *  @return <code> true </code> if a cookbook bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCookbookBatchAdmin() {
        return (false);
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk recipe 
     *  administration service. 
     *
     *  @return a <code> RecipeBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecipeBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.batch.RecipeBatchAdminSession getRecipeBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.batch.RecipeBatchManager.getRecipeBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk recipe 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RecipeBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecipeBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.batch.RecipeBatchAdminSession getRecipeBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.batch.RecipeBatchProxyManager.getRecipeBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk recipe 
     *  administration service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @return a <code> RecipeBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Cookbook </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecipeBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.batch.RecipeBatchAdminSession getRecipeBatchAdminSessionForCookbook(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.batch.RecipeBatchManager.getRecipeBatchAdminSessionForCookbook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk recipe 
     *  administration service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RecipeBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Cookbook </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecipeBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.batch.RecipeBatchAdminSession getRecipeBatchAdminSessionForCookbook(org.osid.id.Id cookbookId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.batch.RecipeBatchProxyManager.getRecipeBatchAdminSessionForCookbook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk direction 
     *  administration service. 
     *
     *  @return a <code> DirectionBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.batch.DirectionBatchAdminSession getDirectionBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.batch.RecipeBatchManager.getDirectionBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk direction 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DirectionBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.batch.DirectionBatchAdminSession getDirectionBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.batch.RecipeBatchProxyManager.getDirectionBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk direction 
     *  administration service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @return a <code> DirectionBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Cookbook </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.batch.DirectionBatchAdminSession getDirectionBatchAdminSessionForCookbook(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.batch.RecipeBatchManager.getDirectionBatchAdminSessionForCookbook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk direction 
     *  administration service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DirectionBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Cookbook </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.batch.DirectionBatchAdminSession getDirectionBatchAdminSessionForCookbook(org.osid.id.Id cookbookId, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.batch.RecipeBatchProxyManager.getDirectionBatchAdminSessionForCookbook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk procedure 
     *  administration service. 
     *
     *  @return a <code> ProcedureBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.batch.ProcedureBatchAdminSession getProcedureBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.batch.RecipeBatchManager.getProcedureBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk procedure 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProcedureBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.batch.ProcedureBatchAdminSession getProcedureBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.batch.RecipeBatchProxyManager.getProcedureBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk procedure 
     *  administration service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @return a <code> ProcedureBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Cookbook </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.batch.ProcedureBatchAdminSession getProcedureBatchAdminSessionForCookbook(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.batch.RecipeBatchManager.getProcedureBatchAdminSessionForCookbook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk procedure 
     *  administration service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProcedureBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Cookbook </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.batch.ProcedureBatchAdminSession getProcedureBatchAdminSessionForCookbook(org.osid.id.Id cookbookId, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.batch.RecipeBatchProxyManager.getProcedureBatchAdminSessionForCookbook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk cookbook 
     *  administration service. 
     *
     *  @return a <code> CookbookBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCookbookBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.batch.CookbookBatchAdminSession getCookbookBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.batch.RecipeBatchManager.getCookbookBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk cookbook 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CookbookBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCookbookBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.batch.CookbookBatchAdminSession getCookbookBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.batch.RecipeBatchProxyManager.getCookbookBatchAdminSession not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        return;
    }
}
