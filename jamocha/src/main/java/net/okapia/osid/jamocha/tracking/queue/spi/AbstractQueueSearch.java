//
// AbstractQueueSearch.java
//
//     A template for making a Queue Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.tracking.queue.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing queue searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractQueueSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.tracking.QueueSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.tracking.records.QueueSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.tracking.QueueSearchOrder queueSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of queues. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  queueIds list of queues
     *  @throws org.osid.NullArgumentException
     *          <code>queueIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongQueues(org.osid.id.IdList queueIds) {
        while (queueIds.hasNext()) {
            try {
                this.ids.add(queueIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongQueues</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of queue Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getQueueIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  queueSearchOrder queue search order 
     *  @throws org.osid.NullArgumentException
     *          <code>queueSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>queueSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderQueueResults(org.osid.tracking.QueueSearchOrder queueSearchOrder) {
	this.queueSearchOrder = queueSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.tracking.QueueSearchOrder getQueueSearchOrder() {
	return (this.queueSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given queue search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a queue implementing the requested record.
     *
     *  @param queueSearchRecordType a queue search record
     *         type
     *  @return the queue search record
     *  @throws org.osid.NullArgumentException
     *          <code>queueSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(queueSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.tracking.records.QueueSearchRecord getQueueSearchRecord(org.osid.type.Type queueSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.tracking.records.QueueSearchRecord record : this.records) {
            if (record.implementsRecordType(queueSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(queueSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this queue search. 
     *
     *  @param queueSearchRecord queue search record
     *  @param queueSearchRecordType queue search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addQueueSearchRecord(org.osid.tracking.records.QueueSearchRecord queueSearchRecord, 
                                           org.osid.type.Type queueSearchRecordType) {

        addRecordType(queueSearchRecordType);
        this.records.add(queueSearchRecord);        
        return;
    }
}
