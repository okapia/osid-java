//
// AbstractMapProgramEntryLookupSession
//
//    A simple framework for providing a ProgramEntry lookup service
//    backed by a fixed collection of program entries.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.chronicle.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a ProgramEntry lookup service backed by a
 *  fixed collection of program entries. The program entries are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>ProgramEntries</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapProgramEntryLookupSession
    extends net.okapia.osid.jamocha.course.chronicle.spi.AbstractProgramEntryLookupSession
    implements org.osid.course.chronicle.ProgramEntryLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.course.chronicle.ProgramEntry> programEntries = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.course.chronicle.ProgramEntry>());


    /**
     *  Makes a <code>ProgramEntry</code> available in this session.
     *
     *  @param  programEntry a program entry
     *  @throws org.osid.NullArgumentException <code>programEntry<code>
     *          is <code>null</code>
     */

    protected void putProgramEntry(org.osid.course.chronicle.ProgramEntry programEntry) {
        this.programEntries.put(programEntry.getId(), programEntry);
        return;
    }


    /**
     *  Makes an array of program entries available in this session.
     *
     *  @param  programEntries an array of program entries
     *  @throws org.osid.NullArgumentException <code>programEntries<code>
     *          is <code>null</code>
     */

    protected void putProgramEntries(org.osid.course.chronicle.ProgramEntry[] programEntries) {
        putProgramEntries(java.util.Arrays.asList(programEntries));
        return;
    }


    /**
     *  Makes a collection of program entries available in this session.
     *
     *  @param  programEntries a collection of program entries
     *  @throws org.osid.NullArgumentException <code>programEntries<code>
     *          is <code>null</code>
     */

    protected void putProgramEntries(java.util.Collection<? extends org.osid.course.chronicle.ProgramEntry> programEntries) {
        for (org.osid.course.chronicle.ProgramEntry programEntry : programEntries) {
            this.programEntries.put(programEntry.getId(), programEntry);
        }

        return;
    }


    /**
     *  Removes a ProgramEntry from this session.
     *
     *  @param  programEntryId the <code>Id</code> of the program entry
     *  @throws org.osid.NullArgumentException <code>programEntryId<code> is
     *          <code>null</code>
     */

    protected void removeProgramEntry(org.osid.id.Id programEntryId) {
        this.programEntries.remove(programEntryId);
        return;
    }


    /**
     *  Gets the <code>ProgramEntry</code> specified by its <code>Id</code>.
     *
     *  @param  programEntryId <code>Id</code> of the <code>ProgramEntry</code>
     *  @return the programEntry
     *  @throws org.osid.NotFoundException <code>programEntryId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>programEntryId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntry getProgramEntry(org.osid.id.Id programEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.course.chronicle.ProgramEntry programEntry = this.programEntries.get(programEntryId);
        if (programEntry == null) {
            throw new org.osid.NotFoundException("programEntry not found: " + programEntryId);
        }

        return (programEntry);
    }


    /**
     *  Gets all <code>ProgramEntries</code>. In plenary mode, the returned
     *  list contains all known programEntries or an error
     *  results. Otherwise, the returned list may contain only those
     *  programEntries that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>ProgramEntries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryList getProgramEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.chronicle.programentry.ArrayProgramEntryList(this.programEntries.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.programEntries.clear();
        super.close();
        return;
    }
}
