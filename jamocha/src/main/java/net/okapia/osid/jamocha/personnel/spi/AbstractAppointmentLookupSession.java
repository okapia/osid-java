//
// AbstractAppointmentLookupSession.java
//
//    A starter implementation framework for providing an Appointment
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.personnel.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an Appointment
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getAppointments(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractAppointmentLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.personnel.AppointmentLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.personnel.Realm realm = new net.okapia.osid.jamocha.nil.personnel.realm.UnknownRealm();
    

    /**
     *  Gets the <code>Realm/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Realm Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getRealmId() {
        return (this.realm.getId());
    }


    /**
     *  Gets the <code>Realm</code> associated with this 
     *  session.
     *
     *  @return the <code>Realm</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.Realm getRealm()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.realm);
    }


    /**
     *  Sets the <code>Realm</code>.
     *
     *  @param  realm the realm for this session
     *  @throws org.osid.NullArgumentException <code>realm</code>
     *          is <code>null</code>
     */

    protected void setRealm(org.osid.personnel.Realm realm) {
        nullarg(realm, "realm");
        this.realm = realm;
        return;
    }


    /**
     *  Tests if this user can perform <code>Appointment</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAppointments() {
        return (true);
    }


    /**
     *  A complete view of the <code>Appointment</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAppointmentView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Appointment</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAppointmentView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include appointments in realms which are children
     *  of this realm in the realm hierarchy.
     */

    @OSID @Override
    public void useFederatedRealmView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this realm only.
     */

    @OSID @Override
    public void useIsolatedRealmView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only appointments whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveAppointmentView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All appointments of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveAppointmentView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Appointment</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Appointment</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Appointment</code> and
     *  retained for compatibility.
     *
     *  In effective mode, appointments are returned that are currently
     *  effective.  In any effective mode, effective appointments and
     *  those currently expired are returned.
     *
     *  @param  appointmentId <code>Id</code> of the
     *          <code>Appointment</code>
     *  @return the appointment
     *  @throws org.osid.NotFoundException <code>appointmentId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>appointmentId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.Appointment getAppointment(org.osid.id.Id appointmentId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.personnel.AppointmentList appointments = getAppointments()) {
            while (appointments.hasNext()) {
                org.osid.personnel.Appointment appointment = appointments.getNextAppointment();
                if (appointment.getId().equals(appointmentId)) {
                    return (appointment);
                }
            }
        } 

        throw new org.osid.NotFoundException(appointmentId + " not found");
    }


    /**
     *  Gets an <code>AppointmentList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  appointments specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Appointments</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, appointments are returned that are currently effective.
     *  In any effective mode, effective appointments and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getAppointments()</code>.
     *
     *  @param  appointmentIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Appointment</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>appointmentIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentList getAppointmentsByIds(org.osid.id.IdList appointmentIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.personnel.Appointment> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = appointmentIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getAppointment(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("appointment " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.personnel.appointment.LinkedAppointmentList(ret));
    }


    /**
     *  Gets an <code>AppointmentList</code> corresponding to the given
     *  appointment genus <code>Type</code> which does not include
     *  appointments of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  appointments or an error results. Otherwise, the returned list
     *  may contain only those appointments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, appointments are returned that are currently effective.
     *  In any effective mode, effective appointments and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getAppointments()</code>.
     *
     *  @param  appointmentGenusType an appointment genus type 
     *  @return the returned <code>Appointment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>appointmentGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentList getAppointmentsByGenusType(org.osid.type.Type appointmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.personnel.appointment.AppointmentGenusFilterList(getAppointments(), appointmentGenusType));
    }


    /**
     *  Gets an <code>AppointmentList</code> corresponding to the
     *  given appointment genus <code>Type</code> and include any
     *  additional appointments with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  appointments or an error results. Otherwise, the returned list
     *  may contain only those appointments that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, appointments are returned that are
     *  currently effective.  In any effective mode, effective
     *  appointments and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAppointments()</code>.
     *
     *  @param  appointmentGenusType an appointment genus type 
     *  @return the returned <code>Appointment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>appointmentGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentList getAppointmentsByParentGenusType(org.osid.type.Type appointmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getAppointmentsByGenusType(appointmentGenusType));
    }


    /**
     *  Gets an <code>AppointmentList</code> containing the given
     *  appointment record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known
     *  appointments or an error results. Otherwise, the returned list
     *  may contain only those appointments that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, appointments are returned that are
     *  currently effective.  In any effective mode, effective
     *  appointments and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAppointments()</code>.
     *
     *  @param  appointmentRecordType an appointment record type 
     *  @return the returned <code>Appointment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>appointmentRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentList getAppointmentsByRecordType(org.osid.type.Type appointmentRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.personnel.appointment.AppointmentRecordFilterList(getAppointments(), appointmentRecordType));
    }


    /**
     *  Gets an <code>AppointmentList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  appointments or an error results. Otherwise, the returned list
     *  may contain only those appointments that are accessible
     *  through this session.
     *  
     *  In active mode, appointments are returned that are currently
     *  active. In any status mode, active and inactive appointments
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Appointment</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.personnel.AppointmentList getAppointmentsOnDate(org.osid.calendaring.DateTime from, 
                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.personnel.appointment.TemporalAppointmentFilterList(getAppointments(), from, to));
    }
        

    /**
     *  Gets a list of appointments corresponding to a person
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  appointments or an error results. Otherwise, the returned list
     *  may contain only those appointments that are accessible
     *  through this session.
     *
     *  In effective mode, appointments are returned that are
     *  currently effective.  In any effective mode, effective
     *  appointments and those currently expired are returned.
     *
     *  @param  personId the <code>Id</code> of the person
     *  @return the returned <code>AppointmentList</code>
     *  @throws org.osid.NullArgumentException <code>personId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.personnel.AppointmentList getAppointmentsForPerson(org.osid.id.Id personId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.personnel.appointment.AppointmentFilterList(new PersonFilter(personId), getAppointments()));
    }


    /**
     *  Gets a list of appointments corresponding to a person
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  appointments or an error results. Otherwise, the returned list
     *  may contain only those appointments that are accessible
     *  through this session.
     *
     *  In effective mode, appointments are returned that are
     *  currently effective.  In any effective mode, effective
     *  appointments and those currently expired are returned.
     *
     *  @param  personId the <code>Id</code> of the person
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>AppointmentList</code>
     *  @throws org.osid.NullArgumentException <code>personId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.personnel.AppointmentList getAppointmentsForPersonOnDate(org.osid.id.Id personId,
                                                                             org.osid.calendaring.DateTime from,
                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.personnel.appointment.TemporalAppointmentFilterList(getAppointmentsForPerson(personId), from, to));
    }


    /**
     *  Gets a list of appointments corresponding to a position
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  appointments or an error results. Otherwise, the returned list
     *  may contain only those appointments that are accessible
     *  through this session.
     *
     *  In effective mode, appointments are returned that are
     *  currently effective.  In any effective mode, effective
     *  appointments and those currently expired are returned.
     *
     *  @param  positionId the <code>Id</code> of the position
     *  @return the returned <code>AppointmentList</code>
     *  @throws org.osid.NullArgumentException <code>positionId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.personnel.AppointmentList getAppointmentsForPosition(org.osid.id.Id positionId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.personnel.appointment.AppointmentFilterList(new PositionFilter(positionId), getAppointments()));
    }


    /**
     *  Gets a list of appointments corresponding to a position
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  appointments or an error results. Otherwise, the returned list
     *  may contain only those appointments that are accessible
     *  through this session.
     *
     *  In effective mode, appointments are returned that are
     *  currently effective.  In any effective mode, effective
     *  appointments and those currently expired are returned.
     *
     *  @param  positionId the <code>Id</code> of the position
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>AppointmentList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>positionId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.personnel.AppointmentList getAppointmentsForPositionOnDate(org.osid.id.Id positionId,
                                                                               org.osid.calendaring.DateTime from,
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.personnel.appointment.TemporalAppointmentFilterList(getAppointmentsForPosition(positionId), from, to));
    }


    /**
     *  Gets a list of appointments corresponding to person and
     *  position <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  appointments or an error results. Otherwise, the returned list
     *  may contain only those appointments that are accessible
     *  through this session.
     *
     *  In effective mode, appointments are returned that are
     *  currently effective.  In any effective mode, effective
     *  appointments and those currently expired are returned.
     *
     *  @param  personId the <code>Id</code> of the person
     *  @param  positionId the <code>Id</code> of the position
     *  @return the returned <code>AppointmentList</code>
     *  @throws org.osid.NullArgumentException <code>personId</code>,
     *          <code>positionId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.personnel.AppointmentList getAppointmentsForPersonAndPosition(org.osid.id.Id personId,
                                                                                  org.osid.id.Id positionId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.personnel.appointment.AppointmentFilterList(new PositionFilter(positionId), getAppointmentsForPerson(personId)));
    }


    /**
     *  Gets a list of appointments corresponding to person and
     *  position <code>Ids</code> and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  appointments or an error results. Otherwise, the returned list
     *  may contain only those appointments that are accessible
     *  through this session.
     *
     *  In effective mode, appointments are returned that are
     *  currently effective.  In any effective mode, effective
     *  appointments and those currently expired are returned.
     *
     *  @param  positionId the <code>Id</code> of the position
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>AppointmentList</code>
     *  @throws org.osid.NullArgumentException <code>personId</code>,
     *          <code>positionId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.personnel.AppointmentList getAppointmentsForPersonAndPositionOnDate(org.osid.id.Id personId,
                                                                                        org.osid.id.Id positionId,
                                                                                        org.osid.calendaring.DateTime from,
                                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.personnel.appointment.TemporalAppointmentFilterList(getAppointmentsForPersonAndPosition(personId, positionId), from, to));
    }


    /**
     *  Gets all <code>Appointments</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  appointments or an error results. Otherwise, the returned list
     *  may contain only those appointments that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, appointments are returned that are
     *  currently effective.  In any effective mode, effective
     *  appointments and those currently expired are returned.
     *
     *  @return a list of <code>Appointments</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.personnel.AppointmentList getAppointments()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the appointment list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of appointments
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.personnel.AppointmentList filterAppointmentsOnViews(org.osid.personnel.AppointmentList list)
        throws org.osid.OperationFailedException {

        org.osid.personnel.AppointmentList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.personnel.appointment.EffectiveAppointmentFilterList(ret);
        }

        return (ret);
    }

    public static class PersonFilter
        implements net.okapia.osid.jamocha.inline.filter.personnel.appointment.AppointmentFilter {
         
        private final org.osid.id.Id personId;
         
         
        /**
         *  Constructs a new <code>PersonFilter</code>.
         *
         *  @param personId the person to filter
         *  @throws org.osid.NullArgumentException
         *          <code>personId</code> is <code>null</code>
         */
        
        public PersonFilter(org.osid.id.Id personId) {
            nullarg(personId, "person Id");
            this.personId = personId;
            return;
        }

         
        /**
         *  Used by the AppointmentFilterList to filter the 
         *  appointment list based on person.
         *
         *  @param appointment the appointment
         *  @return <code>true</code> to pass the appointment,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.personnel.Appointment appointment) {
            return (appointment.getPersonId().equals(this.personId));
        }
    }


    public static class PositionFilter
        implements net.okapia.osid.jamocha.inline.filter.personnel.appointment.AppointmentFilter {
         
        private final org.osid.id.Id positionId;
         
         
        /**
         *  Constructs a new <code>PositionFilter</code>.
         *
         *  @param positionId the position to filter
         *  @throws org.osid.NullArgumentException
         *          <code>positionId</code> is <code>null</code>
         */
        
        public PositionFilter(org.osid.id.Id positionId) {
            nullarg(positionId, "position Id");
            this.positionId = positionId;
            return;
        }

         
        /**
         *  Used by the AppointmentFilterList to filter the 
         *  appointment list based on position.
         *
         *  @param appointment the appointment
         *  @return <code>true</code> to pass the appointment,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.personnel.Appointment appointment) {
            return (appointment.getPositionId().equals(this.positionId));
        }
    }
}
