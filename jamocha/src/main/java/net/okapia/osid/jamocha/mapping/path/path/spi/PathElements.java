//
// PathElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.path.path.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class PathElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the PathElement Id.
     *
     *  @return the path element Id
     */

    public static org.osid.id.Id getPathEntityId() {
        return (makeEntityId("osid.mapping.path.Path"));
    }


    /**
     *  Gets the Coordinate element Id.
     *
     *  @return the Coordinate element Id
     */

    public static org.osid.id.Id getCoordinate() {
        return (makeQueryElementId("osid.mapping.path.path.Coordinate"));
    }


    /**
     *  Gets the OverlappingSpatialUnit element Id.
     *
     *  @return the OverlappingSpatialUnit element Id
     */

    public static org.osid.id.Id getOverlappingSpatialUnit() {
        return (makeQueryElementId("osid.mapping.path.path.OverlappingSpatialUnit"));
    }


    /**
     *  Gets the AlongLocationIds element Id.
     *
     *  @return the AlongLocationIds element Id
     */

    public static org.osid.id.Id getAlongLocationIds() {
        return (makeQueryElementId("osid.mapping.path.path.AlongLocationIds"));
    }


    /**
     *  Gets the IntersectingPathId element Id.
     *
     *  @return the IntersectingPathId element Id
     */

    public static org.osid.id.Id getIntersectingPathId() {
        return (makeQueryElementId("osid.mapping.path.path.IntersectingPathId"));
    }


    /**
     *  Gets the IntersectingPath element Id.
     *
     *  @return the IntersectingPath element Id
     */

    public static org.osid.id.Id getIntersectingPath() {
        return (makeQueryElementId("osid.mapping.path.path.IntersectingPath"));
    }


    /**
     *  Gets the LocationId element Id.
     *
     *  @return the LocationId element Id
     */

    public static org.osid.id.Id getLocationId() {
        return (makeQueryElementId("osid.mapping.path.path.LocationId"));
    }


    /**
     *  Gets the Location element Id.
     *
     *  @return the Location element Id
     */

    public static org.osid.id.Id getLocation() {
        return (makeQueryElementId("osid.mapping.path.path.Location"));
    }


    /**
     *  Gets the RouteId element Id.
     *
     *  @return the RouteId element Id
     */

    public static org.osid.id.Id getRouteId() {
        return (makeQueryElementId("osid.mapping.path.path.RouteId"));
    }


    /**
     *  Gets the Route element Id.
     *
     *  @return the Route element Id
     */

    public static org.osid.id.Id getRoute() {
        return (makeQueryElementId("osid.mapping.path.path.Route"));
    }


    /**
     *  Gets the MapId element Id.
     *
     *  @return the MapId element Id
     */

    public static org.osid.id.Id getMapId() {
        return (makeQueryElementId("osid.mapping.path.path.MapId"));
    }


    /**
     *  Gets the Map element Id.
     *
     *  @return the Map element Id
     */

    public static org.osid.id.Id getMap() {
        return (makeQueryElementId("osid.mapping.path.path.Map"));
    }
}
