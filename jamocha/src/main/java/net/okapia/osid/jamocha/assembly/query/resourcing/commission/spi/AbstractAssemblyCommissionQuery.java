//
// AbstractAssemblyCommissionQuery.java
//
//     A CommissionQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.resourcing.commission.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A CommissionQuery that stores terms.
 */

public abstract class AbstractAssemblyCommissionQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.resourcing.CommissionQuery,
               org.osid.resourcing.CommissionQueryInspector,
               org.osid.resourcing.CommissionSearchOrder {

    private final java.util.Collection<org.osid.resourcing.records.CommissionQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resourcing.records.CommissionQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resourcing.records.CommissionSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyCommissionQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyCommissionQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the resource <code> Id </code> for this query. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResourceId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getResourceIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the resource <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearResourceIdTerms() {
        getAssembler().clearTerms(getResourceIdColumn());
        return;
    }


    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResourceIdTerms() {
        return (getAssembler().getIdTerms(getResourceIdColumn()));
    }


    /**
     *  Orders the results by resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByResource(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getResourceColumn(), style);
        return;
    }


    /**
     *  Gets the ResourceId column name.
     *
     * @return the column name
     */

    protected String getResourceIdColumn() {
        return ("resource_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResourceQuery() {
        throw new org.osid.UnimplementedException("supportsResourceQuery() is false");
    }


    /**
     *  Clears the resource query terms. 
     */

    @OSID @Override
    public void clearResourceTerms() {
        getAssembler().clearTerms(getResourceColumn());
        return;
    }


    /**
     *  Gets the resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsResourceSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getResourceSearchOrder() {
        throw new org.osid.UnimplementedException("supportsResourceSearchOrder() is false");
    }


    /**
     *  Gets the Resource column name.
     *
     * @return the column name
     */

    protected String getResourceColumn() {
        return ("resource");
    }


    /**
     *  Sets the work <code> Id </code> for this query. 
     *
     *  @param  workId the work <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> workId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchWorkId(org.osid.id.Id workId, boolean match) {
        getAssembler().addIdTerm(getWorkIdColumn(), workId, match);
        return;
    }


    /**
     *  Clears the work <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearWorkIdTerms() {
        getAssembler().clearTerms(getWorkIdColumn());
        return;
    }


    /**
     *  Gets the work <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getWorkIdTerms() {
        return (getAssembler().getIdTerms(getWorkIdColumn()));
    }


    /**
     *  Orders the results by work. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByWork(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getWorkColumn(), style);
        return;
    }


    /**
     *  Gets the WorkId column name.
     *
     * @return the column name
     */

    protected String getWorkIdColumn() {
        return ("work_id");
    }


    /**
     *  Tests if a <code> WorkQuery </code> is available. 
     *
     *  @return <code> true </code> if a work query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkQuery() {
        return (false);
    }


    /**
     *  Gets the query for a work. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the work query 
     *  @throws org.osid.UnimplementedException <code> supportsWorkQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkQuery getWorkQuery() {
        throw new org.osid.UnimplementedException("supportsWorkQuery() is false");
    }


    /**
     *  Clears the work query terms. 
     */

    @OSID @Override
    public void clearWorkTerms() {
        getAssembler().clearTerms(getWorkColumn());
        return;
    }


    /**
     *  Gets the work query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.WorkQueryInspector[] getWorkTerms() {
        return (new org.osid.resourcing.WorkQueryInspector[0]);
    }


    /**
     *  Tests if a work search order is available. 
     *
     *  @return <code> true </code> if a work search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkSearchOrder() {
        return (false);
    }


    /**
     *  Gets the work search order. 
     *
     *  @return the work search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsWorkSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkSearchOrder getWorkSearchOrder() {
        throw new org.osid.UnimplementedException("supportsWorkSearchOrder() is false");
    }


    /**
     *  Gets the Work column name.
     *
     * @return the column name
     */

    protected String getWorkColumn() {
        return ("work");
    }


    /**
     *  Sets the competency <code> Id </code> for this query. 
     *
     *  @param  competencyId the competency <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> competencyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCompetencyId(org.osid.id.Id competencyId, boolean match) {
        getAssembler().addIdTerm(getCompetencyIdColumn(), competencyId, match);
        return;
    }


    /**
     *  Clears the competency <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCompetencyIdTerms() {
        getAssembler().clearTerms(getCompetencyIdColumn());
        return;
    }


    /**
     *  Gets the competency <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCompetencyIdTerms() {
        return (getAssembler().getIdTerms(getCompetencyIdColumn()));
    }


    /**
     *  Orders the results by competency. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCompetency(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCompetencyColumn(), style);
        return;
    }


    /**
     *  Gets the CompetencyId column name.
     *
     * @return the column name
     */

    protected String getCompetencyIdColumn() {
        return ("competency_id");
    }


    /**
     *  Tests if a <code> CompetencyQuery </code> is available. 
     *
     *  @return <code> true </code> if a competency query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompetencyQuery() {
        return (false);
    }


    /**
     *  Gets the query for a competency. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the competency query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencyQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyQuery getCompetencyQuery() {
        throw new org.osid.UnimplementedException("supportsCompetencyQuery() is false");
    }


    /**
     *  Clears the competency query terms. 
     */

    @OSID @Override
    public void clearCompetencyTerms() {
        getAssembler().clearTerms(getCompetencyColumn());
        return;
    }


    /**
     *  Gets the competency query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyQueryInspector[] getCompetencyTerms() {
        return (new org.osid.resourcing.CompetencyQueryInspector[0]);
    }


    /**
     *  Tests if a competency search order is available. 
     *
     *  @return <code> true </code> if a competency search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompetencySearchOrder() {
        return (false);
    }


    /**
     *  Gets the competency search order. 
     *
     *  @return the competency search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsCompetencySearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencySearchOrder getCompetencySearchOrder() {
        throw new org.osid.UnimplementedException("supportsCompetencySearchOrder() is false");
    }


    /**
     *  Gets the Competency column name.
     *
     * @return the column name
     */

    protected String getCompetencyColumn() {
        return ("competency");
    }


    /**
     *  Matches percentages within the given range inclusive. 
     *
     *  @param  low start range 
     *  @param  high end range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchPercentage(long low, long high, boolean match) {
        getAssembler().addCardinalRangeTerm(getPercentageColumn(), low, high, match);
        return;
    }


    /**
     *  Clears the percentage query terms. 
     */

    @OSID @Override
    public void clearPercentageTerms() {
        getAssembler().clearTerms(getPercentageColumn());
        return;
    }


    /**
     *  Gets the percentage availability query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getPercentageTerms() {
        return (getAssembler().getCardinalRangeTerms(getPercentageColumn()));
    }


    /**
     *  Orders the results by percentage availability. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPercentage(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getPercentageColumn(), style);
        return;
    }


    /**
     *  Gets the Percentage column name.
     *
     * @return the column name
     */

    protected String getPercentageColumn() {
        return ("percentage");
    }


    /**
     *  Sets the foundry <code> Id </code> for this query to match commissions 
     *  assigned to foundries. 
     *
     *  @param  foundryId the foundry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchFoundryId(org.osid.id.Id foundryId, boolean match) {
        getAssembler().addIdTerm(getFoundryIdColumn(), foundryId, match);
        return;
    }


    /**
     *  Clears the foundry <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearFoundryIdTerms() {
        getAssembler().clearTerms(getFoundryIdColumn());
        return;
    }


    /**
     *  Gets the foundry <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFoundryIdTerms() {
        return (getAssembler().getIdTerms(getFoundryIdColumn()));
    }


    /**
     *  Gets the FoundryId column name.
     *
     * @return the column name
     */

    protected String getFoundryIdColumn() {
        return ("foundry_id");
    }


    /**
     *  Sets the effort <code> Id </code> for this query. 
     *
     *  @param  effortId the effort <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> effortId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEffortId(org.osid.id.Id effortId, boolean match) {
        getAssembler().addIdTerm(getEffortIdColumn(), effortId, match);
        return;
    }


    /**
     *  Clears the effort <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearEffortIdTerms() {
        getAssembler().clearTerms(getEffortIdColumn());
        return;
    }


    /**
     *  Gets the effort <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEffortIdTerms() {
        return (getAssembler().getIdTerms(getEffortIdColumn()));
    }


    /**
     *  Gets the EffortId column name.
     *
     * @return the column name
     */

    protected String getEffortIdColumn() {
        return ("effort_id");
    }


    /**
     *  Tests if an <code> EffortQuery </code> is available. 
     *
     *  @return <code> true </code> if an effort query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEffortQuery() {
        return (false);
    }


    /**
     *  Gets the query for an effort. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the effort query 
     *  @throws org.osid.UnimplementedException <code> supportsEffortQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.EffortQuery getEffortQuery() {
        throw new org.osid.UnimplementedException("supportsEffortQuery() is false");
    }


    /**
     *  Matches commissions with any effort. 
     *
     *  @param  match <code> true </code> to match commissions with any 
     *          effort, <code> false </code> to match commissions with no 
     *          effort 
     */

    @OSID @Override
    public void matchAnyEffort(boolean match) {
        getAssembler().addIdWildcardTerm(getEffortColumn(), match);
        return;
    }


    /**
     *  Clears the effort query terms. 
     */

    @OSID @Override
    public void clearEffortTerms() {
        getAssembler().clearTerms(getEffortColumn());
        return;
    }


    /**
     *  Gets the effort query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.EffortQueryInspector[] getEffortTerms() {
        return (new org.osid.resourcing.EffortQueryInspector[0]);
    }


    /**
     *  Gets the Effort column name.
     *
     * @return the column name
     */

    protected String getEffortColumn() {
        return ("effort");
    }


    /**
     *  Tests if a <code> FoundryQuery </code> is available. 
     *
     *  @return <code> true </code> if a foundry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFoundryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a foundry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the foundry query 
     *  @throws org.osid.UnimplementedException <code> supportsFoundryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQuery getFoundryQuery() {
        throw new org.osid.UnimplementedException("supportsFoundryQuery() is false");
    }


    /**
     *  Clears the foundry query terms. 
     */

    @OSID @Override
    public void clearFoundryTerms() {
        getAssembler().clearTerms(getFoundryColumn());
        return;
    }


    /**
     *  Gets the foundry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQueryInspector[] getFoundryTerms() {
        return (new org.osid.resourcing.FoundryQueryInspector[0]);
    }


    /**
     *  Gets the Foundry column name.
     *
     * @return the column name
     */

    protected String getFoundryColumn() {
        return ("foundry");
    }


    /**
     *  Tests if this commission supports the given record
     *  <code>Type</code>.
     *
     *  @param  commissionRecordType a commission record type 
     *  @return <code>true</code> if the commissionRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>commissionRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type commissionRecordType) {
        for (org.osid.resourcing.records.CommissionQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(commissionRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  commissionRecordType the commission record type 
     *  @return the commission query record 
     *  @throws org.osid.NullArgumentException
     *          <code>commissionRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(commissionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.CommissionQueryRecord getCommissionQueryRecord(org.osid.type.Type commissionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.records.CommissionQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(commissionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(commissionRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  commissionRecordType the commission record type 
     *  @return the commission query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>commissionRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(commissionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.CommissionQueryInspectorRecord getCommissionQueryInspectorRecord(org.osid.type.Type commissionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.records.CommissionQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(commissionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(commissionRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param commissionRecordType the commission record type
     *  @return the commission search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>commissionRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(commissionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.CommissionSearchOrderRecord getCommissionSearchOrderRecord(org.osid.type.Type commissionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.records.CommissionSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(commissionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(commissionRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this commission. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param commissionQueryRecord the commission query record
     *  @param commissionQueryInspectorRecord the commission query inspector
     *         record
     *  @param commissionSearchOrderRecord the commission search order record
     *  @param commissionRecordType commission record type
     *  @throws org.osid.NullArgumentException
     *          <code>commissionQueryRecord</code>,
     *          <code>commissionQueryInspectorRecord</code>,
     *          <code>commissionSearchOrderRecord</code> or
     *          <code>commissionRecordTypecommission</code> is
     *          <code>null</code>
     */
            
    protected void addCommissionRecords(org.osid.resourcing.records.CommissionQueryRecord commissionQueryRecord, 
                                      org.osid.resourcing.records.CommissionQueryInspectorRecord commissionQueryInspectorRecord, 
                                      org.osid.resourcing.records.CommissionSearchOrderRecord commissionSearchOrderRecord, 
                                      org.osid.type.Type commissionRecordType) {

        addRecordType(commissionRecordType);

        nullarg(commissionQueryRecord, "commission query record");
        nullarg(commissionQueryInspectorRecord, "commission query inspector record");
        nullarg(commissionSearchOrderRecord, "commission search odrer record");

        this.queryRecords.add(commissionQueryRecord);
        this.queryInspectorRecords.add(commissionQueryInspectorRecord);
        this.searchOrderRecords.add(commissionSearchOrderRecord);
        
        return;
    }
}
