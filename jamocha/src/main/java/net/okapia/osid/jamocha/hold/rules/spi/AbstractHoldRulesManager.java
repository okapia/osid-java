//
// AbstractHoldRulesManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.hold.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractHoldRulesManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.hold.rules.HoldRulesManager,
               org.osid.hold.rules.HoldRulesProxyManager {

    private final Types holdEnablerRecordTypes             = new TypeRefSet();
    private final Types holdEnablerSearchRecordTypes       = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractHoldRulesManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractHoldRulesManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if looking up hold enablers is supported. 
     *
     *  @return <code> true </code> if hold enabler lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying hold enablers is supported. 
     *
     *  @return <code> true </code> if hold enabler query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching hold enablers is supported. 
     *
     *  @return <code> true </code> if hold enabler search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if a hold enabler administrative service is supported. 
     *
     *  @return <code> true </code> if hold enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if a hold enabler notification service is supported. 
     *
     *  @return <code> true </code> if hold enabler notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if a hold enabler oubliette lookup service is supported. 
     *
     *  @return <code> true </code> if an oubliette enabler hold lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldEnablerOubliette() {
        return (false);
    }


    /**
     *  Tests if a hold enabler oubliette service is supported. 
     *
     *  @return <code> true </code> if hold enabler oubliette assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldEnablerOublietteAssignment() {
        return (false);
    }


    /**
     *  Tests if a hold enabler hold lookup service is supported. 
     *
     *  @return <code> true </code> if a hold enabler hold service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldEnablerSmartHold() {
        return (false);
    }


    /**
     *  Tests if a hold enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a hold enabler rule lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldEnablerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if a hold enabler rule application service is supported. 
     *
     *  @return <code> true </code> if hold enabler rule application service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldEnablerRuleApplication() {
        return (false);
    }


    /**
     *  Gets the supported <code> HoldEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> HoldEnabler </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getHoldEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.holdEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> HoldEnabler </code> record type is 
     *  supported. 
     *
     *  @param  holdEnablerRecordType a <code> Type </code> indicating a 
     *          <code> HoldEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> holdEnablerRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsHoldEnablerRecordType(org.osid.type.Type holdEnablerRecordType) {
        return (this.holdEnablerRecordTypes.contains(holdEnablerRecordType));
    }


    /**
     *  Adds support for a hold enabler record type.
     *
     *  @param holdEnablerRecordType a hold enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>holdEnablerRecordType</code> is <code>null</code>
     */

    protected void addHoldEnablerRecordType(org.osid.type.Type holdEnablerRecordType) {
        this.holdEnablerRecordTypes.add(holdEnablerRecordType);
        return;
    }


    /**
     *  Removes support for a hold enabler record type.
     *
     *  @param holdEnablerRecordType a hold enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>holdEnablerRecordType</code> is <code>null</code>
     */

    protected void removeHoldEnablerRecordType(org.osid.type.Type holdEnablerRecordType) {
        this.holdEnablerRecordTypes.remove(holdEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> HoldEnabler </code> search record types. 
     *
     *  @return a list containing the supported <code> HoldEnabler </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getHoldEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.holdEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> HoldEnabler </code> search record type is 
     *  supported. 
     *
     *  @param  holdEnablerSearchRecordType a <code> Type </code> indicating a 
     *          <code> HoldEnabler </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          holdEnablerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsHoldEnablerSearchRecordType(org.osid.type.Type holdEnablerSearchRecordType) {
        return (this.holdEnablerSearchRecordTypes.contains(holdEnablerSearchRecordType));
    }


    /**
     *  Adds support for a hold enabler search record type.
     *
     *  @param holdEnablerSearchRecordType a hold enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>holdEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addHoldEnablerSearchRecordType(org.osid.type.Type holdEnablerSearchRecordType) {
        this.holdEnablerSearchRecordTypes.add(holdEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a hold enabler search record type.
     *
     *  @param holdEnablerSearchRecordType a hold enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>holdEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removeHoldEnablerSearchRecordType(org.osid.type.Type holdEnablerSearchRecordType) {
        this.holdEnablerSearchRecordTypes.remove(holdEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold enabler 
     *  lookup service. 
     *
     *  @return a <code> HoldEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerLookupSession getHoldEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.rules.HoldRulesManager.getHoldEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold enabler 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HoldEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerLookupSession getHoldEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.rules.HoldRulesProxyManager.getHoldEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold enabler 
     *  lookup service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @return a <code> HoldEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerLookupSession getHoldEnablerLookupSessionForOubliette(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.rules.HoldRulesManager.getHoldEnablerLookupSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold enabler 
     *  lookup service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> HoldEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerLookupSession getHoldEnablerLookupSessionForOubliette(org.osid.id.Id oublietteId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.rules.HoldRulesProxyManager.getHoldEnablerLookupSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold enabler 
     *  query service. 
     *
     *  @return a <code> HoldEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerQuerySession getHoldEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.rules.HoldRulesManager.getHoldEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold enabler 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HoldEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerQuerySession getHoldEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.rules.HoldRulesProxyManager.getHoldEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold enabler 
     *  query service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @return a <code> HoldEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerQuerySession getHoldEnablerQuerySessionForOubliette(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.rules.HoldRulesManager.getHoldEnablerQuerySessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold enabler 
     *  query service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> HoldEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerQuerySession getHoldEnablerQuerySessionForOubliette(org.osid.id.Id oublietteId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.rules.HoldRulesProxyManager.getHoldEnablerQuerySessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold enabler 
     *  search service. 
     *
     *  @return a <code> HoldEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerSearchSession getHoldEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.rules.HoldRulesManager.getHoldEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold enabler 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HoldEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerSearchSession getHoldEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.rules.HoldRulesProxyManager.getHoldEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold enablers 
     *  earch service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @return a <code> HoldEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerSearchSession getHoldEnablerSearchSessionForOubliette(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.rules.HoldRulesManager.getHoldEnablerSearchSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold enablers 
     *  earch service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> HoldEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerSearchSession getHoldEnablerSearchSessionForOubliette(org.osid.id.Id oublietteId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.rules.HoldRulesProxyManager.getHoldEnablerSearchSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold enabler 
     *  administration service. 
     *
     *  @return a <code> HoldEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerAdminSession getHoldEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.rules.HoldRulesManager.getHoldEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold enabler 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HoldEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerAdminSession getHoldEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.rules.HoldRulesProxyManager.getHoldEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold enabler 
     *  administration service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @return a <code> HoldEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerAdminSession getHoldEnablerAdminSessionForOubliette(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.rules.HoldRulesManager.getHoldEnablerAdminSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold enabler 
     *  administration service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> HoldEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerAdminSession getHoldEnablerAdminSessionForOubliette(org.osid.id.Id oublietteId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.rules.HoldRulesProxyManager.getHoldEnablerAdminSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold enabler 
     *  notification service. 
     *
     *  @param  holdEnablerReceiver the notification callback 
     *  @return a <code> HoldEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> holdEnablerReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerNotificationSession getHoldEnablerNotificationSession(org.osid.hold.rules.HoldEnablerReceiver holdEnablerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.rules.HoldRulesManager.getHoldEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold enabler 
     *  notification service. 
     *
     *  @param  holdEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> HoldEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> holdEnablerReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerNotificationSession getHoldEnablerNotificationSession(org.osid.hold.rules.HoldEnablerReceiver holdEnablerReceiver, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.rules.HoldRulesProxyManager.getHoldEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold enabler 
     *  notification service for the given oubliette. 
     *
     *  @param  holdEnablerReceiver the notification callback 
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @return a <code> HoldEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no oubliette found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> holdEnablerReceiver 
     *          </code> or <code> oublietteId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerNotificationSession getHoldEnablerNotificationSessionForOubliette(org.osid.hold.rules.HoldEnablerReceiver holdEnablerReceiver, 
                                                                                                            org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.rules.HoldRulesManager.getHoldEnablerNotificationSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold enabler 
     *  notification service for the given oubliette. 
     *
     *  @param  holdEnablerReceiver the notification callback 
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> HoldEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no oubliette found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> holdEnablerReceiver, 
     *          oublietteId, </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerNotificationSession getHoldEnablerNotificationSessionForOubliette(org.osid.hold.rules.HoldEnablerReceiver holdEnablerReceiver, 
                                                                                                            org.osid.id.Id oublietteId, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.rules.HoldRulesProxyManager.getHoldEnablerNotificationSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup hold enableroubliette 
     *  mappings. 
     *
     *  @return a <code> HoldEnablerOublietteSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerOubliette() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerOublietteSession getHoldEnablerOublietteSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.rules.HoldRulesManager.getHoldEnablerOublietteSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup hold enabler/oubliette 
     *  mappings for hold enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HoldEnablerSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerOubliette() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerOublietteSession getHoldEnablerOublietteSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.rules.HoldRulesProxyManager.getHoldEnablerOublietteSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning hold 
     *  enablers to oubliettes. 
     *
     *  @return a <code> HoldEnablerOublietteAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerOublietteAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerOublietteAssignmentSession getHoldEnablerOublietteAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.rules.HoldRulesManager.getHoldEnablerOublietteAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning hold 
     *  enablers to oubliettes 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HoldEnablerOublietteAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerOublietteAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerOublietteAssignmentSession getHoldEnablerOublietteAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.rules.HoldRulesProxyManager.getHoldEnablerOublietteAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage hold enabler smart 
     *  oubliettes. 
     *
     *  @param  oublietteId the Id of the <code> Oubliette </code> 
     *  @return a <code> HoldEnablerSmartOublietteSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerSmartOubliette() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerSmartOublietteSession getHoldEnablerSmartOublietteSession(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.rules.HoldRulesManager.getHoldEnablerSmartOublietteSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage hold enabler smart 
     *  oubliettes. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> HoldEnablerSmartOublietteSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerSmartOubliette() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerSmartOublietteSession getHoldEnablerSmartOublietteSession(org.osid.id.Id oublietteId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.rules.HoldRulesProxyManager.getHoldEnablerSmartOublietteSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold enabler 
     *  mapping lookup service for looking up the rules applied to the hold. 
     *
     *  @return a <code> HoldEnablerRuleSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerRuleLookupSession getHoldEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.rules.HoldRulesManager.getHoldEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold enabler 
     *  mapping lookup service for looking up the rules applied to the hold. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HoldEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerRuleLookupSession getHoldEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.rules.HoldRulesProxyManager.getHoldEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold enabler 
     *  mapping lookup service for the given hold for looking up rules applied 
     *  to a hold. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @return a <code> HoldEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerRuleLookupSession getHoldEnablerRuleLookupSessionForOubliette(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.rules.HoldRulesManager.getHoldEnablerRuleLookupSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold enabler 
     *  mapping lookup service for the given hold for looking up rules applied 
     *  to a hold. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> HoldEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerRuleLookupSession getHoldEnablerRuleLookupSessionForOubliette(org.osid.id.Id oublietteId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.rules.HoldRulesProxyManager.getHoldEnablerRuleLookupSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold enabler 
     *  assignment service to apply enablers to holds. 
     *
     *  @return a <code> HoldEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerRuleApplication() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerRuleApplicationSession getHoldEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.rules.HoldRulesManager.getHoldEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold enabler 
     *  assignment service to apply enablers to holds. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HoldEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerRuleApplication() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerRuleApplicationSession getHoldEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.rules.HoldRulesProxyManager.getHoldEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold enabler 
     *  assignment service for the given hold to apply enablers to holds. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @return a <code> HoldEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerRuleApplicationSession getHoldEnablerRuleApplicationSessionForOubliette(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.rules.HoldRulesManager.getHoldEnablerRuleApplicationSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold enabler 
     *  assignment service for the given hold to apply enablers to holds. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> HoldEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerRuleApplicationSession getHoldEnablerRuleApplicationSessionForOubliette(org.osid.id.Id oublietteId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.rules.HoldRulesProxyManager.getHoldEnablerRuleApplicationSessionForOubliette not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.holdEnablerRecordTypes.clear();
        this.holdEnablerRecordTypes.clear();

        this.holdEnablerSearchRecordTypes.clear();
        this.holdEnablerSearchRecordTypes.clear();

        return;
    }
}
