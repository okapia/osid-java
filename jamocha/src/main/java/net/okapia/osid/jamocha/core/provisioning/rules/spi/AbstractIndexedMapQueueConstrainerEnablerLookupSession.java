//
// AbstractIndexedMapQueueConstrainerEnablerLookupSession.java
//
//    A simple framework for providing a QueueConstrainerEnabler lookup service
//    backed by a fixed collection of queue constrainer enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a QueueConstrainerEnabler lookup service backed by a
 *  fixed collection of queue constrainer enablers. The queue constrainer enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some queue constrainer enablers may be compatible
 *  with more types than are indicated through these queue constrainer enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>QueueConstrainerEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapQueueConstrainerEnablerLookupSession
    extends AbstractMapQueueConstrainerEnablerLookupSession
    implements org.osid.provisioning.rules.QueueConstrainerEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.provisioning.rules.QueueConstrainerEnabler> queueConstrainerEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.provisioning.rules.QueueConstrainerEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.provisioning.rules.QueueConstrainerEnabler> queueConstrainerEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.provisioning.rules.QueueConstrainerEnabler>());


    /**
     *  Makes a <code>QueueConstrainerEnabler</code> available in this session.
     *
     *  @param  queueConstrainerEnabler a queue constrainer enabler
     *  @throws org.osid.NullArgumentException <code>queueConstrainerEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putQueueConstrainerEnabler(org.osid.provisioning.rules.QueueConstrainerEnabler queueConstrainerEnabler) {
        super.putQueueConstrainerEnabler(queueConstrainerEnabler);

        this.queueConstrainerEnablersByGenus.put(queueConstrainerEnabler.getGenusType(), queueConstrainerEnabler);
        
        try (org.osid.type.TypeList types = queueConstrainerEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.queueConstrainerEnablersByRecord.put(types.getNextType(), queueConstrainerEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a queue constrainer enabler from this session.
     *
     *  @param queueConstrainerEnablerId the <code>Id</code> of the queue constrainer enabler
     *  @throws org.osid.NullArgumentException <code>queueConstrainerEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeQueueConstrainerEnabler(org.osid.id.Id queueConstrainerEnablerId) {
        org.osid.provisioning.rules.QueueConstrainerEnabler queueConstrainerEnabler;
        try {
            queueConstrainerEnabler = getQueueConstrainerEnabler(queueConstrainerEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.queueConstrainerEnablersByGenus.remove(queueConstrainerEnabler.getGenusType());

        try (org.osid.type.TypeList types = queueConstrainerEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.queueConstrainerEnablersByRecord.remove(types.getNextType(), queueConstrainerEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeQueueConstrainerEnabler(queueConstrainerEnablerId);
        return;
    }


    /**
     *  Gets a <code>QueueConstrainerEnablerList</code> corresponding to the given
     *  queue constrainer enabler genus <code>Type</code> which does not include
     *  queue constrainer enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known queue constrainer enablers or an error results. Otherwise,
     *  the returned list may contain only those queue constrainer enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  queueConstrainerEnablerGenusType a queue constrainer enabler genus type 
     *  @return the returned <code>QueueConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerEnablerList getQueueConstrainerEnablersByGenusType(org.osid.type.Type queueConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.rules.queueconstrainerenabler.ArrayQueueConstrainerEnablerList(this.queueConstrainerEnablersByGenus.get(queueConstrainerEnablerGenusType)));
    }


    /**
     *  Gets a <code>QueueConstrainerEnablerList</code> containing the given
     *  queue constrainer enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known queue constrainer enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  queue constrainer enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  queueConstrainerEnablerRecordType a queue constrainer enabler record type 
     *  @return the returned <code>queueConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerEnablerList getQueueConstrainerEnablersByRecordType(org.osid.type.Type queueConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.rules.queueconstrainerenabler.ArrayQueueConstrainerEnablerList(this.queueConstrainerEnablersByRecord.get(queueConstrainerEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.queueConstrainerEnablersByGenus.clear();
        this.queueConstrainerEnablersByRecord.clear();

        super.close();

        return;
    }
}
