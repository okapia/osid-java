//
// AbstractQueryCompositionLookupSession.java
//
//    An inline adapter that maps a CompositionLookupSession to
//    a CompositionQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.repository.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a CompositionLookupSession to
 *  a CompositionQuerySession.
 */

public abstract class AbstractQueryCompositionLookupSession
    extends net.okapia.osid.jamocha.repository.spi.AbstractCompositionLookupSession
    implements org.osid.repository.CompositionLookupSession {

    private boolean activeonly    = false;
    private boolean sequestered   = false;
    private final org.osid.repository.CompositionQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryCompositionLookupSession.
     *
     *  @param querySession the underlying composition query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryCompositionLookupSession(org.osid.repository.CompositionQuerySession querySession) {
        nullarg(querySession, "composition query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Repository</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Repository Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getRepositoryId() {
        return (this.session.getRepositoryId());
    }


    /**
     *  Gets the <code>Repository</code> associated with this 
     *  session.
     *
     *  @return the <code>Repository</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.Repository getRepository()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getRepository());
    }


    /**
     *  Tests if this user can perform <code>Composition</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCompositions() {
        return (this.session.canSearchCompositions());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include compositions in repositories which are children
     *  of this repository in the repository hierarchy.
     */

    @OSID @Override
    public void useFederatedRepositoryView() {
        this.session.useFederatedRepositoryView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this repository only.
     */

    @OSID @Override
    public void useIsolatedRepositoryView() {
        this.session.useIsolatedRepositoryView();
        return;
    }
    

    /**
     *  Only active compositions are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveCompositionView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive compositions are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusCompositionView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    

    /**
     *  The returns from the lookup methods omit sequestered
     *  compositions.
     */

    @OSID @Override
    public void useSequesteredCompositionView() {
        this.sequestered = true;
        return;
    }


    /**
     *  All compositions are returned including sequestered compositions.
     */

    @OSID @Override
    public void useUnsequesteredCompositionView() {
        this.sequestered = false;
        return;
    }


    /**
     *  Tests if a sequestered or unsequestered view is set.
     *
     *  @return <code>true</code> if sequestered</code>,
     *          <code>false</code> if unsequestered
     */

    protected boolean isSequestered() {
        return (this.sequestered);
    }

     
    /**
     *  Gets the <code>Composition</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Composition</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Composition</code> and
     *  retained for compatibility.
     *
     *  In active mode, compositions are returned that are currently
     *  active. In any status mode, active and inactive compositions
     *  are returned.
     *
     *  @param  compositionId <code>Id</code> of the
     *          <code>Composition</code>
     *  @return the composition
     *  @throws org.osid.NotFoundException <code>compositionId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>compositionId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.Composition getComposition(org.osid.id.Id compositionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.repository.CompositionQuery query = getQuery();
        query.matchId(compositionId, true);
        org.osid.repository.CompositionList compositions = this.session.getCompositionsByQuery(query);
        if (compositions.hasNext()) {
            return (compositions.getNextComposition());
        } 
        
        throw new org.osid.NotFoundException(compositionId + " not found");
    }


    /**
     *  Gets a <code>CompositionList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  compositions specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Compositions</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, compositions are returned that are currently
     *  active. In any status mode, active and inactive compositions
     *  are returned.
     *
     *  @param  compositionIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Composition</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>compositionIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.CompositionList getCompositionsByIds(org.osid.id.IdList compositionIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.repository.CompositionQuery query = getQuery();

        try (org.osid.id.IdList ids = compositionIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getCompositionsByQuery(query));
    }


    /**
     *  Gets a <code>CompositionList</code> corresponding to the given
     *  composition genus <code>Type</code> which does not include
     *  compositions of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  compositions or an error results. Otherwise, the returned list
     *  may contain only those compositions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, compositions are returned that are currently
     *  active. In any status mode, active and inactive compositions
     *  are returned.
     *
     *  @param  compositionGenusType a composition genus type 
     *  @return the returned <code>Composition</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>compositionGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.CompositionList getCompositionsByGenusType(org.osid.type.Type compositionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.repository.CompositionQuery query = getQuery();
        query.matchGenusType(compositionGenusType, true);
        return (this.session.getCompositionsByQuery(query));
    }


    /**
     *  Gets a <code>CompositionList</code> corresponding to the given
     *  composition genus <code>Type</code> and include any additional
     *  compositions with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  compositions or an error results. Otherwise, the returned list
     *  may contain only those compositions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, compositions are returned that are currently
     *  active. In any status mode, active and inactive compositions
     *  are returned.
     *
     *  @param  compositionGenusType a composition genus type 
     *  @return the returned <code>Composition</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>compositionGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.CompositionList getCompositionsByParentGenusType(org.osid.type.Type compositionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.repository.CompositionQuery query = getQuery();
        query.matchParentGenusType(compositionGenusType, true);
        return (this.session.getCompositionsByQuery(query));
    }


    /**
     *  Gets a <code>CompositionList</code> containing the given
     *  composition record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  compositions or an error results. Otherwise, the returned list
     *  may contain only those compositions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, compositions are returned that are currently
     *  active. In any status mode, active and inactive compositions
     *  are returned.
     *
     *  @param  compositionRecordType a composition record type 
     *  @return the returned <code>Composition</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>compositionRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.CompositionList getCompositionsByRecordType(org.osid.type.Type compositionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.repository.CompositionQuery query = getQuery();
        query.matchRecordType(compositionRecordType, true);
        return (this.session.getCompositionsByQuery(query));
    }


    /**
     *  Gets a <code>CompositionList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known compositions or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  compositions that are accessible through this session. 
     *
     *  In active mode, compositions are returned that are currently
     *  active. In any status mode, active and inactive compositions
     *  are returned.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Composition</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.repository.CompositionList getCompositionsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.repository.CompositionQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getCompositionsByQuery(query));        
    }

    
    /**
     *  Gets all <code>Compositions</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  compositions or an error results. Otherwise, the returned list
     *  may contain only those compositions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, compositions are returned that are currently
     *  active. In any status mode, active and inactive compositions
     *  are returned.
     *
     *  @return a list of <code>Compositions</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.CompositionList getCompositions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.repository.CompositionQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getCompositionsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.repository.CompositionQuery getQuery() {
        org.osid.repository.CompositionQuery query = this.session.getCompositionQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        if (isSequestered()) {
            query.matchSequestered(true);
        }

        return (query);
    }
}
