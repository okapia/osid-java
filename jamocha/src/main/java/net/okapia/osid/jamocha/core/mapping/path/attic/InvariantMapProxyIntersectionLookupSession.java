//
// InvariantMapProxyIntersectionLookupSession
//
//    Implements an Intersection lookup service backed by a fixed
//    collection of intersections. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.path;


/**
 *  Implements an Intersection lookup service backed by a fixed
 *  collection of intersections. The intersections are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyIntersectionLookupSession
    extends net.okapia.osid.jamocha.core.mapping.path.spi.AbstractMapIntersectionLookupSession
    implements org.osid.mapping.path.IntersectionLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapProxyIntersectionLookupSession</code> with no
     *  intersections.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException <code>proxy</code> is
     *          <code>null</code>
     */

    public InvariantMapProxyIntersectionLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new <code>InvariantMapProxyIntersectionLookupSession</code> with a
     *  single intersection.
     *
     *  @param  intersection an single intersection
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException <code>intersection</code> or
     *          <code>proxy</code> is <code>null</code>
     */

    public InvariantMapProxyIntersectionLookupSession(org.osid.mapping.path.Intersection intersection, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putIntersection(intersection);
        return;
    }


    /**
     *  Constructs a new <code>InvariantMapProxyIntersectionLookupSession</code> using
     *  an array of intersections.
     *
     *  @param  intersections an array of intersections
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException <code>intersections</code> or
     *          <code>proxy</code> is <code>null</code>
     */

    public InvariantMapProxyIntersectionLookupSession(org.osid.mapping.path.Intersection[] intersections, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putIntersections(intersections);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapProxyIntersectionLookupSession</code> using a
     *  collection of intersections.
     *
     *  @param  intersections a collection of intersections
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException <code>intersections</code> or
     *          <code>proxy</code> is <code>null</code>
     */

    public InvariantMapProxyIntersectionLookupSession(java.util.Collection<? extends org.osid.mapping.path.Intersection> intersections,
                                                  org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putIntersections(intersections);
        return;
    }
}
