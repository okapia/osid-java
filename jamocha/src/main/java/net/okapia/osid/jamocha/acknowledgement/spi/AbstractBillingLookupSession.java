//
// AbstractBillingLookupSession.java
//
//    A starter implementation framework for providing a Billing
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.acknowledgement.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A starter implementation framework for providing a Billing
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getBillings(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractBillingLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.acknowledgement.BillingLookupSession {

    private boolean pedantic = false;
    private org.osid.acknowledgement.Billing billing = new net.okapia.osid.jamocha.nil.acknowledgement.billing.UnknownBilling();
    

    /**
     *  Tests if this user can perform <code>Billing</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupBillings() {
        return (true);
    }


    /**
     *  A complete view of the <code>Billing</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeBillingView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Billing</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryBillingView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }

     
    /**
     *  Gets the <code>Billing</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code> Id </code> is found or a
     *  <code> NOT_FOUND </code> results. Otherwise, the returned
     *  <code> Credit </code> may have a different <code> Id </code>
     *  than requested, such as the case where a duplicate <code> Id
     *  </code> was assigned to a <code> Credit </code> and retained
     *  for compatibility.
     *
     *  @param  billingId <code>Id</code> of the
     *          <code>Billing</code>
     *  @return the billing
     *  @throws org.osid.NotFoundException <code>billingId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>billingId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.Billing getBilling(org.osid.id.Id billingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.acknowledgement.BillingList billings = getBillings()) {
            while (billings.hasNext()) {
                org.osid.acknowledgement.Billing billing = billings.getNextBilling();
                if (billing.getId().equals(billingId)) {
                    return (billing);
                }
            }
        } 

        throw new org.osid.NotFoundException(billingId + " not found");
    }


    /**
     *  Gets a <code>BillingList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  billings specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Billings</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getBillings()</code>.
     *
     *  @param  billingIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Billing</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not found 
     *  @throws org.osid.NullArgumentException
     *          <code>billingIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingList getBillingsByIds(org.osid.id.IdList billingIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.acknowledgement.Billing> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = billingIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getBilling(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("billing " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.acknowledgement.billing.LinkedBillingList(ret));
    }


    /**
     *  Gets a <code>BillingList</code> corresponding to the given
     *  billing genus <code>Type</code> which does not include
     *  billings of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  billings or an error results. Otherwise, the returned list
     *  may contain only those billings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getBillings()</code>.
     *
     *  @param  billingGenusType a billing genus type 
     *  @return the returned <code>Billing</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>billingGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingList getBillingsByGenusType(org.osid.type.Type billingGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.acknowledgement.billing.BillingGenusFilterList(getBillings(), billingGenusType));
    }


    /**
     *  Gets a <code>BillingList</code> corresponding to the given
     *  billing genus <code>Type</code> and include any additional
     *  billings with genus types derived from the specified
     *  <code>Type</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  billings or an error results. Otherwise, the returned list
     *  may contain only those billings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getBillings()</code>.
     *
     *  @param  billingGenusType a billing genus type 
     *  @return the returned <code>Billing</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>billingGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingList getBillingsByParentGenusType(org.osid.type.Type billingGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getBillingsByGenusType(billingGenusType));
    }


    /**
     *  Gets a <code>BillingList</code> containing the given
     *  billing record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  billings or an error results. Otherwise, the returned list
     *  may contain only those billings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getBillings()</code>.
     *
     *  @param  billingRecordType a billing record type 
     *  @return the returned <code>Billing</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>billingRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingList getBillingsByRecordType(org.osid.type.Type billingRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.acknowledgement.billing.BillingRecordFilterList(getBillings(), billingRecordType));
    }


    /**
     *  Gets a <code>BillingList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known billings or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  billings that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Billing</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingList getBillingsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.acknowledgement.billing.BillingProviderFilterList(getBillings(), resourceId));
    }


    /**
     *  Gets all <code>Billings</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  billings or an error results. Otherwise, the returned list
     *  may contain only those billings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Billings</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.acknowledgement.BillingList getBillings()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the billing list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of billings
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.acknowledgement.BillingList filterBillingsOnViews(org.osid.acknowledgement.BillingList list)
        throws org.osid.OperationFailedException {
            
        return (list);
    }
}
