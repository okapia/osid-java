//
// AbstractMapActivityBundleLookupSession
//
//    A simple framework for providing an ActivityBundle lookup service
//    backed by a fixed collection of activity bundles.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.registration.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an ActivityBundle lookup service backed by a
 *  fixed collection of activity bundles. The activity bundles are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>ActivityBundles</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapActivityBundleLookupSession
    extends net.okapia.osid.jamocha.course.registration.spi.AbstractActivityBundleLookupSession
    implements org.osid.course.registration.ActivityBundleLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.course.registration.ActivityBundle> activityBundles = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.course.registration.ActivityBundle>());


    /**
     *  Makes an <code>ActivityBundle</code> available in this session.
     *
     *  @param  activityBundle an activity bundle
     *  @throws org.osid.NullArgumentException <code>activityBundle<code>
     *          is <code>null</code>
     */

    protected void putActivityBundle(org.osid.course.registration.ActivityBundle activityBundle) {
        this.activityBundles.put(activityBundle.getId(), activityBundle);
        return;
    }


    /**
     *  Makes an array of activity bundles available in this session.
     *
     *  @param  activityBundles an array of activity bundles
     *  @throws org.osid.NullArgumentException <code>activityBundles<code>
     *          is <code>null</code>
     */

    protected void putActivityBundles(org.osid.course.registration.ActivityBundle[] activityBundles) {
        putActivityBundles(java.util.Arrays.asList(activityBundles));
        return;
    }


    /**
     *  Makes a collection of activity bundles available in this session.
     *
     *  @param  activityBundles a collection of activity bundles
     *  @throws org.osid.NullArgumentException <code>activityBundles<code>
     *          is <code>null</code>
     */

    protected void putActivityBundles(java.util.Collection<? extends org.osid.course.registration.ActivityBundle> activityBundles) {
        for (org.osid.course.registration.ActivityBundle activityBundle : activityBundles) {
            this.activityBundles.put(activityBundle.getId(), activityBundle);
        }

        return;
    }


    /**
     *  Removes an ActivityBundle from this session.
     *
     *  @param  activityBundleId the <code>Id</code> of the activity bundle
     *  @throws org.osid.NullArgumentException <code>activityBundleId<code> is
     *          <code>null</code>
     */

    protected void removeActivityBundle(org.osid.id.Id activityBundleId) {
        this.activityBundles.remove(activityBundleId);
        return;
    }


    /**
     *  Gets the <code>ActivityBundle</code> specified by its <code>Id</code>.
     *
     *  @param  activityBundleId <code>Id</code> of the <code>ActivityBundle</code>
     *  @return the activityBundle
     *  @throws org.osid.NotFoundException <code>activityBundleId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>activityBundleId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundle getActivityBundle(org.osid.id.Id activityBundleId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.course.registration.ActivityBundle activityBundle = this.activityBundles.get(activityBundleId);
        if (activityBundle == null) {
            throw new org.osid.NotFoundException("activityBundle not found: " + activityBundleId);
        }

        return (activityBundle);
    }


    /**
     *  Gets all <code>ActivityBundles</code>. In plenary mode, the returned
     *  list contains all known activityBundles or an error
     *  results. Otherwise, the returned list may contain only those
     *  activityBundles that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>ActivityBundles</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleList getActivityBundles()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.registration.activitybundle.ArrayActivityBundleList(this.activityBundles.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.activityBundles.clear();
        super.close();
        return;
    }
}
