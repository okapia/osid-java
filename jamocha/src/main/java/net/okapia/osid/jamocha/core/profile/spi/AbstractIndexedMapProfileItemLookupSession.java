//
// AbstractIndexedMapProfileItemLookupSession.java
//
//    A simple framework for providing a ProfileItem lookup service
//    backed by a fixed collection of profile items with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.profile.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a ProfileItem lookup service backed by a
 *  fixed collection of profile items. The profile items are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some profile items may be compatible
 *  with more types than are indicated through these profile item
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>ProfileItems</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapProfileItemLookupSession
    extends AbstractMapProfileItemLookupSession
    implements org.osid.profile.ProfileItemLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.profile.ProfileItem> profileItemsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.profile.ProfileItem>());
    private final MultiMap<org.osid.type.Type, org.osid.profile.ProfileItem> profileItemsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.profile.ProfileItem>());


    /**
     *  Makes a <code>ProfileItem</code> available in this session.
     *
     *  @param  profileItem a profile item
     *  @throws org.osid.NullArgumentException <code>profileItem<code> is
     *          <code>null</code>
     */

    @Override
    protected void putProfileItem(org.osid.profile.ProfileItem profileItem) {
        super.putProfileItem(profileItem);

        this.profileItemsByGenus.put(profileItem.getGenusType(), profileItem);
        
        try (org.osid.type.TypeList types = profileItem.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.profileItemsByRecord.put(types.getNextType(), profileItem);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a profile item from this session.
     *
     *  @param profileItemId the <code>Id</code> of the profile item
     *  @throws org.osid.NullArgumentException <code>profileItemId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeProfileItem(org.osid.id.Id profileItemId) {
        org.osid.profile.ProfileItem profileItem;
        try {
            profileItem = getProfileItem(profileItemId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.profileItemsByGenus.remove(profileItem.getGenusType());

        try (org.osid.type.TypeList types = profileItem.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.profileItemsByRecord.remove(types.getNextType(), profileItem);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeProfileItem(profileItemId);
        return;
    }


    /**
     *  Gets a <code>ProfileItemList</code> corresponding to the given
     *  profile item genus <code>Type</code> which does not include
     *  profile items of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known profile items or an error results. Otherwise,
     *  the returned list may contain only those profile items that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  profileItemGenusType a profile item genus type 
     *  @return the returned <code>ProfileItem</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>profileItemGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemList getProfileItemsByGenusType(org.osid.type.Type profileItemGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.profile.profileitem.ArrayProfileItemList(this.profileItemsByGenus.get(profileItemGenusType)));
    }


    /**
     *  Gets a <code>ProfileItemList</code> containing the given
     *  profile item record <code>Type</code>. In plenary mode, the
     *  returned list contains all known profile items or an error
     *  results. Otherwise, the returned list may contain only those
     *  profile items that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  profileItemRecordType a profile item record type 
     *  @return the returned <code>profileItem</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>profileItemRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemList getProfileItemsByRecordType(org.osid.type.Type profileItemRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.profile.profileitem.ArrayProfileItemList(this.profileItemsByRecord.get(profileItemRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.profileItemsByGenus.clear();
        this.profileItemsByRecord.clear();

        super.close();

        return;
    }
}
