//
// AbstractDirectoryLookupSession.java
//
//    A starter implementation framework for providing a Directory
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.filing.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Directory
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getDirectories(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractDirectoryLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.filing.DirectoryLookupSession {

    private boolean pedantic      = false;
    private boolean federated     = false;
    private org.osid.filing.Directory directory = new net.okapia.osid.jamocha.nil.filing.directory.UnknownDirectory();
    

    /**
     *  Gets the <code>Directory/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Directory Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDirectoryId() {
        return (this.directory.getId());
    }


    /**
     *  Gets the <code>Directory</code> associated with this 
     *  session.
     *
     *  @return the <code>Directory</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.Directory getDirectory()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.directory);
    }


    /**
     *  Sets the <code>Directory</code>.
     *
     *  @param  directory the directory for this session
     *  @throws org.osid.NullArgumentException <code>directory</code>
     *          is <code>null</code>
     */

    protected void setDirectory(org.osid.filing.Directory directory) {
        nullarg(directory, "directory");
        this.directory = directory;
        return;
    }


    /**
     *  Tests if this user can perform <code>Directory</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupDirectories() {
        return (true);
    }


    /**
     *  A complete view of the <code>Directory</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeDirectoryView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Directory</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryDirectoryView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include directories in directories which are
     *  children of this directory in the directory hierarchy.
     */

    @OSID @Override
    public void useFederatedDirectoryView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this directory only.
     */

    @OSID @Override
    public void useIsolatedDirectoryView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>Directory</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Directory</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Directory</code> and
     *  retained for compatibility.
     *
     *  @param  directoryId <code>Id</code> of the
     *          <code>Directory</code>
     *  @return the directory
     *  @throws org.osid.NotFoundException <code>directoryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>directoryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.Directory getDirectory(org.osid.id.Id directoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.filing.DirectoryList directories = getDirectories()) {
            while (directories.hasNext()) {
                org.osid.filing.Directory directory = directories.getNextDirectory();
                if (directory.getId().equals(directoryId)) {
                    return (directory);
                }
            }
        } 

        throw new org.osid.NotFoundException(directoryId + " not found");
    }


    /**
     *  Gets a <code>DirectoryList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  directories specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Directories</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getDirectories()</code>.
     *
     *  @param  directoryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Directory</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
*               found
     *  @throws org.osid.NullArgumentException
     *          <code>directoryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.DirectoryList getDirectoriesByIds(org.osid.id.IdList directoryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.filing.Directory> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = directoryIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getDirectory(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("directory " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.filing.directory.LinkedDirectoryList(ret));
    }


    /**
     *  Gets a <code>DirectoryList</code> corresponding to the given
     *  directory genus <code>Type</code> which does not include
     *  directories of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  directories or an error results. Otherwise, the returned list
     *  may contain only those directories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getDirectories()</code>.
     *
     *  @param  directoryGenusType a directory genus type 
     *  @return the returned <code>Directory</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>directoryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.DirectoryList getDirectoriesByGenusType(org.osid.type.Type directoryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.filing.directory.DirectoryGenusFilterList(getDirectories(), directoryGenusType));
    }


    /**
     *  Gets a <code>DirectoryList</code> corresponding to the given
     *  directory genus <code>Type</code> and include any additional
     *  directories with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  directories or an error results. Otherwise, the returned list
     *  may contain only those directories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getDirectories()</code>.
     *
     *  @param  directoryGenusType a directory genus type 
     *  @return the returned <code>Directory</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>directoryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.DirectoryList getDirectoriesByParentGenusType(org.osid.type.Type directoryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getDirectoriesByGenusType(directoryGenusType));
    }


    /**
     *  Gets a <code>DirectoryList</code> containing the given
     *  directory record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  directories or an error results. Otherwise, the returned list
     *  may contain only those directories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getDirectories()</code>.
     *
     *  @param  directoryRecordType a directory record type 
     *  @return the returned <code>Directory</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>directoryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.DirectoryList getDirectoriesByRecordType(org.osid.type.Type directoryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.filing.directory.DirectoryRecordFilterList(getDirectories(), directoryRecordType));
    }


    /**
     *  Gets a <code>DirectoryList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  directories or an error results. Otherwise, the returned list
     *  may contain only those directories that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Directory</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.filing.DirectoryList getDirectoriesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.filing.directory.DirectoryProviderFilterList(getDirectories(), resourceId));
    }


    /**
     *  Gets all <code>Directories</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  directories or an error results. Otherwise, the returned list
     *  may contain only those directories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Directories</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.filing.DirectoryList getDirectories()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the directory list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of directories
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.filing.DirectoryList filterDirectoriesOnViews(org.osid.filing.DirectoryList list)
        throws org.osid.OperationFailedException {

        org.osid.filing.DirectoryList ret = list;

        return (ret);
    }
}
