//
// AbstractOperableValidator.java
//
//     Validates Operables.
//
//
// Tom Coppeto
// Okapia
// 20 September 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.spi;

import net.okapia.osid.jamocha.builder.validator.Validation;


/**
 *  Validates an Operable.
 */

public abstract class AbstractOperableValidator
    extends AbstractValidator {


    /**
     *  Constructs a new <code>AbstractOperableValidator</code>.
     */

    protected AbstractOperableValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AbstractOperableValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractOperableValidator(java.util.EnumSet<Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates an Operable.
     *
     *  @param operable the operable to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.NullArgumentException <code>operable</code> is
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred
     */

    public void validate(org.osid.Operable operable) {
        super.validate(operable);

        if (perform(Validation.METHOD_SUPPORT)) {
            if (operable.isActive()) {
                if (!(operable.isEnabled() && operable.isOperational())) {
                    throw new org.osid.BadLogicException("operable not active");
                }
            } else {
                if (operable.isEnabled() && operable.isOperational()) {
                    throw new org.osid.BadLogicException("operable is active");
                }
            }

            if (operable.isEnabled() && operable.isDisabled()) {
                throw new org.osid.BadLogicException("isEnabled() and isDisabled() are true");
            }
        }

        return;
    }
}
