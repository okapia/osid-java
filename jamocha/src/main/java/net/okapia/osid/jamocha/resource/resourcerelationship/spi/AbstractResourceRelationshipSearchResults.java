//
// AbstractResourceRelationshipSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resource.resourcerelationship.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractResourceRelationshipSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.resource.ResourceRelationshipSearchResults {

    private org.osid.resource.ResourceRelationshipList resourceRelationships;
    private final org.osid.resource.ResourceRelationshipQueryInspector inspector;
    private final java.util.Collection<org.osid.resource.records.ResourceRelationshipSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractResourceRelationshipSearchResults.
     *
     *  @param resourceRelationships the result set
     *  @param resourceRelationshipQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>resourceRelationships</code>
     *          or <code>resourceRelationshipQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractResourceRelationshipSearchResults(org.osid.resource.ResourceRelationshipList resourceRelationships,
                                            org.osid.resource.ResourceRelationshipQueryInspector resourceRelationshipQueryInspector) {
        nullarg(resourceRelationships, "resource relationships");
        nullarg(resourceRelationshipQueryInspector, "resource relationship query inspectpr");

        this.resourceRelationships = resourceRelationships;
        this.inspector = resourceRelationshipQueryInspector;

        return;
    }


    /**
     *  Gets the resource relationship list resulting from a search.
     *
     *  @return a resource relationship list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationships() {
        if (this.resourceRelationships == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.resource.ResourceRelationshipList resourceRelationships = this.resourceRelationships;
        this.resourceRelationships = null;
	return (resourceRelationships);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.resource.ResourceRelationshipQueryInspector getResourceRelationshipQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  resource relationship search record <code> Type. </code> This method must
     *  be used to retrieve a resourceRelationship implementing the requested
     *  record.
     *
     *  @param resourceRelationshipSearchRecordType a resourceRelationship search 
     *         record type 
     *  @return the resource relationship search
     *  @throws org.osid.NullArgumentException
     *          <code>resourceRelationshipSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(resourceRelationshipSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resource.records.ResourceRelationshipSearchResultsRecord getResourceRelationshipSearchResultsRecord(org.osid.type.Type resourceRelationshipSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.resource.records.ResourceRelationshipSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(resourceRelationshipSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(resourceRelationshipSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record resource relationship search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addResourceRelationshipRecord(org.osid.resource.records.ResourceRelationshipSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "resource relationship record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
