//
// AbstractAssemblyAssessmentTakenQuery.java
//
//     An AssessmentTakenQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.assessment.assessmenttaken.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An AssessmentTakenQuery that stores terms.
 */

public abstract class AbstractAssemblyAssessmentTakenQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.assessment.AssessmentTakenQuery,
               org.osid.assessment.AssessmentTakenQueryInspector,
               org.osid.assessment.AssessmentTakenSearchOrder {

    private final java.util.Collection<org.osid.assessment.records.AssessmentTakenQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.assessment.records.AssessmentTakenQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.assessment.records.AssessmentTakenSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyAssessmentTakenQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyAssessmentTakenQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the assessment offered <code> Id </code> for this query. 
     *
     *  @param  assessmentOfferedId an assessment <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentOfferedId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchAssessmentOfferedId(org.osid.id.Id assessmentOfferedId, 
                                         boolean match) {
        getAssembler().addIdTerm(getAssessmentOfferedIdColumn(), assessmentOfferedId, match);
        return;
    }


    /**
     *  Clears all assessment offered <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAssessmentOfferedIdTerms() {
        getAssembler().clearTerms(getAssessmentOfferedIdColumn());
        return;
    }


    /**
     *  Gets the assessment offered <code> Id </code> query terms. 
     *
     *  @return the assessment offered <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssessmentOfferedIdTerms() {
        return (getAssembler().getIdTerms(getAssessmentOfferedIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the assessment 
     *  offered. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAssessmentOffered(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAssessmentOfferedColumn(), style);
        return;
    }


    /**
     *  Gets the AssessmentOfferedId column name.
     *
     * @return the column name
     */

    protected String getAssessmentOfferedIdColumn() {
        return ("assessment_offered_id");
    }


    /**
     *  Tests if an <code> AssessmentOfferedQuery </code> is available. 
     *
     *  @return <code> true </code> if an assessment offered query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentOfferedQuery() {
        return (false);
    }


    /**
     *  Gets the query for an assessment. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the assessment offered query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentOfferedQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedQuery getAssessmentOfferedQuery() {
        throw new org.osid.UnimplementedException("supportsAssessmentOfferedQuery() is false");
    }


    /**
     *  Clears all assessment offered terms. 
     */

    @OSID @Override
    public void clearAssessmentOfferedTerms() {
        getAssembler().clearTerms(getAssessmentOfferedColumn());
        return;
    }


    /**
     *  Gets the assessment offered query terms. 
     *
     *  @return the assessment offered terms 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedQueryInspector[] getAssessmentOfferedTerms() {
        return (new org.osid.assessment.AssessmentOfferedQueryInspector[0]);
    }


    /**
     *  Tests if an assessment search order is available. 
     *
     *  @return <code> true </code> if an assessment offered search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentOfferedSearchOrder() {
        return (false);
    }


    /**
     *  Gets an assessment offered search order. 
     *
     *  @return an assessment offered search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentOfferedSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedSearchOrder getAssessmentOfferedSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAssessmentOfferedSearchOrder() is false");
    }


    /**
     *  Gets the AssessmentOffered column name.
     *
     * @return the column name
     */

    protected String getAssessmentOfferedColumn() {
        return ("assessment_offered");
    }


    /**
     *  Sets the resource <code> Id </code> for this query. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchTakerId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getTakerIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears all resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearTakerIdTerms() {
        getAssembler().clearTerms(getTakerIdColumn());
        return;
    }


    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the resource <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getTakerIdTerms() {
        return (getAssembler().getIdTerms(getTakerIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTaker(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTakerColumn(), style);
        return;
    }


    /**
     *  Gets the TakerId column name.
     *
     * @return the column name
     */

    protected String getTakerIdColumn() {
        return ("taker_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTakerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsTakerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getTakerQuery() {
        throw new org.osid.UnimplementedException("supportsTakerQuery() is false");
    }


    /**
     *  Clears all resource terms. 
     */

    @OSID @Override
    public void clearTakerTerms() {
        getAssembler().clearTerms(getTakerColumn());
        return;
    }


    /**
     *  Gets the resource query terms. 
     *
     *  @return the resource terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getTakerTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTakerSearchOrder() {
        return (false);
    }


    /**
     *  Gets a resource search order. 
     *
     *  @return a resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTakerSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getTakerSearchOrder() {
        throw new org.osid.UnimplementedException("supportsTakerSearchOrder() is false");
    }


    /**
     *  Gets the Taker column name.
     *
     * @return the column name
     */

    protected String getTakerColumn() {
        return ("taker");
    }


    /**
     *  Sets the agent <code> Id </code> for this query. 
     *
     *  @param  agentId an agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchTakingAgentId(org.osid.id.Id agentId, boolean match) {
        getAssembler().addIdTerm(getTakingAgentIdColumn(), agentId, match);
        return;
    }


    /**
     *  Clears all agent <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearTakingAgentIdTerms() {
        getAssembler().clearTerms(getTakingAgentIdColumn());
        return;
    }


    /**
     *  Gets the agent <code> Id </code> query terms. 
     *
     *  @return the agent <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getTakingAgentIdTerms() {
        return (getAssembler().getIdTerms(getTakingAgentIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the agent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTakingAgent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTakingAgentColumn(), style);
        return;
    }


    /**
     *  Gets the TakingAgentId column name.
     *
     * @return the column name
     */

    protected String getTakingAgentIdColumn() {
        return ("taking_agent_id");
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTakingAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agent. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTakingAgentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getTakingAgentQuery() {
        throw new org.osid.UnimplementedException("supportsTakingAgentQuery() is false");
    }


    /**
     *  Clears all taking agent terms. 
     */

    @OSID @Override
    public void clearTakingAgentTerms() {
        getAssembler().clearTerms(getTakingAgentColumn());
        return;
    }


    /**
     *  Gets the agent query terms. 
     *
     *  @return the agent terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getTakingAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Tests if an agent search order is available. 
     *
     *  @return <code> true </code> if an agent search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTakingAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets an agent search order. 
     *
     *  @return an agent search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTakingAgentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getTakingAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsTakingAgentSearchOrder() is false");
    }


    /**
     *  Gets the TakingAgent column name.
     *
     *  @return the column name
     */

    protected String getTakingAgentColumn() {
        return ("taking_agent");
    }


    /**
     *  Matches assessments whose actual start time falls between the
     *  specified range inclusive.
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is less 
     *          than <code> start </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchActualStartTime(org.osid.calendaring.DateTime start, 
                                     org.osid.calendaring.DateTime end, 
                                     boolean match) {
        getAssembler().addDateTimeRangeTerm(getActualStartTimeColumn(), start, end, match);
        return;
    }


    /**
     *  Matches started taken assessments.
     *
     *  @param match <code> true </code> to match assessments with any
     *          start time, <code> false </code> to match assessments
     *          not begun
     */

    @OSID @Override
    public void matchAnyActualStartTime(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getActualStartTimeColumn(), match);
        return;
    }


    /**
     *  Clears all actual start time terms. 
     */

    @OSID @Override
    public void clearActualStartTimeTerms() {
        getAssembler().clearTerms(getActualStartTimeColumn());
        return;
    }


    /**
     *  Gets the actual start time query terms. 
     *
     *  @return the actual start time terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeTerm[] getActualStartTimeTerms() {
        return (getAssembler().getDateTimeTerms(getActualStartTimeColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the assessment 
     *  completion time. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByActualStartTime(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getActualStartTimeColumn(), style);
        return;
    }


    /**
     *  Gets the ActualStartTime column name.
     *
     *  @return the column name
     */

    protected String getActualStartTimeColumn() {
        return ("actual_start_time");
    }


    /**
     *  Matches assessments whose completion time falls between the specified 
     *  range inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is less 
     *          than <code> start </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchCompletionTime(org.osid.calendaring.DateTime start, 
                                    org.osid.calendaring.DateTime end, 
                                    boolean match) {
        getAssembler().addDateTimeRangeTerm(getCompletionTimeColumn(), start, end, match);
        return;
    }


    /**
     *  Matches completed taken assessments.
     *
     *  @param match <code> true </code> to match assessments with any
     *          completion time, <code> false </code> to match
     *          incomplete assessments
     */

    @OSID @Override
    public void matchAnyCompletionTime(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getCompletionTimeColumn(), match);
        return;
    }


    /**
     *  Clears all in completion time terms. 
     */

    @OSID @Override
    public void clearCompletionTimeTerms() {
        getAssembler().clearTerms(getCompletionTimeColumn());
        return;
    }


    /**
     *  Gets the completion time query terms. 
     *
     *  @return the completion time terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeTerm[] getCompletionTimeTerms() {
        return (getAssembler().getDateTimeTerms(getCompletionTimeColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the assessment 
     *  deadline. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCompletionTime(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCompletionTimeColumn(), style);
        return;
    }


    /**
     *  Gets the CompletionTime column name.
     *
     * @return the column name
     */

    protected String getCompletionTimeColumn() {
        return ("completion_time");
    }


    /**
     *  Matches assessments where the time spent falls between the specified 
     *  range inclusive. 
     *
     *  @param  low start of duration range 
     *  @param  high end of duration range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     *  @throws org.osid.NullArgumentException <code> low </code> or <code> 
     *          high </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchTimeSpent(org.osid.calendaring.Duration low, 
                               org.osid.calendaring.Duration high, 
                               boolean match) {
        getAssembler().addDurationRangeTerm(getTimeSpentColumn(), low, high, match);
        return;
    }


    /**
     *  Clears all in time spent terms. 
     */

    @OSID @Override
    public void clearTimeSpentTerms() {
        getAssembler().clearTerms(getTimeSpentColumn());
        return;
    }


    /**
     *  Gets the time spent query terms. 
     *
     *  @return the time spent terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationTerm[] getTimeSpentTerms() {
        return (getAssembler().getDurationTerms(getTimeSpentColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the time spent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTimeSpent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTimeSpentColumn(), style);
        return;
    }


    /**
     *  Gets the TimeSpent column name.
     *
     * @return the column name
     */

    protected String getTimeSpentColumn() {
        return ("time_spent");
    }


    /**
     *  Sets the grade system <code> Id </code> for this query. 
     *
     *  @param  gradeSystemId a grade system <code> Id </code> 
     *  @param  match <code> true for a positive match, false for a negative 
     *          match </code> 
     *  @throws org.osid.NullArgumentException <code> gradeSystemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchScoreSystemId(org.osid.id.Id gradeSystemId, boolean match) {
        getAssembler().addIdTerm(getScoreSystemIdColumn(), gradeSystemId, match);
        return;
    }


    /**
     *  Clears all grade system <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearScoreSystemIdTerms() {
        getAssembler().clearTerms(getScoreSystemIdColumn());
        return;
    }


    /**
     *  Gets the grade system <code> Id </code> query terms. 
     *
     *  @return the grade system <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getScoreSystemIdTerms() {
        return (getAssembler().getIdTerms(getScoreSystemIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the grade 
     *  system. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByScoreSystem(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getScoreSystemColumn(), style);
        return;
    }


    /**
     *  Gets the ScoreSystemId column name.
     *
     * @return the column name
     */

    protected String getScoreSystemIdColumn() {
        return ("score_system_id");
    }


    /**
     *  Tests if a <code> GradeSystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade system query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScoreSystemQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade system. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the grade system query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScoreSystemQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQuery getScoreSystemQuery() {
        throw new org.osid.UnimplementedException("supportsScoreSystemQuery() is false");
    }


    /**
     *  Matches taken assessments that have any grade system assigned. 
     *
     *  @param  match <code> true </code> to match assessments with any grade 
     *          system, <code> false </code> to match assessments with no 
     *          grade system 
     */

    @OSID @Override
    public void matchAnyScoreSystem(boolean match) {
        getAssembler().addIdWildcardTerm(getScoreSystemColumn(), match);
        return;
    }


    /**
     *  Clears all grade system terms. 
     */

    @OSID @Override
    public void clearScoreSystemTerms() {
        getAssembler().clearTerms(getScoreSystemColumn());
        return;
    }


    /**
     *  Gets the grade system query terms. 
     *
     *  @return the grade system terms 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQueryInspector[] getScoreSystemTerms() {
        return (new org.osid.grading.GradeSystemQueryInspector[0]);
    }


    /**
     *  Tests if a grade system search order is available. 
     *
     *  @return <code> true </code> if a grade system search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScoreSystemSearchOrder() {
        return (false);
    }


    /**
     *  Gets a grade system search order. 
     *
     *  @return a grade system search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScoreSystemSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemSearchOrder getScoreSystemSearchOrder() {
        throw new org.osid.UnimplementedException("supportsScoreSystemSearchOrder() is false");
    }


    /**
     *  Gets the ScoreSystem column name.
     *
     * @return the column name
     */

    protected String getScoreSystemColumn() {
        return ("score_system");
    }


    /**
     *  Matches assessments whose score falls between the specified range 
     *  inclusive. 
     *
     *  @param  low start of range 
     *  @param  high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchScore(java.math.BigDecimal low, java.math.BigDecimal high, 
                           boolean match) {
        getAssembler().addDecimalRangeTerm(getScoreColumn(), low, high, match);
        return;
    }


    /**
     *  Matches taken assessments that have any score assigned. 
     *
     *  @param  match <code> true </code> to match assessments with any score, 
     *          <code> false </code> to match assessments with no score 
     */

    @OSID @Override
    public void matchAnyScore(boolean match) {
        getAssembler().addDecimalRangeWildcardTerm(getScoreColumn(), match);
        return;
    }


    /**
     *  Clears all score terms. 
     */

    @OSID @Override
    public void clearScoreTerms() {
        getAssembler().clearTerms(getScoreColumn());
        return;
    }


    /**
     *  Gets the score query terms. 
     *
     *  @return the score terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getScoreTerms() {
        return (getAssembler().getDecimalRangeTerms(getScoreColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the score. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByScore(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getScoreColumn(), style);
        return;
    }


    /**
     *  Gets the Score column name.
     *
     * @return the column name
     */

    protected String getScoreColumn() {
        return ("score");
    }


    /**
     *  Sets the grade <code> Id </code> for this query. 
     *
     *  @param  gradeId a grade <code> Id </code> 
     *  @param  match <code> true for a positive match, false for a negative 
     *          match </code> 
     *  @throws org.osid.NullArgumentException <code> gradeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGradeId(org.osid.id.Id gradeId, boolean match) {
        getAssembler().addIdTerm(getGradeIdColumn(), gradeId, match);
        return;
    }


    /**
     *  Clears all grade <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradeIdTerms() {
        getAssembler().clearTerms(getGradeIdColumn());
        return;
    }


    /**
     *  Gets the grade <code> Id </code> query terms. 
     *
     *  @return the grade <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradeIdTerms() {
        return (getAssembler().getIdTerms(getGradeIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the grade. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByGrade(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getGradeColumn(), style);
        return;
    }


    /**
     *  Gets the GradeId column name.
     *
     * @return the column name
     */

    protected String getGradeIdColumn() {
        return ("grade_id");
    }


    /**
     *  Tests if a <code> GradeQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the grade query 
     *  @throws org.osid.UnimplementedException <code> supportsGradeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeQuery getGradeQuery() {
        throw new org.osid.UnimplementedException("supportsGradeQuery() is false");
    }


    /**
     *  Matches taken assessments that have any grade assigned. 
     *
     *  @param  match <code> true </code> to match assessments with any grade, 
     *          <code> false </code> to match assessments with no grade 
     */

    @OSID @Override
    public void matchAnyGrade(boolean match) {
        getAssembler().addIdWildcardTerm(getGradeColumn(), match);
        return;
    }


    /**
     *  Clears all grade terms. 
     */

    @OSID @Override
    public void clearGradeTerms() {
        getAssembler().clearTerms(getGradeColumn());
        return;
    }


    /**
     *  Gets the grade query terms. 
     *
     *  @return the grade terms 
     */

    @OSID @Override
    public org.osid.grading.GradeQueryInspector[] getGradeTerms() {
        return (new org.osid.grading.GradeQueryInspector[0]);
    }


    /**
     *  Tests if a grade search order is available. 
     *
     *  @return <code> true </code> if a grade search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSearchOrder() {
        return (false);
    }


    /**
     *  Gets a grade search order. 
     *
     *  @return a grade search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSearchOrder getGradeSearchOrder() {
        throw new org.osid.UnimplementedException("supportsGradeSearchOrder() is false");
    }


    /**
     *  Gets the Grade column name.
     *
     * @return the column name
     */

    protected String getGradeColumn() {
        return ("grade");
    }


    /**
     *  Sets the comment string for this query. 
     *
     *  @param  comments comment string 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.InvalidArgumentException <code> comments is </code> 
     *          not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> comments </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchFeedback(String comments, 
                              org.osid.type.Type stringMatchType, 
                              boolean match) {
        getAssembler().addStringTerm(getFeedbackColumn(), comments, stringMatchType, match);
        return;
    }


    /**
     *  Matches taken assessments that have any comments. 
     *
     *  @param  match <code> true </code> to match assessments with any 
     *          comments, <code> false </code> to match assessments with no 
     *          comments 
     */

    @OSID @Override
    public void matchAnyFeedback(boolean match) {
        getAssembler().addStringWildcardTerm(getFeedbackColumn(), match);
        return;
    }


    /**
     *  Clears all comment terms. 
     */

    @OSID @Override
    public void clearFeedbackTerms() {
        getAssembler().clearTerms(getFeedbackColumn());
        return;
    }


    /**
     *  Gets the comment query terms. 
     *
     *  @return the comment terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getFeedbackTerms() {
        return (getAssembler().getStringTerms(getFeedbackColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the comments. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByFeedback(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getFeedbackColumn(), style);
        return;
    }


    /**
     *  Gets the Feedback column name.
     *
     * @return the column name
     */

    protected String getFeedbackColumn() {
        return ("feedback");
    }


    /**
     *  Sets the rubric assessment taken <code> Id </code> for this query. 
     *
     *  @param  assessmentTakenId an assessment taken <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentTakenId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchRubricId(org.osid.id.Id assessmentTakenId, boolean match) {
        getAssembler().addIdTerm(getRubricIdColumn(), assessmentTakenId, match);
        return;
    }


    /**
     *  Clears all rubric assessment taken <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRubricIdTerms() {
        getAssembler().clearTerms(getRubricIdColumn());
        return;
    }


    /**
     *  Gets the assessment taken <code> Id </code> query terms. 
     *
     *  @return the assessment taken <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRubricIdTerms() {
        return (getAssembler().getIdTerms(getRubricIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the rubric 
     *  assessment. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRubric(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRubricColumn(), style);
        return;
    }


    /**
     *  Gets the RubricId column name.
     *
     * @return the column name
     */

    protected String getRubricIdColumn() {
        return ("rubric_id");
    }


    /**
     *  Tests if an <code> AssessmentTakenQuery </code> is available. 
     *
     *  @return <code> true </code> if a rubric assessment taken query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRubricQuery() {
        return (false);
    }


    /**
     *  Gets the query for a rubric assessment. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the assessment taken query 
     *  @throws org.osid.UnimplementedException <code> supportsRubricQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenQuery getRubricQuery() {
        throw new org.osid.UnimplementedException("supportsRubricQuery() is false");
    }


    /**
     *  Matches an assessment taken that has any rubric assessment assigned. 
     *
     *  @param  match <code> true </code> to match assessments taken with any 
     *          rubric, <code> false </code> to match assessments taken with 
     *          no rubric 
     */

    @OSID @Override
    public void matchAnyRubric(boolean match) {
        getAssembler().addIdWildcardTerm(getRubricColumn(), match);
        return;
    }


    /**
     *  Clears all rubric assessment taken terms. 
     */

    @OSID @Override
    public void clearRubricTerms() {
        getAssembler().clearTerms(getRubricColumn());
        return;
    }


    /**
     *  Gets the assessment taken query terms. 
     *
     *  @return the assessment taken terms 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenQueryInspector[] getRubricTerms() {
        return (new org.osid.assessment.AssessmentTakenQueryInspector[0]);
    }


    /**
     *  Tests if an assessment taken search order is available. 
     *
     *  @return <code> true </code> if an assessment taken search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRubricSearchOrder() {
        return (false);
    }


    /**
     *  Gets an assessment taken search order. 
     *
     *  @return a rubric assessment taken search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRubricSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenSearchOrder getRubricSearchOrder() {
        throw new org.osid.UnimplementedException("supportsRubricSearchOrder() is false");
    }


    /**
     *  Gets the Rubric column name.
     *
     * @return the column name
     */

    protected String getRubricColumn() {
        return ("rubric");
    }


    /**
     *  Sets the bank <code> Id </code> for this query. 
     *
     *  @param  bankId a bank <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchBankId(org.osid.id.Id bankId, boolean match) {
        getAssembler().addIdTerm(getBankIdColumn(), bankId, match);
        return;
    }


    /**
     *  Clears all bank <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBankIdTerms() {
        getAssembler().clearTerms(getBankIdColumn());
        return;
    }


    /**
     *  Gets the bank <code> Id </code> query terms. 
     *
     *  @return the bank <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBankIdTerms() {
        return (getAssembler().getIdTerms(getBankIdColumn()));
    }


    /**
     *  Gets the BankId column name.
     *
     * @return the column name
     */

    protected String getBankIdColumn() {
        return ("bank_id");
    }


    /**
     *  Tests if a <code> BankQuery </code> is available. 
     *
     *  @return <code> true </code> if a bank query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBankQuery() {
        return (false);
    }


    /**
     *  Gets the query for a bank. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the bank query 
     *  @throws org.osid.UnimplementedException <code> supportsBankQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.BankQuery getBankQuery() {
        throw new org.osid.UnimplementedException("supportsBankQuery() is false");
    }


    /**
     *  Clears all bank terms. 
     */

    @OSID @Override
    public void clearBankTerms() {
        getAssembler().clearTerms(getBankColumn());
        return;
    }


    /**
     *  Gets the bank query terms. 
     *
     *  @return the bank terms 
     */

    @OSID @Override
    public org.osid.assessment.BankQueryInspector[] getBankTerms() {
        return (new org.osid.assessment.BankQueryInspector[0]);
    }


    /**
     *  Gets the Bank column name.
     *
     * @return the column name
     */

    protected String getBankColumn() {
        return ("bank");
    }


    /**
     *  Tests if this assessmentTaken supports the given record
     *  <code>Type</code>.
     *
     *  @param  assessmentTakenRecordType an assessment taken record type 
     *  @return <code>true</code> if the assessmentTakenRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentTakenRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type assessmentTakenRecordType) {
        for (org.osid.assessment.records.AssessmentTakenQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(assessmentTakenRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  assessmentTakenRecordType the assessment taken record type 
     *  @return the assessment taken query record 
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentTakenRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assessmentTakenRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.AssessmentTakenQueryRecord getAssessmentTakenQueryRecord(org.osid.type.Type assessmentTakenRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.records.AssessmentTakenQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(assessmentTakenRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assessmentTakenRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  assessmentTakenRecordType the assessment taken record type 
     *  @return the assessment taken query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentTakenRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assessmentTakenRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.AssessmentTakenQueryInspectorRecord getAssessmentTakenQueryInspectorRecord(org.osid.type.Type assessmentTakenRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.records.AssessmentTakenQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(assessmentTakenRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assessmentTakenRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param assessmentTakenRecordType the assessment taken record type
     *  @return the assessment taken search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentTakenRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assessmentTakenRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.AssessmentTakenSearchOrderRecord getAssessmentTakenSearchOrderRecord(org.osid.type.Type assessmentTakenRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.records.AssessmentTakenSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(assessmentTakenRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assessmentTakenRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this assessment taken. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param assessmentTakenQueryRecord the assessment taken query record
     *  @param assessmentTakenQueryInspectorRecord the assessment taken query inspector
     *         record
     *  @param assessmentTakenSearchOrderRecord the assessment taken search order record
     *  @param assessmentTakenRecordType assessment taken record type
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentTakenQueryRecord</code>,
     *          <code>assessmentTakenQueryInspectorRecord</code>,
     *          <code>assessmentTakenSearchOrderRecord</code> or
     *          <code>assessmentTakenRecordTypeassessmentTaken</code> is
     *          <code>null</code>
     */
            
    protected void addAssessmentTakenRecords(org.osid.assessment.records.AssessmentTakenQueryRecord assessmentTakenQueryRecord, 
                                      org.osid.assessment.records.AssessmentTakenQueryInspectorRecord assessmentTakenQueryInspectorRecord, 
                                      org.osid.assessment.records.AssessmentTakenSearchOrderRecord assessmentTakenSearchOrderRecord, 
                                      org.osid.type.Type assessmentTakenRecordType) {

        addRecordType(assessmentTakenRecordType);

        nullarg(assessmentTakenQueryRecord, "assessment taken query record");
        nullarg(assessmentTakenQueryInspectorRecord, "assessment taken query inspector record");
        nullarg(assessmentTakenSearchOrderRecord, "assessment taken search odrer record");

        this.queryRecords.add(assessmentTakenQueryRecord);
        this.queryInspectorRecords.add(assessmentTakenQueryInspectorRecord);
        this.searchOrderRecords.add(assessmentTakenSearchOrderRecord);
        
        return;
    }
}
