//
// AbstractAgendaSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.rules.check.agenda.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractAgendaSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.rules.check.AgendaSearchResults {

    private org.osid.rules.check.AgendaList agendas;
    private final org.osid.rules.check.AgendaQueryInspector inspector;
    private final java.util.Collection<org.osid.rules.check.records.AgendaSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractAgendaSearchResults.
     *
     *  @param agendas the result set
     *  @param agendaQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>agendas</code>
     *          or <code>agendaQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractAgendaSearchResults(org.osid.rules.check.AgendaList agendas,
                                            org.osid.rules.check.AgendaQueryInspector agendaQueryInspector) {
        nullarg(agendas, "agendas");
        nullarg(agendaQueryInspector, "agenda query inspectpr");

        this.agendas = agendas;
        this.inspector = agendaQueryInspector;

        return;
    }


    /**
     *  Gets the agenda list resulting from a search.
     *
     *  @return an agenda list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.rules.check.AgendaList getAgendas() {
        if (this.agendas == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.rules.check.AgendaList agendas = this.agendas;
        this.agendas = null;
	return (agendas);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.rules.check.AgendaQueryInspector getAgendaQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  agenda search record <code> Type. </code> This method must
     *  be used to retrieve an agenda implementing the requested
     *  record.
     *
     *  @param agendaSearchRecordType an agenda search 
     *         record type 
     *  @return the agenda search
     *  @throws org.osid.NullArgumentException
     *          <code>agendaSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(agendaSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.rules.check.records.AgendaSearchResultsRecord getAgendaSearchResultsRecord(org.osid.type.Type agendaSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.rules.check.records.AgendaSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(agendaSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(agendaSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record agenda search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addAgendaRecord(org.osid.rules.check.records.AgendaSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "agenda record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
