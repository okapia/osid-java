//
// MutableStepBatchFormList.java
//
//     Implements a StepBatchFormList. This list allows StepBatchForms to be
//     added after this list has been created.
//
//
// Tom Coppeto
// OnTapSolutions
// 29 June 2008
//
//
// Copyright (c) 2008, 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.batch.stepbatchform;


/**
 *  <p>Implements a StepBatchFormList. This list allows StepBatchForms to be
 *  added after this stepBatchForm has been created. One this list has been
 *  returned to the consumer, all subsequent additions occur in a
 *  separate processing thread.  The creator of this stepBatchForm must
 *  invoke <code>eol()</code> when there are no more stepBatchForms to be
 *  added.</p>
 *
 *  <p> If the consumer of the <code>StepBatchFormList</code> interface
 *  reaches the end of the internal buffer before <code>eol()</code>,
 *  then methods will block until more stepBatchForms are added or
 *  <code>eol()</code> is invoked.</p>
 *
 *  <p><code>available()</code> never blocks but may return
 *  <code>0</code> if waiting for more stepBatchForms to be added.</p>
 */

public final class MutableStepBatchFormList
    extends net.okapia.osid.jamocha.workflow.batch.stepbatchform.spi.AbstractMutableStepBatchFormList
    implements org.osid.workflow.batch.StepBatchFormList {


    /**
     *  Creates a new empty <code>MutableStepBatchFormList</code>.
     */

    public MutableStepBatchFormList() {
        super();
    }


    /**
     *  Creates a new <code>MutableStepBatchFormList</code>.
     *
     *  @param stepBatchForm a <code>StepBatchForm</code>
     *  @throws org.osid.NullArgumentException <code>stepBatchForm</code>
     *          is <code>null</code>
     */

    public MutableStepBatchFormList(org.osid.workflow.batch.StepBatchForm stepBatchForm) {
        super(stepBatchForm);
        return;
    }


    /**
     *  Creates a new <code>MutableStepBatchFormList</code>.
     *
     *  @param array an array of stepbatchforms
     *  @throws org.osid.NullArgumentException <code>array</code>
     *          is <code>null</code>
     */

    public MutableStepBatchFormList(org.osid.workflow.batch.StepBatchForm[] array) {
        super(array);
        return;
    }


    /**
     *  Creates a new <code>MutableStepBatchFormList</code>.
     *
     *  @param collection a java.util.Collection of stepbatchforms
     *  @throws org.osid.NullArgumentException <code>collection</code>
     *          is <code>null</code>
     */

    public MutableStepBatchFormList(java.util.Collection<org.osid.workflow.batch.StepBatchForm> collection) {
        super(collection);
        return;
    }
}
