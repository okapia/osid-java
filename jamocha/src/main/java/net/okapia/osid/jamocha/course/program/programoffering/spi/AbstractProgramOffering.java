//
// AbstractProgramOffering.java
//
//     Defines a ProgramOffering.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.program.programoffering.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>ProgramOffering</code>.
 */

public abstract class AbstractProgramOffering
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.course.program.ProgramOffering {

    private org.osid.course.program.Program program;
    private org.osid.course.Term term;
    private org.osid.locale.DisplayText title;
    private String number;

    private org.osid.locale.DisplayText completionRequirementsInfo;
    private long minimumSeats;
    private long maximumSeats;
    private String url;

    private boolean hasSponsors               = false;
    private boolean hasCompletionRequirements = false;
    private boolean earnsCredentials          = false;
    private boolean requiresRegistration      = false;
    
    private final java.util.Collection<org.osid.resource.Resource> sponsors = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.requisite.Requisite> completionRequirements = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.program.Credential> credentials = new java.util.LinkedHashSet<>();

    private final java.util.Collection<org.osid.course.program.records.ProgramOfferingRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the canonical program <code> Id </code> associated with
     *  this program offering.
     *
     *  @return the program <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getProgramId() {
        return (this.program.getId());
    }


    /**
     *  Gets the canonical program associated with this program offering. 
     *
     *  @return the program 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.program.Program getProgram()
        throws org.osid.OperationFailedException {

        return (this.program);
    }


    /**
     *  Sets the program.
     *
     *  @param program a program
     *  @throws org.osid.NullArgumentException
     *          <code>program</code> is <code>null</code>
     */

    protected void setProgram(org.osid.course.program.Program program) {
        nullarg(program, "program");
        this.program = program;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> Term </code> of this 
     *  offering. 
     *
     *  @return the <code> Term </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getTermId() {
        return (this.term.getId());
    }


    /**
     *  Gets the <code> Term </code> of this offering. 
     *
     *  @return the term 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.Term getTerm()
        throws org.osid.OperationFailedException {

        return (this.term);
    }


    /**
     *  Sets the term.
     *
     *  @param term a term
     *  @throws org.osid.NullArgumentException
     *          <code>term</code> is <code>null</code>
     */

    protected void setTerm(org.osid.course.Term term) {
        nullarg(term, "term");
        this.term = term;
        return;
    }


    /**
     *  Gets the formal title of this program. It may be the same as
     *  the display name or it may be used to more formally label the
     *  course.
     *
     *  @return the program offering title 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getTitle() {
        return (this.title);
    }


    /**
     *  Sets the title.
     *
     *  @param title a title
     *  @throws org.osid.NullArgumentException
     *          <code>title</code> is <code>null</code>
     */

    protected void setTitle(org.osid.locale.DisplayText title) {
        nullarg(title, "title");
        this.title = title;
        return;
    }


    /**
     *  Gets the program offering number which is a label generally
     *  used to index the program offering in a catalog.
     *
     *  @return the program offering number 
     */

    @OSID @Override
    public String getNumber() {
        return (this.number);
    }


    /**
     *  Sets the number.
     *
     *  @param number a number
     *  @throws org.osid.NullArgumentException <code>number</code> is
     *          <code>null</code>
     */

    protected void setNumber(String number) {
        nullarg(number, "program number");
        this.number = number;
        return;
    }


    /**
     *  Tests if this program offering has a sponsor. 
     *
     *  @return <code> true </code> if this program offering has
     *          sponsors, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean hasSponsors() {
        return (this.hasSponsors);
    }


    /**
     *  Gets the sponsor <code> Ids. </code> 
     *
     *  @return the sponsor <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> hasSponsors()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.id.IdList getSponsorIds() {
        if (!hasSponsors()) {
            throw new org.osid.IllegalStateException("hasSponsors() is false");
        }

        try {
            org.osid.resource.ResourceList sponsors = getSponsors();
            return (new net.okapia.osid.jamocha.adapter.converter.resource.resource.ResourceToIdList(sponsors));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the sponsors. 
     *
     *  @return the sponsors 
     *  @throws org.osid.IllegalStateException <code> hasSponsors() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getSponsors()
        throws org.osid.OperationFailedException {

        if (!hasSponsors()) {
            throw new org.osid.IllegalStateException("hasSponsors() is false");
        }

        return (new net.okapia.osid.jamocha.resource.resource.ArrayResourceList(this.sponsors));
    }


    /**
     *  Adds a sponsor.
     *
     *  @param sponsor a sponsor
     *  @throws org.osid.NullArgumentException <code>sponsor</code> is
     *          <code>null</code>
     */

    protected void addSponsor(org.osid.resource.Resource sponsor) {
        nullarg(sponsor, "sponsor");

        this.sponsors.add(sponsor);
        this.hasSponsors = true;

        return;
    }


    /**
     *  Sets all the sponsors.
     *
     *  @param sponsors a collection of sponsors
     *  @throws org.osid.NullArgumentException <code>sponsors</code>
     *          is <code>null</code>
     */

    protected void setSponsors(java.util.Collection<org.osid.resource.Resource> sponsors) {
        nullarg(sponsors, "sponsors");

        this.sponsors.clear();
        this.sponsors.addAll(sponsors);
        this.hasSponsors = true;

        return;
    }


    /**
     *  Gets the an informational string for the program completion.
     *
     *  @return the program completion 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getCompletionRequirementsInfo() {
        return (this.completionRequirementsInfo);
    }


    /**
     *  Sets the completion requirements info.
     *
     *  @param info a completion requirements info
     *  @throws org.osid.NullArgumentException
     *          <code>info</code> is <code>null</code>
     */

    protected void setCompletionRequirementsInfo(org.osid.locale.DisplayText info) {
        nullarg(info, "completion requirements info");
        this.completionRequirementsInfo = info;
        return;
    }


    /**
     *  Tests if this program has a rule for the program completion. 
     *
     *  @return <code> true </code> if this program has a completion rule, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasCompletionRequirements() {
        return (this.hasCompletionRequirements);
    }


    /**
     *  Gets the <code> Requisite </code> <code> Ids </code> for the program 
     *  completion. 
     *
     *  @return the completion requisite <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> 
     *          hasCompletionRequirements() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getCompletionRequirementIds() {
        if (!hasCompletionRequirements()) {
            throw new org.osid.IllegalStateException("hasCompletionRequirements() is false");
        }

        try {
            org.osid.course.requisite.RequisiteList completionRequirements = getCompletionRequirements();
            return (new net.okapia.osid.jamocha.adapter.converter.course.requisite.requisite.RequisiteToIdList(completionRequirements));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the requisites for the program completion. Each <code> Requisite 
     *  </code> is an <code> AND </code> term and must be true for the 
     *  requirements to be satisifed. 
     *
     *  @return the completion requisites 
     *  @throws org.osid.IllegalStateException <code> 
     *          hasCompletionRequirements() </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getCompletionRequirements()
        throws org.osid.OperationFailedException {

        if (!hasCompletionRequirements()) {
            throw new org.osid.IllegalStateException("hasCompletionRequirements() is false");
        }

        return (new net.okapia.osid.jamocha.course.requisite.requisite.ArrayRequisiteList(this.completionRequirements));
    }


    /**
     *  Adds a completion requirement.
     *
     *  @param requirement a completion requirement
     *  @throws org.osid.NullArgumentException
     *          <code>requirement</code> is <code>null</code>
     */

    protected void addCompletionRequirement(org.osid.course.requisite.Requisite requirement) {
        nullarg(requirement, "completion requirement");

        this.completionRequirements.add(requirement);
        this.hasCompletionRequirements = true;

        return;
    }


    /**
     *  Sets all the completion requirements.
     *
     *  @param requirements a collection of completion requirements
     *  @throws org.osid.NullArgumentException
     *          <code>requirements</code> is <code>null</code>
     */

    protected void setCompletionRequirements(java.util.Collection<org.osid.course.requisite.Requisite> requirements) {
        nullarg(requirements, "completion requirements");

        this.completionRequirements.clear();
        this.completionRequirements.addAll(requirements);
        this.hasCompletionRequirements = true;

        return;
    }


    /**
     *  Tests if completion of this program results in credentials awarded. 
     *
     *  @return <code> true </code> if this program earns credentials, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean earnsCredentials() {
        return (this.earnsCredentials);
    }


    /**
     *  Gets the awarded credential <code> Ids. </code> 
     *
     *  @return the returned list of credential <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> earnsCredentials() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getCredentialIds() {
        if (!earnsCredentials()) {
            throw new org.osid.IllegalStateException("earnsCredentials() is false");
        }

        try {
            org.osid.course.program.CredentialList credentials = getCredentials();
            return (new net.okapia.osid.jamocha.adapter.converter.course.program.credential.CredentialToIdList(credentials));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the awarded credentials. 
     *
     *  @return the returned list of credentials 
     *  @throws org.osid.IllegalStateException <code> earnsCredentials() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.program.CredentialList getCredentials()
        throws org.osid.OperationFailedException {

        if (!earnsCredentials()) {
            throw new org.osid.IllegalStateException("earnsCredentials() is false");
        }

        return (new net.okapia.osid.jamocha.course.program.credential.ArrayCredentialList(this.credentials));
    }


    /**
     *  Adds a credential.
     *
     *  @param credential a credential
     *  @throws org.osid.NullArgumentException <code>credential</code>
     *          is <code>null</code>
     */

    protected void addCredential(org.osid.course.program.Credential credential) {
        nullarg(credential, "credential");

        this.credentials.add(credential);
        this.earnsCredentials = true;

        return;
    }


    /**
     *  Sets all the credentials.
     *
     *  @param credentials a collection of credentials
     *  @throws org.osid.NullArgumentException
     *          <code>credentials</code> is <code>null</code>
     */

    protected void setCredentials(java.util.Collection<org.osid.course.program.Credential> credentials) {
        nullarg(credentials, "credentials");

        this.credentials.clear();
        this.credentials.addAll(credentials);
        this.earnsCredentials = true;

        return;
    }


    /**
     *  Tests if this program offering requires advanced registration. 
     *
     *  @return <code> true </code> if this program offering requires
     *          advance registration, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean requiresRegistration() {
        return (this.requiresRegistration);
    }


    /**
     *  Sets the requires registration flag.
     *
     *  @param requires <code> true </code> if this program offering
     *          requires advance registration, <code> false </code>
     *          otherwise
     */

    protected void setRequiresRegistration(boolean requires) {
        this.requiresRegistration = requires;
        return;
    }


    /**
     *  Gets the minimum number of students this offering can have.
     *
     *  @return the minimum seats 
     *  @throws org.osid.IllegalStateException <code> requiresRegistration() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public long getMinimumSeats() {
        if (!requiresRegistration()) {
            throw new org.osid.IllegalStateException("requiresRegistration() is false");
        }

        return (this.minimumSeats);
    }


    /**
     *  Sets the minimum seats.
     *
     *  @param seats the minimum seats
     *  @throws org.osid.InvalidArgumentException <code>seats</code>
     *          is negative
     */

    protected void setMinimumSeats(long seats) {
        cardinalarg(seats, "minimum seats");
        this.minimumSeats = seats;
        return;
    }


    /**
     *  Gets the maximum number of students this offering can have. 
     *
     *  @return the maximum seats 
     *  @throws org.osid.IllegalStateException <code> requiresRegistration() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public long getMaximumSeats() {
        if (!requiresRegistration()) {
            throw new org.osid.IllegalStateException("requiresRegistration() is false");
        }

        return (this.maximumSeats);
    }


    /**
     *  Sets the maximum seats.
     *
     *  @param seats the maximum seats
     *  @throws org.osid.NullArgumentException <code>seats</code> is
     *          negative
     */

    protected void setMaximumSeats(long seats) {
        cardinalarg(seats, "maximum seats");
        this.maximumSeats = seats;
        return;
    }


    /**
     *  Gets an external resource, such as a class web site, associated with 
     *  this offering. 
     *
     *  @return a URL string 
     */

    @OSID @Override
    public String getURL() {
        return (this.url);
    }


    /**
     *  Sets the url.
     *
     *  @param url a url
     *  @throws org.osid.NullArgumentException <code>url</code> is
     *          <code>null</code>
     */

    protected void setURL(String url) {
        this.url = url;
        return;
    }


    /**
     *  Tests if this programOffering supports the given record
     *  <code>Type</code>.
     *
     *  @param  programOfferingRecordType a program offering record type 
     *  @return <code>true</code> if the programOfferingRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>programOfferingRecordType</code> is
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type programOfferingRecordType) {
        for (org.osid.course.program.records.ProgramOfferingRecord record : this.records) {
            if (record.implementsRecordType(programOfferingRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>ProgramOffering</code> record <code>Type</code>.
     *
     *  @param  programOfferingRecordType the program offering record type 
     *  @return the program offering record 
     *  @throws org.osid.NullArgumentException
     *          <code>programOfferingRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(programOfferingRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.program.records.ProgramOfferingRecord getProgramOfferingRecord(org.osid.type.Type programOfferingRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.program.records.ProgramOfferingRecord record : this.records) {
            if (record.implementsRecordType(programOfferingRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(programOfferingRecordType + " is not supported");
    }


    /**
     *  Adds a record to this program offering. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param programOfferingRecord the program offering record
     *  @param programOfferingRecordType program offering record type
     *  @throws org.osid.NullArgumentException
     *          <code>programOfferingRecord</code> or
     *          <code>programOfferingRecordTypeprogramOffering</code> is
     *          <code>null</code>
     */
            
    protected void addProgramOfferingRecord(org.osid.course.program.records.ProgramOfferingRecord programOfferingRecord, 
                                            org.osid.type.Type programOfferingRecordType) {
        
        nullarg(programOfferingRecord, "program offering record");
        addRecordType(programOfferingRecordType);
        this.records.add(programOfferingRecord);
        
        return;
    }
}
