//
// MutableIndexedMapEdgeLookupSession
//
//    Implements an Edge lookup service backed by a collection of
//    edges indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.topology;


/**
 *  Implements an Edge lookup service backed by a collection of
 *  edges. The edges are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some edges may be compatible
 *  with more types than are indicated through these edge
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of edges can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapEdgeLookupSession
    extends net.okapia.osid.jamocha.core.topology.spi.AbstractIndexedMapEdgeLookupSession
    implements org.osid.topology.EdgeLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapEdgeLookupSession} with no edges.
     *
     *  @param graph the graph
     *  @throws org.osid.NullArgumentException {@code graph}
     *          is {@code null}
     */

      public MutableIndexedMapEdgeLookupSession(org.osid.topology.Graph graph) {
        setGraph(graph);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapEdgeLookupSession} with a
     *  single edge.
     *  
     *  @param graph the graph
     *  @param  edge an single edge
     *  @throws org.osid.NullArgumentException {@code graph} or
     *          {@code edge} is {@code null}
     */

    public MutableIndexedMapEdgeLookupSession(org.osid.topology.Graph graph,
                                                  org.osid.topology.Edge edge) {
        this(graph);
        putEdge(edge);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapEdgeLookupSession} using an
     *  array of edges.
     *
     *  @param graph the graph
     *  @param  edges an array of edges
     *  @throws org.osid.NullArgumentException {@code graph} or
     *          {@code edges} is {@code null}
     */

    public MutableIndexedMapEdgeLookupSession(org.osid.topology.Graph graph,
                                                  org.osid.topology.Edge[] edges) {
        this(graph);
        putEdges(edges);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapEdgeLookupSession} using a
     *  collection of edges.
     *
     *  @param graph the graph
     *  @param  edges a collection of edges
     *  @throws org.osid.NullArgumentException {@code graph} or
     *          {@code edges} is {@code null}
     */

    public MutableIndexedMapEdgeLookupSession(org.osid.topology.Graph graph,
                                                  java.util.Collection<? extends org.osid.topology.Edge> edges) {

        this(graph);
        putEdges(edges);
        return;
    }
    

    /**
     *  Makes an {@code Edge} available in this session.
     *
     *  @param  edge an edge
     *  @throws org.osid.NullArgumentException {@code edge{@code  is
     *          {@code null}
     */

    @Override
    public void putEdge(org.osid.topology.Edge edge) {
        super.putEdge(edge);
        return;
    }


    /**
     *  Makes an array of edges available in this session.
     *
     *  @param  edges an array of edges
     *  @throws org.osid.NullArgumentException {@code edges{@code 
     *          is {@code null}
     */

    @Override
    public void putEdges(org.osid.topology.Edge[] edges) {
        super.putEdges(edges);
        return;
    }


    /**
     *  Makes collection of edges available in this session.
     *
     *  @param  edges a collection of edges
     *  @throws org.osid.NullArgumentException {@code edge{@code  is
     *          {@code null}
     */

    @Override
    public void putEdges(java.util.Collection<? extends org.osid.topology.Edge> edges) {
        super.putEdges(edges);
        return;
    }


    /**
     *  Removes an Edge from this session.
     *
     *  @param edgeId the {@code Id} of the edge
     *  @throws org.osid.NullArgumentException {@code edgeId{@code  is
     *          {@code null}
     */

    @Override
    public void removeEdge(org.osid.id.Id edgeId) {
        super.removeEdge(edgeId);
        return;
    }    
}
