//
// AbstractBrokerConstrainerEnablerLookupSession.java
//
//    A starter implementation framework for providing a BrokerConstrainerEnabler
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a BrokerConstrainerEnabler
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getBrokerConstrainerEnablers(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractBrokerConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.provisioning.rules.BrokerConstrainerEnablerLookupSession {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean federated     = false;
    private org.osid.provisioning.Distributor distributor = new net.okapia.osid.jamocha.nil.provisioning.distributor.UnknownDistributor();
    

    /**
     *  Gets the <code>Distributor/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Distributor Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.distributor.getId());
    }


    /**
     *  Gets the <code>Distributor</code> associated with this 
     *  session.
     *
     *  @return the <code>Distributor</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.distributor);
    }


    /**
     *  Sets the <code>Distributor</code>.
     *
     *  @param  distributor the distributor for this session
     *  @throws org.osid.NullArgumentException <code>distributor</code>
     *          is <code>null</code>
     */

    protected void setDistributor(org.osid.provisioning.Distributor distributor) {
        nullarg(distributor, "distributor");
        this.distributor = distributor;
        return;
    }


    /**
     *  Tests if this user can perform <code>BrokerConstrainerEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupBrokerConstrainerEnablers() {
        return (true);
    }


    /**
     *  A complete view of the <code>BrokerConstrainerEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeBrokerConstrainerEnablerView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>BrokerConstrainerEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryBrokerConstrainerEnablerView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include broker constrainer enablers in distributors which are children
     *  of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active broker constrainer enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveBrokerConstrainerEnablerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive broker constrainer enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusBrokerConstrainerEnablerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>BrokerConstrainerEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>BrokerConstrainerEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>BrokerConstrainerEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, broker constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive broker constrainer enablers
     *  are returned.
     *
     *  @param  brokerConstrainerEnablerId <code>Id</code> of the
     *          <code>BrokerConstrainerEnabler</code>
     *  @return the broker constrainer enabler
     *  @throws org.osid.NotFoundException <code>brokerConstrainerEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>brokerConstrainerEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerEnabler getBrokerConstrainerEnabler(org.osid.id.Id brokerConstrainerEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.provisioning.rules.BrokerConstrainerEnablerList brokerConstrainerEnablers = getBrokerConstrainerEnablers()) {
            while (brokerConstrainerEnablers.hasNext()) {
                org.osid.provisioning.rules.BrokerConstrainerEnabler brokerConstrainerEnabler = brokerConstrainerEnablers.getNextBrokerConstrainerEnabler();
                if (brokerConstrainerEnabler.getId().equals(brokerConstrainerEnablerId)) {
                    return (brokerConstrainerEnabler);
                }
            }
        } 

        throw new org.osid.NotFoundException(brokerConstrainerEnablerId + " not found");
    }


    /**
     *  Gets a <code>BrokerConstrainerEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  brokerConstrainerEnablers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>BrokerConstrainerEnablers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, broker constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive broker constrainer enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getBrokerConstrainerEnablers()</code>.
     *
     *  @param  brokerConstrainerEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>BrokerConstrainerEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
*               found
     *  @throws org.osid.NullArgumentException
     *          <code>brokerConstrainerEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerEnablerList getBrokerConstrainerEnablersByIds(org.osid.id.IdList brokerConstrainerEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.provisioning.rules.BrokerConstrainerEnabler> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = brokerConstrainerEnablerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getBrokerConstrainerEnabler(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("broker constrainer enabler " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.provisioning.rules.brokerconstrainerenabler.LinkedBrokerConstrainerEnablerList(ret));
    }


    /**
     *  Gets a <code>BrokerConstrainerEnablerList</code> corresponding to the given
     *  broker constrainer enabler genus <code>Type</code> which does not include
     *  broker constrainer enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  broker constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those broker constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, broker constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive broker constrainer enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getBrokerConstrainerEnablers()</code>.
     *
     *  @param  brokerConstrainerEnablerGenusType a brokerConstrainerEnabler genus type 
     *  @return the returned <code>BrokerConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>brokerConstrainerEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerEnablerList getBrokerConstrainerEnablersByGenusType(org.osid.type.Type brokerConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.rules.brokerconstrainerenabler.BrokerConstrainerEnablerGenusFilterList(getBrokerConstrainerEnablers(), brokerConstrainerEnablerGenusType));
    }


    /**
     *  Gets a <code>BrokerConstrainerEnablerList</code> corresponding to the given
     *  broker constrainer enabler genus <code>Type</code> and include any additional
     *  broker constrainer enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  broker constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those broker constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, broker constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive broker constrainer enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getBrokerConstrainerEnablers()</code>.
     *
     *  @param  brokerConstrainerEnablerGenusType a brokerConstrainerEnabler genus type 
     *  @return the returned <code>BrokerConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>brokerConstrainerEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerEnablerList getBrokerConstrainerEnablersByParentGenusType(org.osid.type.Type brokerConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getBrokerConstrainerEnablersByGenusType(brokerConstrainerEnablerGenusType));
    }


    /**
     *  Gets a <code>BrokerConstrainerEnablerList</code> containing the given
     *  broker constrainer enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  broker constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those broker constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *

     *  In active mode, broker constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive broker constrainer enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getBrokerConstrainerEnablers()</code>.
     *
     *  @param  brokerConstrainerEnablerRecordType a brokerConstrainerEnabler record type 
     *  @return the returned <code>BrokerConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>brokerConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerEnablerList getBrokerConstrainerEnablersByRecordType(org.osid.type.Type brokerConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.rules.brokerconstrainerenabler.BrokerConstrainerEnablerRecordFilterList(getBrokerConstrainerEnablers(), brokerConstrainerEnablerRecordType));
    }


    /**
     *  Gets a <code>BrokerConstrainerEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  broker constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those broker constrainer enablers that are accessible
     *  through this session.
     *  
     *  In active mode, broker constrainer enablers are returned that are currently
     *  active in addition to being effective during the given date
     *  range. In any status mode, active and inactive broker constrainer enablers are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>BrokerConstrainerEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerEnablerList getBrokerConstrainerEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.rules.brokerconstrainerenabler.TemporalBrokerConstrainerEnablerFilterList(getBrokerConstrainerEnablers(), from, to));
    }
        

    /**
     *  Gets a <code>BrokerConstrainerEnablerList </code> which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  broker constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those broker constrainer enablers that are accessible
     *  through this session.
     *
     *  In active mode, broker constrainer enablers are returned that are currently
     *  active in addition to being effective during the date
     *  range. In any status mode, active and inactive broker constrainer enablers are
     *  returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>BrokerConstrainerEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerEnablerList getBrokerConstrainerEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (getBrokerConstrainerEnablersOnDate(from, to));
    }


    /**
     *  Gets all <code>BrokerConstrainerEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  broker constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those broker constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, broker constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive broker constrainer enablers
     *  are returned.
     *
     *  @return a list of <code>BrokerConstrainerEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.provisioning.rules.BrokerConstrainerEnablerList getBrokerConstrainerEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the broker constrainer enabler list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of broker constrainer enablers
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.provisioning.rules.BrokerConstrainerEnablerList filterBrokerConstrainerEnablersOnViews(org.osid.provisioning.rules.BrokerConstrainerEnablerList list)
        throws org.osid.OperationFailedException {

        org.osid.provisioning.rules.BrokerConstrainerEnablerList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.provisioning.rules.brokerconstrainerenabler.ActiveBrokerConstrainerEnablerFilterList(ret);
        }

        return (ret);
    }
}
