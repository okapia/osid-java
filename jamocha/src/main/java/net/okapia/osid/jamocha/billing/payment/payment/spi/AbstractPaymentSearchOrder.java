//
// AbstractPaymentSearchOdrer.java
//
//     Defines a PaymentSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.payment.payment.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code PaymentSearchOrder}.
 */

public abstract class AbstractPaymentSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectSearchOrder
    implements org.osid.billing.payment.PaymentSearchOrder {

    private final java.util.Collection<org.osid.billing.payment.records.PaymentSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by the payer. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPayer(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a payer search order is available. 
     *
     *  @return <code> true </code> if a payer search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPayerSearchOrder() {
        return (false);
    }


    /**
     *  Specifies a preference for ordering the result set by the payer. 
     *
     *  @return the payer search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPayerSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerSearchOrder getPayerSearchOrder() {
        throw new org.osid.UnimplementedException("supportsPayerSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the customer. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCustomer(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a customer search order is available. 
     *
     *  @return <code> true </code> if a customer search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCustomerSearchOrder() {
        return (false);
    }


    /**
     *  Specifies a preference for ordering the result set by the customer. 
     *
     *  @return the customer search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCustomerSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerSearchOrder getCustomerSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCustomerSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the billing 
     *  period. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPeriod(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a billing period search order is available. 
     *
     *  @return <code> true </code> if a period search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPeriodSearchOrder() {
        return (false);
    }


    /**
     *  Specifies a preference for ordering the result set by the billing 
     *  period. 
     *
     *  @return the period search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPeriodSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodSearchOrder getPeriodSearchOrder() {
        throw new org.osid.UnimplementedException("supportsPeriodSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the payment 
     *  date. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPaymentDate(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the process 
     *  date. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByProcessDate(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the amount. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAmount(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  paymentRecordType a payment record type 
     *  @return {@code true} if the paymentRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code paymentRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type paymentRecordType) {
        for (org.osid.billing.payment.records.PaymentSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(paymentRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  paymentRecordType the payment record type 
     *  @return the payment search order record
     *  @throws org.osid.NullArgumentException
     *          {@code paymentRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(paymentRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.billing.payment.records.PaymentSearchOrderRecord getPaymentSearchOrderRecord(org.osid.type.Type paymentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.payment.records.PaymentSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(paymentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(paymentRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this payment. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param paymentRecord the payment search odrer record
     *  @param paymentRecordType payment record type
     *  @throws org.osid.NullArgumentException
     *          {@code paymentRecord} or
     *          {@code paymentRecordTypepayment} is
     *          {@code null}
     */
            
    protected void addPaymentRecord(org.osid.billing.payment.records.PaymentSearchOrderRecord paymentSearchOrderRecord, 
                                     org.osid.type.Type paymentRecordType) {

        addRecordType(paymentRecordType);
        this.records.add(paymentSearchOrderRecord);
        
        return;
    }
}
