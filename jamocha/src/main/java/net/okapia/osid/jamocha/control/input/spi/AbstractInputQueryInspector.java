//
// AbstractInputQueryInspector.java
//
//     A template for making an InputQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.input.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for inputs.
 */

public abstract class AbstractInputQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleQueryInspector
    implements org.osid.control.InputQueryInspector {

    private final java.util.Collection<org.osid.control.records.InputQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the device <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDeviceIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the device query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.DeviceQueryInspector[] getDeviceTerms() {
        return (new org.osid.control.DeviceQueryInspector[0]);
    }


    /**
     *  Gets the controller <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getControllerIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the controller query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.ControllerQueryInspector[] getControllerTerms() {
        return (new org.osid.control.ControllerQueryInspector[0]);
    }


    /**
     *  Gets the system <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSystemIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the system query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.SystemQueryInspector[] getSystemTerms() {
        return (new org.osid.control.SystemQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given input query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an input implementing the requested record.
     *
     *  @param inputRecordType an input record type
     *  @return the input query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>inputRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(inputRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.InputQueryInspectorRecord getInputQueryInspectorRecord(org.osid.type.Type inputRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.InputQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(inputRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(inputRecordType + " is not supported");
    }


    /**
     *  Adds a record to this input query. 
     *
     *  @param inputQueryInspectorRecord input query inspector
     *         record
     *  @param inputRecordType input record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addInputQueryInspectorRecord(org.osid.control.records.InputQueryInspectorRecord inputQueryInspectorRecord, 
                                                   org.osid.type.Type inputRecordType) {

        addRecordType(inputRecordType);
        nullarg(inputRecordType, "input record type");
        this.records.add(inputQueryInspectorRecord);        
        return;
    }
}
