//
// AbstractProfileEntryQueryInspector.java
//
//     A template for making a ProfileEntryQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.profile.profileentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for profile entries.
 */

public abstract class AbstractProfileEntryQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQueryInspector
    implements org.osid.profile.ProfileEntryQueryInspector {

    private final java.util.Collection<org.osid.profile.records.ProfileEntryQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the implicit profile entries query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getImplicitTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the related profile entry <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRelatedProfileEntryIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the related profile entry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryQueryInspector[] getRelatedProfileEntryTerms() {
        return (new org.osid.profile.ProfileEntryQueryInspector[0]);
    }


    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResourceIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the agent <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAgentIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the agent query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Gets the profile item <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProfileItemIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the profile item query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemQueryInspector[] getProfileItemTerms() {
        return (new org.osid.profile.ProfileItemQueryInspector[0]);
    }


    /**
     *  Gets the profile <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProfileIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the profile query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.profile.ProfileQueryInspector[] getProfileTerms() {
        return (new org.osid.profile.ProfileQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given profile entry query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a profile entry implementing the requested record.
     *
     *  @param profileEntryRecordType a profile entry record type
     *  @return the profile entry query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(profileEntryRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.profile.records.ProfileEntryQueryInspectorRecord getProfileEntryQueryInspectorRecord(org.osid.type.Type profileEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.profile.records.ProfileEntryQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(profileEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(profileEntryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this profile entry query. 
     *
     *  @param profileEntryQueryInspectorRecord profile entry query inspector
     *         record
     *  @param profileEntryRecordType profileEntry record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addProfileEntryQueryInspectorRecord(org.osid.profile.records.ProfileEntryQueryInspectorRecord profileEntryQueryInspectorRecord, 
                                                   org.osid.type.Type profileEntryRecordType) {

        addRecordType(profileEntryRecordType);
        nullarg(profileEntryRecordType, "profile entry record type");
        this.records.add(profileEntryQueryInspectorRecord);        
        return;
    }
}
