//
// InvariantMapProxyAppointmentLookupSession
//
//    Implements an Appointment lookup service backed by a fixed
//    collection of appointments. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.personnel;


/**
 *  Implements an Appointment lookup service backed by a fixed
 *  collection of appointments. The appointments are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyAppointmentLookupSession
    extends net.okapia.osid.jamocha.core.personnel.spi.AbstractMapAppointmentLookupSession
    implements org.osid.personnel.AppointmentLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyAppointmentLookupSession} with no
     *  appointments.
     *
     *  @param realm the realm
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code realm} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyAppointmentLookupSession(org.osid.personnel.Realm realm,
                                                  org.osid.proxy.Proxy proxy) {
        setRealm(realm);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyAppointmentLookupSession} with a single
     *  appointment.
     *
     *  @param realm the realm
     *  @param appointment an single appointment
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code realm},
     *          {@code appointment} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyAppointmentLookupSession(org.osid.personnel.Realm realm,
                                                  org.osid.personnel.Appointment appointment, org.osid.proxy.Proxy proxy) {

        this(realm, proxy);
        putAppointment(appointment);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyAppointmentLookupSession} using
     *  an array of appointments.
     *
     *  @param realm the realm
     *  @param appointments an array of appointments
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code realm},
     *          {@code appointments} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyAppointmentLookupSession(org.osid.personnel.Realm realm,
                                                  org.osid.personnel.Appointment[] appointments, org.osid.proxy.Proxy proxy) {

        this(realm, proxy);
        putAppointments(appointments);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyAppointmentLookupSession} using a
     *  collection of appointments.
     *
     *  @param realm the realm
     *  @param appointments a collection of appointments
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code realm},
     *          {@code appointments} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyAppointmentLookupSession(org.osid.personnel.Realm realm,
                                                  java.util.Collection<? extends org.osid.personnel.Appointment> appointments,
                                                  org.osid.proxy.Proxy proxy) {

        this(realm, proxy);
        putAppointments(appointments);
        return;
    }
}
