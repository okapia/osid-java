//
// AbstractJournalQuery.java
//
//     A template for making a Journal Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.journaling.journal.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for journals.
 */

public abstract class AbstractJournalQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.journaling.JournalQuery {

    private final java.util.Collection<org.osid.journaling.records.JournalQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the journal entry <code> Id </code> for this query to match 
     *  entries assigned to journals. 
     *
     *  @param  journalEntryId a journal entry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> journalEntryId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchJournalEntryId(org.osid.id.Id journalEntryId, 
                                    boolean match) {
        return;
    }


    /**
     *  Clears the journal entry <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearJournalEntryIdTerms() {
        return;
    }


    /**
     *  Tests if a journal entry query is available. 
     *
     *  @return <code> true </code> if a journal entry query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJournalEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a journal. 
     *
     *  @return the journal entry query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryQuery getJournalEntryQuery() {
        throw new org.osid.UnimplementedException("supportsJournalEntryQuery() is false");
    }


    /**
     *  Matches journals with any journal entry. 
     *
     *  @param  match <code> true </code> to match journals with any journal 
     *          entry, <code> false </code> to match journals with no entries 
     */

    @OSID @Override
    public void matchAnyJournalEntry(boolean match) {
        return;
    }


    /**
     *  Clears the journal entry terms. 
     */

    @OSID @Override
    public void clearJournalEntryTerms() {
        return;
    }


    /**
     *  Sets the branch <code> Id </code> for this query to match branches 
     *  assigned to journals. 
     *
     *  @param  branchId a branch <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> branchId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBranchId(org.osid.id.Id branchId, boolean match) {
        return;
    }


    /**
     *  Clears the branch <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBranchIdTerms() {
        return;
    }


    /**
     *  Tests if a branch query is available. 
     *
     *  @return <code> true </code> if a branch query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBranchQuery() {
        return (false);
    }


    /**
     *  Gets the query for a branch. 
     *
     *  @return the branch query 
     *  @throws org.osid.UnimplementedException <code> supportsBranchQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.BranchQuery getBranchQuery() {
        throw new org.osid.UnimplementedException("supportsBranchQuery() is false");
    }


    /**
     *  Matches journals with any branches. 
     *
     *  @param  match <code> true </code> to match journals with any branch, 
     *          <code> false </code> to match journals with no branches 
     */

    @OSID @Override
    public void matchAnyBranch(boolean match) {
        return;
    }


    /**
     *  Clears the branch terms. 
     */

    @OSID @Override
    public void clearBranchTerms() {
        return;
    }


    /**
     *  Sets the journal <code> Id </code> for this query to match journals 
     *  that have the specified journal as an ancestor. 
     *
     *  @param  journalId a journal <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> journalId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorJournalId(org.osid.id.Id journalId, boolean match) {
        return;
    }


    /**
     *  Clears the ancestor journal <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorJournalIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> JournalQuery </code> is available. 
     *
     *  @return <code> true </code> if a journal query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorJournalQuery() {
        return (false);
    }


    /**
     *  Gets the query for a journal. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the journal query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorJournalQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalQuery getAncestorJournalQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorJournalQuery() is false");
    }


    /**
     *  Matches journals with any ancestor. 
     *
     *  @param  match <code> true </code> to match journals with any ancestor, 
     *          <code> false </code> to match root journals 
     */

    @OSID @Override
    public void matchAnyAncestorJournal(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor journal terms. 
     */

    @OSID @Override
    public void clearAncestorJournalTerms() {
        return;
    }


    /**
     *  Sets the journal <code> Id </code> for this query to match journals 
     *  that have the specified journal as a descendant. 
     *
     *  @param  journalId a journal <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> journalId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantJournalId(org.osid.id.Id journalId, 
                                         boolean match) {
        return;
    }


    /**
     *  Clears the descendant journal <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantJournalIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> JournalQuery </code> is available. 
     *
     *  @return <code> true </code> if a journal query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantJournalQuery() {
        return (false);
    }


    /**
     *  Gets the query for a journal. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the journal query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantJournalQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalQuery getDescendantJournalQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantJournalQuery() is false");
    }


    /**
     *  Matches journals with any descendant. 
     *
     *  @param  match <code> true </code> to match journals with any 
     *          descendant, <code> false </code> to match leaf journals 
     */

    @OSID @Override
    public void matchAnyDescendantJournal(boolean match) {
        return;
    }


    /**
     *  Clears the descendant journal terms. 
     */

    @OSID @Override
    public void clearDescendantJournalTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given journal query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a journal implementing the requested record.
     *
     *  @param journalRecordType a journal record type
     *  @return the journal query record
     *  @throws org.osid.NullArgumentException
     *          <code>journalRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(journalRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.journaling.records.JournalQueryRecord getJournalQueryRecord(org.osid.type.Type journalRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.journaling.records.JournalQueryRecord record : this.records) {
            if (record.implementsRecordType(journalRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(journalRecordType + " is not supported");
    }


    /**
     *  Adds a record to this journal query. 
     *
     *  @param journalQueryRecord journal query record
     *  @param journalRecordType journal record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addJournalQueryRecord(org.osid.journaling.records.JournalQueryRecord journalQueryRecord, 
                                          org.osid.type.Type journalRecordType) {

        addRecordType(journalRecordType);
        nullarg(journalQueryRecord, "journal query record");
        this.records.add(journalQueryRecord);        
        return;
    }
}
