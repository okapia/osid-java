//
// InvariantMapConvocationLookupSession
//
//    Implements a Convocation lookup service backed by a fixed collection of
//    convocations.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.recognition;


/**
 *  Implements a Convocation lookup service backed by a fixed
 *  collection of convocations. The convocations are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapConvocationLookupSession
    extends net.okapia.osid.jamocha.core.recognition.spi.AbstractMapConvocationLookupSession
    implements org.osid.recognition.ConvocationLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapConvocationLookupSession</code> with no
     *  convocations.
     *  
     *  @param academy the academy
     *  @throws org.osid.NullArgumnetException {@code academy} is
     *          {@code null}
     */

    public InvariantMapConvocationLookupSession(org.osid.recognition.Academy academy) {
        setAcademy(academy);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapConvocationLookupSession</code> with a single
     *  convocation.
     *  
     *  @param academy the academy
     *  @param convocation a single convocation
     *  @throws org.osid.NullArgumentException {@code academy} or
     *          {@code convocation} is <code>null</code>
     */

      public InvariantMapConvocationLookupSession(org.osid.recognition.Academy academy,
                                               org.osid.recognition.Convocation convocation) {
        this(academy);
        putConvocation(convocation);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapConvocationLookupSession</code> using an array
     *  of convocations.
     *  
     *  @param academy the academy
     *  @param convocations an array of convocations
     *  @throws org.osid.NullArgumentException {@code academy} or
     *          {@code convocations} is <code>null</code>
     */

      public InvariantMapConvocationLookupSession(org.osid.recognition.Academy academy,
                                               org.osid.recognition.Convocation[] convocations) {
        this(academy);
        putConvocations(convocations);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapConvocationLookupSession</code> using a
     *  collection of convocations.
     *
     *  @param academy the academy
     *  @param convocations a collection of convocations
     *  @throws org.osid.NullArgumentException {@code academy} or
     *          {@code convocations} is <code>null</code>
     */

      public InvariantMapConvocationLookupSession(org.osid.recognition.Academy academy,
                                               java.util.Collection<? extends org.osid.recognition.Convocation> convocations) {
        this(academy);
        putConvocations(convocations);
        return;
    }
}
