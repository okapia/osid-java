//
// AbstractAssemblyInputEnablerQuery.java
//
//     An InputEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.control.rules.inputenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An InputEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblyInputEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.control.rules.InputEnablerQuery,
               org.osid.control.rules.InputEnablerQueryInspector,
               org.osid.control.rules.InputEnablerSearchOrder {

    private final java.util.Collection<org.osid.control.rules.records.InputEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.control.rules.records.InputEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.control.rules.records.InputEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyInputEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyInputEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to the input. 
     *
     *  @param  inputId the device <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> inputId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledInputId(org.osid.id.Id inputId, boolean match) {
        getAssembler().addIdTerm(getRuledInputIdColumn(), inputId, match);
        return;
    }


    /**
     *  Clears the input <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledInputIdTerms() {
        getAssembler().clearTerms(getRuledInputIdColumn());
        return;
    }


    /**
     *  Gets the input <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledInputIdTerms() {
        return (getAssembler().getIdTerms(getRuledInputIdColumn()));
    }


    /**
     *  Gets the RuledInputId column name.
     *
     * @return the column name
     */

    protected String getRuledInputIdColumn() {
        return ("ruled_input_id");
    }


    /**
     *  Tests if an <code> InputQuery </code> is available. 
     *
     *  @return <code> true </code> if an input query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledInputQuery() {
        return (false);
    }


    /**
     *  Gets the query for an input. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the input query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledInputQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.InputQuery getRuledInputQuery() {
        throw new org.osid.UnimplementedException("supportsRuledInputQuery() is false");
    }


    /**
     *  Matches enablers mapped to any input. 
     *
     *  @param  match <code> true </code> for enablers mapped to any input, 
     *          <code> false </code> to match enablers mapped to no inputs 
     */

    @OSID @Override
    public void matchAnyRuledInput(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledInputColumn(), match);
        return;
    }


    /**
     *  Clears the input query terms. 
     */

    @OSID @Override
    public void clearRuledInputTerms() {
        getAssembler().clearTerms(getRuledInputColumn());
        return;
    }


    /**
     *  Gets the input query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.InputQueryInspector[] getRuledInputTerms() {
        return (new org.osid.control.InputQueryInspector[0]);
    }


    /**
     *  Gets the RuledInput column name.
     *
     * @return the column name
     */

    protected String getRuledInputColumn() {
        return ("ruled_input");
    }


    /**
     *  Matches enablers mapped to the system. 
     *
     *  @param  systemId the system <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSystemId(org.osid.id.Id systemId, boolean match) {
        getAssembler().addIdTerm(getSystemIdColumn(), systemId, match);
        return;
    }


    /**
     *  Clears the system <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSystemIdTerms() {
        getAssembler().clearTerms(getSystemIdColumn());
        return;
    }


    /**
     *  Gets the system <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSystemIdTerms() {
        return (getAssembler().getIdTerms(getSystemIdColumn()));
    }


    /**
     *  Gets the SystemId column name.
     *
     * @return the column name
     */

    protected String getSystemIdColumn() {
        return ("system_id");
    }


    /**
     *  Tests if a <code> SystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a system query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSystemQuery() {
        return (false);
    }


    /**
     *  Gets the query for a system. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the system query 
     *  @throws org.osid.UnimplementedException <code> supportsSystemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SystemQuery getSystemQuery() {
        throw new org.osid.UnimplementedException("supportsSystemQuery() is false");
    }


    /**
     *  Clears the system query terms. 
     */

    @OSID @Override
    public void clearSystemTerms() {
        getAssembler().clearTerms(getSystemColumn());
        return;
    }


    /**
     *  Gets the system query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.SystemQueryInspector[] getSystemTerms() {
        return (new org.osid.control.SystemQueryInspector[0]);
    }


    /**
     *  Gets the System column name.
     *
     * @return the column name
     */

    protected String getSystemColumn() {
        return ("system");
    }


    /**
     *  Tests if this inputEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  inputEnablerRecordType an input enabler record type 
     *  @return <code>true</code> if the inputEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>inputEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type inputEnablerRecordType) {
        for (org.osid.control.rules.records.InputEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(inputEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  inputEnablerRecordType the input enabler record type 
     *  @return the input enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>inputEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(inputEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.rules.records.InputEnablerQueryRecord getInputEnablerQueryRecord(org.osid.type.Type inputEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.rules.records.InputEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(inputEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(inputEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  inputEnablerRecordType the input enabler record type 
     *  @return the input enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>inputEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(inputEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.rules.records.InputEnablerQueryInspectorRecord getInputEnablerQueryInspectorRecord(org.osid.type.Type inputEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.rules.records.InputEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(inputEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(inputEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param inputEnablerRecordType the input enabler record type
     *  @return the input enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>inputEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(inputEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.rules.records.InputEnablerSearchOrderRecord getInputEnablerSearchOrderRecord(org.osid.type.Type inputEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.rules.records.InputEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(inputEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(inputEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this input enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param inputEnablerQueryRecord the input enabler query record
     *  @param inputEnablerQueryInspectorRecord the input enabler query inspector
     *         record
     *  @param inputEnablerSearchOrderRecord the input enabler search order record
     *  @param inputEnablerRecordType input enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>inputEnablerQueryRecord</code>,
     *          <code>inputEnablerQueryInspectorRecord</code>,
     *          <code>inputEnablerSearchOrderRecord</code> or
     *          <code>inputEnablerRecordTypeinputEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addInputEnablerRecords(org.osid.control.rules.records.InputEnablerQueryRecord inputEnablerQueryRecord, 
                                      org.osid.control.rules.records.InputEnablerQueryInspectorRecord inputEnablerQueryInspectorRecord, 
                                      org.osid.control.rules.records.InputEnablerSearchOrderRecord inputEnablerSearchOrderRecord, 
                                      org.osid.type.Type inputEnablerRecordType) {

        addRecordType(inputEnablerRecordType);

        nullarg(inputEnablerQueryRecord, "input enabler query record");
        nullarg(inputEnablerQueryInspectorRecord, "input enabler query inspector record");
        nullarg(inputEnablerSearchOrderRecord, "input enabler search odrer record");

        this.queryRecords.add(inputEnablerQueryRecord);
        this.queryInspectorRecords.add(inputEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(inputEnablerSearchOrderRecord);
        
        return;
    }
}
