//
// ReceiptMiter.java
//
//     Defines a Receipt miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.messaging.receipt;


/**
 *  Defines a <code>Receipt</code> miter for use with the builders.
 */

public interface ReceiptMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.messaging.Receipt {


    /**
     *  Sets the message.
     *
     *  @param message a message
     *  @throws org.osid.NullArgumentException <code>message</code> is
     *          <code>null</code>
     */

    public void setMessage(org.osid.messaging.Message message);


    /**
     *  Sets the received time.
     *
     *  @param time a received time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    public void setReceivedTime(org.osid.calendaring.DateTime time);


    /**
     *  Sets the receiving agent.
     *
     *  @param agent a receiving agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public void setReceivingAgent(org.osid.authentication.Agent agent);


    /**
     *  Sets the recipient.
     *
     *  @param recipient a recipient
     *  @throws org.osid.NullArgumentException <code>recipient</code>
     *          is <code>null</code>
     */

    public void setRecipient(org.osid.resource.Resource recipient);


    /**
     *  Adds a Receipt record.
     *
     *  @param record a receipt record
     *  @param recordType the type of receipt record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addReceiptRecord(org.osid.messaging.records.ReceiptRecord record, org.osid.type.Type recordType);
}       


