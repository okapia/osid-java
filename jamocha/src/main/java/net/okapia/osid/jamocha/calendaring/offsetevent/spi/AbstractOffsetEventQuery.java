//
// AbstractOffsetEventQuery.java
//
//     A template for making an OffsetEvent Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.offsetevent.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for offset events.
 */

public abstract class AbstractOffsetEventQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleQuery
    implements org.osid.calendaring.OffsetEventQuery {

    private final java.util.Collection<org.osid.calendaring.records.OffsetEventQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches a fixed start time between the given range inclusive. 
     *
     *  @param  from the start of the range 
     *  @param  to the end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> <code> null </code> 
     */

    @OSID @Override
    public void matchFixedStartTime(org.osid.calendaring.DateTime from, 
                                    org.osid.calendaring.DateTime to, 
                                    boolean match) {
        return;
    }


    /**
     *  Matches events with fixed start times. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnyFixedStartTime(boolean match) {
        return;
    }


    /**
     *  Clears the fixed start time terms. 
     */

    @OSID @Override
    public void clearFixedStartTimeTerms() {
        return;
    }


    /**
     *  Sets the start reference event <code> Id </code> for this query. 
     *
     *  @param  eventId an event <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> eventId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchStartReferenceEventId(org.osid.id.Id eventId, 
                                           boolean match) {
        return;
    }


    /**
     *  Clears the start reference event <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearStartReferenceEventIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> EventQuery </code> is available for querying start 
     *  reference event terms. 
     *
     *  @return <code> true </code> if an event query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStartReferenceEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for the start reference event. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the event query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStartReferenceEventQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventQuery getStartReferenceEventQuery() {
        throw new org.osid.UnimplementedException("supportsStartReferenceEventQuery() is false");
    }


    /**
     *  Matches offset events with any starting reference event. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnyStartReferenceEvent(boolean match) {
        return;
    }


    /**
     *  Clears the start reference event terms. 
     */

    @OSID @Override
    public void clearStartReferenceEventTerms() {
        return;
    }


    /**
     *  Matches a fixed offset amount between the given range inclusive. 
     *
     *  @param  from the start of the range 
     *  @param  to the end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> <code> null </code> 
     */

    @OSID @Override
    public void matchFixedStartOffset(org.osid.calendaring.Duration from, 
                                      org.osid.calendaring.Duration to, 
                                      boolean match) {
        return;
    }


    /**
     *  Matches fixed offset events. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnyFixedStartOffset(boolean match) {
        return;
    }


    /**
     *  Clears the fixed offset terms. 
     */

    @OSID @Override
    public void clearFixedStartOffsetTerms() {
        return;
    }


    /**
     *  Matches a relative weekday offset amount between the given range 
     *  inclusive. 
     *
     *  @param  low the start of the range 
     *  @param  high the end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchRelativeWeekdayStartOffset(long low, long high, 
                                                boolean match) {
        return;
    }


    /**
     *  Clears the relative weekday offset terms. 
     */

    @OSID @Override
    public void clearRelativeWeekdayStartOffsetTerms() {
        return;
    }


    /**
     *  Matches a relative weekday. 
     *
     *  @param  weekday the weekday 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchRelativeStartWeekday(long weekday, boolean match) {
        return;
    }


    /**
     *  Matches relative weekday offset events. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnyRelativeStartWeekday(boolean match) {
        return;
    }


    /**
     *  Clears the relative weekday terms. 
     */

    @OSID @Override
    public void clearRelativeStartWeekdayTerms() {
        return;
    }


    /**
     *  Matches a fixed duration between the given range inclusive. 
     *
     *  @param  low the start of the range 
     *  @param  high the end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchFixedDuration(org.osid.calendaring.Duration low, 
                                   org.osid.calendaring.Duration high, 
                                   boolean match) {
        return;
    }


    /**
     *  Clears the fixed duration offset terms. 
     */

    @OSID @Override
    public void clearFixedDurationTerms() {
        return;
    }


    /**
     *  Sets the end reference event <code> Id </code> for this query. 
     *
     *  @param  eventId an event <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> eventId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEndReferenceEventId(org.osid.id.Id eventId, boolean match) {
        return;
    }


    /**
     *  Clears the end reference event <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearEndReferenceEventIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> EventQuery </code> is available for querying end 
     *  reference event terms. 
     *
     *  @return <code> true </code> if an event query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEndReferenceEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for the end reference event. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the event query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEventReferenceEventQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventQuery getEndReferenceEventQuery() {
        throw new org.osid.UnimplementedException("supportsEndReferenceEventQuery() is false");
    }


    /**
     *  Matches any end reference event events. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnyEndReferenceEvent(boolean match) {
        return;
    }


    /**
     *  Clears the end reference event terms. 
     */

    @OSID @Override
    public void clearEndReferenceEventTerms() {
        return;
    }


    /**
     *  Matches a fixed offset amount between the given range inclusive. 
     *
     *  @param  from the start of the range 
     *  @param  to the end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> <code> null </code> 
     */

    @OSID @Override
    public void matchFixedEndOffset(org.osid.calendaring.Duration from, 
                                    org.osid.calendaring.Duration to, 
                                    boolean match) {
        return;
    }


    /**
     *  Matches fixed offset events. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnyFixedEndOffset(boolean match) {
        return;
    }


    /**
     *  Clears the fixed offset terms. 
     */

    @OSID @Override
    public void clearFixedEndOffsetTerms() {
        return;
    }


    /**
     *  Matches a relative weekday offset amount between the given range 
     *  inclusive. 
     *
     *  @param  low the start of the range 
     *  @param  high the end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchRelativeWeekdayEndOffset(long low, long high, 
                                              boolean match) {
        return;
    }


    /**
     *  Clears the relative weekday offset terms. 
     */

    @OSID @Override
    public void clearRelativeWeekdayEndOffsetTerms() {
        return;
    }


    /**
     *  Matches a relative weekday. 
     *
     *  @param  weekday the weekday 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchRelativeEndWeekday(long weekday, boolean match) {
        return;
    }


    /**
     *  Matches relative weekday offset events. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnyRelativeEndWeekday(boolean match) {
        return;
    }


    /**
     *  Clears the relative weekday terms. 
     */

    @OSID @Override
    public void clearRelativeEndWeekdayTerms() {
        return;
    }


    /**
     *  Matches the location description string. 
     *
     *  @param  location location string 
     *  @param  stringMatchType string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> location </code> is 
     *          not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> location </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchLocationDescription(String location, 
                                         org.osid.type.Type stringMatchType, 
                                         boolean match) {
        return;
    }


    /**
     *  Matches an event that has any location description assigned. 
     *
     *  @param  match <code> true </code> to match events with any location 
     *          description, <code> false </code> to match events with no 
     *          location description 
     */

    @OSID @Override
    public void matchAnyLocationDescription(boolean match) {
        return;
    }


    /**
     *  Clears the location description terms. 
     */

    @OSID @Override
    public void clearLocationDescriptionTerms() {
        return;
    }


    /**
     *  Sets the location <code> Id </code> for this query. 
     *
     *  @param  locationId a location <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> locationId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLocationId(org.osid.id.Id locationId, boolean match) {
        return;
    }


    /**
     *  Clears the location <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearLocationIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> LocationQuery </code> is available for querying 
     *  locations. 
     *
     *  @return <code> true </code> if a location query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a location. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the location query 
     *  @throws org.osid.UnimplementedException <code> supportsLocationQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationQuery getLocationQuery() {
        throw new org.osid.UnimplementedException("supportsLocationQuery() is false");
    }


    /**
     *  Matches an event that has any location assigned. 
     *
     *  @param  match <code> true </code> to match events with any location, 
     *          <code> false </code> to match events with no location 
     */

    @OSID @Override
    public void matchAnyLocation(boolean match) {
        return;
    }


    /**
     *  Clears the location terms. 
     */

    @OSID @Override
    public void clearLocationTerms() {
        return;
    }


    /**
     *  Sets the sponsor <code> Id </code> for this query. 
     *
     *  @param  sponsorId a sponsor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> sponsorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSponsorId(org.osid.id.Id sponsorId, boolean match) {
        return;
    }


    /**
     *  Clears the sponsor <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSponsorIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> LocationQuery </code> is available for querying 
     *  sponsors. 
     *
     *  @return <code> true </code> if a sponsor query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSponsorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a sponsor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the sponsor query 
     *  @throws org.osid.UnimplementedException <code> supportsSponsorQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getSponsorQuery() {
        throw new org.osid.UnimplementedException("supportsSponsorQuery() is false");
    }


    /**
     *  Clears the sponsor terms. 
     */

    @OSID @Override
    public void clearSponsorTerms() {
        return;
    }


    /**
     *  Sets the calendar <code> Id </code> for this query. 
     *
     *  @param  calendarId a calendar <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCalendarId(org.osid.id.Id calendarId, boolean match) {
        return;
    }


    /**
     *  Clears the calendar <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCalendarIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CalendarQuery </code> is available for querying 
     *  calendars. 
     *
     *  @return <code> true </code> if a calendar query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarQuery() {
        return (false);
    }


    /**
     *  Gets the query for a calendar. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the calendar query 
     *  @throws org.osid.UnimplementedException <code> supportsCalendarQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQuery getCalendarQuery() {
        throw new org.osid.UnimplementedException("supportsCalendarQuery() is false");
    }


    /**
     *  Clears the calendar terms. 
     */

    @OSID @Override
    public void clearCalendarTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given offset event query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an offset event implementing the requested record.
     *
     *  @param offsetEventRecordType an offset event record type
     *  @return the offset event query record
     *  @throws org.osid.NullArgumentException
     *          <code>offsetEventRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(offsetEventRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.OffsetEventQueryRecord getOffsetEventQueryRecord(org.osid.type.Type offsetEventRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.OffsetEventQueryRecord record : this.records) {
            if (record.implementsRecordType(offsetEventRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(offsetEventRecordType + " is not supported");
    }


    /**
     *  Adds a record to this offset event query. 
     *
     *  @param offsetEventQueryRecord offset event query record
     *  @param offsetEventRecordType offsetEvent record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addOffsetEventQueryRecord(org.osid.calendaring.records.OffsetEventQueryRecord offsetEventQueryRecord, 
                                          org.osid.type.Type offsetEventRecordType) {

        addRecordType(offsetEventRecordType);
        nullarg(offsetEventQueryRecord, "offset event query record");
        this.records.add(offsetEventQueryRecord);        
        return;
    }
}
