//
// MutableMapProxyAwardEntryLookupSession
//
//    Implements an AwardEntry lookup service backed by a collection of
//    awardEntries that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.chronicle;


/**
 *  Implements an AwardEntry lookup service backed by a collection of
 *  awardEntries. The awardEntries are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of award entries can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyAwardEntryLookupSession
    extends net.okapia.osid.jamocha.core.course.chronicle.spi.AbstractMapAwardEntryLookupSession
    implements org.osid.course.chronicle.AwardEntryLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyAwardEntryLookupSession}
     *  with no award entries.
     *
     *  @param courseCatalog the course catalog
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyAwardEntryLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.proxy.Proxy proxy) {
        setCourseCatalog(courseCatalog);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyAwardEntryLookupSession} with a
     *  single award entry.
     *
     *  @param courseCatalog the course catalog
     *  @param awardEntry an award entry
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code awardEntry}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyAwardEntryLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                org.osid.course.chronicle.AwardEntry awardEntry, org.osid.proxy.Proxy proxy) {
        this(courseCatalog, proxy);
        putAwardEntry(awardEntry);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyAwardEntryLookupSession} using an
     *  array of award entries.
     *
     *  @param courseCatalog the course catalog
     *  @param awardEntries an array of award entries
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code awardEntries}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyAwardEntryLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                org.osid.course.chronicle.AwardEntry[] awardEntries, org.osid.proxy.Proxy proxy) {
        this(courseCatalog, proxy);
        putAwardEntries(awardEntries);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyAwardEntryLookupSession} using a
     *  collection of award entries.
     *
     *  @param courseCatalog the course catalog
     *  @param awardEntries a collection of award entries
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code awardEntries}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyAwardEntryLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                java.util.Collection<? extends org.osid.course.chronicle.AwardEntry> awardEntries,
                                                org.osid.proxy.Proxy proxy) {
   
        this(courseCatalog, proxy);
        setSessionProxy(proxy);
        putAwardEntries(awardEntries);
        return;
    }

    
    /**
     *  Makes a {@code AwardEntry} available in this session.
     *
     *  @param awardEntry an award entry
     *  @throws org.osid.NullArgumentException {@code awardEntry{@code 
     *          is {@code null}
     */

    @Override
    public void putAwardEntry(org.osid.course.chronicle.AwardEntry awardEntry) {
        super.putAwardEntry(awardEntry);
        return;
    }


    /**
     *  Makes an array of awardEntries available in this session.
     *
     *  @param awardEntries an array of award entries
     *  @throws org.osid.NullArgumentException {@code awardEntries{@code 
     *          is {@code null}
     */

    @Override
    public void putAwardEntries(org.osid.course.chronicle.AwardEntry[] awardEntries) {
        super.putAwardEntries(awardEntries);
        return;
    }


    /**
     *  Makes collection of award entries available in this session.
     *
     *  @param awardEntries
     *  @throws org.osid.NullArgumentException {@code awardEntry{@code 
     *          is {@code null}
     */

    @Override
    public void putAwardEntries(java.util.Collection<? extends org.osid.course.chronicle.AwardEntry> awardEntries) {
        super.putAwardEntries(awardEntries);
        return;
    }


    /**
     *  Removes a AwardEntry from this session.
     *
     *  @param awardEntryId the {@code Id} of the award entry
     *  @throws org.osid.NullArgumentException {@code awardEntryId{@code  is
     *          {@code null}
     */

    @Override
    public void removeAwardEntry(org.osid.id.Id awardEntryId) {
        super.removeAwardEntry(awardEntryId);
        return;
    }    
}
