//
// Validation
//
//     An enumeration of validation types.
//
//
// Tom Coppeto
// Okapia
// 21 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator;


/**
 *  An enumeration of validation types.
 */

public enum Validation {
    /** all checks */
    ALL,

    /** no checks */
    NONE,

    /** check for any nulls */
    NULL,

    /** check for any nil implementations of Id */
    NIL_ID,

    /** check for any nil implementations of Type */
    NIL_TYPE,

    /** check for any nil implementations objects */
    NIL_OBJECT,

    /** check to make sure low/high, min,max ranges are in the right order */
    RANGE,

    /** check to make sure cardinal numbers are non-negative */
    CARDINAL,

    /** cross reference type support */
    TYPE_SUPPORT,

    /** check for any empty string returns */
    EMPTY_STRING,

    /** check for consistent Ids */
    ID_CONSISTENCY,

    /** check for proper method support */
    METHOD_SUPPORT,

    /** traverse sub-objects */
    TRAVERSE,

    /** check records */
    RECORDS;
}

