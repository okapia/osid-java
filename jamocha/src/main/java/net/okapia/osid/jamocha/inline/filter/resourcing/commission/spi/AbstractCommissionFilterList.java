//
// AbstractCommissionList
//
//     Implements a filter for a CommissionList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.resourcing.commission.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for a CommissionList. Subclasses call this
 *  constructor and implement the <code>pass()</code> method. This
 *  filter is synchronous but can be wrapped in a BufferedCommissionList
 *  to improve performance.
 */

public abstract class AbstractCommissionFilterList
    extends net.okapia.osid.jamocha.resourcing.commission.spi.AbstractCommissionList
    implements org.osid.resourcing.CommissionList,
               net.okapia.osid.jamocha.inline.filter.resourcing.commission.CommissionFilter {

    private org.osid.resourcing.Commission commission;
    private final org.osid.resourcing.CommissionList list;
    private org.osid.OsidException error;


    /**
     *  Creates a new <code>AbstractCommissionFilterList</code>.
     *
     *  @param commissionList a <code>CommissionList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>commissionList</code> is <code>null</code>
     */

    protected AbstractCommissionFilterList(org.osid.resourcing.CommissionList commissionList) {
        nullarg(commissionList, "commission list");
        this.list = commissionList;
        return;
    }    

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in
     *          this list, <code> false </code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (hasError()) {
            return (true);
        }

        prime();

        if (this.commission == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code> Commission </code> in this list. 
     *
     *  @return the next <code> Commission </code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code> Commission </code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resourcing.Commission getNextCommission()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        if (hasNext()) {
            org.osid.resourcing.Commission commission = this.commission;
            this.commission = null;
            return (commission);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in commission list");
        }
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.commission = null;
        this.list.close();
        return;
    }

    
    /**
     *  Filters Commissions.
     *
     *  @param commission the commission to filter
     *  @return <code>true</code> if the commission passes the filter,
     *          <code>false</code> if the commission should be filtered
     */

    public abstract boolean pass(org.osid.resourcing.Commission commission);


    protected void prime() {
        if (this.commission != null) {
            return;
        }

        org.osid.resourcing.Commission commission = null;

        while (this.list.hasNext()) {
            try {
                commission = this.list.getNextCommission();
            } catch (org.osid.OsidException oe) {
                error(oe);
                return;
            }

            if (pass(commission)) {
                this.commission = commission;
                return;
            }
        }

        return;
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    protected void error(org.osid.OsidException error) {
        this.error = error;
        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    protected boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }
}
