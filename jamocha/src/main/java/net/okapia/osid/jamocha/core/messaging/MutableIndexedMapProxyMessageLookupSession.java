//
// MutableIndexedMapProxyMessageLookupSession
//
//    Implements a Message lookup service backed by a collection of
//    messages indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.messaging;


/**
 *  Implements a Message lookup service backed by a collection of
 *  messages. The messages are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some messages may be compatible
 *  with more types than are indicated through these message
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of messages can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyMessageLookupSession
    extends net.okapia.osid.jamocha.core.messaging.spi.AbstractIndexedMapMessageLookupSession
    implements org.osid.messaging.MessageLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyMessageLookupSession} with
     *  no message.
     *
     *  @param mailbox the mailbox
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code mailbox} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyMessageLookupSession(org.osid.messaging.Mailbox mailbox,
                                                       org.osid.proxy.Proxy proxy) {
        setMailbox(mailbox);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyMessageLookupSession} with
     *  a single message.
     *
     *  @param mailbox the mailbox
     *  @param  message an message
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code mailbox},
     *          {@code message}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyMessageLookupSession(org.osid.messaging.Mailbox mailbox,
                                                       org.osid.messaging.Message message, org.osid.proxy.Proxy proxy) {

        this(mailbox, proxy);
        putMessage(message);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyMessageLookupSession} using
     *  an array of messages.
     *
     *  @param mailbox the mailbox
     *  @param  messages an array of messages
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code mailbox},
     *          {@code messages}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyMessageLookupSession(org.osid.messaging.Mailbox mailbox,
                                                       org.osid.messaging.Message[] messages, org.osid.proxy.Proxy proxy) {

        this(mailbox, proxy);
        putMessages(messages);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyMessageLookupSession} using
     *  a collection of messages.
     *
     *  @param mailbox the mailbox
     *  @param  messages a collection of messages
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code mailbox},
     *          {@code messages}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyMessageLookupSession(org.osid.messaging.Mailbox mailbox,
                                                       java.util.Collection<? extends org.osid.messaging.Message> messages,
                                                       org.osid.proxy.Proxy proxy) {
        this(mailbox, proxy);
        putMessages(messages);
        return;
    }

    
    /**
     *  Makes a {@code Message} available in this session.
     *
     *  @param  message a message
     *  @throws org.osid.NullArgumentException {@code message{@code 
     *          is {@code null}
     */

    @Override
    public void putMessage(org.osid.messaging.Message message) {
        super.putMessage(message);
        return;
    }


    /**
     *  Makes an array of messages available in this session.
     *
     *  @param  messages an array of messages
     *  @throws org.osid.NullArgumentException {@code messages{@code 
     *          is {@code null}
     */

    @Override
    public void putMessages(org.osid.messaging.Message[] messages) {
        super.putMessages(messages);
        return;
    }


    /**
     *  Makes collection of messages available in this session.
     *
     *  @param  messages a collection of messages
     *  @throws org.osid.NullArgumentException {@code message{@code 
     *          is {@code null}
     */

    @Override
    public void putMessages(java.util.Collection<? extends org.osid.messaging.Message> messages) {
        super.putMessages(messages);
        return;
    }


    /**
     *  Removes a Message from this session.
     *
     *  @param messageId the {@code Id} of the message
     *  @throws org.osid.NullArgumentException {@code messageId{@code  is
     *          {@code null}
     */

    @Override
    public void removeMessage(org.osid.id.Id messageId) {
        super.removeMessage(messageId);
        return;
    }    
}
