//
// MutableGradeSystemTransformList.java
//
//     Implements a GradeSystemTransformList. This list allows GradeSystemTransforms to be
//     added after this list has been created.
//
//
// Tom Coppeto
// OnTapSolutions
// 29 June 2008
//
//
// Copyright (c) 2008, 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.transform.gradesystemtransform;


/**
 *  <p>Implements a GradeSystemTransformList. This list allows GradeSystemTransforms to be
 *  added after this gradeSystemTransform has been created. One this list has been
 *  returned to the consumer, all subsequent additions occur in a
 *  separate processing thread.  The creator of this gradeSystemTransform must
 *  invoke <code>eol()</code> when there are no more gradeSystemTransforms to be
 *  added.</p>
 *
 *  <p> If the consumer of the <code>GradeSystemTransformList</code> interface
 *  reaches the end of the internal buffer before <code>eol()</code>,
 *  then methods will block until more gradeSystemTransforms are added or
 *  <code>eol()</code> is invoked.</p>
 *
 *  <p><code>available()</code> never blocks but may return
 *  <code>0</code> if waiting for more gradeSystemTransforms to be added.</p>
 */

public final class MutableGradeSystemTransformList
    extends net.okapia.osid.jamocha.grading.transform.gradesystemtransform.spi.AbstractMutableGradeSystemTransformList
    implements org.osid.grading.transform.GradeSystemTransformList {


    /**
     *  Creates a new empty <code>MutableGradeSystemTransformList</code>.
     */

    public MutableGradeSystemTransformList() {
        super();
    }


    /**
     *  Creates a new <code>MutableGradeSystemTransformList</code>.
     *
     *  @param gradeSystemTransform a <code>GradeSystemTransform</code>
     *  @throws org.osid.NullArgumentException <code>gradeSystemTransform</code>
     *          is <code>null</code>
     */

    public MutableGradeSystemTransformList(org.osid.grading.transform.GradeSystemTransform gradeSystemTransform) {
        super(gradeSystemTransform);
        return;
    }


    /**
     *  Creates a new <code>MutableGradeSystemTransformList</code>.
     *
     *  @param array an array of gradesystemtransforms
     *  @throws org.osid.NullArgumentException <code>array</code>
     *          is <code>null</code>
     */

    public MutableGradeSystemTransformList(org.osid.grading.transform.GradeSystemTransform[] array) {
        super(array);
        return;
    }


    /**
     *  Creates a new <code>MutableGradeSystemTransformList</code>.
     *
     *  @param collection a java.util.Collection of gradesystemtransforms
     *  @throws org.osid.NullArgumentException <code>collection</code>
     *          is <code>null</code>
     */

    public MutableGradeSystemTransformList(java.util.Collection<org.osid.grading.transform.GradeSystemTransform> collection) {
        super(collection);
        return;
    }
}
