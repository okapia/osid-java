//
// MutableIndexedMapProxyTimePeriodLookupSession
//
//    Implements a TimePeriod lookup service backed by a collection of
//    timePeriods indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring;


/**
 *  Implements a TimePeriod lookup service backed by a collection of
 *  timePeriods. The time periods are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some timePeriods may be compatible
 *  with more types than are indicated through these timePeriod
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of time periods can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyTimePeriodLookupSession
    extends net.okapia.osid.jamocha.core.calendaring.spi.AbstractIndexedMapTimePeriodLookupSession
    implements org.osid.calendaring.TimePeriodLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyTimePeriodLookupSession} with
     *  no time period.
     *
     *  @param calendar the calendar
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyTimePeriodLookupSession(org.osid.calendaring.Calendar calendar,
                                                       org.osid.proxy.Proxy proxy) {
        setCalendar(calendar);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyTimePeriodLookupSession} with
     *  a single time period.
     *
     *  @param calendar the calendar
     *  @param  timePeriod an time period
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code timePeriod}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyTimePeriodLookupSession(org.osid.calendaring.Calendar calendar,
                                                       org.osid.calendaring.TimePeriod timePeriod, org.osid.proxy.Proxy proxy) {

        this(calendar, proxy);
        putTimePeriod(timePeriod);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyTimePeriodLookupSession} using
     *  an array of time periods.
     *
     *  @param calendar the calendar
     *  @param  timePeriods an array of time periods
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code timePeriods}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyTimePeriodLookupSession(org.osid.calendaring.Calendar calendar,
                                                       org.osid.calendaring.TimePeriod[] timePeriods, org.osid.proxy.Proxy proxy) {

        this(calendar, proxy);
        putTimePeriods(timePeriods);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyTimePeriodLookupSession} using
     *  a collection of time periods.
     *
     *  @param calendar the calendar
     *  @param  timePeriods a collection of time periods
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code timePeriods}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyTimePeriodLookupSession(org.osid.calendaring.Calendar calendar,
                                                       java.util.Collection<? extends org.osid.calendaring.TimePeriod> timePeriods,
                                                       org.osid.proxy.Proxy proxy) {
        this(calendar, proxy);
        putTimePeriods(timePeriods);
        return;
    }

    
    /**
     *  Makes a {@code TimePeriod} available in this session.
     *
     *  @param  timePeriod a time period
     *  @throws org.osid.NullArgumentException {@code timePeriod{@code 
     *          is {@code null}
     */

    @Override
    public void putTimePeriod(org.osid.calendaring.TimePeriod timePeriod) {
        super.putTimePeriod(timePeriod);
        return;
    }


    /**
     *  Makes an array of time periods available in this session.
     *
     *  @param  timePeriods an array of time periods
     *  @throws org.osid.NullArgumentException {@code timePeriods{@code 
     *          is {@code null}
     */

    @Override
    public void putTimePeriods(org.osid.calendaring.TimePeriod[] timePeriods) {
        super.putTimePeriods(timePeriods);
        return;
    }


    /**
     *  Makes collection of time periods available in this session.
     *
     *  @param  timePeriods a collection of time periods
     *  @throws org.osid.NullArgumentException {@code timePeriod{@code 
     *          is {@code null}
     */

    @Override
    public void putTimePeriods(java.util.Collection<? extends org.osid.calendaring.TimePeriod> timePeriods) {
        super.putTimePeriods(timePeriods);
        return;
    }


    /**
     *  Removes a TimePeriod from this session.
     *
     *  @param timePeriodId the {@code Id} of the time period
     *  @throws org.osid.NullArgumentException {@code timePeriodId{@code  is
     *          {@code null}
     */

    @Override
    public void removeTimePeriod(org.osid.id.Id timePeriodId) {
        super.removeTimePeriod(timePeriodId);
        return;
    }    
}
