//
// AbstractProfileEntrySearchOdrer.java
//
//     Defines a ProfileEntrySearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.profile.profileentry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code ProfileEntrySearchOrder}.
 */

public abstract class AbstractProfileEntrySearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipSearchOrder
    implements org.osid.profile.ProfileEntrySearchOrder {

    private final java.util.Collection<org.osid.profile.records.ProfileEntrySearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by the implicit 
     *  status. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByImplicit(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByResource(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a <code> Resource </code> is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getResourceSearchOrder() {
        throw new org.osid.UnimplementedException("supportsResourceSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the agent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAgent(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an <code> Agent </code> is available. 
     *
     *  @return <code> true </code> if an agent search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the agent search order. 
     *
     *  @return the agent search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgentSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAgentSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the active 
     *  status. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByProfileItem(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a <code> ProfileItem </code> is available. 
     *
     *  @return <code> true </code> if a profile item search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileItemSearchOrder() {
        return (false);
    }


    /**
     *  Gets the profil eitem search order. 
     *
     *  @return the profile item search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemSearchOrder getProfileItemSearchOrder() {
        throw new org.osid.UnimplementedException("supportsProfileItemSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  profileEntryRecordType a profile entry record type 
     *  @return {@code true} if the profileEntryRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code profileEntryRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type profileEntryRecordType) {
        for (org.osid.profile.records.ProfileEntrySearchOrderRecord record : this.records) {
            if (record.implementsRecordType(profileEntryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  profileEntryRecordType the profile entry record type 
     *  @return the profile entry search order record
     *  @throws org.osid.NullArgumentException
     *          {@code profileEntryRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(profileEntryRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.profile.records.ProfileEntrySearchOrderRecord getProfileEntrySearchOrderRecord(org.osid.type.Type profileEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.profile.records.ProfileEntrySearchOrderRecord record : this.records) {
            if (record.implementsRecordType(profileEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(profileEntryRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this profile entry. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param profileEntryRecord the profile entry search odrer record
     *  @param profileEntryRecordType profile entry record type
     *  @throws org.osid.NullArgumentException
     *          {@code profileEntryRecord} or
     *          {@code profileEntryRecordTypeprofileEntry} is
     *          {@code null}
     */
            
    protected void addProfileEntryRecord(org.osid.profile.records.ProfileEntrySearchOrderRecord profileEntrySearchOrderRecord, 
                                     org.osid.type.Type profileEntryRecordType) {

        addRecordType(profileEntryRecordType);
        this.records.add(profileEntrySearchOrderRecord);
        
        return;
    }
}
