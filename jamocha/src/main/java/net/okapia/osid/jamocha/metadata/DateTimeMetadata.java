//
// DateTimeMetadata.java
//
//     Defines a datetime Metadata.
//
//
// Tom Coppeto
// Okapia
// 12 January 2023
//
//
// Copyright (c) 2023 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metadata;


/**
 *  Defines a datetime Metadata.
 */

public final class DateTimeMetadata
    extends net.okapia.osid.jamocha.metadata.spi.AbstractDateTimeMetadata
    implements org.osid.Metadata {


    /**
     *  Constructs a new single unlinked {@code DateTimeMetadata}.
     *
     *  @param elementId the Id of the element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public DateTimeMetadata(org.osid.id.Id elementId) {
        super(elementId);
        return;
    }


    /**
     *  Constructs a new unlinked {@code DateTimeMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @param isLinked {@code true} if the element is linked to
     *         another element, {@code false} otherwise
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public DateTimeMetadata(org.osid.id.Id elementId, boolean isArray) {
        super(elementId, isArray, false);
        return;
    }


    /**
     *  Constructs a new {@code DateTimeMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @param isLinked {@code true} if the element is linked to
     *         another element, {@code false} otherwise
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public DateTimeMetadata(org.osid.id.Id elementId, boolean isArray, boolean isLinked) {
        super(elementId, isArray, isLinked);
        return;
    }
    

    /**
     *  Sets the element label.
     *
     *  @param label the new element label
     *  @throws org.osid.NullArgumentException {@code label} is {@code
     *          null}
     */

    public void setLabel(org.osid.locale.DisplayText label) {
        super.setLabel(label);
	return;
    }


    /**
     *  Sets the instructions.
     *
     *  @param instructions the new instructions
     *  @throws org.osid.NullArgumentException {@code instructions}
     *  	is {@code null}
     */

    public void setInstructions(org.osid.locale.DisplayText instructions) {
        super.setInstructions(instructions);
        return;
    }


    /**
     *  Sets the required flag.
     *
     *  @param required {@code true} if required, {@code false} if
     *         optional
     */

    public void setRequired(boolean required) {
        super.setRequired(required);
        return;
    }


    /**
     *  Sets the has value flag.
     *
     *  @param exists {@code true} if has existing value, {@code
     *         false} if no value exists
     */

    public void setValueExists(boolean exists) {
        super.setValueExists(exists);
        return;
    }


    /**
     *  Sets the read only flag.
     *
     *  @param readonly {@code true} if read only, {@code
     *         false} if can be updated
     */

    public void setReadOnly(boolean readonly) {
        super.setReadOnly(readonly);
        return;
    }


    /**
     *  Sets the units.
     *
     *	@param units the new units
     *  @throws org.osid.NullArgumentException {@code units}
     *          is {@code null}
     */

    public void setUnits(org.osid.locale.DisplayText units) {
        super.setUnits(units);
        return;
    }

    
    /**
     *  Sets the resolution.
     *
     *  @param resolution the smallest resolution allowed
     *  @throws org.osid.NullARgumentException {@code resolution} is
     *          {@code null}
     */

    public void setResolution(org.osid.calendaring.DateTimeResolution resolution) {
        super.setResolution(resolution);
        return;
    }


    /**
     *  Add support for a calendar type.
     *
     *  @param calendarType the type of calendar
     *  @throws org.osid.NullArgumentException {@code calendarType} is
     *          {@code null}
     */

    public void addCalendarType(org.osid.type.Type calendarType) {
        super.addCalendarType(calendarType);
        return;
    }


    /**
     *  Add support for a time type.
     *
     *  @param timeType the type of time
     *  @throws org.osid.NullArgumentException {@code timeType} is
     *          {@code null}
     */

    public void addTimeType(org.osid.type.Type timeType) {
        super.addTimeType(timeType);
        return;
    }


    /**
     *  Sets the min and max datetimes.
     *
     *  @param min the minimum value
     *  @param max the maximum value
     *  @throws org.osid.InvalidArgumentException {@code min} is
     *          greater than {@code max}
     *  @throws org.osid.NullArgumentException {@code min} or
     *          {@code max} is {@code null}
     */

    public void setDateTimeRange(org.osid.calendaring.DateTime min, org.osid.calendaring.DateTime max) {
        super.setDateTimeRange(min, max);
        return;
    }


    /**
     *  Sets the datetime set.
     *
     *  @param values a collection of accepted datetime values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    public void setDateTimeSet(java.util.Collection<org.osid.calendaring.DateTime> values) {
        super.setDateTimeSet(values);
        return;
    }


    /**
     *  Adds a collection of values to the datetime set.
     *
     *  @param values a collection of accepted datetime values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    public void addToDateTimeSet(java.util.Collection<org.osid.calendaring.DateTime> values) {
        super.addToDateTimeSet(values);
        return;
    }


    /**
     *  Adds a value to the datetime set.
     *
     *  @param value a datetime value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void addToDateTimeSet(org.osid.calendaring.DateTime value) {
        super.addToDateTimeSet(value);
        return;
    }


    /**
     *  Removes a value from the datetime set.
     *
     *  @param value a datetime value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void removeFromDateTimeSet(org.osid.calendaring.DateTime value) {
        super.removeFromDateTimeSet(value);
        return;
    }


    /**
     *  Clears the datetime set.
     */

    public void clearDateTimeSet() {
        super.clearDateTimeSet();
        return;
    }


    /**
     *  Sets the default date time set.
     *
     *  @param values a collection of default date time values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    public void setDefaultDateTimeValues(java.util.Collection<org.osid.calendaring.DateTime> values) {
        super.setDefaultDateTimeValues(values);
        return;
    }


    /**
     *  Adds a collection of default date time values.
     *
     *  @param values a collection of default date time values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    public void addDefaultDateTimeValues(java.util.Collection<org.osid.calendaring.DateTime> values) {
        super.addDefaultDateTimeValues(values);
        return;
    }


    /**
     *  Adds a default date time value.
     *
     *  @param value a date time value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void addDefaultDateTimeValue(org.osid.calendaring.DateTime value) {
        super.addDefaultDateTimeValue(value);
        return;
    }


    /**
     *  Removes a default date time value.
     *
     *  @param value a date time value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void removeDefaultDateTimeValue(org.osid.calendaring.DateTime value) {
        super.removeDefaultDateTimeValue(value);
        return;
    }


    /**
     *  Clears the default date time values.
     */

    public void clearDefaultDateTimeValues() {
        super.clearDefaultDateTimeValues();
        return;
    }


    /**
     *  Sets the existing date time set.
     *
     *  @param values a collection of existing date time values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    public void setExistingDateTimeValues(java.util.Collection<org.osid.calendaring.DateTime> values) {
        super.setExistingDateTimeValues(values);
        return;
    }


    /**
     *  Adds a collection of existing date time values.
     *
     *  @param values a collection of existing date time values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    public void addExistingDateTimeValues(java.util.Collection<org.osid.calendaring.DateTime> values) {
        super.addExistingDateTimeValues(values);
        return;
    }


    /**
     *  Adds a existing date time value.
     *
     *  @param value a date time value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void addExistingDateTimeValue(org.osid.calendaring.DateTime value) {
        super.addExistingDateTimeValue(value);
        return;
    }


    /**
     *  Removes a existing date time value.
     *
     *  @param value a date time value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void removeExistingDateTimeValue(org.osid.calendaring.DateTime value) {
        super.removeExistingDateTimeValue(value);
        return;
    }


    /**
     *  Clears the existing date time values.
     */

    public void clearExistingDateTimeValues() {
        super.clearExistingDateTimeValues();
        return;
    }    
}
