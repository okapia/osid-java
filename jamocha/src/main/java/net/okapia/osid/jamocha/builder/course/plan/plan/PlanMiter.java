//
// PlanMiter.java
//
//     Defines a Plan miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.plan.plan;


/**
 *  Defines a <code>Plan</code> miter for use with the builders.
 */

public interface PlanMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRelationshipMiter,
            org.osid.course.plan.Plan {


    /**
     *  Sets the syllabus.
     *
     *  @param syllabus a syllabus
     *  @throws org.osid.NullArgumentException <code>syllabus</code>
     *          is <code>null</code>
     */

    public void setSyllabus(org.osid.course.syllabus.Syllabus syllabus);


    /**
     *  Sets the course offering.
     *
     *  @param courseOffering a course offering
     *  @throws org.osid.NullArgumentException
     *          <code>courseOffering</code> is <code>null</code>
     */

    public void setCourseOffering(org.osid.course.CourseOffering courseOffering);


    /**
     *  Adds a module.
     *
     *  @param module a module
     *  @throws org.osid.NullArgumentException <code>module</code> is
     *          <code>null</code>
     */

    public void addModule(org.osid.course.syllabus.Module module);


    /**
     *  Sets all the modules.
     *
     *  @param modules a collection of modules
     *  @throws org.osid.NullArgumentException <code>modules</code> is
     *          <code>null</code>
     */

    public void setModules(java.util.Collection<org.osid.course.syllabus.Module> modules);


    /**
     *  Adds a Plan record.
     *
     *  @param record a plan record
     *  @param recordType the type of plan record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addPlanRecord(org.osid.course.plan.records.PlanRecord record, org.osid.type.Type recordType);
}       


