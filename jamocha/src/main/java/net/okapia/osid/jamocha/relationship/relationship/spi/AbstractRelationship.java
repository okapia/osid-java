//
// AbstractRelationship.java
//
//     Defines a Relationship.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.relationship.relationship.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Relationship</code>.
 */

public abstract class AbstractRelationship
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.relationship.Relationship {

    private org.osid.id.Id sourceId;
    private org.osid.id.Id destinationId;

    private final java.util.Collection<org.osid.relationship.records.RelationshipRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the from peer <code> Id </code> in this relationship. 
     *
     *  @return the peer 
     */

    @OSID @Override
    public org.osid.id.Id getSourceId() {
        return (this.sourceId);
    }


    /**
     *  Sets the source id.
     *
     *  @param sourceId a source id
     *  @throws org.osid.NullArgumentException
     *          <code>sourceId</code> is <code>null</code>
     */

    protected void setSourceId(org.osid.id.Id sourceId) {
        nullarg(sourceId, "source Id");
        this.sourceId = sourceId;
        return;
    }


    /**
     *  Gets the to peer <code> Id </code> in this relationship. 
     *
     *  @return the related peer 
     */

    @OSID @Override
    public org.osid.id.Id getDestinationId() {
        return (this.destinationId);
    }


    /**
     *  Sets the destination id.
     *
     *  @param destinationId a destination id
     *  @throws org.osid.NullArgumentException
     *          <code>destinationId</code> is <code>null</code>
     */

    protected void setDestinationId(org.osid.id.Id destinationId) {
        nullarg(destinationId, "destination Id");
        this.destinationId = destinationId;
        return;
    }


    /**
     *  Tests if this relationship supports the given record
     *  <code>Type</code>.
     *
     *  @param  relationshipRecordType a relationship record type 
     *  @return <code>true</code> if the relationshipRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type relationshipRecordType) {
        for (org.osid.relationship.records.RelationshipRecord record : this.records) {
            if (record.implementsRecordType(relationshipRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Relationship</code> record <code>Type</code>.
     *
     *  @param  relationshipRecordType the relationship record type 
     *  @return the relationship record 
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(relationshipRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.relationship.records.RelationshipRecord getRelationshipRecord(org.osid.type.Type relationshipRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.relationship.records.RelationshipRecord record : this.records) {
            if (record.implementsRecordType(relationshipRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(relationshipRecordType + " is not supported");
    }


    /**
     *  Adds a record to this relationship. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param relationshipRecord the relationship record
     *  @param relationshipRecordType relationship record type
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipRecord</code> or
     *          <code>relationshipRecordTyperelationship</code> is
     *          <code>null</code>
     */
            
    protected void addRelationshipRecord(org.osid.relationship.records.RelationshipRecord relationshipRecord, 
                                         org.osid.type.Type relationshipRecordType) {

        nullarg(relationshipRecord, "relationship record");
        addRecordType(relationshipRecordType);
        this.records.add(relationshipRecord);
        
        return;
    }
}
