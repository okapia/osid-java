//
// AbstractAgendaQuery.java
//
//     A template for making an Agenda Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.rules.check.agenda.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for agendas.
 */

public abstract class AbstractAgendaQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.rules.check.AgendaQuery {

    private final java.util.Collection<org.osid.rules.check.records.AgendaQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the instruction <code> Id </code> for this query. 
     *
     *  @param  qualifierId the instruction <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> instructionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchInstructionId(org.osid.id.Id qualifierId, boolean match) {
        return;
    }


    /**
     *  Clears the instruction <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearInstructionIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> InstructionQuery </code> is available. 
     *
     *  @return <code> true </code> if an instruction query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstructionQuery() {
        return (false);
    }


    /**
     *  Gets the query for an instruction. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the instruction query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionQuery getInstructionQuery() {
        throw new org.osid.UnimplementedException("supportsInstructionQuery() is false");
    }


    /**
     *  Matches agendas that have any instruction. 
     *
     *  @param  match <code> true </code> to match agendas with any 
     *          instruction, <code> false </code> to match agendas with no 
     *          instruction 
     */

    @OSID @Override
    public void matchAnyInstruction(boolean match) {
        return;
    }


    /**
     *  Clears the instruction query terms. 
     */

    @OSID @Override
    public void clearInstructionTerms() {
        return;
    }


    /**
     *  Sets the engine <code> Id </code> for this query to match agendas 
     *  assigned to foundries. 
     *
     *  @param  engineId the engine <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEngineId(org.osid.id.Id engineId, boolean match) {
        return;
    }


    /**
     *  Clears the engine <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearEngineIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> EngineQuery </code> is available. 
     *
     *  @return <code> true </code> if an engine query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEngineQuery() {
        return (false);
    }


    /**
     *  Gets the query for an engine. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the engine query 
     *  @throws org.osid.UnimplementedException <code> supportsEngineQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.EngineQuery getEngineQuery() {
        throw new org.osid.UnimplementedException("supportsEngineQuery() is false");
    }


    /**
     *  Clears the engine query terms. 
     */

    @OSID @Override
    public void clearEngineTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given agenda query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an agenda implementing the requested record.
     *
     *  @param agendaRecordType an agenda record type
     *  @return the agenda query record
     *  @throws org.osid.NullArgumentException
     *          <code>agendaRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(agendaRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.rules.check.records.AgendaQueryRecord getAgendaQueryRecord(org.osid.type.Type agendaRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.rules.check.records.AgendaQueryRecord record : this.records) {
            if (record.implementsRecordType(agendaRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(agendaRecordType + " is not supported");
    }


    /**
     *  Adds a record to this agenda query. 
     *
     *  @param agendaQueryRecord agenda query record
     *  @param agendaRecordType agenda record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAgendaQueryRecord(org.osid.rules.check.records.AgendaQueryRecord agendaQueryRecord, 
                                          org.osid.type.Type agendaRecordType) {

        addRecordType(agendaRecordType);
        nullarg(agendaQueryRecord, "agenda query record");
        this.records.add(agendaQueryRecord);        
        return;
    }
}
