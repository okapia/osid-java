//
// AbstractAssemblyAuctionQuery.java
//
//     An AuctionQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.bidding.auction.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An AuctionQuery that stores terms.
 */

public abstract class AbstractAssemblyAuctionQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidGovernatorQuery
    implements org.osid.bidding.AuctionQuery,
               org.osid.bidding.AuctionQueryInspector,
               org.osid.bidding.AuctionSearchOrder {

    private final java.util.Collection<org.osid.bidding.records.AuctionQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.bidding.records.AuctionQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.bidding.records.AuctionSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();

    private final AssemblyOsidTemporalQuery query;


    /** 
     *  Constructs a new <code>AbstractAssemblyAuctionQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyAuctionQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        this.query = new AssemblyOsidTemporalQuery(assembler);
        return;
    }
    

    /**
     *  Match effective objects where the current date falls within
     *  the start and end dates inclusive.
     *
     *  @param  match <code> true </code> to match any effective, <code> false 
     *          </code> to match ineffective 
     */

    @OSID @Override
    public void matchEffective(boolean match) {
        this.query.matchEffective(match);
        return;
    }


    /**
     *  Clears the effective query terms. 
     */

    @OSID @Override
    public void clearEffectiveTerms() {
        this.query.clearEffectiveTerms();
        return;
    }


    /**
     *  Gets the effective query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getEffectiveTerms() {
        return (this.query.getEffectiveTerms());
    }


    /**
     *  Specifies a preference for ordering the result set by the effective 
     *  status. 
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByEffective(org.osid.SearchOrderStyle style) {
        this.query.orderByEffective(style);
        return;
    }


    /**
     *  Gets the column name for the effective field.
     *
     *  @return the column name
     */

    protected String getEffectiveColumn() {
        return (this.query.getEffectiveColumn());
    }    


    /**
     *  Matches temporals whose start date falls in between the given
     *  dates inclusive.
     *
     *  @param  start start of date range 
     *  @param  end end of date range 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is less 
     *          than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchStartDate(org.osid.calendaring.DateTime start, 
                               org.osid.calendaring.DateTime end, 
                               boolean match) {
        this.query.matchStartDate(start, end, match);
        return;
    }


    /**
     *  Matches temporals with any start date set. 
     *
     *  @param  match <code> true </code> to match any start date, <code> 
     *          false </code> to match no start date 
     */

    @OSID @Override
    public void matchAnyStartDate(boolean match) {
        this.query.matchAnyStartDate(match);
        return;
    }


    /**
     *  Clears the start date query terms. 
     */

    @OSID @Override
    public void clearStartDateTerms() {
        this.query.clearStartDateTerms();
        return;
    }


    /**
     *  Gets the start date query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getStartDateTerms() {
        return (this.query.getStartDateTerms());
    }


    /**
     *  Specifies a preference for ordering the result set by the start date. 
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStartDate(org.osid.SearchOrderStyle style) {
        this.query.orderByStartDate(style);
        return;
    }


    /**
     *  Gets the column name for the start date field.
     *
     *  @return the column name
     */

    protected String getStartDateColumn() {
        return (this.query.getStartDateColumn());
    }    


    /**
     *  Matches temporals whose effective end date falls in between
     *  the given dates inclusive.
     *
     *  @param  start start of date range 
     *  @param  end end of date range 
     *  @param match <code> true </code> if a positive match, <code>
     *          false </code> for negative match
     *  @throws org.osid.InvalidArgumentException <code> start </code>
     *          is less than <code> end </code>
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchEndDate(org.osid.calendaring.DateTime start, 
                             org.osid.calendaring.DateTime end, boolean match) {

        this.query.matchEndDate(start, end, match);
        return;
    }


    /**
     *  Matches temporals with any end date set. 
     *
     *  @param  match <code> true </code> to match any end date, <code> false 
     *          </code> to match no start date 
     */

    @OSID @Override
    public void matchAnyEndDate(boolean match) {
        this.query.matchAnyEndDate(match);
        return;
    }


    /**
     *  Clears the end date query terms. 
     */

    @OSID @Override
    public void clearEndDateTerms() {
        this.query.clearEndDateTerms();
        return;
    }


    /**
     *  Gets the end date query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getEndDateTerms() {
        return (this.query.getEndDateTerms());
    }


    
    /**
     *  Specifies a preference for ordering the result set by the end date. 
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByEndDate(org.osid.SearchOrderStyle style) {
        this.query.orderByEndDate(style);
        return;
    }


    /**
     *  Gets the column name for the end date field.
     *
     *  @return the column name
     */

    protected String getEndDateColumn() {
        return (this.query.getEndDateColumn());
    }    


    /**
     *  Matches temporals where the given date range falls entirely between 
     *  the start and end dates inclusive. 
     *
     *  @param  from start date 
     *  @param  to end date 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is less 
     *          than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDate(org.osid.calendaring.DateTime from, 
                          org.osid.calendaring.DateTime to, boolean match) {

        this.query.matchDate(from, to, match);
        return;
    }


    /**
     *  Clears the date query terms. 
     */

    @OSID @Override
    public void clearDateTerms() {
        this.query.clearDateTerms();
        return;
    }

    
    /**
     *  Gets the date query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getDateTerms() {
        return (this.query.getDateTerms());
    }


    /**
     *  Gets the column name for the date field.
     *
     *  @return the column name
     */

    protected String getDateColumn() {
        return (this.query.getDateColumn());
    }    


    /**
     *  Matches the currency type. 
     *
     *  @param  currencyType a currency type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> currencyType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCurrencyType(org.osid.type.Type currencyType, 
                                  boolean match) {
        getAssembler().addTypeTerm(getCurrencyTypeColumn(), currencyType, match);
        return;
    }


    /**
     *  Matches auctions with any currency type set. 
     *
     *  @param  match <code> true </code> to match auctions with any currency 
     *          type, <code> false </code> to match auctions with no currency 
     *          type 
     */

    @OSID @Override
    public void matchAnyCurrencyType(boolean match) {
        getAssembler().addTypeWildcardTerm(getCurrencyTypeColumn(), match);
        return;
    }


    /**
     *  Clears the currency type terms. 
     */

    @OSID @Override
    public void clearCurrencyTypeTerms() {
        getAssembler().clearTerms(getCurrencyTypeColumn());
        return;
    }


    /**
     *  Gets the currency type query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getCurrencyTypeTerms() {
        return (getAssembler().getTypeTerms(getCurrencyTypeColumn()));
    }


    /**
     *  Orders the results by the currency type. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCurrencyType(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCurrencyTypeColumn(), style);
        return;
    }


    /**
     *  Gets the CurrencyType column name.
     *
     * @return the column name
     */

    protected String getCurrencyTypeColumn() {
        return ("currency_type");
    }


    /**
     *  Matches the minimum bidders between the given range inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     */

    @OSID @Override
    public void matchMinimumBidders(long start, long end, boolean match) {
        getAssembler().addCardinalRangeTerm(getMinimumBiddersColumn(), start, end, match);
        return;
    }


    /**
     *  Matches auctions with any minimum bidders set. 
     *
     *  @param  match <code> true </code> to match auctions with any minimum 
     *          bidders, <code> false </code> to match auctions with no 
     *          minimum bidders 
     */

    @OSID @Override
    public void matchAnyMinimumBidders(boolean match) {
        getAssembler().addCardinalRangeWildcardTerm(getMinimumBiddersColumn(), match);
        return;
    }


    /**
     *  Clears the minimum bidders terms. 
     */

    @OSID @Override
    public void clearMinimumBiddersTerms() {
        getAssembler().clearTerms(getMinimumBiddersColumn());
        return;
    }


    /**
     *  Gets the minimum bidders query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getMinimumBiddersTerms() {
        return (getAssembler().getCardinalRangeTerms(getMinimumBiddersColumn()));
    }


    /**
     *  Orders the results by the minimum bidders. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMinimumBidders(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getMinimumBiddersColumn(), style);
        return;
    }


    /**
     *  Gets the MinimumBidders column name.
     *
     * @return the column name
     */

    protected String getMinimumBiddersColumn() {
        return ("minimum_bidders");
    }


    /**
     *  Matches sealed auctions. 
     *
     *  @param  match <code> true </code> to match sealed auctions, <code> 
     *          false </code> to match visible bid auctions 
     */

    @OSID @Override
    public void matchSealed(boolean match) {
        getAssembler().addBooleanTerm(getSealedColumn(), match);
        return;
    }


    /**
     *  Clears the sealed terms. 
     */

    @OSID @Override
    public void clearSealedTerms() {
        getAssembler().clearTerms(getSealedColumn());
        return;
    }


    /**
     *  Gets the sealed auctions query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getSealedTerms() {
        return (getAssembler().getBooleanTerms(getSealedColumn()));
    }


    /**
     *  Orders the results by the sealed flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySealed(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSealedColumn(), style);
        return;
    }


    /**
     *  Gets the Sealed column name.
     *
     * @return the column name
     */

    protected String getSealedColumn() {
        return ("sealed");
    }


    /**
     *  Sets the resource <code> Id </code> for this query. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSellerId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getSellerIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the resource <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSellerIdTerms() {
        getAssembler().clearTerms(getSellerIdColumn());
        return;
    }


    /**
     *  Gets the seller <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSellerIdTerms() {
        return (getAssembler().getIdTerms(getSellerIdColumn()));
    }


    /**
     *  Orders the results by seller. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySeller(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSellerColumn(), style);
        return;
    }


    /**
     *  Gets the SellerId column name.
     *
     * @return the column name
     */

    protected String getSellerIdColumn() {
        return ("seller_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSellerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsSellerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getSellerQuery() {
        throw new org.osid.UnimplementedException("supportsSellerQuery() is false");
    }


    /**
     *  Matches auctions with any seller set. 
     *
     *  @param  match <code> true </code> to match auctions with any seller, 
     *          <code> false </code> to match auctions with no seller 
     */

    @OSID @Override
    public void matchAnySeller(boolean match) {
        getAssembler().addIdWildcardTerm(getSellerColumn(), match);
        return;
    }


    /**
     *  Clears the seller query terms. 
     */

    @OSID @Override
    public void clearSellerTerms() {
        getAssembler().clearTerms(getSellerColumn());
        return;
    }


    /**
     *  Gets the seller query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getSellerTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSellerSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsSellerSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getSellerSearchOrder() {
        throw new org.osid.UnimplementedException("supportsSellerSearchOrder() is false");
    }


    /**
     *  Gets the Seller column name.
     *
     * @return the column name
     */

    protected String getSellerColumn() {
        return ("seller");
    }


    /**
     *  Sets the resource <code> Id </code> for this query. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchItemId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getItemIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the resource <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearItemIdTerms() {
        getAssembler().clearTerms(getItemIdColumn());
        return;
    }


    /**
     *  Gets the item <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getItemIdTerms() {
        return (getAssembler().getIdTerms(getItemIdColumn()));
    }


    /**
     *  Orders the results by item. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByItem(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getItemColumn(), style);
        return;
    }


    /**
     *  Gets the ItemId column name.
     *
     * @return the column name
     */

    protected String getItemIdColumn() {
        return ("item_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsItemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getItemQuery() {
        throw new org.osid.UnimplementedException("supportsItemQuery() is false");
    }


    /**
     *  Matches auctions with any item set. 
     *
     *  @param  match <code> true </code> to match auctions with any item, 
     *          <code> false </code> to match auctions with no item 
     */

    @OSID @Override
    public void matchAnyItem(boolean match) {
        getAssembler().addIdWildcardTerm(getItemColumn(), match);
        return;
    }


    /**
     *  Clears the seller query terms. 
     */

    @OSID @Override
    public void clearItemTerms() {
        getAssembler().clearTerms(getItemColumn());
        return;
    }


    /**
     *  Gets the item query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getItemTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsItemSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getItemSearchOrder() {
        throw new org.osid.UnimplementedException("supportsItemSearchOrder() is false");
    }


    /**
     *  Gets the Item column name.
     *
     * @return the column name
     */

    protected String getItemColumn() {
        return ("item");
    }


    /**
     *  Matches the lot size between the given range inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     */

    @OSID @Override
    public void matchLotSize(long start, long end, boolean match) {
        getAssembler().addCardinalRangeTerm(getLotSizeColumn(), start, end, match);
        return;
    }


    /**
     *  Matches auctions with any lot size set. 
     *
     *  @param  match <code> true </code> to match auctions with any lot size, 
     *          <code> false </code> to match auctions with no lot size set 
     */

    @OSID @Override
    public void matchAnyLotSize(boolean match) {
        getAssembler().addCardinalRangeWildcardTerm(getLotSizeColumn(), match);
        return;
    }


    /**
     *  Clears the lot size terms. 
     */

    @OSID @Override
    public void clearLotSizeTerms() {
        getAssembler().clearTerms(getLotSizeColumn());
        return;
    }


    /**
     *  Gets the lot size query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getLotSizeTerms() {
        return (getAssembler().getCardinalRangeTerms(getLotSizeColumn()));
    }


    /**
     *  Orders the results by lot size. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByLotSize(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getLotSizeColumn(), style);
        return;
    }


    /**
     *  Gets the LotSize column name.
     *
     * @return the column name
     */

    protected String getLotSizeColumn() {
        return ("lot_size");
    }


    /**
     *  Matches the remaining items between the given range inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     */

    @OSID @Override
    public void matchRemainingItems(long start, long end, boolean match) {
        getAssembler().addCardinalRangeTerm(getRemainingItemsColumn(), start, end, match);
        return;
    }


    /**
     *  Clears the remaining items terms. 
     */

    @OSID @Override
    public void clearRemainingItemsTerms() {
        getAssembler().clearTerms(getRemainingItemsColumn());
        return;
    }


    /**
     *  Gets the remaining items query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getRemainingItemsTerms() {
        return (getAssembler().getCardinalRangeTerms(getRemainingItemsColumn()));
    }


    /**
     *  Orders the results by remaining items. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRemainingItems(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRemainingItemsColumn(), style);
        return;
    }


    /**
     *  Gets the RemainingItems column name.
     *
     * @return the column name
     */

    protected String getRemainingItemsColumn() {
        return ("remaining_items");
    }


    /**
     *  Matches the item limit between the given range inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     */

    @OSID @Override
    public void matchItemLimit(long start, long end, boolean match) {
        getAssembler().addCardinalRangeTerm(getItemLimitColumn(), start, end, match);
        return;
    }


    /**
     *  Matches auctions with any item limit. 
     *
     *  @param  match <code> true </code> to match auctions with any item 
     *          limit, <code> false </code> to match auctions with no item 
     *          limit 
     */

    @OSID @Override
    public void matchAnyItemLimit(boolean match) {
        getAssembler().addCardinalRangeWildcardTerm(getItemLimitColumn(), match);
        return;
    }


    /**
     *  Clears the item limit terms. 
     */

    @OSID @Override
    public void clearItemLimitTerms() {
        getAssembler().clearTerms(getItemLimitColumn());
        return;
    }


    /**
     *  Gets the items limit query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getItemLimitTerms() {
        return (getAssembler().getCardinalRangeTerms(getItemLimitColumn()));
    }


    /**
     *  Orders the results by the auction item limit. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByItemLimit(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getItemLimitColumn(), style);
        return;
    }


    /**
     *  Gets the ItemLimit column name.
     *
     * @return the column name
     */

    protected String getItemLimitColumn() {
        return ("item_limit");
    }


    /**
     *  Matches the starting price between the given range inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> or <code> 
     *          start.getCurencyType() </code> != <code> end.getCurrencyType() 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchStartingPrice(org.osid.financials.Currency start, 
                                   org.osid.financials.Currency end, 
                                   boolean match) {
        getAssembler().addCurrencyRangeTerm(getStartingPriceColumn(), start, end, match);
        return;
    }


    /**
     *  Matches auctions with any starting price. 
     *
     *  @param  match <code> true </code> to match auctions with any starting 
     *          price, <code> false </code> to match auctions with no starting 
     *          price 
     */

    @OSID @Override
    public void matchAnyStartingPrice(boolean match) {
        getAssembler().addCurrencyRangeWildcardTerm(getStartingPriceColumn(), match);
        return;
    }


    /**
     *  Clears the starting price terms. 
     */

    @OSID @Override
    public void clearStartingPriceTerms() {
        getAssembler().clearTerms(getStartingPriceColumn());
        return;
    }


    /**
     *  Gets the starting price query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getStartingPriceTerms() {
        return (getAssembler().getCurrencyRangeTerms(getStartingPriceColumn()));
    }


    /**
     *  Orders the results by the starting price. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStartingPrice(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getStartingPriceColumn(), style);
        return;
    }


    /**
     *  Gets the StartingPrice column name.
     *
     * @return the column name
     */

    protected String getStartingPriceColumn() {
        return ("starting_price");
    }


    /**
     *  Matches the price increment between the given range inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> or <code> 
     *          start.getCurencyType() </code> != <code> end.getCurrencyType() 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchPriceIncrement(org.osid.financials.Currency start, 
                                    org.osid.financials.Currency end, 
                                    boolean match) {
        getAssembler().addCurrencyRangeTerm(getPriceIncrementColumn(), start, end, match);
        return;
    }


    /**
     *  Matches auctions with any price increment. 
     *
     *  @param  match <code> true </code> to match auctions with any price 
     *          increment, <code> false </code> to match auctions with no 
     *          price increment 
     */

    @OSID @Override
    public void matchAnyPriceIncrement(boolean match) {
        getAssembler().addCurrencyRangeWildcardTerm(getPriceIncrementColumn(), match);
        return;
    }


    /**
     *  Clears the price increment terms. 
     */

    @OSID @Override
    public void clearPriceIncrementTerms() {
        getAssembler().clearTerms(getPriceIncrementColumn());
        return;
    }


    /**
     *  Gets the price increment query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getPriceIncrementTerms() {
        return (getAssembler().getCurrencyRangeTerms(getPriceIncrementColumn()));
    }


    /**
     *  Orders the results by the price increment 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPriceIncrement(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getPriceIncrementColumn(), style);
        return;
    }


    /**
     *  Gets the PriceIncrement column name.
     *
     * @return the column name
     */

    protected String getPriceIncrementColumn() {
        return ("price_increment");
    }


    /**
     *  Matches the reserve price between the given range inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> or <code> 
     *          start.getCurencyType() </code> != <code> end.getCurrencyType() 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchReservePrice(org.osid.financials.Currency start, 
                                  org.osid.financials.Currency end, 
                                  boolean match) {
        getAssembler().addCurrencyRangeTerm(getReservePriceColumn(), start, end, match);
        return;
    }


    /**
     *  Matches auctions with any reserve price. 
     *
     *  @param  match <code> true </code> to match auctions with any reserve 
     *          price, <code> false </code> to match auctions with no reserve 
     *          price 
     */

    @OSID @Override
    public void matchAnyReservePrice(boolean match) {
        getAssembler().addCurrencyRangeWildcardTerm(getReservePriceColumn(), match);
        return;
    }


    /**
     *  Clears the reserve price terms. 
     */

    @OSID @Override
    public void clearReservePriceTerms() {
        getAssembler().clearTerms(getReservePriceColumn());
        return;
    }


    /**
     *  Gets the reserve price query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getReservePriceTerms() {
        return (getAssembler().getCurrencyRangeTerms(getReservePriceColumn()));
    }


    /**
     *  Orders the results by the reserve price. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByReservePrice(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getReservePriceColumn(), style);
        return;
    }


    /**
     *  Gets the ReservePrice column name.
     *
     * @return the column name
     */

    protected String getReservePriceColumn() {
        return ("reserve_price");
    }


    /**
     *  Matches the buyout price between the given range inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> or <code> 
     *          start.getCurencyType() </code> != <code> end.getCurrencyType() 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchBuyoutPrice(org.osid.financials.Currency start, 
                                 org.osid.financials.Currency end, 
                                 boolean match) {
        getAssembler().addCurrencyRangeTerm(getBuyoutPriceColumn(), start, end, match);
        return;
    }


    /**
     *  Matches auctions with any buyout price. 
     *
     *  @param  match <code> true </code> to match auctions with any buyout 
     *          price, <code> false </code> to match auctions with no buyout 
     *          price 
     */

    @OSID @Override
    public void matchAnyBuyoutPrice(boolean match) {
        getAssembler().addCurrencyRangeWildcardTerm(getBuyoutPriceColumn(), match);
        return;
    }


    /**
     *  Clears the buyout price terms. 
     */

    @OSID @Override
    public void clearBuyoutPriceTerms() {
        getAssembler().clearTerms(getBuyoutPriceColumn());
        return;
    }


    /**
     *  Gets the buyout price query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getBuyoutPriceTerms() {
        return (getAssembler().getCurrencyRangeTerms(getBuyoutPriceColumn()));
    }


    /**
     *  Orders the results by the buyout price. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBuyoutPrice(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getBuyoutPriceColumn(), style);
        return;
    }


    /**
     *  Gets the BuyoutPrice column name.
     *
     * @return the column name
     */

    protected String getBuyoutPriceColumn() {
        return ("buyout_price");
    }


    /**
     *  Sets the bid <code> Id </code> for this query. 
     *
     *  @param  bidId the bid <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> bidId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchBidId(org.osid.id.Id bidId, boolean match) {
        getAssembler().addIdTerm(getBidIdColumn(), bidId, match);
        return;
    }


    /**
     *  Clears the bid <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearBidIdTerms() {
        getAssembler().clearTerms(getBidIdColumn());
        return;
    }


    /**
     *  Gets the bid <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBidIdTerms() {
        return (getAssembler().getIdTerms(getBidIdColumn()));
    }


    /**
     *  Gets the BidId column name.
     *
     * @return the column name
     */

    protected String getBidIdColumn() {
        return ("bid_id");
    }


    /**
     *  Tests if a <code> BidQuery </code> is available. 
     *
     *  @return <code> true </code> if a bid query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBidQuery() {
        return (false);
    }


    /**
     *  Gets the query for a bid. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the bid query 
     *  @throws org.osid.UnimplementedException <code> supportsBidQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidQuery getBidQuery() {
        throw new org.osid.UnimplementedException("supportsBidQuery() is false");
    }


    /**
     *  Clears the bid query terms. 
     */

    @OSID @Override
    public void clearBidTerms() {
        getAssembler().clearTerms(getBidColumn());
        return;
    }


    /**
     *  Gets the bid query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.bidding.BidQueryInspector[] getBidTerms() {
        return (new org.osid.bidding.BidQueryInspector[0]);
    }


    /**
     *  Gets the Bid column name.
     *
     * @return the column name
     */

    protected String getBidColumn() {
        return ("bid");
    }


    /**
     *  Sets the auction house <code> Id </code> for this query to match 
     *  auctions assigned to auction houses. 
     *
     *  @param  auctionHouseId the auction house <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchAuctionHouseId(org.osid.id.Id auctionHouseId, 
                                    boolean match) {
        getAssembler().addIdTerm(getAuctionHouseIdColumn(), auctionHouseId, match);
        return;
    }


    /**
     *  Clears the auction house <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAuctionHouseIdTerms() {
        getAssembler().clearTerms(getAuctionHouseIdColumn());
        return;
    }


    /**
     *  Gets the auction house <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAuctionHouseIdTerms() {
        return (getAssembler().getIdTerms(getAuctionHouseIdColumn()));
    }


    /**
     *  Gets the AuctionHouseId column name.
     *
     * @return the column name
     */

    protected String getAuctionHouseIdColumn() {
        return ("auction_house_id");
    }


    /**
     *  Tests if an <code> AuctionHouseQuery </code> is available. 
     *
     *  @return <code> true </code> if a auction house query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionHouseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a auction house. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the auction house query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionHouseQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseQuery getAuctionHouseQuery() {
        throw new org.osid.UnimplementedException("supportsAuctionHouseQuery() is false");
    }


    /**
     *  Clears the auction house query terms. 
     */

    @OSID @Override
    public void clearAuctionHouseTerms() {
        getAssembler().clearTerms(getAuctionHouseColumn());
        return;
    }


    /**
     *  Gets the auction house query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseQueryInspector[] getAuctionHouseTerms() {
        return (new org.osid.bidding.AuctionHouseQueryInspector[0]);
    }


    /**
     *  Gets the AuctionHouse column name.
     *
     * @return the column name
     */

    protected String getAuctionHouseColumn() {
        return ("auction_house");
    }


    /**
     *  Tests if this auction supports the given record
     *  <code>Type</code>.
     *
     *  @param  auctionRecordType an auction record type 
     *  @return <code>true</code> if the auctionRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>auctionRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type auctionRecordType) {
        for (org.osid.bidding.records.AuctionQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(auctionRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  auctionRecordType the auction record type 
     *  @return the auction query record 
     *  @throws org.osid.NullArgumentException
     *          <code>auctionRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(auctionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.records.AuctionQueryRecord getAuctionQueryRecord(org.osid.type.Type auctionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.bidding.records.AuctionQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(auctionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(auctionRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  auctionRecordType the auction record type 
     *  @return the auction query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>auctionRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(auctionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.records.AuctionQueryInspectorRecord getAuctionQueryInspectorRecord(org.osid.type.Type auctionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.bidding.records.AuctionQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(auctionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(auctionRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param auctionRecordType the auction record type
     *  @return the auction search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>auctionRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(auctionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.records.AuctionSearchOrderRecord getAuctionSearchOrderRecord(org.osid.type.Type auctionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.bidding.records.AuctionSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(auctionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(auctionRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this auction. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param auctionQueryRecord the auction query record
     *  @param auctionQueryInspectorRecord the auction query inspector
     *         record
     *  @param auctionSearchOrderRecord the auction search order record
     *  @param auctionRecordType auction record type
     *  @throws org.osid.NullArgumentException
     *          <code>auctionQueryRecord</code>,
     *          <code>auctionQueryInspectorRecord</code>,
     *          <code>auctionSearchOrderRecord</code> or
     *          <code>auctionRecordTypeauction</code> is
     *          <code>null</code>
     */
            
    protected void addAuctionRecords(org.osid.bidding.records.AuctionQueryRecord auctionQueryRecord, 
                                      org.osid.bidding.records.AuctionQueryInspectorRecord auctionQueryInspectorRecord, 
                                      org.osid.bidding.records.AuctionSearchOrderRecord auctionSearchOrderRecord, 
                                      org.osid.type.Type auctionRecordType) {

        addRecordType(auctionRecordType);

        nullarg(auctionQueryRecord, "auction query record");
        nullarg(auctionQueryInspectorRecord, "auction query inspector record");
        nullarg(auctionSearchOrderRecord, "auction search odrer record");

        this.queryRecords.add(auctionQueryRecord);
        this.queryInspectorRecords.add(auctionQueryInspectorRecord);
        this.searchOrderRecords.add(auctionSearchOrderRecord);
        
        return;
    }


    protected class AssemblyOsidTemporalQuery
        extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidTemporalQuery
        implements org.osid.OsidTemporalQuery,
                   org.osid.OsidTemporalQueryInspector,
                   org.osid.OsidTemporalSearchOrder {
        
        
        /** 
         *  Constructs a new <code>AssemblyOsidTemporalQuery</code>.
         *
         *  @param assembler the query assembler
         *  @throws org.osid.NullArgumentException <code>assembler</code>
         *          is <code>null</code>
         */
        
        protected AssemblyOsidTemporalQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
            super(assembler);
            return;
        }


        /**
         *  Gets the column name for the effective field.
         *
         *  @return the column name
         */
        
        protected String getEffectiveColumn() {
            return (super.getEffectiveColumn());
        }    


        /**
         *  Gets the column name for the start date field.
         *
         *  @return the column name
         */
        
        protected String getStartDateColumn() {
            return (super.getStartDateColumn());
        }    


        /**
         *  Gets the column name for the end date field.
         *
         *  @return the column name
         */
        
        protected String getEndDateColumn() {
            return (super.getEndDateColumn());
        }    


        /**
         *  Gets the column name for the date field.
         *
         *  @return the column name
         */
        
        protected String getDateColumn() {
            return (super.getDateColumn());
        }    
    }
}
