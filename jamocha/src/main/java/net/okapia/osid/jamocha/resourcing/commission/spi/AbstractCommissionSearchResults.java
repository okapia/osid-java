//
// AbstractCommissionSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.commission.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractCommissionSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.resourcing.CommissionSearchResults {

    private org.osid.resourcing.CommissionList commissions;
    private final org.osid.resourcing.CommissionQueryInspector inspector;
    private final java.util.Collection<org.osid.resourcing.records.CommissionSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractCommissionSearchResults.
     *
     *  @param commissions the result set
     *  @param commissionQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>commissions</code>
     *          or <code>commissionQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractCommissionSearchResults(org.osid.resourcing.CommissionList commissions,
                                            org.osid.resourcing.CommissionQueryInspector commissionQueryInspector) {
        nullarg(commissions, "commissions");
        nullarg(commissionQueryInspector, "commission query inspectpr");

        this.commissions = commissions;
        this.inspector = commissionQueryInspector;

        return;
    }


    /**
     *  Gets the commission list resulting from a search.
     *
     *  @return a commission list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.resourcing.CommissionList getCommissions() {
        if (this.commissions == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.resourcing.CommissionList commissions = this.commissions;
        this.commissions = null;
	return (commissions);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.resourcing.CommissionQueryInspector getCommissionQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  commission search record <code> Type. </code> This method must
     *  be used to retrieve a commission implementing the requested
     *  record.
     *
     *  @param commissionSearchRecordType a commission search 
     *         record type 
     *  @return the commission search
     *  @throws org.osid.NullArgumentException
     *          <code>commissionSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(commissionSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.CommissionSearchResultsRecord getCommissionSearchResultsRecord(org.osid.type.Type commissionSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.resourcing.records.CommissionSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(commissionSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(commissionSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record commission search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addCommissionRecord(org.osid.resourcing.records.CommissionSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "commission record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
