//
// AbstractQueueConstrainerLookupSession.java
//
//    A starter implementation framework for providing a QueueConstrainer
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.tracking.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a QueueConstrainer
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getQueueConstrainers(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractQueueConstrainerLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.tracking.rules.QueueConstrainerLookupSession {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean federated     = false;
    private org.osid.tracking.FrontOffice frontOffice = new net.okapia.osid.jamocha.nil.tracking.frontoffice.UnknownFrontOffice();
    

    /**
     *  Gets the <code>FrontOffice/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>FrontOffice Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFrontOfficeId() {
        return (this.frontOffice.getId());
    }


    /**
     *  Gets the <code>FrontOffice</code> associated with this 
     *  session.
     *
     *  @return the <code>FrontOffice</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOffice getFrontOffice()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.frontOffice);
    }


    /**
     *  Sets the <code>FrontOffice</code>.
     *
     *  @param  frontOffice the front office for this session
     *  @throws org.osid.NullArgumentException <code>frontOffice</code>
     *          is <code>null</code>
     */

    protected void setFrontOffice(org.osid.tracking.FrontOffice frontOffice) {
        nullarg(frontOffice, "front office");
        this.frontOffice = frontOffice;
        return;
    }


    /**
     *  Tests if this user can perform <code>QueueConstrainer</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupQueueConstrainers() {
        return (true);
    }


    /**
     *  A complete view of the <code>QueueConstrainer</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeQueueConstrainerView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>QueueConstrainer</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryQueueConstrainerView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include queue constrainers in front offices which are children
     *  of this front office in the front office hierarchy.
     */

    @OSID @Override
    public void useFederatedFrontOfficeView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this front office only.
     */

    @OSID @Override
    public void useIsolatedFrontOfficeView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active queue constrainers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveQueueConstrainerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive queue constrainers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusQueueConstrainerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>QueueConstrainer</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>QueueConstrainer</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>QueueConstrainer</code> and
     *  retained for compatibility.
     *
     *  In active mode, queue constrainers are returned that are currently
     *  active. In any status mode, active and inactive queue constrainers
     *  are returned.
     *
     *  @param  queueConstrainerId <code>Id</code> of the
     *          <code>QueueConstrainer</code>
     *  @return the queue constrainer
     *  @throws org.osid.NotFoundException <code>queueConstrainerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>queueConstrainerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainer getQueueConstrainer(org.osid.id.Id queueConstrainerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.tracking.rules.QueueConstrainerList queueConstrainers = getQueueConstrainers()) {
            while (queueConstrainers.hasNext()) {
                org.osid.tracking.rules.QueueConstrainer queueConstrainer = queueConstrainers.getNextQueueConstrainer();
                if (queueConstrainer.getId().equals(queueConstrainerId)) {
                    return (queueConstrainer);
                }
            }
        } 

        throw new org.osid.NotFoundException(queueConstrainerId + " not found");
    }


    /**
     *  Gets a <code>QueueConstrainerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  queueConstrainers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>QueueConstrainers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, queue constrainers are returned that are currently
     *  active. In any status mode, active and inactive queue constrainers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getQueueConstrainers()</code>.
     *
     *  @param  queueConstrainerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>QueueConstrainer</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerList getQueueConstrainersByIds(org.osid.id.IdList queueConstrainerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.tracking.rules.QueueConstrainer> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = queueConstrainerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getQueueConstrainer(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("queue constrainer " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.tracking.rules.queueconstrainer.LinkedQueueConstrainerList(ret));
    }


    /**
     *  Gets a <code>QueueConstrainerList</code> corresponding to the given
     *  queue constrainer genus <code>Type</code> which does not include
     *  queue constrainers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  queue constrainers or an error results. Otherwise, the returned list
     *  may contain only those queue constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, queue constrainers are returned that are currently
     *  active. In any status mode, active and inactive queue constrainers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getQueueConstrainers()</code>.
     *
     *  @param  queueConstrainerGenusType a queueConstrainer genus type 
     *  @return the returned <code>QueueConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerList getQueueConstrainersByGenusType(org.osid.type.Type queueConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.tracking.rules.queueconstrainer.QueueConstrainerGenusFilterList(getQueueConstrainers(), queueConstrainerGenusType));
    }


    /**
     *  Gets a <code>QueueConstrainerList</code> corresponding to the given
     *  queue constrainer genus <code>Type</code> and include any additional
     *  queue constrainers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  queue constrainers or an error results. Otherwise, the returned list
     *  may contain only those queue constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, queue constrainers are returned that are currently
     *  active. In any status mode, active and inactive queue constrainers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getQueueConstrainers()</code>.
     *
     *  @param  queueConstrainerGenusType a queueConstrainer genus type 
     *  @return the returned <code>QueueConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerList getQueueConstrainersByParentGenusType(org.osid.type.Type queueConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getQueueConstrainersByGenusType(queueConstrainerGenusType));
    }


    /**
     *  Gets a <code>QueueConstrainerList</code> containing the given
     *  queue constrainer record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  queue constrainers or an error results. Otherwise, the returned list
     *  may contain only those queue constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, queue constrainers are returned that are currently
     *  active. In any status mode, active and inactive queue constrainers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getQueueConstrainers()</code>.
     *
     *  @param  queueConstrainerRecordType a queueConstrainer record type 
     *  @return the returned <code>QueueConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerList getQueueConstrainersByRecordType(org.osid.type.Type queueConstrainerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.tracking.rules.queueconstrainer.QueueConstrainerRecordFilterList(getQueueConstrainers(), queueConstrainerRecordType));
    }


    /**
     *  Gets all <code>QueueConstrainers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  queue constrainers or an error results. Otherwise, the returned list
     *  may contain only those queue constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, queue constrainers are returned that are currently
     *  active. In any status mode, active and inactive queue constrainers
     *  are returned.
     *
     *  @return a list of <code>QueueConstrainers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.tracking.rules.QueueConstrainerList getQueueConstrainers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the queue constrainer list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of queue constrainers
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.tracking.rules.QueueConstrainerList filterQueueConstrainersOnViews(org.osid.tracking.rules.QueueConstrainerList list)
        throws org.osid.OperationFailedException {

        org.osid.tracking.rules.QueueConstrainerList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.tracking.rules.queueconstrainer.ActiveQueueConstrainerFilterList(ret);
        }

        return (ret);
    }
}
