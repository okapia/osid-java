//
// AbstractAwardEntry.java
//
//     Defines an AwardEntry builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.chronicle.awardentry.spi;


/**
 *  Defines an <code>AwardEntry</code> builder.
 */

public abstract class AbstractAwardEntryBuilder<T extends AbstractAwardEntryBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.course.chronicle.awardentry.AwardEntryMiter awardEntry;


    /**
     *  Constructs a new <code>AbstractAwardEntryBuilder</code>.
     *
     *  @param awardEntry the award entry to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractAwardEntryBuilder(net.okapia.osid.jamocha.builder.course.chronicle.awardentry.AwardEntryMiter awardEntry) {
        super(awardEntry);
        this.awardEntry = awardEntry;
        return;
    }


    /**
     *  Builds the award entry.
     *
     *  @return the new award entry
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.course.chronicle.AwardEntry build() {
        (new net.okapia.osid.jamocha.builder.validator.course.chronicle.awardentry.AwardEntryValidator(getValidations())).validate(this.awardEntry);
        return (new net.okapia.osid.jamocha.builder.course.chronicle.awardentry.ImmutableAwardEntry(this.awardEntry));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the award entry miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.course.chronicle.awardentry.AwardEntryMiter getMiter() {
        return (this.awardEntry);
    }


    /**
     *  Sets the student.
     *
     *  @param student a student
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>student</code> is
     *          <code>null</code>
     */

    public T student(org.osid.resource.Resource student) {
        getMiter().setStudent(student);
        return (self());
    }


    /**
     *  Sets the award.
     *
     *  @param award an award
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>award</code> is
     *          <code>null</code>
     */

    public T award(org.osid.recognition.Award award) {
        getMiter().setAward(award);
        return (self());
    }


    /**
     *  Sets the date awarded.
     *
     *  @param date a date awarded
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T dateAwarded(org.osid.calendaring.DateTime date) {
        getMiter().setDateAwarded(date);
        return (self());
    }


    /**
     *  Sets the program.
     *
     *  @param program a program
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>program</code> is
     *          <code>null</code>
     */

    public T program(org.osid.course.program.Program program) {
        getMiter().setProgram(program);
        return (self());
    }


    /**
     *  Sets the course.
     *
     *  @param course a course
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>course</code> is
     *          <code>null</code>
     */

    public T course(org.osid.course.Course course) {
        getMiter().setCourse(course);
        return (self());
    }


    /**
     *  Sets the assessment.
     *
     *  @param assessment an assessment
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>assessment</code>
     *          is <code>null</code>
     */

    public T assessment(org.osid.assessment.Assessment assessment) {
        getMiter().setAssessment(assessment);
        return (self());
    }


    /**
     *  Adds an AwardEntry record.
     *
     *  @param record an award entry record
     *  @param recordType the type of award entry record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.course.chronicle.records.AwardEntryRecord record, org.osid.type.Type recordType) {
        getMiter().addAwardEntryRecord(record, recordType);
        return (self());
    }
}       


