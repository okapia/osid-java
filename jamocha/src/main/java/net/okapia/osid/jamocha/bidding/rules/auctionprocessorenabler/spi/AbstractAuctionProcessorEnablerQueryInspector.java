//
// AbstractAuctionProcessorEnablerQueryInspector.java
//
//     A template for making an AuctionProcessorEnablerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.rules.auctionprocessorenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for auction processor enablers.
 */

public abstract class AbstractAuctionProcessorEnablerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQueryInspector
    implements org.osid.bidding.rules.AuctionProcessorEnablerQueryInspector {

    private final java.util.Collection<org.osid.bidding.rules.records.AuctionProcessorEnablerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the auction processor <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledAuctionProcessorIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the auction processor query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorQueryInspector[] getRuledAuctionProcessorTerms() {
        return (new org.osid.bidding.rules.AuctionProcessorQueryInspector[0]);
    }


    /**
     *  Gets the auction house <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAuctionHouseIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the auction house query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseQueryInspector[] getAuctionHouseTerms() {
        return (new org.osid.bidding.AuctionHouseQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given auction processor enabler query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an auction processor enabler implementing the requested record.
     *
     *  @param auctionProcessorEnablerRecordType an auction processor enabler record type
     *  @return the auction processor enabler query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>auctionProcessorEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(auctionProcessorEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.rules.records.AuctionProcessorEnablerQueryInspectorRecord getAuctionProcessorEnablerQueryInspectorRecord(org.osid.type.Type auctionProcessorEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.bidding.rules.records.AuctionProcessorEnablerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(auctionProcessorEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(auctionProcessorEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this auction processor enabler query. 
     *
     *  @param auctionProcessorEnablerQueryInspectorRecord auction processor enabler query inspector
     *         record
     *  @param auctionProcessorEnablerRecordType auctionProcessorEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAuctionProcessorEnablerQueryInspectorRecord(org.osid.bidding.rules.records.AuctionProcessorEnablerQueryInspectorRecord auctionProcessorEnablerQueryInspectorRecord, 
                                                   org.osid.type.Type auctionProcessorEnablerRecordType) {

        addRecordType(auctionProcessorEnablerRecordType);
        nullarg(auctionProcessorEnablerRecordType, "auction processor enabler record type");
        this.records.add(auctionProcessorEnablerQueryInspectorRecord);        
        return;
    }
}
