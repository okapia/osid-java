//
// AbstractStepConstrainerSearch.java
//
//     A template for making a StepConstrainer Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.rules.stepconstrainer.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing step constrainer searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractStepConstrainerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.workflow.rules.StepConstrainerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.workflow.rules.records.StepConstrainerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.workflow.rules.StepConstrainerSearchOrder stepConstrainerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of step constrainers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  stepConstrainerIds list of step constrainers
     *  @throws org.osid.NullArgumentException
     *          <code>stepConstrainerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongStepConstrainers(org.osid.id.IdList stepConstrainerIds) {
        while (stepConstrainerIds.hasNext()) {
            try {
                this.ids.add(stepConstrainerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongStepConstrainers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of step constrainer Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getStepConstrainerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  stepConstrainerSearchOrder step constrainer search order 
     *  @throws org.osid.NullArgumentException
     *          <code>stepConstrainerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>stepConstrainerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderStepConstrainerResults(org.osid.workflow.rules.StepConstrainerSearchOrder stepConstrainerSearchOrder) {
	this.stepConstrainerSearchOrder = stepConstrainerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.workflow.rules.StepConstrainerSearchOrder getStepConstrainerSearchOrder() {
	return (this.stepConstrainerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given step constrainer search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a step constrainer implementing the requested record.
     *
     *  @param stepConstrainerSearchRecordType a step constrainer search record
     *         type
     *  @return the step constrainer search record
     *  @throws org.osid.NullArgumentException
     *          <code>stepConstrainerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(stepConstrainerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.rules.records.StepConstrainerSearchRecord getStepConstrainerSearchRecord(org.osid.type.Type stepConstrainerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.workflow.rules.records.StepConstrainerSearchRecord record : this.records) {
            if (record.implementsRecordType(stepConstrainerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(stepConstrainerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this step constrainer search. 
     *
     *  @param stepConstrainerSearchRecord step constrainer search record
     *  @param stepConstrainerSearchRecordType stepConstrainer search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addStepConstrainerSearchRecord(org.osid.workflow.rules.records.StepConstrainerSearchRecord stepConstrainerSearchRecord, 
                                           org.osid.type.Type stepConstrainerSearchRecordType) {

        addRecordType(stepConstrainerSearchRecordType);
        this.records.add(stepConstrainerSearchRecord);        
        return;
    }
}
