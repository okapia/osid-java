//
// AbstractAcademySearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recognition.academy.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractAcademySearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.recognition.AcademySearchResults {

    private org.osid.recognition.AcademyList academies;
    private final org.osid.recognition.AcademyQueryInspector inspector;
    private final java.util.Collection<org.osid.recognition.records.AcademySearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractAcademySearchResults.
     *
     *  @param academies the result set
     *  @param academyQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>academies</code>
     *          or <code>academyQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractAcademySearchResults(org.osid.recognition.AcademyList academies,
                                            org.osid.recognition.AcademyQueryInspector academyQueryInspector) {
        nullarg(academies, "academies");
        nullarg(academyQueryInspector, "academy query inspectpr");

        this.academies = academies;
        this.inspector = academyQueryInspector;

        return;
    }


    /**
     *  Gets the academy list resulting from a search.
     *
     *  @return an academy list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.recognition.AcademyList getAcademies() {
        if (this.academies == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.recognition.AcademyList academies = this.academies;
        this.academies = null;
	return (academies);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.recognition.AcademyQueryInspector getAcademyQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  academy search record <code> Type. </code> This method must
     *  be used to retrieve an academy implementing the requested
     *  record.
     *
     *  @param academySearchRecordType an academy search 
     *         record type 
     *  @return the academy search
     *  @throws org.osid.NullArgumentException
     *          <code>academySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(academySearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.recognition.records.AcademySearchResultsRecord getAcademySearchResultsRecord(org.osid.type.Type academySearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.recognition.records.AcademySearchResultsRecord record : this.records) {
            if (record.implementsRecordType(academySearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(academySearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record academy search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addAcademyRecord(org.osid.recognition.records.AcademySearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "academy record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
