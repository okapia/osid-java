//
// ActionElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.action;

/**
 *  Ids for object elements for use in forms and queries.
 */

public class ActionElements
    extends net.okapia.osid.jamocha.spi.OsidRuleElements {


    /**
     *  Gets the ActionElement Id.
     *
     *  @return the action element Id
     */

    public static org.osid.id.Id getActionEntityId() {
        return (makeEntityId("osid.control.Action"));
    }


    /**
     *  Gets the ActionGroupId element Id.
     *
     *  @return the ActionGroupId element Id
     */

    public static org.osid.id.Id getActionGroupId() {
        return (makeElementId("osid.control.action.ActionGroupId"));
    }


    /**
     *  Gets the ActionGroup element Id.
     *
     *  @return the ActionGroup element Id
     */

    public static org.osid.id.Id getActionGroup() {
        return (makeElementId("osid.control.action.ActionGroup"));
    }


    /**
     *  Gets the Delay element Id.
     *
     *  @return the Delay element Id
     */

    public static org.osid.id.Id getDelay() {
        return (makeElementId("osid.control.action.Delay"));
    }


    /**
     *  Gets the NextActionGroupId element Id.
     *
     *  @return the NextActionGroupId element Id
     */

    public static org.osid.id.Id getNextActionGroupId() {
        return (makeElementId("osid.control.action.NextActionGroupId"));
    }


    /**
     *  Gets the NextActionGroup element Id.
     *
     *  @return the NextActionGroup element Id
     */

    public static org.osid.id.Id getNextActionGroup() {
        return (makeElementId("osid.control.action.NextActionGroup"));
    }


    /**
     *  Gets the SceneId element Id.
     *
     *  @return the SceneId element Id
     */

    public static org.osid.id.Id getSceneId() {
        return (makeElementId("osid.control.action.SceneId"));
    }


    /**
     *  Gets the Scene element Id.
     *
     *  @return the Scene element Id
     */

    public static org.osid.id.Id getScene() {
        return (makeElementId("osid.control.action.Scene"));
    }


    /**
     *  Gets the SettingId element Id.
     *
     *  @return the SettingId element Id
     */

    public static org.osid.id.Id getSettingId() {
        return (makeElementId("osid.control.action.SettingId"));
    }


    /**
     *  Gets the Setting element Id.
     *
     *  @return the Setting element Id
     */

    public static org.osid.id.Id getSetting() {
        return (makeElementId("osid.control.action.Setting"));
    }


    /**
     *  Gets the MatchingControllerId element Id.
     *
     *  @return the MatchingControllerId element Id
     */

    public static org.osid.id.Id getMatchingControllerId() {
        return (makeElementId("osid.control.action.MatchingControllerId"));
    }


    /**
     *  Gets the MatchingController element Id.
     *
     *  @return the MatchingController element Id
     */

    public static org.osid.id.Id getMatchingController() {
        return (makeElementId("osid.control.action.MatchingController"));
    }


    /**
     *  Gets the MatchingAmountFactor element Id.
     *
     *  @return the MatchingAmountFactor element Id
     */

    public static org.osid.id.Id getMatchingAmountFactor() {
        return (makeElementId("osid.control.action.MatchingAmountFactor"));
    }


    /**
     *  Gets the MatchingRateFactor element Id.
     *
     *  @return the MatchingRateFactor element Id
     */

    public static org.osid.id.Id getMatchingRateFactor() {
        return (makeElementId("osid.control.action.MatchingRateFactor"));
    }


    /**
     *  Gets the Blocking element Id.
     *
     *  @return the Blocking element Id
     */

    public static org.osid.id.Id getBlocking() {
        return (makeQueryElementId("osid.control.action.Blocking"));
    }
}
