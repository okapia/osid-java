//
// AbstractDateTimeMetadata.java
//
//     Defines a datetime Metadata.
//
//
// Tom Coppeto
// Okapia
// 15 March 2013
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metadata.spi;

import org.osid.binding.java.annotation.OSID;

import net.okapia.osid.primordium.calendaring.GregorianUTCDateTime;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeSet;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a datetime Metadata.
 */

public abstract class AbstractDateTimeMetadata
    extends AbstractMetadata
    implements org.osid.Metadata {

    private final Types calendarTypes = new TypeSet();
    private final Types timeTypes = new TypeSet();

    private org.osid.calendaring.DateTime minimum = GregorianUTCDateTime.valueOf("-13798000000");
    private org.osid.calendaring.DateTime maximum = GregorianUTCDateTime.valueOf("3000000000");
    private org.osid.calendaring.DateTimeResolution resolution = org.osid.calendaring.DateTimeResolution.SECOND;

    private final java.util.Collection<org.osid.calendaring.DateTime> set = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.DateTime> defvals  = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.DateTime> existing = new java.util.LinkedHashSet<>();


    /**
     *  Constructs a new {@code AbstractDateTimeMetadata}.
     *
     *  @param elementId the Id of the element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    protected AbstractDateTimeMetadata(org.osid.id.Id elementId) {
        super(org.osid.Syntax.DATETIME, elementId);
        return;
    }


    /**
     *  Constructs a new {@code AbstractDateTimeMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @param isLinked {@code true} if the element is linked to
     *         another element, {@code false} otherwise
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    protected AbstractDateTimeMetadata(org.osid.id.Id elementId, boolean isArray, boolean isLinked) {
        super(org.osid.Syntax.DATETIME, elementId, isArray, isLinked);
        return;
    }


    /**
     *  Gets the smallest resolution of the date time value. 
     *
     *  @return the resolution 
     *  @throws org.osid.IllegalStateException syntax is not a
     *          <code>DATETIME</code>, <code>DURATION</code>, or
     *          <code>TIME</code>
     */

    @OSID @Override
    public org.osid.calendaring.DateTimeResolution getDateTimeResolution() {
        return (this.resolution);
    }


    /**
     *  Sets the resolution.
     *
     *  @param resolution the smallest resolution allowed
     *  @throws org.osid.NullARgumentException {@code resolution} is
     *          {@code null}
     */

    protected void setResolution(org.osid.calendaring.DateTimeResolution resolution) {
        nullarg(resolution, "resolution");
        this.resolution = resolution;
        return;
    }


    /**
     *  Gets the set of acceptable calendar types. 
     *
     *  @return the set of calendar types 
     *  @throws org.osid.IllegalStateException syntax is not a
     *          <code>DATETIME</code>, <code>DURATION</code>, or
     *          <code>TIME</code>
     */

    @OSID @Override
    public org.osid.type.Type[] getCalendarTypes() {
        return (this.calendarTypes.toArray());
    }
        

    /**
     *  Tests if the given calendar type is supported. 
     *
     *  @param  calendarType a calendar Type 
     *  @return <code> true </code> if the type is supported, <code> false 
     *          </code> otherwise 
     *  @throws org.osid.IllegalStateException syntax is not a
     *          <code>DATETIME</code>, <code>DURATION</code>, or
     *          <code>TIME</code>
     *  @throws org.osid.NullArgumentException <code> calendarType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCalendarType(org.osid.type.Type calendarType) {
        return (this.calendarTypes.contains(calendarType));
    }


    /**
     *  Add support for a calendar type.
     *
     *  @param calendarType the type of calendar
     *  @throws org.osid.NullArgumentException {@code calendarType} is
     *          {@code null}
     */

    protected void addCalendarType(org.osid.type.Type calendarType) {
        this.calendarTypes.add(calendarType);
        return;
    }


    /**
     *  Gets the set of acceptable time types. 
     *
     *  @return a set of time types or an empty array if not restricted 
     *  @throws org.osid.IllegalStateException syntax is not a
     *          <code>DATETIME</code> or <code>DURATION</code>
     */

    @OSID @Override
    public org.osid.type.Type[] getTimeTypes() {
        return (this.timeTypes.toArray());
    }


    /**
     *  Tests if the given time type is supported. 
     *
     *  @param  timeType a time Type 
     *  @return <code> true </code> if the type is supported, <code> false 
     *          </code> otherwise 
     *  @throws org.osid.IllegalStateException syntax is not a
     *          <code>DATETIME</code> or <code>DURATION</code>
     *  @throws org.osid.NullArgumentException <code> timeType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean supportsTimeType(org.osid.type.Type timeType) {
        return (this.timeTypes.contains(timeType));
    }


    /**
     *  Add support for a time type.
     *
     *  @param timeType the type of time
     *  @throws org.osid.NullArgumentException {@code timeType} is
     *          {@code null}
     */

    protected void addTimeType(org.osid.type.Type timeType) {
        this.timeTypes.add(timeType);
        return;
    }


    /**
     *  Gets the minimum datetime value. 
     *
     *  @return the minimum datetime 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          DATETIME </code>
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getMinimumDateTime() {
        return (this.minimum);
    }


    /**
     *  Gets the maximum datetime value. 
     *
     *  @return the maximum datetime 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          DATETIME </code>
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getMaximumDateTime() {
        return (this.maximum);
    }


    /**
     *  Sets the min and max datetimes.
     *
     *  @param min the minimum value
     *  @param max the maximum value
     *  @throws org.osid.InvalidArgumentException {@code min} is
     *          greater than {@code max}
     *  @throws org.osid.NullArgumentException {@code min} or
     *          {@code max} is {@code null}
     */

    protected void setDateTimeRange(org.osid.calendaring.DateTime min, org.osid.calendaring.DateTime max) {
        nullarg(min, "min datetime");
        nullarg(max, "max datetime");

        if (min.isGreater(max)) {
            throw new org.osid.InvalidArgumentException("min is greater than max");
        }

        this.maximum = min;
        this.maximum = min;

        return;
    }


    /**
     *  Gets the set of acceptable datetime values. 
     *
     *  @return a set of datetimes or an empty array if not restricted 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          DATETIME </code>
     */

    @OSID @Override
    public org.osid.calendaring.DateTime[] getDateTimeSet() {
        return (this.set.toArray(new org.osid.calendaring.DateTime[this.set.size()]));
    }

    
    /**
     *  Sets the datetime set.
     *
     *  @param values a collection of accepted datetime values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setDateTimeSet(java.util.Collection<org.osid.calendaring.DateTime> values) {
        this.set.clear();
        addToDateTimeSet(values);
        return;
    }


    /**
     *  Adds a collection of values to the datetime set.
     *
     *  @param values a collection of accepted datetime values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addToDateTimeSet(java.util.Collection<org.osid.calendaring.DateTime> values) {
        nullarg(values, "datetime set");
        this.set.addAll(values);
        return;
    }


    /**
     *  Adds a value to the datetime set.
     *
     *  @param value a datetime value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void addToDateTimeSet(org.osid.calendaring.DateTime value) {
        nullarg(value, "datetime value");
        this.set.add(value);
        return;
    }


    /**
     *  Removes a value from the datetime set.
     *
     *  @param value a datetime value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void removeFromDateTimeSet(org.osid.calendaring.DateTime value) {
        nullarg(value, "datetime value");
        this.set.remove(value);
        return;
    }


    /**
     *  Clears the datetime set.
     */

    protected void clearDateTimeSet() {
        this.set.clear();
        return;
    }


    /**
     *  Gets the default date time values. These are the values used
     *  if the element value is not provided or is cleared. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the default date time values 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          DATE TIME </code> or <code> isRequired() </code> is
     *          <code> true </code>
     */

    @OSID @Override
    public org.osid.calendaring.DateTime[] getDefaultDateTimeValues() {
        return (this.defvals.toArray(new org.osid.calendaring.DateTime[this.defvals.size()]));
    }


    /**
     *  Sets the default date time set.
     *
     *  @param values a collection of default date time values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setDefaultDateTimeValues(java.util.Collection<org.osid.calendaring.DateTime> values) {
        clearDefaultDateTimeValues();
        addDefaultDateTimeValues(values);
        return;
    }


    /**
     *  Adds a collection of default date time values.
     *
     *  @param values a collection of default date time values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addDefaultDateTimeValues(java.util.Collection<org.osid.calendaring.DateTime> values) {
        nullarg(values, "default date time values");
        this.defvals.addAll(values);
        return;
    }


    /**
     *  Adds a default date time value.
     *
     *  @param value a date time value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void addDefaultDateTimeValue(org.osid.calendaring.DateTime value) {
        nullarg(value, "default date time value");
        this.defvals.add(value);
        return;
    }


    /**
     *  Removes a default date time value.
     *
     *  @param value a date time value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void removeDefaultDateTimeValue(org.osid.calendaring.DateTime value) {
        nullarg(value, "default date time value");
        this.defvals.remove(value);
        return;
    }


    /**
     *  Clears the default date time values.
     */

    protected void clearDefaultDateTimeValues() {
        this.defvals.clear();
        return;
    }


    /**
     *  Gets the existing date time values. If <code> hasValue()
     *  </code> and <code> isRequired() </code> are <code> false,
     *  </code> then these values are the default values. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the existing date time values 
     *  @throws org.osid.IllegalStateException syntax is not a <code> 
     *          DATE TIME </code> or <code> isValueKnown() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime[] getExistingDateTimeValues() {
        return (this.existing.toArray(new org.osid.calendaring.DateTime[this.existing.size()]));
    }


    /**
     *  Sets the existing date time set.
     *
     *  @param values a collection of existing date time values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setExistingDateTimeValues(java.util.Collection<org.osid.calendaring.DateTime> values) {
        clearExistingDateTimeValues();
        addExistingDateTimeValues(values);
        return;
    }


    /**
     *  Adds a collection of existing date time values.
     *
     *  @param values a collection of existing date time values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addExistingDateTimeValues(java.util.Collection<org.osid.calendaring.DateTime> values) {
        nullarg(values, "existing date time values");

        this.existing.addAll(values);
        setValueKnown(true);

        return;
    }


    /**
     *  Adds a existing date time value.
     *
     *  @param value a date time value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void addExistingDateTimeValue(org.osid.calendaring.DateTime value) {
        nullarg(value, "existing date time value");

        this.existing.add(value);
        setValueKnown(true);

        return;
    }


    /**
     *  Removes a existing date time value.
     *
     *  @param value a date time value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void removeExistingDateTimeValue(org.osid.calendaring.DateTime value) {
        nullarg(value, "existing date time value");
        this.existing.remove(value);
        return;
    }


    /**
     *  Clears the existing date time values.
     */

    protected void clearExistingDateTimeValues() {
        this.existing.clear();
        setValueKnown(false);
        return;
    }    
}