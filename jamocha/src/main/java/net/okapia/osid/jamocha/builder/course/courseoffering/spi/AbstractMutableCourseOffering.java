//
// AbstractMutableCourseOffering.java
//
//     Defines a mutable CourseOffering.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.courseoffering.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>CourseOffering</code>.
 */

public abstract class AbstractMutableCourseOffering
    extends net.okapia.osid.jamocha.course.courseoffering.spi.AbstractCourseOffering
    implements org.osid.course.CourseOffering,
               net.okapia.osid.jamocha.builder.course.courseoffering.CourseOfferingMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this course offering. 
     *
     *  @param record course offering record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addCourseOfferingRecord(org.osid.course.records.CourseOfferingRecord record, org.osid.type.Type recordType) {
        super.addCourseOfferingRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the start date for when this course offering is
     *  effective.
     *
     *  @param date the start date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setStartDate(org.osid.calendaring.DateTime date) {
        super.setStartDate(date);
        return;
    }


    /**
     *  Sets the end date for when this course offering ceases to be
     *  effective.
     *
     *  @param date the end date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setEndDate(org.osid.calendaring.DateTime date) {
        super.setEndDate(date);
        return;
    }

    
    /**
     *  Sets the display name for this course offering.
     *
     *  @param displayName the name for this course offering
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this course offering.
     *
     *  @param description the description of this course offering
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the end state for the expiration of this course offering
     *  relationship.
     *
     *  @param state the end state
     *  @throws org.osid.NullArgumentException <code>state</code> is
     *          <code>null</code>
     */

    @Override
    public void setEndReason(org.osid.process.State state) {
        super.setEndReason(state);
        return;
    }


    /**
     *  Sets the course.
     *
     *  @param course a course
     *  @throws org.osid.NullArgumentException <code>course</code> is
     *          <code>null</code>
     */

    @Override
    public void setCourse(org.osid.course.Course course) {
        super.setCourse(course);
        return;
    }


    /**
     *  Sets the term.
     *
     *  @param term a term
     *  @throws org.osid.NullArgumentException <code>term</code> is
     *          <code>null</code>
     */

    @Override
    public void setTerm(org.osid.course.Term term) {
        super.setTerm(term);
        return;
    }


    /**
     *  Sets the title.
     *
     *  @param title a title
     *  @throws org.osid.NullArgumentException <code>title</code> is
     *          <code>null</code>
     */

    @Override
    public void setTitle(org.osid.locale.DisplayText title) {
        super.setTitle(title);
        return;
    }


    /**
     *  Sets the number.
     *
     *  @param number a number
     *  @throws org.osid.NullArgumentException <code>number</code> is
     *          <code>null</code>
     */

    @Override
    public void setNumber(String number) {
        super.setNumber(number);
        return;
    }


    /**
     *  Adds an instructor.
     *
     *  @param instructor an instructor
     *  @throws org.osid.NullArgumentException <code>instructor</code>
     *          is <code>null</code>
     */

    @Override
    public void addInstructor(org.osid.resource.Resource instructor) {
        super.addInstructor(instructor);
        return;
    }


    /**
     *  Sets all the instructors.
     *
     *  @param instructors a collection of instructors
     *  @throws org.osid.NullArgumentException
     *          <code>instructors</code> is <code>null</code>
     */

    @Override
    public void setInstructors(java.util.Collection<org.osid.resource.Resource> instructors) {
        super.setInstructors(instructors);
        return;
    }


    /**
     *  Adds a sponsor.
     *
     *  @param sponsor a sponsor
     *  @throws org.osid.NullArgumentException
     *          <code>sponsor</code> is <code>null</code>
     */

    @Override
    public void addSponsor(org.osid.resource.Resource sponsor) {
        super.addSponsor(sponsor);
        return;
    }


    /**
     *  Sets all the sponsors.
     *
     *  @param sponsors a collection of sponsors
     *  @throws org.osid.NullArgumentException
     *          <code>sponsors</code> is <code>null</code>
     */

    @Override
    public void setSponsors(java.util.Collection<org.osid.resource.Resource> sponsors) {
        super.setSponsors(sponsors);
        return;
    }


    /**
     *  Adds a credit smount.
     *
     *  @param credit a credit amount
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    @Override
    public void addCreditAmount(org.osid.grading.Grade amount) {
        super.addCreditAmount(amount);
        return;
    }


    /**
     *  Sets all the credit amount options.
     *
     *  @param amounts a collection of credit amounts
     *  @throws org.osid.NullArgumentException <code>amounts</code>
     *          is <code>null</code>
     */

    @Override
    public void setCreditAmounts(java.util.Collection<org.osid.grading.Grade> amounts) {
        super.setCreditAmounts(amounts);
        return;
    }


    /**
     *  Adds a grading option.
     *
     *  @param option a grading option
     *  @throws org.osid.NullArgumentException <code>option</code> is
     *          <code>null</code>
     */

    @Override
    public void addGradingOption(org.osid.grading.GradeSystem option) {
        super.addGradingOption(option);
        return;
    }


    /**
     *  Sets all the grading options.
     *
     *  @param options a collection of grading options
     *  @throws org.osid.NullArgumentException <code>options</code> is
     *          <code>null</code>
     */

    @Override
    public void setGradingOptions(java.util.Collection<org.osid.grading.GradeSystem> options) {
        super.setGradingOptions(options);
        return;
    }


    /**
     *  Sets the requires registration flag.
     *
     *  @param requires <code> true </code> if this course requires
     *          advance registration, <code> false </code> otherwise
     */

    @Override
    public void setRequiresRegistration(boolean requires) {
        super.setRequiresRegistration(requires);
        return;
    }


    /**
     *  Sets the minimum seats.
     *
     *  @param seats the minimum seats
     *  @throws org.osid.InvalidArgumentException <code>seats</code>
     *          is negative
     */

    @Override
    public void setMinimumSeats(long seats) {
        super.setMinimumSeats(seats);
        return;
    }


    /**
     *  Sets the maximum seats.
     *
     *  @param seats the maximum seats
     *  @throws org.osid.InvalidArgumentException <code>seats</code>
     *          is negative
     */

    @Override
    public void setMaximumSeats(long seats) {
        super.setMaximumSeats(seats);
        return;
    }


    /**
     *  Sets the URL.
     *
     *  @param url a URL
     *  @throws org.osid.NullArgumentException <code>url</code> is
     *          <code>null</code>
     */

    @Override
    public void setURL(String url) {
        super.setURL(url);
        return;
    }


    /**
     *  Sets the schedule info.
     *
     *  @param info a schedule info
     *  @throws org.osid.NullArgumentException <code>info</code> is
     *          <code>null</code>
     */

    @Override
    public void setScheduleInfo(org.osid.locale.DisplayText info) {
        super.setScheduleInfo(info);
        return;
    }


    /**
     *  Sets the event.
     *
     *  @param event an event
     *  @throws org.osid.NullArgumentException <code>event</code> is
     *          <code>null</code>
     */

    @Override
    public void setEvent(org.osid.calendaring.Event event) {
        super.setEvent(event);
        return;
    }
}

