//
// AbstractIndexedMapCreditLookupSession.java
//
//    A simple framework for providing a Credit lookup service
//    backed by a fixed collection of credits with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.acknowledgement.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Credit lookup service backed by a
 *  fixed collection of credits. The credits are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some credits may be compatible
 *  with more types than are indicated through these credit
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Credits</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapCreditLookupSession
    extends AbstractMapCreditLookupSession
    implements org.osid.acknowledgement.CreditLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.acknowledgement.Credit> creditsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.acknowledgement.Credit>());
    private final MultiMap<org.osid.type.Type, org.osid.acknowledgement.Credit> creditsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.acknowledgement.Credit>());


    /**
     *  Makes a <code>Credit</code> available in this session.
     *
     *  @param  credit a credit
     *  @throws org.osid.NullArgumentException <code>credit<code> is
     *          <code>null</code>
     */

    @Override
    protected void putCredit(org.osid.acknowledgement.Credit credit) {
        super.putCredit(credit);

        this.creditsByGenus.put(credit.getGenusType(), credit);
        
        try (org.osid.type.TypeList types = credit.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.creditsByRecord.put(types.getNextType(), credit);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a credit from this session.
     *
     *  @param creditId the <code>Id</code> of the credit
     *  @throws org.osid.NullArgumentException <code>creditId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeCredit(org.osid.id.Id creditId) {
        org.osid.acknowledgement.Credit credit;
        try {
            credit = getCredit(creditId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.creditsByGenus.remove(credit.getGenusType());

        try (org.osid.type.TypeList types = credit.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.creditsByRecord.remove(types.getNextType(), credit);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeCredit(creditId);
        return;
    }


    /**
     *  Gets a <code>CreditList</code> corresponding to the given
     *  credit genus <code>Type</code> which does not include
     *  credits of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known credits or an error results. Otherwise,
     *  the returned list may contain only those credits that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  creditGenusType a credit genus type 
     *  @return the returned <code>Credit</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>creditGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByGenusType(org.osid.type.Type creditGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.acknowledgement.credit.ArrayCreditList(this.creditsByGenus.get(creditGenusType)));
    }


    /**
     *  Gets a <code>CreditList</code> containing the given
     *  credit record <code>Type</code>. In plenary mode, the
     *  returned list contains all known credits or an error
     *  results. Otherwise, the returned list may contain only those
     *  credits that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  creditRecordType a credit record type 
     *  @return the returned <code>credit</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>creditRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByRecordType(org.osid.type.Type creditRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.acknowledgement.credit.ArrayCreditList(this.creditsByRecord.get(creditRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.creditsByGenus.clear();
        this.creditsByRecord.clear();

        super.close();

        return;
    }
}
