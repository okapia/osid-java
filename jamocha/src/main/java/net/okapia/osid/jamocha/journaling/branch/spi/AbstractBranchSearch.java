//
// AbstractBranchSearch.java
//
//     A template for making a Branch Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.journaling.branch.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing branch searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractBranchSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.journaling.BranchSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.journaling.records.BranchSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.journaling.BranchSearchOrder branchSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of branches. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  branchIds list of branches
     *  @throws org.osid.NullArgumentException
     *          <code>branchIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongBranches(org.osid.id.IdList branchIds) {
        while (branchIds.hasNext()) {
            try {
                this.ids.add(branchIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongBranches</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of branch Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getBranchIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  branchSearchOrder branch search order 
     *  @throws org.osid.NullArgumentException
     *          <code>branchSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>branchSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderBranchResults(org.osid.journaling.BranchSearchOrder branchSearchOrder) {
	this.branchSearchOrder = branchSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.journaling.BranchSearchOrder getBranchSearchOrder() {
	return (this.branchSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given branch search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a branch implementing the requested record.
     *
     *  @param branchSearchRecordType a branch search record
     *         type
     *  @return the branch search record
     *  @throws org.osid.NullArgumentException
     *          <code>branchSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(branchSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.journaling.records.BranchSearchRecord getBranchSearchRecord(org.osid.type.Type branchSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.journaling.records.BranchSearchRecord record : this.records) {
            if (record.implementsRecordType(branchSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(branchSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this branch search. 
     *
     *  @param branchSearchRecord branch search record
     *  @param branchSearchRecordType branch search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBranchSearchRecord(org.osid.journaling.records.BranchSearchRecord branchSearchRecord, 
                                           org.osid.type.Type branchSearchRecordType) {

        addRecordType(branchSearchRecordType);
        this.records.add(branchSearchRecord);        
        return;
    }
}
