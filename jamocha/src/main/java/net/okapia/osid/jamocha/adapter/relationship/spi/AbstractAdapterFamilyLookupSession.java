//
// AbstractAdapterFamilyLookupSession.java
//
//    A Family lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.relationship.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Family lookup session adapter.
 */

public abstract class AbstractAdapterFamilyLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.relationship.FamilyLookupSession {

    private final org.osid.relationship.FamilyLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterFamilyLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterFamilyLookupSession(org.osid.relationship.FamilyLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Family} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupFamilies() {
        return (this.session.canLookupFamilies());
    }


    /**
     *  A complete view of the {@code Family} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeFamilyView() {
        this.session.useComparativeFamilyView();
        return;
    }


    /**
     *  A complete view of the {@code Family} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryFamilyView() {
        this.session.usePlenaryFamilyView();
        return;
    }

     
    /**
     *  Gets the {@code Family} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Family} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Family} and
     *  retained for compatibility.
     *
     *  @param familyId {@code Id} of the {@code Family}
     *  @return the family
     *  @throws org.osid.NotFoundException {@code familyId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code familyId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.Family getFamily(org.osid.id.Id familyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFamily(familyId));
    }


    /**
     *  Gets a {@code FamilyList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  families specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Families} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  familyIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Family} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code familyIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.FamilyList getFamiliesByIds(org.osid.id.IdList familyIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFamiliesByIds(familyIds));
    }


    /**
     *  Gets a {@code FamilyList} corresponding to the given
     *  family genus {@code Type} which does not include
     *  families of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  families or an error results. Otherwise, the returned list
     *  may contain only those families that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  familyGenusType a family genus type 
     *  @return the returned {@code Family} list
     *  @throws org.osid.NullArgumentException
     *          {@code familyGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.FamilyList getFamiliesByGenusType(org.osid.type.Type familyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFamiliesByGenusType(familyGenusType));
    }


    /**
     *  Gets a {@code FamilyList} corresponding to the given
     *  family genus {@code Type} and include any additional
     *  families with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  families or an error results. Otherwise, the returned list
     *  may contain only those families that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  familyGenusType a family genus type 
     *  @return the returned {@code Family} list
     *  @throws org.osid.NullArgumentException
     *          {@code familyGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.FamilyList getFamiliesByParentGenusType(org.osid.type.Type familyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFamiliesByParentGenusType(familyGenusType));
    }


    /**
     *  Gets a {@code FamilyList} containing the given
     *  family record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  families or an error results. Otherwise, the returned list
     *  may contain only those families that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  familyRecordType a family record type 
     *  @return the returned {@code Family} list
     *  @throws org.osid.NullArgumentException
     *          {@code familyRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.FamilyList getFamiliesByRecordType(org.osid.type.Type familyRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFamiliesByRecordType(familyRecordType));
    }


    /**
     *  Gets a {@code FamilyList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  families or an error results. Otherwise, the returned list
     *  may contain only those families that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Family} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.relationship.FamilyList getFamiliesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFamiliesByProvider(resourceId));
    }


    /**
     *  Gets all {@code Families}. 
     *
     *  In plenary mode, the returned list contains all known
     *  families or an error results. Otherwise, the returned list
     *  may contain only those families that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Families} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.FamilyList getFamilies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFamilies());
    }
}
