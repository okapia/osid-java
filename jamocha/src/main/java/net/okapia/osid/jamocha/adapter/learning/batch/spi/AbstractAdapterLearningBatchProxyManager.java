//
// AbstractLearningBatchProxyManager.java
//
//     An adapter for a LearningBatchProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.learning.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a LearningBatchProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterLearningBatchProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.learning.batch.LearningBatchProxyManager>
    implements org.osid.learning.batch.LearningBatchProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterLearningBatchProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterLearningBatchProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterLearningBatchProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterLearningBatchProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of availabilities is available. 
     *
     *  @return <code> true </code> if an objective bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveBatchAdmin() {
        return (getAdapteeManager().supportsObjectiveBatchAdmin());
    }


    /**
     *  Tests if bulk administration of activities is available. 
     *
     *  @return <code> true </code> if an activity bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityBatchAdmin() {
        return (getAdapteeManager().supportsActivityBatchAdmin());
    }


    /**
     *  Tests if bulk administration of proficiencies is available. 
     *
     *  @return <code> true </code> if a proficiency bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProficiencyBatchAdmin() {
        return (getAdapteeManager().supportsProficiencyBatchAdmin());
    }


    /**
     *  Tests if bulk administration of objective banks is available. 
     *
     *  @return <code> true </code> if an objective bank bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveBankBatchAdmin() {
        return (getAdapteeManager().supportsObjectiveBankBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk objective 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObjectiveBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.batch.ObjectiveBatchAdminSession getObjectiveBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getObjectiveBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk objective 
     *  administration service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the <code> 
     *          ObjectiveBank </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ObjectiveBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> ObjectiveBank </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.batch.ObjectiveBatchAdminSession getObjectiveBatchAdminSessionForObjectiveBank(org.osid.id.Id objectiveBankId, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getObjectiveBatchAdminSessionForObjectiveBank(objectiveBankId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk activity 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ActivityBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.batch.ActivityBatchAdminSession getActivityBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk activity 
     *  administration service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the <code> 
     *          ObjectiveBank </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ActivityBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> ObjectiveBank </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.batch.ActivityBatchAdminSession getActivityBatchAdminSessionForObjectiveBank(org.osid.id.Id objectiveBankId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityBatchAdminSessionForObjectiveBank(objectiveBankId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  proficiency administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProficiencyBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProficiencyBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.batch.ProficiencyBatchAdminSession getProficiencyBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProficiencyBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  proficiency administration service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the <code> 
     *          ObjectiveBank </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProficiencyBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> ObjectiveBank </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProficiencyBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.batch.ProficiencyBatchAdminSession getProficiencyBatchAdminSessionForObjectiveBank(org.osid.id.Id objectiveBankId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProficiencyBatchAdminSessionForObjectiveBank(objectiveBankId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk objective 
     *  bank administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObjectiveBankBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveBankBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.batch.ObjectiveBankBatchAdminSession getObjectiveBankBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getObjectiveBankBatchAdminSession(proxy));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
