//
// AbstractFunctionSearch.java
//
//     A template for making a Function Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authorization.function.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing function searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractFunctionSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.authorization.FunctionSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.authorization.records.FunctionSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.authorization.FunctionSearchOrder functionSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of functions. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  functionIds list of functions
     *  @throws org.osid.NullArgumentException
     *          <code>functionIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongFunctions(org.osid.id.IdList functionIds) {
        while (functionIds.hasNext()) {
            try {
                this.ids.add(functionIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongFunctions</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of function Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getFunctionIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  functionSearchOrder function search order 
     *  @throws org.osid.NullArgumentException
     *          <code>functionSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>functionSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderFunctionResults(org.osid.authorization.FunctionSearchOrder functionSearchOrder) {
	this.functionSearchOrder = functionSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.authorization.FunctionSearchOrder getFunctionSearchOrder() {
	return (this.functionSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given function search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a function implementing the requested record.
     *
     *  @param functionSearchRecordType a function search record
     *         type
     *  @return the function search record
     *  @throws org.osid.NullArgumentException
     *          <code>functionSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(functionSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.authorization.records.FunctionSearchRecord getFunctionSearchRecord(org.osid.type.Type functionSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.authorization.records.FunctionSearchRecord record : this.records) {
            if (record.implementsRecordType(functionSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(functionSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this function search. 
     *
     *  @param functionSearchRecord function search record
     *  @param functionSearchRecordType function search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addFunctionSearchRecord(org.osid.authorization.records.FunctionSearchRecord functionSearchRecord, 
                                           org.osid.type.Type functionSearchRecordType) {

        addRecordType(functionSearchRecordType);
        this.records.add(functionSearchRecord);        
        return;
    }
}
