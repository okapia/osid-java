//
// AbstractIdBatchManager.java
//
//     An adapter for a IdBatchManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.id.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a IdBatchManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterIdBatchManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.id.batch.IdBatchManager>
    implements org.osid.id.batch.IdBatchManager {


    /**
     *  Constructs a new {@code AbstractAdapterIdBatchManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterIdBatchManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterIdBatchManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterIdBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if an <code> Id </code> bulk issue service is supported. 
     *
     *  @return <code> true </code> if <code> Id </code> bulk issuing is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIdBatchIssue() {
        return (getAdapteeManager().supportsIdBatchIssue());
    }


    /**
     *  Tests if an <code> Id </code> bulk administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if <code> Id </code> bulk administration 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIdBatchAdmin() {
        return (getAdapteeManager().supportsIdBatchAdmin());
    }


    /**
     *  Gets the session associated with the Id bulk issue service. 
     *
     *  @return an <code> IdBatchIssueSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIdBatchIssue() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.batch.IdBatchIssueSession getIdBatchIssueSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getIdBatchIssueSession());
    }


    /**
     *  Gets the session associated with the Id bulk admin service. 
     *
     *  @return an <code> IdBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIdBatchAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.batch.IdBatchAdminSession getIdBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getIdBatchAdminSession());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
