//
// AbstractMapRelationshipLookupSession
//
//    A simple framework for providing a Relationship lookup service
//    backed by a fixed collection of relationships.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.relationship.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Relationship lookup service backed by a
 *  fixed collection of relationships. The relationships are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Relationships</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapRelationshipLookupSession
    extends net.okapia.osid.jamocha.relationship.spi.AbstractRelationshipLookupSession
    implements org.osid.relationship.RelationshipLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.relationship.Relationship> relationships = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.relationship.Relationship>());


    /**
     *  Makes a <code>Relationship</code> available in this session.
     *
     *  @param  relationship a relationship
     *  @throws org.osid.NullArgumentException <code>relationship<code>
     *          is <code>null</code>
     */

    protected void putRelationship(org.osid.relationship.Relationship relationship) {
        this.relationships.put(relationship.getId(), relationship);
        return;
    }


    /**
     *  Makes an array of relationships available in this session.
     *
     *  @param  relationships an array of relationships
     *  @throws org.osid.NullArgumentException <code>relationships<code>
     *          is <code>null</code>
     */

    protected void putRelationships(org.osid.relationship.Relationship[] relationships) {
        putRelationships(java.util.Arrays.asList(relationships));
        return;
    }


    /**
     *  Makes a collection of relationships available in this session.
     *
     *  @param  relationships a collection of relationships
     *  @throws org.osid.NullArgumentException <code>relationships<code>
     *          is <code>null</code>
     */

    protected void putRelationships(java.util.Collection<? extends org.osid.relationship.Relationship> relationships) {
        for (org.osid.relationship.Relationship relationship : relationships) {
            this.relationships.put(relationship.getId(), relationship);
        }

        return;
    }


    /**
     *  Removes a Relationship from this session.
     *
     *  @param  relationshipId the <code>Id</code> of the relationship
     *  @throws org.osid.NullArgumentException <code>relationshipId<code> is
     *          <code>null</code>
     */

    protected void removeRelationship(org.osid.id.Id relationshipId) {
        this.relationships.remove(relationshipId);
        return;
    }


    /**
     *  Gets the <code>Relationship</code> specified by its <code>Id</code>.
     *
     *  @param  relationshipId <code>Id</code> of the <code>Relationship</code>
     *  @return the relationship
     *  @throws org.osid.NotFoundException <code>relationshipId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>relationshipId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.Relationship getRelationship(org.osid.id.Id relationshipId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.relationship.Relationship relationship = this.relationships.get(relationshipId);
        if (relationship == null) {
            throw new org.osid.NotFoundException("relationship not found: " + relationshipId);
        }

        return (relationship);
    }


    /**
     *  Gets all <code>Relationships</code>. In plenary mode, the returned
     *  list contains all known relationships or an error
     *  results. Otherwise, the returned list may contain only those
     *  relationships that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Relationships</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationships()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.relationship.relationship.ArrayRelationshipList(this.relationships.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.relationships.clear();
        super.close();
        return;
    }
}
