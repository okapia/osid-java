//
// AbstractTodoProducerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.checklist.mason.todoproducer.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractTodoProducerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.checklist.mason.TodoProducerSearchResults {

    private org.osid.checklist.mason.TodoProducerList todoProducers;
    private final org.osid.checklist.mason.TodoProducerQueryInspector inspector;
    private final java.util.Collection<org.osid.checklist.mason.records.TodoProducerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractTodoProducerSearchResults.
     *
     *  @param todoProducers the result set
     *  @param todoProducerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>todoProducers</code>
     *          or <code>todoProducerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractTodoProducerSearchResults(org.osid.checklist.mason.TodoProducerList todoProducers,
                                            org.osid.checklist.mason.TodoProducerQueryInspector todoProducerQueryInspector) {
        nullarg(todoProducers, "todo producers");
        nullarg(todoProducerQueryInspector, "todo producer query inspectpr");

        this.todoProducers = todoProducers;
        this.inspector = todoProducerQueryInspector;

        return;
    }


    /**
     *  Gets the todo producer list resulting from a search.
     *
     *  @return a todo producer list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerList getTodoProducers() {
        if (this.todoProducers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.checklist.mason.TodoProducerList todoProducers = this.todoProducers;
        this.todoProducers = null;
	return (todoProducers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.checklist.mason.TodoProducerQueryInspector getTodoProducerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  todo producer search record <code> Type. </code> This method must
     *  be used to retrieve a todoProducer implementing the requested
     *  record.
     *
     *  @param todoProducerSearchRecordType a todoProducer search 
     *         record type 
     *  @return the todo producer search
     *  @throws org.osid.NullArgumentException
     *          <code>todoProducerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(todoProducerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.checklist.mason.records.TodoProducerSearchResultsRecord getTodoProducerSearchResultsRecord(org.osid.type.Type todoProducerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.checklist.mason.records.TodoProducerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(todoProducerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(todoProducerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record todo producer search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addTodoProducerRecord(org.osid.checklist.mason.records.TodoProducerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "todo producer record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
