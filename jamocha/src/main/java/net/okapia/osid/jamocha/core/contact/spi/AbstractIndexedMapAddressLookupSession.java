//
// AbstractIndexedMapAddressLookupSession.java
//
//    A simple framework for providing an Address lookup service
//    backed by a fixed collection of addresses with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.contact.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an Address lookup service backed by a
 *  fixed collection of addresses. The addresses are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some addresses may be compatible
 *  with more types than are indicated through these address
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Addresses</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapAddressLookupSession
    extends AbstractMapAddressLookupSession
    implements org.osid.contact.AddressLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.contact.Address> addressesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.contact.Address>());
    private final MultiMap<org.osid.type.Type, org.osid.contact.Address> addressesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.contact.Address>());


    /**
     *  Makes an <code>Address</code> available in this session.
     *
     *  @param  address an address
     *  @throws org.osid.NullArgumentException <code>address<code> is
     *          <code>null</code>
     */

    @Override
    protected void putAddress(org.osid.contact.Address address) {
        super.putAddress(address);

        this.addressesByGenus.put(address.getGenusType(), address);
        
        try (org.osid.type.TypeList types = address.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.addressesByRecord.put(types.getNextType(), address);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an address from this session.
     *
     *  @param addressId the <code>Id</code> of the address
     *  @throws org.osid.NullArgumentException <code>addressId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeAddress(org.osid.id.Id addressId) {
        org.osid.contact.Address address;
        try {
            address = getAddress(addressId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.addressesByGenus.remove(address.getGenusType());

        try (org.osid.type.TypeList types = address.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.addressesByRecord.remove(types.getNextType(), address);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeAddress(addressId);
        return;
    }


    /**
     *  Gets an <code>AddressList</code> corresponding to the given
     *  address genus <code>Type</code> which does not include
     *  addresses of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known addresses or an error results. Otherwise,
     *  the returned list may contain only those addresses that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  addressGenusType an address genus type 
     *  @return the returned <code>Address</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>addressGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressList getAddressesByGenusType(org.osid.type.Type addressGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.contact.address.ArrayAddressList(this.addressesByGenus.get(addressGenusType)));
    }


    /**
     *  Gets an <code>AddressList</code> containing the given
     *  address record <code>Type</code>. In plenary mode, the
     *  returned list contains all known addresses or an error
     *  results. Otherwise, the returned list may contain only those
     *  addresses that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  addressRecordType an address record type 
     *  @return the returned <code>address</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>addressRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressList getAddressesByRecordType(org.osid.type.Type addressRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.contact.address.ArrayAddressList(this.addressesByRecord.get(addressRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.addressesByGenus.clear();
        this.addressesByRecord.clear();

        super.close();

        return;
    }
}
