//
// AbstractItem.java
//
//     Defines an Item builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.ordering.item.spi;


/**
 *  Defines an <code>Item</code> builder.
 */

public abstract class AbstractItemBuilder<T extends AbstractItemBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.ordering.item.ItemMiter item;


    /**
     *  Constructs a new <code>AbstractItemBuilder</code>.
     *
     *  @param item the item to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractItemBuilder(net.okapia.osid.jamocha.builder.ordering.item.ItemMiter item) {
        super(item);
        this.item = item;
        return;
    }


    /**
     *  Builds the item.
     *
     *  @return the new item
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.ordering.Item build() {
        (new net.okapia.osid.jamocha.builder.validator.ordering.item.ItemValidator(getValidations())).validate(this.item);
        return (new net.okapia.osid.jamocha.builder.ordering.item.ImmutableItem(this.item));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the item miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.ordering.item.ItemMiter getMiter() {
        return (this.item);
    }


    /**
     *  Sets the order.
     *
     *  @param order an order
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>order</code> is <code>null</code>
     */

    public T order(org.osid.ordering.Order order) {
        getMiter().setOrder(order);
        return (self());
    }


    /**
     *  Sets the derived flag.
     *
     *  @return the builder
     */

    public T derived() {
        getMiter().setDerived(true);
        return (self());
    }


    /**
     *  Unsets the derived flag.
     *
     *  @return the builder
     */

    public T explicit() {
        getMiter().setDerived(false);
        return (self());
    }


    /**
     *  Sets the product.
     *
     *  @param product a product
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>product</code> is <code>null</code>
     */

    public T product(org.osid.ordering.Product product) {
        getMiter().setProduct(product);
        return (self());
    }


    /**
     *  Adds an unit price.
     *
     *  @param price an unit price
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>price</code> is
     *          <code>null</code>
     */

    public T unitPrice(org.osid.ordering.Price price) {
        getMiter().addUnitPrice(price);
        return (self());
    }


    /**
     *  Sets all the unit prices.
     *
     *  @param prices a collection of unit prices
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>prices</code> is
     *          <code>null</code>
     */

    public T unitPrices(java.util.Collection<org.osid.ordering.Price> prices) {
        getMiter().setUnitPrices(prices);
        return (self());
    }


    /**
     *  Sets the quantity.
     *
     *  @param quantity a quantity
     *  @return the builder
     *  @throws org.osid.InvalidArgumentException
     *          <code>quantity</code> is negative
     */

    public T quantity(long quantity) {
        getMiter().setQuantity(quantity);
        return (self());
    }


    /**
     *  Adds a cost.
     *
     *  @param cost a cost
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>cost</code> is
     *          <code>null</code>
     */

    public T cost(org.osid.ordering.Cost cost) {
        getMiter().addCost(cost);
        return (self());
    }


    /**
     *  Sets all the costs.
     *
     *  @param costs a collection of costs
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>costs</code> is
     *          <code>null</code>
     */

    public T costs(java.util.Collection<org.osid.ordering.Cost> costs) {
        getMiter().setCosts(costs);
        return (self());
    }


    /**
     *  Adds an Item record.
     *
     *  @param record an item record
     *  @param recordType the type of item record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.ordering.records.ItemRecord record, org.osid.type.Type recordType) {
        getMiter().addItemRecord(record, recordType);
        return (self());
    }
}       


