//
// AbstractAssemblyCourseQuery.java
//
//     A CourseQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.course.course.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A CourseQuery that stores terms.
 */

public abstract class AbstractAssemblyCourseQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOperableOsidObjectQuery
    implements org.osid.course.CourseQuery,
               org.osid.course.CourseQueryInspector,
               org.osid.course.CourseSearchOrder {

    private final java.util.Collection<org.osid.course.records.CourseQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.records.CourseQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.records.CourseSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyCourseQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyCourseQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Adds a title for this query. 
     *
     *  @param  title title string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> title </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> title </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchTitle(String title, org.osid.type.Type stringMatchType, 
                           boolean match) {
        getAssembler().addStringTerm(getTitleColumn(), title, stringMatchType, match);
        return;
    }


    /**
     *  Matches a title that has any value. 
     *
     *  @param  match <code> true </code> to match courses with any title, 
     *          <code> false </code> to match courses with no title 
     */

    @OSID @Override
    public void matchAnyTitle(boolean match) {
        getAssembler().addStringWildcardTerm(getTitleColumn(), match);
        return;
    }


    /**
     *  Clears the title terms. 
     */

    @OSID @Override
    public void clearTitleTerms() {
        getAssembler().clearTerms(getTitleColumn());
        return;
    }


    /**
     *  Gets the title query terms. 
     *
     *  @return the title query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getTitleTerms() {
        return (getAssembler().getStringTerms(getTitleColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by course title. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTitle(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTitleColumn(), style);
        return;
    }


    /**
     *  Gets the Title column name.
     *
     * @return the column name
     */

    protected String getTitleColumn() {
        return ("title");
    }


    /**
     *  Adds a course number for this query. 
     *
     *  @param  number course number string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> number </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> number </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchNumber(String number, org.osid.type.Type stringMatchType, 
                            boolean match) {
        getAssembler().addStringTerm(getNumberColumn(), number, stringMatchType, match);
        return;
    }


    /**
     *  Matches a course number that has any value. 
     *
     *  @param  match <code> true </code> to match courses with any number, 
     *          <code> false </code> to match courses with no title 
     */

    @OSID @Override
    public void matchAnyNumber(boolean match) {
        getAssembler().addStringWildcardTerm(getNumberColumn(), match);
        return;
    }


    /**
     *  Clears the number terms. 
     */

    @OSID @Override
    public void clearNumberTerms() {
        getAssembler().clearTerms(getNumberColumn());
        return;
    }


    /**
     *  Gets the bumber query terms. 
     *
     *  @return the number query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getNumberTerms() {
        return (getAssembler().getStringTerms(getNumberColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by course number. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByNumber(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getNumberColumn(), style);
        return;
    }


    /**
     *  Gets the Number column name.
     *
     * @return the column name
     */

    protected String getNumberColumn() {
        return ("number");
    }


    /**
     *  Sets the resource <code> Id </code> for this query to match courses 
     *  that have a sponsor. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSponsorId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getSponsorIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the sponsor <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSponsorIdTerms() {
        getAssembler().clearTerms(getSponsorIdColumn());
        return;
    }


    /**
     *  Gets the sponsor <code> Id </code> query terms. 
     *
     *  @return the sponsor <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSponsorIdTerms() {
        return (getAssembler().getIdTerms(getSponsorIdColumn()));
    }


    /**
     *  Gets the SponsorId column name.
     *
     * @return the column name
     */

    protected String getSponsorIdColumn() {
        return ("sponsor_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSponsorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a sponsor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a resource query 
     *  @throws org.osid.UnimplementedException <code> supportsSponsorQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getSponsorQuery() {
        throw new org.osid.UnimplementedException("supportsSponsorQuery() is false");
    }


    /**
     *  Matches courses that have any sponsor. 
     *
     *  @param  match <code> true </code> to match courses with any sponsor, 
     *          <code> false </code> to match courses with no sponsors 
     */

    @OSID @Override
    public void matchAnySponsor(boolean match) {
        getAssembler().addIdWildcardTerm(getSponsorColumn(), match);
        return;
    }


    /**
     *  Clears the sponsor terms. 
     */

    @OSID @Override
    public void clearSponsorTerms() {
        getAssembler().clearTerms(getSponsorColumn());
        return;
    }


    /**
     *  Gets the sponsor query terms. 
     *
     *  @return the sponsor query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getSponsorTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the Sponsor column name.
     *
     * @return the column name
     */

    protected String getSponsorColumn() {
        return ("sponsor");
    }


    /**
     *  Sets the grade <code> Id </code> for this query to match courses 
     *  that have a credit amount.
     *
     *  @param  gradeId a grade <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCreditAmountId(org.osid.id.Id gradeId, boolean match) {
        getAssembler().addIdTerm(getCreditAmountIdColumn(), gradeId, match);
        return;
    }


    /**
     *  Clears the credit amount <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCreditAmountIdTerms() {
        getAssembler().clearTerms(getCreditAmountIdColumn());
        return;
    }


    /**
     *  Gets the credit amount <code> Id </code> query terms. 
     *
     *  @return the credit amount <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCreditAmountIdTerms() {
        return (getAssembler().getIdTerms(getCreditAmountIdColumn()));
    }


    /**
     *  Gets the CreditAmountId column name.
     *
     * @return the column name
     */

    protected String getCreditAmountIdColumn() {
        return ("credit_amount_id");
    }


    /**
     *  Tests if a <code> GradeQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreditAmountQuery() {
        return (false);
    }


    /**
     *  Gets the query for a credit amount. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a grade query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCreditAmountQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeQuery getCreditAmountQuery() {
        throw new org.osid.UnimplementedException("supportsCreditAmountQuery() is false");
    }


    /**
     *  Matches courses that have any credit amount. 
     *
     *  @param match <code> true </code> to match courses with any
     *         credit amount, <code> false </code> to match courses
     *         with no credit amount
     */

    @OSID @Override
    public void matchAnyCreditAmount(boolean match) {
        getAssembler().addIdWildcardTerm(getCreditAmountColumn(), match);
        return;
    }


    /**
     *  Clears the credit anount terms. 
     */

    @OSID @Override
    public void clearCreditAmountTerms() {
        getAssembler().clearTerms(getCreditAmountColumn());
        return;
    }


    /**
     *  Gets the creditamount query terms. 
     *
     *  @return the credit amountquery terms 
     */

    @OSID @Override
    public org.osid.grading.GradeQueryInspector[] getCreditAmountTerms() {
        return (new org.osid.grading.GradeQueryInspector[0]);
    }


    /**
     *  Gets the CreditAmount column name.
     *
     *  @return the column name
     */

    protected String getCreditAmountColumn() {
        return ("credit_amount");
    }


    /**
     *  Matches courses with the prerequisites informational string. 
     *
     *  @param  prereqInfo prerequisite informational string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> prereqInfo </code> 
     *          not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> prereqInfo </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchPrerequisitesInfo(String prereqInfo, 
                                       org.osid.type.Type stringMatchType, 
                                       boolean match) {
        getAssembler().addStringTerm(getPrerequisitesInfoColumn(), prereqInfo, stringMatchType, match);
        return;
    }


    /**
     *  Matches a course that has any prerequisite information assigned. 
     *
     *  @param  match <code> true </code> to match courses with any 
     *          prerequisite information, <code> false </code> otherwise 
     */

    @OSID @Override
    public void matchAnyPrerequisitesInfo(boolean match) {
        getAssembler().addStringWildcardTerm(getPrerequisitesInfoColumn(), match);
        return;
    }


    /**
     *  Clears the prerequisite info terms. 
     */

    @OSID @Override
    public void clearPrerequisitesInfoTerms() {
        getAssembler().clearTerms(getPrerequisitesInfoColumn());
        return;
    }


    /**
     *  Gets the prerequisite query terms. 
     *
     *  @return the prereq query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getPrerequisitesInfoTerms() {
        return (getAssembler().getStringTerms(getPrerequisitesInfoColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by course 
     *  prerequisite information. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPrerequisitesInfo(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getPrerequisitesInfoColumn(), style);
        return;
    }


    /**
     *  Gets the PrerequisitesInfo column name.
     *
     * @return the column name
     */

    protected String getPrerequisitesInfoColumn() {
        return ("prerequisites_info");
    }


    /**
     *  Sets the requisite <code> Id </code> for this query. 
     *
     *  @param  requisiteId a requisite <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> requisiteId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPrerequisitesId(org.osid.id.Id requisiteId, boolean match) {
        getAssembler().addIdTerm(getPrerequisitesIdColumn(), requisiteId, match);
        return;
    }


    /**
     *  Clears the requisite <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPrerequisitesIdTerms() {
        getAssembler().clearTerms(getPrerequisitesIdColumn());
        return;
    }


    /**
     *  Gets the requisite <code> Id </code> query terms. 
     *
     *  @return the requisite <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPrerequisitesIdTerms() {
        return (getAssembler().getIdTerms(getPrerequisitesIdColumn()));
    }


    /**
     *  Gets the PrerequisitesId column name.
     *
     * @return the column name
     */

    protected String getPrerequisitesIdColumn() {
        return ("prerequisites_id");
    }


    /**
     *  Tests if a <code> RequisiteQuery </code> is available. 
     *
     *  @return <code> true </code> if a requisite query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPrerequisitesQuery() {
        return (false);
    }


    /**
     *  Gets the query for a requisite. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a prerequisites query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPrerequisitesQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteQuery getPrerequisitesQuery() {
        throw new org.osid.UnimplementedException("supportsPrerequisitesQuery() is false");
    }


    /**
     *  Matches courses that have any prerequisites. 
     *
     *  @param  match <code> true </code> to match courses with any 
     *          prerequisites, <code> false </code> to match courses with no 
     *          prerequisites 
     */

    @OSID @Override
    public void matchAnyPrerequisites(boolean match) {
        getAssembler().addIdWildcardTerm(getPrerequisitesColumn(), match);
        return;
    }


    /**
     *  Clears the prerequisites terms. 
     */

    @OSID @Override
    public void clearPrerequisitesTerms() {
        getAssembler().clearTerms(getPrerequisitesColumn());
        return;
    }


    /**
     *  Gets the requisite query terms. 
     *
     *  @return the requisite query terms 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteQueryInspector[] getPrerequisitesTerms() {
        return (new org.osid.course.requisite.RequisiteQueryInspector[0]);
    }


    /**
     *  Gets the Prerequisites column name.
     *
     * @return the column name
     */

    protected String getPrerequisitesColumn() {
        return ("prerequisites");
    }


    /**
     *  Sets the grade <code> Id </code> for this query. 
     *
     *  @param  gradeId a grade <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLevelId(org.osid.id.Id gradeId, boolean match) {
        getAssembler().addIdTerm(getLevelIdColumn(), gradeId, match);
        return;
    }


    /**
     *  Clears the grade <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearLevelIdTerms() {
        getAssembler().clearTerms(getLevelIdColumn());
        return;
    }


    /**
     *  Gets the grade level <code> Id </code> query terms. 
     *
     *  @return the grade <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLevelIdTerms() {
        return (getAssembler().getIdTerms(getLevelIdColumn()));
    }


    /**
     *  Gets the LevelId column name.
     *
     * @return the column name
     */

    protected String getLevelIdColumn() {
        return ("level_id");
    }


    /**
     *  Tests if a <code> GradeQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLevelQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade level. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a grade query 
     *  @throws org.osid.UnimplementedException <code> supportsLevelQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeQuery getLevelQuery() {
        throw new org.osid.UnimplementedException("supportsLevelQuery() is false");
    }


    /**
     *  Matches courses that have any grade level. 
     *
     *  @param  match <code> true </code> to match courses with any level, 
     *          <code> false </code> to match courses with no level 
     */

    @OSID @Override
    public void matchAnyLevel(boolean match) {
        getAssembler().addIdWildcardTerm(getLevelColumn(), match);
        return;
    }


    /**
     *  Clears the level terms. 
     */

    @OSID @Override
    public void clearLevelTerms() {
        getAssembler().clearTerms(getLevelColumn());
        return;
    }


    /**
     *  Gets the grade level query terms. 
     *
     *  @return the grade query terms 
     */

    @OSID @Override
    public org.osid.grading.GradeQueryInspector[] getLevelTerms() {
        return (new org.osid.grading.GradeQueryInspector[0]);
    }


    /**
     *  Gets the Level column name.
     *
     * @return the column name
     */

    protected String getLevelColumn() {
        return ("level");
    }


    /**
     *  Sets the grade system <code> Id </code> for this query. 
     *
     *  @param  gradeSystemId a grade system <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeSystemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGradingOptionId(org.osid.id.Id gradeSystemId, 
                                     boolean match) {
        getAssembler().addIdTerm(getGradingOptionIdColumn(), gradeSystemId, match);
        return;
    }


    /**
     *  Clears the grade system <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradingOptionIdTerms() {
        getAssembler().clearTerms(getGradingOptionIdColumn());
        return;
    }


    /**
     *  Gets the grade system <code> Id </code> query terms. 
     *
     *  @return the grade system <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradingOptionIdTerms() {
        return (getAssembler().getIdTerms(getGradingOptionIdColumn()));
    }


    /**
     *  Gets the GradingOptionId column name.
     *
     * @return the column name
     */

    protected String getGradingOptionIdColumn() {
        return ("grading_option_id");
    }


    /**
     *  Tests if a <code> GradeSystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade system query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradingOptionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grading option. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a grade system query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradingOptionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQuery getGradingOptionQuery() {
        throw new org.osid.UnimplementedException("supportsGradingOptionQuery() is false");
    }


    /**
     *  Matches courses that have any grading option. 
     *
     *  @param  match <code> true </code> to match courses with any grading 
     *          option, <code> false </code> to match courses with no grading 
     *          options 
     */

    @OSID @Override
    public void matchAnyGradingOption(boolean match) {
        getAssembler().addIdWildcardTerm(getGradingOptionColumn(), match);
        return;
    }


    /**
     *  Clears the grading option terms. 
     */

    @OSID @Override
    public void clearGradingOptionTerms() {
        getAssembler().clearTerms(getGradingOptionColumn());
        return;
    }


    /**
     *  Gets the grade system query terms. 
     *
     *  @return the grade system terms 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQueryInspector[] getGradingOptionTerms() {
        return (new org.osid.grading.GradeSystemQueryInspector[0]);
    }


    /**
     *  Gets the GradingOption column name.
     *
     * @return the column name
     */

    protected String getGradingOptionColumn() {
        return ("grading_option");
    }


    /**
     *  Sets the objective <code> Id </code> for this query. 
     *
     *  @param  objectiveId an objective <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> objectiveId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLearningObjectiveId(org.osid.id.Id objectiveId, 
                                         boolean match) {
        getAssembler().addIdTerm(getLearningObjectiveIdColumn(), objectiveId, match);
        return;
    }


    /**
     *  Clears the objective <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearLearningObjectiveIdTerms() {
        getAssembler().clearTerms(getLearningObjectiveIdColumn());
        return;
    }


    /**
     *  Gets the objective <code> Id </code> query terms. 
     *
     *  @return the objective <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLearningObjectiveIdTerms() {
        return (getAssembler().getIdTerms(getLearningObjectiveIdColumn()));
    }


    /**
     *  Gets the LearningObjectiveId column name.
     *
     * @return the column name
     */

    protected String getLearningObjectiveIdColumn() {
        return ("learning_objective_id");
    }


    /**
     *  Tests if a <code> ObjectiveQuery </code> is available. 
     *
     *  @return <code> true </code> if an objective query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLearningObjectiveQuery() {
        return (false);
    }


    /**
     *  Gets the query for a learning objective. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return an objective query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLearningObjectiveQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQuery getLearningObjectiveQuery() {
        throw new org.osid.UnimplementedException("supportsLearningObjectiveQuery() is false");
    }


    /**
     *  Matches courses that have any learning objective. 
     *
     *  @param  match <code> true </code> to match courses with any learning 
     *          objective, <code> false </code> to match courses with no 
     *          learning objectives 
     */

    @OSID @Override
    public void matchAnyLearningObjective(boolean match) {
        getAssembler().addIdWildcardTerm(getLearningObjectiveColumn(), match);
        return;
    }


    /**
     *  Clears the learning objective terms. 
     */

    @OSID @Override
    public void clearLearningObjectiveTerms() {
        getAssembler().clearTerms(getLearningObjectiveColumn());
        return;
    }


    /**
     *  Gets the objective query terms. 
     *
     *  @return the objective query terms 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQueryInspector[] getLearningObjectiveTerms() {
        return (new org.osid.learning.ObjectiveQueryInspector[0]);
    }


    /**
     *  Gets the LearningObjective column name.
     *
     * @return the column name
     */

    protected String getLearningObjectiveColumn() {
        return ("learning_objective");
    }


    /**
     *  Sets the course offering <code> Id </code> for this query to match 
     *  courses that have a related course offering. 
     *
     *  @param  courseOfferingId a course offering <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseOfferingId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchActivityUnitId(org.osid.id.Id courseOfferingId, 
                                    boolean match) {
        getAssembler().addIdTerm(getActivityUnitIdColumn(), courseOfferingId, match);
        return;
    }


    /**
     *  Clears the activity unit <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearActivityUnitIdTerms() {
        getAssembler().clearTerms(getActivityUnitIdColumn());
        return;
    }


    /**
     *  Gets the activity unit <code> Id </code> query terms. 
     *
     *  @return the activity unit <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActivityUnitIdTerms() {
        return (getAssembler().getIdTerms(getActivityUnitIdColumn()));
    }


    /**
     *  Gets the ActivityUnitId column name.
     *
     * @return the column name
     */

    protected String getActivityUnitIdColumn() {
        return ("activity_unit_id");
    }


    /**
     *  Tests if a <code> ActivityUnitQuery </code> is available. 
     *
     *  @return <code> true </code> if a activity unit query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityUnitQuery() {
        return (false);
    }


    /**
     *  Gets the query for an activity unit. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the activity unit query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitQuery getActivityUnitQuery() {
        throw new org.osid.UnimplementedException("supportsActivityUnitQuery() is false");
    }


    /**
     *  Matches courses that have any course offering. 
     *
     *  @param  match <code> true </code> to match courses with any related 
     *          activity unit, <code> false </code> to match courses with no 
     *          activity units 
     */

    @OSID @Override
    public void matchAnyActivityUnit(boolean match) {
        getAssembler().addIdWildcardTerm(getActivityUnitColumn(), match);
        return;
    }


    /**
     *  Clears the activity unit terms. 
     */

    @OSID @Override
    public void clearActivityUnitTerms() {
        getAssembler().clearTerms(getActivityUnitColumn());
        return;
    }


    /**
     *  Gets the activity unit query terms. 
     *
     *  @return the activity unit query terms 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitQueryInspector[] getActivityUnitTerms() {
        return (new org.osid.course.ActivityUnitQueryInspector[0]);
    }


    /**
     *  Gets the ActivityUnit column name.
     *
     * @return the column name
     */

    protected String getActivityUnitColumn() {
        return ("activity_unit");
    }


    /**
     *  Sets the course offering <code> Id </code> for this query to match 
     *  courses that have a related course offering. 
     *
     *  @param  courseOfferingId a course offering <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseOfferingId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseOfferingId(org.osid.id.Id courseOfferingId, 
                                      boolean match) {
        getAssembler().addIdTerm(getCourseOfferingIdColumn(), courseOfferingId, match);
        return;
    }


    /**
     *  Clears the course offering <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseOfferingIdTerms() {
        getAssembler().clearTerms(getCourseOfferingIdColumn());
        return;
    }


    /**
     *  Gets the course offering <code> Id </code> query terms. 
     *
     *  @return the course offering <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseOfferingIdTerms() {
        return (getAssembler().getIdTerms(getCourseOfferingIdColumn()));
    }


    /**
     *  Gets the CourseOfferingId column name.
     *
     * @return the column name
     */

    protected String getCourseOfferingIdColumn() {
        return ("course_offering_id");
    }


    /**
     *  Tests if a <code> CourseOfferingQuery </code> is available. 
     *
     *  @return <code> true </code> if a course offering query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseOfferingQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course offering. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course offering query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingQuery getCourseOfferingQuery() {
        throw new org.osid.UnimplementedException("supportsCourseOfferingQuery() is false");
    }


    /**
     *  Matches courses that have any course offering. 
     *
     *  @param  match <code> true </code> to match courses with any course 
     *          offering, <code> false </code> to match courses with no course 
     *          offering 
     */

    @OSID @Override
    public void matchAnyCourseOffering(boolean match) {
        getAssembler().addIdWildcardTerm(getCourseOfferingColumn(), match);
        return;
    }


    /**
     *  Clears the course offering terms. 
     */

    @OSID @Override
    public void clearCourseOfferingTerms() {
        getAssembler().clearTerms(getCourseOfferingColumn());
        return;
    }


    /**
     *  Gets the course offering query terms. 
     *
     *  @return the course offering query terms 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingQueryInspector[] getCourseOfferingTerms() {
        return (new org.osid.course.CourseOfferingQueryInspector[0]);
    }


    /**
     *  Gets the CourseOffering column name.
     *
     * @return the column name
     */

    protected String getCourseOfferingColumn() {
        return ("course_offering");
    }


    /**
     *  Sets the course catalog <code> Id </code> for this query to match 
     *  courses assigned to course catalogs. 
     *
     *  @param  courseCatalogId the course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        getAssembler().addIdTerm(getCourseCatalogIdColumn(), courseCatalogId, match);
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        getAssembler().clearTerms(getCourseCatalogIdColumn());
        return;
    }


    /**
     *  Gets the course catalog <code> Id </code> query terms. 
     *
     *  @return the course catalog <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseCatalogIdTerms() {
        return (getAssembler().getIdTerms(getCourseCatalogIdColumn()));
    }


    /**
     *  Gets the CourseCatalogId column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogIdColumn() {
        return ("course_catalog_id");
    }


    /**
     *  Tests if a <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        getAssembler().clearTerms(getCourseCatalogColumn());
        return;
    }


    /**
     *  Gets the course catalog query terms. 
     *
     *  @return the course catalog query terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }


    /**
     *  Gets the CourseCatalog column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogColumn() {
        return ("course_catalog");
    }


    /**
     *  Tests if this course supports the given record
     *  <code>Type</code>.
     *
     *  @param  courseRecordType a course record type 
     *  @return <code>true</code> if the courseRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>courseRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type courseRecordType) {
        for (org.osid.course.records.CourseQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(courseRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  courseRecordType the course record type 
     *  @return the course query record 
     *  @throws org.osid.NullArgumentException
     *          <code>courseRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(courseRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.CourseQueryRecord getCourseQueryRecord(org.osid.type.Type courseRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.records.CourseQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(courseRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(courseRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  courseRecordType the course record type 
     *  @return the course query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>courseRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(courseRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.CourseQueryInspectorRecord getCourseQueryInspectorRecord(org.osid.type.Type courseRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.records.CourseQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(courseRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(courseRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param courseRecordType the course record type
     *  @return the course search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>courseRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(courseRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.CourseSearchOrderRecord getCourseSearchOrderRecord(org.osid.type.Type courseRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.records.CourseSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(courseRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(courseRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this course. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param courseQueryRecord the course query record
     *  @param courseQueryInspectorRecord the course query inspector
     *         record
     *  @param courseSearchOrderRecord the course search order record
     *  @param courseRecordType course record type
     *  @throws org.osid.NullArgumentException
     *          <code>courseQueryRecord</code>,
     *          <code>courseQueryInspectorRecord</code>,
     *          <code>courseSearchOrderRecord</code> or
     *          <code>courseRecordTypecourse</code> is
     *          <code>null</code>
     */
            
    protected void addCourseRecords(org.osid.course.records.CourseQueryRecord courseQueryRecord, 
                                      org.osid.course.records.CourseQueryInspectorRecord courseQueryInspectorRecord, 
                                      org.osid.course.records.CourseSearchOrderRecord courseSearchOrderRecord, 
                                      org.osid.type.Type courseRecordType) {

        addRecordType(courseRecordType);

        nullarg(courseQueryRecord, "course query record");
        nullarg(courseQueryInspectorRecord, "course query inspector record");
        nullarg(courseSearchOrderRecord, "course search odrer record");

        this.queryRecords.add(courseQueryRecord);
        this.queryInspectorRecords.add(courseQueryInspectorRecord);
        this.searchOrderRecords.add(courseSearchOrderRecord);
        
        return;
    }
}
