//
// AbstractPriceSchedule.java
//
//     Defines a PriceSchedule.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.priceschedule.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>PriceSchedule</code>.
 */

public abstract class AbstractPriceSchedule
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.ordering.PriceSchedule {

    private final java.util.Collection<org.osid.ordering.Price> prices = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.ordering.records.PriceScheduleRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Ids </code> of the prices. 
     *
     *  @return the price <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getPriceIds() {
        try {
            org.osid.ordering.PriceList prices = getPrices();
            return (new net.okapia.osid.jamocha.adapter.converter.ordering.price.PriceToIdList(prices));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the prices. 
     *
     *  @return the prices 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.ordering.PriceList getPrices()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.ordering.price.ArrayPriceList(this.prices));
    }


    /**
     *  Adds a price.
     *
     *  @param price a price
     *  @throws org.osid.NullArgumentException
     *          <code>price</code> is <code>null</code>
     */

    protected void addPrice(org.osid.ordering.Price price) {
        nullarg(price, "price");
        this.prices.add(price);
        return;
    }


    /**
     *  Sets all the prices.
     *
     *  @param prices a collection of prices
     *  @throws org.osid.NullArgumentException
     *          <code>prices</code> is <code>null</code>
     */

    protected void setPrices(java.util.Collection<org.osid.ordering.Price> prices) {
        nullarg(prices, "prices");
        this.prices.clear();
        this.prices.addAll(prices);
        return;
    }


    /**
     *  Tests if this priceSchedule supports the given record
     *  <code>Type</code>.
     *
     *  @param  priceScheduleRecordType a price schedule record type 
     *  @return <code>true</code> if the priceScheduleRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>priceScheduleRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type priceScheduleRecordType) {
        for (org.osid.ordering.records.PriceScheduleRecord record : this.records) {
            if (record.implementsRecordType(priceScheduleRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>PriceSchedule</code> record <code>Type</code>.
     *
     *  @param  priceScheduleRecordType the price schedule record type 
     *  @return the price schedule record 
     *  @throws org.osid.NullArgumentException
     *          <code>priceScheduleRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(priceScheduleRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.PriceScheduleRecord getPriceScheduleRecord(org.osid.type.Type priceScheduleRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.records.PriceScheduleRecord record : this.records) {
            if (record.implementsRecordType(priceScheduleRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(priceScheduleRecordType + " is not supported");
    }


    /**
     *  Adds a record to this price schedule. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param priceScheduleRecord the price schedule record
     *  @param priceScheduleRecordType price schedule record type
     *  @throws org.osid.NullArgumentException
     *          <code>priceScheduleRecord</code> or
     *          <code>priceScheduleRecordTypepriceSchedule</code> is
     *          <code>null</code>
     */
            
    protected void addPriceScheduleRecord(org.osid.ordering.records.PriceScheduleRecord priceScheduleRecord, 
                                          org.osid.type.Type priceScheduleRecordType) {
        
        nullarg(priceScheduleRecord, "price schedule record");
        addRecordType(priceScheduleRecordType);
        this.records.add(priceScheduleRecord);
        
        return;
    }
}
