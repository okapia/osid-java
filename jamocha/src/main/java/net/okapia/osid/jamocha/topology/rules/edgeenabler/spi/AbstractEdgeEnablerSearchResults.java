//
// AbstractEdgeEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.topology.rules.edgeenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractEdgeEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.topology.rules.EdgeEnablerSearchResults {

    private org.osid.topology.rules.EdgeEnablerList edgeEnablers;
    private final org.osid.topology.rules.EdgeEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.topology.rules.records.EdgeEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractEdgeEnablerSearchResults.
     *
     *  @param edgeEnablers the result set
     *  @param edgeEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>edgeEnablers</code>
     *          or <code>edgeEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractEdgeEnablerSearchResults(org.osid.topology.rules.EdgeEnablerList edgeEnablers,
                                            org.osid.topology.rules.EdgeEnablerQueryInspector edgeEnablerQueryInspector) {
        nullarg(edgeEnablers, "edge enablers");
        nullarg(edgeEnablerQueryInspector, "edge enabler query inspectpr");

        this.edgeEnablers = edgeEnablers;
        this.inspector = edgeEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the edge enabler list resulting from a search.
     *
     *  @return an edge enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerList getEdgeEnablers() {
        if (this.edgeEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.topology.rules.EdgeEnablerList edgeEnablers = this.edgeEnablers;
        this.edgeEnablers = null;
	return (edgeEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.topology.rules.EdgeEnablerQueryInspector getEdgeEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  edge enabler search record <code> Type. </code> This method must
     *  be used to retrieve an edgeEnabler implementing the requested
     *  record.
     *
     *  @param edgeEnablerSearchRecordType an edgeEnabler search 
     *         record type 
     *  @return the edge enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>edgeEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(edgeEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.topology.rules.records.EdgeEnablerSearchResultsRecord getEdgeEnablerSearchResultsRecord(org.osid.type.Type edgeEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.topology.rules.records.EdgeEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(edgeEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(edgeEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record edge enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addEdgeEnablerRecord(org.osid.topology.rules.records.EdgeEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "edge enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
