//
// AbstractQueryTodoProducerLookupSession.java
//
//    An inline adapter that maps a TodoProducerLookupSession to
//    a TodoProducerQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.checklist.mason.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a TodoProducerLookupSession to
 *  a TodoProducerQuerySession.
 */

public abstract class AbstractQueryTodoProducerLookupSession
    extends net.okapia.osid.jamocha.checklist.mason.spi.AbstractTodoProducerLookupSession
    implements org.osid.checklist.mason.TodoProducerLookupSession {

    private boolean activeonly    = false;    
    private final org.osid.checklist.mason.TodoProducerQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryTodoProducerLookupSession.
     *
     *  @param querySession the underlying todo producer query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryTodoProducerLookupSession(org.osid.checklist.mason.TodoProducerQuerySession querySession) {
        nullarg(querySession, "todo producer query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Checklist</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Checklist Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getChecklistId() {
        return (this.session.getChecklistId());
    }


    /**
     *  Gets the <code>Checklist</code> associated with this 
     *  session.
     *
     *  @return the <code>Checklist</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.Checklist getChecklist()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getChecklist());
    }


    /**
     *  Tests if this user can perform <code>TodoProducer</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupTodoProducers() {
        return (this.session.canSearchTodoProducers());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include todo producers in checklists which are children
     *  of this checklist in the checklist hierarchy.
     */

    @OSID @Override
    public void useFederatedChecklistView() {
        this.session.useFederatedChecklistView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this checklist only.
     */

    @OSID @Override
    public void useIsolatedChecklistView() {
        this.session.useIsolatedChecklistView();
        return;
    }
    

    /**
     *  Only active todo producers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveTodoProducerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive todo producers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusTodoProducerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>TodoProducer</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>TodoProducer</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>TodoProducer</code> and
     *  retained for compatibility.
     *
     *  In active mode, todo producers are returned that are currently
     *  active. In any status mode, active and inactive todo producers
     *  are returned.
     *
     *  @param  todoProducerId <code>Id</code> of the
     *          <code>TodoProducer</code>
     *  @return the todo producer
     *  @throws org.osid.NotFoundException <code>todoProducerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>todoProducerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducer getTodoProducer(org.osid.id.Id todoProducerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.checklist.mason.TodoProducerQuery query = getQuery();
        query.matchId(todoProducerId, true);
        org.osid.checklist.mason.TodoProducerList todoProducers = this.session.getTodoProducersByQuery(query);
        if (todoProducers.hasNext()) {
            return (todoProducers.getNextTodoProducer());
        } 
        
        throw new org.osid.NotFoundException(todoProducerId + " not found");
    }


    /**
     *  Gets a <code>TodoProducerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  todoProducers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>TodoProducers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, todo producers are returned that are currently
     *  active. In any status mode, active and inactive todo producers
     *  are returned.
     *
     *  @param  todoProducerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>TodoProducer</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>todoProducerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerList getTodoProducersByIds(org.osid.id.IdList todoProducerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.checklist.mason.TodoProducerQuery query = getQuery();

        try (org.osid.id.IdList ids = todoProducerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getTodoProducersByQuery(query));
    }


    /**
     *  Gets a <code>TodoProducerList</code> corresponding to the given
     *  todo producer genus <code>Type</code> which does not include
     *  todo producers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  todo producers or an error results. Otherwise, the returned list
     *  may contain only those todo producers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, todo producers are returned that are currently
     *  active. In any status mode, active and inactive todo producers
     *  are returned.
     *
     *  @param  todoProducerGenusType a todoProducer genus type 
     *  @return the returned <code>TodoProducer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>todoProducerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerList getTodoProducersByGenusType(org.osid.type.Type todoProducerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.checklist.mason.TodoProducerQuery query = getQuery();
        query.matchGenusType(todoProducerGenusType, true);
        return (this.session.getTodoProducersByQuery(query));
    }


    /**
     *  Gets a <code>TodoProducerList</code> corresponding to the given
     *  todo producer genus <code>Type</code> and include any additional
     *  todo producers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  todo producers or an error results. Otherwise, the returned list
     *  may contain only those todo producers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, todo producers are returned that are currently
     *  active. In any status mode, active and inactive todo producers
     *  are returned.
     *
     *  @param  todoProducerGenusType a todoProducer genus type 
     *  @return the returned <code>TodoProducer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>todoProducerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerList getTodoProducersByParentGenusType(org.osid.type.Type todoProducerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.checklist.mason.TodoProducerQuery query = getQuery();
        query.matchParentGenusType(todoProducerGenusType, true);
        return (this.session.getTodoProducersByQuery(query));
    }


    /**
     *  Gets a <code>TodoProducerList</code> containing the given
     *  todo producer record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  todo producers or an error results. Otherwise, the returned list
     *  may contain only those todo producers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, todo producers are returned that are currently
     *  active. In any status mode, active and inactive todo producers
     *  are returned.
     *
     *  @param  todoProducerRecordType a todoProducer record type 
     *  @return the returned <code>TodoProducer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>todoProducerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerList getTodoProducersByRecordType(org.osid.type.Type todoProducerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.checklist.mason.TodoProducerQuery query = getQuery();
        query.matchRecordType(todoProducerRecordType, true);
        return (this.session.getTodoProducersByQuery(query));
    }

    
    /**
     *  Gets all <code>TodoProducers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  todo producers or an error results. Otherwise, the returned list
     *  may contain only those todo producers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, todo producers are returned that are currently
     *  active. In any status mode, active and inactive todo producers
     *  are returned.
     *
     *  @return a list of <code>TodoProducers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerList getTodoProducers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.checklist.mason.TodoProducerQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getTodoProducersByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.checklist.mason.TodoProducerQuery getQuery() {
        org.osid.checklist.mason.TodoProducerQuery query = this.session.getTodoProducerQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
