//
// AbstractNodeDictionaryHierarchySession.java
//
//     Defines a Dictionary hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.dictionary.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a dictionary hierarchy session for delivering a hierarchy
 *  of dictionaries using the DictionaryNode interface.
 */

public abstract class AbstractNodeDictionaryHierarchySession
    extends net.okapia.osid.jamocha.dictionary.spi.AbstractDictionaryHierarchySession
    implements org.osid.dictionary.DictionaryHierarchySession {

    private java.util.Collection<org.osid.dictionary.DictionaryNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root dictionary <code> Ids </code> in this hierarchy.
     *
     *  @return the root dictionary <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootDictionaryIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.dictionary.dictionarynode.DictionaryNodeToIdList(this.roots));
    }


    /**
     *  Gets the root dictionaries in the dictionary hierarchy. A node
     *  with no parents is an orphan. While all dictionary <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root dictionaries 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryList getRootDictionaries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.dictionary.dictionarynode.DictionaryNodeToDictionaryList(new net.okapia.osid.jamocha.dictionary.dictionarynode.ArrayDictionaryNodeList(this.roots)));
    }


    /**
     *  Adds a root dictionary node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootDictionary(org.osid.dictionary.DictionaryNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root dictionary nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootDictionaries(java.util.Collection<org.osid.dictionary.DictionaryNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root dictionary node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootDictionary(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.dictionary.DictionaryNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Dictionary </code> has any parents. 
     *
     *  @param  dictionaryId a dictionary <code> Id </code> 
     *  @return <code> true </code> if the dictionary has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> dictionaryId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> dictionaryId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentDictionaries(org.osid.id.Id dictionaryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getDictionaryNode(dictionaryId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  dictionary.
     *
     *  @param  id an <code> Id </code> 
     *  @param  dictionaryId the <code> Id </code> of a dictionary 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> dictionaryId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> dictionaryId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> dictionaryId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfDictionary(org.osid.id.Id id, org.osid.id.Id dictionaryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.dictionary.DictionaryNodeList parents = getDictionaryNode(dictionaryId).getParentDictionaryNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextDictionaryNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given dictionary. 
     *
     *  @param  dictionaryId a dictionary <code> Id </code> 
     *  @return the parent <code> Ids </code> of the dictionary 
     *  @throws org.osid.NotFoundException <code> dictionaryId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> dictionaryId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentDictionaryIds(org.osid.id.Id dictionaryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.dictionary.dictionary.DictionaryToIdList(getParentDictionaries(dictionaryId)));
    }


    /**
     *  Gets the parents of the given dictionary. 
     *
     *  @param  dictionaryId the <code> Id </code> to query 
     *  @return the parents of the dictionary 
     *  @throws org.osid.NotFoundException <code> dictionaryId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> dictionaryId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryList getParentDictionaries(org.osid.id.Id dictionaryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.dictionary.dictionarynode.DictionaryNodeToDictionaryList(getDictionaryNode(dictionaryId).getParentDictionaryNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  dictionary.
     *
     *  @param  id an <code> Id </code> 
     *  @param  dictionaryId the Id of a dictionary 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> dictionaryId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> dictionaryId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> dictionaryId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfDictionary(org.osid.id.Id id, org.osid.id.Id dictionaryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfDictionary(id, dictionaryId)) {
            return (true);
        }

        try (org.osid.dictionary.DictionaryList parents = getParentDictionaries(dictionaryId)) {
            while (parents.hasNext()) {
                if (isAncestorOfDictionary(id, parents.getNextDictionary().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a dictionary has any children. 
     *
     *  @param  dictionaryId a dictionary <code> Id </code> 
     *  @return <code> true </code> if the <code> dictionaryId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> dictionaryId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> dictionaryId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildDictionaries(org.osid.id.Id dictionaryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getDictionaryNode(dictionaryId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  dictionary.
     *
     *  @param  id an <code> Id </code> 
     *  @param dictionaryId the <code> Id </code> of a 
     *         dictionary
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> dictionaryId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> dictionaryId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> dictionaryId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfDictionary(org.osid.id.Id id, org.osid.id.Id dictionaryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfDictionary(dictionaryId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  dictionary.
     *
     *  @param  dictionaryId the <code> Id </code> to query 
     *  @return the children of the dictionary 
     *  @throws org.osid.NotFoundException <code> dictionaryId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> dictionaryId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildDictionaryIds(org.osid.id.Id dictionaryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.dictionary.dictionary.DictionaryToIdList(getChildDictionaries(dictionaryId)));
    }


    /**
     *  Gets the children of the given dictionary. 
     *
     *  @param  dictionaryId the <code> Id </code> to query 
     *  @return the children of the dictionary 
     *  @throws org.osid.NotFoundException <code> dictionaryId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> dictionaryId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryList getChildDictionaries(org.osid.id.Id dictionaryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.dictionary.dictionarynode.DictionaryNodeToDictionaryList(getDictionaryNode(dictionaryId).getChildDictionaryNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  dictionary.
     *
     *  @param  id an <code> Id </code> 
     *  @param dictionaryId the <code> Id </code> of a 
     *         dictionary
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> dictionaryId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> dictionaryId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> dictionaryId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfDictionary(org.osid.id.Id id, org.osid.id.Id dictionaryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfDictionary(dictionaryId, id)) {
            return (true);
        }

        try (org.osid.dictionary.DictionaryList children = getChildDictionaries(dictionaryId)) {
            while (children.hasNext()) {
                if (isDescendantOfDictionary(id, children.getNextDictionary().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  dictionary.
     *
     *  @param  dictionaryId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified dictionary node 
     *  @throws org.osid.NotFoundException <code> dictionaryId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> dictionaryId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getDictionaryNodeIds(org.osid.id.Id dictionaryId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.dictionary.dictionarynode.DictionaryNodeToNode(getDictionaryNode(dictionaryId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given dictionary.
     *
     *  @param  dictionaryId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified dictionary node 
     *  @throws org.osid.NotFoundException <code> dictionaryId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> dictionaryId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryNode getDictionaryNodes(org.osid.id.Id dictionaryId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getDictionaryNode(dictionaryId));
    }


    /**
     *  Closes this <code>DictionaryHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a dictionary node.
     *
     *  @param dictionaryId the id of the dictionary node
     *  @throws org.osid.NotFoundException <code>dictionaryId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>dictionaryId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.dictionary.DictionaryNode getDictionaryNode(org.osid.id.Id dictionaryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(dictionaryId, "dictionary Id");
        for (org.osid.dictionary.DictionaryNode dictionary : this.roots) {
            if (dictionary.getId().equals(dictionaryId)) {
                return (dictionary);
            }

            org.osid.dictionary.DictionaryNode r = findDictionary(dictionary, dictionaryId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(dictionaryId + " is not found");
    }


    protected org.osid.dictionary.DictionaryNode findDictionary(org.osid.dictionary.DictionaryNode node, 
                                                                org.osid.id.Id dictionaryId) 
	throws org.osid.OperationFailedException {

        try (org.osid.dictionary.DictionaryNodeList children = node.getChildDictionaryNodes()) {
            while (children.hasNext()) {
                org.osid.dictionary.DictionaryNode dictionary = children.getNextDictionaryNode();
                if (dictionary.getId().equals(dictionaryId)) {
                    return (dictionary);
                }
                
                dictionary = findDictionary(dictionary, dictionaryId);
                if (dictionary != null) {
                    return (dictionary);
                }
            }
        }

        return (null);
    }
}
