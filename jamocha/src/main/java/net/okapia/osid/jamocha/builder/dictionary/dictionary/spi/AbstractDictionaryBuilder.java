//
// AbstractDictionary.java
//
//     Defines a Dictionary builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.dictionary.dictionary.spi;


/**
 *  Defines a <code>Dictionary</code> builder.
 */

public abstract class AbstractDictionaryBuilder<T extends AbstractDictionaryBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidCatalogBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.dictionary.dictionary.DictionaryMiter dictionary;


    /**
     *  Constructs a new <code>AbstractDictionaryBuilder</code>.
     *
     *  @param dictionary the dictionary to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractDictionaryBuilder(net.okapia.osid.jamocha.builder.dictionary.dictionary.DictionaryMiter dictionary) {
        super(dictionary);
        this.dictionary = dictionary;
        return;
    }


    /**
     *  Builds the dictionary.
     *
     *  @return the new dictionary
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.dictionary.Dictionary build() {
        (new net.okapia.osid.jamocha.builder.validator.dictionary.dictionary.DictionaryValidator(getValidations())).validate(this.dictionary);
        return (new net.okapia.osid.jamocha.builder.dictionary.dictionary.ImmutableDictionary(this.dictionary));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the dictionary miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.dictionary.dictionary.DictionaryMiter getMiter() {
        return (this.dictionary);
    }


    /**
     *  Adds a Dictionary record.
     *
     *  @param record a dictionary record
     *  @param recordType the type of dictionary record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.dictionary.records.DictionaryRecord record, org.osid.type.Type recordType) {
        getMiter().addDictionaryRecord(record, recordType);
        return (self());
    }
}       


