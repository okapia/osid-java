//
// AbstractImmutableMeter.java
//
//     Wraps a mutable Meter to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.metering.meter.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Meter</code> to hide modifiers. This
 *  wrapper provides an immutized Meter from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying meter whose state changes are visible.
 */

public abstract class AbstractImmutableMeter
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.metering.Meter {

    private final org.osid.metering.Meter meter;


    /**
     *  Constructs a new <code>AbstractImmutableMeter</code>.
     *
     *  @param meter the meter to immutablize
     *  @throws org.osid.NullArgumentException <code>meter</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableMeter(org.osid.metering.Meter meter) {
        super(meter);
        this.meter = meter;
        return;
    }


    /**
     *  Gets the meter record corresponding to the given <code> Meter </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> meterRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(meterRecordType) </code> is <code> true </code> . 
     *
     *  @param  meterRecordType the meter record type 
     *  @return the meter record 
     *  @throws org.osid.NullArgumentException <code> meterRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(meterRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.records.MeterRecord getMeterRecord(org.osid.type.Type meterRecordType)
        throws org.osid.OperationFailedException {

        return (this.meter.getMeterRecord(meterRecordType));
    }
}

