//
// AbstractAssemblyConvocationQuery.java
//
//     A ConvocationQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.recognition.convocation.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ConvocationQuery that stores terms.
 */

public abstract class AbstractAssemblyConvocationQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidGovernatorQuery
    implements org.osid.recognition.ConvocationQuery,
               org.osid.recognition.ConvocationQueryInspector,
               org.osid.recognition.ConvocationSearchOrder {

    private final java.util.Collection<org.osid.recognition.records.ConvocationQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.recognition.records.ConvocationQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.recognition.records.ConvocationSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyConvocationQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyConvocationQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets an award <code> Id. </code> 
     *
     *  @param  awardId an award <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> awardId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAwardId(org.osid.id.Id awardId, boolean match) {
        getAssembler().addIdTerm(getAwardIdColumn(), awardId, match);
        return;
    }


    /**
     *  Clears the award <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAwardIdTerms() {
        getAssembler().clearTerms(getAwardIdColumn());
        return;
    }


    /**
     *  Gets the award <code> Id </code> terms. 
     *
     *  @return the award <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAwardIdTerms() {
        return (getAssembler().getIdTerms(getAwardIdColumn()));
    }


    /**
     *  Gets the AwardId column name.
     *
     * @return the column name
     */

    protected String getAwardIdColumn() {
        return ("award_id");
    }


    /**
     *  Tests if an <code> AwardQuery </code> is available. 
     *
     *  @return <code> true </code> if an award query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardQuery() {
        return (false);
    }


    /**
     *  Gets the query for an award query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the award query 
     *  @throws org.osid.UnimplementedException <code> supportsAwardQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardQuery getAwardQuery() {
        throw new org.osid.UnimplementedException("supportsAwardQuery() is false");
    }


    /**
     *  Matches convocations with any award. 
     *
     *  @param  match <code> true </code> to match convocations with any 
     *          award, <code> false </code> to match convocations with no 
     *          awards 
     */

    @OSID @Override
    public void matchAnyAward(boolean match) {
        getAssembler().addIdWildcardTerm(getAwardColumn(), match);
        return;
    }


    /**
     *  Clears the award terms. 
     */

    @OSID @Override
    public void clearAwardTerms() {
        getAssembler().clearTerms(getAwardColumn());
        return;
    }


    /**
     *  Gets the award terms. 
     *
     *  @return the award terms 
     */

    @OSID @Override
    public org.osid.recognition.AwardQueryInspector[] getAwardTerms() {
        return (new org.osid.recognition.AwardQueryInspector[0]);
    }


    /**
     *  Gets the Award column name.
     *
     * @return the column name
     */

    protected String getAwardColumn() {
        return ("award");
    }


    /**
     *  Matches the date between the given dates inclusive. 
     *
     *  @param  from starting date 
     *  @param  to ending date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDate(org.osid.calendaring.DateTime from, 
                          org.osid.calendaring.DateTime to, boolean match) {
        getAssembler().addDateTimeRangeTerm(getDateColumn(), from, to, match);
        return;
    }


    /**
     *  Matches convocations with any date. 
     *
     *  @param  match <code> true </code> to match convocations with any date, 
     *          <code> false </code> to match convocations with no date 
     */

    @OSID @Override
    public void matchAnyDate(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getDateColumn(), match);
        return;
    }


    /**
     *  Clears the date terms. 
     */

    @OSID @Override
    public void clearDateTerms() {
        getAssembler().clearTerms(getDateColumn());
        return;
    }


    /**
     *  Gets the date terms. 
     *
     *  @return the date terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getDateColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDateColumn(), style);
        return;
    }


    /**
     *  Gets the Date column name.
     *
     * @return the column name
     */

    protected String getDateColumn() {
        return ("date");
    }


    /**
     *  Sets a time period <code> Id. </code> 
     *
     *  @param  timePeriodId a time period <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> timePeriodId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchTimePeriodId(org.osid.id.Id timePeriodId, boolean match) {
        getAssembler().addIdTerm(getTimePeriodIdColumn(), timePeriodId, match);
        return;
    }


    /**
     *  Clears the time period <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearTimePeriodIdTerms() {
        getAssembler().clearTerms(getTimePeriodIdColumn());
        return;
    }


    /**
     *  Gets the time period <code> Id </code> terms. 
     *
     *  @return the time period <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getTimePeriodIdTerms() {
        return (getAssembler().getIdTerms(getTimePeriodIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the time period. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTimePeriod(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTimePeriodColumn(), style);
        return;
    }


    /**
     *  Gets the TimePeriodId column name.
     *
     * @return the column name
     */

    protected String getTimePeriodIdColumn() {
        return ("time_period_id");
    }


    /**
     *  Tests if a <code> TimePeriodQuery </code> is available. 
     *
     *  @return <code> true </code> if a time period query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTimePeriodQuery() {
        return (false);
    }


    /**
     *  Gets the query for a time period query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the time period query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodQuery getTimePeriodQuery() {
        throw new org.osid.UnimplementedException("supportsTimePeriodQuery() is false");
    }


    /**
     *  Matches any time period. 
     *
     *  @param  match <code> true </code> to match convocations with any time 
     *          period, <code> false </code> to match convocations with no 
     *          time period 
     */

    @OSID @Override
    public void matchAnyTimePeriod(boolean match) {
        getAssembler().addIdWildcardTerm(getTimePeriodColumn(), match);
        return;
    }


    /**
     *  Clears the time period terms. 
     */

    @OSID @Override
    public void clearTimePeriodTerms() {
        getAssembler().clearTerms(getTimePeriodColumn());
        return;
    }


    /**
     *  Gets the time period terms. 
     *
     *  @return the time period terms 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodQueryInspector[] getTimePeriodTerms() {
        return (new org.osid.calendaring.TimePeriodQueryInspector[0]);
    }


    /**
     *  Tests if a time period order is available. 
     *
     *  @return <code> true </code> if a time period order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTimePeriodSearchOrder() {
        return (false);
    }


    /**
     *  Gets the time period order. 
     *
     *  @return the time period search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodSearchOrder getTimePeriodSearchOrder() {
        throw new org.osid.UnimplementedException("supportsTimePeriodSearchOrder() is false");
    }


    /**
     *  Gets the TimePeriod column name.
     *
     * @return the column name
     */

    protected String getTimePeriodColumn() {
        return ("time_period");
    }


    /**
     *  Sets the conferral <code> Id </code> for this query. 
     *
     *  @param  conferralId a conferral <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> conferralId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchConferralId(org.osid.id.Id conferralId, boolean match) {
        getAssembler().addIdTerm(getConferralIdColumn(), conferralId, match);
        return;
    }


    /**
     *  Clears the conferral <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearConferralIdTerms() {
        getAssembler().clearTerms(getConferralIdColumn());
        return;
    }


    /**
     *  Gets the conferral <code> Id </code> terms. 
     *
     *  @return the conferral <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getConferralIdTerms() {
        return (getAssembler().getIdTerms(getConferralIdColumn()));
    }


    /**
     *  Gets the ConferralId column name.
     *
     * @return the column name
     */

    protected String getConferralIdColumn() {
        return ("conferral_id");
    }


    /**
     *  Tests if a conferral query is available. 
     *
     *  @return <code> true </code> if a conferral query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConferralQuery() {
        return (false);
    }


    /**
     *  Gets the query for an award. 
     *
     *  @return the conferral query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralQuery getConferralQuery() {
        throw new org.osid.UnimplementedException("supportsConferralQuery() is false");
    }


    /**
     *  Matches convocations with any conferral. 
     *
     *  @param  match <code> true </code> to match convocations with any 
     *          conferral, <code> false </code> to match convocations with no 
     *          conferrals 
     */

    @OSID @Override
    public void matchAnyConferral(boolean match) {
        getAssembler().addIdWildcardTerm(getConferralColumn(), match);
        return;
    }


    /**
     *  Clears the conferral terms. 
     */

    @OSID @Override
    public void clearConferralTerms() {
        getAssembler().clearTerms(getConferralColumn());
        return;
    }


    /**
     *  Gets the conferral terms. 
     *
     *  @return the conferral terms 
     */

    @OSID @Override
    public org.osid.recognition.ConferralQueryInspector[] getConferralTerms() {
        return (new org.osid.recognition.ConferralQueryInspector[0]);
    }


    /**
     *  Gets the Conferral column name.
     *
     * @return the column name
     */

    protected String getConferralColumn() {
        return ("conferral");
    }


    /**
     *  Sets the award <code> Id </code> for this query to match convocations 
     *  assigned to academies. 
     *
     *  @param  academyId an academy <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> academyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAcademyId(org.osid.id.Id academyId, boolean match) {
        getAssembler().addIdTerm(getAcademyIdColumn(), academyId, match);
        return;
    }


    /**
     *  Clears the academy <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAcademyIdTerms() {
        getAssembler().clearTerms(getAcademyIdColumn());
        return;
    }


    /**
     *  Gets the academy <code> Id </code> terms. 
     *
     *  @return the academy <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAcademyIdTerms() {
        return (getAssembler().getIdTerms(getAcademyIdColumn()));
    }


    /**
     *  Gets the AcademyId column name.
     *
     * @return the column name
     */

    protected String getAcademyIdColumn() {
        return ("academy_id");
    }


    /**
     *  Tests if an <code> AcademyQuery </code> is available. 
     *
     *  @return <code> true </code> if an academy query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAcademyQuery() {
        return (false);
    }


    /**
     *  Gets the query for an academy query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the academy query 
     *  @throws org.osid.UnimplementedException <code> supportsAcademyQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AcademyQuery getAcademyQuery() {
        throw new org.osid.UnimplementedException("supportsAcademyQuery() is false");
    }


    /**
     *  Clears the academy terms. 
     */

    @OSID @Override
    public void clearAcademyTerms() {
        getAssembler().clearTerms(getAcademyColumn());
        return;
    }


    /**
     *  Gets the academy terms. 
     *
     *  @return the academy terms 
     */

    @OSID @Override
    public org.osid.recognition.AcademyQueryInspector[] getAcademyTerms() {
        return (new org.osid.recognition.AcademyQueryInspector[0]);
    }


    /**
     *  Gets the Academy column name.
     *
     * @return the column name
     */

    protected String getAcademyColumn() {
        return ("academy");
    }


    /**
     *  Tests if this convocation supports the given record
     *  <code>Type</code>.
     *
     *  @param  convocationRecordType a convocation record type 
     *  @return <code>true</code> if the convocationRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>convocationRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type convocationRecordType) {
        for (org.osid.recognition.records.ConvocationQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(convocationRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  convocationRecordType the convocation record type 
     *  @return the convocation query record 
     *  @throws org.osid.NullArgumentException
     *          <code>convocationRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(convocationRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.recognition.records.ConvocationQueryRecord getConvocationQueryRecord(org.osid.type.Type convocationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recognition.records.ConvocationQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(convocationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(convocationRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  convocationRecordType the convocation record type 
     *  @return the convocation query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>convocationRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(convocationRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.recognition.records.ConvocationQueryInspectorRecord getConvocationQueryInspectorRecord(org.osid.type.Type convocationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recognition.records.ConvocationQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(convocationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(convocationRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param convocationRecordType the convocation record type
     *  @return the convocation search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>convocationRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(convocationRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.recognition.records.ConvocationSearchOrderRecord getConvocationSearchOrderRecord(org.osid.type.Type convocationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recognition.records.ConvocationSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(convocationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(convocationRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this convocation. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param convocationQueryRecord the convocation query record
     *  @param convocationQueryInspectorRecord the convocation query inspector
     *         record
     *  @param convocationSearchOrderRecord the convocation search order record
     *  @param convocationRecordType convocation record type
     *  @throws org.osid.NullArgumentException
     *          <code>convocationQueryRecord</code>,
     *          <code>convocationQueryInspectorRecord</code>,
     *          <code>convocationSearchOrderRecord</code> or
     *          <code>convocationRecordTypeconvocation</code> is
     *          <code>null</code>
     */
            
    protected void addConvocationRecords(org.osid.recognition.records.ConvocationQueryRecord convocationQueryRecord, 
                                      org.osid.recognition.records.ConvocationQueryInspectorRecord convocationQueryInspectorRecord, 
                                      org.osid.recognition.records.ConvocationSearchOrderRecord convocationSearchOrderRecord, 
                                      org.osid.type.Type convocationRecordType) {

        addRecordType(convocationRecordType);

        nullarg(convocationQueryRecord, "convocation query record");
        nullarg(convocationQueryInspectorRecord, "convocation query inspector record");
        nullarg(convocationSearchOrderRecord, "convocation search odrer record");

        this.queryRecords.add(convocationQueryRecord);
        this.queryInspectorRecords.add(convocationQueryInspectorRecord);
        this.searchOrderRecords.add(convocationSearchOrderRecord);
        
        return;
    }
}
