//
// AbstractChecklistBatchManager.java
//
//     An adapter for a ChecklistBatchManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.checklist.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a ChecklistBatchManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterChecklistBatchManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.checklist.batch.ChecklistBatchManager>
    implements org.osid.checklist.batch.ChecklistBatchManager {


    /**
     *  Constructs a new {@code AbstractAdapterChecklistBatchManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterChecklistBatchManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterChecklistBatchManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterChecklistBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of todos is available. 
     *
     *  @return <code> true </code> if a todo bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTodoBatchAdmin() {
        return (getAdapteeManager().supportsTodoBatchAdmin());
    }


    /**
     *  Tests if bulk administration of checklists is available. 
     *
     *  @return <code> true </code> if a checklist bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChecklistBatchAdmin() {
        return (getAdapteeManager().supportsChecklistBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk todo 
     *  administration service. 
     *
     *  @return a <code> TodoBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.batch.TodoBatchAdminSession getTodoBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTodoBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk todo 
     *  administration service for the given checklist. 
     *
     *  @param  checklistId the <code> Id </code> of the <code> Checklist 
     *          </code> 
     *  @return a <code> TodoBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Checklist </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> checklistId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.batch.TodoBatchAdminSession getTodoBatchAdminSessionForChecklist(org.osid.id.Id checklistId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTodoBatchAdminSessionForChecklist(checklistId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk checklist 
     *  administration service. 
     *
     *  @return a <code> ChecklistBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChecklistBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.batch.ChecklistBatchAdminSession getChecklistBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getChecklistBatchAdminSession());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
