//
// AbstractAssemblyFrontOfficeQuery.java
//
//     A FrontOfficeQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.tracking.frontoffice.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A FrontOfficeQuery that stores terms.
 */

public abstract class AbstractAssemblyFrontOfficeQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.tracking.FrontOfficeQuery,
               org.osid.tracking.FrontOfficeQueryInspector,
               org.osid.tracking.FrontOfficeSearchOrder {

    private final java.util.Collection<org.osid.tracking.records.FrontOfficeQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.tracking.records.FrontOfficeQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.tracking.records.FrontOfficeSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyFrontOfficeQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyFrontOfficeQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the issue <code> Id </code> for this query to match front offices 
     *  that have a related issue. 
     *
     *  @param  issueId a issue <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> issueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchIssueId(org.osid.id.Id issueId, boolean match) {
        getAssembler().addIdTerm(getIssueIdColumn(), issueId, match);
        return;
    }


    /**
     *  Clears the issue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearIssueIdTerms() {
        getAssembler().clearTerms(getIssueIdColumn());
        return;
    }


    /**
     *  Gets the issue <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getIssueIdTerms() {
        return (getAssembler().getIdTerms(getIssueIdColumn()));
    }


    /**
     *  Gets the IssueId column name.
     *
     * @return the column name
     */

    protected String getIssueIdColumn() {
        return ("issue_id");
    }


    /**
     *  Tests if a <code> IssueQuery </code> is available. 
     *
     *  @return <code> true </code> if a issue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a issue. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the issue query 
     *  @throws org.osid.UnimplementedException <code> supportsIssueQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueQuery getIssueQuery() {
        throw new org.osid.UnimplementedException("supportsIssueQuery() is false");
    }


    /**
     *  Matches front offices that have any issue. 
     *
     *  @param  match <code> true </code> to match front offices with any 
     *          issue, <code> false </code> to match front offices with no 
     *          issue 
     */

    @OSID @Override
    public void matchAnyIssue(boolean match) {
        getAssembler().addIdWildcardTerm(getIssueColumn(), match);
        return;
    }


    /**
     *  Clears the issue query terms. 
     */

    @OSID @Override
    public void clearIssueTerms() {
        getAssembler().clearTerms(getIssueColumn());
        return;
    }


    /**
     *  Gets the issue query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.tracking.IssueQueryInspector[] getIssueTerms() {
        return (new org.osid.tracking.IssueQueryInspector[0]);
    }


    /**
     *  Gets the Issue column name.
     *
     * @return the column name
     */

    protected String getIssueColumn() {
        return ("issue");
    }


    /**
     *  Sets the effort <code> Id </code> for this query to match front 
     *  offices containing queues. 
     *
     *  @param  queueId the queue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> queueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchQueueId(org.osid.id.Id queueId, boolean match) {
        getAssembler().addIdTerm(getQueueIdColumn(), queueId, match);
        return;
    }


    /**
     *  Clears the queue query terms. 
     */

    @OSID @Override
    public void clearQueueIdTerms() {
        getAssembler().clearTerms(getQueueIdColumn());
        return;
    }


    /**
     *  Gets the queue <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getQueueIdTerms() {
        return (getAssembler().getIdTerms(getQueueIdColumn()));
    }


    /**
     *  Gets the QueueId column name.
     *
     * @return the column name
     */

    protected String getQueueIdColumn() {
        return ("queue_id");
    }


    /**
     *  Tests if a <code> QueueQuery </code> is available. 
     *
     *  @return <code> true </code> if a queue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a queue. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the queue query 
     *  @throws org.osid.UnimplementedException <code> supportsQueueQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueQuery getQueueQuery() {
        throw new org.osid.UnimplementedException("supportsQueueQuery() is false");
    }


    /**
     *  Matches front offices that have any queue. 
     *
     *  @param  match <code> true </code> to match front offices with any 
     *          queue, <code> false </code> to match front offices with no 
     *          queue 
     */

    @OSID @Override
    public void matchAnyQueue(boolean match) {
        getAssembler().addIdWildcardTerm(getQueueColumn(), match);
        return;
    }


    /**
     *  Clears the queue query terms. 
     */

    @OSID @Override
    public void clearQueueTerms() {
        getAssembler().clearTerms(getQueueColumn());
        return;
    }


    /**
     *  Gets the queue query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.tracking.QueueQueryInspector[] getQueueTerms() {
        return (new org.osid.tracking.QueueQueryInspector[0]);
    }


    /**
     *  Gets the Queue column name.
     *
     * @return the column name
     */

    protected String getQueueColumn() {
        return ("queue");
    }


    /**
     *  Sets the front office <code> Id </code> for this query to match front 
     *  offices that have the specified front office as an ancestor. 
     *
     *  @param  frontOfficeId a front office <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorFrontOfficeId(org.osid.id.Id frontOfficeId, 
                                           boolean match) {
        getAssembler().addIdTerm(getAncestorFrontOfficeIdColumn(), frontOfficeId, match);
        return;
    }


    /**
     *  Clears the ancestor front office <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorFrontOfficeIdTerms() {
        getAssembler().clearTerms(getAncestorFrontOfficeIdColumn());
        return;
    }


    /**
     *  Gets the ancestor front office <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorFrontOfficeIdTerms() {
        return (getAssembler().getIdTerms(getAncestorFrontOfficeIdColumn()));
    }


    /**
     *  Gets the AncestorFrontOfficeId column name.
     *
     * @return the column name
     */

    protected String getAncestorFrontOfficeIdColumn() {
        return ("ancestor_front_office_id");
    }


    /**
     *  Tests if a <code> FrontOfficeQuery </code> is available. 
     *
     *  @return <code> true </code> if a front office query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorFrontOfficeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a frontOffice/ Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the front office query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorFrontOfficeQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeQuery getAncestorFrontOfficeQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorFrontOfficeQuery() is false");
    }


    /**
     *  Matches front offices with any ancestor. 
     *
     *  @param  match <code> true </code> to match front offices with any 
     *          ancestor, <code> false </code> to match root frontOffices 
     */

    @OSID @Override
    public void matchAnyAncestorFrontOffice(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorFrontOfficeColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor front office query terms. 
     */

    @OSID @Override
    public void clearAncestorFrontOfficeTerms() {
        getAssembler().clearTerms(getAncestorFrontOfficeColumn());
        return;
    }


    /**
     *  Gets the ancestor front office query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeQueryInspector[] getAncestorFrontOfficeTerms() {
        return (new org.osid.tracking.FrontOfficeQueryInspector[0]);
    }


    /**
     *  Gets the AncestorFrontOffice column name.
     *
     * @return the column name
     */

    protected String getAncestorFrontOfficeColumn() {
        return ("ancestor_front_office");
    }


    /**
     *  Sets the front office <code> Id </code> for this query to match front 
     *  offices that have the specified front office as a descendant. 
     *
     *  @param  frontOfficeId a front office <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantFrontOfficeId(org.osid.id.Id frontOfficeId, 
                                             boolean match) {
        getAssembler().addIdTerm(getDescendantFrontOfficeIdColumn(), frontOfficeId, match);
        return;
    }


    /**
     *  Clears the descendant front office <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantFrontOfficeIdTerms() {
        getAssembler().clearTerms(getDescendantFrontOfficeIdColumn());
        return;
    }


    /**
     *  Gets the descendant front office <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantFrontOfficeIdTerms() {
        return (getAssembler().getIdTerms(getDescendantFrontOfficeIdColumn()));
    }


    /**
     *  Gets the DescendantFrontOfficeId column name.
     *
     * @return the column name
     */

    protected String getDescendantFrontOfficeIdColumn() {
        return ("descendant_front_office_id");
    }


    /**
     *  Tests if a <code> FrontOfficeQuery </code> is available. 
     *
     *  @return <code> true </code> if a front office query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantFrontOfficeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a frontOffice/ Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the front office query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantFrontOfficeQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeQuery getDescendantFrontOfficeQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantFrontOfficeQuery() is false");
    }


    /**
     *  Matches front offices with any descendant. 
     *
     *  @param  match <code> true </code> to match front offices with any 
     *          descendant, <code> false </code> to match leaf frontOffices 
     */

    @OSID @Override
    public void matchAnyDescendantFrontOffice(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantFrontOfficeColumn(), match);
        return;
    }


    /**
     *  Clears the descendant front office query terms. 
     */

    @OSID @Override
    public void clearDescendantFrontOfficeTerms() {
        getAssembler().clearTerms(getDescendantFrontOfficeColumn());
        return;
    }


    /**
     *  Gets the descendant front office query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeQueryInspector[] getDescendantFrontOfficeTerms() {
        return (new org.osid.tracking.FrontOfficeQueryInspector[0]);
    }


    /**
     *  Gets the DescendantFrontOffice column name.
     *
     * @return the column name
     */

    protected String getDescendantFrontOfficeColumn() {
        return ("descendant_front_office");
    }


    /**
     *  Tests if this frontOffice supports the given record
     *  <code>Type</code>.
     *
     *  @param  frontOfficeRecordType a front office record type 
     *  @return <code>true</code> if the frontOfficeRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>frontOfficeRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type frontOfficeRecordType) {
        for (org.osid.tracking.records.FrontOfficeQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(frontOfficeRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  frontOfficeRecordType the front office record type 
     *  @return the front office query record 
     *  @throws org.osid.NullArgumentException
     *          <code>frontOfficeRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(frontOfficeRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.tracking.records.FrontOfficeQueryRecord getFrontOfficeQueryRecord(org.osid.type.Type frontOfficeRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.tracking.records.FrontOfficeQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(frontOfficeRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(frontOfficeRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  frontOfficeRecordType the front office record type 
     *  @return the front office query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>frontOfficeRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(frontOfficeRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.tracking.records.FrontOfficeQueryInspectorRecord getFrontOfficeQueryInspectorRecord(org.osid.type.Type frontOfficeRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.tracking.records.FrontOfficeQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(frontOfficeRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(frontOfficeRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param frontOfficeRecordType the front office record type
     *  @return the front office search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>frontOfficeRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(frontOfficeRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.tracking.records.FrontOfficeSearchOrderRecord getFrontOfficeSearchOrderRecord(org.osid.type.Type frontOfficeRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.tracking.records.FrontOfficeSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(frontOfficeRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(frontOfficeRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this front office. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param frontOfficeQueryRecord the front office query record
     *  @param frontOfficeQueryInspectorRecord the front office query inspector
     *         record
     *  @param frontOfficeSearchOrderRecord the front office search order record
     *  @param frontOfficeRecordType front office record type
     *  @throws org.osid.NullArgumentException
     *          <code>frontOfficeQueryRecord</code>,
     *          <code>frontOfficeQueryInspectorRecord</code>,
     *          <code>frontOfficeSearchOrderRecord</code> or
     *          <code>frontOfficeRecordTypefrontOffice</code> is
     *          <code>null</code>
     */
            
    protected void addFrontOfficeRecords(org.osid.tracking.records.FrontOfficeQueryRecord frontOfficeQueryRecord, 
                                      org.osid.tracking.records.FrontOfficeQueryInspectorRecord frontOfficeQueryInspectorRecord, 
                                      org.osid.tracking.records.FrontOfficeSearchOrderRecord frontOfficeSearchOrderRecord, 
                                      org.osid.type.Type frontOfficeRecordType) {

        addRecordType(frontOfficeRecordType);

        nullarg(frontOfficeQueryRecord, "front office query record");
        nullarg(frontOfficeQueryInspectorRecord, "front office query inspector record");
        nullarg(frontOfficeSearchOrderRecord, "front office search odrer record");

        this.queryRecords.add(frontOfficeQueryRecord);
        this.queryInspectorRecords.add(frontOfficeQueryInspectorRecord);
        this.searchOrderRecords.add(frontOfficeSearchOrderRecord);
        
        return;
    }
}
