//
// AbstractPollsSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.polls.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractPollsSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.voting.PollsSearchResults {

    private org.osid.voting.PollsList pollses;
    private final org.osid.voting.PollsQueryInspector inspector;
    private final java.util.Collection<org.osid.voting.records.PollsSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractPollsSearchResults.
     *
     *  @param pollses the result set
     *  @param pollsQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>pollses</code>
     *          or <code>pollsQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractPollsSearchResults(org.osid.voting.PollsList pollses,
                                            org.osid.voting.PollsQueryInspector pollsQueryInspector) {
        nullarg(pollses, "pollses");
        nullarg(pollsQueryInspector, "polls query inspectpr");

        this.pollses = pollses;
        this.inspector = pollsQueryInspector;

        return;
    }


    /**
     *  Gets the polls list resulting from a search.
     *
     *  @return a polls list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.voting.PollsList getPolls() {
        if (this.pollses == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.voting.PollsList pollses = this.pollses;
        this.pollses = null;
	return (pollses);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.voting.PollsQueryInspector getPollsQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  polls search record <code> Type. </code> This method must
     *  be used to retrieve a polls implementing the requested
     *  record.
     *
     *  @param pollsSearchRecordType a polls search 
     *         record type 
     *  @return the polls search
     *  @throws org.osid.NullArgumentException
     *          <code>pollsSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(pollsSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.records.PollsSearchResultsRecord getPollsSearchResultsRecord(org.osid.type.Type pollsSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.voting.records.PollsSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(pollsSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(pollsSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record polls search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addPollsRecord(org.osid.voting.records.PollsSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "polls record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
