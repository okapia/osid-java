//
// AbstractOsidRuleQueryInspector.java
//
//     Defines an OsidRuleQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 1 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.query.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an OsidRuleQueryInspector to extend. 
 */

public abstract class AbstractOsidRuleQueryInspector
    extends AbstractOperableOsidObjectQueryInspector
    implements org.osid.OsidRuleQueryInspector {
    
    private final java.util.Collection<org.osid.search.terms.IdTerm> ruleIdTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.rules.RuleQueryInspector> ruleTerms = new java.util.LinkedHashSet<>();


    /**
     *  Gets the rule <code>Id</code> query terms.
     *
     *  @return the rule <code>Id</code> terms
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuleIdTerms() {
        return (this.ruleIdTerms.toArray(new org.osid.search.terms.IdTerm[this.ruleIdTerms.size()]));
    }

    
    /**
     *  Adds a rule <code>Id</code> term.
     *
     *  @param term a rule <code>Id</code> term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addRuleIdTerm(org.osid.search.terms.IdTerm term) {
        nullarg(term, "rule Id term");
        this.ruleIdTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of rule <code>Id</code> terms.
     *
     *  @param terms a collection of rule <code>Id</code> terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addRuleIdTerms(java.util.Collection<org.osid.search.terms.IdTerm> terms) {
        nullarg(terms, "rule Id terms");
        this.ruleIdTerms.addAll(terms);
        return;
    }


    /**
     *  Adds a rule <code>Id</code> term.
     *
     *  @param ruleId the rule <code>Id</code>
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>ruleId</code>
     *          or is <code>null</code>
     */

    protected void addRuleIdTerm(org.osid.id.Id ruleId, boolean match) {
        this.ruleIdTerms.add(new net.okapia.osid.primordium.terms.IdTerm(ruleId, match));
        return;
    }


    /**
     *  Gets the rule query terms.
     *
     *  @return the rule terms
     */

    @OSID @Override
    public org.osid.rules.RuleQueryInspector[] getRuleTerms() {
        return (this.ruleTerms.toArray(new org.osid.rules.RuleQueryInspector[this.ruleTerms.size()]));
    }

    
    /**
     *  Adds a rule term.
     *
     *  @param term a rule term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addRuleTerm(org.osid.rules.RuleQueryInspector term) {
        nullarg(term, "rule term");
        this.ruleTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of rule terms.
     *
     *  @param terms a collection of rule terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addRuleTerms(java.util.Collection<org.osid.rules.RuleQueryInspector> terms) {
        nullarg(terms, "rule terms");
        this.ruleTerms.addAll(terms);
        return;
    }
}

