//
// AbstractImmutableShipment.java
//
//     Wraps a mutable Shipment to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.inventory.shipment.shipment.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Shipment</code> to hide modifiers. This
 *  wrapper provides an immutized Shipment from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying shipment whose state changes are visible.
 */

public abstract class AbstractImmutableShipment
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.inventory.shipment.Shipment {

    private final org.osid.inventory.shipment.Shipment shipment;


    /**
     *  Constructs a new <code>AbstractImmutableShipment</code>.
     *
     *  @param shipment the shipment to immutablize
     *  @throws org.osid.NullArgumentException <code>shipment</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableShipment(org.osid.inventory.shipment.Shipment shipment) {
        super(shipment);
        this.shipment = shipment;
        return;
    }


    /**
     *  Gets the resource <code> Id </code> representing the shipment. 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSourceId() {
        return (this.shipment.getSourceId());
    }


    /**
     *  Gets the resource. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getSource()
        throws org.osid.OperationFailedException {

        return (this.shipment.getSource());
    }


    /**
     *  Tests if this shipment has a related order. 
     *
     *  @return <code> true </code> if this shipment relates to an odrer, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasOrder() {
        return (this.shipment.hasOrder());
    }


    /**
     *  Gets the order <code> Id </code> associated with this shipment. 
     *
     *  @return the order <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasOrder() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getOrderId() {
        return (this.shipment.getOrderId());
    }


    /**
     *  Gets the order associated with this shipment. 
     *
     *  @return the order 
     *  @throws org.osid.IllegalStateException <code> hasOrder() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.ordering.Order getOrder()
        throws org.osid.OperationFailedException {

        return (this.shipment.getOrder());
    }


    /**
     *  Gets the date this shipment was received. 
     *
     *  @return the received date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getDate() {
        return (this.shipment.getDate());
    }


    /**
     *  Gets the entry <code> Ids </code> of this shipment. 
     *
     *  @return the entry <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getEntryIds() {
        return (this.shipment.getEntryIds());
    }


    /**
     *  Gets the entries of this shipment. 
     *
     *  @return the entries 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.inventory.shipment.EntryList getEntries()
        throws org.osid.OperationFailedException {

        return (this.shipment.getEntries());
    }


    /**
     *  Gets the shipment record corresponding to the given <code> Shipment 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> 
     *  shipmentRecordType </code> may be the <code> Type </code> returned in 
     *  <code> getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(shipmentRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  shipmentRecordType the type of shipment record to retrieve 
     *  @return the shipment record 
     *  @throws org.osid.NullArgumentException <code> shipmentRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(shipmentRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.records.ShipmentRecord getShipmentRecord(org.osid.type.Type shipmentRecordType)
        throws org.osid.OperationFailedException {

        return (this.shipment.getShipmentRecord(shipmentRecordType));
    }
}

