//
// AbstractIndexedMapConferralLookupSession.java
//
//    A simple framework for providing a Conferral lookup service
//    backed by a fixed collection of conferrals with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.recognition.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Conferral lookup service backed by a
 *  fixed collection of conferrals. The conferrals are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some conferrals may be compatible
 *  with more types than are indicated through these conferral
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Conferrals</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapConferralLookupSession
    extends AbstractMapConferralLookupSession
    implements org.osid.recognition.ConferralLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.recognition.Conferral> conferralsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.recognition.Conferral>());
    private final MultiMap<org.osid.type.Type, org.osid.recognition.Conferral> conferralsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.recognition.Conferral>());


    /**
     *  Makes a <code>Conferral</code> available in this session.
     *
     *  @param  conferral a conferral
     *  @throws org.osid.NullArgumentException <code>conferral<code> is
     *          <code>null</code>
     */

    @Override
    protected void putConferral(org.osid.recognition.Conferral conferral) {
        super.putConferral(conferral);

        this.conferralsByGenus.put(conferral.getGenusType(), conferral);
        
        try (org.osid.type.TypeList types = conferral.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.conferralsByRecord.put(types.getNextType(), conferral);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a conferral from this session.
     *
     *  @param conferralId the <code>Id</code> of the conferral
     *  @throws org.osid.NullArgumentException <code>conferralId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeConferral(org.osid.id.Id conferralId) {
        org.osid.recognition.Conferral conferral;
        try {
            conferral = getConferral(conferralId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.conferralsByGenus.remove(conferral.getGenusType());

        try (org.osid.type.TypeList types = conferral.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.conferralsByRecord.remove(types.getNextType(), conferral);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeConferral(conferralId);
        return;
    }


    /**
     *  Gets a <code>ConferralList</code> corresponding to the given
     *  conferral genus <code>Type</code> which does not include
     *  conferrals of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known conferrals or an error results. Otherwise,
     *  the returned list may contain only those conferrals that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  conferralGenusType a conferral genus type 
     *  @return the returned <code>Conferral</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>conferralGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsByGenusType(org.osid.type.Type conferralGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.recognition.conferral.ArrayConferralList(this.conferralsByGenus.get(conferralGenusType)));
    }


    /**
     *  Gets a <code>ConferralList</code> containing the given
     *  conferral record <code>Type</code>. In plenary mode, the
     *  returned list contains all known conferrals or an error
     *  results. Otherwise, the returned list may contain only those
     *  conferrals that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  conferralRecordType a conferral record type 
     *  @return the returned <code>conferral</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>conferralRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsByRecordType(org.osid.type.Type conferralRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.recognition.conferral.ArrayConferralList(this.conferralsByRecord.get(conferralRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.conferralsByGenus.clear();
        this.conferralsByRecord.clear();

        super.close();

        return;
    }
}
