//
// AbstractMapFoundryLookupSession
//
//    A simple framework for providing a Foundry lookup service
//    backed by a fixed collection of foundries.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Foundry lookup service backed by a
 *  fixed collection of foundries. The foundries are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Foundries</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapFoundryLookupSession
    extends net.okapia.osid.jamocha.resourcing.spi.AbstractFoundryLookupSession
    implements org.osid.resourcing.FoundryLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.resourcing.Foundry> foundries = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.resourcing.Foundry>());


    /**
     *  Makes a <code>Foundry</code> available in this session.
     *
     *  @param  foundry a foundry
     *  @throws org.osid.NullArgumentException <code>foundry<code>
     *          is <code>null</code>
     */

    protected void putFoundry(org.osid.resourcing.Foundry foundry) {
        this.foundries.put(foundry.getId(), foundry);
        return;
    }


    /**
     *  Makes an array of foundries available in this session.
     *
     *  @param  foundries an array of foundries
     *  @throws org.osid.NullArgumentException <code>foundries<code>
     *          is <code>null</code>
     */

    protected void putFoundries(org.osid.resourcing.Foundry[] foundries) {
        putFoundries(java.util.Arrays.asList(foundries));
        return;
    }


    /**
     *  Makes a collection of foundries available in this session.
     *
     *  @param  foundries a collection of foundries
     *  @throws org.osid.NullArgumentException <code>foundries<code>
     *          is <code>null</code>
     */

    protected void putFoundries(java.util.Collection<? extends org.osid.resourcing.Foundry> foundries) {
        for (org.osid.resourcing.Foundry foundry : foundries) {
            this.foundries.put(foundry.getId(), foundry);
        }

        return;
    }


    /**
     *  Removes a Foundry from this session.
     *
     *  @param  foundryId the <code>Id</code> of the foundry
     *  @throws org.osid.NullArgumentException <code>foundryId<code> is
     *          <code>null</code>
     */

    protected void removeFoundry(org.osid.id.Id foundryId) {
        this.foundries.remove(foundryId);
        return;
    }


    /**
     *  Gets the <code>Foundry</code> specified by its <code>Id</code>.
     *
     *  @param  foundryId <code>Id</code> of the <code>Foundry</code>
     *  @return the foundry
     *  @throws org.osid.NotFoundException <code>foundryId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>foundryId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Foundry getFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.resourcing.Foundry foundry = this.foundries.get(foundryId);
        if (foundry == null) {
            throw new org.osid.NotFoundException("foundry not found: " + foundryId);
        }

        return (foundry);
    }


    /**
     *  Gets all <code>Foundries</code>. In plenary mode, the returned
     *  list contains all known foundries or an error
     *  results. Otherwise, the returned list may contain only those
     *  foundries that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Foundries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryList getFoundries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resourcing.foundry.ArrayFoundryList(this.foundries.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.foundries.clear();
        super.close();
        return;
    }
}
