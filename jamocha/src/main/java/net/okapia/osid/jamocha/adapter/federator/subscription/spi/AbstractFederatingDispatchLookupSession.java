//
// AbstractFederatingDispatchLookupSession.java
//
//     An abstract federating adapter for a DispatchLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.subscription.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  DispatchLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingDispatchLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.subscription.DispatchLookupSession>
    implements org.osid.subscription.DispatchLookupSession {

    private boolean parallel = false;
    private org.osid.subscription.Publisher publisher = new net.okapia.osid.jamocha.nil.subscription.publisher.UnknownPublisher();


    /**
     *  Constructs a new <code>AbstractFederatingDispatchLookupSession</code>.
     */

    protected AbstractFederatingDispatchLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.subscription.DispatchLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Publisher/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Publisher Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getPublisherId() {
        return (this.publisher.getId());
    }


    /**
     *  Gets the <code>Publisher</code> associated with this 
     *  session.
     *
     *  @return the <code>Publisher</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.Publisher getPublisher()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.publisher);
    }


    /**
     *  Sets the <code>Publisher</code>.
     *
     *  @param  publisher the publisher for this session
     *  @throws org.osid.NullArgumentException <code>publisher</code>
     *          is <code>null</code>
     */

    protected void setPublisher(org.osid.subscription.Publisher publisher) {
        nullarg(publisher, "publisher");
        this.publisher = publisher;
        return;
    }


    /**
     *  Tests if this user can perform <code>Dispatch</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupDispatches() {
        for (org.osid.subscription.DispatchLookupSession session : getSessions()) {
            if (session.canLookupDispatches()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Dispatch</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeDispatchView() {
        for (org.osid.subscription.DispatchLookupSession session : getSessions()) {
            session.useComparativeDispatchView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Dispatch</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryDispatchView() {
        for (org.osid.subscription.DispatchLookupSession session : getSessions()) {
            session.usePlenaryDispatchView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include dispatches in publishers which are children
     *  of this publisher in the publisher hierarchy.
     */

    @OSID @Override
    public void useFederatedPublisherView() {
        for (org.osid.subscription.DispatchLookupSession session : getSessions()) {
            session.useFederatedPublisherView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this publisher only.
     */

    @OSID @Override
    public void useIsolatedPublisherView() {
        for (org.osid.subscription.DispatchLookupSession session : getSessions()) {
            session.useIsolatedPublisherView();
        }

        return;
    }


    /**
     *  Only active dispatches are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveDispatchView() {
        for (org.osid.subscription.DispatchLookupSession session : getSessions()) {
            session.useActiveDispatchView();
        }

        return;
    }


    /**
     *  Active and inactive dispatches are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusDispatchView() {
        for (org.osid.subscription.DispatchLookupSession session : getSessions()) {
            session.useAnyStatusDispatchView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>Dispatch</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Dispatch</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Dispatch</code> and
     *  retained for compatibility.
     *
     *  In active mode, dispatches are returned that are currently
     *  active. In any status mode, active and inactive dispatches
     *  are returned.
     *
     *  @param  dispatchId <code>Id</code> of the
     *          <code>Dispatch</code>
     *  @return the dispatch
     *  @throws org.osid.NotFoundException <code>dispatchId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>dispatchId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.Dispatch getDispatch(org.osid.id.Id dispatchId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.subscription.DispatchLookupSession session : getSessions()) {
            try {
                return (session.getDispatch(dispatchId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(dispatchId + " not found");
    }


    /**
     *  Gets a <code>DispatchList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  dispatches specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Dispatches</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, dispatches are returned that are currently
     *  active. In any status mode, active and inactive dispatches are
     *  returned.
     *
     *  @param  dispatchIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Dispatch</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>dispatchIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.DispatchList getDispatchesByIds(org.osid.id.IdList dispatchIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.subscription.dispatch.MutableDispatchList ret = new net.okapia.osid.jamocha.subscription.dispatch.MutableDispatchList();

        try (org.osid.id.IdList ids = dispatchIds) {
            while (ids.hasNext()) {
                ret.addDispatch(getDispatch(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>DispatchList</code> corresponding to the given
     *  dispatch genus <code>Type</code> which does not include
     *  dispatches of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  dispatches or an error results. Otherwise, the returned list
     *  may contain only those dispatches that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, dispatches are returned that are currently
     *  active. In any status mode, active and inactive dispatches are
     *  returned.
     *
     *  @param  dispatchGenusType a dispatch genus type 
     *  @return the returned <code>Dispatch</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>dispatchGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.DispatchList getDispatchesByGenusType(org.osid.type.Type dispatchGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.subscription.dispatch.FederatingDispatchList ret = getDispatchList();

        for (org.osid.subscription.DispatchLookupSession session : getSessions()) {
            ret.addDispatchList(session.getDispatchesByGenusType(dispatchGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>DispatchList</code> corresponding to the given
     *  dispatch genus <code>Type</code> and include any additional
     *  dispatches with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  dispatches or an error results. Otherwise, the returned list
     *  may contain only those dispatches that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, dispatches are returned that are currently
     *  active. In any status mode, active and inactive dispatches are
     *  returned.
     *
     *  @param  dispatchGenusType a dispatch genus type 
     *  @return the returned <code>Dispatch</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>dispatchGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.DispatchList getDispatchesByParentGenusType(org.osid.type.Type dispatchGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.subscription.dispatch.FederatingDispatchList ret = getDispatchList();

        for (org.osid.subscription.DispatchLookupSession session : getSessions()) {
            ret.addDispatchList(session.getDispatchesByParentGenusType(dispatchGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>DispatchList</code> containing the given
     *  dispatch record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  dispatches or an error results. Otherwise, the returned list
     *  may contain only those dispatches that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, dispatches are returned that are currently
     *  active. In any status mode, active and inactive dispatches are
     *  returned.
     *
     *  @param  dispatchRecordType a dispatch record type 
     *  @return the returned <code>Dispatch</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>dispatchRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.DispatchList getDispatchesByRecordType(org.osid.type.Type dispatchRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.subscription.dispatch.FederatingDispatchList ret = getDispatchList();

        for (org.osid.subscription.DispatchLookupSession session : getSessions()) {
            ret.addDispatchList(session.getDispatchesByRecordType(dispatchRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>DispatchList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known dispatches or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  dispatches that are accessible through this session. 
     *
     *  In active mode, dispatches are returned that are currently
     *  active. In any status mode, active and inactive dispatches
     *  are returned.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Dispatch</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.subscription.DispatchList getDispatchesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.subscription.dispatch.FederatingDispatchList ret = getDispatchList();

        for (org.osid.subscription.DispatchLookupSession session : getSessions()) {
            ret.addDispatchList(session.getDispatchesByProvider(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Dispatches</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  dispatches or an error results. Otherwise, the returned list
     *  may contain only those dispatches that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, dispatches are returned that are currently
     *  active. In any status mode, active and inactive dispatches
     *  are returned.
     *
     *  @return a list of <code>Dispatches</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.DispatchList getDispatches()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.subscription.dispatch.FederatingDispatchList ret = getDispatchList();

        for (org.osid.subscription.DispatchLookupSession session : getSessions()) {
            ret.addDispatchList(session.getDispatches());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.subscription.dispatch.FederatingDispatchList getDispatchList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.subscription.dispatch.ParallelDispatchList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.subscription.dispatch.CompositeDispatchList());
        }
    }
}
