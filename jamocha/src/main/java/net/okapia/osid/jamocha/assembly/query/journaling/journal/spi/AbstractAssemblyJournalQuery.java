//
// AbstractAssemblyJournalQuery.java
//
//     A JournalQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.journaling.journal.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A JournalQuery that stores terms.
 */

public abstract class AbstractAssemblyJournalQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.journaling.JournalQuery,
               org.osid.journaling.JournalQueryInspector,
               org.osid.journaling.JournalSearchOrder {

    private final java.util.Collection<org.osid.journaling.records.JournalQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.journaling.records.JournalQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.journaling.records.JournalSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyJournalQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyJournalQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the journal entry <code> Id </code> for this query to match 
     *  entries assigned to journals. 
     *
     *  @param  journalEntryId a journal entry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> journalEntryId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchJournalEntryId(org.osid.id.Id journalEntryId, 
                                    boolean match) {
        getAssembler().addIdTerm(getJournalEntryIdColumn(), journalEntryId, match);
        return;
    }


    /**
     *  Clears the journal entry <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearJournalEntryIdTerms() {
        getAssembler().clearTerms(getJournalEntryIdColumn());
        return;
    }


    /**
     *  Gets the journal entry <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getJournalEntryIdTerms() {
        return (getAssembler().getIdTerms(getJournalEntryIdColumn()));
    }


    /**
     *  Gets the JournalEntryId column name.
     *
     * @return the column name
     */

    protected String getJournalEntryIdColumn() {
        return ("journal_entry_id");
    }


    /**
     *  Tests if a journal entry query is available. 
     *
     *  @return <code> true </code> if a journal entry query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJournalEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a journal. 
     *
     *  @return the journal entry query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryQuery getJournalEntryQuery() {
        throw new org.osid.UnimplementedException("supportsJournalEntryQuery() is false");
    }


    /**
     *  Matches journals with any journal entry. 
     *
     *  @param  match <code> true </code> to match journals with any journal 
     *          entry, <code> false </code> to match journals with no entries 
     */

    @OSID @Override
    public void matchAnyJournalEntry(boolean match) {
        getAssembler().addIdWildcardTerm(getJournalEntryColumn(), match);
        return;
    }


    /**
     *  Clears the journal entry terms. 
     */

    @OSID @Override
    public void clearJournalEntryTerms() {
        getAssembler().clearTerms(getJournalEntryColumn());
        return;
    }


    /**
     *  Gets the journal entry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryQueryInspector[] getJournalEntryTerms() {
        return (new org.osid.journaling.JournalEntryQueryInspector[0]);
    }


    /**
     *  Gets the JournalEntry column name.
     *
     * @return the column name
     */

    protected String getJournalEntryColumn() {
        return ("journal_entry");
    }


    /**
     *  Sets the branch <code> Id </code> for this query to match branches 
     *  assigned to journals. 
     *
     *  @param  branchId a branch <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> branchId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBranchId(org.osid.id.Id branchId, boolean match) {
        getAssembler().addIdTerm(getBranchIdColumn(), branchId, match);
        return;
    }


    /**
     *  Clears the branch <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBranchIdTerms() {
        getAssembler().clearTerms(getBranchIdColumn());
        return;
    }


    /**
     *  Gets the branch <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBranchIdTerms() {
        return (getAssembler().getIdTerms(getBranchIdColumn()));
    }


    /**
     *  Gets the BranchId column name.
     *
     * @return the column name
     */

    protected String getBranchIdColumn() {
        return ("branch_id");
    }


    /**
     *  Tests if a branch query is available. 
     *
     *  @return <code> true </code> if a branch query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBranchQuery() {
        return (false);
    }


    /**
     *  Gets the query for a branch. 
     *
     *  @return the branch query 
     *  @throws org.osid.UnimplementedException <code> supportsBranchQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.BranchQuery getBranchQuery() {
        throw new org.osid.UnimplementedException("supportsBranchQuery() is false");
    }


    /**
     *  Matches journals with any branches. 
     *
     *  @param  match <code> true </code> to match journals with any branch, 
     *          <code> false </code> to match journals with no branches 
     */

    @OSID @Override
    public void matchAnyBranch(boolean match) {
        getAssembler().addIdWildcardTerm(getBranchColumn(), match);
        return;
    }


    /**
     *  Clears the branch terms. 
     */

    @OSID @Override
    public void clearBranchTerms() {
        getAssembler().clearTerms(getBranchColumn());
        return;
    }


    /**
     *  Gets the branch query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.journaling.BranchQueryInspector[] getBranchTerms() {
        return (new org.osid.journaling.BranchQueryInspector[0]);
    }


    /**
     *  Gets the Branch column name.
     *
     * @return the column name
     */

    protected String getBranchColumn() {
        return ("branch");
    }


    /**
     *  Sets the journal <code> Id </code> for this query to match journals 
     *  that have the specified journal as an ancestor. 
     *
     *  @param  journalId a journal <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> journalId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorJournalId(org.osid.id.Id journalId, boolean match) {
        getAssembler().addIdTerm(getAncestorJournalIdColumn(), journalId, match);
        return;
    }


    /**
     *  Clears the ancestor journal <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorJournalIdTerms() {
        getAssembler().clearTerms(getAncestorJournalIdColumn());
        return;
    }


    /**
     *  Gets the ancestor journal <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorJournalIdTerms() {
        return (getAssembler().getIdTerms(getAncestorJournalIdColumn()));
    }


    /**
     *  Gets the AncestorJournalId column name.
     *
     * @return the column name
     */

    protected String getAncestorJournalIdColumn() {
        return ("ancestor_journal_id");
    }


    /**
     *  Tests if a <code> JournalQuery </code> is available. 
     *
     *  @return <code> true </code> if a journal query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorJournalQuery() {
        return (false);
    }


    /**
     *  Gets the query for a journal. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the journal query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorJournalQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalQuery getAncestorJournalQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorJournalQuery() is false");
    }


    /**
     *  Matches journals with any ancestor. 
     *
     *  @param  match <code> true </code> to match journals with any ancestor, 
     *          <code> false </code> to match root journals 
     */

    @OSID @Override
    public void matchAnyAncestorJournal(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorJournalColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor journal terms. 
     */

    @OSID @Override
    public void clearAncestorJournalTerms() {
        getAssembler().clearTerms(getAncestorJournalColumn());
        return;
    }


    /**
     *  Gets the ancestor journal query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.journaling.JournalQueryInspector[] getAncestorJournalTerms() {
        return (new org.osid.journaling.JournalQueryInspector[0]);
    }


    /**
     *  Gets the AncestorJournal column name.
     *
     * @return the column name
     */

    protected String getAncestorJournalColumn() {
        return ("ancestor_journal");
    }


    /**
     *  Sets the journal <code> Id </code> for this query to match journals 
     *  that have the specified journal as a descendant. 
     *
     *  @param  journalId a journal <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> journalId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantJournalId(org.osid.id.Id journalId, 
                                         boolean match) {
        getAssembler().addIdTerm(getDescendantJournalIdColumn(), journalId, match);
        return;
    }


    /**
     *  Clears the descendant journal <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantJournalIdTerms() {
        getAssembler().clearTerms(getDescendantJournalIdColumn());
        return;
    }


    /**
     *  Gets the descendant journal <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantJournalIdTerms() {
        return (getAssembler().getIdTerms(getDescendantJournalIdColumn()));
    }


    /**
     *  Gets the DescendantJournalId column name.
     *
     * @return the column name
     */

    protected String getDescendantJournalIdColumn() {
        return ("descendant_journal_id");
    }


    /**
     *  Tests if a <code> JournalQuery </code> is available. 
     *
     *  @return <code> true </code> if a journal query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantJournalQuery() {
        return (false);
    }


    /**
     *  Gets the query for a journal. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the journal query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantJournalQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalQuery getDescendantJournalQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantJournalQuery() is false");
    }


    /**
     *  Matches journals with any descendant. 
     *
     *  @param  match <code> true </code> to match journals with any 
     *          descendant, <code> false </code> to match leaf journals 
     */

    @OSID @Override
    public void matchAnyDescendantJournal(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantJournalColumn(), match);
        return;
    }


    /**
     *  Clears the descendant journal terms. 
     */

    @OSID @Override
    public void clearDescendantJournalTerms() {
        getAssembler().clearTerms(getDescendantJournalColumn());
        return;
    }


    /**
     *  Gets the descendant journal query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.journaling.JournalQueryInspector[] getDescendantJournalTerms() {
        return (new org.osid.journaling.JournalQueryInspector[0]);
    }


    /**
     *  Gets the DescendantJournal column name.
     *
     * @return the column name
     */

    protected String getDescendantJournalColumn() {
        return ("descendant_journal");
    }


    /**
     *  Tests if this journal supports the given record
     *  <code>Type</code>.
     *
     *  @param  journalRecordType a journal record type 
     *  @return <code>true</code> if the journalRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>journalRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type journalRecordType) {
        for (org.osid.journaling.records.JournalQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(journalRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  journalRecordType the journal record type 
     *  @return the journal query record 
     *  @throws org.osid.NullArgumentException
     *          <code>journalRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(journalRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.journaling.records.JournalQueryRecord getJournalQueryRecord(org.osid.type.Type journalRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.journaling.records.JournalQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(journalRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(journalRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  journalRecordType the journal record type 
     *  @return the journal query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>journalRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(journalRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.journaling.records.JournalQueryInspectorRecord getJournalQueryInspectorRecord(org.osid.type.Type journalRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.journaling.records.JournalQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(journalRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(journalRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param journalRecordType the journal record type
     *  @return the journal search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>journalRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(journalRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.journaling.records.JournalSearchOrderRecord getJournalSearchOrderRecord(org.osid.type.Type journalRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.journaling.records.JournalSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(journalRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(journalRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this journal. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param journalQueryRecord the journal query record
     *  @param journalQueryInspectorRecord the journal query inspector
     *         record
     *  @param journalSearchOrderRecord the journal search order record
     *  @param journalRecordType journal record type
     *  @throws org.osid.NullArgumentException
     *          <code>journalQueryRecord</code>,
     *          <code>journalQueryInspectorRecord</code>,
     *          <code>journalSearchOrderRecord</code> or
     *          <code>journalRecordTypejournal</code> is
     *          <code>null</code>
     */
            
    protected void addJournalRecords(org.osid.journaling.records.JournalQueryRecord journalQueryRecord, 
                                      org.osid.journaling.records.JournalQueryInspectorRecord journalQueryInspectorRecord, 
                                      org.osid.journaling.records.JournalSearchOrderRecord journalSearchOrderRecord, 
                                      org.osid.type.Type journalRecordType) {

        addRecordType(journalRecordType);

        nullarg(journalQueryRecord, "journal query record");
        nullarg(journalQueryInspectorRecord, "journal query inspector record");
        nullarg(journalSearchOrderRecord, "journal search odrer record");

        this.queryRecords.add(journalQueryRecord);
        this.queryInspectorRecords.add(journalQueryInspectorRecord);
        this.searchOrderRecords.add(journalSearchOrderRecord);
        
        return;
    }
}
