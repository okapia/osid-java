//
// AbstractMapNodeLookupSession
//
//    A simple framework for providing a Node lookup service
//    backed by a fixed collection of nodes.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.topology.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Node lookup service backed by a
 *  fixed collection of nodes. The nodes are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Nodes</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapNodeLookupSession
    extends net.okapia.osid.jamocha.topology.spi.AbstractNodeLookupSession
    implements org.osid.topology.NodeLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.topology.Node> nodes = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.topology.Node>());


    /**
     *  Makes a <code>Node</code> available in this session.
     *
     *  @param  node a node
     *  @throws org.osid.NullArgumentException <code>node<code>
     *          is <code>null</code>
     */

    protected void putNode(org.osid.topology.Node node) {
        this.nodes.put(node.getId(), node);
        return;
    }


    /**
     *  Makes an array of nodes available in this session.
     *
     *  @param  nodes an array of nodes
     *  @throws org.osid.NullArgumentException <code>nodes<code>
     *          is <code>null</code>
     */

    protected void putNodes(org.osid.topology.Node[] nodes) {
        putNodes(java.util.Arrays.asList(nodes));
        return;
    }


    /**
     *  Makes a collection of nodes available in this session.
     *
     *  @param  nodes a collection of nodes
     *  @throws org.osid.NullArgumentException <code>nodes<code>
     *          is <code>null</code>
     */

    protected void putNodes(java.util.Collection<? extends org.osid.topology.Node> nodes) {
        for (org.osid.topology.Node node : nodes) {
            this.nodes.put(node.getId(), node);
        }

        return;
    }


    /**
     *  Removes a Node from this session.
     *
     *  @param  nodeId the <code>Id</code> of the node
     *  @throws org.osid.NullArgumentException <code>nodeId<code> is
     *          <code>null</code>
     */

    protected void removeNode(org.osid.id.Id nodeId) {
        this.nodes.remove(nodeId);
        return;
    }


    /**
     *  Gets the <code>Node</code> specified by its <code>Id</code>.
     *
     *  @param  nodeId <code>Id</code> of the <code>Node</code>
     *  @return the node
     *  @throws org.osid.NotFoundException <code>nodeId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>nodeId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.Node getNode(org.osid.id.Id nodeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.topology.Node node = this.nodes.get(nodeId);
        if (node == null) {
            throw new org.osid.NotFoundException("node not found: " + nodeId);
        }

        return (node);
    }


    /**
     *  Gets all <code>Nodes</code>. In plenary mode, the returned
     *  list contains all known nodes or an error
     *  results. Otherwise, the returned list may contain only those
     *  nodes that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Nodes</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.NodeList getNodes()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.topology.node.ArrayNodeList(this.nodes.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.nodes.clear();
        super.close();
        return;
    }
}
