//
// AbstractImmutableEnrollment.java
//
//     Wraps a mutable Enrollment to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.program.enrollment.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Enrollment</code> to hide modifiers. This
 *  wrapper provides an immutized Enrollment from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying enrollment whose state changes are visible.
 */

public abstract class AbstractImmutableEnrollment
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.course.program.Enrollment {

    private final org.osid.course.program.Enrollment enrollment;


    /**
     *  Constructs a new <code>AbstractImmutableEnrollment</code>.
     *
     *  @param enrollment the enrollment to immutablize
     *  @throws org.osid.NullArgumentException <code>enrollment</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableEnrollment(org.osid.course.program.Enrollment enrollment) {
        super(enrollment);
        this.enrollment = enrollment;
        return;
    }


    /**
     *  Gets the program offering <code> Id </code> associated with this 
     *  registration. 
     *
     *  @return the program offering <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getProgramOfferingId() {
        return (this.enrollment.getProgramOfferingId());
    }


    /**
     *  Gets the program offering associated with this registration. 
     *
     *  @return the program offering 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOffering getProgramOffering()
        throws org.osid.OperationFailedException {

        return (this.enrollment.getProgramOffering());
    }


    /**
     *  Gets the <code> Id </code> of the student <code> Resource. </code> 
     *
     *  @return the <code> Resource </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getStudentId() {
        return (this.enrollment.getStudentId());
    }


    /**
     *  Gets the student <code> Resource. </code> 
     *
     *  @return the student 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getStudent()
        throws org.osid.OperationFailedException {

        return (this.enrollment.getStudent());
    }


    /**
     *  Gets the enrollment record corresponding to the given <code> 
     *  Enrollment </code> record <code> Type. </code> This method is used to 
     *  retrieve an object implementing the requested record. The <code> 
     *  enrollmentRecordType </code> may be the <code> Type </code> returned 
     *  in <code> getRecordTypes() </code> or any of its parents in a <code> 
     *  Type </code> hierarchy where <code> 
     *  hasRecordType(enrollmentRecordType) </code> is <code> true </code> . 
     *
     *  @param  enrollmentRecordType the type of enrollment record to retrieve 
     *  @return the enrollment record 
     *  @throws org.osid.NullArgumentException <code> enrollmentRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(enrollmentRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.program.records.EnrollmentRecord getEnrollmentRecord(org.osid.type.Type enrollmentRecordType)
        throws org.osid.OperationFailedException {

        return (this.enrollment.getEnrollmentRecord(enrollmentRecordType));
    }
}

