//
// AbstractQueryPoolConstrainerLookupSession.java
//
//    An inline adapter that maps a PoolConstrainerLookupSession to
//    a PoolConstrainerQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a PoolConstrainerLookupSession to
 *  a PoolConstrainerQuerySession.
 */

public abstract class AbstractQueryPoolConstrainerLookupSession
    extends net.okapia.osid.jamocha.provisioning.rules.spi.AbstractPoolConstrainerLookupSession
    implements org.osid.provisioning.rules.PoolConstrainerLookupSession {

    private boolean activeonly    = false;
    private final org.osid.provisioning.rules.PoolConstrainerQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryPoolConstrainerLookupSession.
     *
     *  @param querySession the underlying pool constrainer query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryPoolConstrainerLookupSession(org.osid.provisioning.rules.PoolConstrainerQuerySession querySession) {
        nullarg(querySession, "pool constrainer query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Distributor</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Distributor Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.session.getDistributorId());
    }


    /**
     *  Gets the <code>Distributor</code> associated with this 
     *  session.
     *
     *  @return the <code>Distributor</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getDistributor());
    }


    /**
     *  Tests if this user can perform <code>PoolConstrainer</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPoolConstrainers() {
        return (this.session.canSearchPoolConstrainers());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include pool constrainers in distributors which are
     *  children of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        this.session.useFederatedDistributorView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        this.session.useIsolatedDistributorView();
        return;
    }
    

    /**
     *  Only active pool constrainers are returned by methods in this
     *  session.
     */
     
    @OSID @Override
    public void useActivePoolConstrainerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive pool constrainers are returned by methods
     *  in this session.
     */
    
    @OSID @Override
    public void useAnyStatusPoolConstrainerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>PoolConstrainer</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>PoolConstrainer</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>PoolConstrainer</code> and
     *  retained for compatibility.
     *
     *  In active mode, pool constrainers are returned that are
     *  currently active. In any status mode, active and inactive pool
     *  constrainers are returned.
     *
     *  @param  poolConstrainerId <code>Id</code> of the
     *          <code>PoolConstrainer</code>
     *  @return the pool constrainer
     *  @throws org.osid.NotFoundException <code>poolConstrainerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>poolConstrainerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainer getPoolConstrainer(org.osid.id.Id poolConstrainerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.PoolConstrainerQuery query = getQuery();
        query.matchId(poolConstrainerId, true);
        org.osid.provisioning.rules.PoolConstrainerList poolConstrainers = this.session.getPoolConstrainersByQuery(query);
        if (poolConstrainers.hasNext()) {
            return (poolConstrainers.getNextPoolConstrainer());
        } 
        
        throw new org.osid.NotFoundException(poolConstrainerId + " not found");
    }


    /**
     *  Gets a <code>PoolConstrainerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  poolConstrainers specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>PoolConstrainers</code> may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  In active mode, pool constrainers are returned that are
     *  currently active. In any status mode, active and inactive pool
     *  constrainers are returned.
     *
     *  @param  poolConstrainerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>PoolConstrainer</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>poolConstrainerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerList getPoolConstrainersByIds(org.osid.id.IdList poolConstrainerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.PoolConstrainerQuery query = getQuery();

        try (org.osid.id.IdList ids = poolConstrainerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getPoolConstrainersByQuery(query));
    }


    /**
     *  Gets a <code>PoolConstrainerList</code> corresponding to the
     *  given pool constrainer genus <code>Type</code> which does not
     *  include pool constrainers of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known pool
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those pool constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, pool constrainers are returned that are
     *  currently active. In any status mode, active and inactive pool
     *  constrainers are returned.
     *
     *  @param  poolConstrainerGenusType a poolConstrainer genus type 
     *  @return the returned <code>PoolConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>poolConstrainerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerList getPoolConstrainersByGenusType(org.osid.type.Type poolConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.PoolConstrainerQuery query = getQuery();
        query.matchGenusType(poolConstrainerGenusType, true);
        return (this.session.getPoolConstrainersByQuery(query));
    }


    /**
     *  Gets a <code>PoolConstrainerList</code> corresponding to the
     *  given pool constrainer genus <code>Type</code> and include any
     *  additional pool constrainers with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known pool
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those pool constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, pool constrainers are returned that are
     *  currently active. In any status mode, active and inactive pool
     *  constrainers are returned.
     *
     *  @param  poolConstrainerGenusType a poolConstrainer genus type 
     *  @return the returned <code>PoolConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>poolConstrainerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerList getPoolConstrainersByParentGenusType(org.osid.type.Type poolConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.PoolConstrainerQuery query = getQuery();
        query.matchParentGenusType(poolConstrainerGenusType, true);
        return (this.session.getPoolConstrainersByQuery(query));
    }


    /**
     *  Gets a <code>PoolConstrainerList</code> containing the given
     *  pool constrainer record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known pool
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those pool constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, pool constrainers are returned that are
     *  currently active. In any status mode, active and inactive pool
     *  constrainers are returned.
     *
     *  @param  poolConstrainerRecordType a poolConstrainer record type 
     *  @return the returned <code>PoolConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>poolConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerList getPoolConstrainersByRecordType(org.osid.type.Type poolConstrainerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.PoolConstrainerQuery query = getQuery();
        query.matchRecordType(poolConstrainerRecordType, true);
        return (this.session.getPoolConstrainersByQuery(query));
    }

    
    /**
     *  Gets all <code>PoolConstrainers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  pool constrainers or an error results. Otherwise, the returned list
     *  may contain only those pool constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, pool constrainers are returned that are currently
     *  active. In any status mode, active and inactive pool constrainers
     *  are returned.
     *
     *  @return a list of <code>PoolConstrainers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerList getPoolConstrainers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.PoolConstrainerQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getPoolConstrainersByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.provisioning.rules.PoolConstrainerQuery getQuery() {
        org.osid.provisioning.rules.PoolConstrainerQuery query = this.session.getPoolConstrainerQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
