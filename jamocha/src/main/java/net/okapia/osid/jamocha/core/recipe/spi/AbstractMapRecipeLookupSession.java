//
// AbstractMapRecipeLookupSession
//
//    A simple framework for providing a Recipe lookup service
//    backed by a fixed collection of recipes.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.recipe.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Recipe lookup service backed by a
 *  fixed collection of recipes. The recipes are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Recipes</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapRecipeLookupSession
    extends net.okapia.osid.jamocha.recipe.spi.AbstractRecipeLookupSession
    implements org.osid.recipe.RecipeLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.recipe.Recipe> recipes = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.recipe.Recipe>());


    /**
     *  Makes a <code>Recipe</code> available in this session.
     *
     *  @param  recipe a recipe
     *  @throws org.osid.NullArgumentException <code>recipe<code>
     *          is <code>null</code>
     */

    protected void putRecipe(org.osid.recipe.Recipe recipe) {
        this.recipes.put(recipe.getId(), recipe);
        return;
    }


    /**
     *  Makes an array of recipes available in this session.
     *
     *  @param  recipes an array of recipes
     *  @throws org.osid.NullArgumentException <code>recipes<code>
     *          is <code>null</code>
     */

    protected void putRecipes(org.osid.recipe.Recipe[] recipes) {
        putRecipes(java.util.Arrays.asList(recipes));
        return;
    }


    /**
     *  Makes a collection of recipes available in this session.
     *
     *  @param  recipes a collection of recipes
     *  @throws org.osid.NullArgumentException <code>recipes<code>
     *          is <code>null</code>
     */

    protected void putRecipes(java.util.Collection<? extends org.osid.recipe.Recipe> recipes) {
        for (org.osid.recipe.Recipe recipe : recipes) {
            this.recipes.put(recipe.getId(), recipe);
        }

        return;
    }


    /**
     *  Removes a Recipe from this session.
     *
     *  @param  recipeId the <code>Id</code> of the recipe
     *  @throws org.osid.NullArgumentException <code>recipeId<code> is
     *          <code>null</code>
     */

    protected void removeRecipe(org.osid.id.Id recipeId) {
        this.recipes.remove(recipeId);
        return;
    }


    /**
     *  Gets the <code>Recipe</code> specified by its <code>Id</code>.
     *
     *  @param  recipeId <code>Id</code> of the <code>Recipe</code>
     *  @return the recipe
     *  @throws org.osid.NotFoundException <code>recipeId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>recipeId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.Recipe getRecipe(org.osid.id.Id recipeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.recipe.Recipe recipe = this.recipes.get(recipeId);
        if (recipe == null) {
            throw new org.osid.NotFoundException("recipe not found: " + recipeId);
        }

        return (recipe);
    }


    /**
     *  Gets all <code>Recipes</code>. In plenary mode, the returned
     *  list contains all known recipes or an error
     *  results. Otherwise, the returned list may contain only those
     *  recipes that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Recipes</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.RecipeList getRecipes()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.recipe.recipe.ArrayRecipeList(this.recipes.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.recipes.clear();
        super.close();
        return;
    }
}
