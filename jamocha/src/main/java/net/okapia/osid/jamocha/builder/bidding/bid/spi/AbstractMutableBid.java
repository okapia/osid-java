//
// AbstractMutableBid.java
//
//     Defines a mutable Bid.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.bidding.bid.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Bid</code>.
 */

public abstract class AbstractMutableBid
    extends net.okapia.osid.jamocha.bidding.bid.spi.AbstractBid
    implements org.osid.bidding.Bid,
               net.okapia.osid.jamocha.builder.bidding.bid.BidMiter {

    private boolean winner;


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this bid. 
     *
     *  @param record bid record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addBidRecord(org.osid.bidding.records.BidRecord record, org.osid.type.Type recordType) {
        super.addBidRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the start date for when this bid is effective.
     *
     *  @param date the start date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setStartDate(org.osid.calendaring.DateTime date) {
        super.setStartDate(date);
        return;
    }


    /**
     *  Sets the end date for when this bid ceases to be
     *  effective.
     *
     *  @param date the end date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setEndDate(org.osid.calendaring.DateTime date) {
        super.setEndDate(date);
        return;
    }


    /**
     *  Sets the display name for this bid.
     *
     *  @param displayName the name for this bid
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this bid.
     *
     *  @param description the description of this bid
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the end state for the expiration of this bid
     *  relationship.
     *
     *  @param state the end state
     *  @throws org.osid.NullArgumentException <code>state</code> is
     *          <code>null</code>
     */

    @Override
    public void setEndReason(org.osid.process.State state) {
        super.setEndReason(state);
        return;
    }


    /**
     *  Sets the auction.
     *
     *  @param auction an auction
     *  @throws org.osid.NullArgumentException
     *          <code>auction</code> is <code>null</code>
     */

    @Override
    public void setAuction(org.osid.bidding.Auction auction) {
        super.setAuction(auction);
        return;
    }


    /**
     *  Sets the bidder.
     *
     *  @param bidder a bidder
     *  @throws org.osid.NullArgumentException
     *          <code>bidder</code> is <code>null</code>
     */

    @Override
    public void setBidder(org.osid.resource.Resource bidder) {
        super.setBidder(bidder);
        return;
    }


    /**
     *  Sets the bidding agent.
     *
     *  @param biddingAgent a bidding agent
     *  @throws org.osid.NullArgumentException
     *          <code>biddingAgent</code> is <code>null</code>
     */

    @Override
    public void setBiddingAgent(org.osid.authentication.Agent biddingAgent) {
        super.setBiddingAgent(biddingAgent);
        return;
    }


    /**
     *  Sets the quantity.
     *
     *  @param quantity a quantity
     *  @throws org.osid.InvalidArgumentException
     *          <code>quantity</code> is negative
     */

    @Override
    public void setQuantity(long quantity) {
        super.setQuantity(quantity);
        return;
    }


    /**
     *  Sets the current bid.
     *
     *  @param bid a current bid
     *  @throws org.osid.NullArgumentException <code>bid</code> is
     *          <code>null</code>
     */

    @Override
    public void setCurrentBid(org.osid.financials.Currency bid) {
        super.setCurrentBid(bid);
        return;
    }


    /**
     *  Sets the maximum bid.
     *
     *  @param bid a maximum bid
     *  @throws org.osid.NullArgumentException <code>bid</code> is
     *          <code>null</code>
     */

    @Override
    public void setMaximumBid(org.osid.financials.Currency bid) {
        super.setMaximumBid(bid);
        return;
    }


    /**
     *  Sets the winner flag.
     *
     *  @param winner <code> true </code> if this was a winnign bid,
     *          <code> false </code> otherwise
     */

    @Override
    public void setWinner(boolean winner) {
        this.winner = winner;
        return;
    }


    /**
     *  Sets the settlement amount.
     *
     *  @param amount a settlement amount
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    @Override
    public void setSettlementAmount(org.osid.financials.Currency amount) {
        super.setSettlementAmount(amount);
        return;
    }
}

