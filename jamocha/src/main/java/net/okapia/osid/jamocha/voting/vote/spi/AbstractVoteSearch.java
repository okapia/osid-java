//
// AbstractVoteSearch.java
//
//     A template for making a Vote Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.vote.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing vote searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractVoteSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.voting.VoteSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.voting.records.VoteSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.voting.VoteSearchOrder voteSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of votes. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  voteIds list of votes
     *  @throws org.osid.NullArgumentException
     *          <code>voteIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongVotes(org.osid.id.IdList voteIds) {
        while (voteIds.hasNext()) {
            try {
                this.ids.add(voteIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongVotes</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of vote Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getVoteIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  voteSearchOrder vote search order 
     *  @throws org.osid.NullArgumentException
     *          <code>voteSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>voteSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderVoteResults(org.osid.voting.VoteSearchOrder voteSearchOrder) {
	this.voteSearchOrder = voteSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.voting.VoteSearchOrder getVoteSearchOrder() {
	return (this.voteSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given vote search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a vote implementing the requested record.
     *
     *  @param voteSearchRecordType a vote search record
     *         type
     *  @return the vote search record
     *  @throws org.osid.NullArgumentException
     *          <code>voteSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(voteSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.records.VoteSearchRecord getVoteSearchRecord(org.osid.type.Type voteSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.voting.records.VoteSearchRecord record : this.records) {
            if (record.implementsRecordType(voteSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(voteSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this vote search. 
     *
     *  @param voteSearchRecord vote search record
     *  @param voteSearchRecordType vote search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addVoteSearchRecord(org.osid.voting.records.VoteSearchRecord voteSearchRecord, 
                                           org.osid.type.Type voteSearchRecordType) {

        addRecordType(voteSearchRecordType);
        this.records.add(voteSearchRecord);        
        return;
    }
}
