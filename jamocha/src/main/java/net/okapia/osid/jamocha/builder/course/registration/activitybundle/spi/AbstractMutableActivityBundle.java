//
// AbstractMutableActivityBundle.java
//
//     Defines a mutable ActivityBundle.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.registration.activitybundle.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>ActivityBundle</code>.
 */

public abstract class AbstractMutableActivityBundle
    extends net.okapia.osid.jamocha.course.registration.activitybundle.spi.AbstractActivityBundle
    implements org.osid.course.registration.ActivityBundle,
               net.okapia.osid.jamocha.builder.course.registration.activitybundle.ActivityBundleMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this activity bundle. 
     *
     *  @param record activity bundle record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addActivityBundleRecord(org.osid.course.registration.records.ActivityBundleRecord record, org.osid.type.Type recordType) {
        super.addActivityBundleRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the display name for this activity bundle.
     *
     *  @param displayName the name for this activity bundle
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this activity bundle.
     *
     *  @param description the description of this activity bundle
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the course offering.
     *
     *  @param courseOffering a course offering
     *  @throws org.osid.NullArgumentException
     *          <code>courseOffering</code> is <code>null</code>
     */

    @Override
    public void setCourseOffering(org.osid.course.CourseOffering courseOffering) {
        super.setCourseOffering(courseOffering);
        return;
    }


    /**
     *  Adds an activity.
     *
     *  @param activity an activity
     *  @throws org.osid.NullArgumentException <code>activity</code>
     *          is <code>null</code>
     */

    @Override
    public void addActivity(org.osid.course.Activity activity) {
        super.addActivity(activity);
        return;
    }


    /**
     *  Sets all the activities.
     *
     *  @param activities a collection of activities
     *  @throws org.osid.NullArgumentException <code>activities</code>
     *          is <code>null</code>
     */

    @Override
    public void setActivities(java.util.Collection<org.osid.course.Activity> activities) {
        super.setActivities(activities);
        return;
    }


    /**
     *  Adds a credit option.
     *
     *  @param credits a credit option to add
     *  @throws org.osid.NullArgumentException <code>credits</code> is
     *          <code>null</code>
     */

    @Override
    public void addCredits(java.math.BigDecimal credits) {
        super.addCredits(credits);
        return;
    }


    /**
     *  Sets all the credits.
     *
     *  @param credits the credits options
     *  @throws org.osid.NullArgumentException <code>credits</code> is
     *          <code>null</code>
     */

    @Override
    public void setCredits(java.util.Collection <java.math.BigDecimal> credits) {
        super.setCredits(credits);
        return;
    }


    /**
     *  Adds a grading option.
     *
     *  @param option a grading option
     *  @throws org.osid.NullArgumentException <code>option</code> is
     *          <code>null</code>
     */

    @Override
    public void addGradingOption(org.osid.grading.GradeSystem option) {
        super.addGradingOption(option);
        return;
    }


    /**
     *  Sets all the grading options.
     *
     *  @param options a collection of grading options
     *  @throws org.osid.NullArgumentException <code>options</code> is
     *          <code>null</code>
     */

    @Override
    public void setGradingOptions(java.util.Collection<org.osid.grading.GradeSystem> options) {
        super.setGradingOptions(options);
        return;
    }
}

