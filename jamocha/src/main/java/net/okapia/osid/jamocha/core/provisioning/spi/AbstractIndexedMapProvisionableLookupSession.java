//
// AbstractIndexedMapProvisionableLookupSession.java
//
//    A simple framework for providing a Provisionable lookup service
//    backed by a fixed collection of provisionables with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Provisionable lookup service backed by a
 *  fixed collection of provisionables. The provisionables are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some provisionables may be compatible
 *  with more types than are indicated through these provisionable
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Provisionables</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapProvisionableLookupSession
    extends AbstractMapProvisionableLookupSession
    implements org.osid.provisioning.ProvisionableLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.provisioning.Provisionable> provisionablesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.provisioning.Provisionable>());
    private final MultiMap<org.osid.type.Type, org.osid.provisioning.Provisionable> provisionablesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.provisioning.Provisionable>());


    /**
     *  Makes a <code>Provisionable</code> available in this session.
     *
     *  @param  provisionable a provisionable
     *  @throws org.osid.NullArgumentException <code>provisionable<code> is
     *          <code>null</code>
     */

    @Override
    protected void putProvisionable(org.osid.provisioning.Provisionable provisionable) {
        super.putProvisionable(provisionable);

        this.provisionablesByGenus.put(provisionable.getGenusType(), provisionable);
        
        try (org.osid.type.TypeList types = provisionable.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.provisionablesByRecord.put(types.getNextType(), provisionable);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a provisionable from this session.
     *
     *  @param provisionableId the <code>Id</code> of the provisionable
     *  @throws org.osid.NullArgumentException <code>provisionableId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeProvisionable(org.osid.id.Id provisionableId) {
        org.osid.provisioning.Provisionable provisionable;
        try {
            provisionable = getProvisionable(provisionableId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.provisionablesByGenus.remove(provisionable.getGenusType());

        try (org.osid.type.TypeList types = provisionable.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.provisionablesByRecord.remove(types.getNextType(), provisionable);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeProvisionable(provisionableId);
        return;
    }


    /**
     *  Gets a <code>ProvisionableList</code> corresponding to the given
     *  provisionable genus <code>Type</code> which does not include
     *  provisionables of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known provisionables or an error results. Otherwise,
     *  the returned list may contain only those provisionables that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  provisionableGenusType a provisionable genus type 
     *  @return the returned <code>Provisionable</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>provisionableGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableList getProvisionablesByGenusType(org.osid.type.Type provisionableGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.provisionable.ArrayProvisionableList(this.provisionablesByGenus.get(provisionableGenusType)));
    }


    /**
     *  Gets a <code>ProvisionableList</code> containing the given
     *  provisionable record <code>Type</code>. In plenary mode, the
     *  returned list contains all known provisionables or an error
     *  results. Otherwise, the returned list may contain only those
     *  provisionables that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  provisionableRecordType a provisionable record type 
     *  @return the returned <code>provisionable</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>provisionableRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableList getProvisionablesByRecordType(org.osid.type.Type provisionableRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.provisionable.ArrayProvisionableList(this.provisionablesByRecord.get(provisionableRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.provisionablesByGenus.clear();
        this.provisionablesByRecord.clear();

        super.close();

        return;
    }
}
