//
// AbstractMappingManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractMappingManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.mapping.MappingManager,
               org.osid.mapping.MappingProxyManager {

    private final Types locationRecordTypes                = new TypeRefSet();
    private final Types locationSearchRecordTypes          = new TypeRefSet();

    private final Types mapRecordTypes                     = new TypeRefSet();
    private final Types mapSearchRecordTypes               = new TypeRefSet();

    private final Types resourceLocationRecordTypes        = new TypeRefSet();

    private final Types coordinateTypes                    = new TypeRefSet();
    private final Types headingTypes                       = new TypeRefSet();
    private final Types spatialUnitRecordTypes             = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractMappingManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractMappingManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any map federation is exposed. Federation is exposed when a 
     *  specific map may be identified, selected and used to create a lookup 
     *  or admin session. Federation is not exposed when a set of maps appears 
     *  as a single map. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if looking up locations is supported. 
     *
     *  @return <code> true </code> if location lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationLookup() {
        return (false);
    }


    /**
     *  Tests if querying locations is supported. 
     *
     *  @return <code> true </code> if location query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationQuery() {
        return (false);
    }


    /**
     *  Tests if searching locations is supported. 
     *
     *  @return <code> true </code> if location search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationSearch() {
        return (false);
    }


    /**
     *  Tests if location <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if location administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationAdmin() {
        return (false);
    }


    /**
     *  Tests if a location <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if location notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationNotification() {
        return (false);
    }


    /**
     *  Tests if a location <code> </code> hierarchy service is supported. 
     *
     *  @return <code> true </code> if location hierarchy is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationHierarchy() {
        return (false);
    }


    /**
     *  Tests if a location hierarchy design service is supported. 
     *
     *  @return <code> true </code> if location hierarchy design is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests if a location map lookup service is supported. 
     *
     *  @return <code> true </code> if a location map lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationMap() {
        return (false);
    }


    /**
     *  Tests if a location map assignment service is supported. 
     *
     *  @return <code> true </code> if a location to map assignment service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationMapAssignment() {
        return (false);
    }


    /**
     *  Tests if a location smart map service is supported. 
     *
     *  @return <code> true </code> if a location smart map service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationSmartMap() {
        return (false);
    }


    /**
     *  Tests if a location adjacency service is supported. 
     *
     *  @return <code> true </code> if a location adjacency service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationAdjacency() {
        return (false);
    }


    /**
     *  Tests if a location spatial service is supported. 
     *
     *  @return <code> true </code> if a location spatial service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationSpatial() {
        return (false);
    }


    /**
     *  Tests if a resource location service is supported. 
     *
     *  @return <code> true </code> if a resource location service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceLocation() {
        return (false);
    }


    /**
     *  Tests if a resource location update service is supported. 
     *
     *  @return <code> true </code> if a resource location update service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceLocationUpdate() {
        return (false);
    }


    /**
     *  Tests if a resource location notification service is supported. 
     *
     *  @return <code> true </code> if a resource location notification 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceLocationNotification() {
        return (false);
    }


    /**
     *  Tests if a resource position notification service is supported. 
     *
     *  @return <code> true </code> if a resource position notification 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourcePositionNotification() {
        return (false);
    }


    /**
     *  Tests if a location service is supported for the current agent. 
     *
     *  @return <code> true </code> if my location is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMyLocation() {
        return (false);
    }


    /**
     *  Tests if looking up maps is supported. 
     *
     *  @return <code> true </code> if map lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMapLookup() {
        return (false);
    }


    /**
     *  Tests if querying maps is supported. 
     *
     *  @return <code> true </code> if a map query service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMapQuery() {
        return (false);
    }


    /**
     *  Tests if searching maps is supported. 
     *
     *  @return <code> true </code> if map search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMapSearch() {
        return (false);
    }


    /**
     *  Tests if map administrative service is supported. 
     *
     *  @return <code> true </code> if map administration is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMapAdmin() {
        return (false);
    }


    /**
     *  Tests if a map <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if map notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMapNotification() {
        return (false);
    }


    /**
     *  Tests for the availability of a map hierarchy traversal service. 
     *
     *  @return <code> true </code> if map hierarchy traversal is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMapHierarchy() {
        return (false);
    }


    /**
     *  Tests for the availability of a map hierarchy design service. 
     *
     *  @return <code> true </code> if map hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMapHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests if the mapping batch service is supported. 
     *
     *  @return <code> true </code> if maping batch service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMappingBatch() {
        return (false);
    }


    /**
     *  Tests if the mapping path service is supported. 
     *
     *  @return <code> true </code> if maping path service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMappingPath() {
        return (false);
    }


    /**
     *  Tests if the mapping route service is supported. 
     *
     *  @return <code> true </code> if maping route service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMappingRoute() {
        return (false);
    }


    /**
     *  Gets the supported <code> Location </code> record types. 
     *
     *  @return a list containing the supported <code> Location </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getLocationRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.locationRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Location </code> record type is supported. 
     *
     *  @param  locationRecordType a <code> Type </code> indicating a <code> 
     *          Location </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> locationRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsLocationRecordType(org.osid.type.Type locationRecordType) {
        return (this.locationRecordTypes.contains(locationRecordType));
    }


    /**
     *  Adds support for a location record type.
     *
     *  @param locationRecordType a location record type
     *  @throws org.osid.NullArgumentException
     *  <code>locationRecordType</code> is <code>null</code>
     */

    protected void addLocationRecordType(org.osid.type.Type locationRecordType) {
        this.locationRecordTypes.add(locationRecordType);
        return;
    }


    /**
     *  Removes support for a location record type.
     *
     *  @param locationRecordType a location record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>locationRecordType</code> is <code>null</code>
     */

    protected void removeLocationRecordType(org.osid.type.Type locationRecordType) {
        this.locationRecordTypes.remove(locationRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Location </code> search types. 
     *
     *  @return a list containing the supported <code> Location </code> search 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getLocationSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.locationSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Location </code> search type is supported. 
     *
     *  @param  locationSearchRecordType a <code> Type </code> indicating a 
     *          <code> Location </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> locationSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsLocationSearchRecordType(org.osid.type.Type locationSearchRecordType) {
        return (this.locationSearchRecordTypes.contains(locationSearchRecordType));
    }


    /**
     *  Adds support for a location search record type.
     *
     *  @param locationSearchRecordType a location search record type
     *  @throws org.osid.NullArgumentException
     *  <code>locationSearchRecordType</code> is <code>null</code>
     */

    protected void addLocationSearchRecordType(org.osid.type.Type locationSearchRecordType) {
        this.locationSearchRecordTypes.add(locationSearchRecordType);
        return;
    }


    /**
     *  Removes support for a location search record type.
     *
     *  @param locationSearchRecordType a location search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>locationSearchRecordType</code> is <code>null</code>
     */

    protected void removeLocationSearchRecordType(org.osid.type.Type locationSearchRecordType) {
        this.locationSearchRecordTypes.remove(locationSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Map </code> record types. 
     *
     *  @return a list containing the supported <code> Map </code> types 
     */

    @OSID @Override
    public org.osid.type.TypeList getMapRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.mapRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Map </code> record type is supported. 
     *
     *  @param  mapRecordType a <code> Type </code> indicating a <code> Map 
     *          </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> mapRecordType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean supportsMapRecordType(org.osid.type.Type mapRecordType) {
        return (this.mapRecordTypes.contains(mapRecordType));
    }


    /**
     *  Adds support for a map record type.
     *
     *  @param mapRecordType a map record type
     *  @throws org.osid.NullArgumentException
     *  <code>mapRecordType</code> is <code>null</code>
     */

    protected void addMapRecordType(org.osid.type.Type mapRecordType) {
        this.mapRecordTypes.add(mapRecordType);
        return;
    }


    /**
     *  Removes support for a map record type.
     *
     *  @param mapRecordType a map record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>mapRecordType</code> is <code>null</code>
     */

    protected void removeMapRecordType(org.osid.type.Type mapRecordType) {
        this.mapRecordTypes.remove(mapRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Map </code> search record types. 
     *
     *  @return a list containing the supported <code> Map </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getMapSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.mapSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Map </code> search record type is supported. 
     *
     *  @param  mapSearchRecordType a <code> Type </code> indicating a <code> 
     *          Map </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> mapSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsMapSearchRecordType(org.osid.type.Type mapSearchRecordType) {
        return (this.mapSearchRecordTypes.contains(mapSearchRecordType));
    }


    /**
     *  Adds support for a map search record type.
     *
     *  @param mapSearchRecordType a map search record type
     *  @throws org.osid.NullArgumentException
     *  <code>mapSearchRecordType</code> is <code>null</code>
     */

    protected void addMapSearchRecordType(org.osid.type.Type mapSearchRecordType) {
        this.mapSearchRecordTypes.add(mapSearchRecordType);
        return;
    }


    /**
     *  Removes support for a map search record type.
     *
     *  @param mapSearchRecordType a map search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>mapSearchRecordType</code> is <code>null</code>
     */

    protected void removeMapSearchRecordType(org.osid.type.Type mapSearchRecordType) {
        this.mapSearchRecordTypes.remove(mapSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code>ResourceLocationRecord</code> types.
     *
     *  @return a list containing the supported
     *          <code>ResourceLocationRecord</code> types
     */

    @OSID @Override
    public org.osid.type.TypeList getResourceLocationRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.resourceLocationRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code>ResourceLocationRecord</code> type is
     *  supported.
     *
     *  @param resourceLocationRecordType a <code> Type </code>
     *          indicating a <code>ResourceLocationRecord</code> type
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceLocationRecordType</code> is
     *          <code>null</code>
     */

    @OSID @Override
    public boolean supportsResourceLocationRecordType(org.osid.type.Type resourceLocationRecordType) {
        return (this.resourceLocationRecordTypes.contains(resourceLocationRecordType));
    }


    /**
     *  Adds support for a resource location record type.
     *
     *  @param resourceLocationRecordType a resource location record type
     *  @throws org.osid.NullArgumentException
     *  <code>resourceLocationRecordType</code> is <code>null</code>
     */

    protected void addResourceLocationRecordType(org.osid.type.Type resourceLocationRecordType) {
        this.resourceLocationRecordTypes.add(resourceLocationRecordType);
        return;
    }


    /**
     *  Removes support for a resource location record type.
     *
     *  @param resourceLocationRecordType a resource location record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>resourceLocationRecordType</code> is <code>null</code>
     */

    protected void removeResourceLocationRecordType(org.osid.type.Type resourceLocationRecordType) {
        this.resourceLocationRecordTypes.remove(resourceLocationRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Coordinate </code> types. 
     *
     *  @return a list containing the supported <code> Coordinate </code> 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCoordinateTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.coordinateTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Coordinate </code> type is supported. 
     *
     *  @param  coordinateType a <code> Type </code> indicating a <code> 
     *          Coordinate </code> type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> coordinateType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCoordinateType(org.osid.type.Type coordinateType) {
        return (this.coordinateTypes.contains(coordinateType));
    }


    /**
     *  Adds support for a coordinate type.
     *
     *  @param coordinateType a coordinate type
     *  @throws org.osid.NullArgumentException
     *  <code>coordinateType</code> is <code>null</code>
     */

    protected void addCoordinateType(org.osid.type.Type coordinateType) {
        this.coordinateTypes.add(coordinateType);
        return;
    }


    /**
     *  Removes support for a coordinate type.
     *
     *  @param coordinateType a coordinate type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>coordinateType</code> is <code>null</code>
     */

    protected void removeCoordinateType(org.osid.type.Type coordinateType) {
        this.coordinateTypes.remove(coordinateType);
        return;
    }


    /**
     *  Gets the supported <code> Heading </code> types. 
     *
     *  @return a list containing the supported <code> Heading </code> types 
     */

    @OSID @Override
    public org.osid.type.TypeList getHeadingTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.headingTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Heading </code> type is supported. 
     *
     *  @param  headingType a <code> Type </code> indicating a <code> Heading 
     *          </code> type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> headingType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean supportsHeadingType(org.osid.type.Type headingType) {
        return (this.headingTypes.contains(headingType));
    }


    /**
     *  Adds support for a heading type.
     *
     *  @param headingType a heading type
     *  @throws org.osid.NullArgumentException
     *  <code>headingType</code> is <code>null</code>
     */

    protected void addHeadingType(org.osid.type.Type headingType) {
        this.headingTypes.add(headingType);
        return;
    }


    /**
     *  Removes support for a heading type.
     *
     *  @param headingType a heading type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>headingType</code> is <code>null</code>
     */

    protected void removeHeadingType(org.osid.type.Type headingType) {
        this.headingTypes.remove(headingType);
        return;
    }


    /**
     *  Gets the supported <code> SpatialUnit </code> record types. 
     *
     *  @return a list containing the supported <code> SpatialUnit </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSpatialUnitRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.spatialUnitRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> SpatialUnit </code> record type is 
     *  supported. 
     *
     *  @param  spatialUnitRecordType a <code> Type </code> indicating a 
     *          <code> SpatialUnit </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> spatialUnitRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSpatialUnitRecordType(org.osid.type.Type spatialUnitRecordType) {
        return (this.spatialUnitRecordTypes.contains(spatialUnitRecordType));
    }


    /**
     *  Adds support for a spatial unit record type.
     *
     *  @param spatialUnitRecordType a spatial unit record type
     *  @throws org.osid.NullArgumentException
     *  <code>spatialUnitRecordType</code> is <code>null</code>
     */

    protected void addSpatialUnitRecordType(org.osid.type.Type spatialUnitRecordType) {
        this.spatialUnitRecordTypes.add(spatialUnitRecordType);
        return;
    }


    /**
     *  Removes support for a spatial unit record type.
     *
     *  @param spatialUnitRecordType a spatial unit record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>spatialUnitRecordType</code> is <code>null</code>
     */

    protected void removeSpatialUnitRecordType(org.osid.type.Type spatialUnitRecordType) {
        this.spatialUnitRecordTypes.remove(spatialUnitRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  lookup service. 
     *
     *  @return a <code> LocationLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationLookupSession getLocationLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingManager.getLocationLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LocationLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationLookupSession getLocationLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingProxyManager.getLocationLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  lookup service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the map 
     *  @return a <code> LocationLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationLookupSession getLocationLookupSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingManager.getLocationLookupSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  lookup service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the map 
     *  @param  proxy a proxy 
     *  @return a <code> LocationLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationLookupSession getLocationLookupSessionForMap(org.osid.id.Id mapId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingProxyManager.getLocationLookupSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location query 
     *  service. 
     *
     *  @return a <code> LocationQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLocationQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationQuerySession getLocationQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingManager.getLocationQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LocationQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLocationQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationQuerySession getLocationQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingProxyManager.getLocationQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location query 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> LocationQuerySession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLocationQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationQuerySession getLocationQuerySessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingManager.getLocationQuerySessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location query 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the map 
     *  @param  proxy a proxy 
     *  @return a <code> LocationQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLocationQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationQuerySession getLocationQuerySessionForMap(org.osid.id.Id mapId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingProxyManager.getLocationQuerySessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  search service. 
     *
     *  @return a <code> LocationSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationSearchSession getLocationSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingManager.getLocationSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LocationSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationSearchSession getLocationSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingProxyManager.getLocationSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  search service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> LocationSearchSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationSearchSession getLocationSearchSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingManager.getLocationSearchSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  search service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> LocationSearchSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationSearchSession getLocationSearchSessionForMap(org.osid.id.Id mapId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingProxyManager.getLocationSearchSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  administration service. 
     *
     *  @return a <code> LocationAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLocationAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationAdminSession getLocationAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingManager.getLocationAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LocationAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLocationAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationAdminSession getLocationAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingProxyManager.getLocationAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  administration service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> LocationAdminSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLocationAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationAdminSession getLocationAdminSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingManager.getLocationAdminSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  administration service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> LocationAdminSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLocationAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationAdminSession getLocationAdminSessionForMap(org.osid.id.Id mapId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingProxyManager.getLocationAdminSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  notification service. 
     *
     *  @param  locationReceiver the notification callback 
     *  @return a <code> LocationNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> locationReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationNotificationSession getLocationNotificationSession(org.osid.mapping.LocationReceiver locationReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingManager.getLocationNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  notification service. 
     *
     *  @param  locationReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> LocationNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> locationReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationNotificationSession getLocationNotificationSession(org.osid.mapping.LocationReceiver locationReceiver, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingProxyManager.getLocationNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  notification service for the given map. 
     *
     *  @param  locationReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> LocationNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> locationReceiver </code> 
     *          or <code> mapId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationNotificationSession getLocationNotificationSessionForMap(org.osid.mapping.LocationReceiver locationReceiver, 
                                                                                             org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingManager.getLocationNotificationSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  notification service for the given map. 
     *
     *  @param  locationReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> LocationNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> locationReceiver, mapId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationNotificationSession getLocationNotificationSessionForMap(org.osid.mapping.LocationReceiver locationReceiver, 
                                                                                             org.osid.id.Id mapId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingProxyManager.getLocationNotificationSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  hierarchy service. 
     *
     *  @return a <code> LocationHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationHierarchySession getLocationHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingManager.getLocationHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  hierarchy service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LocationHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationHierarchySession getLocationHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingProxyManager.getLocationHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  hierarchy service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> LocationHierarchySession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationHierarchy() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationHierarchySession getLocationHierarchySessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingManager.getLocationHierarchySessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  hierarchy service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> LocationHierarchySession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationHierarchy() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationHierarchySession getLocationHierarchySessionForMap(org.osid.id.Id mapId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingProxyManager.getLocationHierarchySessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  hierarchy design service. 
     *
     *  @return a <code> LocationHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationHierarchyDesignSession getLocationHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingManager.getLocationHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  hierarchy design service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LocationHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationHierarchyDesignSession getLocationHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingProxyManager.getLocationHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  hierarchy design service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> LocationHierarchySession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationHierarchyDesign() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationHierarchyDesignSession getLocationHierarchyDesignSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingManager.getLocationHierarchyDesignSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  hierarchy design service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> LocationHierarchySession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationHierarchyDesign() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationHierarchyDesignSession getLocationHierarchyDesignSessionForMap(org.osid.id.Id mapId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingProxyManager.getLocationHierarchyDesignSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup location/map mappings. 
     *
     *  @return a <code> LocationMapSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLocationMap() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationMapSession getLocationMapSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingManager.getLocationMapSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup location/map mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LocationMapSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLocationMap() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationMapSession getLocationMapSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingProxyManager.getLocationMapSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  locations to maps. 
     *
     *  @return a <code> LocationMapAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationMapAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationMapAssignmentSession getLocationMapAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingManager.getLocationMapAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  locations to maps. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LocationMapAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationMapAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationMapAssignmentSession getLocationMapAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingProxyManager.getLocationMapAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage locatin smart maps. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> LocationSmartMapSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationSmartMap() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationSmartMapSession getLocationSmartMapSession(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingManager.getLocationSmartMapSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage location smart maps. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> LocationSmartMapSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationSmartMap() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationSmartMapSession getLocationSmartMapSession(org.osid.id.Id mapId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingProxyManager.getLocationSmartMapSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  adjacency service. 
     *
     *  @return a <code> LocationAdjacencySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationAdjacency() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationAdjacencySession getLocationAdjacencySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingManager.getLocationAdjacencySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  adjacency service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LocationAdjacencySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationAdjacency() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationAdjacencySession getLocationAdjacencySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingProxyManager.getLocationAdjacencySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  adjacency service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> LocationAdjacencySession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationAdjacency() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationAdjacencySession getLocationAdjacencySessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingManager.getLocationAdjacencySessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  adjacency service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> LocationAdjacencySession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationAdjacency() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationAdjacencySession getLocationAdjacencySessionForMap(org.osid.id.Id mapId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingProxyManager.getLocationAdjacencySessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  spatial service. 
     *
     *  @return a <code> LocationSpatialSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationSpatial() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationSpatialSession getLocationSpatialSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingManager.getLocationSpatialSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  spatial service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LocationSpatialSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationSpatial() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationSpatialSession getLocationSpatialSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingProxyManager.getLocationSpatialSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  spatial service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> LocationSpatialSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationSpatial() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationSpatialSession getLocationSpatialSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingManager.getLocationSpatialSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  spatial service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> LocationSpatialSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationSpatial() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationSpatialSession getLocationSpatialSessionForMap(org.osid.id.Id mapId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingProxyManager.getLocationSpatialSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  location service. 
     *
     *  @return a <code> ResourceLocationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceLocation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.ResourceLocationSession getResourceLocationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingManager.getResourceLocationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  location service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ResourceLocationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceLocation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.ResourceLocationSession getResourceLocationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingProxyManager.getResourceLocationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  location service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> ResourceLocationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceLocation() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.ResourceLocationSession getResourceLocationSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingManager.getResourceLocationSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  location service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ResourceLocationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceLocation() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.ResourceLocationSession getResourceLocationSessionForMap(org.osid.id.Id mapId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingProxyManager.getResourceLocationSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  location update service. 
     *
     *  @return a <code> ResourceLocationUpdateSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceLocationUpdate() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.ResourceLocationUpdateSession getResourceLocationUpdateSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingManager.getResourceLocationUpdateSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  location update service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ResourceLocationUpdateSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceLocationUpdate() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.ResourceLocationUpdateSession getResourceLocationUpdateSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingProxyManager.getResourceLocationUpdateSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  location update service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> ResourceLocationUpdateSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceLocationUpdate() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.ResourceLocationUpdateSession getResourceLocationUpdateSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingManager.getResourceLocationUpdateSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  location update service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ResourceLocationUpdateSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceLocationUpdate() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.ResourceLocationUpdateSession getResourceLocationUpdateSessionForMap(org.osid.id.Id mapId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingProxyManager.getResourceLocationUpdateSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  location notification service. 
     *
     *  @param  resourceLocationReceiver the notification callback 
     *  @return a <code> ResourceLocationNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> resourceLocationReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceLocationNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.ResourceLocationNotificationSession getResourceLocationNotificationSession(org.osid.mapping.ResourceLocationReceiver resourceLocationReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingManager.getResourceLocationNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  location notification service. 
     *
     *  @param  resourceLocationReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> ResourceLocationNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> resourceLocationReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceLocationNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.ResourceLocationNotificationSession getResourceLocationNotificationSession(org.osid.mapping.ResourceLocationReceiver resourceLocationReceiver, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingProxyManager.getResourceLocationNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  location notification service for the given map. 
     *
     *  @param  resourceLocationReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> ResourceLocationNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> resourceLocationReceiver 
     *          </code> or <code> mapId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceLocationNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.ResourceLocationNotificationSession getResourceLocationNotificationSessionForMap(org.osid.mapping.ResourceLocationReceiver resourceLocationReceiver, 
                                                                                                             org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingManager.getResourceLocationNotificationSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  location notification service for the given map. 
     *
     *  @param  resourceLocationReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ResourceLocationNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          resourceLocationReceiver, mapId </code> or <code> proxy 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceLocationNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.ResourceLocationNotificationSession getResourceLocationNotificationSessionForMap(org.osid.mapping.ResourceLocationReceiver resourceLocationReceiver, 
                                                                                                             org.osid.id.Id mapId, 
                                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingProxyManager.getResourceLocationNotificationSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  position notification service. 
     *
     *  @param  resourcePositionReceiver the notification callback 
     *  @return a <code> ResourcePositionNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> resourcePositionReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourcePositionNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.ResourcePositionNotificationSession getResourcePositionNotificationSession(org.osid.mapping.ResourcePositionReceiver resourcePositionReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingManager.getResourcePositionNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  position notification service. 
     *
     *  @param  resourcePositionReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> ResourcePositionNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> resourcePositionReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourcePositionNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.ResourceLocationNotificationSession getResourcePositionNotificationSession(org.osid.mapping.ResourcePositionReceiver resourcePositionReceiver, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingProxyManager.getResourcePositionNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  position notification service for the given map. 
     *
     *  @param  resourcePositionReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> ResourcePositionNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> resourcePositionReceiver 
     *          </code> or <code> mapId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourcePositionNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.ResourcePositionNotificationSession getResourcePositionNotificationSessionForMap(org.osid.mapping.ResourcePositionReceiver resourcePositionReceiver, 
                                                                                                             org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingManager.getResourcePositionNotificationSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  position notification service for the given map. 
     *
     *  @param  resourcePositionReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ResourcePositionNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          resourcePositionReceiver, mapId </code> or <code> proxy 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourcePositionNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.ResourcePositionNotificationSession getResourcePositionNotificationSessionForMap(org.osid.mapping.ResourcePositionReceiver resourcePositionReceiver, 
                                                                                                             org.osid.id.Id mapId, 
                                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingProxyManager.getResourcePositionNotificationSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my location 
     *  service. 
     *
     *  @return a <code> MyLocationLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMyLocationLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MyLocationSession getMyLocationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingManager.getMyLocationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my location 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MyLocationLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMyLocationLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MyLocationSession getMyLocationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingProxyManager.getMyLocationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my location 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the map 
     *  @return a <code> MyLocationLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMyLocationLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MyLocationSession getMyLocationSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingManager.getMyLocationSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my location 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the map 
     *  @param  proxy a proxy 
     *  @return a <code> MyLocationLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMyLocationLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MyLocationSession getMyLocationSessionForMap(org.osid.id.Id mapId, 
                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingProxyManager.getMyLocationSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the map lookup 
     *  service. 
     *
     *  @return a <code> MapLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMapLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MapLookupSession getMapLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingManager.getMapLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the map lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MapLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMapLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MapLookupSession getMapLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingProxyManager.getMapLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the map query 
     *  service. 
     *
     *  @return a <code> MapQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMapQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MapQuerySession getMapQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingManager.getMapQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the map query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MapQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMapQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MapQuerySession getMapQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingProxyManager.getMapQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the map search 
     *  service. 
     *
     *  @return a <code> MapSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMapSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MapSearchSession getMapSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingManager.getMapSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the map search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MapSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMapSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MapSearchSession getMapSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingProxyManager.getMapSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the map 
     *  administrative service. 
     *
     *  @return a <code> MapAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMapAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MapAdminSession getMapAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingManager.getMapAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the map 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MapAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMapAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MapAdminSession getMapAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingProxyManager.getMapAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the map 
     *  notification service. 
     *
     *  @param  mapReceiver the notification callback 
     *  @return a <code> MapNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> mapReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMapNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MapNotificationSession getMapNotificationSession(org.osid.mapping.MapReceiver mapReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingManager.getMapNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the map 
     *  notification service. 
     *
     *  @param  mapReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> MapNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> mapReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMapNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MapNotificationSession getMapNotificationSession(org.osid.mapping.MapReceiver mapReceiver, 
                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingProxyManager.getMapNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the map hierarchy 
     *  service. 
     *
     *  @return a <code> MapHierarchySession </code> for maps 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMapHierarchy() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MapHierarchySession getMapHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingManager.getMapHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the map hierarchy 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MapHierarchySession </code> for maps 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMapHierarchy() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MapHierarchySession getMapHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingProxyManager.getMapHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the map hierarchy 
     *  design service. 
     *
     *  @return a <code> HierarchyDesignSession </code> for maps 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMapHierarchyDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MapHierarchyDesignSession getMapHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingManager.getMapHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the map hierarchy 
     *  design service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HierarchyDesignSession </code> for maps 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMapHierarchyDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MapHierarchyDesignSession getMapHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingProxyManager.getMapHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the mapping batch manager. 
     *
     *  @return a <code> MappingBatchManager </code> for paths 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMappingBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.batch.MappingBatchManager getMappingBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingManager.getMappingBatchManager not implemented");
    }


    /**
     *  Gets the mapping batch manager. 
     *
     *  @return a <code> MappingBatchProxyManager </code> for paths 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMappingBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.batch.MappingBatchProxyManager getMappingBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingProxyManager.getMappingBatchProxyManager not implemented");
    }


    /**
     *  Gets the mapping path manager. 
     *
     *  @return a <code> MappingPathManager </code> for paths 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMappingPath() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.MappingPathManager getMappingPathManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingManager.getMappingPathManager not implemented");
    }


    /**
     *  Gets the mapping path manager. 
     *
     *  @return a <code> MappingPathProxyManager </code> for paths 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMappingPath() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.MappingPathProxyManager getMappingPathProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingProxyManager.getMappingPathProxyManager not implemented");
    }


    /**
     *  Gets the mapping route manager. 
     *
     *  @return a <code> MappingRouteManager </code> for routes 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMappingRoute() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.MappingRouteManager getMappingRouteManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingManager.getMappingRouteManager not implemented");
    }


    /**
     *  Gets the mapping route manager. 
     *
     *  @return a <code> MappingRouteProxyManager </code> for routes 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMappingRoute() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.MappingRouteProxyManager getMappingRouteProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.MappingProxyManager.getMappingRouteProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.locationRecordTypes.clear();
        this.locationRecordTypes.clear();

        this.locationSearchRecordTypes.clear();
        this.locationSearchRecordTypes.clear();

        this.mapRecordTypes.clear();
        this.mapRecordTypes.clear();

        this.mapSearchRecordTypes.clear();
        this.mapSearchRecordTypes.clear();

        this.resourceLocationRecordTypes.clear();

        this.coordinateTypes.clear();
        this.coordinateTypes.clear();

        this.headingTypes.clear();
        this.headingTypes.clear();

        this.spatialUnitRecordTypes.clear();
        this.spatialUnitRecordTypes.clear();

        return;
    }
}
