//
// AbstractParameterProcessorEnablerSearch.java
//
//     A template for making a ParameterProcessorEnabler Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.configuration.rules.parameterprocessorenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing parameter processor enabler searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractParameterProcessorEnablerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.configuration.rules.ParameterProcessorEnablerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.configuration.rules.records.ParameterProcessorEnablerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.configuration.rules.ParameterProcessorEnablerSearchOrder parameterProcessorEnablerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of parameter processor enablers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  parameterProcessorEnablerIds list of parameter processor enablers
     *  @throws org.osid.NullArgumentException
     *          <code>parameterProcessorEnablerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongParameterProcessorEnablers(org.osid.id.IdList parameterProcessorEnablerIds) {
        while (parameterProcessorEnablerIds.hasNext()) {
            try {
                this.ids.add(parameterProcessorEnablerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongParameterProcessorEnablers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of parameter processor enabler Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getParameterProcessorEnablerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  parameterProcessorEnablerSearchOrder parameter processor enabler search order 
     *  @throws org.osid.NullArgumentException
     *          <code>parameterProcessorEnablerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>parameterProcessorEnablerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderParameterProcessorEnablerResults(org.osid.configuration.rules.ParameterProcessorEnablerSearchOrder parameterProcessorEnablerSearchOrder) {
	this.parameterProcessorEnablerSearchOrder = parameterProcessorEnablerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.configuration.rules.ParameterProcessorEnablerSearchOrder getParameterProcessorEnablerSearchOrder() {
	return (this.parameterProcessorEnablerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given parameter processor enabler search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a parameter processor enabler implementing the requested record.
     *
     *  @param parameterProcessorEnablerSearchRecordType a parameter processor enabler search record
     *         type
     *  @return the parameter processor enabler search record
     *  @throws org.osid.NullArgumentException
     *          <code>parameterProcessorEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(parameterProcessorEnablerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.configuration.rules.records.ParameterProcessorEnablerSearchRecord getParameterProcessorEnablerSearchRecord(org.osid.type.Type parameterProcessorEnablerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.configuration.rules.records.ParameterProcessorEnablerSearchRecord record : this.records) {
            if (record.implementsRecordType(parameterProcessorEnablerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(parameterProcessorEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this parameter processor enabler search. 
     *
     *  @param parameterProcessorEnablerSearchRecord parameter processor enabler search record
     *  @param parameterProcessorEnablerSearchRecordType parameterProcessorEnabler search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addParameterProcessorEnablerSearchRecord(org.osid.configuration.rules.records.ParameterProcessorEnablerSearchRecord parameterProcessorEnablerSearchRecord, 
                                           org.osid.type.Type parameterProcessorEnablerSearchRecordType) {

        addRecordType(parameterProcessorEnablerSearchRecordType);
        this.records.add(parameterProcessorEnablerSearchRecord);        
        return;
    }
}
