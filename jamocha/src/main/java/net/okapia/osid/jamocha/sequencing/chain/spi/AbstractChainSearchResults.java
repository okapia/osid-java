//
// AbstractChainSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.sequencing.chain.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractChainSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.sequencing.ChainSearchResults {

    private org.osid.sequencing.ChainList chains;
    private final org.osid.sequencing.ChainQueryInspector inspector;
    private final java.util.Collection<org.osid.sequencing.records.ChainSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractChainSearchResults.
     *
     *  @param chains the result set
     *  @param chainQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>chains</code>
     *          or <code>chainQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractChainSearchResults(org.osid.sequencing.ChainList chains,
                                            org.osid.sequencing.ChainQueryInspector chainQueryInspector) {
        nullarg(chains, "chains");
        nullarg(chainQueryInspector, "chain query inspectpr");

        this.chains = chains;
        this.inspector = chainQueryInspector;

        return;
    }


    /**
     *  Gets the chain list resulting from a search.
     *
     *  @return a chain list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.sequencing.ChainList getChains() {
        if (this.chains == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.sequencing.ChainList chains = this.chains;
        this.chains = null;
	return (chains);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.sequencing.ChainQueryInspector getChainQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  chain search record <code> Type. </code> This method must
     *  be used to retrieve a chain implementing the requested
     *  record.
     *
     *  @param chainSearchRecordType a chain search 
     *         record type 
     *  @return the chain search
     *  @throws org.osid.NullArgumentException
     *          <code>chainSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(chainSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.sequencing.records.ChainSearchResultsRecord getChainSearchResultsRecord(org.osid.type.Type chainSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.sequencing.records.ChainSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(chainSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(chainSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record chain search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addChainRecord(org.osid.sequencing.records.ChainSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "chain record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
