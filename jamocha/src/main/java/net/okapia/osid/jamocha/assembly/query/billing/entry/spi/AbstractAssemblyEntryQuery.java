//
// AbstractAssemblyEntryQuery.java
//
//     An EntryQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.billing.entry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An EntryQuery that stores terms.
 */

public abstract class AbstractAssemblyEntryQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.billing.EntryQuery,
               org.osid.billing.EntryQueryInspector,
               org.osid.billing.EntrySearchOrder {

    private final java.util.Collection<org.osid.billing.records.EntryQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.billing.records.EntryQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.billing.records.EntrySearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyEntryQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyEntryQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the customer <code> Id </code> for this query. 
     *
     *  @param  customerId a customer <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> customerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCustomerId(org.osid.id.Id customerId, boolean match) {
        getAssembler().addIdTerm(getCustomerIdColumn(), customerId, match);
        return;
    }


    /**
     *  Clears the customer <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCustomerIdTerms() {
        getAssembler().clearTerms(getCustomerIdColumn());
        return;
    }


    /**
     *  Gets the item <code> Id </code> query terms. 
     *
     *  @return the item <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCustomerIdTerms() {
        return (getAssembler().getIdTerms(getCustomerIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by customer. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCustomer(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCustomerColumn(), style);
        return;
    }


    /**
     *  Gets the CustomerId column name.
     *
     * @return the column name
     */

    protected String getCustomerIdColumn() {
        return ("customer_id");
    }


    /**
     *  Tests if a <code> CustomerQuery </code> is available. 
     *
     *  @return <code> true </code> if a customer query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCustomerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a customer. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the customer query 
     *  @throws org.osid.UnimplementedException <code> supportsCustomerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerQuery getCustomerQuery() {
        throw new org.osid.UnimplementedException("supportsCustomerQuery() is false");
    }


    /**
     *  Clears the customer terms. 
     */

    @OSID @Override
    public void clearCustomerTerms() {
        getAssembler().clearTerms(getCustomerColumn());
        return;
    }


    /**
     *  Gets the item query terms. 
     *
     *  @return the item query terms 
     */

    @OSID @Override
    public org.osid.billing.CustomerQueryInspector[] getCustomerTerms() {
        return (new org.osid.billing.CustomerQueryInspector[0]);
    }


    /**
     *  Tests if a customer search order is available. 
     *
     *  @return <code> true </code> if a customer search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCustomerSearchOrder() {
        return (false);
    }


    /**
     *  Gets the customer order. 
     *
     *  @return the customer search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCustomerSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerSearchOrder getCustomerSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCustomerSearchOrder() is false");
    }


    /**
     *  Gets the Customer column name.
     *
     * @return the column name
     */

    protected String getCustomerColumn() {
        return ("customer");
    }


    /**
     *  Sets the item <code> Id </code> for this query. 
     *
     *  @param  itemId an item <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> itemId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchItemId(org.osid.id.Id itemId, boolean match) {
        getAssembler().addIdTerm(getItemIdColumn(), itemId, match);
        return;
    }


    /**
     *  Clears the item <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearItemIdTerms() {
        getAssembler().clearTerms(getItemIdColumn());
        return;
    }


    /**
     *  Gets the item <code> Id </code> query terms. 
     *
     *  @return the item <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getItemIdTerms() {
        return (getAssembler().getIdTerms(getItemIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by item. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByItem(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getItemColumn(), style);
        return;
    }


    /**
     *  Gets the ItemId column name.
     *
     * @return the column name
     */

    protected String getItemIdColumn() {
        return ("item_id");
    }


    /**
     *  Tests if an <code> ItemQuery </code> is available. 
     *
     *  @return <code> true </code> if an item query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemQuery() {
        return (false);
    }


    /**
     *  Gets the query for an item. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the item query 
     *  @throws org.osid.UnimplementedException <code> supportsItemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.ItemQuery getItemQuery() {
        throw new org.osid.UnimplementedException("supportsItemQuery() is false");
    }


    /**
     *  Clears the item terms. 
     */

    @OSID @Override
    public void clearItemTerms() {
        getAssembler().clearTerms(getItemColumn());
        return;
    }


    /**
     *  Gets the item query terms. 
     *
     *  @return the item query terms 
     */

    @OSID @Override
    public org.osid.billing.ItemQueryInspector[] getItemTerms() {
        return (new org.osid.billing.ItemQueryInspector[0]);
    }


    /**
     *  Tests if an item search order is available. 
     *
     *  @return <code> true </code> if an item search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemSearchOrder() {
        return (false);
    }


    /**
     *  Gets the item search order. 
     *
     *  @return the item search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsItemSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.ItemSearchOrder getItemSearchOrder() {
        throw new org.osid.UnimplementedException("supportsItemSearchOrder() is false");
    }


    /**
     *  Gets the Item column name.
     *
     * @return the column name
     */

    protected String getItemColumn() {
        return ("item");
    }


    /**
     *  Sets the period <code> Id </code> for this query to match categories 
     *  that have a related term. 
     *
     *  @param  periodId a billing period <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> periodId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPeriodId(org.osid.id.Id periodId, boolean match) {
        getAssembler().addIdTerm(getPeriodIdColumn(), periodId, match);
        return;
    }


    /**
     *  Clears the period <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPeriodIdTerms() {
        getAssembler().clearTerms(getPeriodIdColumn());
        return;
    }


    /**
     *  Gets the period <code> Id </code> query terms. 
     *
     *  @return the period <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPeriodIdTerms() {
        return (getAssembler().getIdTerms(getPeriodIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by billing period. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPeriod(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getPeriodColumn(), style);
        return;
    }


    /**
     *  Gets the PeriodId column name.
     *
     * @return the column name
     */

    protected String getPeriodIdColumn() {
        return ("period_id");
    }


    /**
     *  Tests if a <code> PeriodQuery </code> is available. 
     *
     *  @return <code> true </code> if a period query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPeriodQuery() {
        return (false);
    }


    /**
     *  Gets the query for a reporting period. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the period query 
     *  @throws org.osid.UnimplementedException <code> supportsPeriodQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodQuery getPeriodQuery() {
        throw new org.osid.UnimplementedException("supportsPeriodQuery() is false");
    }


    /**
     *  Clears the period terms. 
     */

    @OSID @Override
    public void clearPeriodTerms() {
        getAssembler().clearTerms(getPeriodColumn());
        return;
    }


    /**
     *  Gets the period query terms. 
     *
     *  @return the period query terms 
     */

    @OSID @Override
    public org.osid.billing.PeriodQueryInspector[] getPeriodTerms() {
        return (new org.osid.billing.PeriodQueryInspector[0]);
    }


    /**
     *  Tests if a billing period search order is available. 
     *
     *  @return <code> true </code> if a term search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPeriodSearchOrder() {
        return (false);
    }


    /**
     *  Gets the billing period search order. 
     *
     *  @return the period search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPeriodSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodSearchOrder getPeriodSearchOrder() {
        throw new org.osid.UnimplementedException("supportsPeriodSearchOrder() is false");
    }


    /**
     *  Gets the Period column name.
     *
     * @return the column name
     */

    protected String getPeriodColumn() {
        return ("period");
    }


    /**
     *  Matches entries with a quantity between the given range inclusive. 
     *
     *  @param  low start of range 
     *  @param  high end of range 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchQuantity(long low, long high, boolean match) {
        getAssembler().addCardinalRangeTerm(getQuantityColumn(), low, high, match);
        return;
    }


    /**
     *  Clears the quantity terms. 
     */

    @OSID @Override
    public void clearQuantityTerms() {
        getAssembler().clearTerms(getQuantityColumn());
        return;
    }


    /**
     *  Gets the quantity terms. 
     *
     *  @return the quantity terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getQuantityTerms() {
        return (getAssembler().getCardinalRangeTerms(getQuantityColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the quantity. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByQuantity(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getQuantityColumn(), style);
        return;
    }


    /**
     *  Gets the Quantity column name.
     *
     * @return the column name
     */

    protected String getQuantityColumn() {
        return ("quantity");
    }


    /**
     *  Matches the amount between the given range inclusive. 
     *
     *  @param  low start of range 
     *  @param  high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     *  @throws org.osid.NullArgumentException <code> low </code> or <code> 
     *          high </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchAmount(org.osid.financials.Currency low, 
                            org.osid.financials.Currency high, boolean match) {
        getAssembler().addCurrencyRangeTerm(getAmountColumn(), low, high, match);
        return;
    }


    /**
     *  Matches items that have any amount set. 
     *
     *  @param  match <code> true </code> to match items with any amount, 
     *          <code> false </code> to match items with no amount 
     */

    @OSID @Override
    public void matchAnyAmount(boolean match) {
        getAssembler().addCurrencyRangeWildcardTerm(getAmountColumn(), match);
        return;
    }


    /**
     *  Clears the amount terms. 
     */

    @OSID @Override
    public void clearAmountTerms() {
        getAssembler().clearTerms(getAmountColumn());
        return;
    }


    /**
     *  Gets the amount query terms. 
     *
     *  @return the amount query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getAmountTerms() {
        return (getAssembler().getCurrencyRangeTerms(getAmountColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the amount. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAmount(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAmountColumn(), style);
        return;
    }


    /**
     *  Gets the Amount column name.
     *
     * @return the column name
     */

    protected String getAmountColumn() {
        return ("amount");
    }


    /**
     *  Matches items that have debit amounts. 
     *
     *  @param  match <code> true </code> to match items with a debit amount, 
     *          <code> false </code> to match items with a credit amount 
     */

    @OSID @Override
    public void matchDebit(boolean match) {
        getAssembler().addBooleanTerm(getDebitColumn(), match);
        return;
    }


    /**
     *  Clears the debit terms. 
     */

    @OSID @Override
    public void clearDebitTerms() {
        getAssembler().clearTerms(getDebitColumn());
        return;
    }


    /**
     *  Gets the debit query terms. 
     *
     *  @return the debit query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getDebitTerms() {
        return (getAssembler().getBooleanTerms(getDebitColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the debit flag. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDebit(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDebitColumn(), style);
        return;
    }


    /**
     *  Gets the Debit column name.
     *
     * @return the column name
     */

    protected String getDebitColumn() {
        return ("debit");
    }


    /**
     *  Sets the business <code> Id </code> for this query to match entries 
     *  assigned to businesses. 
     *
     *  @param  businessId the business <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBusinessId(org.osid.id.Id businessId, boolean match) {
        getAssembler().addIdTerm(getBusinessIdColumn(), businessId, match);
        return;
    }


    /**
     *  Clears the business <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBusinessIdTerms() {
        getAssembler().clearTerms(getBusinessIdColumn());
        return;
    }


    /**
     *  Gets the business <code> Id </code> query terms. 
     *
     *  @return the business <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBusinessIdTerms() {
        return (getAssembler().getIdTerms(getBusinessIdColumn()));
    }


    /**
     *  Gets the BusinessId column name.
     *
     * @return the column name
     */

    protected String getBusinessIdColumn() {
        return ("business_id");
    }


    /**
     *  Tests if a <code> BusinessQuery </code> is available. 
     *
     *  @return <code> true </code> if a business query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a business. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the business query 
     *  @throws org.osid.UnimplementedException <code> supportsBusinessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.BusinessQuery getBusinessQuery() {
        throw new org.osid.UnimplementedException("supportsBusinessQuery() is false");
    }


    /**
     *  Clears the business terms. 
     */

    @OSID @Override
    public void clearBusinessTerms() {
        getAssembler().clearTerms(getBusinessColumn());
        return;
    }


    /**
     *  Gets the business query terms. 
     *
     *  @return the business query terms 
     */

    @OSID @Override
    public org.osid.billing.BusinessQueryInspector[] getBusinessTerms() {
        return (new org.osid.billing.BusinessQueryInspector[0]);
    }


    /**
     *  Gets the Business column name.
     *
     * @return the column name
     */

    protected String getBusinessColumn() {
        return ("business");
    }


    /**
     *  Tests if this entry supports the given record
     *  <code>Type</code>.
     *
     *  @param  entryRecordType an entry record type 
     *  @return <code>true</code> if the entryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>entryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type entryRecordType) {
        for (org.osid.billing.records.EntryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(entryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  entryRecordType the entry record type 
     *  @return the entry query record 
     *  @throws org.osid.NullArgumentException
     *          <code>entryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(entryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.records.EntryQueryRecord getEntryQueryRecord(org.osid.type.Type entryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.records.EntryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(entryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(entryRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  entryRecordType the entry record type 
     *  @return the entry query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>entryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(entryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.records.EntryQueryInspectorRecord getEntryQueryInspectorRecord(org.osid.type.Type entryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.records.EntryQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(entryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(entryRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param entryRecordType the entry record type
     *  @return the entry search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>entryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(entryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.records.EntrySearchOrderRecord getEntrySearchOrderRecord(org.osid.type.Type entryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.records.EntrySearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(entryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(entryRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this entry. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param entryQueryRecord the entry query record
     *  @param entryQueryInspectorRecord the entry query inspector
     *         record
     *  @param entrySearchOrderRecord the entry search order record
     *  @param entryRecordType entry record type
     *  @throws org.osid.NullArgumentException
     *          <code>entryQueryRecord</code>,
     *          <code>entryQueryInspectorRecord</code>,
     *          <code>entrySearchOrderRecord</code> or
     *          <code>entryRecordTypeentry</code> is
     *          <code>null</code>
     */
            
    protected void addEntryRecords(org.osid.billing.records.EntryQueryRecord entryQueryRecord, 
                                      org.osid.billing.records.EntryQueryInspectorRecord entryQueryInspectorRecord, 
                                      org.osid.billing.records.EntrySearchOrderRecord entrySearchOrderRecord, 
                                      org.osid.type.Type entryRecordType) {

        addRecordType(entryRecordType);

        nullarg(entryQueryRecord, "entry query record");
        nullarg(entryQueryInspectorRecord, "entry query inspector record");
        nullarg(entrySearchOrderRecord, "entry search odrer record");

        this.queryRecords.add(entryQueryRecord);
        this.queryInspectorRecords.add(entryQueryInspectorRecord);
        this.searchOrderRecords.add(entrySearchOrderRecord);
        
        return;
    }
}
