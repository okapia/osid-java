//
// LogEntryMiter.java
//
//     Defines a LogEntry miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.tracking.logentry;


/**
 *  Defines a <code>LogEntry</code> miter for use with the builders.
 */

public interface LogEntryMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.tracking.LogEntry {


    /**
     *  Sets the agent.
     *
     *  @param agent an agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public void setAgent(org.osid.authentication.Agent agent);


    /**
     *  Sets the issue.
     *
     *  @param issue an issue
     *  @throws org.osid.NullArgumentException <code>issue</code> is
     *          <code>null</code>
     */

    public void setIssue(org.osid.tracking.Issue issue);


    /**
     *  Sets the date.
     *
     *  @param date a date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setDate(org.osid.calendaring.DateTime date);


    /**
     *  Sets the action.
     *
     *  @param action an action
     *  @throws org.osid.NullArgumentException <code>action</code> is
     *          <code>null</code>
     */

    public void setAction(org.osid.tracking.IssueAction action);


    /**
     *  Sets the summary.
     *
     *  @param summary a summary
     *  @throws org.osid.NullArgumentException <code>summary</code> is
     *          <code>null</code>
     */

    public void setSummary(org.osid.locale.DisplayText summary);


    /**
     *  Sets the message.
     *
     *  @param message a message
     *  @throws org.osid.NullArgumentException <code>message</code> is
     *          <code>null</code>
     */

    public void setMessage(org.osid.locale.DisplayText message);


    /**
     *  Adds a LogEntry record.
     *
     *  @param record a logEntry record
     *  @param recordType the type of logEntry record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addLogEntryRecord(org.osid.tracking.records.LogEntryRecord record, org.osid.type.Type recordType);
}       


