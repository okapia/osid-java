//
// TimeMetadata.java
//
//     Defines a time Metadata.
//
//
// Tom Coppeto
// Okapia
// 11 January 2023
//
//
// Copyright (c) 2023 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metadata;


/**
 *  Defines a time Metadata.
 */

public final class TimeMetadata
    extends net.okapia.osid.jamocha.metadata.spi.AbstractTimeMetadata
    implements org.osid.Metadata {


    /**
     *  Constructs a new single unlinked {@code TimeMetadata}.
     *
     *  @param elementId the Id of the element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public TimeMetadata(org.osid.id.Id elementId) {
        super(elementId);
        return;
    }


    /**
     *  Constructs a new unlinked {@code TimeMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public TimeMetadata(org.osid.id.Id elementId, boolean isArray) {
        super(elementId, isArray, false);
        return;
    }


    /**
     *  Constructs a new {@code TimeMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @param isLinked {@code true} if the element is linked to
     *         another element, {@code false} otherwise
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public TimeMetadata(org.osid.id.Id elementId, boolean isArray, boolean isLinked) {
        super(elementId, isArray, isLinked);
        return;
    }


    /**
     *  Sets the resolution.
     *
     *  @param resolution the smallest resolution allowed
     *  @throws org.osid.NullARgumentException {@code resolution} is
     *          {@code null}
     */

    public void setResolution(org.osid.calendaring.DateTimeResolution resolution) {
        super.setResolution(resolution);
        return;
    }


    /**
     *  Add support for a time type.
     *
     *  @param timeType the type of time
     *  @throws org.osid.NullArgumentException {@code timeType} is
     *          {@code null}
     */

    public void addTimeType(org.osid.type.Type timeType) {
        super.addTimeType(timeType);
        return;
    }


    /**
     *  Sets the min and max times.
     *
     *  @param min the minimum value
     *  @param max the maximum value
     *  @throws org.osid.InvalidArgumentException {@code min} is
     *          greater than {@code max}
     *  @throws org.osid.NullArgumentException {@code min} or
     *          {@code max} is {@code null}
     */

    public void setTimeRange(org.osid.calendaring.Time min, org.osid.calendaring.Time max) {
        super.setTimeRange(min, max);
        return;
    }


    /**
     *  Sets the time set.
     *
     *  @param values a collection of accepted time values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    public void setTimeSet(java.util.Collection<org.osid.calendaring.Time> values) {
        super.setTimeSet(values);
        return;
    }


    /**
     *  Adds a collection of values to the time set.
     *
     *  @param values a collection of accepted time values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    public void addToTimeSet(java.util.Collection<org.osid.calendaring.Time> values) {
        super.addToTimeSet(values);
        return;
    }


    /**
     *  Adds a value to the time set.
     *
     *  @param value a time value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void addToTimeSet(org.osid.calendaring.Time value) {
        super.addToTimeSet(value);
        return;
    }


    /**
     *  Removes a value from the time set.
     *
     *  @param value a time value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void removeFromTimeSet(org.osid.calendaring.Time value) {
        super.removeFromTimeSet(value);
        return;
    }


    /**
     *  Clears the time set.
     */

    public void clearTimeSet() {
        super.clearTimeSet();
        return;
    }


    /**
     *  Sets the default time set.
     *
     *  @param values a collection of default time values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    public void setDefaultTimeValues(java.util.Collection<org.osid.calendaring.Time> values) {
        super.setDefaultTimeValues(values);
        return;
    }


    /**
     *  Adds a collection of default time values.
     *
     *  @param values a collection of default time values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    public void addDefaultTimeValues(java.util.Collection<org.osid.calendaring.Time> values) {
        super.addDefaultTimeValues(values);
        return;
    }


    /**
     *  Adds a default time value.
     *
     *  @param value a time value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void addDefaultTimeValue(org.osid.calendaring.Time value) {
        super.addDefaultTimeValue(value);
        return;
    }


    /**
     *  Removes a default time value.
     *
     *  @param value a time value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void removeDefaultTimeValue(org.osid.calendaring.Time value) {
        super.removeDefaultTimeValue(value);
        return;
    }


    /**
     *  Clears the default time values.
     */

    public void clearDefaultTimeValues() {
        super.clearDefaultTimeValues();
        return;
    }


    /**
     *  Sets the existing time set.
     *
     *  @param values a collection of existing time values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    public void setExistingTimeValues(java.util.Collection<org.osid.calendaring.Time> values) {
        super.setExistingTimeValues(values);
        return;
    }


    /**
     *  Adds a collection of existing time values.
     *
     *  @param values a collection of existing time values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    public void addExistingTimeValues(java.util.Collection<org.osid.calendaring.Time> values) {
        super.addExistingTimeValues(values);
        return;
    }


    /**
     *  Adds a existing time value.
     *
     *  @param value a time value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void addExistingTimeValue(org.osid.calendaring.Time value) {
        super.addExistingTimeValue(value);
        return;
    }


    /**
     *  Removes a existing time value.
     *
     *  @param value a time value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void removeExistingTimeValue(org.osid.calendaring.Time value) {
        super.removeExistingTimeValue(value);
        return;
    }


    /**
     *  Clears the existing time values.
     */

    public void clearExistingTimeValues() {
        super.clearExistingTimeValues();
        return;
    }        
}
