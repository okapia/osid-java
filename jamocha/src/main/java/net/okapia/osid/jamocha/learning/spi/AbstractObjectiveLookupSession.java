//
// AbstractObjectiveLookupSession.java
//
//    A starter implementation framework for providing an Objective
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.learning.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an Objective
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getObjectives(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractObjectiveLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.learning.ObjectiveLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.learning.ObjectiveBank objectiveBank = new net.okapia.osid.jamocha.nil.learning.objectivebank.UnknownObjectiveBank();
    

    /**
     *  Gets the <code>ObjectiveBank/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>ObjectiveBank Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getObjectiveBankId() {
        return (this.objectiveBank.getId());
    }


    /**
     *  Gets the <code>ObjectiveBank</code> associated with this 
     *  session.
     *
     *  @return the <code>ObjectiveBank</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBank getObjectiveBank()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.objectiveBank);
    }


    /**
     *  Sets the <code>ObjectiveBank</code>.
     *
     *  @param  objectiveBank the objective bank for this session
     *  @throws org.osid.NullArgumentException <code>objectiveBank</code>
     *          is <code>null</code>
     */

    protected void setObjectiveBank(org.osid.learning.ObjectiveBank objectiveBank) {
        nullarg(objectiveBank, "objective bank");
        this.objectiveBank = objectiveBank;
        return;
    }


    /**
     *  Tests if this user can perform <code>Objective</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupObjectives() {
        return (true);
    }


    /**
     *  A complete view of the <code>Objective</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeObjectiveView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Objective</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryObjectiveView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include objectives in objective banks which are children
     *  of this objective bank in the objective bank hierarchy.
     */

    @OSID @Override
    public void useFederatedObjectiveBankView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this objective bank only.
     */

    @OSID @Override
    public void useIsolatedObjectiveBankView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>Objective</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Objective</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Objective</code> and
     *  retained for compatibility.
     *
     *  @param  objectiveId <code>Id</code> of the
     *          <code>Objective</code>
     *  @return the objective
     *  @throws org.osid.NotFoundException <code>objectiveId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>objectiveId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.Objective getObjective(org.osid.id.Id objectiveId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.learning.ObjectiveList objectives = getObjectives()) {
            while (objectives.hasNext()) {
                org.osid.learning.Objective objective = objectives.getNextObjective();
                if (objective.getId().equals(objectiveId)) {
                    return (objective);
                }
            }
        } 

        throw new org.osid.NotFoundException(objectiveId + " not found");
    }


    /**
     *  Gets an <code>ObjectiveList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  objectives specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Objectives</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getObjectives()</code>.
     *
     *  @param  objectiveIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Objective</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveList getObjectivesByIds(org.osid.id.IdList objectiveIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.learning.Objective> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = objectiveIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getObjective(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("objective " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.learning.objective.LinkedObjectiveList(ret));
    }


    /**
     *  Gets an <code>ObjectiveList</code> corresponding to the given
     *  objective genus <code>Type</code> which does not include
     *  objectives of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  objectives or an error results. Otherwise, the returned list
     *  may contain only those objectives that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getObjectives()</code>.
     *
     *  @param  objectiveGenusType an objective genus type 
     *  @return the returned <code>Objective</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveList getObjectivesByGenusType(org.osid.type.Type objectiveGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.learning.objective.ObjectiveGenusFilterList(getObjectives(), objectiveGenusType));
    }


    /**
     *  Gets an <code>ObjectiveList</code> corresponding to the given
     *  objective genus <code>Type</code> and include any additional
     *  objectives with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  objectives or an error results. Otherwise, the returned list
     *  may contain only those objectives that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getObjectives()</code>.
     *
     *  @param  objectiveGenusType an objective genus type 
     *  @return the returned <code>Objective</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveList getObjectivesByParentGenusType(org.osid.type.Type objectiveGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getObjectivesByGenusType(objectiveGenusType));
    }


    /**
     *  Gets an <code>ObjectiveList</code> containing the given
     *  objective record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  objectives or an error results. Otherwise, the returned list
     *  may contain only those objectives that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getObjectives()</code>.
     *
     *  @param  objectiveRecordType an objective record type 
     *  @return the returned <code>Objective</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveList getObjectivesByRecordType(org.osid.type.Type objectiveRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.learning.objective.ObjectiveRecordFilterList(getObjectives(), objectiveRecordType));
    }


    /**
     *  Gets all <code>Objectives</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  objectives or an error results. Otherwise, the returned list
     *  may contain only those objectives that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Objectives</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.learning.ObjectiveList getObjectives()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the objective list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of objectives
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.learning.ObjectiveList filterObjectivesOnViews(org.osid.learning.ObjectiveList list)
        throws org.osid.OperationFailedException {

        return (list);
    }
}
