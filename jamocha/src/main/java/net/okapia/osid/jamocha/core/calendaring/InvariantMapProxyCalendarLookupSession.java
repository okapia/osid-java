//
// InvariantMapProxyCalendarLookupSession
//
//    Implements a Calendar lookup service backed by a fixed
//    collection of calendars. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring;


/**
 *  Implements a Calendar lookup service backed by a fixed
 *  collection of calendars. The calendars are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyCalendarLookupSession
    extends net.okapia.osid.jamocha.core.calendaring.spi.AbstractMapCalendarLookupSession
    implements org.osid.calendaring.CalendarLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyCalendarLookupSession} with no
     *  calendars.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public InvariantMapProxyCalendarLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyCalendarLookupSession} with a
     *  single calendar.
     *
     *  @param calendar a single calendar
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyCalendarLookupSession(org.osid.calendaring.Calendar calendar, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putCalendar(calendar);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyCalendarLookupSession} using
     *  an array of calendars.
     *
     *  @param calendars an array of calendars
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendars} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyCalendarLookupSession(org.osid.calendaring.Calendar[] calendars, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putCalendars(calendars);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyCalendarLookupSession} using a
     *  collection of calendars.
     *
     *  @param calendars a collection of calendars
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendars} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyCalendarLookupSession(java.util.Collection<? extends org.osid.calendaring.Calendar> calendars,
                                                  org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putCalendars(calendars);
        return;
    }
}
