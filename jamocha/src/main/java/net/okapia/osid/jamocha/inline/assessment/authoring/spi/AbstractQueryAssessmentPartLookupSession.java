//
// AbstractQueryAssessmentPartLookupSession.java
//
//    An inline adapter that maps an AssessmentPartLookupSession to
//    an AssessmentPartQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.assessment.authoring.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an AssessmentPartLookupSession to
 *  an AssessmentPartQuerySession.
 */

public abstract class AbstractQueryAssessmentPartLookupSession
    extends net.okapia.osid.jamocha.assessment.authoring.spi.AbstractAssessmentPartLookupSession
    implements org.osid.assessment.authoring.AssessmentPartLookupSession {

    private boolean activeonly    = false;
    private boolean sequestered   = false;
    private final org.osid.assessment.authoring.AssessmentPartQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryAssessmentPartLookupSession.
     *
     *  @param querySession the underlying assessment part query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryAssessmentPartLookupSession(org.osid.assessment.authoring.AssessmentPartQuerySession querySession) {
        nullarg(querySession, "assessment part query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Bank</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Bank Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBankId() {
        return (this.session.getBankId());
    }


    /**
     *  Gets the <code>Bank</code> associated with this 
     *  session.
     *
     *  @return the <code>Bank</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.Bank getBank()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getBank());
    }


    /**
     *  Tests if this user can perform <code>AssessmentPart</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAssessmentParts() {
        return (this.session.canSearchAssessmentParts());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include assessment parts in banks which are children
     *  of this bank in the bank hierarchy.
     */

    @OSID @Override
    public void useFederatedBankView() {
        this.session.useFederatedBankView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this bank only.
     */

    @OSID @Override
    public void useIsolatedBankView() {
        this.session.useIsolatedBankView();
        return;
    }
    

    /**
     *  Only active assessment parts are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveAssessmentPartView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive assessment parts are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusAssessmentPartView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    

    /**
     *  The returns from the lookup methods omit sequestered
     *  assessment parts.
     */

    @OSID @Override
    public void useSequesteredAssessmentPartView() {
        this.sequestered = true;
        return;
    }


    /**
     *  All assessment parts are returned including sequestered assessment parts.
     */

    @OSID @Override
    public void useUnsequesteredAssessmentPartView() {
        this.sequestered = false;
        return;
    }


    /**
     *  Tests if a sequestered or unsequestered view is set.
     *
     *  @return <code>true</code> if sequestered</code>,
     *          <code>false</code> if unsequestered
     */

    protected boolean isSequestered() {
        return (this.sequestered);
    }

     
    /**
     *  Gets the <code>AssessmentPart</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>AssessmentPart</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>AssessmentPart</code> and
     *  retained for compatibility.
     *
     *  In active mode, assessment parts are returned that are currently
     *  active. In any status mode, active and inactive assessment parts
     *  are returned.
     *
     *  @param  assessmentPartId <code>Id</code> of the
     *          <code>AssessmentPart</code>
     *  @return the assessment part
     *  @throws org.osid.NotFoundException <code>assessmentPartId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>assessmentPartId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPart getAssessmentPart(org.osid.id.Id assessmentPartId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.assessment.authoring.AssessmentPartQuery query = getQuery();
        query.matchId(assessmentPartId, true);
        org.osid.assessment.authoring.AssessmentPartList assessmentParts = this.session.getAssessmentPartsByQuery(query);
        if (assessmentParts.hasNext()) {
            return (assessmentParts.getNextAssessmentPart());
        } 
        
        throw new org.osid.NotFoundException(assessmentPartId + " not found");
    }


    /**
     *  Gets an <code>AssessmentPartList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  assessmentParts specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>AssessmentParts</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, assessment parts are returned that are currently
     *  active. In any status mode, active and inactive assessment parts
     *  are returned.
     *
     *  @param  assessmentPartIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>AssessmentPart</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentPartIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartList getAssessmentPartsByIds(org.osid.id.IdList assessmentPartIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.assessment.authoring.AssessmentPartQuery query = getQuery();

        try (org.osid.id.IdList ids = assessmentPartIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getAssessmentPartsByQuery(query));
    }


    /**
     *  Gets an <code>AssessmentPartList</code> corresponding to the given
     *  assessment part genus <code>Type</code> which does not include
     *  assessment parts of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  assessment parts or an error results. Otherwise, the returned list
     *  may contain only those assessment parts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, assessment parts are returned that are currently
     *  active. In any status mode, active and inactive assessment parts
     *  are returned.
     *
     *  @param  assessmentPartGenusType an assessmentPart genus type 
     *  @return the returned <code>AssessmentPart</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentPartGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartList getAssessmentPartsByGenusType(org.osid.type.Type assessmentPartGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.assessment.authoring.AssessmentPartQuery query = getQuery();
        query.matchGenusType(assessmentPartGenusType, true);
        return (this.session.getAssessmentPartsByQuery(query));
    }


    /**
     *  Gets an <code>AssessmentPartList</code> corresponding to the given
     *  assessment part genus <code>Type</code> and include any additional
     *  assessment parts with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  assessment parts or an error results. Otherwise, the returned list
     *  may contain only those assessment parts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, assessment parts are returned that are currently
     *  active. In any status mode, active and inactive assessment parts
     *  are returned.
     *
     *  @param  assessmentPartGenusType an assessmentPart genus type 
     *  @return the returned <code>AssessmentPart</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentPartGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartList getAssessmentPartsByParentGenusType(org.osid.type.Type assessmentPartGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.assessment.authoring.AssessmentPartQuery query = getQuery();
        query.matchParentGenusType(assessmentPartGenusType, true);
        return (this.session.getAssessmentPartsByQuery(query));
    }


    /**
     *  Gets an <code>AssessmentPartList</code> containing the given
     *  assessment part record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  assessment parts or an error results. Otherwise, the returned list
     *  may contain only those assessment parts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, assessment parts are returned that are currently
     *  active. In any status mode, active and inactive assessment parts
     *  are returned.
     *
     *  @param  assessmentPartRecordType an assessmentPart record type 
     *  @return the returned <code>AssessmentPart</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentPartRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartList getAssessmentPartsByRecordType(org.osid.type.Type assessmentPartRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.assessment.authoring.AssessmentPartQuery query = getQuery();
        query.matchRecordType(assessmentPartRecordType, true);
        return (this.session.getAssessmentPartsByQuery(query));
    }


    /**
     *  Gets an <code> AssessmentPart </code> for the given assessment. 
     *  
     *  In plenary mode, the returned list contains all known assessment parts 
     *  or an error results. Otherwise, the returned list may contain only 
     *  those assessment parts that are accessible through this session. 
     *  
     *  In active mode, assessment parts are returned that are currently 
     *  active. In any effective mode, active and inactive assessment parts 
     *  are returned. 
     *  
     *  In sequestered mode, no sequestered assessment parts are returned. In 
     *  unsequestered mode, all assessment parts are returned. 
     *
     *  @param  assessmentId an assessment <code> Id </code> 
     *  @return the returned <code> AssessmentPart </code> list 
     *  @throws org.osid.NullArgumentException <code> assessmentId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartList getAssessmentPartsForAssessment(org.osid.id.Id assessmentId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.assessment.authoring.AssessmentPartQuery query = getQuery();
        query.matchAssessmentId(assessmentId, true);
        return (this.session.getAssessmentPartsByQuery(query));
    }


    /**
     *  Gets all <code>AssessmentParts</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  assessment parts or an error results. Otherwise, the returned list
     *  may contain only those assessment parts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, assessment parts are returned that are currently
     *  active. In any status mode, active and inactive assessment parts
     *  are returned.
     *
     *  @return a list of <code>AssessmentParts</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartList getAssessmentParts()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.assessment.authoring.AssessmentPartQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getAssessmentPartsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.assessment.authoring.AssessmentPartQuery getQuery() {
        org.osid.assessment.authoring.AssessmentPartQuery query = this.session.getAssessmentPartQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        if (isSequestered()) {
            query.matchSequestered(true);
        }

        return (query);
    }
}
