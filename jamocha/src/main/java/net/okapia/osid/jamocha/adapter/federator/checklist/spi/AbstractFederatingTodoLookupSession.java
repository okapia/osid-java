//
// AbstractFederatingTodoLookupSession.java
//
//     An abstract federating adapter for a TodoLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.checklist.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  TodoLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.</p>
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingTodoLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.checklist.TodoLookupSession>
    implements org.osid.checklist.TodoLookupSession {

    private boolean parallel = false;
    private org.osid.checklist.Checklist checklist = new net.okapia.osid.jamocha.nil.checklist.checklist.UnknownChecklist();


    /**
     *  Constructs a new <code>AbstractFederatingTodoLookupSession</code>.
     */

    protected AbstractFederatingTodoLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.checklist.TodoLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Checklist/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Checklist Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getChecklistId() {
        return (this.checklist.getId());
    }


    /**
     *  Gets the <code>Checklist</code> associated with this 
     *  session.
     *
     *  @return the <code>Checklist</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.Checklist getChecklist()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.checklist);
    }


    /**
     *  Sets the <code>Checklist</code>.
     *
     *  @param  checklist the checklist for this session
     *  @throws org.osid.NullArgumentException <code>checklist</code>
     *          is <code>null</code>
     */

    protected void setChecklist(org.osid.checklist.Checklist checklist) {
        nullarg(checklist, "checklist");
        this.checklist = checklist;
        return;
    }


    /**
     *  Tests if this user can perform <code>Todo</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupTodos() {
        for (org.osid.checklist.TodoLookupSession session : getSessions()) {
            if (session.canLookupTodos()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Todo</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeTodoView() {
        for (org.osid.checklist.TodoLookupSession session : getSessions()) {
            session.useComparativeTodoView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Todo</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryTodoView() {
        for (org.osid.checklist.TodoLookupSession session : getSessions()) {
            session.usePlenaryTodoView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include todos in checklists which are children
     *  of this checklist in the checklist hierarchy.
     */

    @OSID @Override
    public void useFederatedChecklistView() {
        for (org.osid.checklist.TodoLookupSession session : getSessions()) {
            session.useFederatedChecklistView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this checklist only.
     */

    @OSID @Override
    public void useIsolatedChecklistView() {
        for (org.osid.checklist.TodoLookupSession session : getSessions()) {
            session.useIsolatedChecklistView();
        }

        return;
    }


    /**
     *  Only todos whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveTodoView() {
        for (org.osid.checklist.TodoLookupSession session : getSessions()) {
            session.useEffectiveTodoView();
        }

        return;
    }


    /**
     *  All todos of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveTodoView() {
        for (org.osid.checklist.TodoLookupSession session : getSessions()) {
            session.useAnyEffectiveTodoView();
        }

        return;
    }


    /**
     *  The returns from the lookup methods omit sequestered
     *  todos.
     */

    @OSID @Override
    public void useSequesteredTodoView() {
        for (org.osid.checklist.TodoLookupSession session : getSessions()) {
            session.useSequesteredTodoView();
        }

        return;
    }


    /**
     *  All todos are returned including sequestered todos.
     */

    @OSID @Override
    public void useUnsequesteredTodoView() {
        for (org.osid.checklist.TodoLookupSession session : getSessions()) {
            session.useUnsequesteredTodoView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Todo</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Todo</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Todo</code> and
     *  retained for compatibility.
     *
     *  In effective mode, todos are returned that are currently
     *  effective.  In any effective mode, effective todos and
     *  those currently expired are returned.
     *
     *  @param  todoId <code>Id</code> of the
     *          <code>Todo</code>
     *  @return the todo
     *  @throws org.osid.NotFoundException <code>todoId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>todoId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.Todo getTodo(org.osid.id.Id todoId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.checklist.TodoLookupSession session : getSessions()) {
            try {
                return (session.getTodo(todoId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(todoId + " not found");
    }


    /**
     *  Gets a <code>TodoList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  todos specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Todos</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, todos are returned that are currently effective.
     *  In any effective mode, effective todos and those currently expired
     *  are returned.
     *
     *  @param  todoIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Todo</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>todoIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.TodoList getTodosByIds(org.osid.id.IdList todoIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.checklist.todo.MutableTodoList ret = new net.okapia.osid.jamocha.checklist.todo.MutableTodoList();

        try (org.osid.id.IdList ids = todoIds) {
            while (ids.hasNext()) {
                ret.addTodo(getTodo(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>TodoList</code> corresponding to the given
     *  todo genus <code>Type</code> which does not include
     *  todos of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  todos or an error results. Otherwise, the returned list
     *  may contain only those todos that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, todos are returned that are currently effective.
     *  In any effective mode, effective todos and those currently expired
     *  are returned.
     *
     *  @param  todoGenusType a todo genus type 
     *  @return the returned <code>Todo</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>todoGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.TodoList getTodosByGenusType(org.osid.type.Type todoGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.checklist.todo.FederatingTodoList ret = getTodoList();

        for (org.osid.checklist.TodoLookupSession session : getSessions()) {
            ret.addTodoList(session.getTodosByGenusType(todoGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>TodoList</code> corresponding to the given
     *  todo genus <code>Type</code> and include any additional
     *  todos with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  todos or an error results. Otherwise, the returned list
     *  may contain only those todos that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, todos are returned that are currently
     *  effective.  In any effective mode, effective todos and
     *  those currently expired are returned.
     *
     *  @param  todoGenusType a todo genus type 
     *  @return the returned <code>Todo</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>todoGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.TodoList getTodosByParentGenusType(org.osid.type.Type todoGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.checklist.todo.FederatingTodoList ret = getTodoList();

        for (org.osid.checklist.TodoLookupSession session : getSessions()) {
            ret.addTodoList(session.getTodosByParentGenusType(todoGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>TodoList</code> containing the given
     *  todo record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  todos or an error results. Otherwise, the returned list
     *  may contain only those todos that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, todos are returned that are currently
     *  effective.  In any effective mode, effective todos and
     *  those currently expired are returned.
     *
     *  @param  todoRecordType a todo record type 
     *  @return the returned <code>Todo</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>todoRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.TodoList getTodosByRecordType(org.osid.type.Type todoRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.checklist.todo.FederatingTodoList ret = getTodoList();

        for (org.osid.checklist.TodoLookupSession session : getSessions()) {
            ret.addTodoList(session.getTodosByRecordType(todoRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>TodoList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  todos or an error results. Otherwise, the returned list
     *  may contain only those todos that are accessible
     *  through this session.
     *  
     *  In active mode, todos are returned that are currently
     *  active. In any status mode, active and inactive todos
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Todo</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.checklist.TodoList getTodosOnDate(org.osid.calendaring.DateTime from, 
                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.checklist.todo.FederatingTodoList ret = getTodoList();

        for (org.osid.checklist.TodoLookupSession session : getSessions()) {
            ret.addTodoList(session.getTodosOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a <code>TodoList</code> at the given priority
     *  <code>Type</code> or higher.
     *
     *  In plenary mode, the returned list contains all known todos or
     *  an error results. Otherwise, the returned list may contain
     *  only those todos that are accessible through this session.
     *
     *  In effective mode, todos are returned that are currently
     *  effective. In any effective mode, effective todos and those
     *  currently expired are returned.
     *
     *  This method doesn't understand ordering of priority types.
     *
     *  @param  priorityType a priority type
     *  @return the returned <code>Todo</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>priorityType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.checklist.TodoList getTodosByPriority(org.osid.type.Type priorityType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.checklist.todo.FederatingTodoList ret = getTodoList();

        for (org.osid.checklist.TodoLookupSession session : getSessions()) {
            ret.addTodoList(session.getTodosByPriority(priorityType));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>TodoList</code> with a due date within the given
     *  date range inclusive.
     *
     *  In plenary mode, the returned list contains all known todos or
     *  an error results. Otherwise, the returned list may contain
     *  only those todos that are accessible through this session.
     *
     *  In effective mode, todos are returned that are currently
     *  effective. In any effective mode, effective todos and those
     *  currently expired are returned.
     *
     *  In sequestered mode, no sequestered todos are returned. In
     *  unsequestered mode, all todos are returned.
     *
     *  @param  from starting date
     *  @param  to ending date
     *  @return the returned <code>Todo</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.checklist.TodoList getTodosByDueDate(org.osid.calendaring.DateTime from,
                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.checklist.todo.FederatingTodoList ret = getTodoList();
        
        for (org.osid.checklist.TodoLookupSession session : getSessions()) {
            ret.addTodoList(session.getTodosByDueDate(from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>TodoList</code> of todos dependent upon the given
     *  todo.
     *
     *  In plenary mode, the returned list contains all known todos or
     *  an error results. Otherwise, the returned list may contain
     *  only those todos that are accessible through this session.
     *
     *  In effective mode, todos are returned that are currently
     *  effective. In any effective mode, effective todos and those
     *  currently expired are returned.
     *
     *  In sequestered mode, no sequestered todos are returned. In
     *  unsequestered mode, all todos are returned.
     *
     *  @param  dependencyTodoId a todo
     *  @return the returned <code>Todo</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>dependencyTodoId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.checklist.TodoList getTodosByDependency(org.osid.id.Id dependencyTodoId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.checklist.todo.FederatingTodoList ret = getTodoList();
        
        for (org.osid.checklist.TodoLookupSession session : getSessions()) {
            ret.addTodoList(session.getTodosByDependency(dependencyTodoId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Todos</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  todos or an error results. Otherwise, the returned list
     *  may contain only those todos that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, todos are returned that are currently
     *  effective.  In any effective mode, effective todos and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Todos</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.TodoList getTodos()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.checklist.todo.FederatingTodoList ret = getTodoList();

        for (org.osid.checklist.TodoLookupSession session : getSessions()) {
            ret.addTodoList(session.getTodos());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.checklist.todo.FederatingTodoList getTodoList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.checklist.todo.ParallelTodoList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.checklist.todo.CompositeTodoList());
        }
    }
}
