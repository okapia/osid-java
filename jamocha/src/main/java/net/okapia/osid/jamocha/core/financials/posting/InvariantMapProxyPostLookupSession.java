//
// InvariantMapProxyPostLookupSession
//
//    Implements a Post lookup service backed by a fixed
//    collection of posts. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.financials.posting;


/**
 *  Implements a Post lookup service backed by a fixed
 *  collection of posts. The posts are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyPostLookupSession
    extends net.okapia.osid.jamocha.core.financials.posting.spi.AbstractMapPostLookupSession
    implements org.osid.financials.posting.PostLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyPostLookupSession} with no
     *  posts.
     *
     *  @param business the business
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyPostLookupSession(org.osid.financials.Business business,
                                                  org.osid.proxy.Proxy proxy) {
        setBusiness(business);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyPostLookupSession} with a single
     *  post.
     *
     *  @param business the business
     *  @param post a single post
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code business},
     *          {@code post} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyPostLookupSession(org.osid.financials.Business business,
                                                  org.osid.financials.posting.Post post, org.osid.proxy.Proxy proxy) {

        this(business, proxy);
        putPost(post);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyPostLookupSession} using
     *  an array of posts.
     *
     *  @param business the business
     *  @param posts an array of posts
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code business},
     *          {@code posts} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyPostLookupSession(org.osid.financials.Business business,
                                                  org.osid.financials.posting.Post[] posts, org.osid.proxy.Proxy proxy) {

        this(business, proxy);
        putPosts(posts);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyPostLookupSession} using a
     *  collection of posts.
     *
     *  @param business the business
     *  @param posts a collection of posts
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code business},
     *          {@code posts} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyPostLookupSession(org.osid.financials.Business business,
                                                  java.util.Collection<? extends org.osid.financials.posting.Post> posts,
                                                  org.osid.proxy.Proxy proxy) {

        this(business, proxy);
        putPosts(posts);
        return;
    }
}
