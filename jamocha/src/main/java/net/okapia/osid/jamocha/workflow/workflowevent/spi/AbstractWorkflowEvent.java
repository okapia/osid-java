//
// AbstractWorkflowEvent.java
//
//     Defines a WorkflowEvent.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.workflowevent.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>WorkflowEvent</code>.
 */

public abstract class AbstractWorkflowEvent
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.workflow.WorkflowEvent {

    private org.osid.calendaring.DateTime timestamp;
    private org.osid.workflow.Process process;
    private org.osid.resource.Resource worker;
    private org.osid.authentication.Agent workingAgent;
    private org.osid.workflow.Work work;
    private org.osid.workflow.Step step;
    private boolean cancel = false;

    private final java.util.Collection<org.osid.workflow.records.WorkflowEventRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the timestamp of this event. 
     *
     *  @return the timestamp 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getTimestamp() {
        return (this.timestamp);
    }


    /**
     *  Sets the timestamp.
     *
     *  @param timestamp a timestamp
     *  @throws org.osid.NullArgumentException
     *          <code>timestamp</code> is <code>null</code>
     */

    protected void setTimestamp(org.osid.calendaring.DateTime timestamp) {
        nullarg(timestamp, "timestamp");
        this.timestamp = timestamp;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the process. 
     *
     *  @return the process <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getProcessId() {
        return (this.process.getId());
    }


    /**
     *  Gets the process. 
     *
     *  @return the process 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.workflow.Process getProcess()
        throws org.osid.OperationFailedException {

        return (this.process);
    }


    /**
     *  Sets the process.
     *
     *  @param process a process
     *  @throws org.osid.NullArgumentException
     *          <code>process</code> is <code>null</code>
     */

    protected void setProcess(org.osid.workflow.Process process) {
        nullarg(process, "process");
        this.process = process;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the resource that caused this event. 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getWorkerId() {
        return (this.worker.getId());
    }


    /**
     *  Gets the resource that caused this event. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getWorker()
        throws org.osid.OperationFailedException {

        return (this.worker);
    }


    /**
     *  Sets the worker.
     *
     *  @param worker a worker
     *  @throws org.osid.NullArgumentException
     *          <code>worker</code> is <code>null</code>
     */

    protected void setWorker(org.osid.resource.Resource worker) {
        nullarg(worker, "worker");
        this.worker = worker;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the agent that caused this event. 
     *
     *  @return the agent <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getWorkingAgentId() {
        return (this.workingAgent.getId());
    }


    /**
     *  Gets the agent that caused this event. 
     *
     *  @return the agent 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getWorkingAgent()
        throws org.osid.OperationFailedException {

        return (this.workingAgent);
    }


    /**
     *  Sets the working agent.
     *
     *  @param agent a working agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    protected void setWorkingAgent(org.osid.authentication.Agent agent) {
        nullarg(agent, "working agent");
        this.workingAgent = agent;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the work. 
     *
     *  @return the work <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getWorkId() {
        return (this.work.getId());
    }


    /**
     *  Gets the work. 
     *
     *  @return the work 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.workflow.Work getWork()
        throws org.osid.OperationFailedException {

        return (this.work);
    }


    /**
     *  Sets the work.
     *
     *  @param work a work
     *  @throws org.osid.NullArgumentException
     *          <code>work</code> is <code>null</code>
     */

    protected void setWork(org.osid.workflow.Work work) {
        nullarg(work, "work");
        this.work = work;
        return;
    }


    /**
     *  Tests if this work event indicates the work has been canceled
     *  the workflow and is not associated with a step at the time of
     *  this event.
     *
     *  @return <code> true </code> if the work canceled, <code> false
     *          </code> if the work is associated with a step in this
     *          event
     */

    @OSID @Override
    public boolean didCancel() {
        return (this.cancel);
    }


    /**
     *  Sets the did cancel.
     *
     *  @param cancel <code> true </code> if the work canceled, <code>
     *         false </code> if the work is associated with a step in
     *         this event
     */

    protected void setCancel(boolean cancel) {
        this.cancel = cancel;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the step at which the work is in at the 
     *  time of this event. 
     *
     *  @return the step <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> didComplete() </code> or 
     *          <code> didCancel() </code> is <code> true </code> 
     */

    @OSID @Override
    public org.osid.id.Id getStepId() {
        return (this.step.getId());
    }


    /**
     *  Gets the step at which the work is in at the time of this event. 
     *
     *  @return the step 
     *  @throws org.osid.IllegalStateException <code> didComplete() </code> or 
     *          <code> didCancel() </code> is <code> true </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.workflow.Step getStep()
        throws org.osid.OperationFailedException {

        return (this.step);
    }


    /**
     *  Sets the step.
     *
     *  @param step a step
     *  @throws org.osid.NullArgumentException
     *          <code>step</code> is <code>null</code>
     */

    protected void setStep(org.osid.workflow.Step step) {
        nullarg(step, "step");
        this.step = step;
        return;
    }


    /**
     *  Tests if this workflowEvent supports the given record
     *  <code>Type</code>.
     *
     *  @param  workflowEventRecordType a workflow event record type 
     *  @return <code>true</code> if the workflowEventRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>workflowEventRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type workflowEventRecordType) {
        for (org.osid.workflow.records.WorkflowEventRecord record : this.records) {
            if (record.implementsRecordType(workflowEventRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>WorkflowEvent</code> record <code>Type</code>.
     *
     *  @param  workflowEventRecordType the workflow event record type 
     *  @return the workflow event record 
     *  @throws org.osid.NullArgumentException
     *          <code>workflowEventRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(workflowEventRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.records.WorkflowEventRecord getWorkflowEventRecord(org.osid.type.Type workflowEventRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.workflow.records.WorkflowEventRecord record : this.records) {
            if (record.implementsRecordType(workflowEventRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(workflowEventRecordType + " is not supported");
    }


    /**
     *  Adds a record to this workflow event. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param workflowEventRecord the workflow event record
     *  @param workflowEventRecordType workflow event record type
     *  @throws org.osid.NullArgumentException
     *          <code>workflowEventRecord</code> or
     *          <code>workflowEventRecordTypeworkflowEvent</code> is
     *          <code>null</code>
     */
            
    protected void addWorkflowEventRecord(org.osid.workflow.records.WorkflowEventRecord workflowEventRecord, 
                                          org.osid.type.Type workflowEventRecordType) {

        nullarg(workflowEventRecord, "workflow event record");
        addRecordType(workflowEventRecordType);
        this.records.add(workflowEventRecord);
        
        return;
    }
}
