//
// InvariantMapCanonicalUnitProcessorLookupSession
//
//    Implements a CanonicalUnitProcessor lookup service backed by a fixed collection of
//    canonicalUnitProcessors.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.offering.rules;


/**
 *  Implements a CanonicalUnitProcessor lookup service backed by a fixed
 *  collection of canonical unit processors. The canonical unit processors are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapCanonicalUnitProcessorLookupSession
    extends net.okapia.osid.jamocha.core.offering.rules.spi.AbstractMapCanonicalUnitProcessorLookupSession
    implements org.osid.offering.rules.CanonicalUnitProcessorLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapCanonicalUnitProcessorLookupSession</code> with no
     *  canonical unit processors.
     *  
     *  @param catalogue the catalogue
     *  @throws org.osid.NullArgumnetException {@code catalogue} is
     *          {@code null}
     */

    public InvariantMapCanonicalUnitProcessorLookupSession(org.osid.offering.Catalogue catalogue) {
        setCatalogue(catalogue);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapCanonicalUnitProcessorLookupSession</code> with a single
     *  canonical unit processor.
     *  
     *  @param catalogue the catalogue
     *  @param canonicalUnitProcessor a single canonical unit processor
     *  @throws org.osid.NullArgumentException {@code catalogue} or
     *          {@code canonicalUnitProcessor} is <code>null</code>
     */

      public InvariantMapCanonicalUnitProcessorLookupSession(org.osid.offering.Catalogue catalogue,
                                               org.osid.offering.rules.CanonicalUnitProcessor canonicalUnitProcessor) {
        this(catalogue);
        putCanonicalUnitProcessor(canonicalUnitProcessor);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapCanonicalUnitProcessorLookupSession</code> using an array
     *  of canonical unit processors.
     *  
     *  @param catalogue the catalogue
     *  @param canonicalUnitProcessors an array of canonical unit processors
     *  @throws org.osid.NullArgumentException {@code catalogue} or
     *          {@code canonicalUnitProcessors} is <code>null</code>
     */

      public InvariantMapCanonicalUnitProcessorLookupSession(org.osid.offering.Catalogue catalogue,
                                               org.osid.offering.rules.CanonicalUnitProcessor[] canonicalUnitProcessors) {
        this(catalogue);
        putCanonicalUnitProcessors(canonicalUnitProcessors);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapCanonicalUnitProcessorLookupSession</code> using a
     *  collection of canonical unit processors.
     *
     *  @param catalogue the catalogue
     *  @param canonicalUnitProcessors a collection of canonical unit processors
     *  @throws org.osid.NullArgumentException {@code catalogue} or
     *          {@code canonicalUnitProcessors} is <code>null</code>
     */

      public InvariantMapCanonicalUnitProcessorLookupSession(org.osid.offering.Catalogue catalogue,
                                               java.util.Collection<? extends org.osid.offering.rules.CanonicalUnitProcessor> canonicalUnitProcessors) {
        this(catalogue);
        putCanonicalUnitProcessors(canonicalUnitProcessors);
        return;
    }
}
