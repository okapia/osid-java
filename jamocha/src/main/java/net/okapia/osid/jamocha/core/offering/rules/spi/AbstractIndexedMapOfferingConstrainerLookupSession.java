//
// AbstractIndexedMapOfferingConstrainerLookupSession.java
//
//    A simple framework for providing an OfferingConstrainer lookup service
//    backed by a fixed collection of offering constrainers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.offering.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an OfferingConstrainer lookup service backed by a
 *  fixed collection of offering constrainers. The offering constrainers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some offering constrainers may be compatible
 *  with more types than are indicated through these offering constrainer
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>OfferingConstrainers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapOfferingConstrainerLookupSession
    extends AbstractMapOfferingConstrainerLookupSession
    implements org.osid.offering.rules.OfferingConstrainerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.offering.rules.OfferingConstrainer> offeringConstrainersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.offering.rules.OfferingConstrainer>());
    private final MultiMap<org.osid.type.Type, org.osid.offering.rules.OfferingConstrainer> offeringConstrainersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.offering.rules.OfferingConstrainer>());


    /**
     *  Makes an <code>OfferingConstrainer</code> available in this session.
     *
     *  @param  offeringConstrainer an offering constrainer
     *  @throws org.osid.NullArgumentException <code>offeringConstrainer<code> is
     *          <code>null</code>
     */

    @Override
    protected void putOfferingConstrainer(org.osid.offering.rules.OfferingConstrainer offeringConstrainer) {
        super.putOfferingConstrainer(offeringConstrainer);

        this.offeringConstrainersByGenus.put(offeringConstrainer.getGenusType(), offeringConstrainer);
        
        try (org.osid.type.TypeList types = offeringConstrainer.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.offeringConstrainersByRecord.put(types.getNextType(), offeringConstrainer);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }

    
    /**
     *  Makes an array of offering constrainers available in this session.
     *
     *  @param  offeringConstrainers an array of offering constrainers
     *  @throws org.osid.NullArgumentException <code>offeringConstrainers<code>
     *          is <code>null</code>
     */

    @Override
    protected void putOfferingConstrainers(org.osid.offering.rules.OfferingConstrainer[] offeringConstrainers) {
        for (org.osid.offering.rules.OfferingConstrainer offeringConstrainer : offeringConstrainers) {
            putOfferingConstrainer(offeringConstrainer);
        }

        return;
    }


    /**
     *  Makes a collection of offering constrainers available in this session.
     *
     *  @param  offeringConstrainers a collection of offering constrainers
     *  @throws org.osid.NullArgumentException <code>offeringConstrainers<code>
     *          is <code>null</code>
     */

    @Override
    protected void putOfferingConstrainers(java.util.Collection<? extends org.osid.offering.rules.OfferingConstrainer> offeringConstrainers) {
        for (org.osid.offering.rules.OfferingConstrainer offeringConstrainer : offeringConstrainers) {
            putOfferingConstrainer(offeringConstrainer);
        }

        return;
    }


    /**
     *  Removes an offering constrainer from this session.
     *
     *  @param offeringConstrainerId the <code>Id</code> of the offering constrainer
     *  @throws org.osid.NullArgumentException <code>offeringConstrainerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeOfferingConstrainer(org.osid.id.Id offeringConstrainerId) {
        org.osid.offering.rules.OfferingConstrainer offeringConstrainer;
        try {
            offeringConstrainer = getOfferingConstrainer(offeringConstrainerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.offeringConstrainersByGenus.remove(offeringConstrainer.getGenusType());

        try (org.osid.type.TypeList types = offeringConstrainer.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.offeringConstrainersByRecord.remove(types.getNextType(), offeringConstrainer);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeOfferingConstrainer(offeringConstrainerId);
        return;
    }


    /**
     *  Gets an <code>OfferingConstrainerList</code> corresponding to the given
     *  offering constrainer genus <code>Type</code> which does not include
     *  offering constrainers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known offering constrainers or an error results. Otherwise,
     *  the returned list may contain only those offering constrainers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  offeringConstrainerGenusType an offering constrainer genus type 
     *  @return the returned <code>OfferingConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>offeringConstrainerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerList getOfferingConstrainersByGenusType(org.osid.type.Type offeringConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.offering.rules.offeringconstrainer.ArrayOfferingConstrainerList(this.offeringConstrainersByGenus.get(offeringConstrainerGenusType)));
    }


    /**
     *  Gets an <code>OfferingConstrainerList</code> containing the given
     *  offering constrainer record <code>Type</code>. In plenary mode, the
     *  returned list contains all known offering constrainers or an error
     *  results. Otherwise, the returned list may contain only those
     *  offering constrainers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  offeringConstrainerRecordType an offering constrainer record type 
     *  @return the returned <code>offeringConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>offeringConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerList getOfferingConstrainersByRecordType(org.osid.type.Type offeringConstrainerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.offering.rules.offeringconstrainer.ArrayOfferingConstrainerList(this.offeringConstrainersByRecord.get(offeringConstrainerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.offeringConstrainersByGenus.clear();
        this.offeringConstrainersByRecord.clear();

        super.close();

        return;
    }
}
