//
// AbstractAssemblyModelQuery.java
//
//     A ModelQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.inventory.model.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ModelQuery that stores terms.
 */

public abstract class AbstractAssemblyModelQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.inventory.ModelQuery,
               org.osid.inventory.ModelQueryInspector,
               org.osid.inventory.ModelSearchOrder {

    private final java.util.Collection<org.osid.inventory.records.ModelQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.inventory.records.ModelQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.inventory.records.ModelSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyModelQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyModelQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the inventory <code> Id </code> for this query to match models 
     *  that have a related manufacturer. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchManufacturerId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getManufacturerIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the manufacturer terms. 
     */

    @OSID @Override
    public void clearManufacturerIdTerms() {
        getAssembler().clearTerms(getManufacturerIdColumn());
        return;
    }


    /**
     *  Gets the manufacturer <code> Id </code> query terms. 
     *
     *  @return the resource <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getManufacturerIdTerms() {
        return (getAssembler().getIdTerms(getManufacturerIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the 
     *  manufacturer. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByManufacturer(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getManufacturerColumn(), style);
        return;
    }


    /**
     *  Gets the ManufacturerId column name.
     *
     * @return the column name
     */

    protected String getManufacturerIdColumn() {
        return ("manufacturer_id");
    }


    /**
     *  Tests if a <code> ManufacturerQuery </code> is available for the 
     *  location. 
     *
     *  @return <code> true </code> if a manufacturer query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsManufacturerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a manufacturer. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the manufacturer query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsManufacturerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getManufacturerQuery() {
        throw new org.osid.UnimplementedException("supportsManufacturerQuery() is false");
    }


    /**
     *  Matches any manufacturer. 
     *
     *  @param  match <code> true </code> to match models with any inventory, 
     *          <code> false </code> to match models with no inventories 
     */

    @OSID @Override
    public void matchAnyManufacturer(boolean match) {
        getAssembler().addIdWildcardTerm(getManufacturerColumn(), match);
        return;
    }


    /**
     *  Clears the manufacturer terms. 
     */

    @OSID @Override
    public void clearManufacturerTerms() {
        getAssembler().clearTerms(getManufacturerColumn());
        return;
    }


    /**
     *  Gets the manufacturer query terms. 
     *
     *  @return the resource query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getManufacturerTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a <code> ResourceSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsManufacturerSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a manufacturer resource. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsManufacturerSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getManufacturerSearchOrder() {
        throw new org.osid.UnimplementedException("supportsManufacturerSearchOrder() is false");
    }


    /**
     *  Gets the Manufacturer column name.
     *
     * @return the column name
     */

    protected String getManufacturerColumn() {
        return ("manufacturer");
    }


    /**
     *  Matches an archetype. 
     *
     *  @param  archetype an archetype for the model 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> archetype </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchArchetype(String archetype, 
                               org.osid.type.Type stringMatchType, 
                               boolean match) {
        getAssembler().addStringTerm(getArchetypeColumn(), archetype, stringMatchType, match);
        return;
    }


    /**
     *  Matches items that have any archetype. 
     *
     *  @param  match <code> true </code> to match items with any archetype, 
     *          <code> false </code> to match items with no archetype 
     */

    @OSID @Override
    public void matchAnyArchetype(boolean match) {
        getAssembler().addStringWildcardTerm(getArchetypeColumn(), match);
        return;
    }


    /**
     *  Clears the archetype terms. 
     */

    @OSID @Override
    public void clearArchetypeTerms() {
        getAssembler().clearTerms(getArchetypeColumn());
        return;
    }


    /**
     *  Gets the archetype query terms. 
     *
     *  @return the archetype query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getArchetypeTerms() {
        return (getAssembler().getStringTerms(getArchetypeColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the archetype. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByArchetype(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getArchetypeColumn(), style);
        return;
    }


    /**
     *  Gets the Archetype column name.
     *
     * @return the column name
     */

    protected String getArchetypeColumn() {
        return ("archetype");
    }


    /**
     *  Matches a model number. 
     *
     *  @param  number a model number 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> number </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchNumber(String number, org.osid.type.Type stringMatchType, 
                            boolean match) {
        getAssembler().addStringTerm(getNumberColumn(), number, stringMatchType, match);
        return;
    }


    /**
     *  Matches items that have any model number. 
     *
     *  @param  match <code> true </code> to match items with any model 
     *          number, <code> false </code> to match items with no model 
     *          number 
     */

    @OSID @Override
    public void matchAnyNumber(boolean match) {
        getAssembler().addStringWildcardTerm(getNumberColumn(), match);
        return;
    }


    /**
     *  Clears the model number terms. 
     */

    @OSID @Override
    public void clearNumberTerms() {
        getAssembler().clearTerms(getNumberColumn());
        return;
    }


    /**
     *  Gets the model number query terms. 
     *
     *  @return the model number query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getNumberTerms() {
        return (getAssembler().getStringTerms(getNumberColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the model 
     *  number. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByNumber(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getNumberColumn(), style);
        return;
    }


    /**
     *  Gets the Number column name.
     *
     * @return the column name
     */

    protected String getNumberColumn() {
        return ("number");
    }


    /**
     *  Sets the warehouse <code> Id </code> for this query to match models 
     *  assigned to warehouses. 
     *
     *  @param  warehouseId the warehouse <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchWarehouseId(org.osid.id.Id warehouseId, boolean match) {
        getAssembler().addIdTerm(getWarehouseIdColumn(), warehouseId, match);
        return;
    }


    /**
     *  Clears the warehouse <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearWarehouseIdTerms() {
        getAssembler().clearTerms(getWarehouseIdColumn());
        return;
    }


    /**
     *  Gets the warehouse <code> Id </code> query terms. 
     *
     *  @return the warehouse <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getWarehouseIdTerms() {
        return (getAssembler().getIdTerms(getWarehouseIdColumn()));
    }


    /**
     *  Gets the WarehouseId column name.
     *
     * @return the column name
     */

    protected String getWarehouseIdColumn() {
        return ("warehouse_id");
    }


    /**
     *  Tests if a <code> WarehouseQuery </code> is available. 
     *
     *  @return <code> true </code> if a warehouse query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWarehouseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a warehouse. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the warehouse query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWarehouseQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseQuery getWarehouseQuery() {
        throw new org.osid.UnimplementedException("supportsWarehouseQuery() is false");
    }


    /**
     *  Clears the warehouse terms. 
     */

    @OSID @Override
    public void clearWarehouseTerms() {
        getAssembler().clearTerms(getWarehouseColumn());
        return;
    }


    /**
     *  Gets the warehouse query terms. 
     *
     *  @return the warehouse query terms 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseQueryInspector[] getWarehouseTerms() {
        return (new org.osid.inventory.WarehouseQueryInspector[0]);
    }


    /**
     *  Gets the Warehouse column name.
     *
     * @return the column name
     */

    protected String getWarehouseColumn() {
        return ("warehouse");
    }


    /**
     *  Tests if this model supports the given record
     *  <code>Type</code>.
     *
     *  @param  modelRecordType a model record type 
     *  @return <code>true</code> if the modelRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>modelRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type modelRecordType) {
        for (org.osid.inventory.records.ModelQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(modelRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  modelRecordType the model record type 
     *  @return the model query record 
     *  @throws org.osid.NullArgumentException
     *          <code>modelRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(modelRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.records.ModelQueryRecord getModelQueryRecord(org.osid.type.Type modelRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inventory.records.ModelQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(modelRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(modelRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  modelRecordType the model record type 
     *  @return the model query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>modelRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(modelRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.records.ModelQueryInspectorRecord getModelQueryInspectorRecord(org.osid.type.Type modelRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inventory.records.ModelQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(modelRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(modelRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param modelRecordType the model record type
     *  @return the model search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>modelRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(modelRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.records.ModelSearchOrderRecord getModelSearchOrderRecord(org.osid.type.Type modelRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inventory.records.ModelSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(modelRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(modelRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this model. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param modelQueryRecord the model query record
     *  @param modelQueryInspectorRecord the model query inspector
     *         record
     *  @param modelSearchOrderRecord the model search order record
     *  @param modelRecordType model record type
     *  @throws org.osid.NullArgumentException
     *          <code>modelQueryRecord</code>,
     *          <code>modelQueryInspectorRecord</code>,
     *          <code>modelSearchOrderRecord</code> or
     *          <code>modelRecordTypemodel</code> is
     *          <code>null</code>
     */
            
    protected void addModelRecords(org.osid.inventory.records.ModelQueryRecord modelQueryRecord, 
                                      org.osid.inventory.records.ModelQueryInspectorRecord modelQueryInspectorRecord, 
                                      org.osid.inventory.records.ModelSearchOrderRecord modelSearchOrderRecord, 
                                      org.osid.type.Type modelRecordType) {

        addRecordType(modelRecordType);

        nullarg(modelQueryRecord, "model query record");
        nullarg(modelQueryInspectorRecord, "model query inspector record");
        nullarg(modelSearchOrderRecord, "model search odrer record");

        this.queryRecords.add(modelQueryRecord);
        this.queryInspectorRecords.add(modelQueryInspectorRecord);
        this.searchOrderRecords.add(modelSearchOrderRecord);
        
        return;
    }
}
