//
// AbstractPackageQuerySession.java
//
//     A template for making PackageQuerySessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.installation.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A basic template for query sessions.
 */

public abstract class AbstractPackageQuerySession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.installation.PackageQuerySession {

    private boolean federated = false;
    private org.osid.installation.Depot depot = new net.okapia.osid.jamocha.nil.installation.depot.UnknownDepot();


      /**
       *  Gets the <code>Depot/code> <code>Id</code> associated
       *  with this session.
       *
       *  @return the <code>Depot Id</code> associated with
       *          this session
       *  @throws org.osid.IllegalStateException this session has been
       *          closed
       */

    @OSID @Override
      public org.osid.id.Id getDepotId() {
        return (this.depot.getId());
    }


    /**
     *  Gets the <code>Depot</code> associated with this 
     *  session.
     *
     *  @return the <code>Depot</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.Depot getDepot()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.depot);
    }


    /**
     *  Sets the <code>Depot</code>.
     *
     *  @param  depot the depot for this session
     *  @throws org.osid.NullArgumentException <code>depot</code>
     *          is <code>null</code>
     */

    protected void setDepot(org.osid.installation.Depot depot) {
        nullarg(depot, "depot");
        this.depot = depot;
        return;
    }

    /**
     *  Federates the view for methods in this session. A federated
     *  view will include packages in depots which are children
     *  of this depot in the depot hierarchy.
     */

    @OSID @Override
    public void useFederatedDepotView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this depot only.
     */

    @OSID @Override
    public void useIsolatedDepotView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Tests if this user can perform <code> Package </code>
     *  searches. A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a <code>
     *  PERMISSION_DENIED. </code> This is intended as a hint to an
     *  application that may opt not to offer search operations to
     *  unauthorized users.
     *
     *  @return <code> false </code> if search methods are not authorized, 
     *          <code> true </code> otherwise 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canSearchPackages() {
        return (true);
    }
}
