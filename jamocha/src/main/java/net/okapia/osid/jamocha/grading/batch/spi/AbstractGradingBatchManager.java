//
// AbstractGradingBatchManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractGradingBatchManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.grading.batch.GradingBatchManager,
               org.osid.grading.batch.GradingBatchProxyManager {


    /**
     *  Constructs a new <code>AbstractGradingBatchManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractGradingBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if bulk administration of grade systems is available. 
     *
     *  @return <code> true </code> if a grade system bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSystemBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of grade entries is available. 
     *
     *  @return <code> true </code> if a grade entry bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeEntryBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of gradebook columns is available. 
     *
     *  @return <code> true </code> if a gradebook columnbulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookColumnBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of gradebooks is available. 
     *
     *  @return <code> true </code> if a gradebook bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookBatchAdmin() {
        return (false);
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk grade 
     *  system administration service. 
     *
     *  @return a <code> GradeSystemBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.batch.GradeSystemBatchAdminSession getGradeSystemBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.batch.GradingBatchManager.getGradeSystemBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk grade 
     *  system administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GradeSystemBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.batch.GradeSystemBatchAdminSession getGradeSystemBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.batch.GradingBatchProxyManager.getGradeSystemBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk grade 
     *  system administration service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the <code> Gradebook 
     *          </code> 
     *  @return a <code> GradeSystemBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Gradebook </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.batch.GradeSystemBatchAdminSession getGradeSystemBatchAdminSessionForGradebook(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.batch.GradingBatchManager.getGradeSystemBatchAdminSessionForGradebook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk grade 
     *  system administration service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the <code> Gradebook 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> GradeSystemBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Gradebook </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.batch.GradeSystemBatchAdminSession getGradeSystemBatchAdminSessionForGradebook(org.osid.id.Id gradebookId, 
                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.batch.GradingBatchProxyManager.getGradeSystemBatchAdminSessionForGradebook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk grade 
     *  entry administration service. 
     *
     *  @return a <code> GradeEntryBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeEntryBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.batch.GradeEntryBatchAdminSession getGradeEntryBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.batch.GradingBatchManager.getGradeEntryBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk grade 
     *  entry administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GradeEntryBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeEntryBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.batch.GradeEntryBatchAdminSession getGradeEntryBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.batch.GradingBatchProxyManager.getGradeEntryBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk grade 
     *  entry administration service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the <code> Gradebook 
     *          </code> 
     *  @return a <code> GradeEntryBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Gradebook </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeEntryBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.batch.GradeEntryBatchAdminSession getGradeEntryBatchAdminSessionForGradebook(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.batch.GradingBatchManager.getGradeEntryBatchAdminSessionForGradebook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk grade 
     *  entry administration service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the <code> Gradebook 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> GradeEntryBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Gradebook </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeEntryBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.batch.GradeEntryBatchAdminSession getGradeEntryBatchAdminSessionForGradebook(org.osid.id.Id gradebookId, 
                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.batch.GradingBatchProxyManager.getGradeEntryBatchAdminSessionForGradebook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk gradebook 
     *  columnadministration service. 
     *
     *  @return a <code> GradebookColumnBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.batch.GradebookColumnBatchAdminSession getGradebookColumnBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.batch.GradingBatchManager.getGradebookColumnBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk gradebook 
     *  columnadministration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GradebookColumnBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.batch.GradebookColumnBatchAdminSession getGradebookColumnBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.batch.GradingBatchProxyManager.getGradebookColumnBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk gradebook 
     *  columnadministration service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the <code> Gradebook 
     *          </code> 
     *  @return a <code> GradebookColumnBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Gradebook </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.batch.GradebookColumnBatchAdminSession getGradebookColumnBatchAdminSessionForGradebook(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.batch.GradingBatchManager.getGradebookColumnBatchAdminSessionForGradebook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk gradebook 
     *  columnadministration service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the <code> Gradebook 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> GradebookColumnBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Gradebook </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.batch.GradebookColumnBatchAdminSession getGradebookColumnBatchAdminSessionForGradebook(org.osid.id.Id gradebookId, 
                                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.batch.GradingBatchProxyManager.getGradebookColumnBatchAdminSessionForGradebook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk gradebook 
     *  administration service. 
     *
     *  @return a <code> GradebookBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.batch.GradebookBatchAdminSession getGradebookBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.batch.GradingBatchManager.getGradebookBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk gradebook 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GradebookBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.batch.GradebookBatchAdminSession getGradebookBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.grading.batch.GradingBatchProxyManager.getGradebookBatchAdminSession not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        return;
    }
}
