//
// AbstractAudit.java
//
//     Defines an Audit.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inquiry.audit.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Audit</code>.
 */

public abstract class AbstractAudit
    extends net.okapia.osid.jamocha.spi.AbstractOsidGovernator
    implements org.osid.inquiry.Audit {

    private final java.util.Collection<org.osid.inquiry.records.AuditRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Tests if this audit supports the given record
     *  <code>Type</code>.
     *
     *  @param  auditRecordType an audit record type 
     *  @return <code>true</code> if the auditRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>auditRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type auditRecordType) {
        for (org.osid.inquiry.records.AuditRecord record : this.records) {
            if (record.implementsRecordType(auditRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  auditRecordType the audit record type 
     *  @return the audit record 
     *  @throws org.osid.NullArgumentException
     *          <code>auditRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(auditRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inquiry.records.AuditRecord getAuditRecord(org.osid.type.Type auditRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inquiry.records.AuditRecord record : this.records) {
            if (record.implementsRecordType(auditRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(auditRecordType + " is not supported");
    }


    /**
     *  Adds a record to this audit. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param auditRecord the audit record
     *  @param auditRecordType audit record type
     *  @throws org.osid.NullArgumentException
     *          <code>auditRecord</code> or
     *          <code>auditRecordTypeaudit</code> is
     *          <code>null</code>
     */
            
    protected void addAuditRecord(org.osid.inquiry.records.AuditRecord auditRecord, 
                                     org.osid.type.Type auditRecordType) {

        addRecordType(auditRecordType);
        this.records.add(auditRecord);
        
        return;
    }
}
