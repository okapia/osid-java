//
// AbstractImmutableSpeedZone.java
//
//     Wraps a mutable SpeedZone to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.mapping.path.speedzone.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>SpeedZone</code> to hide modifiers. This
 *  wrapper provides an immutized SpeedZone from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying speedZone whose state changes are visible.
 */

public abstract class AbstractImmutableSpeedZone
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRule
    implements org.osid.mapping.path.SpeedZone {

    private final org.osid.mapping.path.SpeedZone speedZone;


    /**
     *  Constructs a new <code>AbstractImmutableSpeedZone</code>.
     *
     *  @param speedZone the speed zone to immutablize
     *  @throws org.osid.NullArgumentException <code>speedZone</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableSpeedZone(org.osid.mapping.path.SpeedZone speedZone) {
        super(speedZone);
        this.speedZone = speedZone;
        return;
    }


    /**
     *  Gets the path <code> Id </code> of this speed zone. 
     *
     *  @return the <code> Id </code> of the path 
     */

    @OSID @Override
    public org.osid.id.Id getPathId() {
        return (this.speedZone.getPathId());
    }


    /**
     *  Gets the path of this speed zone. 
     *
     *  @return the path 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.mapping.path.Path getPath()
        throws org.osid.OperationFailedException {

        return (this.speedZone.getPath());
    }


    /**
     *  Gets the starting coordinate of the speed zone on the path. 
     *
     *  @return the start of the zone 
     */

    @OSID @Override
    public org.osid.mapping.Coordinate getStartingCoordinate() {
        return (this.speedZone.getStartingCoordinate());
    }


    /**
     *  Gets the ending coordinate of the speed zone on the path. 
     *
     *  @return the end of the zone 
     */

    @OSID @Override
    public org.osid.mapping.Coordinate getEndingCoordinate() {
        return (this.speedZone.getEndingCoordinate());
    }


    /**
     *  Tests if this speed zone is implicit. An implicit speed zone is 
     *  generated from other information such as an <code> Obstacle. </code> 
     *
     *  @return <code> true </code> if this speed zone is implicit, <code> 
     *          false </code> if explicitly managed 
     */

    @OSID @Override
    public boolean isImplicit() {
        return (this.speedZone.isImplicit());
    }


    /**
     *  Gets the speed limit in this zone. 
     *
     *  @return the speed limit 
     */

    @OSID @Override
    public org.osid.mapping.Speed getSpeedLimit() {
        return (this.speedZone.getSpeedLimit());
    }


    /**
     *  Gets the speed zone record corresponding to the given <code> SpeedZone 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record.. The <code> 
     *  speedZoneRecordType </code> may be the <code> Type </code> returned in 
     *  <code> getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(speedZoneRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  speedZoneRecordType the type of speed zone record to retrieve 
     *  @return the speed zone record 
     *  @throws org.osid.NullArgumentException <code> speedZoneRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(speedZoneRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.records.SpeedZoneRecord getSpeedZoneRecord(org.osid.type.Type speedZoneRecordType)
        throws org.osid.OperationFailedException {

        return (this.speedZone.getSpeedZoneRecord(speedZoneRecordType));
    }
}

