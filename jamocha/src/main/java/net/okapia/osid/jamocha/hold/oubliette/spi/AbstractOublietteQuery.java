//
// AbstractOublietteQuery.java
//
//     A template for making an Oubliette Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.hold.oubliette.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for oubliettes.
 */

public abstract class AbstractOublietteQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.hold.OublietteQuery {

    private final java.util.Collection<org.osid.hold.records.OublietteQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the effort <code> Id </code> for this query to match oubliettes 
     *  containing blocks. 
     *
     *  @param  blockId the block <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> blockId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBlockId(org.osid.id.Id blockId, boolean match) {
        return;
    }


    /**
     *  Clears the block query terms. 
     */

    @OSID @Override
    public void clearBlockIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BlockQuery </code> is available. 
     *
     *  @return <code> true </code> if a block query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlockQuery() {
        return (false);
    }


    /**
     *  Gets the query for a block. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the block query 
     *  @throws org.osid.UnimplementedException <code> supportsBlockQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockQuery getBlockQuery() {
        throw new org.osid.UnimplementedException("supportsBlockQuery() is false");
    }


    /**
     *  Matches oubliettes that have any block. 
     *
     *  @param  match <code> true </code> to match oubliettes with any block, 
     *          <code> false </code> to match oubliettes with no block 
     */

    @OSID @Override
    public void matchAnyBlock(boolean match) {
        return;
    }


    /**
     *  Clears the block query terms. 
     */

    @OSID @Override
    public void clearBlockTerms() {
        return;
    }


    /**
     *  Sets the issue <code> Id </code> for this query to match oubliettes 
     *  that have a related issue. 
     *
     *  @param  issueId a issue <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> issueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchIssueId(org.osid.id.Id issueId, boolean match) {
        return;
    }


    /**
     *  Clears the issue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearIssueIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> IssueQuery </code> is available. 
     *
     *  @return <code> true </code> if a issue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a issue. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the issue query 
     *  @throws org.osid.UnimplementedException <code> supportsIssueQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueQuery getIssueQuery() {
        throw new org.osid.UnimplementedException("supportsIssueQuery() is false");
    }


    /**
     *  Matches oubliettes that have any issue. 
     *
     *  @param  match <code> true </code> to match oubliettes with any issue, 
     *          <code> false </code> to match oubliettes with no issue 
     */

    @OSID @Override
    public void matchAnyIssue(boolean match) {
        return;
    }


    /**
     *  Clears the issue query terms. 
     */

    @OSID @Override
    public void clearIssueTerms() {
        return;
    }


    /**
     *  Sets the hold <code> Id </code> for this query. 
     *
     *  @param  holdId the hold <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> holdId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchHoldId(org.osid.id.Id holdId, boolean match) {
        return;
    }


    /**
     *  Clears the hold <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearHoldIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> HoldQuery </code> is available. 
     *
     *  @return <code> true </code> if a hold query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldQuery() {
        return (false);
    }


    /**
     *  Gets the query for a hold. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the hold query 
     *  @throws org.osid.UnimplementedException <code> supportsHoldQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldQuery getHoldQuery() {
        throw new org.osid.UnimplementedException("supportsHoldQuery() is false");
    }


    /**
     *  Matches oubliettes referenced by any hold. 
     *
     *  @param  match <code> true </code> to match oubliettes with any holds, 
     *          <code> false </code> to match oubliettes with no holds 
     */

    @OSID @Override
    public void matchAnyHold(boolean match) {
        return;
    }


    /**
     *  Clears the hold query terms. 
     */

    @OSID @Override
    public void clearHoldTerms() {
        return;
    }


    /**
     *  Sets the oubliette <code> Id </code> for this query to match 
     *  oubliettes that have the specified oubliette as an ancestor. 
     *
     *  @param  oublietteId an oubliette <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorOublietteId(org.osid.id.Id oublietteId, 
                                         boolean match) {
        return;
    }


    /**
     *  Clears the ancestor oubliette <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorOublietteIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> OublietteQuery </code> is available. 
     *
     *  @return <code> true </code> if an oubliette query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorOublietteQuery() {
        return (false);
    }


    /**
     *  Gets the query for an oubliette. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the oubliette query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorOublietteQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hold.OublietteQuery getAncestorOublietteQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorOublietteQuery() is false");
    }


    /**
     *  Matches oubliettes with any ancestor. 
     *
     *  @param  match <code> true </code> to match oubliettes with any 
     *          ancestor, <code> false </code> to match root oubliettes 
     */

    @OSID @Override
    public void matchAnyAncestorOubliette(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor oubliette query terms. 
     */

    @OSID @Override
    public void clearAncestorOublietteTerms() {
        return;
    }


    /**
     *  Sets the oubliette <code> Id </code> for this query to match 
     *  oubliettes that have the specified oubliette as a descendant. 
     *
     *  @param  oublietteId an oubliette <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantOublietteId(org.osid.id.Id oublietteId, 
                                           boolean match) {
        return;
    }


    /**
     *  Clears the descendant oubliette <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantOublietteIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> OublietteQuery </code> is available. 
     *
     *  @return <code> true </code> if an oubliette query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantOublietteQuery() {
        return (false);
    }


    /**
     *  Gets the query for an oubliette/ Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the oubliette query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantOublietteQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hold.OublietteQuery getDescendantOublietteQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantOublietteQuery() is false");
    }


    /**
     *  Matches oubliettes with any descendant. 
     *
     *  @param  match <code> true </code> to match oubliettes with any 
     *          descendant, <code> false </code> to match leaf oubliettes 
     */

    @OSID @Override
    public void matchAnyDescendantOubliette(boolean match) {
        return;
    }


    /**
     *  Clears the descendant oubliette query terms. 
     */

    @OSID @Override
    public void clearDescendantOublietteTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given oubliette query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an oubliette implementing the requested record.
     *
     *  @param oublietteRecordType an oubliette record type
     *  @return the oubliette query record
     *  @throws org.osid.NullArgumentException
     *          <code>oublietteRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(oublietteRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.hold.records.OublietteQueryRecord getOublietteQueryRecord(org.osid.type.Type oublietteRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.hold.records.OublietteQueryRecord record : this.records) {
            if (record.implementsRecordType(oublietteRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(oublietteRecordType + " is not supported");
    }


    /**
     *  Adds a record to this oubliette query. 
     *
     *  @param oublietteQueryRecord oubliette query record
     *  @param oublietteRecordType oubliette record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addOublietteQueryRecord(org.osid.hold.records.OublietteQueryRecord oublietteQueryRecord, 
                                          org.osid.type.Type oublietteRecordType) {

        addRecordType(oublietteRecordType);
        nullarg(oublietteQueryRecord, "oubliette query record");
        this.records.add(oublietteQueryRecord);        
        return;
    }
}
