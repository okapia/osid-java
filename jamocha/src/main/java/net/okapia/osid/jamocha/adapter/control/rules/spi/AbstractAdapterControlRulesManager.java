//
// AbstractControlRulesManager.java
//
//     An adapter for a ControlRulesManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.control.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a ControlRulesManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterControlRulesManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.control.rules.ControlRulesManager>
    implements org.osid.control.rules.ControlRulesManager {


    /**
     *  Constructs a new {@code AbstractAdapterControlRulesManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterControlRulesManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterControlRulesManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterControlRulesManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up device enablers is supported. 
     *
     *  @return <code> true </code> if device enabler lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceEnablerLookup() {
        return (getAdapteeManager().supportsDeviceEnablerLookup());
    }


    /**
     *  Tests if querying device enablers is supported. 
     *
     *  @return <code> true </code> if device enabler query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceEnablerQuery() {
        return (getAdapteeManager().supportsDeviceEnablerQuery());
    }


    /**
     *  Tests if searching device enablers is supported. 
     *
     *  @return <code> true </code> if device enabler search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceEnablerSearch() {
        return (getAdapteeManager().supportsDeviceEnablerSearch());
    }


    /**
     *  Tests if a device enabler administrative service is supported. 
     *
     *  @return <code> true </code> if device enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceEnablerAdmin() {
        return (getAdapteeManager().supportsDeviceEnablerAdmin());
    }


    /**
     *  Tests if a device enabler notification service is supported. 
     *
     *  @return <code> true </code> if device enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceEnablerNotification() {
        return (getAdapteeManager().supportsDeviceEnablerNotification());
    }


    /**
     *  Tests if a device enabler system lookup service is supported. 
     *
     *  @return <code> true </code> if a device enabler system lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceEnablerSystem() {
        return (getAdapteeManager().supportsDeviceEnablerSystem());
    }


    /**
     *  Tests if a device enabler system service is supported. 
     *
     *  @return <code> true </code> if device enabler system assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceEnablerSystemAssignment() {
        return (getAdapteeManager().supportsDeviceEnablerSystemAssignment());
    }


    /**
     *  Tests if a device enabler system lookup service is supported. 
     *
     *  @return <code> true </code> if a device enabler system service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceEnablerSmartSystem() {
        return (getAdapteeManager().supportsDeviceEnablerSmartSystem());
    }


    /**
     *  Tests if looking up input enablers is supported. 
     *
     *  @return <code> true </code> if input enabler lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputEnablerLookup() {
        return (getAdapteeManager().supportsInputEnablerLookup());
    }


    /**
     *  Tests if querying input enablers is supported. 
     *
     *  @return <code> true </code> if input enabler query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputEnablerQuery() {
        return (getAdapteeManager().supportsInputEnablerQuery());
    }


    /**
     *  Tests if searching input enablers is supported. 
     *
     *  @return <code> true </code> if input enabler search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputEnablerSearch() {
        return (getAdapteeManager().supportsInputEnablerSearch());
    }


    /**
     *  Tests if an input enabler administrative service is supported. 
     *
     *  @return <code> true </code> if input enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputEnablerAdmin() {
        return (getAdapteeManager().supportsInputEnablerAdmin());
    }


    /**
     *  Tests if an input enabler notification service is supported. 
     *
     *  @return <code> true </code> if input enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputEnablerNotification() {
        return (getAdapteeManager().supportsInputEnablerNotification());
    }


    /**
     *  Tests if an input enabler system lookup service is supported. 
     *
     *  @return <code> true </code> if an input enabler system lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputEnablerSystem() {
        return (getAdapteeManager().supportsInputEnablerSystem());
    }


    /**
     *  Tests if an input enabler system service is supported. 
     *
     *  @return <code> true </code> if input enabler system assignment service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputEnablerSystemAssignment() {
        return (getAdapteeManager().supportsInputEnablerSystemAssignment());
    }


    /**
     *  Tests if an input enabler system lookup service is supported. 
     *
     *  @return <code> true </code> if an input enabler system service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputEnablerSmartSystem() {
        return (getAdapteeManager().supportsInputEnablerSmartSystem());
    }


    /**
     *  Tests if an input enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if an processor enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputEnablerRuleLookup() {
        return (getAdapteeManager().supportsInputEnablerRuleLookup());
    }


    /**
     *  Tests if an input enabler rule application service is supported. 
     *
     *  @return <code> true </code> if input enabler rule application service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputEnablerRuleApplication() {
        return (getAdapteeManager().supportsInputEnablerRuleApplication());
    }


    /**
     *  Tests if looking up trigger enablers is supported. 
     *
     *  @return <code> true </code> if trigger enabler lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerEnablerLookup() {
        return (getAdapteeManager().supportsTriggerEnablerLookup());
    }


    /**
     *  Tests if querying trigger enablers is supported. 
     *
     *  @return <code> true </code> if trigger enabler query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerEnablerQuery() {
        return (getAdapteeManager().supportsTriggerEnablerQuery());
    }


    /**
     *  Tests if searching trigger enablers is supported. 
     *
     *  @return <code> true </code> if trigger enabler search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerEnablerSearch() {
        return (getAdapteeManager().supportsTriggerEnablerSearch());
    }


    /**
     *  Tests if a trigger enabler administrative service is supported. 
     *
     *  @return <code> true </code> if trigger enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerEnablerAdmin() {
        return (getAdapteeManager().supportsTriggerEnablerAdmin());
    }


    /**
     *  Tests if a trigger enabler notification service is supported. 
     *
     *  @return <code> true </code> if trigger enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerEnablerNotification() {
        return (getAdapteeManager().supportsTriggerEnablerNotification());
    }


    /**
     *  Tests if a trigger enabler system lookup service is supported. 
     *
     *  @return <code> true </code> if a trigger enabler system lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerEnablerSystem() {
        return (getAdapteeManager().supportsTriggerEnablerSystem());
    }


    /**
     *  Tests if a trigger enabler system service is supported. 
     *
     *  @return <code> true </code> if trigger enabler system assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerEnablerSystemAssignment() {
        return (getAdapteeManager().supportsTriggerEnablerSystemAssignment());
    }


    /**
     *  Tests if a trigger enabler system lookup service is supported. 
     *
     *  @return <code> true </code> if a trigger enabler system service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerEnablerSmartSystem() {
        return (getAdapteeManager().supportsTriggerEnablerSmartSystem());
    }


    /**
     *  Tests if a trigger enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a trigger enabler rule lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerEnablerRuleLookup() {
        return (getAdapteeManager().supportsTriggerEnablerRuleLookup());
    }


    /**
     *  Tests if a trigger enabler rule application service is supported. 
     *
     *  @return <code> true </code> if trigger enabler rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerEnablerRuleApplication() {
        return (getAdapteeManager().supportsTriggerEnablerRuleApplication());
    }


    /**
     *  Tests if looking up action enablers is supported. 
     *
     *  @return <code> true </code> if action enabler lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionEnablerLookup() {
        return (getAdapteeManager().supportsActionEnablerLookup());
    }


    /**
     *  Tests if querying action enablers is supported. 
     *
     *  @return <code> true </code> if action enabler query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionEnablerQuery() {
        return (getAdapteeManager().supportsActionEnablerQuery());
    }


    /**
     *  Tests if searching action enablers is supported. 
     *
     *  @return <code> true </code> if action enabler search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionEnablerSearch() {
        return (getAdapteeManager().supportsActionEnablerSearch());
    }


    /**
     *  Tests if an action enabler administrative service is supported. 
     *
     *  @return <code> true </code> if action enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionEnablerAdmin() {
        return (getAdapteeManager().supportsActionEnablerAdmin());
    }


    /**
     *  Tests if an action enabler notification service is supported. 
     *
     *  @return <code> true </code> if action enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionEnablerNotification() {
        return (getAdapteeManager().supportsActionEnablerNotification());
    }


    /**
     *  Tests if an action enabler system lookup service is supported. 
     *
     *  @return <code> true </code> if an action enabler system lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionEnablerSystem() {
        return (getAdapteeManager().supportsActionEnablerSystem());
    }


    /**
     *  Tests if an action enabler system service is supported. 
     *
     *  @return <code> true </code> if action enabler system assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionEnablerSystemAssignment() {
        return (getAdapteeManager().supportsActionEnablerSystemAssignment());
    }


    /**
     *  Tests if an action enabler system lookup service is supported. 
     *
     *  @return <code> true </code> if an action enabler system service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionEnablerSmartSystem() {
        return (getAdapteeManager().supportsActionEnablerSmartSystem());
    }


    /**
     *  Gets the supported <code> DeviceEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> DeviceEnabler </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDeviceEnablerRecordTypes() {
        return (getAdapteeManager().getDeviceEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> DeviceEnabler </code> record type is 
     *  supported. 
     *
     *  @param  deviceEnablerRecordType a <code> Type </code> indicating a 
     *          <code> DeviceEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> deviceEnablerRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDeviceEnablerRecordType(org.osid.type.Type deviceEnablerRecordType) {
        return (getAdapteeManager().supportsDeviceEnablerRecordType(deviceEnablerRecordType));
    }


    /**
     *  Gets the supported <code> DeviceEnabler </code> search record types. 
     *
     *  @return a list containing the supported <code> DeviceEnabler </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDeviceEnablerSearchRecordTypes() {
        return (getAdapteeManager().getDeviceEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> DeviceEnabler </code> search record type is 
     *  supported. 
     *
     *  @param  deviceEnablerSearchRecordType a <code> Type </code> indicating 
     *          a <code> DeviceEnabler </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          deviceEnablerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDeviceEnablerSearchRecordType(org.osid.type.Type deviceEnablerSearchRecordType) {
        return (getAdapteeManager().supportsDeviceEnablerSearchRecordType(deviceEnablerSearchRecordType));
    }


    /**
     *  Gets the supported <code> InputEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> InputEnabler </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getInputEnablerRecordTypes() {
        return (getAdapteeManager().getInputEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> InputEnabler </code> record type is 
     *  supported. 
     *
     *  @param  inputEnablerRecordType a <code> Type </code> indicating an 
     *          <code> InputEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> inputEnablerRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsInputEnablerRecordType(org.osid.type.Type inputEnablerRecordType) {
        return (getAdapteeManager().supportsInputEnablerRecordType(inputEnablerRecordType));
    }


    /**
     *  Gets the supported <code> InputEnabler </code> search record types. 
     *
     *  @return a list containing the supported <code> InputEnabler </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getInputEnablerSearchRecordTypes() {
        return (getAdapteeManager().getInputEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> InputEnabler </code> search record type is 
     *  supported. 
     *
     *  @param  inputEnablerSearchRecordType a <code> Type </code> indicating 
     *          an <code> InputEnabler </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          inputEnablerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsInputEnablerSearchRecordType(org.osid.type.Type inputEnablerSearchRecordType) {
        return (getAdapteeManager().supportsInputEnablerSearchRecordType(inputEnablerSearchRecordType));
    }


    /**
     *  Gets the supported <code> TriggerEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> TriggerEnabler </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getTriggerEnablerRecordTypes() {
        return (getAdapteeManager().getTriggerEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> TriggerEnabler </code> record type is 
     *  supported. 
     *
     *  @param  triggerEnablerRecordType a <code> Type </code> indicating a 
     *          <code> TriggerEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> triggerEnablerRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsTriggerEnablerRecordType(org.osid.type.Type triggerEnablerRecordType) {
        return (getAdapteeManager().supportsTriggerEnablerRecordType(triggerEnablerRecordType));
    }


    /**
     *  Gets the supported <code> TriggerEnabler </code> search record types. 
     *
     *  @return a list containing the supported <code> TriggerEnabler </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getTriggerEnablerSearchRecordTypes() {
        return (getAdapteeManager().getTriggerEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> TriggerEnabler </code> search record type is 
     *  supported. 
     *
     *  @param  triggerEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> TriggerEnabler </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          triggerEnablerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsTriggerEnablerSearchRecordType(org.osid.type.Type triggerEnablerSearchRecordType) {
        return (getAdapteeManager().supportsTriggerEnablerSearchRecordType(triggerEnablerSearchRecordType));
    }


    /**
     *  Gets the supported <code> ActionEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> ActionEnabler </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getActionEnablerRecordTypes() {
        return (getAdapteeManager().getActionEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> ActionEnabler </code> record type is 
     *  supported. 
     *
     *  @param  actionEnablerRecordType a <code> Type </code> indicating an 
     *          <code> ActionEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> actionEnablerRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsActionEnablerRecordType(org.osid.type.Type actionEnablerRecordType) {
        return (getAdapteeManager().supportsActionEnablerRecordType(actionEnablerRecordType));
    }


    /**
     *  Gets the supported <code> ActionEnabler </code> search record types. 
     *
     *  @return a list containing the supported <code> ActionEnabler </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getActionEnablerSearchRecordTypes() {
        return (getAdapteeManager().getActionEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> ActionEnabler </code> search record type is 
     *  supported. 
     *
     *  @param  actionEnablerSearchRecordType a <code> Type </code> indicating 
     *          an <code> ActionEnabler </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          actionEnablerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsActionEnablerSearchRecordType(org.osid.type.Type actionEnablerSearchRecordType) {
        return (getAdapteeManager().supportsActionEnablerSearchRecordType(actionEnablerSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  lookup service. 
     *
     *  @return a <code> DeviceEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerLookupSession getDeviceEnablerLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceEnablerLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  lookup service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> DeviceEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerLookupSession getDeviceEnablerLookupSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceEnablerLookupSessionForSystem(systemId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  query service. 
     *
     *  @return a <code> DeviceEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerQuerySession getDeviceEnablerQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceEnablerQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  query service for the given System. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> DeviceEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerQuerySession getDeviceEnablerQuerySessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceEnablerQuerySessionForSystem(systemId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  search service. 
     *
     *  @return a <code> DeviceEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerSearchSession getDeviceEnablerSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceEnablerSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device 
     *  enablers earch service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> DeviceEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerSearchSession getDeviceEnablerSearchSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceEnablerSearchSessionForSystem(systemId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  administration service. 
     *
     *  @return a <code> DeviceEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerAdminSession getDeviceEnablerAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceEnablerAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  administration service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> DeviceEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerAdminSession getDeviceEnablerAdminSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceEnablerAdminSessionForSystem(systemId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  notification service. 
     *
     *  @param  deviceEnablerReceiver the notification callback 
     *  @return a <code> DeviceEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> deviceEnablerReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerNotificationSession getDeviceEnablerNotificationSession(org.osid.control.rules.DeviceEnablerReceiver deviceEnablerReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceEnablerNotificationSession(deviceEnablerReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  notification service for the given system. 
     *
     *  @param  deviceEnablerReceiver the notification callback 
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> DeviceEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> deviceEnablerReceiver 
     *          </code> or <code> systemId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerNotificationSession getDeviceEnablerNotificationSessionForSystem(org.osid.control.rules.DeviceEnablerReceiver deviceEnablerReceiver, 
                                                                                                                org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceEnablerNotificationSessionForSystem(deviceEnablerReceiver, systemId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup device enabler/system 
     *  mappings for device enablers. 
     *
     *  @return a <code> DeviceEnablerSystemSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerSystem() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerSystemSession getDeviceEnablerSystemSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceEnablerSystemSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning device 
     *  enablers to systems. 
     *
     *  @return a <code> DeviceEnablerSystemAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerSystemAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerSystemAssignmentSession getDeviceEnablerSystemAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceEnablerSystemAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage device enabler smart 
     *  systems. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> DeviceEnablerSmartSystemSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerSmartSystem() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerSmartSystemSession getDeviceEnablerSmartSystemSession(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceEnablerSmartSystemSession(systemId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  mapping lookup service. 
     *
     *  @return a <code> DeviceEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerRuleLookupSession getDeviceEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceEnablerRuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  mapping lookup service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> DeviceEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerRuleLookupSession getDeviceEnablerRuleLookupSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceEnablerRuleLookupSessionForSystem(systemId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  assignment service. 
     *
     *  @return a <code> DeviceEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerRuleApplication() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerRuleApplicationSession getDeviceEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceEnablerRuleApplicationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  assignment service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> DeviceEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerRuleApplicationSession getDeviceEnablerRuleApplicationSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceEnablerRuleApplicationSessionForSystem(systemId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  lookup service. 
     *
     *  @return an <code> InputEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerLookupSession getInputEnablerLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInputEnablerLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  lookup service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> InputEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerLookupSession getInputEnablerLookupSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInputEnablerLookupSessionForSystem(systemId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  query service. 
     *
     *  @return an <code> InputEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerQuerySession getInputEnablerQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInputEnablerQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  query service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> InputEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerQuerySession getInputEnablerQuerySessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInputEnablerQuerySessionForSystem(systemId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  search service. 
     *
     *  @return an <code> InputEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerSearchSession getInputEnablerSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInputEnablerSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enablers 
     *  earch service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> InputEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerSearchSession getInputEnablerSearchSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInputEnablerSearchSessionForSystem(systemId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  administration service. 
     *
     *  @return an <code> InputEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerAdminSession getInputEnablerAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInputEnablerAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  administration service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> InputEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerAdminSession getInputEnablerAdminSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInputEnablerAdminSessionForSystem(systemId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  notification service. 
     *
     *  @param  inputEnablerReceiver the notification callback 
     *  @return an <code> InputEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> inputEnablerReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerNotificationSession getInputEnablerNotificationSession(org.osid.control.rules.InputEnablerReceiver inputEnablerReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInputEnablerNotificationSession(inputEnablerReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  notification service for the given system. 
     *
     *  @param  inputEnablerReceiver the notification callback 
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> InputEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> inputEnablerReceiver 
     *          </code> or <code> systemId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerNotificationSession getInputEnablerNotificationSessionForSystem(org.osid.control.rules.InputEnablerReceiver inputEnablerReceiver, 
                                                                                                              org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInputEnablerNotificationSessionForSystem(inputEnablerReceiver, systemId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup input enabler/system 
     *  mappings for input enablers. 
     *
     *  @return an <code> InputEnablerSystemSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerSystem() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerSystemSession getInputEnablerSystemSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInputEnablerSystemSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning input 
     *  enablers to systems. 
     *
     *  @return an <code> InputEnablerSystemAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerSystemAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerSystemAssignmentSession getInputEnablerSystemAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInputEnablerSystemAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage input enabler smart 
     *  systems. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> InputEnablerSmartSystemSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerSmartSystem() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerSmartSystemSession getInputEnablerSmartSystemSession(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInputEnablerSmartSystemSession(systemId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  mapping lookup service. 
     *
     *  @return an <code> InputEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerRuleLookupSession getInputEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInputEnablerRuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  mapping lookup service. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> InputEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerRuleLookupSession getInputEnablerRuleLookupSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInputEnablerRuleLookupSessionForSystem(systemId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  assignment service. 
     *
     *  @return an <code> InputEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerRuleApplication() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerRuleApplicationSession getInputEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInputEnablerRuleApplicationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  assignment service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> InputEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerRuleApplicationSession getInputEnablerRuleApplicationSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInputEnablerRuleApplicationSessionForSystem(systemId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler lookup service. 
     *
     *  @return a <code> TriggerEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerLookupSession getTriggerEnablerLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerEnablerLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler lookup service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> TriggerEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerLookupSession getTriggerEnablerLookupSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerEnablerLookupSessionForSystem(systemId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler query service. 
     *
     *  @return a <code> TriggerEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerQuerySession getTriggerEnablerQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerEnablerQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler query service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> TriggerEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerQuerySession getTriggerEnablerQuerySessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerEnablerQuerySessionForSystem(systemId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler search service. 
     *
     *  @return a <code> TriggerEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerSearchSession getTriggerEnablerSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerEnablerSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enablers earch service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> TriggerEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerSearchSession getTriggerEnablerSearchSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerEnablerSearchSessionForSystem(systemId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler administration service. 
     *
     *  @return a <code> TriggerEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerAdminSession getTriggerEnablerAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerEnablerAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler administration service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> TriggerEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerAdminSession getTriggerEnablerAdminSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerEnablerAdminSessionForSystem(systemId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler notification service. 
     *
     *  @param  triggerEnablerReceiver the notification callback 
     *  @return a <code> TriggerEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> triggerEnablerReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerNotificationSession getTriggerEnablerNotificationSession(org.osid.control.rules.TriggerEnablerReceiver triggerEnablerReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerEnablerNotificationSession(triggerEnablerReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler notification service for the given system. 
     *
     *  @param  triggerEnablerReceiver the notification callback 
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> TriggerEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> triggerEnablerReceiver 
     *          </code> or <code> systemId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerNotificationSession getTriggerEnablerNotificationSessionForSystem(org.osid.control.rules.TriggerEnablerReceiver triggerEnablerReceiver, 
                                                                                                                  org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerEnablerNotificationSessionForSystem(triggerEnablerReceiver, systemId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup trigger enabler/system 
     *  mappings for trigger enablers. 
     *
     *  @return a <code> TriggerEnablerSystemSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerSystem() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerSystemSession getTriggerEnablerSystemSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerEnablerSystemSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning trigger 
     *  enablers to systems. 
     *
     *  @return a <code> TriggerEnablerSystemAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerSystemAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerSystemAssignmentSession getTriggerEnablerSystemAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerEnablerSystemAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage trigger enabler smart 
     *  systems. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> TriggerEnablerSmartSystemSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerSmartSystem() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerSmartSystemSession getTriggerEnablerSmartSystemSession(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerEnablerSmartSystemSession(systemId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler mapping lookup service. 
     *
     *  @return a <code> TriggerEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerRuleLookupSession getTriggerEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerEnablerRuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler mapping lookup service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> TriggerEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerRuleLookupSession getTriggerEnablerRuleLookupSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerEnablerRuleLookupSessionForSystem(systemId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler assignment service. 
     *
     *  @return a <code> TriggerEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerRuleApplicationSession getTriggerEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerEnablerRuleApplicationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler assignment service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> TriggerEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerRuleApplicationSession getTriggerEnablerRuleApplicationSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerEnablerRuleApplicationSessionForSystem(systemId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  lookup service. 
     *
     *  @return an <code> ActionEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerLookupSession getActionEnablerLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActionEnablerLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  lookup service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> ActionEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerLookupSession getActionEnablerLookupSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActionEnablerLookupSessionForSystem(systemId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  query service. 
     *
     *  @return an <code> ActionEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerQuerySession getActionEnablerQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActionEnablerQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  query service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> ActionEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerQuerySession getActionEnablerQuerySessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActionEnablerQuerySessionForSystem(systemId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  search service. 
     *
     *  @return an <code> ActionEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerSearchSession getActionEnablerSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActionEnablerSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action 
     *  enablers earch service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> ActionEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerSearchSession getActionEnablerSearchSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActionEnablerSearchSessionForSystem(systemId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  administration service. 
     *
     *  @return an <code> ActionEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerAdminSession getActionEnablerAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActionEnablerAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  administration service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> ActionEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerAdminSession getActionEnablerAdminSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActionEnablerAdminSessionForSystem(systemId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  notification service. 
     *
     *  @param  actionEnablerReceiver the notification callback 
     *  @return an <code> ActionEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> actionEnablerReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerNotificationSession getActionEnablerNotificationSession(org.osid.control.rules.ActionEnablerReceiver actionEnablerReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActionEnablerNotificationSession(actionEnablerReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  notification service for the given system. 
     *
     *  @param  actionEnablerReceiver the notification callback 
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> ActionEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> actionEnablerReceiver 
     *          </code> or <code> systemId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerNotificationSession getActionEnablerNotificationSessionForSystem(org.osid.control.rules.ActionEnablerReceiver actionEnablerReceiver, 
                                                                                                                org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActionEnablerNotificationSessionForSystem(actionEnablerReceiver, systemId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup action enabler/system 
     *  mappings for action enablers. 
     *
     *  @return an <code> ActionEnablerSystemSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerSystem() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerSystemSession getActionEnablerSystemSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActionEnablerSystemSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning action 
     *  enablers to systems. 
     *
     *  @return an <code> ActionEnablerSystemAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerSystemAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerSystemAssignmentSession getActionEnablerSystemAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActionEnablerSystemAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage action enabler smart 
     *  systems. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> ActionEnablerSmartSystemSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerSmartSystem() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerSmartSystemSession getActionEnablerSmartSystemSession(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActionEnablerSmartSystemSession(systemId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  mapping lookup service. 
     *
     *  @return an <code> ActionEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerRuleLookupSession getActionEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActionEnablerRuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  mapping lookup service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> ActionEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerRuleLookupSession getActionEnablerRuleLookupSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActionEnablerRuleLookupSessionForSystem(systemId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  assignment service. 
     *
     *  @return an <code> ActionEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerRuleApplication() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerRuleApplicationSession getActionEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActionEnablerRuleApplicationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  assignment service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> ActionEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerRuleApplicationSession getActionEnablerRuleApplicationSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActionEnablerRuleApplicationSessionForSystem(systemId));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
