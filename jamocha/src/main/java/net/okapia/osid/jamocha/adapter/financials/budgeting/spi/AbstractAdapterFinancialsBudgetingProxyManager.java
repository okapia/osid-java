//
// AbstractFinancialsBudgetingProxyManager.java
//
//     An adapter for a FinancialsBudgetingProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.financials.budgeting.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a FinancialsBudgetingProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterFinancialsBudgetingProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.financials.budgeting.FinancialsBudgetingProxyManager>
    implements org.osid.financials.budgeting.FinancialsBudgetingProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterFinancialsBudgetingProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterFinancialsBudgetingProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterFinancialsBudgetingProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterFinancialsBudgetingProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any business federation is exposed. Federation is exposed 
     *  when a specific business may be identified, selected and used to 
     *  create a lookup or admin session. Federation is not exposed when a set 
     *  of catalogs appears as a single catalog. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up budgets is supported. 
     *
     *  @return <code> true </code> if budget lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBudgetLookup() {
        return (getAdapteeManager().supportsBudgetLookup());
    }


    /**
     *  Tests if querying budgets is supported. 
     *
     *  @return <code> true </code> if budget query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBudgetQuery() {
        return (getAdapteeManager().supportsBudgetQuery());
    }


    /**
     *  Tests if searching budgets is supported. 
     *
     *  @return <code> true </code> if budget search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBudgetSearch() {
        return (getAdapteeManager().supportsBudgetSearch());
    }


    /**
     *  Tests if budget <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if budget administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBudgetAdmin() {
        return (getAdapteeManager().supportsBudgetAdmin());
    }


    /**
     *  Tests if a budget <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if budget notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBudgetNotification() {
        return (getAdapteeManager().supportsBudgetNotification());
    }


    /**
     *  Tests if a budget cataloging service is supported. 
     *
     *  @return <code> true </code> if budget catalog is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBudgetBusiness() {
        return (getAdapteeManager().supportsBudgetBusiness());
    }


    /**
     *  Tests if a budget cataloging service is supported. A cataloging 
     *  service maps budgets to catalogs. 
     *
     *  @return <code> true </code> if budget cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBudgetBusinessAssignment() {
        return (getAdapteeManager().supportsBudgetBusinessAssignment());
    }


    /**
     *  Tests if a budget smart business session is available. 
     *
     *  @return <code> true </code> if a budget smart business session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBudgetSmartBusiness() {
        return (getAdapteeManager().supportsBudgetSmartBusiness());
    }


    /**
     *  Tests if looking up budget entries is supported. 
     *
     *  @return <code> true </code> if budget entry lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBudgetEntryLookup() {
        return (getAdapteeManager().supportsBudgetEntryLookup());
    }


    /**
     *  Tests if querying budget entries is supported. 
     *
     *  @return <code> true </code> if budget entry query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBudgetEntryQuery() {
        return (getAdapteeManager().supportsBudgetEntryQuery());
    }


    /**
     *  Tests if searching budget entries is supported. 
     *
     *  @return <code> true </code> if budget entry search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBudgetEntrySearch() {
        return (getAdapteeManager().supportsBudgetEntrySearch());
    }


    /**
     *  Tests if budget entry administrative service is supported. 
     *
     *  @return <code> true </code> if budget entry administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBudgetEntryAdmin() {
        return (getAdapteeManager().supportsBudgetEntryAdmin());
    }


    /**
     *  Tests if an entry <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if budget entry notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBudgetEntryNotification() {
        return (getAdapteeManager().supportsBudgetEntryNotification());
    }


    /**
     *  Tests if a budget entry cataloging service is supported. 
     *
     *  @return <code> true </code> if budget entry catalog is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBudgetEntryBusiness() {
        return (getAdapteeManager().supportsBudgetEntryBusiness());
    }


    /**
     *  Tests if a budget entry cataloging service is supported. 
     *
     *  @return <code> true </code> if budget entry cataloging is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBudgetEntryBusinessAssignment() {
        return (getAdapteeManager().supportsBudgetEntryBusinessAssignment());
    }


    /**
     *  Tests if a budget entry smart business session is available. 
     *
     *  @return <code> true </code> if a budget entry smart business session 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBudgetEntrySmartBusiness() {
        return (getAdapteeManager().supportsBudgetEntrySmartBusiness());
    }


    /**
     *  Tests for the availability of a financials budgeting batch service. 
     *
     *  @return <code> true </code> if a financials budgeting batch service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFinancialsBudgetingBatch() {
        return (getAdapteeManager().supportsFinancialsBudgetingBatch());
    }


    /**
     *  Gets the supported <code> Budget </code> record types. 
     *
     *  @return a list containing the supported <code> Budget </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBudgetRecordTypes() {
        return (getAdapteeManager().getBudgetRecordTypes());
    }


    /**
     *  Tests if the given <code> Budget </code> record type is supported. 
     *
     *  @param  budgetRecordType a <code> Type </code> indicating a <code> 
     *          Budget </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> budgetRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBudgetRecordType(org.osid.type.Type budgetRecordType) {
        return (getAdapteeManager().supportsBudgetRecordType(budgetRecordType));
    }


    /**
     *  Gets the supported <code> Budget </code> search record types. 
     *
     *  @return a list containing the supported <code> Budget </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBudgetSearchRecordTypes() {
        return (getAdapteeManager().getBudgetSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Budget </code> search record type is 
     *  supported. 
     *
     *  @param  budgetSearchRecordType a <code> Type </code> indicating a 
     *          <code> Budget </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> budgetSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBudgetSearchRecordType(org.osid.type.Type budgetSearchRecordType) {
        return (getAdapteeManager().supportsBudgetSearchRecordType(budgetSearchRecordType));
    }


    /**
     *  Gets the supported <code> BudgetEntry </code> record types. 
     *
     *  @return a list containing the supported <code> BudgetEntry </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBudgetEntryRecordTypes() {
        return (getAdapteeManager().getBudgetEntryRecordTypes());
    }


    /**
     *  Tests if the given <code> BudgetEntry </code> record type is 
     *  supported. 
     *
     *  @param  budgetEntryRecordType a <code> Type </code> indicating a 
     *          <code> BudgetEntry </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> budgetEntryRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBudgetEntryRecordType(org.osid.type.Type budgetEntryRecordType) {
        return (getAdapteeManager().supportsBudgetEntryRecordType(budgetEntryRecordType));
    }


    /**
     *  Gets the supported <code> BudgetEntry </code> search record types. 
     *
     *  @return a list containing the supported <code> BudgetEntry </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBudgetEntrySearchRecordTypes() {
        return (getAdapteeManager().getBudgetEntrySearchRecordTypes());
    }


    /**
     *  Tests if the given <code> BudgetEntry </code> search record type is 
     *  supported. 
     *
     *  @param  budgetEntrySearchRecordType a <code> Type </code> indicating a 
     *          <code> BudgetEntry </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          budgetEntrySearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBudgetEntrySearchRecordType(org.osid.type.Type budgetEntrySearchRecordType) {
        return (getAdapteeManager().supportsBudgetEntrySearchRecordType(budgetEntrySearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget lookup 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> BudgetSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBudgetLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetLookupSession getBudgetLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBudgetLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget lookup 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the business 
     *  @param  proxy proxy 
     *  @return a <code> BudgetLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBudgetLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetLookupSession getBudgetLookupSessionForBusiness(org.osid.id.Id businessId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBudgetLookupSessionForBusiness(businessId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget query 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> BudgetQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBudgetQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetQuerySession getBudgetQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBudgetQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget query 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> BudgetQuerySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBudgetQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetQuerySession getBudgetQuerySessionForBusiness(org.osid.id.Id businessId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBudgetQuerySessionForBusiness(businessId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget search 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> BudgetSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBudgetSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetSearchSession getBudgetSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBudgetSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget search 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> BudgetSearchSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBudgetSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetSearchSession getBudgetSearchSessionForBusiness(org.osid.id.Id businessId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBudgetSearchSessionForBusiness(businessId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> BudgetAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBudgetAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetAdminSession getBudgetAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBudgetAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> BudgetAdminSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBudgetAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetAdminSession getBudgetAdminSessionForBusiness(org.osid.id.Id businessId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBudgetAdminSessionForBusiness(businessId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget 
     *  notification service. 
     *
     *  @param  budgetReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return a <code> BudgetNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> budgetReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetNotificationSession getBudgetNotificationSession(org.osid.financials.budgeting.BudgetReceiver budgetReceiver, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBudgetNotificationSession(budgetReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget 
     *  notification service for the given business. 
     *
     *  @param  budgetReceiver the notification callback 
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> BudgetNotificationSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> budgetReceiver, 
     *          businessId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetNotificationSession getBudgetNotificationSessionForBusiness(org.osid.financials.budgeting.BudgetReceiver budgetReceiver, 
                                                                                                           org.osid.id.Id businessId, 
                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBudgetNotificationSessionForBusiness(budgetReceiver, businessId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup budget/catalog mappings. 
     *
     *  @param  proxy proxy 
     *  @return a <code> BudgetBusinessSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetBusiness() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetBusinessSession getBudgetBusinessSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBudgetBusinessSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning budgets 
     *  to businesses. 
     *
     *  @param  proxy proxy 
     *  @return a <code> BudgetBusinessAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetBusinessAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetBusinessAssignmentSession getBudgetBusinessAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBudgetBusinessAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget smart 
     *  business service. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> BudgetSmartBusinessSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetSmartBusiness() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetSmartBusinessSession getBudgetSmartBusinessSession(org.osid.id.Id businessId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBudgetSmartBusinessSession(businessId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget entry 
     *  lookup service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> BudgetEntryLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryLookupSession getBudgetEntryLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBudgetEntryLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget entry 
     *  lookup service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> BudgetEntryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntryLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryLookupSession getBudgetEntryLookupSessionForBusiness(org.osid.id.Id businessId, 
                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBudgetEntryLookupSessionForBusiness(businessId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget entry 
     *  query service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> BudgetEntryQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryQuerySession getBudgetEntryQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBudgetEntryQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget entry 
     *  query service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> BudgetEntryQuerySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntryQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryQuerySession getBudgetEntryQuerySessionForBusiness(org.osid.id.Id businessId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBudgetEntryQuerySessionForBusiness(businessId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget entry 
     *  search service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> BudgetEntrySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntrySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntrySearchSession getBudgetEntrySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBudgetEntrySearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget entry 
     *  search service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> BudgetEntrySearchSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntrySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntrySearchSession getBudgetEntrySearchSessionForBusiness(org.osid.id.Id businessId, 
                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBudgetEntrySearchSessionForBusiness(businessId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget entry 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> BudgetEntryAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryAdminSession getBudgetEntryAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBudgetEntryAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget entry 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> BudgetEntryAdminSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntryAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryAdminSession getBudgetEntryAdminSessionForBusiness(org.osid.id.Id businessId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBudgetEntryAdminSessionForBusiness(businessId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget entry 
     *  notification service. 
     *
     *  @param  budgetEntryReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return a <code> BudgetEntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> budgetEntryReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryNotificationSession getBudgetEntryNotificationSession(org.osid.financials.budgeting.BudgetEntryReceiver budgetEntryReceiver, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBudgetEntryNotificationSession(budgetEntryReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget entry 
     *  notification service for the given business. 
     *
     *  @param  budgetEntryReceiver the notification callback 
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> BudgetEntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> budgetEntryReceiver, 
     *          businessId, </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryNotificationSession getBudgetEntryNotificationSessionForBusiness(org.osid.financials.budgeting.BudgetEntryReceiver budgetEntryReceiver, 
                                                                                                                     org.osid.id.Id businessId, 
                                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBudgetEntryNotificationSessionForBusiness(budgetEntryReceiver, businessId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup entry/catalog mappings. 
     *
     *  @param  proxy proxy 
     *  @return a <code> BudgetEntryCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntryCatalog() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryBusinessSession getBudgetEntryBusinessSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBudgetEntryBusinessSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning budget 
     *  entries to businesses. 
     *
     *  @param  proxy proxy 
     *  @return a <code> BudgetEntryCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntryCatalogAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryBusinessAssignmentSession getBudgetEntryBusinessAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBudgetEntryBusinessAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the budget entry 
     *  smart business service. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> BudgetEntrySmartBusinessSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntrySmartBusiness() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntrySmartBusinessSession getBudgetEntrySmartBusinessSession(org.osid.id.Id businessId, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBudgetEntrySmartBusinessSession(businessId, proxy));
    }


    /**
     *  Gets a <code> FinancialsBudgetingBatchProxyManager. </code> 
     *
     *  @return a <code> FinancialsBudgetingBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFinancialsBudgetingBatch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.batch.FinancialsBudgetingBatchProxyManager getFinancialsBudgetingBatchProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFinancialsBudgetingBatchProxyManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
