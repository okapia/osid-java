//
// AbstractAdapterPoolLookupSession.java
//
//    A Pool lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.provisioning.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Pool lookup session adapter.
 */

public abstract class AbstractAdapterPoolLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.provisioning.PoolLookupSession {

    private final org.osid.provisioning.PoolLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterPoolLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterPoolLookupSession(org.osid.provisioning.PoolLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Distributor/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Distributor Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.session.getDistributorId());
    }


    /**
     *  Gets the {@code Distributor} associated with this session.
     *
     *  @return the {@code Distributor} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getDistributor());
    }


    /**
     *  Tests if this user can perform {@code Pool} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupPools() {
        return (this.session.canLookupPools());
    }


    /**
     *  A complete view of the {@code Pool} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePoolView() {
        this.session.useComparativePoolView();
        return;
    }


    /**
     *  A complete view of the {@code Pool} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPoolView() {
        this.session.usePlenaryPoolView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include pools in distributors which are children
     *  of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        this.session.useFederatedDistributorView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        this.session.useIsolatedDistributorView();
        return;
    }
    

    /**
     *  Only active pools are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActivePoolView() {
        this.session.useActivePoolView();
        return;
    }


    /**
     *  Active and inactive pools are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusPoolView() {
        this.session.useAnyStatusPoolView();
        return;
    }
    
     
    /**
     *  Gets the {@code Pool} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Pool} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Pool} and
     *  retained for compatibility.
     *
     *  In active mode, pools are returned that are currently
     *  active. In any status mode, active and inactive pools
     *  are returned.
     *
     *  @param poolId {@code Id} of the {@code Pool}
     *  @return the pool
     *  @throws org.osid.NotFoundException {@code poolId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code poolId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Pool getPool(org.osid.id.Id poolId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPool(poolId));
    }


    /**
     *  Gets a {@code PoolList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  pools specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Pools} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, pools are returned that are currently
     *  active. In any status mode, active and inactive pools
     *  are returned.
     *
     *  @param  poolIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Pool} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code poolIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.PoolList getPoolsByIds(org.osid.id.IdList poolIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPoolsByIds(poolIds));
    }


    /**
     *  Gets a {@code PoolList} corresponding to the given
     *  pool genus {@code Type} which does not include
     *  pools of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  pools or an error results. Otherwise, the returned list
     *  may contain only those pools that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, pools are returned that are currently
     *  active. In any status mode, active and inactive pools
     *  are returned.
     *
     *  @param  poolGenusType a pool genus type 
     *  @return the returned {@code Pool} list
     *  @throws org.osid.NullArgumentException
     *          {@code poolGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.PoolList getPoolsByGenusType(org.osid.type.Type poolGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPoolsByGenusType(poolGenusType));
    }


    /**
     *  Gets a {@code PoolList} corresponding to the given
     *  pool genus {@code Type} and include any additional
     *  pools with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  pools or an error results. Otherwise, the returned list
     *  may contain only those pools that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, pools are returned that are currently
     *  active. In any status mode, active and inactive pools
     *  are returned.
     *
     *  @param  poolGenusType a pool genus type 
     *  @return the returned {@code Pool} list
     *  @throws org.osid.NullArgumentException
     *          {@code poolGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.PoolList getPoolsByParentGenusType(org.osid.type.Type poolGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPoolsByParentGenusType(poolGenusType));
    }


    /**
     *  Gets a {@code PoolList} containing the given
     *  pool record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  pools or an error results. Otherwise, the returned list
     *  may contain only those pools that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, pools are returned that are currently
     *  active. In any status mode, active and inactive pools
     *  are returned.
     *
     *  @param  poolRecordType a pool record type 
     *  @return the returned {@code Pool} list
     *  @throws org.osid.NullArgumentException
     *          {@code poolRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.PoolList getPoolsByRecordType(org.osid.type.Type poolRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPoolsByRecordType(poolRecordType));
    }


    /**
     *  Gets a {@code PoolList} by broker {@code .} 
     *  
     *  {@code} In plenary mode, the returned list contains all known 
     *  pools or an error results. Otherwise, the returned list may contain 
     *  only those pools that are accessible through this session. 
     *  
     *  In active mode, pools are returned that are currently active. In any 
     *  status mode, active and inactive pool are returned. 
     *
     *  @param  brokerId a broker {@code Id} 
     *  @return the returned {@code Pool} list 
     *  @throws org.osid.NullArgumentException {@code brokerId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.provisioning.PoolList getPoolsForBroker(org.osid.id.Id brokerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPoolsForBroker(brokerId));
    }


    /**
     *  Gets a {@code PoolList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  pools or an error results. Otherwise, the returned list
     *  may contain only those pools that are accessible through
     *  this session.
     *
     *  In active mode, pools are returned that are currently
     *  active. In any status mode, active and inactive pools
     *  are returned.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Pool} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.provisioning.PoolList getPoolsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPoolsByProvider(resourceId));
    }


    /**
     *  Gets all {@code Pools}. 
     *
     *  In plenary mode, the returned list contains all known
     *  pools or an error results. Otherwise, the returned list
     *  may contain only those pools that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, pools are returned that are currently
     *  active. In any status mode, active and inactive pools
     *  are returned.
     *
     *  @return a list of {@code Pools} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.PoolList getPools()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPools());
    }
}
