//
// PublisherElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.subscription.publisher.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class PublisherElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the PublisherElement Id.
     *
     *  @return the publisher element Id
     */

    public static org.osid.id.Id getPublisherEntityId() {
        return (makeEntityId("osid.subscription.Publisher"));
    }


    /**
     *  Gets the SubscriptionId element Id.
     *
     *  @return the SubscriptionId element Id
     */

    public static org.osid.id.Id getSubscriptionId() {
        return (makeQueryElementId("osid.subscription.publisher.SubscriptionId"));
    }


    /**
     *  Gets the Subscription element Id.
     *
     *  @return the Subscription element Id
     */

    public static org.osid.id.Id getSubscription() {
        return (makeQueryElementId("osid.subscription.publisher.Subscription"));
    }


    /**
     *  Gets the DispatchId element Id.
     *
     *  @return the DispatchId element Id
     */

    public static org.osid.id.Id getDispatchId() {
        return (makeQueryElementId("osid.subscription.publisher.DispatchId"));
    }


    /**
     *  Gets the Dispatch element Id.
     *
     *  @return the Dispatch element Id
     */

    public static org.osid.id.Id getDispatch() {
        return (makeQueryElementId("osid.subscription.publisher.Dispatch"));
    }


    /**
     *  Gets the AncestorPublisherId element Id.
     *
     *  @return the AncestorPublisherId element Id
     */

    public static org.osid.id.Id getAncestorPublisherId() {
        return (makeQueryElementId("osid.subscription.publisher.AncestorPublisherId"));
    }


    /**
     *  Gets the AncestorPublisher element Id.
     *
     *  @return the AncestorPublisher element Id
     */

    public static org.osid.id.Id getAncestorPublisher() {
        return (makeQueryElementId("osid.subscription.publisher.AncestorPublisher"));
    }


    /**
     *  Gets the DescendantPublisherId element Id.
     *
     *  @return the DescendantPublisherId element Id
     */

    public static org.osid.id.Id getDescendantPublisherId() {
        return (makeQueryElementId("osid.subscription.publisher.DescendantPublisherId"));
    }


    /**
     *  Gets the DescendantPublisher element Id.
     *
     *  @return the DescendantPublisher element Id
     */

    public static org.osid.id.Id getDescendantPublisher() {
        return (makeQueryElementId("osid.subscription.publisher.DescendantPublisher"));
    }
}
