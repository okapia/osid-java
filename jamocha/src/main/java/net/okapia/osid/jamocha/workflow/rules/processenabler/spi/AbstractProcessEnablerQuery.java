//
// AbstractProcessEnablerQuery.java
//
//     A template for making a ProcessEnabler Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.rules.processenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for process enablers.
 */

public abstract class AbstractProcessEnablerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.workflow.rules.ProcessEnablerQuery {

    private final java.util.Collection<org.osid.workflow.rules.records.ProcessEnablerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches enablers mapped to a process. 
     *
     *  @param  processId the process <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> processId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledProcessId(org.osid.id.Id processId, boolean match) {
        return;
    }


    /**
     *  Clears the process <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledProcessIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ProcessQuery </code> is available. 
     *
     *  @return <code> true </code> if a process query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledProcessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a process. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the process query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledProcessQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessQuery getRuledProcessQuery() {
        throw new org.osid.UnimplementedException("supportsRuledProcessQuery() is false");
    }


    /**
     *  Matches rules mapped to any process. 
     *
     *  @param  match <code> true </code> for rules mapped to any process, 
     *          <code> false </code> to match rules mapped to no processs 
     */

    @OSID @Override
    public void matchAnyRuledProcess(boolean match) {
        return;
    }


    /**
     *  Clears the process query terms. 
     */

    @OSID @Override
    public void clearRuledProcessTerms() {
        return;
    }


    /**
     *  Matches enablers mapped to an office. 
     *
     *  @param  officeId the office <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOfficeId(org.osid.id.Id officeId, boolean match) {
        return;
    }


    /**
     *  Clears the office <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearOfficeIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> OfficeQuery </code> is available. 
     *
     *  @return <code> true </code> if an office query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfficeQuery() {
        return (false);
    }


    /**
     *  Gets the query for an office. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the office query 
     *  @throws org.osid.UnimplementedException <code> supportsOfficeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.OfficeQuery getOfficeQuery() {
        throw new org.osid.UnimplementedException("supportsOfficeQuery() is false");
    }


    /**
     *  Clears the office query terms. 
     */

    @OSID @Override
    public void clearOfficeTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given process enabler query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a process enabler implementing the requested record.
     *
     *  @param processEnablerRecordType a process enabler record type
     *  @return the process enabler query record
     *  @throws org.osid.NullArgumentException
     *          <code>processEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(processEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.rules.records.ProcessEnablerQueryRecord getProcessEnablerQueryRecord(org.osid.type.Type processEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.workflow.rules.records.ProcessEnablerQueryRecord record : this.records) {
            if (record.implementsRecordType(processEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(processEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this process enabler query. 
     *
     *  @param processEnablerQueryRecord process enabler query record
     *  @param processEnablerRecordType processEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addProcessEnablerQueryRecord(org.osid.workflow.rules.records.ProcessEnablerQueryRecord processEnablerQueryRecord, 
                                          org.osid.type.Type processEnablerRecordType) {

        addRecordType(processEnablerRecordType);
        nullarg(processEnablerQueryRecord, "process enabler query record");
        this.records.add(processEnablerQueryRecord);        
        return;
    }
}
