//
// AbstractSequenceRuleQueryInspector.java
//
//     A template for making a SequenceRuleQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.authoring.sequencerule.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for sequence rules.
 */

public abstract class AbstractSequenceRuleQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleQueryInspector
    implements org.osid.assessment.authoring.SequenceRuleQueryInspector {

    private final java.util.Collection<org.osid.assessment.authoring.records.SequenceRuleQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the assessment part <code> Id </code> query terms. 
     *
     *  @return the assessment parent <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssessmentPartIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the assessment part query terms. 
     *
     *  @return the assessment part terms 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartQueryInspector[] getAssessmentPartTerms() {
        return (new org.osid.assessment.authoring.AssessmentPartQueryInspector[0]);
    }


    /**
     *  Gets the assessment part <code> Id </code> query terms. 
     *
     *  @return the assessment parent <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getNextAssessmentPartIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the assessment part query terms. 
     *
     *  @return the assessment part terms 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartQueryInspector[] getNextAssessmentPartTerms() {
        return (new org.osid.assessment.authoring.AssessmentPartQueryInspector[0]);
    }


    /**
     *  Gets the minimum score query terms. 
     *
     *  @return the minimum score terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getMinimumScoreTerms() {
        return (new org.osid.search.terms.CardinalRangeTerm[0]);
    }


    /**
     *  Gets the maximum score query terms. 
     *
     *  @return the maximum score terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getMaximumScoreTerms() {
        return (new org.osid.search.terms.CardinalRangeTerm[0]);
    }


    /**
     *  Gets the minimum score query terms. 
     *
     *  @return the cumulative terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getCumulativeTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the assessment part <code> Id </code> query terms. 
     *
     *  @return the assessment parent <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAppliedAssessmentPartIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the assessment part query terms. 
     *
     *  @return the assessment part terms 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartQueryInspector[] getAppliedAssessmentPartTerms() {
        return (new org.osid.assessment.authoring.AssessmentPartQueryInspector[0]);
    }


    /**
     *  Gets the bank <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBankIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the bank query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.assessment.BankQueryInspector[] getBankTerms() {
        return (new org.osid.assessment.BankQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given sequence rule query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a sequence rule implementing the requested record.
     *
     *  @param sequenceRuleRecordType a sequence rule record type
     *  @return the sequence rule query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(sequenceRuleRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.authoring.records.SequenceRuleQueryInspectorRecord getSequenceRuleQueryInspectorRecord(org.osid.type.Type sequenceRuleRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.authoring.records.SequenceRuleQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(sequenceRuleRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(sequenceRuleRecordType + " is not supported");
    }


    /**
     *  Adds a record to this sequence rule query. 
     *
     *  @param sequenceRuleQueryInspectorRecord sequence rule query inspector
     *         record
     *  @param sequenceRuleRecordType sequenceRule record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addSequenceRuleQueryInspectorRecord(org.osid.assessment.authoring.records.SequenceRuleQueryInspectorRecord sequenceRuleQueryInspectorRecord, 
                                                   org.osid.type.Type sequenceRuleRecordType) {

        addRecordType(sequenceRuleRecordType);
        nullarg(sequenceRuleRecordType, "sequence rule record type");
        this.records.add(sequenceRuleQueryInspectorRecord);        
        return;
    }
}
