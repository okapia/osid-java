//
// AbstractAdapterCourseLookupSession.java
//
//    A Course lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.course.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Course lookup session adapter.
 */

public abstract class AbstractAdapterCourseLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.course.CourseLookupSession {

    private final org.osid.course.CourseLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterCourseLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterCourseLookupSession(org.osid.course.CourseLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code CourseCatalog/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code CourseCatalog Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.session.getCourseCatalogId());
    }


    /**
     *  Gets the {@code CourseCatalog} associated with this session.
     *
     *  @return the {@code CourseCatalog} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCourseCatalog());
    }


    /**
     *  Tests if this user can perform {@code Course} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupCourses() {
        return (this.session.canLookupCourses());
    }


    /**
     *  A complete view of the {@code Course} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCourseView() {
        this.session.useComparativeCourseView();
        return;
    }


    /**
     *  A complete view of the {@code Course} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCourseView() {
        this.session.usePlenaryCourseView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include courses in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.session.useFederatedCourseCatalogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.session.useIsolatedCourseCatalogView();
        return;
    }
    

    /**
     *  Only active courses are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveCourseView() {
        this.session.useActiveCourseView();
        return;
    }


    /**
     *  Active and inactive courses are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusCourseView() {
        this.session.useAnyStatusCourseView();
        return;
    }
    
     
    /**
     *  Gets the {@code Course} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Course} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Course} and
     *  retained for compatibility.
     *
     *  In active mode, courses are returned that are currently
     *  active. In any status mode, active and inactive courses
     *  are returned.
     *
     *  @param courseId {@code Id} of the {@code Course}
     *  @return the course
     *  @throws org.osid.NotFoundException {@code courseId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code courseId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.Course getCourse(org.osid.id.Id courseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCourse(courseId));
    }


    /**
     *  Gets a {@code CourseList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  courses specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Courses} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, courses are returned that are currently
     *  active. In any status mode, active and inactive courses
     *  are returned.
     *
     *  @param  courseIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Course} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code courseIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseList getCoursesByIds(org.osid.id.IdList courseIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCoursesByIds(courseIds));
    }


    /**
     *  Gets a {@code CourseList} corresponding to the given
     *  course genus {@code Type} which does not include
     *  courses of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  courses or an error results. Otherwise, the returned list
     *  may contain only those courses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, courses are returned that are currently
     *  active. In any status mode, active and inactive courses
     *  are returned.
     *
     *  @param  courseGenusType a course genus type 
     *  @return the returned {@code Course} list
     *  @throws org.osid.NullArgumentException
     *          {@code courseGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseList getCoursesByGenusType(org.osid.type.Type courseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCoursesByGenusType(courseGenusType));
    }


    /**
     *  Gets a {@code CourseList} corresponding to the given
     *  course genus {@code Type} and include any additional
     *  courses with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  courses or an error results. Otherwise, the returned list
     *  may contain only those courses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, courses are returned that are currently
     *  active. In any status mode, active and inactive courses
     *  are returned.
     *
     *  @param  courseGenusType a course genus type 
     *  @return the returned {@code Course} list
     *  @throws org.osid.NullArgumentException
     *          {@code courseGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseList getCoursesByParentGenusType(org.osid.type.Type courseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCoursesByParentGenusType(courseGenusType));
    }


    /**
     *  Gets a {@code CourseList} containing the given
     *  course record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  courses or an error results. Otherwise, the returned list
     *  may contain only those courses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, courses are returned that are currently
     *  active. In any status mode, active and inactive courses
     *  are returned.
     *
     *  @param  courseRecordType a course record type 
     *  @return the returned {@code Course} list
     *  @throws org.osid.NullArgumentException
     *          {@code courseRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseList getCoursesByRecordType(org.osid.type.Type courseRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCoursesByRecordType(courseRecordType));
    }


    /**
     *  Gets a <code> CourseList </code> by the given number.
     *  
     *  In plenary mode, the returned list contains all known courses
     *  or an error results. Otherwise, the returned list may contain
     *  only those courses that are accessible through this session.
     *  
     *  In active mode, courses are returned that are currently
     *  active. In any status mode, active and inactive courses are
     *  returned.
     *
     *  @param  number a course number 
     *  @return the returned <code> CourseList </code> list 
     *  @throws org.osid.NullArgumentException <code> number </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.CourseList getCoursesByNumber(String number)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCoursesByNumber(number));
    }


    /**
     *  Gets all {@code Courses}. 
     *
     *  In plenary mode, the returned list contains all known
     *  courses or an error results. Otherwise, the returned list
     *  may contain only those courses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, courses are returned that are currently
     *  active. In any status mode, active and inactive courses
     *  are returned.
     *
     *  @return a list of {@code Courses} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseList getCourses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCourses());
    }
}
