//
// AbstractImmutableCheck.java
//
//     Wraps a mutable Check to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.rules.check.check.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Check</code> to hide modifiers. This
 *  wrapper provides an immutized Check from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying check whose state changes are visible.
 */

public abstract class AbstractImmutableCheck
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRule
    implements org.osid.rules.check.Check {

    private final org.osid.rules.check.Check check;


    /**
     *  Constructs a new <code>AbstractImmutableCheck</code>.
     *
     *  @param check the check to immutablize
     *  @throws org.osid.NullArgumentException <code>check</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableCheck(org.osid.rules.check.Check check) {
        super(check);
        this.check = check;
        return;
    }


    /**
     *  Tests if this check is a placeholder check that always fails
     *  when evaluated. A failure check can be used to block certain
     *  conditions specified in an <code> Instruction. </code> If
     *  <code> isFailCheck() </code> is <code> true, </code> then
     *  <code> isTimeCheckByDate(), </code> <code>
     *  isTimeCheckByEvent(), </code> <code>
     *  isTimeCheckByCyclicEvent(), </code> <code> isHoldCheck(),
     *  isInquiryCheck(), </code> and <code> isProcessCheck() </code>
     *  must be <code> false. </code>
     *
     *  @return <code> true </code> if this is a fail check, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isFailCheck() {
        return (this.check.isFailCheck());
    }


    /**
     *  Tests if this check is for a time check specified by a
     *  date. If <code> isTimeCheckByDate() </code> is <code> true,
     *  </code> then <code> isFailCheck(), </code> <code>
     *  isTimeCheckByEvent(), </code> <code>
     *  isTimeCheckByCyclicEvent(), </code> <code> isHoldCheck(),
     *  </code> <code> isInquiryCheck(), </code> and <code>
     *  isProcessCheck() </code> must be <code> false. </code>
     *
     *  @return <code> true </code> if this check is for a time specified by 
     *          date, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean isTimeCheckByDate() {
        return (this.check.isTimeCheckByDate());
    }


    /**
     *  Gets the time check start date. The check passes if the current
     *  time is within the time interval inclusive. If the start time
     *  of the interval is undefined, then the check for the start
     *  time always passes. If the end time of the interval is
     *  undefined, then the check for the deadline always passes.
     *
     *  @return the start date
     *  @throws org.osid.IllegalStateException <code> isTimeCheckByDate() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getTimeCheckStartDate() {
        return (this.check.getTimeCheckStartDate());
    }


    /**
     *  Gets the time check end date. The check passes if the current
     *  time is within the time interval inclusive. If the end time
     *  of the interval is undefined, then the check for the end
     *  time always passes. If the end time of the interval is
     *  undefined, then the check for the deadline always passes.
     *
     *  @return the end date
     *  @throws org.osid.IllegalStateException <code> isTimeCheckByDate() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getTimeCheckEndDate() {
        return (this.check.getTimeCheckEndDate());
    }


    /**
     *  Tests if this check is for a time check specified by an
     *  event. The starting and ending times of the event are used as
     *  a time interval to perform the time check. If <code>
     *  isTimeCheckByEvent() </code> is <code> true, </code> then
     *  <code> isFailCheck(), </code> <code> isTimeCheckByDate(),
     *  </code> <code> isTimeCheckByCyclicEvent(), </code> <code>
     *  isHoldCheck(), </code> <code> isInquiryCheck(), </code> and
     *  <code> isProcessCheck() </code> must be <code> false. </code>
     *
     *  @return <code> true </code> if this check is for a time speciifed by 
     *          event, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean isTimeCheckByEvent() {
        return (this.check.isTimeCheckByEvent());
    }


    /**
     *  Gets the time check event <code> Id. </code> 
     *
     *  @return the event <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isTimeCheckByEvent() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getTimeCheckEventId() {
        return (this.check.getTimeCheckEventId());
    }


    /**
     *  Gets the time check event. 
     *
     *  @return the event 
     *  @throws org.osid.IllegalStateException <code> isTimeCheckByEvent() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.Event getTimeCheckEvent()
        throws org.osid.OperationFailedException {

        return (this.check.getTimeCheckEvent());
    }


    /**
     *  Tests if this check is for a time check specified by a cyclic
     *  event.  The time check is performed using the starting and
     *  ending dates of the derived event. If the event is a rceurring
     *  event, the time must be within the starting and ending dates
     *  of at least one of the events in the series. If <code>
     *  isTimeCheckByCyclicEvent() </code> is <code> true, </code>
     *  then <code> isFailCheck(), </code> <code> isTimeCheckByDate(),
     *  </code> <code> isTimeCheckByEvent(), </code> <code>
     *  isHoldCheck(), isInquiryCheck(), </code> and <code>
     *  isProcessCheck() </code> must be <code> false. </code>
     *
     *  @return <code> true </code> if this check is for a time check 
     *          specified by cyclic event, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean isTimeCheckByCyclicEvent() {
        return (this.check.isTimeCheckByCyclicEvent());
    }


    /**
     *  Gets the time check cyclic event <code> Id. </code> 
     *
     *  @return the cyclic event <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> 
     *          isTimeCheckByCyclicEvent() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getTimeCheckCyclicEventId() {
        return (this.check.getTimeCheckCyclicEventId());
    }


    /**
     *  Gets the time check cyclic event. 
     *
     *  @return the cyclic event 
     *  @throws org.osid.IllegalStateException <code> 
     *          isTimeCheckByCyclicEvent() </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEvent getTimeCheckCyclicEvent()
        throws org.osid.OperationFailedException {

        return (this.check.getTimeCheckCyclicEvent());
    }


    /**
     *  Tests if this check is for a hold service block. If a block
     *  exists for the agent being checked, the check fails. If <code>
     *  isHoldCheck() </code> is <code> true, </code> then <code>
     *  isFailCheck(), </code> <code> isTimeCheckByDate(), </code>
     *  <code> isTimeCheckByEvent(), </code> <code>
     *  isTimeCheckByCyclicEvent(), </code> <code> isInquiryCheck(),
     *  </code> and <code> isProcessCheck() </code> must be <code>
     *  false. </code>
     *
     *  @return <code> true </code> if this check is for a block,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean isHoldCheck() {
        return (this.check.isHoldCheck());
    }


    /**
     *  Gets the <code> Block Id </code> for this check, 
     *
     *  @return the <code> Block Id </code> 
     *  @throws org.osid.IllegalStateException <code> isHoldCheck() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getHoldCheckBlockId() {
        return (this.check.getHoldCheckBlockId());
    }


    /**
     *  Gets the <code> Block </code> for this check. 
     *
     *  @return the <code> Block </code> 
     *  @throws org.osid.IllegalStateException <code> isHoldCheck() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.hold.Block getHoldCheckBlock()
        throws org.osid.OperationFailedException {

        return (this.check.getHoldCheckBlock());
    }


    /**
     *  Tests if this check is for a hold service block. If a block exists for 
     *  the agent being checked, the check fails. If <code> isInquiryCheck() 
     *  </code> is <code> true, </code> then <code> isFailCheck(), </code> 
     *  <code> isTimeCheckByDate(), </code> <code> isTimeCheckByEvent(), 
     *  </code> <code> isTimeCheckByCyclicEvent(), </code> <code> 
     *  isHoldCheck(), </code> and <code> isProcessCheck() </code> must be 
     *  <code> false. </code> 
     *
     *  @return <code> true </code> if this check is for a block, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isInquiryCheck() {
        return (this.check.isInquiryCheck());
    }


    /**
     *  Gets the <code> Audit Id </code> for this check, 
     *
     *  @return the <code> Block Id </code> 
     *  @throws org.osid.IllegalStateException <code> isInquiryCheck() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getInquiryCheckAuditId() {
        return (this.check.getInquiryCheckAuditId());
    }


    /**
     *  Gets the <code> Audit </code> for this check. 
     *
     *  @return the <code> Audit </code> 
     *  @throws org.osid.IllegalStateException <code> isInquiryCheck()
     *          </code> is <code> false </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.inquiry.Audit getInquiryCheckAudit()
        throws org.osid.OperationFailedException {

        return (this.check.getInquiryCheckAudit());
    }


    /**
     *  Tests if this check is for a another agenda. The agenda for
     *  the specified agenda is retrieved and processed before
     *  continuing with the checks in this agenda. If <code>
     *  isProcessCheck() </code> is <code> true, </code> then <code>
     *  isFailCheck(), </code> <code> isTimeCheckByDate(), </code>
     *  <code> isTimeCheckByEvent(), </code> <code>
     *  isTimeCheckByCyclicEvent(), </code> <code> isHoldCheck(),
     *  </code> and <code> isInquiryCheck() </code> must be <code>
     *  false.  </code>
     *
     *  @return <code> true </code> if this check is for another agenda, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean isProcessCheck() {
        return (this.check.isProcessCheck());
    }


    /**
     *  Gets the <code> Agenda Id </code> for this check. 
     *
     *  @return the <code> Block Id </code> 
     *  @throws org.osid.IllegalStateException <code> isProcessCheck() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getProcessCheckAgendaId() {
        return (this.check.getProcessCheckAgendaId());
    }


    /**
     *  Gets the <code> Agenda </code> for this check. 
     *
     *  @return the <code> Agenda </code> 
     *  @throws org.osid.IllegalStateException <code> isProcessCheck() </code> 
     *          is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.rules.check.Agenda getProcessCheckAgenda()
        throws org.osid.OperationFailedException {

        return (this.check.getProcessCheckAgenda());
    }


    /**
     *  Gets the check record corresponding to the given <code> Check </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> checkRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(checkRecordType) </code> is <code> true </code> . 
     *
     *  @param  checkRecordType the type of check record to retrieve 
     *  @return the check record 
     *  @throws org.osid.NullArgumentException <code> checkRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(checkRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.records.CheckRecord getCheckRecord(org.osid.type.Type checkRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.check.getCheckRecord(checkRecordType));
    }
}

