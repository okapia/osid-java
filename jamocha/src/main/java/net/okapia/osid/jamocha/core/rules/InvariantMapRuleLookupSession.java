//
// InvariantMapRuleLookupSession
//
//    Implements a Rule lookup service backed by a fixed collection of
//    rules.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.rules;


/**
 *  Implements a Rule lookup service backed by a fixed
 *  collection of rules. The rules are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapRuleLookupSession
    extends net.okapia.osid.jamocha.core.rules.spi.AbstractMapRuleLookupSession
    implements org.osid.rules.RuleLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapRuleLookupSession</code> with no
     *  rules.
     *  
     *  @param engine the engine
     *  @throws org.osid.NullArgumnetException {@code engine} is
     *          {@code null}
     */

    public InvariantMapRuleLookupSession(org.osid.rules.Engine engine) {
        setEngine(engine);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapRuleLookupSession</code> with a single
     *  rule.
     *  
     *  @param engine the engine
     *  @param rule a single rule
     *  @throws org.osid.NullArgumentException {@code engine} or
     *          {@code rule} is <code>null</code>
     */

      public InvariantMapRuleLookupSession(org.osid.rules.Engine engine,
                                               org.osid.rules.Rule rule) {
        this(engine);
        putRule(rule);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapRuleLookupSession</code> using an array
     *  of rules.
     *  
     *  @param engine the engine
     *  @param rules an array of rules
     *  @throws org.osid.NullArgumentException {@code engine} or
     *          {@code rules} is <code>null</code>
     */

      public InvariantMapRuleLookupSession(org.osid.rules.Engine engine,
                                               org.osid.rules.Rule[] rules) {
        this(engine);
        putRules(rules);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapRuleLookupSession</code> using a
     *  collection of rules.
     *
     *  @param engine the engine
     *  @param rules a collection of rules
     *  @throws org.osid.NullArgumentException {@code engine} or
     *          {@code rules} is <code>null</code>
     */

      public InvariantMapRuleLookupSession(org.osid.rules.Engine engine,
                                               java.util.Collection<? extends org.osid.rules.Rule> rules) {
        this(engine);
        putRules(rules);
        return;
    }
}
