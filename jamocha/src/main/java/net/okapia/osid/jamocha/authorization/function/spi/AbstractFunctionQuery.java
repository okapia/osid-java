//
// AbstractFunctionQuery.java
//
//     A template for making a Function Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authorization.function.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for functions.
 */

public abstract class AbstractFunctionQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleQuery
    implements org.osid.authorization.FunctionQuery {

    private final java.util.Collection<org.osid.authorization.records.FunctionQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the qualifier hierarchy <code> Id </code> for this query. 
     *
     *  @param  qualifierHierarchyId a hierarchy <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> qualifierHierarchyId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchQualifierHierarchyId(org.osid.id.Id qualifierHierarchyId, 
                                          boolean match) {
        return;
    }


    /**
     *  Clears the qualifier hierarchy <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearQualifierHierarchyIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> HierarchyQuery </code> is available. 
     *
     *  @return <code> true </code> if a qualifier hierarchy query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQualifierHierarchyQuery() {
        return (false);
    }


    /**
     *  Gets the query for a qualifier hierarchy. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the qualifier hierarchy query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierHierarchyQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyQuery getQualifierHierarchyQuery() {
        throw new org.osid.UnimplementedException("supportsQualifierHierarchyQuery() is false");
    }


    /**
     *  Matches functions that have any qualifier hierarchy. 
     *
     *  @param  match <code> true </code> to match functions with any 
     *          qualifier hierarchy, <code> false </code> to match functions 
     *          with no qualifier hierarchy 
     */

    @OSID @Override
    public void matchAnyQualifierHierarchy(boolean match) {
        return;
    }


    /**
     *  Clears the qualifier hierarchy query terms. 
     */

    @OSID @Override
    public void clearQualifierHierarchyTerms() {
        return;
    }


    /**
     *  Sets the authorization <code> Id </code> for this query. 
     *
     *  @param  authorizationId an authorization <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> authorizationId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchAuthorizationId(org.osid.id.Id authorizationId, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the authorization <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAuthorizationIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AuthorizationQuery </code> is available. 
     *
     *  @return <code> true </code> if an authorization query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizationQuery() {
        return (false);
    }


    /**
     *  Gets the query for an authorization. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the authorization query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationQuery getAuthorizationQuery() {
        throw new org.osid.UnimplementedException("supportsAuthorizationQuery() is false");
    }


    /**
     *  Matches functions that have any authorization mapping. 
     *
     *  @param  match <code> true </code> to match functions with any 
     *          authorization mapping, <code> false </code> to match functions 
     *          with no authorization mapping 
     */

    @OSID @Override
    public void matchAnyAuthorization(boolean match) {
        return;
    }


    /**
     *  Clears the authorization query terms. 
     */

    @OSID @Override
    public void clearAuthorizationTerms() {
        return;
    }


    /**
     *  Sets the vault <code> Id </code> for this query. 
     *
     *  @param  vaultId a vault <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchVaultId(org.osid.id.Id vaultId, boolean match) {
        return;
    }


    /**
     *  Clears the vault <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearVaultIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> VaultQuery </code> is available. 
     *
     *  @return <code> true </code> if a vault query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVaultQuery() {
        return (false);
    }


    /**
     *  Gets the query for a vault. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the vault query 
     *  @throws org.osid.UnimplementedException <code> supportsVaultQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.VaultQuery getVaultQuery() {
        throw new org.osid.UnimplementedException("supportsVaultQuery() is false");
    }


    /**
     *  Clears the vault query terms. 
     */

    @OSID @Override
    public void clearVaultTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given function query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a function implementing the requested record.
     *
     *  @param functionRecordType a function record type
     *  @return the function query record
     *  @throws org.osid.NullArgumentException
     *          <code>functionRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(functionRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.authorization.records.FunctionQueryRecord getFunctionQueryRecord(org.osid.type.Type functionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authorization.records.FunctionQueryRecord record : this.records) {
            if (record.implementsRecordType(functionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(functionRecordType + " is not supported");
    }


    /**
     *  Adds a record to this function query. 
     *
     *  @param functionQueryRecord function query record
     *  @param functionRecordType function record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addFunctionQueryRecord(org.osid.authorization.records.FunctionQueryRecord functionQueryRecord, 
                                          org.osid.type.Type functionRecordType) {

        addRecordType(functionRecordType);
        nullarg(functionQueryRecord, "function query record");
        this.records.add(functionQueryRecord);        
        return;
    }
}
