//
// ActionGroupBatchFormFilterList.java
//
//     Implements a filtering ActionGroupBatchFormList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.control.batch.actiongroupbatchform;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filtering ActionGroupBatchFormList.
 */

public final class ActionGroupBatchFormFilterList
    extends net.okapia.osid.jamocha.inline.filter.control.batch.actiongroupbatchform.spi.AbstractActionGroupBatchFormFilterList
    implements org.osid.control.batch.ActionGroupBatchFormList,
               ActionGroupBatchFormFilter {

    private final ActionGroupBatchFormFilter filter;


    /**
     *  Creates a new <code>ActionGroupBatchFormFilterList</code>.
     *
     *  @param filter an inline query filter
     *  @param list an <code>ActionGroupBatchFormList</code>
     *  @throws org.osid.NullArgumentException <code>filter</code> or
     *          <code>list</code> is <code>null</code>
     */

    public ActionGroupBatchFormFilterList(ActionGroupBatchFormFilter filter, org.osid.control.batch.ActionGroupBatchFormList list) {
        super(list);

        nullarg(filter, "inline query filter");
        this.filter = filter;

        return;
    }    

    
    /**
     *  Filters ActionGroupBatchForms.
     *
     *  @param actionGroupBatchForm the action group batch form to filter
     *  @return <code>true</code> if the action group batch form passes the filter,
     *          <code>false</code> if the action group batch form should be filtered
     */

    @Override
    public boolean pass(org.osid.control.batch.ActionGroupBatchForm actionGroupBatchForm) {
        return (this.filter.pass(actionGroupBatchForm));
    }
}
