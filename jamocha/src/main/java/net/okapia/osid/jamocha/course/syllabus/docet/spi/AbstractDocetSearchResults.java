//
// AbstractDocetSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.syllabus.docet.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractDocetSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.course.syllabus.DocetSearchResults {

    private org.osid.course.syllabus.DocetList docets;
    private final org.osid.course.syllabus.DocetQueryInspector inspector;
    private final java.util.Collection<org.osid.course.syllabus.records.DocetSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractDocetSearchResults.
     *
     *  @param docets the result set
     *  @param docetQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>docets</code>
     *          or <code>docetQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractDocetSearchResults(org.osid.course.syllabus.DocetList docets,
                                            org.osid.course.syllabus.DocetQueryInspector docetQueryInspector) {
        nullarg(docets, "docets");
        nullarg(docetQueryInspector, "docet query inspectpr");

        this.docets = docets;
        this.inspector = docetQueryInspector;

        return;
    }


    /**
     *  Gets the docet list resulting from a search.
     *
     *  @return a docet list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetList getDocets() {
        if (this.docets == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.course.syllabus.DocetList docets = this.docets;
        this.docets = null;
	return (docets);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.course.syllabus.DocetQueryInspector getDocetQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  docet search record <code> Type. </code> This method must
     *  be used to retrieve a docet implementing the requested
     *  record.
     *
     *  @param docetSearchRecordType a docet search 
     *         record type 
     *  @return the docet search
     *  @throws org.osid.NullArgumentException
     *          <code>docetSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(docetSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.syllabus.records.DocetSearchResultsRecord getDocetSearchResultsRecord(org.osid.type.Type docetSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.course.syllabus.records.DocetSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(docetSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(docetSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record docet search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addDocetRecord(org.osid.course.syllabus.records.DocetSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "docet record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
