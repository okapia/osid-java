//
// AbstractAssemblyStateQuery.java
//
//     A StateQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.process.state.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A StateQuery that stores terms.
 */

public abstract class AbstractAssemblyStateQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.process.StateQuery,
               org.osid.process.StateQueryInspector,
               org.osid.process.StateSearchOrder {

    private final java.util.Collection<org.osid.process.records.StateQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.process.records.StateQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.process.records.StateSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyStateQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyStateQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the process <code> Id </code> for this query. 
     *
     *  @param  processId the process <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> processId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProcessId(org.osid.id.Id processId, boolean match) {
        getAssembler().addIdTerm(getProcessIdColumn(), processId, match);
        return;
    }


    /**
     *  Clears the process <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearProcessIdTerms() {
        getAssembler().clearTerms(getProcessIdColumn());
        return;
    }


    /**
     *  Gets the process <code> Id </code> terms. 
     *
     *  @return the process <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProcessIdTerms() {
        return (getAssembler().getIdTerms(getProcessIdColumn()));
    }


    /**
     *  Gets the ProcessId column name.
     *
     * @return the column name
     */

    protected String getProcessIdColumn() {
        return ("process_id");
    }


    /**
     *  Tests if a <code> ProcessQuery </code> is available. 
     *
     *  @return <code> true </code> if a process query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a process. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the process query 
     *  @throws org.osid.UnimplementedException <code> supportsProcessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.ProcessQuery getProcessQuery() {
        throw new org.osid.UnimplementedException("supportsProcessQuery() is false");
    }


    /**
     *  Clears the process terms. 
     */

    @OSID @Override
    public void clearProcessTerms() {
        getAssembler().clearTerms(getProcessColumn());
        return;
    }


    /**
     *  Gets the process terms. 
     *
     *  @return the process terms 
     */

    @OSID @Override
    public org.osid.process.ProcessQueryInspector[] getProcessTerms() {
        return (new org.osid.process.ProcessQueryInspector[0]);
    }


    /**
     *  Gets the Process column name.
     *
     * @return the column name
     */

    protected String getProcessColumn() {
        return ("process");
    }


    /**
     *  Tests if this state supports the given record
     *  <code>Type</code>.
     *
     *  @param  stateRecordType a state record type 
     *  @return <code>true</code> if the stateRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>stateRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type stateRecordType) {
        for (org.osid.process.records.StateQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(stateRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  stateRecordType the state record type 
     *  @return the state query record 
     *  @throws org.osid.NullArgumentException
     *          <code>stateRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(stateRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.process.records.StateQueryRecord getStateQueryRecord(org.osid.type.Type stateRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.process.records.StateQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(stateRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(stateRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  stateRecordType the state record type 
     *  @return the state query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>stateRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(stateRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.process.records.StateQueryInspectorRecord getStateQueryInspectorRecord(org.osid.type.Type stateRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.process.records.StateQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(stateRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(stateRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param stateRecordType the state record type
     *  @return the state search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>stateRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(stateRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.process.records.StateSearchOrderRecord getStateSearchOrderRecord(org.osid.type.Type stateRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.process.records.StateSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(stateRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(stateRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this state. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param stateQueryRecord the state query record
     *  @param stateQueryInspectorRecord the state query inspector
     *         record
     *  @param stateSearchOrderRecord the state search order record
     *  @param stateRecordType state record type
     *  @throws org.osid.NullArgumentException
     *          <code>stateQueryRecord</code>,
     *          <code>stateQueryInspectorRecord</code>,
     *          <code>stateSearchOrderRecord</code> or
     *          <code>stateRecordTypestate</code> is
     *          <code>null</code>
     */
            
    protected void addStateRecords(org.osid.process.records.StateQueryRecord stateQueryRecord, 
                                      org.osid.process.records.StateQueryInspectorRecord stateQueryInspectorRecord, 
                                      org.osid.process.records.StateSearchOrderRecord stateSearchOrderRecord, 
                                      org.osid.type.Type stateRecordType) {

        addRecordType(stateRecordType);

        nullarg(stateQueryRecord, "state query record");
        nullarg(stateQueryInspectorRecord, "state query inspector record");
        nullarg(stateSearchOrderRecord, "state search odrer record");

        this.queryRecords.add(stateQueryRecord);
        this.queryInspectorRecords.add(stateQueryInspectorRecord);
        this.searchOrderRecords.add(stateSearchOrderRecord);
        
        return;
    }
}
