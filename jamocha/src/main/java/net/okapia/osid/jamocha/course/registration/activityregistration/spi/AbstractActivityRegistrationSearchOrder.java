//
// AbstractActivityRegistrationSearchOdrer.java
//
//     Defines an ActivityRegistrationSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.registration.activityregistration.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an {@code ActivityRegistrationSearchOrder}.
 */

public abstract class AbstractActivityRegistrationSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipSearchOrder
    implements org.osid.course.registration.ActivityRegistrationSearchOrder {

    private final java.util.Collection<org.osid.course.registration.records.ActivityRegistrationSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by registration. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRegistration(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a regiistration search order is available. 
     *
     *  @return <code> true </code> if a registration search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRegistrationSearchOrder() {
        return (false);
    }


    /**
     *  Gets the registration search order. 
     *
     *  @return the registration search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRegistrationSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationSearchOrder getRegistrationSearchOrder() {
        throw new org.osid.UnimplementedException("supportsRegistrationSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by activity. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByActivity(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an activity search order is available. 
     *
     *  @return <code> true </code> if an activity search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivitySearchOrder() {
        return (false);
    }


    /**
     *  Gets the activity search order. 
     *
     *  @return the activity search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivitySearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivitySearchOrder getActivitySearchOrder() {
        throw new org.osid.UnimplementedException("supportsActivitySearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by student. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStudent(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a student search order is available. 
     *
     *  @return <code> true </code> if a student search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStudentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the student search order. 
     *
     *  @return the student search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStudentSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getStudentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsStudentSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  activityRegistrationRecordType an activity registration record type 
     *  @return {@code true} if the activityRegistrationRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code activityRegistrationRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type activityRegistrationRecordType) {
        for (org.osid.course.registration.records.ActivityRegistrationSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(activityRegistrationRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  activityRegistrationRecordType the activity registration record type 
     *  @return the activity registration search order record
     *  @throws org.osid.NullArgumentException
     *          {@code activityRegistrationRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(activityRegistrationRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.course.registration.records.ActivityRegistrationSearchOrderRecord getActivityRegistrationSearchOrderRecord(org.osid.type.Type activityRegistrationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.registration.records.ActivityRegistrationSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(activityRegistrationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(activityRegistrationRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this activity registration. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param activityRegistrationRecord the activity registration search odrer record
     *  @param activityRegistrationRecordType activity registration record type
     *  @throws org.osid.NullArgumentException
     *          {@code activityRegistrationRecord} or
     *          {@code activityRegistrationRecordTypeactivityRegistration} is
     *          {@code null}
     */
            
    protected void addActivityRegistrationRecord(org.osid.course.registration.records.ActivityRegistrationSearchOrderRecord activityRegistrationSearchOrderRecord, 
                                     org.osid.type.Type activityRegistrationRecordType) {

        addRecordType(activityRegistrationRecordType);
        this.records.add(activityRegistrationSearchOrderRecord);
        
        return;
    }
}
