//
// AbstractInstallationContent.java
//
//     Defines an InstallationContent builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.installation.installationcontent.spi;


/**
 *  Defines an <code>InstallationContent</code> builder.
 */

public abstract class AbstractInstallationContentBuilder<T extends AbstractInstallationContentBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.installation.installationcontent.InstallationContentMiter installationContent;


    /**
     *  Constructs a new <code>AbstractInstallationContentBuilder</code>.
     *
     *  @param installationContent the installation content to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractInstallationContentBuilder(net.okapia.osid.jamocha.builder.installation.installationcontent.InstallationContentMiter installationContent) {
        super(installationContent);
        this.installationContent = installationContent;
        return;
    }


    /**
     *  Builds the installation content.
     *
     *  @return the new installation content
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.NullArgumentException <code>installationContent</code>
     *          is <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.installation.InstallationContent build() {
        (new net.okapia.osid.jamocha.builder.validator.installation.installationcontent.InstallationContentValidator(getValidations())).validate(this.installationContent);
        return (new net.okapia.osid.jamocha.builder.installation.installationcontent.ImmutableInstallationContent(this.installationContent));
    }


    /**
     *  Gets the installation content. This method is used to get the miter
     *  interface for further updates. Use <code>build()</code> to
     *  finalize and validate construction.
     *
     *  @return the new installationContent
     */

    @Override
    public net.okapia.osid.jamocha.builder.installation.installationcontent.InstallationContentMiter getMiter() {
        return (this.installationContent);
    }


    /**
     *  Sets the package.
     *
     *  @param pkg a package
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>pkg</code> is <code>null</code>
     */

    public T pkg(org.osid.installation.Package pkg) {
        getMiter().setPackage(pkg);
        return (self());
    }


    /**
     *  Sets the data length.
     *
     *  @param dataLength a data length
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>dataLength</code> is <code>null</code>
     */

    public T dataLength(long dataLength) {
        getMiter().setDataLength(dataLength);
        return (self());
    }


    /**
     *  Sets the data.
     *
     *  @param data the data
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>data</code> is
     *          <code>null</code>
     */

    public T datum(java.nio.ByteBuffer data) {
        getMiter().setData(data);
        return (self());
    }


    /**
     *  Adds an InstallationContent record.
     *
     *  @param record an installation content record
     *  @param recordType the type of installation content record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.installation.records.InstallationContentRecord record, org.osid.type.Type recordType) {
        getMiter().addInstallationContentRecord(record, recordType);
        return (self());
    }
}       


