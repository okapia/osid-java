//
// AbstractFamilyQuery.java
//
//     A template for making a Family Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.relationship.family.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for families.
 */

public abstract class AbstractFamilyQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.relationship.FamilyQuery {

    private final java.util.Collection<org.osid.relationship.records.FamilyQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches a relationship <code> Id. </code> 
     *
     *  @param  relationshipId a relationship <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> relationshipId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchRelationshipId(org.osid.id.Id relationshipId, 
                                    boolean match) {
        return;
    }


    /**
     *  Clears the relationship <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRelationshipIdTerms() {
        return;
    }


    /**
     *  Tests if a relationship query is available. 
     *
     *  @return <code> true </code> if a relationship query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelationshipQuery() {
        return (false);
    }


    /**
     *  Gets the query interface for a relationship. 
     *
     *  @return the relationship query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipQuery getRelationshipQuery() {
        throw new org.osid.UnimplementedException("supportsRelationshipQuery() is false");
    }


    /**
     *  Matches families with any relationship. 
     *
     *  @param  match <code> true </code> to match families with any 
     *          relationship, <code> false </code> to match families with no 
     *          relationship 
     */

    @OSID @Override
    public void matchAnyRelationship(boolean match) {
        return;
    }


    /**
     *  Clears the relationship terms. 
     */

    @OSID @Override
    public void clearRelationshipTerms() {
        return;
    }


    /**
     *  Sets the family <code> Id </code> for this query to match families 
     *  that have the specified family as an ancestor. 
     *
     *  @param  familyId a family <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> familyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorFamilyId(org.osid.id.Id familyId, boolean match) {
        return;
    }


    /**
     *  Clears the ancestor family <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorFamilyIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> FamilyQuery </code> is available. 
     *
     *  @return <code> true </code> if a family query interface is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorFamilyQuery() {
        return (false);
    }


    /**
     *  Gets the query interface for a family. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the family query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorFamilyQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.FamilyQuery getAncestorFamilyQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorFamilyQuery() is false");
    }


    /**
     *  Matches families with any ancestor. 
     *
     *  @param  match <code> true </code> to match families with any ancestor, 
     *          <code> false </code> to match root families 
     */

    @OSID @Override
    public void matchAnyAncestorFamily(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor family terms. 
     */

    @OSID @Override
    public void clearAncestorFamilyTerms() {
        return;
    }


    /**
     *  Sets the family <code> Id </code> for this query to match families 
     *  that have the specified family as a descednant. 
     *
     *  @param  familyId a family <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> familyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantFamilyId(org.osid.id.Id familyId, boolean match) {
        return;
    }


    /**
     *  Clears the descendant family <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantFamilyIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> FamilyQuery </code> is available. 
     *
     *  @return <code> true </code> if a family query interface is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantFamilyQuery() {
        return (false);
    }


    /**
     *  Gets the query interface for a family. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the family query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantFamilyQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.relationship.FamilyQuery getDescendantFamilyQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantFamilyQuery() is false");
    }


    /**
     *  Matches families with any decendant. 
     *
     *  @param  match <code> true </code> to match families with any 
     *          decendants, <code> false </code> to match leaf families 
     */

    @OSID @Override
    public void matchAnyDescendantFamily(boolean match) {
        return;
    }


    /**
     *  Clears the descendant family terms. 
     */

    @OSID @Override
    public void clearDescendantFamilyTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given family query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a family implementing the requested record.
     *
     *  @param familyRecordType a family record type
     *  @return the family query record
     *  @throws org.osid.NullArgumentException
     *          <code>familyRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(familyRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.relationship.records.FamilyQueryRecord getFamilyQueryRecord(org.osid.type.Type familyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.relationship.records.FamilyQueryRecord record : this.records) {
            if (record.implementsRecordType(familyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(familyRecordType + " is not supported");
    }


    /**
     *  Adds a record to this family query. 
     *
     *  @param familyQueryRecord family query record
     *  @param familyRecordType family record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addFamilyQueryRecord(org.osid.relationship.records.FamilyQueryRecord familyQueryRecord, 
                                          org.osid.type.Type familyRecordType) {

        addRecordType(familyRecordType);
        nullarg(familyQueryRecord, "family query record");
        this.records.add(familyQueryRecord);        
        return;
    }
}
