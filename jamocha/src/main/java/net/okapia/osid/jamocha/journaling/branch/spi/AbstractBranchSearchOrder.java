//
// AbstractBranchSearchOdrer.java
//
//     Defines a BranchSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.journaling.branch.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code BranchSearchOrder}.
 */

public abstract class AbstractBranchSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOperableOsidObjectSearchOrder
    implements org.osid.journaling.BranchSearchOrder {

    private final java.util.Collection<org.osid.journaling.records.BranchSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specified a preference for ordering results by the origin journal 
     *  entry. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByOriginJournalEntry(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a <code> JournalEntrySearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a journal entry search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOriginJournalEntrySearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for the origin journal entry. 
     *
     *  @return the journal entry search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOriginJournalEntrySearchOrder() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntrySearchOrder getOriginJournalEntrySearchOrder() {
        throw new org.osid.UnimplementedException("supportsOriginJournalEntrySearchOrder() is false");
    }


    /**
     *  Specified a preference for ordering results by the latest journal 
     *  entry. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByLatestJournalEntry(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a <code> JournalEntrySearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a journal entry search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLatestJournalEntrySearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for the latest journal entry. 
     *
     *  @return the journal entry search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLatestJournalEntrySearchOrder() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntrySearchOrder getLatestJournalEntrySearchOrder() {
        throw new org.osid.UnimplementedException("supportsLatestJournalEntrySearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  branchRecordType a branch record type 
     *  @return {@code true} if the branchRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code branchRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type branchRecordType) {
        for (org.osid.journaling.records.BranchSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(branchRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  branchRecordType the branch record type 
     *  @return the branch search order record
     *  @throws org.osid.NullArgumentException
     *          {@code branchRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(branchRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.journaling.records.BranchSearchOrderRecord getBranchSearchOrderRecord(org.osid.type.Type branchRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.journaling.records.BranchSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(branchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(branchRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this branch. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param branchRecord the branch search odrer record
     *  @param branchRecordType branch record type
     *  @throws org.osid.NullArgumentException
     *          {@code branchRecord} or
     *          {@code branchRecordTypebranch} is
     *          {@code null}
     */
            
    protected void addBranchRecord(org.osid.journaling.records.BranchSearchOrderRecord branchSearchOrderRecord, 
                                     org.osid.type.Type branchRecordType) {

        addRecordType(branchRecordType);
        this.records.add(branchSearchOrderRecord);
        
        return;
    }
}
