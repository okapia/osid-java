//
// AbstractImmutableScheduleSlot.java
//
//     Wraps a mutable ScheduleSlot to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.calendaring.scheduleslot.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>ScheduleSlot</code> to hide modifiers. This
 *  wrapper provides an immutized ScheduleSlot from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying scheduleSlot whose state changes are visible.
 */

public abstract class AbstractImmutableScheduleSlot
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableContainableOsidObject
    implements org.osid.calendaring.ScheduleSlot {

    private final org.osid.calendaring.ScheduleSlot scheduleSlot;


    /**
     *  Constructs a new <code>AbstractImmutableScheduleSlot</code>.
     *
     *  @param scheduleSlot the schedule slot to immutablize
     *  @throws org.osid.NullArgumentException <code>scheduleSlot</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableScheduleSlot(org.osid.calendaring.ScheduleSlot scheduleSlot) {
        super(scheduleSlot);
        this.scheduleSlot = scheduleSlot;
        return;
    }


    /**
     *  Gets the <code> Ids </code> of the schedule slots included inside this 
     *  one. 
     *
     *  @return the schedules slot <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getScheduleSlotIds() {
        return (this.scheduleSlot.getScheduleSlotIds());
    }


    /**
     *  Gets the schedule slots included inside this one. 
     *
     *  @return the schedule slots 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotList getScheduleSlots()
        throws org.osid.OperationFailedException {

        return (this.scheduleSlot.getScheduleSlots());
    }


    /**
     *  Tests if this schedule has a weekly interval. If <code> true, </code> 
     *  <code> hasFixedInterval() </code> must be <code> false. </code> 
     *
     *  @return <code> true </code> if there is a weekly interval, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasWeeklyInterval() {
        return (this.scheduleSlot.hasWeeklyInterval());
    }


    /**
     *  Gets the weekdays of the schedule. On a Gregorian calendar, Sunday is 
     *  zero. 
     *
     *  @return the weekdays 
     *  @throws org.osid.IllegalStateException <code> hasWeeklyInterval() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public long[] getWeekdays() {
        return (this.scheduleSlot.getWeekdays());
    }


    /**
     *  Tests if this schedule has a weekly interval based on the week of the 
     *  month. This method must be <code> false </code> if <code> 
     *  hasWeeklyInterval() </code> is <code> false. </code> 
     *
     *  @return <code> true </code> if there is a week of month specified, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasWeekOfMonthInterval() {
        return (this.scheduleSlot.hasWeekOfMonthInterval());
    }


    /**
     *  Gets the number of weeks of the interval. 1 is every week. 2 is every 
     *  other week. -1 is every week back in time. 
     *
     *  @return <code> the week interval </code> 
     *  @throws org.osid.IllegalStateException <code> hasWeekdlyInterval() 
     *          </code> is <code> false </code> or <code> 
     *          hasWeekofMonthInterval() </code> is <code> true </code> 
     */

    @OSID @Override
    public long getWeeklyInterval() {
        return (this.scheduleSlot.getWeeklyInterval());
    }


    /**
     *  Gets the week of the month for the interval. 1 is the first week of 
     *  the month. -1 is the last week of the month. 0 is invalid. 
     *
     *  @return <code> the week interval </code> 
     *  @throws org.osid.IllegalStateException <code> hasWeeklyInterval() 
     *          </code> is <code> false </code> or <code> 
     *          hasWeekofMonthInterval() </code> is <code> false </code> 
     */

    @OSID @Override
    public long getWeekOfMonth() {
        return (this.scheduleSlot.getWeekOfMonth());
    }


    /**
     *  Gets the time of this recurring schedule. 
     *
     *  @return the time 
     *  @throws org.osid.IllegalStateException <code> hasWeeklyInterval() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Time getWeekdayTime() {
        return (this.scheduleSlot.getWeekdayTime());
    }


    /**
     *  Tests if this schedule has a fixed time interval. 
     *
     *  @return <code> true </code> if there is a fixed time interval, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasFixedInterval() {
        return (this.scheduleSlot.hasFixedInterval());
    }


    /**
     *  Gets the repeating interval. 
     *
     *  @return the interval 
     *  @throws org.osid.IllegalStateException <code> hasFixedInterval() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getFixedInterval() {
        return (this.scheduleSlot.getFixedInterval());
    }


    /**
     *  Gets the duration.
     *
     *  @return the duration 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getDuration() {
        return (this.scheduleSlot.getDuration());
    }


    /**
     *  Gets the schedule slot record corresponding to the given <code> 
     *  ScheduleSlot </code> record <code> Type. </code> This method is used 
     *  to retrieve an object implementing the requested record. The <code> 
     *  scheduleSlotRecordType </code> may be the <code> Type </code> returned 
     *  in <code> getRecordTypes() </code> or any of its parents in a <code> 
     *  Type </code> hierarchy where <code> 
     *  hasRecordType(scheduleSlotRecordType) </code> is <code> true </code> . 
     *
     *  @param  scheduleSlotRecordType the type of the record to retrieve 
     *  @return the schedule slot record 
     *  @throws org.osid.NullArgumentException <code> scheduleSlotRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(scheduleSlotRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.records.ScheduleSlotRecord getScheduleSlotRecord(org.osid.type.Type scheduleSlotRecordType)
        throws org.osid.OperationFailedException {

        return (this.scheduleSlot.getScheduleSlotRecord(scheduleSlotRecordType));
    }
}

