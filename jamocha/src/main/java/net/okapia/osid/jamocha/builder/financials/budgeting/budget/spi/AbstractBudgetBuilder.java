//
// AbstractBudget.java
//
//     Defines a Budget builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.financials.budgeting.budget.spi;


/**
 *  Defines a <code>Budget</code> builder.
 */

public abstract class AbstractBudgetBuilder<T extends AbstractBudgetBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.financials.budgeting.budget.BudgetMiter budget;


    /**
     *  Constructs a new <code>AbstractBudgetBuilder</code>.
     *
     *  @param budget the budget to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractBudgetBuilder(net.okapia.osid.jamocha.builder.financials.budgeting.budget.BudgetMiter budget) {
        super(budget);
        this.budget = budget;
        return;
    }


    /**
     *  Builds the budget.
     *
     *  @return the new budget
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.financials.budgeting.Budget build() {
        (new net.okapia.osid.jamocha.builder.validator.financials.budgeting.budget.BudgetValidator(getValidations())).validate(this.budget);
        return (new net.okapia.osid.jamocha.builder.financials.budgeting.budget.ImmutableBudget(this.budget));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the budget miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.financials.budgeting.budget.BudgetMiter getMiter() {
        return (this.budget);
    }


    /**
     *  Sets the activity.
     *
     *  @param activity an activity
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>activity</code>
     *          is <code>null</code>
     */

    public T activity(org.osid.financials.Activity activity) {
        getMiter().setActivity(activity);
        return (self());
    }


    /**
     *  Sets the fiscal period.
     *
     *  @param period a fiscal period
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>period</code> is
     *          <code>null</code>
     */

    public T fiscalPeriod(org.osid.financials.FiscalPeriod period) {
        getMiter().setFiscalPeriod(period);
        return (self());
    }


    /**
     *  Adds a Budget record.
     *
     *  @param record a budget record
     *  @param recordType the type of budget record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.financials.budgeting.records.BudgetRecord record, org.osid.type.Type recordType) {
        getMiter().addBudgetRecord(record, recordType);
        return (self());
    }
}       


