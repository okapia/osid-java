//
// AbstractRoutingJobProcessorEnablerLookupSession
//
//     A routing federating adapter for a JobProcessorEnablerLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.resourcing.rules.spi;

import org.osid.binding.java.annotation.OSID;

import net.okapia.osid.torrefacto.collect.IdHashMap;
import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A federating adapter for a JobProcessorEnablerLookupSession. Sessions are
 *  added to this session through <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 *
 *  The routing for Ids and Types are manually set by subclasses of
 *  this session that add child sessions for federation.
 *
 *  Routing for Ids is based on the existince of an Id embedded in the
 *  authority of a job processor enabler Id. The embedded Id is specified when
 *  adding child sessions. Optionally, a list of types for genus and
 *  record may also be mapped to a child session. A service Id for
 *  routing must be unique across all federated sessions while a Type
 *  may map to more than one session.
 *
 *  Lookup methods consult the mapping tables for Ids and Types, and
 *  will fallback to searching all sessions if the job processor enabler is not
 *  found and fallback is true. In the case of record and genus Type
 *  lookups, a fallback will search all child sessions regardless if
 *  any are found.
 */

public abstract class AbstractRoutingJobProcessorEnablerLookupSession
    extends AbstractFederatingJobProcessorEnablerLookupSession
    implements org.osid.resourcing.rules.JobProcessorEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.resourcing.rules.JobProcessorEnablerLookupSession> sessionsById = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.resourcing.rules.JobProcessorEnablerLookupSession>());
    private final MultiMap<org.osid.type.Type, org.osid.resourcing.rules.JobProcessorEnablerLookupSession> sessionsByType = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.resourcing.rules.JobProcessorEnablerLookupSession>());
    private final boolean idFallback;
    private final boolean typeFallback;

    
    /**
     *  Constructs a new
     *  <code>AbstractRoutingJobProcessorEnablerLookupSession</code> with
     *  fallbacks for both Types and Ids.
     */

    protected AbstractRoutingJobProcessorEnablerLookupSession() {
        this.idFallback   = true;
        this.typeFallback = true;
        selectAll();
        return;
    }


    /**
     *  Constructs a new
     *  <code>AbstractRoutingJobProcessorEnablerLookupSession</code>.
     *
     *  @param idFallback <code>true</code> to fall back to searching
     *         all sessions if an Id is not in routing table
     *  @param typeFallback <code>true</code> to fall back to
     *         searching all sessions if a Type is not in routing
     *         table
     */

    protected AbstractRoutingJobProcessorEnablerLookupSession(boolean idFallback, boolean typeFallback) {
        this.idFallback   = idFallback;
        this.typeFallback = typeFallback;
        selectAll();
        return;
    }


    /**
     *  Maps a session to a service Id. Use <code>addSession()</code>
     *  to add a session to this federation.
     *
     *  @param session a session to map
     *  @param serviceId 
     *  @throws org.osid.NullArgumentException <code>session</code> or
     *          <code>serviceId</code> is <code>null</code>
     */

    protected void mapSession(org.osid.resourcing.rules.JobProcessorEnablerLookupSession session, org.osid.id.Id serviceId) {
        nullarg(serviceId, "service Id");
        this.sessionsById.put(serviceId, session);
        return;
    }


    /**
     *  Maps a session to a record or genus Type. Use
     *  <code>addSession()</code> to add a session to this federation.
     *
     *  @param session a session to add
     *  @param type
     *  @throws org.osid.NullArgumentException <code>session</code> or
     *          <code>type</code> is <code>null</code>
     */

    protected void mapSession(org.osid.resourcing.rules.JobProcessorEnablerLookupSession session, org.osid.type.Type type) {
        nullarg(type, "type");
        this.sessionsByType.put(type, session);
        return;
    }


    /**
     *  Unmaps a session from a serviceId.
     *
     *  @param serviceId 
     *  @throws org.osid.NullArgumentException 
     *          <code>serviceId</code> is <code>null</code>
     */

    protected void unmapSession(org.osid.id.Id serviceId) {
        nullarg(serviceId, "service Id");
        this.sessionsById.remove(serviceId);
        return;
    }


    /**
     *  Unmaps a session from both the Id and Type indices.
     *
     *  @param session
     *  @throws org.osid.NullArgumentException 
     *          <code>session</code> is <code>null</code>
     */

    protected void unmapSession(org.osid.resourcing.rules.JobProcessorEnablerLookupSession session) {
        nullarg(session, "session");

        for (org.osid.id.Id id : this.sessionsById.keySet()) {
            if (session.equals(this.sessionsById.get(id))) {
                this.sessionsById.remove(id);
            }
        }

        this.sessionsByType.removeValue(session);

        return;
    }


    /**
     *  Removes a session from this federation.
     *
     *  @param session
     *  @throws org.osid.NullArgumentException 
     *          <code>session</code> is <code>null</code>
     */

    protected void removeSession(org.osid.resourcing.rules.JobProcessorEnablerLookupSession session) {
        unmapSession(session);
        super.removeSession(session);
        return;
    }


    /**
     *  Gets the <code>JobProcessorEnabler</code> specified by its <code>Id</code>. 
     *
     *  @param  jobProcessorEnablerId <code>Id</code> of the
     *          <code>JobProcessorEnabler</code>
     *  @return the job processor enabler
     *  @throws org.osid.NotFoundException <code>jobProcessorEnablerId</code> not 
     *          found in any session
     *  @throws org.osid.NullArgumentException <code>jobProcessorEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnabler getJobProcessorEnabler(org.osid.id.Id jobProcessorEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.resourcing.rules.JobProcessorEnablerLookupSession session = this.sessionsById.get(jobProcessorEnablerId.getAuthority());
        if (session != null) {
            return (session.getJobProcessorEnabler(jobProcessorEnablerId));
        }
        
        if (this.idFallback) {
            return (super.getJobProcessorEnabler(jobProcessorEnablerId));
        }

        throw new org.osid.NotFoundException(jobProcessorEnablerId + " not found");
    }


    /**
     *  Gets a <code>JobProcessorEnablerList</code> corresponding to the given
     *  jobProcessorEnabler genus <code>Type</code> which does not include
     *  job processor enablers of types derived from the specified
     *  <code>Type</code>.
     *  
     *
     *  @param  jobProcessorEnablerGenusType a job processor enabler genus type 
     *  @return the returned <code>JobProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>jobProcessorEnablerGenusType</code> is
     *          <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerList getJobProcessorEnablersByGenusType(org.osid.type.Type jobProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.rules.jobprocessorenabler.FederatingJobProcessorEnablerList ret = getJobProcessorEnablerList();
        java.util.Collection<org.osid.resourcing.rules.JobProcessorEnablerLookupSession> sessions = this.sessionsByType.get(jobProcessorEnablerGenusType);

        if (sessions.size() == 0) {
            if (this.typeFallback) {
                return (super.getJobProcessorEnablersByGenusType(jobProcessorEnablerGenusType));
            } else {
                return (new net.okapia.osid.jamocha.nil.resourcing.rules.jobprocessorenabler.EmptyJobProcessorEnablerList());
            }
        }


        for (org.osid.resourcing.rules.JobProcessorEnablerLookupSession session : sessions) {
            ret.addJobProcessorEnablerList(session.getJobProcessorEnablersByGenusType(jobProcessorEnablerGenusType));
        }
        
        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>JobProcessorEnablerList</code> corresponding to the given
     *  job processor enabler genus <code>Type</code> and include any additional
     *  job processor enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  @param  jobProcessorEnablerGenusType a job processor enabler genus type 
     *  @return the returned <code>JobProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>jobProcessorEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerList getJobProcessorEnablersByParentGenusType(org.osid.type.Type jobProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.rules.jobprocessorenabler.FederatingJobProcessorEnablerList ret = getJobProcessorEnablerList();
        java.util.Collection<org.osid.resourcing.rules.JobProcessorEnablerLookupSession> sessions = this.sessionsByType.get(jobProcessorEnablerGenusType);

        if (sessions.size() == 0) {
            if (this.typeFallback) {
                return (super.getJobProcessorEnablersByParentGenusType(jobProcessorEnablerGenusType));
            } else {
                return (new net.okapia.osid.jamocha.nil.resourcing.rules.jobprocessorenabler.EmptyJobProcessorEnablerList());
            }
        }


        for (org.osid.resourcing.rules.JobProcessorEnablerLookupSession session : sessions) {
            ret.addJobProcessorEnablerList(session.getJobProcessorEnablersByParentGenusType(jobProcessorEnablerGenusType));
        }
        
        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>JobProcessorEnablerList</code> containing the given
     *  job processor enabler record <code>Type</code>.
     *
     *  @param  jobProcessorEnablerRecordType a job processor enabler record type 
     *  @return the returned <code>JobProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>jobProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerList getJobProcessorEnablersByRecordType(org.osid.type.Type jobProcessorEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.rules.jobprocessorenabler.FederatingJobProcessorEnablerList ret = getJobProcessorEnablerList();
        java.util.Collection<org.osid.resourcing.rules.JobProcessorEnablerLookupSession> sessions = this.sessionsByType.get(jobProcessorEnablerRecordType);

        if (sessions.size() == 0) {
            if (this.typeFallback) {
                return (super.getJobProcessorEnablersByRecordType(jobProcessorEnablerRecordType));
            } else {
                return (new net.okapia.osid.jamocha.nil.resourcing.rules.jobprocessorenabler.EmptyJobProcessorEnablerList());
            }
        }

        for (org.osid.resourcing.rules.JobProcessorEnablerLookupSession session : sessions) {
            ret.addJobProcessorEnablerList(session.getJobProcessorEnablersByRecordType(jobProcessorEnablerRecordType));
        }
        
        ret.noMore();
        return (ret);
    }
}
