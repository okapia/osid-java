//
// AbstractImmutableBid.java
//
//     Wraps a mutable Bid to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.bidding.bid.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Bid</code> to hide modifiers. This
 *  wrapper provides an immutized Bid from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying bid whose state changes are visible.
 */

public abstract class AbstractImmutableBid
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.bidding.Bid {

    private final org.osid.bidding.Bid bid;


    /**
     *  Constructs a new <code>AbstractImmutableBid</code>.
     *
     *  @param bid the bid to immutablize
     *  @throws org.osid.NullArgumentException <code>bid</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableBid(org.osid.bidding.Bid bid) {
        super(bid);
        this.bid = bid;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the auction. 
     *
     *  @return the auction <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAuctionId() {
        return (this.bid.getAuctionId());
    }


    /**
     *  Gets the auction. 
     *
     *  @return the auction 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.bidding.Auction getAuction()
        throws org.osid.OperationFailedException {

        return (this.bid.getAuction());
    }


    /**
     *  Gets the <code> Id </code> of the bidder. 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getBidderId() {
        return (this.bid.getBidderId());
    }


    /**
     *  Gets the bidder. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getBidder()
        throws org.osid.OperationFailedException {

        return (this.bid.getBidder());
    }


    /**
     *  Gets the <code> Id </code> of the bidding agent. 
     *
     *  @return the agent <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getBiddingAgentId() {
        return (this.bid.getBiddingAgentId());
    }


    /**
     *  Gets the bidding agent. 
     *
     *  @return the agent 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getBiddingAgent()
        throws org.osid.OperationFailedException {

        return (this.bid.getBiddingAgent());
    }


    /**
     *  Gets the quantity of items in this bid. 
     *
     *  @return the quantity of items 
     */

    @OSID @Override
    public long getQuantity() {
        return (this.bid.getQuantity());
    }


    /**
     *  Gets the current bid amount. 
     *
     *  @return the current bid 
     */

    @OSID @Override
    public org.osid.financials.Currency getCurrentBid() {
        return (this.bid.getCurrentBid());
    }


    /**
     *  Gets the maximum bid. For some auctions, the current bid is the 
     *  maximum bid. For others, the current bid may be less then the maximum 
     *  bid and automatically increment when greater bids are made up to the 
     *  maximum bid. 
     *
     *  @return the maximum bid 
     */

    @OSID @Override
    public org.osid.financials.Currency getMaximumBid() {
        return (this.bid.getMaximumBid());
    }


    /**
     *  Tests if this was a winning bid. 
     *
     *  @return <code> true </code> if this was a winnign bid, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isWinner() {
        return (this.bid.isWinner());
    }


    /**
     *  Gets the settlement amount owed from the bidder. 
     *
     *  @return the settlement amount 
     */

    @OSID @Override
    public org.osid.financials.Currency getSettlementAmount() {
        return (this.bid.getSettlementAmount());
    }


    /**
     *  Gets the bid record corresponding to the given <code> Bid </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> bidRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(bidRecordType) </code> is <code> true </code> . 
     *
     *  @param  bidRecordType the type of bid record to retrieve 
     *  @return the bid record 
     *  @throws org.osid.NullArgumentException <code> bidRecordType </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(bidRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.records.BidRecord getBidRecord(org.osid.type.Type bidRecordType)
        throws org.osid.OperationFailedException {

        return (this.bid.getBidRecord(bidRecordType));
    }
}

