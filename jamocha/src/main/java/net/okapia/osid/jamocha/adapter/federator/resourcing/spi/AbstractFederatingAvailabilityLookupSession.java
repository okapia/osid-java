//
// AbstractFederatingAvailabilityLookupSession.java
//
//     An abstract federating adapter for an AvailabilityLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.resourcing.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  AvailabilityLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingAvailabilityLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.resourcing.AvailabilityLookupSession>
    implements org.osid.resourcing.AvailabilityLookupSession {

    private boolean parallel = false;
    private org.osid.resourcing.Foundry foundry = new net.okapia.osid.jamocha.nil.resourcing.foundry.UnknownFoundry();


    /**
     *  Constructs a new <code>AbstractFederatingAvailabilityLookupSession</code>.
     */

    protected AbstractFederatingAvailabilityLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.resourcing.AvailabilityLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Foundry/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Foundry Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFoundryId() {
        return (this.foundry.getId());
    }


    /**
     *  Gets the <code>Foundry</code> associated with this 
     *  session.
     *
     *  @return the <code>Foundry</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Foundry getFoundry()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.foundry);
    }


    /**
     *  Sets the <code>Foundry</code>.
     *
     *  @param  foundry the foundry for this session
     *  @throws org.osid.NullArgumentException <code>foundry</code>
     *          is <code>null</code>
     */

    protected void setFoundry(org.osid.resourcing.Foundry foundry) {
        nullarg(foundry, "foundry");
        this.foundry = foundry;
        return;
    }


    /**
     *  Tests if this user can perform <code>Availability</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAvailabilities() {
        for (org.osid.resourcing.AvailabilityLookupSession session : getSessions()) {
            if (session.canLookupAvailabilities()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Availability</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAvailabilityView() {
        for (org.osid.resourcing.AvailabilityLookupSession session : getSessions()) {
            session.useComparativeAvailabilityView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Availability</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAvailabilityView() {
        for (org.osid.resourcing.AvailabilityLookupSession session : getSessions()) {
            session.usePlenaryAvailabilityView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include availabilities in foundries which are children
     *  of this foundry in the foundry hierarchy.
     */

    @OSID @Override
    public void useFederatedFoundryView() {
        for (org.osid.resourcing.AvailabilityLookupSession session : getSessions()) {
            session.useFederatedFoundryView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this foundry only.
     */

    @OSID @Override
    public void useIsolatedFoundryView() {
        for (org.osid.resourcing.AvailabilityLookupSession session : getSessions()) {
            session.useIsolatedFoundryView();
        }

        return;
    }


    /**
     *  Only availabilities whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveAvailabilityView() {
        for (org.osid.resourcing.AvailabilityLookupSession session : getSessions()) {
            session.useEffectiveAvailabilityView();
        }

        return;
    }


    /**
     *  All availabilities of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveAvailabilityView() {
        for (org.osid.resourcing.AvailabilityLookupSession session : getSessions()) {
            session.useAnyEffectiveAvailabilityView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Availability</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Availability</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Availability</code> and
     *  retained for compatibility.
     *
     *  In effective mode, availabilities are returned that are currently
     *  effective.  In any effective mode, effective availabilities and
     *  those currently expired are returned.
     *
     *  @param  availabilityId <code>Id</code> of the
     *          <code>Availability</code>
     *  @return the availability
     *  @throws org.osid.NotFoundException <code>availabilityId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>availabilityId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Availability getAvailability(org.osid.id.Id availabilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.resourcing.AvailabilityLookupSession session : getSessions()) {
            try {
                return (session.getAvailability(availabilityId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(availabilityId + " not found");
    }


    /**
     *  Gets an <code>AvailabilityList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  availabilities specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>Availabilities</code> may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  In effective mode, availabilities are returned that are
     *  currently effective.  In any effective mode, effective
     *  availabilities and those currently expired are returned.
     *
     *  @param  availabilityIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Availability</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>availabilityIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilitiesByIds(org.osid.id.IdList availabilityIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.resourcing.availability.MutableAvailabilityList ret = new net.okapia.osid.jamocha.resourcing.availability.MutableAvailabilityList();

        try (org.osid.id.IdList ids = availabilityIds) {
            while (ids.hasNext()) {
                ret.addAvailability(getAvailability(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>AvailabilityList</code> corresponding to the
     *  given availability genus <code>Type</code> which does not
     *  include availabilities of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned
     *  list may contain only those availabilities that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, availabilities are returned that are
     *  currently effective.  In any effective mode, effective
     *  availabilities and those currently expired are returned.
     *
     *  @param  availabilityGenusType an availability genus type 
     *  @return the returned <code>Availability</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>availabilityGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilitiesByGenusType(org.osid.type.Type availabilityGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.availability.FederatingAvailabilityList ret = getAvailabilityList();

        for (org.osid.resourcing.AvailabilityLookupSession session : getSessions()) {
            ret.addAvailabilityList(session.getAvailabilitiesByGenusType(availabilityGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AvailabilityList</code> corresponding to the
     *  given availability genus <code>Type</code> and include any
     *  additional availabilities with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned
     *  list may contain only those availabilities that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, availabilities are returned that are
     *  currently effective.  In any effective mode, effective
     *  availabilities and those currently expired are returned.
     *
     *  @param  availabilityGenusType an availability genus type 
     *  @return the returned <code>Availability</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>availabilityGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilitiesByParentGenusType(org.osid.type.Type availabilityGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.availability.FederatingAvailabilityList ret = getAvailabilityList();

        for (org.osid.resourcing.AvailabilityLookupSession session : getSessions()) {
            ret.addAvailabilityList(session.getAvailabilitiesByParentGenusType(availabilityGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AvailabilityList</code> containing the given
     *  availability record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned
     *  list may contain only those availabilities that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, availabilities are returned that are
     *  currently effective.  In any effective mode, effective
     *  availabilities and those currently expired are returned.
     *
     *  @param  availabilityRecordType an availability record type 
     *  @return the returned <code>Availability</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>availabilityRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilitiesByRecordType(org.osid.type.Type availabilityRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.availability.FederatingAvailabilityList ret = getAvailabilityList();

        for (org.osid.resourcing.AvailabilityLookupSession session : getSessions()) {
            ret.addAvailabilityList(session.getAvailabilitiesByRecordType(availabilityRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AvailabilityList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned list
     *  may contain only those availabilities that are accessible
     *  through this session.
     *  
     *  In active mode, availabilities are returned that are currently
     *  active. In any status mode, active and inactive availabilities
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Availability</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilitiesOnDate(org.osid.calendaring.DateTime from, 
                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.availability.FederatingAvailabilityList ret = getAvailabilityList();

        for (org.osid.resourcing.AvailabilityLookupSession session : getSessions()) {
            ret.addAvailabilityList(session.getAvailabilitiesOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a list of availabilities corresponding to a resource
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned list
     *  may contain only those availabilities that are accessible
     *  through this session.
     *
     *  In effective mode, availabilities are returned that are
     *  currently effective.  In any effective mode, effective
     *  availabilities and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>AvailabilityList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.resourcing.AvailabilityList getAvailabilitiesForResource(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.availability.FederatingAvailabilityList ret = getAvailabilityList();

        for (org.osid.resourcing.AvailabilityLookupSession session : getSessions()) {
            ret.addAvailabilityList(session.getAvailabilitiesForResource(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of availabilities corresponding to a resource
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned list
     *  may contain only those availabilities that are accessible
     *  through this session.
     *
     *  In effective mode, availabilities are returned that are
     *  currently effective.  In any effective mode, effective
     *  availabilities and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>AvailabilityList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilitiesForResourceOnDate(org.osid.id.Id resourceId,
                                                                                   org.osid.calendaring.DateTime from,
                                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.availability.FederatingAvailabilityList ret = getAvailabilityList();

        for (org.osid.resourcing.AvailabilityLookupSession session : getSessions()) {
            ret.addAvailabilityList(session.getAvailabilitiesForResourceOnDate(resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of availabilities corresponding to a job
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned list
     *  may contain only those availabilities that are accessible
     *  through this session.
     *
     *  In effective mode, availabilities are returned that are
     *  currently effective.  In any effective mode, effective
     *  availabilities and those currently expired are returned.
     *
     *  @param  jobId the <code>Id</code> of the job
     *  @return the returned <code>AvailabilityList</code>
     *  @throws org.osid.NullArgumentException <code>jobId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.resourcing.AvailabilityList getAvailabilitiesForJob(org.osid.id.Id jobId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.availability.FederatingAvailabilityList ret = getAvailabilityList();

        for (org.osid.resourcing.AvailabilityLookupSession session : getSessions()) {
            ret.addAvailabilityList(session.getAvailabilitiesForJob(jobId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of availabilities corresponding to a job
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned list
     *  may contain only those availabilities that are accessible
     *  through this session.
     *
     *  In effective mode, availabilities are returned that are
     *  currently effective.  In any effective mode, effective
     *  availabilities and those currently expired are returned.
     *
     *  @param  jobId the <code>Id</code> of the job
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>AvailabilityList</code>
     *  @throws org.osid.NullArgumentException <code>jobId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilitiesForJobOnDate(org.osid.id.Id jobId,
                                                                              org.osid.calendaring.DateTime from,
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.availability.FederatingAvailabilityList ret = getAvailabilityList();

        for (org.osid.resourcing.AvailabilityLookupSession session : getSessions()) {
            ret.addAvailabilityList(session.getAvailabilitiesForJobOnDate(jobId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of availabilities corresponding to resource and job
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned list
     *  may contain only those availabilities that are accessible
     *  through this session.
     *
     *  In effective mode, availabilities are returned that are
     *  currently effective.  In any effective mode, effective
     *  availabilities and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  jobId the <code>Id</code> of the job
     *  @return the returned <code>AvailabilityList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>jobId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilitiesForResourceAndJob(org.osid.id.Id resourceId,
                                                                                   org.osid.id.Id jobId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.availability.FederatingAvailabilityList ret = getAvailabilityList();

        for (org.osid.resourcing.AvailabilityLookupSession session : getSessions()) {
            ret.addAvailabilityList(session.getAvailabilitiesForResourceAndJob(resourceId, jobId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of availabilities corresponding to resource and job
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned list
     *  may contain only those availabilities that are accessible
     *  through this session.
     *
     *  In effective mode, availabilities are returned that are
     *  currently effective.  In any effective mode, effective
     *  availabilities and those currently expired are returned.
     *
     *  @param  jobId the <code>Id</code> of the job
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>AvailabilityList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>jobId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilitiesForResourceAndJobOnDate(org.osid.id.Id resourceId,
                                                                                         org.osid.id.Id jobId,
                                                                                         org.osid.calendaring.DateTime from,
                                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.availability.FederatingAvailabilityList ret = getAvailabilityList();

        for (org.osid.resourcing.AvailabilityLookupSession session : getSessions()) {
            ret.addAvailabilityList(session.getAvailabilitiesForResourceAndJobOnDate(resourceId, jobId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Availabilities</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned list
     *  may contain only those availabilities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, availabilities are returned that are currently
     *  effective.  In any effective mode, effective availabilities and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Availabilities</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilities()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.availability.FederatingAvailabilityList ret = getAvailabilityList();

        for (org.osid.resourcing.AvailabilityLookupSession session : getSessions()) {
            ret.addAvailabilityList(session.getAvailabilities());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.resourcing.availability.FederatingAvailabilityList getAvailabilityList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.resourcing.availability.ParallelAvailabilityList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.resourcing.availability.CompositeAvailabilityList());
        }
    }
}
