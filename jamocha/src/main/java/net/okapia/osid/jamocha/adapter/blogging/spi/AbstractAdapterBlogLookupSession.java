//
// AbstractAdapterBlogLookupSession.java
//
//    A Blog lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.blogging.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Blog lookup session adapter.
 */

public abstract class AbstractAdapterBlogLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.blogging.BlogLookupSession {

    private final org.osid.blogging.BlogLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterBlogLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterBlogLookupSession(org.osid.blogging.BlogLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Blog} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupBlogs() {
        return (this.session.canLookupBlogs());
    }


    /**
     *  A complete view of the {@code Blog} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeBlogView() {
        this.session.useComparativeBlogView();
        return;
    }


    /**
     *  A complete view of the {@code Blog} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryBlogView() {
        this.session.usePlenaryBlogView();
        return;
    }

     
    /**
     *  Gets the {@code Blog} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Blog} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Blog} and
     *  retained for compatibility.
     *
     *  @param blogId {@code Id} of the {@code Blog}
     *  @return the blog
     *  @throws org.osid.NotFoundException {@code blogId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code blogId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.Blog getBlog(org.osid.id.Id blogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBlog(blogId));
    }


    /**
     *  Gets a {@code BlogList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  blogs specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Blogs} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  blogIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Blog} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code blogIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.BlogList getBlogsByIds(org.osid.id.IdList blogIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBlogsByIds(blogIds));
    }


    /**
     *  Gets a {@code BlogList} corresponding to the given
     *  blog genus {@code Type} which does not include
     *  blogs of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  blogs or an error results. Otherwise, the returned list
     *  may contain only those blogs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  blogGenusType a blog genus type 
     *  @return the returned {@code Blog} list
     *  @throws org.osid.NullArgumentException {@code blogGenusType}
     *          is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.BlogList getBlogsByGenusType(org.osid.type.Type blogGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBlogsByGenusType(blogGenusType));
    }


    /**
     *  Gets a {@code BlogList} corresponding to the given blog genus
     *  {@code Type} and include any additional blogs with genus types
     *  derived from the specified {@code Type}.
     *
     *  In plenary mode, the returned list contains all known blogs or
     *  an error results. Otherwise, the returned list may contain
     *  only those blogs that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  @param  blogGenusType a blog genus type 
     *  @return the returned {@code Blog} list
     *  @throws org.osid.NullArgumentException
     *          {@code blogGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.BlogList getBlogsByParentGenusType(org.osid.type.Type blogGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBlogsByParentGenusType(blogGenusType));
    }


    /**
     *  Gets a {@code BlogList} containing the given
     *  blog record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  blogs or an error results. Otherwise, the returned list
     *  may contain only those blogs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  blogRecordType a blog record type 
     *  @return the returned {@code Blog} list
     *  @throws org.osid.NullArgumentException
     *          {@code blogRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.BlogList getBlogsByRecordType(org.osid.type.Type blogRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBlogsByRecordType(blogRecordType));
    }


    /**
     *  Gets a {@code BlogList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  blogs or an error results. Otherwise, the returned list
     *  may contain only those blogs that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Blog} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.blogging.BlogList getBlogsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBlogsByProvider(resourceId));
    }


    /**
     *  Gets all {@code Blogs}. 
     *
     *  In plenary mode, the returned list contains all known
     *  blogs or an error results. Otherwise, the returned list
     *  may contain only those blogs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Blogs} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.BlogList getBlogs()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBlogs());
    }
}
