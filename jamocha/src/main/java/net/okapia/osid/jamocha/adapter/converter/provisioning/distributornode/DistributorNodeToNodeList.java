//
// DistributorNodeToNodeList.java
//
//     Implements a DistributorNodeToNodeList adapter.
//
//
// Tom Coppeto
// Okapia
// 21 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.converter.provisioning.distributornode;

import org.osid.binding.java.annotation.OSID;


/**
 *  Implements an {@code DistributorNodeList} adapter to convert {@code
 *  DistributorNodes} into hierarchy {@code Nodes}.
 */

public final class DistributorNodeToNodeList
    extends net.okapia.osid.jamocha.adapter.converter.provisioning.distributornode.spi.AbstractDistributorNodeToNodeList
    implements org.osid.hierarchy.NodeList {


    /**
     *  Constructs a new {@code DistributorNodeToNodeList} from a
     *  DistributorNodeList.
     *
     *  @param list the distributor node list to convert
     *  @throws org.osid.NullArgumentException {@code list} is 
     *          {@code null}
     */

    public DistributorNodeToNodeList(org.osid.provisioning.DistributorNodeList list) {
        super(list);
        return;
    }
}
