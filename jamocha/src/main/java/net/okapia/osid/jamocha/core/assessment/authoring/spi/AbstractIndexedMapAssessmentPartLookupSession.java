//
// AbstractIndexedMapAssessmentPartLookupSession.java
//
//    A simple framework for providing an AssessmentPart lookup service
//    backed by a fixed collection of assessment parts with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.assessment.authoring.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an AssessmentPart lookup service backed by a
 *  fixed collection of assessment parts. The assessment parts are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some assessment parts may be compatible
 *  with more types than are indicated through these assessment part
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>AssessmentParts</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapAssessmentPartLookupSession
    extends AbstractMapAssessmentPartLookupSession
    implements org.osid.assessment.authoring.AssessmentPartLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.assessment.authoring.AssessmentPart> assessmentPartsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.assessment.authoring.AssessmentPart>());
    private final MultiMap<org.osid.type.Type, org.osid.assessment.authoring.AssessmentPart> assessmentPartsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.assessment.authoring.AssessmentPart>());


    /**
     *  Makes an <code>AssessmentPart</code> available in this session.
     *
     *  @param  assessmentPart an assessment part
     *  @throws org.osid.NullArgumentException <code>assessmentPart<code> is
     *          <code>null</code>
     */

    @Override
    protected void putAssessmentPart(org.osid.assessment.authoring.AssessmentPart assessmentPart) {
        super.putAssessmentPart(assessmentPart);

        this.assessmentPartsByGenus.put(assessmentPart.getGenusType(), assessmentPart);
        
        try (org.osid.type.TypeList types = assessmentPart.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.assessmentPartsByRecord.put(types.getNextType(), assessmentPart);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an assessment part from this session.
     *
     *  @param assessmentPartId the <code>Id</code> of the assessment part
     *  @throws org.osid.NullArgumentException <code>assessmentPartId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeAssessmentPart(org.osid.id.Id assessmentPartId) {
        org.osid.assessment.authoring.AssessmentPart assessmentPart;
        try {
            assessmentPart = getAssessmentPart(assessmentPartId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.assessmentPartsByGenus.remove(assessmentPart.getGenusType());

        try (org.osid.type.TypeList types = assessmentPart.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.assessmentPartsByRecord.remove(types.getNextType(), assessmentPart);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeAssessmentPart(assessmentPartId);
        return;
    }


    /**
     *  Gets an <code>AssessmentPartList</code> corresponding to the given
     *  assessment part genus <code>Type</code> which does not include
     *  assessment parts of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known assessment parts or an error results. Otherwise,
     *  the returned list may contain only those assessment parts that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  assessmentPartGenusType an assessment part genus type 
     *  @return the returned <code>AssessmentPart</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentPartGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartList getAssessmentPartsByGenusType(org.osid.type.Type assessmentPartGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.assessment.authoring.assessmentpart.ArrayAssessmentPartList(this.assessmentPartsByGenus.get(assessmentPartGenusType)));
    }


    /**
     *  Gets an <code>AssessmentPartList</code> containing the given
     *  assessment part record <code>Type</code>. In plenary mode, the
     *  returned list contains all known assessment parts or an error
     *  results. Otherwise, the returned list may contain only those
     *  assessment parts that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  assessmentPartRecordType an assessment part record type 
     *  @return the returned <code>assessmentPart</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentPartRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartList getAssessmentPartsByRecordType(org.osid.type.Type assessmentPartRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.assessment.authoring.assessmentpart.ArrayAssessmentPartList(this.assessmentPartsByRecord.get(assessmentPartRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.assessmentPartsByGenus.clear();
        this.assessmentPartsByRecord.clear();

        super.close();

        return;
    }
}
