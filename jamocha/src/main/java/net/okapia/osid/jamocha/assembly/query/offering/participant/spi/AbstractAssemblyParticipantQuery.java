//
// AbstractAssemblyParticipantQuery.java
//
//     A ParticipantQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.offering.participant.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ParticipantQuery that stores terms.
 */

public abstract class AbstractAssemblyParticipantQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.offering.ParticipantQuery,
               org.osid.offering.ParticipantQueryInspector,
               org.osid.offering.ParticipantSearchOrder {

    private final java.util.Collection<org.osid.offering.records.ParticipantQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.offering.records.ParticipantQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.offering.records.ParticipantSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyParticipantQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyParticipantQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets an offering <code> Id. </code> 
     *
     *  @param  offeringId an offering <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> offeringId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOfferingId(org.osid.id.Id offeringId, boolean match) {
        getAssembler().addIdTerm(getOfferingIdColumn(), offeringId, match);
        return;
    }


    /**
     *  Clears all offering <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearOfferingIdTerms() {
        getAssembler().clearTerms(getOfferingIdColumn());
        return;
    }


    /**
     *  Gets the offering <code> Id </code> query terms. 
     *
     *  @return the offering <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOfferingIdTerms() {
        return (getAssembler().getIdTerms(getOfferingIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the offering. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByOffering(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getOfferingColumn(), style);
        return;
    }


    /**
     *  Gets the OfferingId column name.
     *
     * @return the column name
     */

    protected String getOfferingIdColumn() {
        return ("offering_id");
    }


    /**
     *  Tests if an <code> OfferingQuery </code> is available. 
     *
     *  @return <code> true </code> if an offering query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingQuery() {
        return (false);
    }


    /**
     *  Gets the query for an offering query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the offering query 
     *  @throws org.osid.UnimplementedException <code> supportsOfferingQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingQuery getOfferingQuery() {
        throw new org.osid.UnimplementedException("supportsOfferingQuery() is false");
    }


    /**
     *  Clears all offering terms. 
     */

    @OSID @Override
    public void clearOfferingTerms() {
        getAssembler().clearTerms(getOfferingColumn());
        return;
    }


    /**
     *  Gets the offering query terms. 
     *
     *  @return the offering terms 
     */

    @OSID @Override
    public org.osid.offering.OfferingQueryInspector[] getOfferingTerms() {
        return (new org.osid.offering.OfferingQueryInspector[0]);
    }


    /**
     *  Tests if a offering search order is available. 
     *
     *  @return <code> true </code> if an offering search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingSearchOrder() {
        return (false);
    }


    /**
     *  Gets the offering search order. 
     *
     *  @return the offering search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingSearchOrder getOfferingSearchOrder() {
        throw new org.osid.UnimplementedException("supportsOfferingSearchOrder() is false");
    }


    /**
     *  Gets the Offering column name.
     *
     * @return the column name
     */

    protected String getOfferingColumn() {
        return ("offering");
    }


    /**
     *  Sets a resource <code> Id. </code> 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResourceId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getResourceIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears all resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearResourceIdTerms() {
        getAssembler().clearTerms(getResourceIdColumn());
        return;
    }


    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the resource <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResourceIdTerms() {
        return (getAssembler().getIdTerms(getResourceIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByResource(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getResourceColumn(), style);
        return;
    }


    /**
     *  Gets the ResourceId column name.
     *
     * @return the column name
     */

    protected String getResourceIdColumn() {
        return ("resource_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResourceQuery() {
        throw new org.osid.UnimplementedException("supportsResourceQuery() is false");
    }


    /**
     *  Clears all resource terms. 
     */

    @OSID @Override
    public void clearResourceTerms() {
        getAssembler().clearTerms(getResourceColumn());
        return;
    }


    /**
     *  Gets the resource query terms. 
     *
     *  @return the resource terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a offering search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getResourceSearchOrder() {
        throw new org.osid.UnimplementedException("supportsResourceSearchOrder() is false");
    }


    /**
     *  Gets the Resource column name.
     *
     * @return the column name
     */

    protected String getResourceColumn() {
        return ("resource");
    }


    /**
     *  Sets the time period <code> Id </code> for this query to match 
     *  participants that have a related term. 
     *
     *  @param  timePeriodId a time period <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> timePeriodId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchTimePeriodId(org.osid.id.Id timePeriodId, boolean match) {
        getAssembler().addIdTerm(getTimePeriodIdColumn(), timePeriodId, match);
        return;
    }


    /**
     *  Clears the time period <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearTimePeriodIdTerms() {
        getAssembler().clearTerms(getTimePeriodIdColumn());
        return;
    }


    /**
     *  Gets the time period <code> Id </code> query terms. 
     *
     *  @return the time period <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getTimePeriodIdTerms() {
        return (getAssembler().getIdTerms(getTimePeriodIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by time period. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTimePeriod(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTimePeriodColumn(), style);
        return;
    }


    /**
     *  Gets the TimePeriodId column name.
     *
     * @return the column name
     */

    protected String getTimePeriodIdColumn() {
        return ("time_period_id");
    }


    /**
     *  Tests if a <code> TimePeriodQuery </code> is available. 
     *
     *  @return <code> true </code> if a time period query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTimePeriodQuery() {
        return (false);
    }


    /**
     *  Gets the query for a time period. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the time period query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodQuery getTimePeriodQuery() {
        throw new org.osid.UnimplementedException("supportsTimePeriodQuery() is false");
    }


    /**
     *  Clears the time period terms. 
     */

    @OSID @Override
    public void clearTimePeriodTerms() {
        getAssembler().clearTerms(getTimePeriodColumn());
        return;
    }


    /**
     *  Gets the time period query terms. 
     *
     *  @return the time period query terms 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodQueryInspector[] getTimePeriodTerms() {
        return (new org.osid.calendaring.TimePeriodQueryInspector[0]);
    }


    /**
     *  Tests if a time period order is available. 
     *
     *  @return <code> true </code> if a time period order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTimePeriodSearchOrder() {
        return (false);
    }


    /**
     *  Gets the time period order. 
     *
     *  @return the time period search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodSearchOrder getTimePeriodSearchOrder() {
        throw new org.osid.UnimplementedException("supportsTimePeriodSearchOrder() is false");
    }


    /**
     *  Gets the TimePeriod column name.
     *
     * @return the column name
     */

    protected String getTimePeriodColumn() {
        return ("time_period");
    }


    /**
     *  Sets the grade system <code> Id </code> for this query. 
     *
     *  @param  gradeSystemId a grade system <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeSystemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResultOptionId(org.osid.id.Id gradeSystemId, 
                                    boolean match) {
        getAssembler().addIdTerm(getResultOptionIdColumn(), gradeSystemId, match);
        return;
    }


    /**
     *  Clears the grade system <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearResultOptionIdTerms() {
        getAssembler().clearTerms(getResultOptionIdColumn());
        return;
    }


    /**
     *  Gets the grade system <code> Id </code> query terms. 
     *
     *  @return the grade system <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResultOptionIdTerms() {
        return (getAssembler().getIdTerms(getResultOptionIdColumn()));
    }


    /**
     *  Gets the ResultOptionId column name.
     *
     * @return the column name
     */

    protected String getResultOptionIdColumn() {
        return ("result_option_id");
    }


    /**
     *  Tests if a <code> GradeSystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade system query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResultOptionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grading option. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a grade system query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResultOptionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQuery getResultOptionQuery() {
        throw new org.osid.UnimplementedException("supportsResultOptionQuery() is false");
    }


    /**
     *  Matches participants that have any grading option. 
     *
     *  @param  match <code> true </code> to match participants with any 
     *          grading option, <code> false </code> to match participants 
     *          with no grading options 
     */

    @OSID @Override
    public void matchAnyResultOption(boolean match) {
        getAssembler().addIdWildcardTerm(getResultOptionColumn(), match);
        return;
    }


    /**
     *  Clears the grading option terms. 
     */

    @OSID @Override
    public void clearResultOptionTerms() {
        getAssembler().clearTerms(getResultOptionColumn());
        return;
    }


    /**
     *  Gets the grade system query terms. 
     *
     *  @return the grade system terms 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQueryInspector[] getResultOptionTerms() {
        return (new org.osid.grading.GradeSystemQueryInspector[0]);
    }


    /**
     *  Gets the ResultOption column name.
     *
     * @return the column name
     */

    protected String getResultOptionColumn() {
        return ("result_option");
    }


    /**
     *  Sets the catalogue <code> Id </code> for this query to match 
     *  participants assigned to catalogues. 
     *
     *  @param  catalogueId a catalogue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCatalogueId(org.osid.id.Id catalogueId, boolean match) {
        getAssembler().addIdTerm(getCatalogueIdColumn(), catalogueId, match);
        return;
    }


    /**
     *  Clears all catalogue <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCatalogueIdTerms() {
        getAssembler().clearTerms(getCatalogueIdColumn());
        return;
    }


    /**
     *  Gets the catalogue <code> Id </code> query terms. 
     *
     *  @return the catalogue <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCatalogueIdTerms() {
        return (getAssembler().getIdTerms(getCatalogueIdColumn()));
    }


    /**
     *  Gets the CatalogueId column name.
     *
     * @return the column name
     */

    protected String getCatalogueIdColumn() {
        return ("catalogue_id");
    }


    /**
     *  Tests if a <code> CatalogueQuery </code> is available. 
     *
     *  @return <code> true </code> if a catalogue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a catalogue query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the catalogue query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogueQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CatalogueQuery getCatalogueQuery() {
        throw new org.osid.UnimplementedException("supportsCatalogueQuery() is false");
    }


    /**
     *  Clears all catalogue terms. 
     */

    @OSID @Override
    public void clearCatalogueTerms() {
        getAssembler().clearTerms(getCatalogueColumn());
        return;
    }


    /**
     *  Gets the catalogue query terms. 
     *
     *  @return the catalogue terms 
     */

    @OSID @Override
    public org.osid.offering.CatalogueQueryInspector[] getCatalogueTerms() {
        return (new org.osid.offering.CatalogueQueryInspector[0]);
    }


    /**
     *  Gets the Catalogue column name.
     *
     * @return the column name
     */

    protected String getCatalogueColumn() {
        return ("catalogue");
    }


    /**
     *  Tests if this participant supports the given record
     *  <code>Type</code>.
     *
     *  @param  participantRecordType a participant record type 
     *  @return <code>true</code> if the participantRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>participantRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type participantRecordType) {
        for (org.osid.offering.records.ParticipantQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(participantRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  participantRecordType the participant record type 
     *  @return the participant query record 
     *  @throws org.osid.NullArgumentException
     *          <code>participantRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(participantRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.records.ParticipantQueryRecord getParticipantQueryRecord(org.osid.type.Type participantRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.records.ParticipantQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(participantRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(participantRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  participantRecordType the participant record type 
     *  @return the participant query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>participantRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(participantRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.records.ParticipantQueryInspectorRecord getParticipantQueryInspectorRecord(org.osid.type.Type participantRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.records.ParticipantQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(participantRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(participantRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param participantRecordType the participant record type
     *  @return the participant search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>participantRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(participantRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.records.ParticipantSearchOrderRecord getParticipantSearchOrderRecord(org.osid.type.Type participantRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.records.ParticipantSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(participantRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(participantRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this participant. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param participantQueryRecord the participant query record
     *  @param participantQueryInspectorRecord the participant query inspector
     *         record
     *  @param participantSearchOrderRecord the participant search order record
     *  @param participantRecordType participant record type
     *  @throws org.osid.NullArgumentException
     *          <code>participantQueryRecord</code>,
     *          <code>participantQueryInspectorRecord</code>,
     *          <code>participantSearchOrderRecord</code> or
     *          <code>participantRecordTypeparticipant</code> is
     *          <code>null</code>
     */
            
    protected void addParticipantRecords(org.osid.offering.records.ParticipantQueryRecord participantQueryRecord, 
                                      org.osid.offering.records.ParticipantQueryInspectorRecord participantQueryInspectorRecord, 
                                      org.osid.offering.records.ParticipantSearchOrderRecord participantSearchOrderRecord, 
                                      org.osid.type.Type participantRecordType) {

        addRecordType(participantRecordType);

        nullarg(participantQueryRecord, "participant query record");
        nullarg(participantQueryInspectorRecord, "participant query inspector record");
        nullarg(participantSearchOrderRecord, "participant search odrer record");

        this.queryRecords.add(participantQueryRecord);
        this.queryInspectorRecords.add(participantQueryInspectorRecord);
        this.searchOrderRecords.add(participantSearchOrderRecord);
        
        return;
    }
}
