//
// AbstractFederatingProgramOfferingLookupSession.java
//
//     An abstract federating adapter for a ProgramOfferingLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.course.program.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  ProgramOfferingLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingProgramOfferingLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.course.program.ProgramOfferingLookupSession>
    implements org.osid.course.program.ProgramOfferingLookupSession {

    private boolean parallel = false;
    private org.osid.course.CourseCatalog courseCatalog = new net.okapia.osid.jamocha.nil.course.coursecatalog.UnknownCourseCatalog();


    /**
     *  Constructs a new <code>AbstractFederatingProgramOfferingLookupSession</code>.
     */

    protected AbstractFederatingProgramOfferingLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.course.program.ProgramOfferingLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>CourseCatalog/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.courseCatalog.getId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.courseCatalog);
    }


    /**
     *  Sets the <code>CourseCatalog</code>.
     *
     *  @param  courseCatalog the course catalog for this session
     *  @throws org.osid.NullArgumentException <code>courseCatalog</code>
     *          is <code>null</code>
     */

    protected void setCourseCatalog(org.osid.course.CourseCatalog courseCatalog) {
        nullarg(courseCatalog, "course catalog");
        this.courseCatalog = courseCatalog;
        return;
    }


    /**
     *  Tests if this user can perform <code>ProgramOffering</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupProgramOfferings() {
        for (org.osid.course.program.ProgramOfferingLookupSession session : getSessions()) {
            if (session.canLookupProgramOfferings()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>ProgramOffering</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeProgramOfferingView() {
        for (org.osid.course.program.ProgramOfferingLookupSession session : getSessions()) {
            session.useComparativeProgramOfferingView();
        }

        return;
    }


    /**
     *  A complete view of the <code>ProgramOffering</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryProgramOfferingView() {
        for (org.osid.course.program.ProgramOfferingLookupSession session : getSessions()) {
            session.usePlenaryProgramOfferingView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include program offerings in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        for (org.osid.course.program.ProgramOfferingLookupSession session : getSessions()) {
            session.useFederatedCourseCatalogView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        for (org.osid.course.program.ProgramOfferingLookupSession session : getSessions()) {
            session.useIsolatedCourseCatalogView();
        }

        return;
    }


    /**
     *  Only program offerings whose effective dates are current are
     *  returned by methods in this session.
     */

    @OSID @Override
    public void useEffectiveProgramOfferingView() {
        for (org.osid.course.program.ProgramOfferingLookupSession session : getSessions()) {
            session.useEffectiveProgramOfferingView();
        }

        return;
    }


    /**
     *  All program offering of any effective dates are returned by
     *  all methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveProgramOfferingView() {
        for (org.osid.course.program.ProgramOfferingLookupSession session : getSessions()) {
            session.useAnyEffectiveProgramOfferingView();
        }

        return;
    }

    
    /**
     *  Gets the <code>ProgramOffering</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>ProgramOffering</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>ProgramOffering</code> and retained for compatibility.
     *
     *  In effective mode, program offerings are returned that are
     *  currently effective.  In any effective mode, effective program
     *  offerings and those currently expired are returned.
     *
     *  @param  programOfferingId <code>Id</code> of the
     *          <code>ProgramOffering</code>
     *  @return the program offering
     *  @throws org.osid.NotFoundException <code>programOfferingId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>programOfferingId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOffering getProgramOffering(org.osid.id.Id programOfferingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.course.program.ProgramOfferingLookupSession session : getSessions()) {
            try {
                return (session.getProgramOffering(programOfferingId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(programOfferingId + " not found");
    }


    /**
     *  Gets a <code>ProgramOfferingList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  programOfferings specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>ProgramOfferings</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, program offerings are returned that are
     *  currently effective.  In any effective mode, effective program
     *  offerings and those currently expired are returned.
     *
     *  @param  programOfferingIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>ProgramOffering</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>programOfferingIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsByIds(org.osid.id.IdList programOfferingIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.course.program.programoffering.MutableProgramOfferingList ret = new net.okapia.osid.jamocha.course.program.programoffering.MutableProgramOfferingList();

        try (org.osid.id.IdList ids = programOfferingIds) {
            while (ids.hasNext()) {
                ret.addProgramOffering(getProgramOffering(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>ProgramOfferingList</code> corresponding to the given
     *  program offering genus <code>Type</code> which does not include
     *  program offerings of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  program offerings or an error results. Otherwise, the returned list
     *  may contain only those program offerings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, program offerings are returned that are
     *  currently effective.  In any effective mode, effective program
     *  offerings and those currently expired are returned.
     *
     *  @param  programOfferingGenusType a programOffering genus type 
     *  @return the returned <code>ProgramOffering</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>programOfferingGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsByGenusType(org.osid.type.Type programOfferingGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.program.programoffering.FederatingProgramOfferingList ret = getProgramOfferingList();

        for (org.osid.course.program.ProgramOfferingLookupSession session : getSessions()) {
            ret.addProgramOfferingList(session.getProgramOfferingsByGenusType(programOfferingGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ProgramOfferingList</code> corresponding to the given
     *  program offering genus <code>Type</code> and include any additional
     *  program offerings with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  program offerings or an error results. Otherwise, the returned list
     *  may contain only those program offerings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, program offerings are returned that are
     *  currently effective.  In any effective mode, effective program
     *  offerings and those currently expired are returned.
     *
     *  @param  programOfferingGenusType a programOffering genus type 
     *  @return the returned <code>ProgramOffering</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>programOfferingGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsByParentGenusType(org.osid.type.Type programOfferingGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.program.programoffering.FederatingProgramOfferingList ret = getProgramOfferingList();

        for (org.osid.course.program.ProgramOfferingLookupSession session : getSessions()) {
            ret.addProgramOfferingList(session.getProgramOfferingsByParentGenusType(programOfferingGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ProgramOfferingList</code> containing the given
     *  program offering record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  program offerings or an error results. Otherwise, the returned list
     *  may contain only those program offerings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, program offerings are returned that are
     *  currently effective.  In any effective mode, effective program
     *  offerings and those currently expired are returned.
     *
     *  @param  programOfferingRecordType a programOffering record type 
     *  @return the returned <code>ProgramOffering</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>programOfferingRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsByRecordType(org.osid.type.Type programOfferingRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.program.programoffering.FederatingProgramOfferingList ret = getProgramOfferingList();

        for (org.osid.course.program.ProgramOfferingLookupSession session : getSessions()) {
            ret.addProgramOfferingList(session.getProgramOfferingsByRecordType(programOfferingRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ProgramOfferingList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known program
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those program offerings that are accessible
     *  through this session.
     *  
     *  In active mode, program offerings are returned that are
     *  currently active. In any status mode, active and inactive
     *  program offerings are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>ProgramOffering</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsOnDate(org.osid.calendaring.DateTime from, 
                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.program.programoffering.FederatingProgramOfferingList ret = getProgramOfferingList();

        for (org.osid.course.program.ProgramOfferingLookupSession session : getSessions()) {
            ret.addProgramOfferingList(session.getProgramOfferingsOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code> ProgramOfferings </code> associated with a
     *  given <code> Program. </code> In plenary mode, the returned
     *  list contains all known program offerings or an error
     *  results. Otherwise, the returned list may contain only those
     *  program offerings that are accessible through this session.
     *
     *  In effective mode, program offerings are returned that are
     *  currently effective.  In any effective mode, effective program
     *  offerings and those currently expired are returned.
     *
     *  @param  programId a program <code> Id </code>
     *  @return a list of <code> ProgramOfferings </code>
     *  @throws org.osid.NullArgumentException <code> programId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsForProgram(org.osid.id.Id programId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.program.programoffering.FederatingProgramOfferingList ret = getProgramOfferingList();

        for (org.osid.course.program.ProgramOfferingLookupSession session : getSessions()) {
            ret.addProgramOfferingList(session.getProgramOfferingsForProgram(programId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ProgramOfferingList</code> for a program and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known program
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those program offerings that are accessible
     *  through this session.
     *  
     *  In active mode, program offerings are returned that are
     *  currently active. In any status mode, active and inactive
     *  program offerings are returned.
     *
     *  @param programId a program <code>Id</code>
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>ProgramOffering</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>programId</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsForProgramOnDate(org.osid.id.Id programId,
                                                                                           org.osid.calendaring.DateTime from, 
                                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.program.programoffering.FederatingProgramOfferingList ret = getProgramOfferingList();

        for (org.osid.course.program.ProgramOfferingLookupSession session : getSessions()) {
            ret.addProgramOfferingList(session.getProgramOfferingsForProgramOnDate(programId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code> ProgramOfferings </code> associated with a
     *  given <code> Term. </code> 
     *
     *  In plenary mode, the returned list contains all known program
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those program offerings that are accessible
     *  through this session.
     *
     *  In effective mode, program offerings are returned that are
     *  currently effective.  In any effective mode, effective program
     *  offerings and those currently expired are returned.
     *
     *  @param  termId a term <code> Id </code>
     *  @return a list of <code> ProgramOfferings </code>
     *  @throws org.osid.NullArgumentException <code> termId </code> is <code>
     *          null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsForTerm(org.osid.id.Id termId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.program.programoffering.FederatingProgramOfferingList ret = getProgramOfferingList();

        for (org.osid.course.program.ProgramOfferingLookupSession session : getSessions()) {
            ret.addProgramOfferingList(session.getProgramOfferingsForTerm(termId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ProgramOfferingList</code> for a term and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known program
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those program offerings that are accessible
     *  through this session.
     *  
     *  In active mode, program offerings are returned that are
     *  currently active. In any status mode, active and inactive
     *  program offerings are returned.
     *
     *  @param termId a term <code>Id</code>
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>ProgramOffering</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>termId</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsForTermOnDate(org.osid.id.Id termId,        
                                                                                        org.osid.calendaring.DateTime from, 
                                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.program.programoffering.FederatingProgramOfferingList ret = getProgramOfferingList();

        for (org.osid.course.program.ProgramOfferingLookupSession session : getSessions()) {
            ret.addProgramOfferingList(session.getProgramOfferingsForTermOnDate(termId, from, to));
        }

        ret.noMore();
        return (ret);
    }

    /**
     *  Gets all <code> ProgramOfferings </code> associated with a
     *  given <code> Term </code> and <code> Program. </code> 
     *
     *  In plenary mode, the returned list contains all known program
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those program offerings that are accessible
     *  through this session.
     *
     *  In effective mode, program offerings are returned that are
     *  currently effective.  In any effective mode, effective program
     *  offerings and those currently expired are returned.
     *
     *  @param  programId a program <code> Id </code>
     *  @param  termId a term <code> Id </code>
     *  @return a list of <code> ProgramOfferings </code>
     *  @throws org.osid.NullArgumentException <code> programId
     *          </code> or <code> termId </code> is <code> null
     *          </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsForProgramAndTerm(org.osid.id.Id programId,
                                                                                           org.osid.id.Id termId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.program.programoffering.FederatingProgramOfferingList ret = getProgramOfferingList();

        for (org.osid.course.program.ProgramOfferingLookupSession session : getSessions()) {
            ret.addProgramOfferingList(session.getProgramOfferingsForProgramAndTerm(programId, termId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ProgramOfferingList</code> for a program, term,
     *  and effective during the entire given date range inclusive but
     *  not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known program
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those program offerings that are accessible
     *  through this session.
     *  
     *  In active mode, program offerings are returned that are
     *  currently active. In any status mode, active and inactive
     *  program offerings are returned.
     *
     *  @param programId a program <code>Id</code>
     *  @param termId a term <code>Id</code>
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>ProgramOffering</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>programId</code>,
     *          <code>termId</code>, <code>from</code>, or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsForProgramAndTermOnDate(org.osid.id.Id programId,
                                                                                           org.osid.id.Id termId,
                                                                                           org.osid.calendaring.DateTime from, 
                                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.program.programoffering.FederatingProgramOfferingList ret = getProgramOfferingList();

        for (org.osid.course.program.ProgramOfferingLookupSession session : getSessions()) {
            ret.addProgramOfferingList(session.getProgramOfferingsForProgramAndTermOnDate(programId, termId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>ProgramOfferings</code>. 
     *
     *  In plenary mode, the returned list contains all known program
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those program offerings that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, program offerings are returned that are
     *  currently effective.  In any effective mode, effective program
     *  offerings and those currently expired are returned.
     *
     *  @return a list of <code>ProgramOfferings</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferings()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.program.programoffering.FederatingProgramOfferingList ret = getProgramOfferingList();

        for (org.osid.course.program.ProgramOfferingLookupSession session : getSessions()) {
            ret.addProgramOfferingList(session.getProgramOfferings());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.course.program.programoffering.FederatingProgramOfferingList getProgramOfferingList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.course.program.programoffering.ParallelProgramOfferingList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.course.program.programoffering.CompositeProgramOfferingList());
        }
    }
}
