//
// AbstractFederatingVoteLookupSession.java
//
//     An abstract federating adapter for a VoteLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.voting.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  VoteLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingVoteLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.voting.VoteLookupSession>
    implements org.osid.voting.VoteLookupSession {

    private boolean parallel = false;
    private org.osid.voting.Polls polls = new net.okapia.osid.jamocha.nil.voting.polls.UnknownPolls();


    /**
     *  Constructs a new <code>AbstractFederatingVoteLookupSession</code>.
     */

    protected AbstractFederatingVoteLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.voting.VoteLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Polls/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Polls Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getPollsId() {
        return (this.polls.getId());
    }


    /**
     *  Gets the <code>Polls</code> associated with this 
     *  session.
     *
     *  @return the <code>Polls</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Polls getPolls()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.polls);
    }


    /**
     *  Sets the <code>Polls</code>.
     *
     *  @param  polls the polls for this session
     *  @throws org.osid.NullArgumentException <code>polls</code>
     *          is <code>null</code>
     */

    protected void setPolls(org.osid.voting.Polls polls) {
        nullarg(polls, "polls");
        this.polls = polls;
        return;
    }


    /**
     *  Tests if this user can perform <code>Vote</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupVotes() {
        for (org.osid.voting.VoteLookupSession session : getSessions()) {
            if (session.canLookupVotes()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Vote</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeVoteView() {
        for (org.osid.voting.VoteLookupSession session : getSessions()) {
            session.useComparativeVoteView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Vote</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryVoteView() {
        for (org.osid.voting.VoteLookupSession session : getSessions()) {
            session.usePlenaryVoteView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include votes in pollses which are children
     *  of this polls in the polls hierarchy.
     */

    @OSID @Override
    public void useFederatedPollsView() {
        for (org.osid.voting.VoteLookupSession session : getSessions()) {
            session.useFederatedPollsView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this polls only.
     */

    @OSID @Override
    public void useIsolatedPollsView() {
        for (org.osid.voting.VoteLookupSession session : getSessions()) {
            session.useIsolatedPollsView();
        }

        return;
    }


    /**
     *  Only votes whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveVoteView() {
        for (org.osid.voting.VoteLookupSession session : getSessions()) {
            session.useEffectiveVoteView();
        }

        return;
    }


    /**
     *  All votes of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveVoteView() {
        for (org.osid.voting.VoteLookupSession session : getSessions()) {
            session.useAnyEffectiveVoteView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Vote</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Vote</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Vote</code> and
     *  retained for compatibility.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and
     *  those currently expired are returned.
     *
     *  @param  voteId <code>Id</code> of the
     *          <code>Vote</code>
     *  @return the vote
     *  @throws org.osid.NotFoundException <code>voteId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>voteId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Vote getVote(org.osid.id.Id voteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.voting.VoteLookupSession session : getSessions()) {
            try {
                return (session.getVote(voteId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(voteId + " not found");
    }


    /**
     *  Gets a <code>VoteList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  votes specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Votes</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and those
     *  currently expired are returned.
     *
     *  @param  voteIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Vote</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>voteIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesByIds(org.osid.id.IdList voteIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.voting.vote.MutableVoteList ret = new net.okapia.osid.jamocha.voting.vote.MutableVoteList();

        try (org.osid.id.IdList ids = voteIds) {
            while (ids.hasNext()) {
                ret.addVote(getVote(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>VoteList</code> corresponding to the given
     *  vote genus <code>Type</code> which does not include
     *  votes of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  votes or an error results. Otherwise, the returned list
     *  may contain only those votes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and those
     *  currently expired are returned.
     *
     *  @param  voteGenusType a vote genus type 
     *  @return the returned <code>Vote</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>voteGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesByGenusType(org.osid.type.Type voteGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.vote.FederatingVoteList ret = getVoteList();

        for (org.osid.voting.VoteLookupSession session : getSessions()) {
            ret.addVoteList(session.getVotesByGenusType(voteGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>VoteList</code> corresponding to the given
     *  vote genus <code>Type</code> and include any additional
     *  votes with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  votes or an error results. Otherwise, the returned list
     *  may contain only those votes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and those
     *  currently expired are returned.
     *
     *  @param  voteGenusType a vote genus type 
     *  @return the returned <code>Vote</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>voteGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesByParentGenusType(org.osid.type.Type voteGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.vote.FederatingVoteList ret = getVoteList();

        for (org.osid.voting.VoteLookupSession session : getSessions()) {
            ret.addVoteList(session.getVotesByParentGenusType(voteGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>VoteList</code> containing the given
     *  vote record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  votes or an error results. Otherwise, the returned list
     *  may contain only those votes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and those
     *  currently expired are returned.
     *
     *  @param  voteRecordType a vote record type 
     *  @return the returned <code>Vote</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>voteRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesByRecordType(org.osid.type.Type voteRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.vote.FederatingVoteList ret = getVoteList();

        for (org.osid.voting.VoteLookupSession session : getSessions()) {
            ret.addVoteList(session.getVotesByRecordType(voteRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>VoteList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  votes or an error results. Otherwise, the returned list
     *  may contain only those votes that are accessible
     *  through this session.
     *  
     *  In active mode, votes are returned that are currently
     *  active. In any status mode, active and inactive votes
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Vote</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.voting.VoteList getVotesOnDate(org.osid.calendaring.DateTime from, 
                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.vote.FederatingVoteList ret = getVoteList();

        for (org.osid.voting.VoteLookupSession session : getSessions()) {
            ret.addVoteList(session.getVotesOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a list of votes corresponding to a voter <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known votes or
     *  an error results. Otherwise, the returned list may contain
     *  only those votes that are accessible through this session.
     *
     *  In effective mode, votes are returned that are
     *  currently effective.  In any effective mode, effective
     *  votes and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the voter
     *  @return the returned <code>VoteList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.voting.VoteList getVotesForVoter(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.vote.FederatingVoteList ret = getVoteList();

        for (org.osid.voting.VoteLookupSession session : getSessions()) {
            ret.addVoteList(session.getVotesForVoter(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of votes corresponding to a voter
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  votes or an error results. Otherwise, the returned list
     *  may contain only those votes that are accessible
     *  through this session.
     *
     *  In effective mode, votes are returned that are
     *  currently effective.  In any effective mode, effective
     *  votes and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the voter
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>VoteList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesForVoterOnDate(org.osid.id.Id resourceId,
                                                           org.osid.calendaring.DateTime from,
                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.vote.FederatingVoteList ret = getVoteList();

        for (org.osid.voting.VoteLookupSession session : getSessions()) {
            ret.addVoteList(session.getVotesForVoterOnDate(resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of votes corresponding to a candidate
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  votes or an error results. Otherwise, the returned list
     *  may contain only those votes that are accessible
     *  through this session.
     *
     *  In effective mode, votes are returned that are
     *  currently effective.  In any effective mode, effective
     *  votes and those currently expired are returned.
     *
     *  @param  candidateId the <code>Id</code> of the candidate
     *  @return the returned <code>VoteList</code>
     *  @throws org.osid.NullArgumentException <code>candidateId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.voting.VoteList getVotesForCandidate(org.osid.id.Id candidateId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.vote.FederatingVoteList ret = getVoteList();

        for (org.osid.voting.VoteLookupSession session : getSessions()) {
            ret.addVoteList(session.getVotesForCandidate(candidateId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of votes corresponding to a candidate
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  votes or an error results. Otherwise, the returned list
     *  may contain only those votes that are accessible
     *  through this session.
     *
     *  In effective mode, votes are returned that are
     *  currently effective.  In any effective mode, effective
     *  votes and those currently expired are returned.
     *
     *  @param  candidateId the <code>Id</code> of the candidate
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>VoteList</code>
     *  @throws org.osid.NullArgumentException <code>candidateId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesForCandidateOnDate(org.osid.id.Id candidateId,
                                                               org.osid.calendaring.DateTime from,
                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.vote.FederatingVoteList ret = getVoteList();

        for (org.osid.voting.VoteLookupSession session : getSessions()) {
            ret.addVoteList(session.getVotesForCandidateOnDate(candidateId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of votes corresponding to voter and candidate
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  votes or an error results. Otherwise, the returned list
     *  may contain only those votes that are accessible
     *  through this session.
     *
     *  In effective mode, votes are returned that are
     *  currently effective.  In any effective mode, effective
     *  votes and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the voter
     *  @param  candidateId the <code>Id</code> of the candidate
     *  @return the returned <code>VoteList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>candidateId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesForVoterAndCandidate(org.osid.id.Id resourceId,
                                                                 org.osid.id.Id candidateId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.vote.FederatingVoteList ret = getVoteList();

        for (org.osid.voting.VoteLookupSession session : getSessions()) {
            ret.addVoteList(session.getVotesForVoterAndCandidate(resourceId, candidateId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of votes corresponding to voter and candidate
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  votes or an error results. Otherwise, the returned list
     *  may contain only those votes that are accessible
     *  through this session.
     *
     *  In effective mode, votes are returned that are
     *  currently effective.  In any effective mode, effective
     *  votes and those currently expired are returned.
     *
     *  @param  candidateId the <code>Id</code> of the candidate
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>VoteList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>candidateId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesForVoterAndCandidateOnDate(org.osid.id.Id resourceId,
                                                                       org.osid.id.Id candidateId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.vote.FederatingVoteList ret = getVoteList();

        for (org.osid.voting.VoteLookupSession session : getSessions()) {
            ret.addVoteList(session.getVotesForVoterAndCandidateOnDate(resourceId, candidateId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of votes corresponding to a race <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known votes or
     *  an error results. Otherwise, the returned list may contain
     *  only those votes that are accessible through this session.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and those
     *  currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the race
     *  @return the returned <code>VoteList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.voting.VoteList getVotesForRace(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.vote.FederatingVoteList ret = getVoteList();

        for (org.osid.voting.VoteLookupSession session : getSessions()) {
            ret.addVoteList(session.getVotesForRace(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of votes corresponding to a race
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  votes or an error results. Otherwise, the returned list
     *  may contain only those votes that are accessible
     *  through this session.
     *
     *  In effective mode, votes are returned that are
     *  currently effective.  In any effective mode, effective
     *  votes and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the race
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>VoteList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesForRaceOnDate(org.osid.id.Id resourceId,
                                                          org.osid.calendaring.DateTime from,
                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.vote.FederatingVoteList ret = getVoteList();

        for (org.osid.voting.VoteLookupSession session : getSessions()) {
            ret.addVoteList(session.getVotesForRaceOnDate(resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of votes corresponding to voter and race
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  votes or an error results. Otherwise, the returned list
     *  may contain only those votes that are accessible
     *  through this session.
     *
     *  In effective mode, votes are returned that are
     *  currently effective.  In any effective mode, effective
     *  votes and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the voter
     *  @param  raceId the <code>Id</code> of the race
     *  @return the returned <code>VoteList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>raceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesForVoterAndRace(org.osid.id.Id resourceId,
                                                            org.osid.id.Id raceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.vote.FederatingVoteList ret = getVoteList();

        for (org.osid.voting.VoteLookupSession session : getSessions()) {
            ret.addVoteList(session.getVotesForVoterAndRace(resourceId, raceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of votes corresponding to voter and race
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  votes or an error results. Otherwise, the returned list
     *  may contain only those votes that are accessible
     *  through this session.
     *
     *  In effective mode, votes are returned that are
     *  currently effective.  In any effective mode, effective
     *  votes and those currently expired are returned.
     *
     *  @param  raceId the <code>Id</code> of the race
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>VoteList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>raceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesForVoterAndRaceOnDate(org.osid.id.Id resourceId,
                                                                  org.osid.id.Id raceId,
                                                                  org.osid.calendaring.DateTime from,
                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.vote.FederatingVoteList ret = getVoteList();

        for (org.osid.voting.VoteLookupSession session : getSessions()) {
            ret.addVoteList(session.getVotesForVoterAndRaceOnDate(resourceId, raceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of votes corresponding to a ballot <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known votes or
     *  an error results. Otherwise, the returned list may contain
     *  only those votes that are accessible through this session.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and those
     *  currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the ballot
     *  @return the returned <code>VoteList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.voting.VoteList getVotesForBallot(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.vote.FederatingVoteList ret = getVoteList();

        for (org.osid.voting.VoteLookupSession session : getSessions()) {
            ret.addVoteList(session.getVotesForBallot(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of votes corresponding to a ballot
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  votes or an error results. Otherwise, the returned list
     *  may contain only those votes that are accessible
     *  through this session.
     *
     *  In effective mode, votes are returned that are
     *  currently effective.  In any effective mode, effective
     *  votes and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the ballot
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>VoteList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesForBallotOnDate(org.osid.id.Id resourceId,
                                                            org.osid.calendaring.DateTime from,
                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.vote.FederatingVoteList ret = getVoteList();

        for (org.osid.voting.VoteLookupSession session : getSessions()) {
            ret.addVoteList(session.getVotesForBallotOnDate(resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of votes corresponding to voter and ballot
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  votes or an error results. Otherwise, the returned list
     *  may contain only those votes that are accessible
     *  through this session.
     *
     *  In effective mode, votes are returned that are
     *  currently effective.  In any effective mode, effective
     *  votes and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the voter
     *  @param  ballotId the <code>Id</code> of the ballot
     *  @return the returned <code>VoteList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>ballotId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesForVoterAndBallot(org.osid.id.Id resourceId,
                                                              org.osid.id.Id ballotId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.vote.FederatingVoteList ret = getVoteList();

        for (org.osid.voting.VoteLookupSession session : getSessions()) {
            ret.addVoteList(session.getVotesForVoterAndBallot(resourceId, ballotId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of votes corresponding to voter and ballot
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  votes or an error results. Otherwise, the returned list
     *  may contain only those votes that are accessible
     *  through this session.
     *
     *  In effective mode, votes are returned that are
     *  currently effective.  In any effective mode, effective
     *  votes and those currently expired are returned.
     *
     *  @param  ballotId the <code>Id</code> of the ballot
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>VoteList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>ballotId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesForVoterAndBallotOnDate(org.osid.id.Id resourceId,
                                                                    org.osid.id.Id ballotId,
                                                                    org.osid.calendaring.DateTime from,
                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.vote.FederatingVoteList ret = getVoteList();

        for (org.osid.voting.VoteLookupSession session : getSessions()) {
            ret.addVoteList(session.getVotesForVoterAndBallotOnDate(resourceId, ballotId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Votes</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  votes or an error results. Otherwise, the returned list
     *  may contain only those votes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Votes</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotes()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.vote.FederatingVoteList ret = getVoteList();

        for (org.osid.voting.VoteLookupSession session : getSessions()) {
            ret.addVoteList(session.getVotes());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.voting.vote.FederatingVoteList getVoteList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.voting.vote.ParallelVoteList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.voting.vote.CompositeVoteList());
        }
    }
}
