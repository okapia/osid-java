//
// AbstractConfigurationSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.configuration.configuration.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractConfigurationSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.configuration.ConfigurationSearchResults {

    private org.osid.configuration.ConfigurationList configurations;
    private final org.osid.configuration.ConfigurationQueryInspector inspector;
    private final java.util.Collection<org.osid.configuration.records.ConfigurationSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractConfigurationSearchResults.
     *
     *  @param configurations the result set
     *  @param configurationQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>configurations</code>
     *          or <code>configurationQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractConfigurationSearchResults(org.osid.configuration.ConfigurationList configurations,
                                            org.osid.configuration.ConfigurationQueryInspector configurationQueryInspector) {
        nullarg(configurations, "configurations");
        nullarg(configurationQueryInspector, "configuration query inspectpr");

        this.configurations = configurations;
        this.inspector = configurationQueryInspector;

        return;
    }


    /**
     *  Gets the configuration list resulting from a search.
     *
     *  @return a configuration list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationList getConfigurations() {
        if (this.configurations == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.configuration.ConfigurationList configurations = this.configurations;
        this.configurations = null;
	return (configurations);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.configuration.ConfigurationQueryInspector getConfigurationQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  configuration search record <code> Type. </code> This method must
     *  be used to retrieve a configuration implementing the requested
     *  record.
     *
     *  @param configurationSearchRecordType a configuration search 
     *         record type 
     *  @return the configuration search
     *  @throws org.osid.NullArgumentException
     *          <code>configurationSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(configurationSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.configuration.records.ConfigurationSearchResultsRecord getConfigurationSearchResultsRecord(org.osid.type.Type configurationSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.configuration.records.ConfigurationSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(configurationSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(configurationSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record configuration search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addConfigurationRecord(org.osid.configuration.records.ConfigurationSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "configuration record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
