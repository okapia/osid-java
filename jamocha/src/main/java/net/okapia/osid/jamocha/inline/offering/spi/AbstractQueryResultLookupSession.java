//
// AbstractQueryResultLookupSession.java
//
//    An inline adapter that maps a ResultLookupSession to
//    a ResultQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.offering.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a ResultLookupSession to
 *  a ResultQuerySession.
 */

public abstract class AbstractQueryResultLookupSession
    extends net.okapia.osid.jamocha.offering.spi.AbstractResultLookupSession
    implements org.osid.offering.ResultLookupSession {

    private boolean effectiveonly = false;
    private final org.osid.offering.ResultQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryResultLookupSession.
     *
     *  @param querySession the underlying result query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryResultLookupSession(org.osid.offering.ResultQuerySession querySession) {
        nullarg(querySession, "result query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Catalogue</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Catalogue Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCatalogueId() {
        return (this.session.getCatalogueId());
    }


    /**
     *  Gets the <code>Catalogue</code> associated with this 
     *  session.
     *
     *  @return the <code>Catalogue</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Catalogue getCatalogue()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCatalogue());
    }


    /**
     *  Tests if this user can perform <code>Result</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupResults() {
        return (this.session.canSearchResults());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include results in catalogues which are children
     *  of this catalogue in the catalogue hierarchy.
     */

    @OSID @Override
    public void useFederatedCatalogueView() {
        this.session.useFederatedCatalogueView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this catalogue only.
     */

    @OSID @Override
    public void useIsolatedCatalogueView() {
        this.session.useIsolatedCatalogueView();
        return;
    }
    

    /**
     *  Only results whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveResultView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All results of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveResultView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Result</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Result</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Result</code> and
     *  retained for compatibility.
     *
     *  In effective mode, results are returned that are currently
     *  effective.  In any effective mode, effective results and
     *  those currently expired are returned.
     *
     *  @param  resultId <code>Id</code> of the
     *          <code>Result</code>
     *  @return the result
     *  @throws org.osid.NotFoundException <code>resultId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>resultId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Result getResult(org.osid.id.Id resultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.ResultQuery query = getQuery();
        query.matchId(resultId, true);
        org.osid.offering.ResultList results = this.session.getResultsByQuery(query);
        if (results.hasNext()) {
            return (results.getNextResult());
        } 
        
        throw new org.osid.NotFoundException(resultId + " not found");
    }


    /**
     *  Gets a <code>ResultList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  results specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Results</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, results are returned that are currently effective.
     *  In any effective mode, effective results and those currently expired
     *  are returned.
     *
     *  @param  resultIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Result</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>resultIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ResultList getResultsByIds(org.osid.id.IdList resultIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.ResultQuery query = getQuery();

        try (org.osid.id.IdList ids = resultIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getResultsByQuery(query));
    }


    /**
     *  Gets a <code>ResultList</code> corresponding to the given
     *  result genus <code>Type</code> which does not include
     *  results of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  results or an error results. Otherwise, the returned list
     *  may contain only those results that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, results are returned that are currently effective.
     *  In any effective mode, effective results and those currently expired
     *  are returned.
     *
     *  @param  resultGenusType a result genus type 
     *  @return the returned <code>Result</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>resultGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ResultList getResultsByGenusType(org.osid.type.Type resultGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.ResultQuery query = getQuery();
        query.matchGenusType(resultGenusType, true);
        return (this.session.getResultsByQuery(query));
    }


    /**
     *  Gets a <code>ResultList</code> corresponding to the given
     *  result genus <code>Type</code> and include any additional
     *  results with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  results or an error results. Otherwise, the returned list
     *  may contain only those results that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, results are returned that are currently
     *  effective.  In any effective mode, effective results and
     *  those currently expired are returned.
     *
     *  @param  resultGenusType a result genus type 
     *  @return the returned <code>Result</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>resultGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ResultList getResultsByParentGenusType(org.osid.type.Type resultGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.ResultQuery query = getQuery();
        query.matchParentGenusType(resultGenusType, true);
        return (this.session.getResultsByQuery(query));
    }


    /**
     *  Gets a <code>ResultList</code> containing the given
     *  result record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  results or an error results. Otherwise, the returned list
     *  may contain only those results that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, results are returned that are currently
     *  effective.  In any effective mode, effective results and
     *  those currently expired are returned.
     *
     *  @param  resultRecordType a result record type 
     *  @return the returned <code>Result</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>resultRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ResultList getResultsByRecordType(org.osid.type.Type resultRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.ResultQuery query = getQuery();
        query.matchRecordType(resultRecordType, true);
        return (this.session.getResultsByQuery(query));
    }


    /**
     *  Gets a <code>ResultList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  results or an error results. Otherwise, the returned list
     *  may contain only those results that are accessible
     *  through this session.
     *  
     *  In effective mode, results are returned that are currently
     *  effective.  In any effective mode, effective results and
     *  those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Result</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.offering.ResultList getResultsOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.ResultQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getResultsByQuery(query));
    }
        
    
    /**
     *  Gets all <code>Results</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  results or an error results. Otherwise, the returned list
     *  may contain only those results that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, results are returned that are currently
     *  effective.  In any effective mode, effective results and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Results</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ResultList getResults()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {


        org.osid.offering.ResultQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getResultsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.offering.ResultQuery getQuery() {
        org.osid.offering.ResultQuery query = this.session.getResultQuery();
        
        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }

        return (query);
    }
}
