//
// AbstractMapDeviceEnablerLookupSession
//
//    A simple framework for providing a DeviceEnabler lookup service
//    backed by a fixed collection of device enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a DeviceEnabler lookup service backed by a
 *  fixed collection of device enablers. The device enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>DeviceEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapDeviceEnablerLookupSession
    extends net.okapia.osid.jamocha.control.rules.spi.AbstractDeviceEnablerLookupSession
    implements org.osid.control.rules.DeviceEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.control.rules.DeviceEnabler> deviceEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.control.rules.DeviceEnabler>());


    /**
     *  Makes a <code>DeviceEnabler</code> available in this session.
     *
     *  @param  deviceEnabler a device enabler
     *  @throws org.osid.NullArgumentException <code>deviceEnabler<code>
     *          is <code>null</code>
     */

    protected void putDeviceEnabler(org.osid.control.rules.DeviceEnabler deviceEnabler) {
        this.deviceEnablers.put(deviceEnabler.getId(), deviceEnabler);
        return;
    }


    /**
     *  Makes an array of device enablers available in this session.
     *
     *  @param  deviceEnablers an array of device enablers
     *  @throws org.osid.NullArgumentException <code>deviceEnablers<code>
     *          is <code>null</code>
     */

    protected void putDeviceEnablers(org.osid.control.rules.DeviceEnabler[] deviceEnablers) {
        putDeviceEnablers(java.util.Arrays.asList(deviceEnablers));
        return;
    }


    /**
     *  Makes a collection of device enablers available in this session.
     *
     *  @param  deviceEnablers a collection of device enablers
     *  @throws org.osid.NullArgumentException <code>deviceEnablers<code>
     *          is <code>null</code>
     */

    protected void putDeviceEnablers(java.util.Collection<? extends org.osid.control.rules.DeviceEnabler> deviceEnablers) {
        for (org.osid.control.rules.DeviceEnabler deviceEnabler : deviceEnablers) {
            this.deviceEnablers.put(deviceEnabler.getId(), deviceEnabler);
        }

        return;
    }


    /**
     *  Removes a DeviceEnabler from this session.
     *
     *  @param  deviceEnablerId the <code>Id</code> of the device enabler
     *  @throws org.osid.NullArgumentException <code>deviceEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeDeviceEnabler(org.osid.id.Id deviceEnablerId) {
        this.deviceEnablers.remove(deviceEnablerId);
        return;
    }


    /**
     *  Gets the <code>DeviceEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  deviceEnablerId <code>Id</code> of the <code>DeviceEnabler</code>
     *  @return the deviceEnabler
     *  @throws org.osid.NotFoundException <code>deviceEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>deviceEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnabler getDeviceEnabler(org.osid.id.Id deviceEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.control.rules.DeviceEnabler deviceEnabler = this.deviceEnablers.get(deviceEnablerId);
        if (deviceEnabler == null) {
            throw new org.osid.NotFoundException("deviceEnabler not found: " + deviceEnablerId);
        }

        return (deviceEnabler);
    }


    /**
     *  Gets all <code>DeviceEnablers</code>. In plenary mode, the returned
     *  list contains all known deviceEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  deviceEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>DeviceEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerList getDeviceEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.control.rules.deviceenabler.ArrayDeviceEnablerList(this.deviceEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.deviceEnablers.clear();
        super.close();
        return;
    }
}
