//
// AbstractAdapterJobConstrainerEnablerLookupSession.java
//
//    A JobConstrainerEnabler lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.resourcing.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A JobConstrainerEnabler lookup session adapter.
 */

public abstract class AbstractAdapterJobConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.resourcing.rules.JobConstrainerEnablerLookupSession {

    private final org.osid.resourcing.rules.JobConstrainerEnablerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterJobConstrainerEnablerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterJobConstrainerEnablerLookupSession(org.osid.resourcing.rules.JobConstrainerEnablerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Foundry/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Foundry Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFoundryId() {
        return (this.session.getFoundryId());
    }


    /**
     *  Gets the {@code Foundry} associated with this session.
     *
     *  @return the {@code Foundry} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Foundry getFoundry()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getFoundry());
    }


    /**
     *  Tests if this user can perform {@code JobConstrainerEnabler} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupJobConstrainerEnablers() {
        return (this.session.canLookupJobConstrainerEnablers());
    }


    /**
     *  A complete view of the {@code JobConstrainerEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeJobConstrainerEnablerView() {
        this.session.useComparativeJobConstrainerEnablerView();
        return;
    }


    /**
     *  A complete view of the {@code JobConstrainerEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryJobConstrainerEnablerView() {
        this.session.usePlenaryJobConstrainerEnablerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include job constrainer enablers in foundries which are children
     *  of this foundry in the foundry hierarchy.
     */

    @OSID @Override
    public void useFederatedFoundryView() {
        this.session.useFederatedFoundryView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this foundry only.
     */

    @OSID @Override
    public void useIsolatedFoundryView() {
        this.session.useIsolatedFoundryView();
        return;
    }
    

    /**
     *  Only active job constrainer enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveJobConstrainerEnablerView() {
        this.session.useActiveJobConstrainerEnablerView();
        return;
    }


    /**
     *  Active and inactive job constrainer enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusJobConstrainerEnablerView() {
        this.session.useAnyStatusJobConstrainerEnablerView();
        return;
    }
    
     
    /**
     *  Gets the {@code JobConstrainerEnabler} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code JobConstrainerEnabler} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code JobConstrainerEnabler} and
     *  retained for compatibility.
     *
     *  In active mode, job constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive job constrainer enablers
     *  are returned.
     *
     *  @param jobConstrainerEnablerId {@code Id} of the {@code JobConstrainerEnabler}
     *  @return the job constrainer enabler
     *  @throws org.osid.NotFoundException {@code jobConstrainerEnablerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code jobConstrainerEnablerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnabler getJobConstrainerEnabler(org.osid.id.Id jobConstrainerEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJobConstrainerEnabler(jobConstrainerEnablerId));
    }


    /**
     *  Gets a {@code JobConstrainerEnablerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  jobConstrainerEnablers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code JobConstrainerEnablers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, job constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive job constrainer enablers
     *  are returned.
     *
     *  @param  jobConstrainerEnablerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code JobConstrainerEnabler} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code jobConstrainerEnablerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerList getJobConstrainerEnablersByIds(org.osid.id.IdList jobConstrainerEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJobConstrainerEnablersByIds(jobConstrainerEnablerIds));
    }


    /**
     *  Gets a {@code JobConstrainerEnablerList} corresponding to the given
     *  job constrainer enabler genus {@code Type} which does not include
     *  job constrainer enablers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  job constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those job constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, job constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive job constrainer enablers
     *  are returned.
     *
     *  @param  jobConstrainerEnablerGenusType a jobConstrainerEnabler genus type 
     *  @return the returned {@code JobConstrainerEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code jobConstrainerEnablerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerList getJobConstrainerEnablersByGenusType(org.osid.type.Type jobConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJobConstrainerEnablersByGenusType(jobConstrainerEnablerGenusType));
    }


    /**
     *  Gets a {@code JobConstrainerEnablerList} corresponding to the given
     *  job constrainer enabler genus {@code Type} and include any additional
     *  job constrainer enablers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  job constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those job constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, job constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive job constrainer enablers
     *  are returned.
     *
     *  @param  jobConstrainerEnablerGenusType a jobConstrainerEnabler genus type 
     *  @return the returned {@code JobConstrainerEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code jobConstrainerEnablerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerList getJobConstrainerEnablersByParentGenusType(org.osid.type.Type jobConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJobConstrainerEnablersByParentGenusType(jobConstrainerEnablerGenusType));
    }


    /**
     *  Gets a {@code JobConstrainerEnablerList} containing the given
     *  job constrainer enabler record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  job constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those job constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, job constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive job constrainer enablers
     *  are returned.
     *
     *  @param  jobConstrainerEnablerRecordType a jobConstrainerEnabler record type 
     *  @return the returned {@code JobConstrainerEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code jobConstrainerEnablerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerList getJobConstrainerEnablersByRecordType(org.osid.type.Type jobConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJobConstrainerEnablersByRecordType(jobConstrainerEnablerRecordType));
    }


    /**
     *  Gets a {@code JobConstrainerEnablerList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  job constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those job constrainer enablers that are accessible
     *  through this session.
     *  
     *  In active mode, job constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive job constrainer enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code JobConstrainerEnabler} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerList getJobConstrainerEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJobConstrainerEnablersOnDate(from, to));
    }
        

    /**
     *  Gets a {@code JobConstrainerEnablerList } which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  job constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those job constrainer enablers that are accessible
     *  through this session.
     *
     *  In active mode, job constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive job constrainer enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned {@code JobConstrainerEnabler} list
     *  @throws org.osid.InvalidArgumentException {@code from} is
     *          greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code agent},
     *          {@code from}, or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerList getJobConstrainerEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (this.session.getJobConstrainerEnablersOnDateWithAgent(agentId, from, to));
    }


    /**
     *  Gets all {@code JobConstrainerEnablers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  job constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those job constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, job constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive job constrainer enablers
     *  are returned.
     *
     *  @return a list of {@code JobConstrainerEnablers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerList getJobConstrainerEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJobConstrainerEnablers());
    }
}
