//
// AbstractIndexedMapDocetLookupSession.java
//
//    A simple framework for providing a Docet lookup service
//    backed by a fixed collection of docets with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.syllabus.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Docet lookup service backed by a
 *  fixed collection of docets. The docets are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some docets may be compatible
 *  with more types than are indicated through these docet
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Docets</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapDocetLookupSession
    extends AbstractMapDocetLookupSession
    implements org.osid.course.syllabus.DocetLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.course.syllabus.Docet> docetsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.syllabus.Docet>());
    private final MultiMap<org.osid.type.Type, org.osid.course.syllabus.Docet> docetsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.syllabus.Docet>());


    /**
     *  Makes a <code>Docet</code> available in this session.
     *
     *  @param  docet a docet
     *  @throws org.osid.NullArgumentException <code>docet<code> is
     *          <code>null</code>
     */

    @Override
    protected void putDocet(org.osid.course.syllabus.Docet docet) {
        super.putDocet(docet);

        this.docetsByGenus.put(docet.getGenusType(), docet);
        
        try (org.osid.type.TypeList types = docet.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.docetsByRecord.put(types.getNextType(), docet);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a docet from this session.
     *
     *  @param docetId the <code>Id</code> of the docet
     *  @throws org.osid.NullArgumentException <code>docetId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeDocet(org.osid.id.Id docetId) {
        org.osid.course.syllabus.Docet docet;
        try {
            docet = getDocet(docetId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.docetsByGenus.remove(docet.getGenusType());

        try (org.osid.type.TypeList types = docet.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.docetsByRecord.remove(types.getNextType(), docet);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeDocet(docetId);
        return;
    }


    /**
     *  Gets a <code>DocetList</code> corresponding to the given
     *  docet genus <code>Type</code> which does not include
     *  docets of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known docets or an error results. Otherwise,
     *  the returned list may contain only those docets that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  docetGenusType a docet genus type 
     *  @return the returned <code>Docet</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>docetGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetList getDocetsByGenusType(org.osid.type.Type docetGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.syllabus.docet.ArrayDocetList(this.docetsByGenus.get(docetGenusType)));
    }


    /**
     *  Gets a <code>DocetList</code> containing the given
     *  docet record <code>Type</code>. In plenary mode, the
     *  returned list contains all known docets or an error
     *  results. Otherwise, the returned list may contain only those
     *  docets that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  docetRecordType a docet record type 
     *  @return the returned <code>docet</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>docetRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetList getDocetsByRecordType(org.osid.type.Type docetRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.syllabus.docet.ArrayDocetList(this.docetsByRecord.get(docetRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.docetsByGenus.clear();
        this.docetsByRecord.clear();

        super.close();

        return;
    }
}
