//
// AbstractOsidObjectBuilder.java
//
//     Defines a builder for an OSID Object.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.spi;


/**
 *  Defines the OsidObject builder.
 */

public abstract class AbstractOsidObjectBuilder<T extends AbstractOsidObjectBuilder<? extends T>>
    extends AbstractIdentifiableBuilder<T> {

    private final OsidObjectMiter object;


    /**
     *  Creates a new <code>AbstractOsidObjectBuilder</code>.
     *
     *  @param object an object miter interface
     *  @throws org.osid.NullArgumentException <code>object</code> is
     *          <code>null</code>
     */

    protected AbstractOsidObjectBuilder(OsidObjectMiter object) {
        super(object);
        this.object = object;
        return;
    }


    /**
     *  Sets the displayName for this object.
     *
     *  @param displayName the name for this object
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */
    
    public T displayName(org.osid.locale.DisplayText displayName) {
        this.object.setDisplayName(displayName);
        return (self());
    }
    

    /**
     *  Sets the description of this object.
     *
     *  @param description the description of this object
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */
    
    public T description(org.osid.locale.DisplayText description) {
        this.object.setDescription(description);
        return (self());
    }
    
    
    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */
    
    public T genus(org.osid.type.Type genusType) {
        this.object.setGenusType(genusType);
        return (self());
    }
    
    
    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    public T recordType(org.osid.type.Type recordType) {
        this.object.addRecordType(recordType);
        return (self());
    }


    /**
     *  Adds a properties set.
     *
     *  @param set
     *  @param recordType
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */
    
    public T properties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        this.object.addProperties(set, recordType);
        return (self());
    }
}
