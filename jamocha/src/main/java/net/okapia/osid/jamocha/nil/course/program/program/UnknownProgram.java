//
// UnknownProgram.java
//
//     Defines an unknown Program.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.course.program.program;


/**
 *  Defines an unknown <code>Program</code>.
 */

public final class UnknownProgram
    extends net.okapia.osid.jamocha.nil.course.program.program.spi.AbstractUnknownProgram
    implements org.osid.course.program.Program {


    /**
     *  Constructs a new <code>UnknownProgram</code>.
     */

    public UnknownProgram() {
        return;
    }


    /**
     *  Constructs a new <code>UnknownProgram</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public UnknownProgram(boolean optional) {
        super(optional);
        addProgramRecord(new ProgramRecord(), net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
        return;
    }


    /**
     *  Gets an unknown Program.
     *
     *  @return an unknown Program
     */

    public static org.osid.course.program.Program create() {
        return (net.okapia.osid.jamocha.builder.validator.course.program.program.ProgramValidator.validateProgram(new UnknownProgram()));
    }


    public class ProgramRecord 
        extends net.okapia.osid.jamocha.spi.AbstractOsidRecord
        implements org.osid.course.program.records.ProgramRecord {

        
        protected ProgramRecord() {
            addRecordType(net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
            return;
        }
    }        
}
