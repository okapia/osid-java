//
// AbstractStateSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.process.state.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractStateSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.process.StateSearchResults {

    private org.osid.process.StateList states;
    private final org.osid.process.StateQueryInspector inspector;
    private final java.util.Collection<org.osid.process.records.StateSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractStateSearchResults.
     *
     *  @param states the result set
     *  @param stateQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>states</code>
     *          or <code>stateQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractStateSearchResults(org.osid.process.StateList states,
                                            org.osid.process.StateQueryInspector stateQueryInspector) {
        nullarg(states, "states");
        nullarg(stateQueryInspector, "state query inspectpr");

        this.states = states;
        this.inspector = stateQueryInspector;

        return;
    }


    /**
     *  Gets the state list resulting from a search.
     *
     *  @return a state list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.process.StateList getStates() {
        if (this.states == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.process.StateList states = this.states;
        this.states = null;
	return (states);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.process.StateQueryInspector getStateQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  state search record <code> Type. </code> This method must
     *  be used to retrieve a state implementing the requested
     *  record.
     *
     *  @param stateSearchRecordType a state search 
     *         record type 
     *  @return the state search
     *  @throws org.osid.NullArgumentException
     *          <code>stateSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(stateSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.process.records.StateSearchResultsRecord getStateSearchResultsRecord(org.osid.type.Type stateSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.process.records.StateSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(stateSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(stateSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record state search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addStateRecord(org.osid.process.records.StateSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "state record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
