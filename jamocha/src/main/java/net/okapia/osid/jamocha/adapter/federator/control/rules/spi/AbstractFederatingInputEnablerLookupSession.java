//
// AbstractFederatingInputEnablerLookupSession.java
//
//     An abstract federating adapter for an InputEnablerLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.control.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  InputEnablerLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingInputEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.control.rules.InputEnablerLookupSession>
    implements org.osid.control.rules.InputEnablerLookupSession {

    private boolean parallel = false;
    private org.osid.control.System system = new net.okapia.osid.jamocha.nil.control.system.UnknownSystem();


    /**
     *  Constructs a new <code>AbstractFederatingInputEnablerLookupSession</code>.
     */

    protected AbstractFederatingInputEnablerLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.control.rules.InputEnablerLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>System/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>System Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getSystemId() {
        return (this.system.getId());
    }


    /**
     *  Gets the <code>System</code> associated with this 
     *  session.
     *
     *  @return the <code>System</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.System getSystem()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.system);
    }


    /**
     *  Sets the <code>System</code>.
     *
     *  @param  system the system for this session
     *  @throws org.osid.NullArgumentException <code>system</code>
     *          is <code>null</code>
     */

    protected void setSystem(org.osid.control.System system) {
        nullarg(system, "system");
        this.system = system;
        return;
    }


    /**
     *  Tests if this user can perform <code>InputEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupInputEnablers() {
        for (org.osid.control.rules.InputEnablerLookupSession session : getSessions()) {
            if (session.canLookupInputEnablers()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>InputEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeInputEnablerView() {
        for (org.osid.control.rules.InputEnablerLookupSession session : getSessions()) {
            session.useComparativeInputEnablerView();
        }

        return;
    }


    /**
     *  A complete view of the <code>InputEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryInputEnablerView() {
        for (org.osid.control.rules.InputEnablerLookupSession session : getSessions()) {
            session.usePlenaryInputEnablerView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include input enablers in systems which are children
     *  of this system in the system hierarchy.
     */

    @OSID @Override
    public void useFederatedSystemView() {
        for (org.osid.control.rules.InputEnablerLookupSession session : getSessions()) {
            session.useFederatedSystemView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this system only.
     */

    @OSID @Override
    public void useIsolatedSystemView() {
        for (org.osid.control.rules.InputEnablerLookupSession session : getSessions()) {
            session.useIsolatedSystemView();
        }

        return;
    }


    /**
     *  Only active input enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveInputEnablerView() {
        for (org.osid.control.rules.InputEnablerLookupSession session : getSessions()) {
            session.useActiveInputEnablerView();
        }

        return;
    }


    /**
     *  Active and inactive input enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusInputEnablerView() {
        for (org.osid.control.rules.InputEnablerLookupSession session : getSessions()) {
            session.useAnyStatusInputEnablerView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>InputEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>InputEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>InputEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, input enablers are returned that are currently
     *  active. In any status mode, active and inactive input enablers
     *  are returned.
     *
     *  @param  inputEnablerId <code>Id</code> of the
     *          <code>InputEnabler</code>
     *  @return the input enabler
     *  @throws org.osid.NotFoundException <code>inputEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>inputEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnabler getInputEnabler(org.osid.id.Id inputEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.control.rules.InputEnablerLookupSession session : getSessions()) {
            try {
                return (session.getInputEnabler(inputEnablerId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(inputEnablerId + " not found");
    }


    /**
     *  Gets an <code>InputEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  inputEnablers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>InputEnablers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, input enablers are returned that are currently
     *  active. In any status mode, active and inactive input enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getInputEnablers()</code>.
     *
     *  @param  inputEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>InputEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>inputEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerList getInputEnablersByIds(org.osid.id.IdList inputEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.control.rules.inputenabler.MutableInputEnablerList ret = new net.okapia.osid.jamocha.control.rules.inputenabler.MutableInputEnablerList();

        try (org.osid.id.IdList ids = inputEnablerIds) {
            while (ids.hasNext()) {
                ret.addInputEnabler(getInputEnabler(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>InputEnablerList</code> corresponding to the given
     *  input enabler genus <code>Type</code> which does not include
     *  input enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  input enablers or an error results. Otherwise, the returned list
     *  may contain only those input enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, input enablers are returned that are currently
     *  active. In any status mode, active and inactive input enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getInputEnablers()</code>.
     *
     *  @param  inputEnablerGenusType an inputEnabler genus type 
     *  @return the returned <code>InputEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>inputEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerList getInputEnablersByGenusType(org.osid.type.Type inputEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.control.rules.inputenabler.FederatingInputEnablerList ret = getInputEnablerList();

        for (org.osid.control.rules.InputEnablerLookupSession session : getSessions()) {
            ret.addInputEnablerList(session.getInputEnablersByGenusType(inputEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>InputEnablerList</code> corresponding to the given
     *  input enabler genus <code>Type</code> and include any additional
     *  input enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  input enablers or an error results. Otherwise, the returned list
     *  may contain only those input enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, input enablers are returned that are currently
     *  active. In any status mode, active and inactive input enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getInputEnablers()</code>.
     *
     *  @param  inputEnablerGenusType an inputEnabler genus type 
     *  @return the returned <code>InputEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>inputEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerList getInputEnablersByParentGenusType(org.osid.type.Type inputEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.control.rules.inputenabler.FederatingInputEnablerList ret = getInputEnablerList();

        for (org.osid.control.rules.InputEnablerLookupSession session : getSessions()) {
            ret.addInputEnablerList(session.getInputEnablersByParentGenusType(inputEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>InputEnablerList</code> containing the given
     *  input enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  input enablers or an error results. Otherwise, the returned list
     *  may contain only those input enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, input enablers are returned that are currently
     *  active. In any status mode, active and inactive input enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getInputEnablers()</code>.
     *
     *  @param  inputEnablerRecordType an inputEnabler record type 
     *  @return the returned <code>InputEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>inputEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerList getInputEnablersByRecordType(org.osid.type.Type inputEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.control.rules.inputenabler.FederatingInputEnablerList ret = getInputEnablerList();

        for (org.osid.control.rules.InputEnablerLookupSession session : getSessions()) {
            ret.addInputEnablerList(session.getInputEnablersByRecordType(inputEnablerRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>InputEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  input enablers or an error results. Otherwise, the returned list
     *  may contain only those input enablers that are accessible
     *  through this session.
     *  
     *  In active mode, input enablers are returned that are currently
     *  active. In any status mode, active and inactive input enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>InputEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.control.rules.InputEnablerList getInputEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.control.rules.inputenabler.FederatingInputEnablerList ret = getInputEnablerList();

        for (org.osid.control.rules.InputEnablerLookupSession session : getSessions()) {
            ret.addInputEnablerList(session.getInputEnablersOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets an <code>InputEnablerList </code> which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  input enablers or an error results. Otherwise, the returned list
     *  may contain only those input enablers that are accessible
     *  through this session.
     *
     *  In active mode, input enablers are returned that are currently
     *  active. In any status mode, active and inactive input enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>InputEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerList getInputEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                                   org.osid.calendaring.DateTime from,
                                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.control.rules.inputenabler.FederatingInputEnablerList ret = getInputEnablerList();

        for (org.osid.control.rules.InputEnablerLookupSession session : getSessions()) {
            ret.addInputEnablerList(session.getInputEnablersOnDateWithAgent(agentId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>InputEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  input enablers or an error results. Otherwise, the returned list
     *  may contain only those input enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, input enablers are returned that are currently
     *  active. In any status mode, active and inactive input enablers
     *  are returned.
     *
     *  @return a list of <code>InputEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerList getInputEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.control.rules.inputenabler.FederatingInputEnablerList ret = getInputEnablerList();

        for (org.osid.control.rules.InputEnablerLookupSession session : getSessions()) {
            ret.addInputEnablerList(session.getInputEnablers());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.control.rules.inputenabler.FederatingInputEnablerList getInputEnablerList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.control.rules.inputenabler.ParallelInputEnablerList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.control.rules.inputenabler.CompositeInputEnablerList());
        }
    }
}
