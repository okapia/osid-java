//
// AbstractQueryPackageLookupSession.java
//
//    An inline adapter that maps a PackageLookupSession to
//    a PackageQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.installation.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a PackageLookupSession to
 *  a PackageQuerySession.
 */

public abstract class AbstractQueryPackageLookupSession
    extends net.okapia.osid.jamocha.installation.spi.AbstractPackageLookupSession
    implements org.osid.installation.PackageLookupSession {

    private final org.osid.installation.PackageQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryPackageLookupSession.
     *
     *  @param querySession the underlying package query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryPackageLookupSession(org.osid.installation.PackageQuerySession querySession) {
        nullarg(querySession, "package query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Depot</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Depot Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDepotId() {
        return (this.session.getDepotId());
    }


    /**
     *  Gets the <code>Depot</code> associated with this 
     *  session.
     *
     *  @return the <code>Depot</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.Depot getDepot()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getDepot());
    }


    /**
     *  Tests if this user can perform <code>Package</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPackages() {
        return (this.session.canSearchPackages());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include packages in depots which are children
     *  of this depot in the depot hierarchy.
     */

    @OSID @Override
    public void useFederatedDepotView() {
        this.session.useFederatedDepotView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this depot only.
     */

    @OSID @Override
    public void useIsolatedDepotView() {
        this.session.useIsolatedDepotView();
        return;
    }
    

    /**
     *  The returns from the lookup methods may omit multiple versions of the 
     *  same package. 
     */

    @OSID @Override
    public void useNormalizedVersionView() {
        this.session.useNormalizedVersionView();
        return;
    }


    /**
     *  All versions of the same package are returned. 
     */

    @OSID @Override
    public void useDenormalizedVersionView() {
        this.session.useDenormalizedVersionView();
        return;
    }


    /**
     *  Gets the <code>Package</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Package</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Package</code> and
     *  retained for compatibility.
     *
     *  @param  pkgId <code>Id</code> of the
     *          <code>Package</code>
     *  @return the package
     *  @throws org.osid.NotFoundException <code>pkgId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>pkgId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.Package getPackage(org.osid.id.Id pkgId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.installation.PackageQuery query = getQuery();
        query.matchId(pkgId, true);
        org.osid.installation.PackageList packages = this.session.getPackagesByQuery(query);
        if (packages.hasNext()) {
            return (packages.getNextPackage());
        } 
        
        throw new org.osid.NotFoundException(pkgId + " not found");
    }


    /**
     *  Gets a <code>PackageList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  packages specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Packages</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  pkgIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Package</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>pkgIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.PackageList getPackagesByIds(org.osid.id.IdList pkgIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.installation.PackageQuery query = getQuery();

        try (org.osid.id.IdList ids = pkgIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getPackagesByQuery(query));
    }


    /**
     *  Gets a <code>PackageList</code> corresponding to the given
     *  package genus <code>Type</code> which does not include
     *  packages of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  packages or an error results. Otherwise, the returned list
     *  may contain only those packages that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  pkgGenusType a pkg genus type 
     *  @return the returned <code>Package</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pkgGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.PackageList getPackagesByGenusType(org.osid.type.Type pkgGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.installation.PackageQuery query = getQuery();
        query.matchGenusType(pkgGenusType, true);
        return (this.session.getPackagesByQuery(query));
    }


    /**
     *  Gets a <code>PackageList</code> corresponding to the given
     *  package genus <code>Type</code> and include any additional
     *  packages with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  packages or an error results. Otherwise, the returned list
     *  may contain only those packages that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  pkgGenusType a pkg genus type 
     *  @return the returned <code>Package</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pkgGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.PackageList getPackagesByParentGenusType(org.osid.type.Type pkgGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.installation.PackageQuery query = getQuery();
        query.matchParentGenusType(pkgGenusType, true);
        return (this.session.getPackagesByQuery(query));
    }


    /**
     *  Gets a <code>PackageList</code> containing the given
     *  package record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  packages or an error results. Otherwise, the returned list
     *  may contain only those packages that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  pkgRecordType a pkg record type 
     *  @return the returned <code>Package</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pkgRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.PackageList getPackagesByRecordType(org.osid.type.Type pkgRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.installation.PackageQuery query = getQuery();
        query.matchRecordType(pkgRecordType, true);
        return (this.session.getPackagesByQuery(query));
    }


    /**
     *  Gets a <code>PackageList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known packages or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  packages that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Package</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.installation.PackageList getPackagesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.installation.PackageQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getPackagesByQuery(query));        
    }

    
    /**
     *  Gets a list of packages depending on the given package. 
     *
     *  @param  packageId an <code> Id </code> of a <code> Package </code> 
     *  @return list of package dependents 
     *  @throws org.osid.NotFoundException <code> packageId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> packageId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.installation.PackageList getDependentPackages(org.osid.id.Id packageId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.installation.PackageQuery query = getQuery();
        query.matchDependencyId(packageId, true);
        return (this.session.getPackagesByQuery(query));
    }


    /**
     *  Gets a list of packages in the specified package version
     *  chain.
     *
     *  @param  packageId an <code> Id </code> of a <code> Package </code> 
     *  @return list of dependencies
     *  @throws org.osid.NotFoundException <code> packageId </code> is
     *          not found
     *  @throws org.osid.NullArgumentException <code> packageId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.installation.PackageList getPackageVersions(org.osid.id.Id packageId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.installation.PackageQuery query = getQuery();
        query.matchVersionedPackageId(packageId, true);
        return (this.session.getPackagesByQuery(query));
    }


    /**
     *  Gets all <code>Packages</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  packages or an error results. Otherwise, the returned list
     *  may contain only those packages that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Packages</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.PackageList getPackages()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.installation.PackageQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getPackagesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.installation.PackageQuery getQuery() {
        org.osid.installation.PackageQuery query = this.session.getPackageQuery();
        return (query);
    }
}
