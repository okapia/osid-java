//
// SingleRelationshipEnablerList.java
//
//     Implements a RelationshipEnablerList based on a single RelationshipEnabler.
//
//
// Tom Coppeto
// OnTapSolutions
// 29 June 2008
//
//
// Copyright (c) 2008, 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.relationship.rules.relationshipenabler;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a ObjectList based on a single RelationshipEnabler.
 */

public final class SingleRelationshipEnablerList
    extends net.okapia.osid.jamocha.relationship.rules.relationshipenabler.spi.AbstractRelationshipEnablerList
    implements org.osid.relationship.rules.RelationshipEnablerList {

    private final org.osid.relationship.rules.RelationshipEnabler relationshipEnabler;
    private boolean read = false;


    /**
     *  Creates a new <code>SingleRelationshipEnablerList</code>.
     *
     *  @param relationshipEnabler a relationshipenabler
     *  @throws org.osid.NullArgumentException <code>relationshipEnabler</code>
     *          is <code>null</code>
     */

    public SingleRelationshipEnablerList(org.osid.relationship.rules.RelationshipEnabler relationshipEnabler) {
        nullarg(relationshipEnabler, "relationshipenabler");
        this.relationshipEnabler = relationshipEnabler;
        return;
    }


    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code>true</code> if more elements are available in
     *          this list, <code>false</code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (this.read) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code>RelationshipEnabler</code> in this list. 
     *
     *  @return the next <code>RelationshipEnabler</code> in this list. The
     *          <code> hasNext() </code> method should be used to test
     *          that a next <code>RelationshipEnabler</code> is available before
     *          calling this method.
     *  @throws org.osid.IllegalStateException no more elements
     *          available in this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnabler getNextRelationshipEnabler()
        throws org.osid.OperationFailedException {

        if (hasNext()) {
            this.read = true;
            return (this.relationshipEnabler);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in relationshipenabler list");
        }
    }
}
