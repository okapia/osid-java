//
// AbstractImmutableItem.java
//
//     Wraps a mutable Item to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.inventory.item.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Item</code> to hide modifiers. This
 *  wrapper provides an immutized Item from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying item whose state changes are visible.
 */

public abstract class AbstractImmutableItem
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.inventory.Item {

    private final org.osid.inventory.Item item;


    /**
     *  Constructs a new <code>AbstractImmutableItem</code>.
     *
     *  @param item the item to immutablize
     *  @throws org.osid.NullArgumentException <code>item</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableItem(org.osid.inventory.Item item) {
        super(item);
        this.item = item;
        return;
    }


    /**
     *  Gets the stock <code> Id </code> to which this item belongs. 
     *
     *  @return a stock <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getStockId() {
        return (this.item.getStockId());
    }


    /**
     *  Gets the stock to which this item belongs. 
     *
     *  @return a stock 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.inventory.Stock getStock()
        throws org.osid.OperationFailedException {

        return (this.item.getStock());
    }


    /**
     *  Gets the property identification number for this item. 
     *
     *  @return a property tag 
     */

    @OSID @Override
    public String getPropertyTag() {
        return (this.item.getPropertyTag());
    }


    /**
     *  Gets the serial number for this item. 
     *
     *  @return a serial number 
     */

    @OSID @Override
    public String getSerialNumber() {
        return (this.item.getSerialNumber());
    }


    /**
     *  Gets a display string for the location. 
     *
     *  @return a location descrption 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getLocationDescription() {
        return (this.item.getLocationDescription());
    }


    /**
     *  Tests if this item has a known location. 
     *
     *  @return <code> true </code> if a location is associated with this 
     *          item, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasLocation() {
        return (this.item.hasLocation());
    }


    /**
     *  Gets the location <code> Id </code> to which this item belongs. 
     *
     *  @return a location <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasLocation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getLocationId() {
        return (this.item.getLocationId());
    }


    /**
     *  Gets the location to which this item belongs. 
     *
     *  @return a location 
     *  @throws org.osid.IllegalStateException <code> hasLocation() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.mapping.Location getLocation()
        throws org.osid.OperationFailedException {

        return (this.item.getLocation());
    }


    /**
     *  Tests if this item is a part of another item. 
     *
     *  @return <code> true </code> if this item is a part of another item, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean isPart() {
        return (this.item.isPart());
    }


    /**
     *  Gets the item <code> Id </code> to which this item belongs. 
     *
     *  @return a item <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isPart() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getItemId() {
        return (this.item.getItemId());
    }


    /**
     *  Gets the item to which this item belongs. 
     *
     *  @return a item 
     *  @throws org.osid.IllegalStateException <code> isPart() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.inventory.Item getItem()
        throws org.osid.OperationFailedException {

        return (this.item.getItem());
    }


    /**
     *  Gets the item record corresponding to the given <code> Item </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> itemRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(itemRecordType) </code> is <code> true </code> . 
     *
     *  @param  itemRecordType the type of item record to retrieve 
     *  @return the item record 
     *  @throws org.osid.NullArgumentException <code> itemRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(itemRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.records.ItemRecord getItemRecord(org.osid.type.Type itemRecordType)
        throws org.osid.OperationFailedException {

        return (this.item.getItemRecord(itemRecordType));
    }
}

