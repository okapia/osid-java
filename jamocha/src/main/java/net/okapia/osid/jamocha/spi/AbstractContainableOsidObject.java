//
// AbstractContainableOsidObject.java
//
//     Defines a containable OsidObject.
//
//
// Tom Coppeto
// Okapia
// 5 October 2011
//
//
// Copyright (c) 2011 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a containable <code>OsidObject</code>.
 */

public abstract class AbstractContainableOsidObject
    extends AbstractOsidObject
    implements org.osid.Containable,
               org.osid.OsidObject {

    private final Containable containable = new Containable();


    /**
     *  Tests if this <code> Containable </code> is sequestered in
     *  that it should not appear outside of its aggregated
     *  composition.
     *
     *  @return <code> true </code> if this containable is
     *          sequestered, <code> false </code> if this containable
     *          may appear outside its aggregate
     */

    @OSID @Override
    public boolean isSequestered() {
        return (this.containable.isSequestered());
    }


    /**
     *  Sets the sequestered flag.
     *
     *  @param sequestered <code> true </code> if this containable is
     *         sequestered, <code> false </code> if this containable
     *         may appear outside its aggregate
     */

    protected void setSequestered(boolean sequestered) {
        this.containable.setSequestered(sequestered);
        return;
    }


    protected class Containable
        extends AbstractContainable
        implements org.osid.Containable {


        /**
         *  Sets the sequestered flag.
         *
         *  @param sequestered <code> true </code> if this containable is
         *         sequestered, <code> false </code> if this containable
         *         may appear outside its aggregate
         */
        
        protected void setSequestered(boolean sequestered) {
            super.setSequestered(sequestered);
            return;
        }
    }
}
