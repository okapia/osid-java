//
// AbstractVoteLookupSession.java
//
//    A starter implementation framework for providing a Vote
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Vote lookup
 *  service.
 *
 *  Although this abstract class requires only the implementation of
 *  getVotes(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractVoteLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.voting.VoteLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.voting.Polls polls = new net.okapia.osid.jamocha.nil.voting.polls.UnknownPolls();
    

    /**
     *  Gets the <code>Polls/code> <code>Id</code> associated with
     *  this session.
     *
     *  @return the <code>Polls Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getPollsId() {
        return (this.polls.getId());
    }


    /**
     *  Gets the <code>Polls</code> associated with this session.
     *
     *  @return the <code>Polls</code> associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Polls getPolls()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.polls);
    }


    /**
     *  Sets the <code>Polls</code>.
     *
     *  @param  polls the polls for this session
     *  @throws org.osid.NullArgumentException <code>polls</code>
     *          is <code>null</code>
     */

    protected void setPolls(org.osid.voting.Polls polls) {
        nullarg(polls, "polls");
        this.polls = polls;
        return;
    }


    /**
     *  Tests if this user can perform <code>Vote</code> lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupVotes() {
        return (true);
    }


    /**
     *  A complete view of the <code>Vote</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeVoteView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Vote</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryVoteView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include votes in pollses which are children of this
     *  polls in the polls hierarchy.
     */

    @OSID @Override
    public void useFederatedPollsView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this polls only.
     */

    @OSID @Override
    public void useIsolatedPollsView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only votes whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveVoteView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All votes of any effective dates are returned by all methods
     *  in this session.
     */

    @OSID @Override
    public void useAnyEffectiveVoteView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Vote</code> specified by its <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Vote</code> may have a different <code>Id</code> than
     *  requested, such as the case where a duplicate <code>Id</code>
     *  was assigned to a <code>Vote</code> and retained for
     *  compatibility.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and
     *  those currently expired are returned.
     *
     *  @param  voteId <code>Id</code> of the
     *          <code>Vote</code>
     *  @return the vote
     *  @throws org.osid.NotFoundException <code>voteId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>voteId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Vote getVote(org.osid.id.Id voteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.voting.VoteList votes = getVotes()) {
            while (votes.hasNext()) {
                org.osid.voting.Vote vote = votes.getNextVote();
                if (vote.getId().equals(voteId)) {
                    return (vote);
                }
            }
        } 

        throw new org.osid.NotFoundException(voteId + " not found");
    }


    /**
     *  Gets a <code>VoteList</code> corresponding to the given
     *  <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the votes
     *  specified in the <code>Id</code> list, in the order of the
     *  list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Votes</code> may
     *  be omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and those
     *  currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getVotes()</code>.
     *
     *  @param voteIds the list of <code>Ids</code> to retrieve
     *  @return the returned <code>Vote</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>voteIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesByIds(org.osid.id.IdList voteIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.voting.Vote> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = voteIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getVote(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("vote " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.voting.vote.LinkedVoteList(ret));
    }


    /**
     *  Gets a <code>VoteList</code> corresponding to the given vote
     *  genus <code>Type</code> which does not include votes of types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known votes or
     *  an error results. Otherwise, the returned list may contain
     *  only those votes that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and those
     *  currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getVotes()</code>.
     *
     *  @param voteGenusType a vote genus type
     *  @return the returned <code>Vote</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>voteGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesByGenusType(org.osid.type.Type voteGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.voting.vote.VoteGenusFilterList(getVotes(), voteGenusType));
    }


    /**
     *  Gets a <code>VoteList</code> corresponding to the given vote
     *  genus <code>Type</code> and include any additional votes with
     *  genus types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known votes or
     *  an error results. Otherwise, the returned list may contain
     *  only those votes that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and those
     *  currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getVotes()</code>.
     *
     *  @param voteGenusType a vote genus type
     *  @return the returned <code>Vote</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>voteGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesByParentGenusType(org.osid.type.Type voteGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getVotesByGenusType(voteGenusType));
    }


    /**
     *  Gets a <code>VoteList</code> containing the given vote record
     *  <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known votes or
     *  an error results. Otherwise, the returned list may contain
     *  only those votes that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and those
     *  currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getVotes()</code>.
     *
     *  @param  voteRecordType a vote record type 
     *  @return the returned <code>Vote</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>voteRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesByRecordType(org.osid.type.Type voteRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.voting.vote.VoteRecordFilterList(getVotes(), voteRecordType));
    }


    /**
     *  Gets a <code>VoteList</code> effective during the entire given
     *  date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known votes or
     *  an error results. Otherwise, the returned list may contain
     *  only those votes that are accessible through this session.
     *  
     *  In active mode, votes are returned that are currently
     *  active. In any status mode, active and inactive votes are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Vote</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.voting.VoteList getVotesOnDate(org.osid.calendaring.DateTime from, 
                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.voting.vote.TemporalVoteFilterList(getVotes(), from, to));
    }
        

    /**
     *  Gets a list of votes corresponding to a voter <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known votes or
     *  an error results. Otherwise, the returned list may contain
     *  only those votes that are accessible through this session.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and those
     *  currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the voter
     *  @return the returned <code>VoteList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.voting.VoteList getVotesForVoter(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.voting.vote.VoteFilterList(new VoterFilter(resourceId), getVotes()));
    }


    /**
     *  Gets a list of votes corresponding to a voter <code>Id</code>
     *  and effective during the entire given date range inclusive but
     *  not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known votes or
     *  an error results. Otherwise, the returned list may contain
     *  only those votes that are accessible through this session.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and those
     *  currently expired are returned.
     *
     *  @param resourceId the <code>Id</code> of the voter
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>VoteList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesForVoterOnDate(org.osid.id.Id resourceId,
                                                           org.osid.calendaring.DateTime from,
                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.voting.vote.TemporalVoteFilterList(getVotesForVoter(resourceId), from, to));
    }


    /**
     *  Gets a list of votes corresponding to a candidate
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known votes or
     *  an error results. Otherwise, the returned list may contain
     *  only those votes that are accessible through this session.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and those
     *  currently expired are returned.
     *
     *  @param  candidateId the <code>Id</code> of the candidate
     *  @return the returned <code>VoteList</code>
     *  @throws org.osid.NullArgumentException <code>candidateId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.voting.VoteList getVotesForCandidate(org.osid.id.Id candidateId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.voting.vote.VoteFilterList(new CandidateFilter(candidateId), getVotes()));
    }


    /**
     *  Gets a list of votes corresponding to a candidate
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known votes or
     *  an error results. Otherwise, the returned list may contain
     *  only those votes that are accessible through this session.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and those
     *  currently expired are returned.
     *
     *  @param candidateId the <code>Id</code> of the candidate
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>VoteList</code>
     *  @throws org.osid.NullArgumentException <code>candidateId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesForCandidateOnDate(org.osid.id.Id candidateId,
                                                               org.osid.calendaring.DateTime from,
                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.voting.vote.TemporalVoteFilterList(getVotesForCandidate(candidateId), from, to));
    }


    /**
     *  Gets a list of votes corresponding to voter and candidate
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known votes or
     *  an error results. Otherwise, the returned list may contain
     *  only those votes that are accessible through this session.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and those
     *  currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the voter
     *  @param  candidateId the <code>Id</code> of the candidate
     *  @return the returned <code>VoteList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>candidateId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.voting.VoteList getVotesForVoterAndCandidate(org.osid.id.Id resourceId,
                                                                        org.osid.id.Id candidateId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.voting.vote.VoteFilterList(new CandidateFilter(candidateId), getVotesForVoter(resourceId)));
    }


    /**
     *  Gets a list of votes corresponding to voter and candidate
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known votes or
     *  an error results. Otherwise, the returned list may contain
     *  only those votes that are accessible through this session.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and those
     *  currently expired are returned.
     *
     *  @param candidateId the <code>Id</code> of the candidate
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>VoteList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>candidateId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesForVoterAndCandidateOnDate(org.osid.id.Id resourceId,
                                                                       org.osid.id.Id candidateId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.voting.vote.TemporalVoteFilterList(getVotesForVoterAndCandidate(resourceId, candidateId), from, to));
    }


    /**
     *  Gets a list of votes corresponding to a race
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known votes or
     *  an error results. Otherwise, the returned list may contain
     *  only those votes that are accessible through this session.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and those
     *  currently expired are returned.
     *
     *  @param  raceId the <code>Id</code> of the race
     *  @return the returned <code>VoteList</code>
     *  @throws org.osid.NullArgumentException <code>raceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.voting.VoteList getVotesForRace(org.osid.id.Id raceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.voting.Vote> ret = new java.util.ArrayList<>();

        try (org.osid.voting.VoteList votes = getVotes()) {
            while (votes.hasNext()) {
                org.osid.voting.Vote vote = votes.getNextVote();
                org.osid.voting.Candidate candidate = vote.getCandidate();
                if (candidate.getRaceId().equals(raceId)) {
                    ret.add(vote);
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.voting.vote.LinkedVoteList(ret));
    }


    /**
     *  Gets a list of votes corresponding to a race
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known votes or
     *  an error results. Otherwise, the returned list may contain
     *  only those votes that are accessible through this session.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and those
     *  currently expired are returned.
     *
     *  @param raceId the <code>Id</code> of the race
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>VoteList</code>
     *  @throws org.osid.NullArgumentException <code>raceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesForRaceOnDate(org.osid.id.Id raceId,
                                                          org.osid.calendaring.DateTime from,
                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.voting.vote.TemporalVoteFilterList(getVotesForRace(raceId), from, to));
    }


    /**
     *  Gets a list of votes corresponding to voter and race
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known votes or
     *  an error results. Otherwise, the returned list may contain
     *  only those votes that are accessible through this session.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and those
     *  currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the voter
     *  @param  raceId the <code>Id</code> of the race
     *  @return the returned <code>VoteList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>raceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesForVoterAndRace(org.osid.id.Id resourceId,
                                                            org.osid.id.Id raceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.voting.vote.VoteFilterList(new VoterFilter(resourceId), getVotesForRace(raceId)));
    }


    /**
     *  Gets a list of votes corresponding to voter and race
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known votes or
     *  an error results. Otherwise, the returned list may contain
     *  only those votes that are accessible through this session.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and those
     *  currently expired are returned.
     *
     *  @param raceId the <code>Id</code> of the race
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>VoteList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>, <code>raceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesForVoterAndRaceOnDate(org.osid.id.Id resourceId,
                                                                  org.osid.id.Id raceId,
                                                                  org.osid.calendaring.DateTime from,
                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.voting.vote.TemporalVoteFilterList(getVotesForVoterAndRace(resourceId, raceId), from, to));
    }


    /**
     *  Gets a list of votes corresponding to a ballot
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known votes or
     *  an error results. Otherwise, the returned list may contain
     *  only those votes that are accessible through this session.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and those
     *  currently expired are returned.
     *
     *  @param  ballotId the <code>Id</code> of the ballot
     *  @return the returned <code>VoteList</code>
     *  @throws org.osid.NullArgumentException <code>ballotId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.voting.VoteList getVotesForBallot(org.osid.id.Id ballotId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.voting.Vote> ret = new java.util.ArrayList<>();

        try (org.osid.voting.VoteList votes = getVotes()) {
            while (votes.hasNext()) {
                org.osid.voting.Vote vote = votes.getNextVote();
                org.osid.voting.Candidate candidate = vote.getCandidate();
                org.osid.voting.Race race = candidate.getRace();
                if (race.getBallotId().equals(ballotId)) {
                    ret.add(vote);
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.voting.vote.LinkedVoteList(ret));
    }


    /**
     *  Gets a list of votes corresponding to a ballot
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known votes or
     *  an error results. Otherwise, the returned list may contain
     *  only those votes that are accessible through this session.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and those
     *  currently expired are returned.
     *
     *  @param ballotId the <code>Id</code> of the ballot
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>VoteList</code>
     *  @throws org.osid.NullArgumentException <code>ballotId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesForBallotOnDate(org.osid.id.Id ballotId,
                                                            org.osid.calendaring.DateTime from,
                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.voting.vote.TemporalVoteFilterList(getVotesForBallot(ballotId), from, to));
    }


    /**
     *  Gets a list of votes corresponding to voter and ballot
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known votes or
     *  an error results. Otherwise, the returned list may contain
     *  only those votes that are accessible through this session.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and those
     *  currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the voter
     *  @param  ballotId the <code>Id</code> of the ballot
     *  @return the returned <code>VoteList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>ballotId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesForVoterAndBallot(org.osid.id.Id resourceId,
                                                              org.osid.id.Id ballotId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.voting.vote.VoteFilterList(new VoterFilter(resourceId), getVotesForBallot(ballotId)));
    }


    /**
     *  Gets a list of votes corresponding to voter and ballot
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known votes or
     *  an error results. Otherwise, the returned list may contain
     *  only those votes that are accessible through this session.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and those
     *  currently expired are returned.
     *
     *  @param ballotId the <code>Id</code> of the ballot
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>VoteList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>, <code>ballotId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesForVoterAndBallotOnDate(org.osid.id.Id resourceId,
                                                                    org.osid.id.Id ballotId,
                                                                    org.osid.calendaring.DateTime from,
                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.voting.vote.TemporalVoteFilterList(getVotesForVoterAndBallot(resourceId, ballotId), from, to));
    }


    /**
     *  Gets all <code>Votes</code>.
     *
     *  In plenary mode, the returned list contains all known votes or
     *  an error results. Otherwise, the returned list may contain
     *  only those votes that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and those
     *  currently expired are returned.
     *
     *  @return a list of <code>Votes</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.voting.VoteList getVotes()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the vote list for active and effective views. Should
     *  be called by <code>getObjects()</code> if no filtering is
     *  already performed.
     *
     *  @param list the list of votes
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.voting.VoteList filterVotesOnViews(org.osid.voting.VoteList list)
        throws org.osid.OperationFailedException {

        org.osid.voting.VoteList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.voting.vote.EffectiveVoteFilterList(ret);
        }

        return (ret);
    }


    public static class VoterFilter
        implements net.okapia.osid.jamocha.inline.filter.voting.vote.VoteFilter {
         
        private final org.osid.id.Id resourceId;
         
         
        /**
         *  Constructs a new <code>VoterFilter</code>.
         *
         *  @param resourceId the voter to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */
        
        public VoterFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "voter Id");
            this.resourceId = resourceId;
            return;
        }

         
        /**
         *  Used by the VoteFilterList to filter the 
         *  vote list based on voter.
         *
         *  @param vote the vote
         *  @return <code>true</code> to pass the vote,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.voting.Vote vote) {
            return (vote.getVoterId().equals(this.resourceId));
        }
    }


    public static class CandidateFilter
        implements net.okapia.osid.jamocha.inline.filter.voting.vote.VoteFilter {
         
        private final org.osid.id.Id candidateId;
         
         
        /**
         *  Constructs a new <code>CandidateFilter</code>.
         *
         *  @param candidateId the candidate to filter
         *  @throws org.osid.NullArgumentException
         *          <code>candidateId</code> is <code>null</code>
         */
        
        public CandidateFilter(org.osid.id.Id candidateId) {
            nullarg(candidateId, "candidate Id");
            this.candidateId = candidateId;
            return;
        }

         
        /**
         *  Used by the VoteFilterList to filter the 
         *  vote list based on candidate.
         *
         *  @param vote the vote
         *  @return <code>true</code> to pass the vote,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.voting.Vote vote) {
            return (vote.getCandidateId().equals(this.candidateId));
        }
    }
}
