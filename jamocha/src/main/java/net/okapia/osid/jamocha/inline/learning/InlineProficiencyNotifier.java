//
// InlineProficiencyNotifier.java
//
//     A callback interface for performing proficiency
//     notifications.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.learning.spi;


/**
 *  A callback interface for performing proficiency
 *  notifications.
 */

public interface InlineProficiencyNotifier {



    /**
     *  Notifies the creation of a new proficiency.
     *
     *  @param proficiencyId the {@code Id} of the new 
     *         proficiency
     *  @param peer1Id the {@code Id} of the peer1
     *  @param peer2Id the {@code Id} of the peer2
     *  @throws org.osid.NullArgumentException {@code [objectId]},
     *          {@code peer1Id}, or {@code peer2Id} is 
     *          {@code null}
     */

    public void newProficiency(org.osid.id.Id proficiencyId, org.osid.id.Id peer1Id, org.osid.id.Id peer2Id);


    /**
     *  Notifies the change of an updated proficiency.
     *
     *  @param proficiencyId the {@code Id} of the changed
     *         proficiency
     *  @param peer1Id the {@code Id} of the peer1
     *  @param peer2Id the {@code Id} of the peer2
     *  @throws org.osid.NullArgumentException {@code [objectId]},
     *          {@code peer1Id}, or {@code peer2Id} is 
     *          {@code null}
     */

    public void changedProficiency(org.osid.id.Id proficiencyId, org.osid.id.Id peer1Id, org.osid.id.Id peer2Id);


    /**
     *  Notifies a deleted proficiency.
     *
     *  @param proficiencyId the {@code Id} of the deleted
     *         proficiency
     *  @param peer1Id the {@code Id} of the peer1
     *  @param peer2Id the {@code Id} of the peer2
     *  @throws org.osid.NullArgumentException {@code [objectId]},
     *          {@code peer1Id}, or {@code peer2Id} is 
     *          {@code null}
     */

    public void deletedProficiency(org.osid.id.Id proficiencyId, org.osid.id.Id peer1Id, org.osid.id.Id peer2Id);
}
