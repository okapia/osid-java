//
// AbstractImmutableActivityUnit.java
//
//     Wraps a mutable ActivityUnit to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.activityunit.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>ActivityUnit</code> to hide modifiers. This
 *  wrapper provides an immutized ActivityUnit from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying activityUnit whose state changes are visible.
 */

public abstract class AbstractImmutableActivityUnit
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOperableOsidObject
    implements org.osid.course.ActivityUnit {

    private final org.osid.course.ActivityUnit activityUnit;


    /**
     *  Constructs a new <code>AbstractImmutableActivityUnit</code>.
     *
     *  @param activityUnit the activity unit to immutablize
     *  @throws org.osid.NullArgumentException <code>activityUnit</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableActivityUnit(org.osid.course.ActivityUnit activityUnit) {
        super(activityUnit);
        this.activityUnit = activityUnit;
        return;
    }


    /**
     *  Gets the <code>Id</code> of the <code>Course</code> of this
     *  activity unit.
     *
     *  @return the <code>Course</code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCourseId() {
        return (this.activityUnit.getCourseId());
    }


    /**
     *  Gets the <code>Course</code> of this activity unit.
     *
     *  @return the course  
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.Course getCourse()
        throws org.osid.OperationFailedException {

        return (this.activityUnit.getCourse());
    }


    /**
     *  Gets the total time required for this activity. 
     *
     *  @return the total effort 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getTotalTargetEffort() {
        return (this.activityUnit.getTotalTargetEffort());
    }


    /**
     *  Tests if this is a contact activity. 
     *
     *  @return <code> true </code> if this is a contact activity, <code> 
     *          false </code> if an independent activity 
     */

    @OSID @Override
    public boolean isContact() {
        return (this.activityUnit.isContact());
    }


    /**
     *  Gets the total contact time for this activity. 
     *
     *  @return the total effort 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getTotalTargetContactTime() {
        return (this.activityUnit.getTotalTargetContactTime());
    }


    /**
     *  Gets the total indivudal time required for this activity. 
     *
     *  @return the total individual effort 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getTotalTargetIndividualEffort() {
        return (this.activityUnit.getTotalTargetIndividualEffort());
    }


    /**
     *  Tests if this activity is recurring. 
     *
     *  @return <code> true </code> if this activity is recurring, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean isRecurringWeekly() {
        return (this.activityUnit.isRecurringWeekly());
    }


    /**
     *  Gets the time required for this recurring effort on a weekly basis. 
     *
     *  @return the weekly time 
     *  @throws org.osid.IllegalStateException <code> isRecurringWeekly() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getWeeklyEffort() {
        return (this.activityUnit.getWeeklyEffort());
    }


    /**
     *  Gets the weekly contact time for ths activity. 
     *
     *  @return the weekly time 
     *  @throws org.osid.IllegalStateException <code> isRecurringWeekly() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getWeeklyContactTime() {
        return (this.activityUnit.getWeeklyContactTime());
    }


    /**
     *  Gets the weekly individual time for ths activity. 
     *
     *  @return the weekly time 
     *  @throws org.osid.IllegalStateException <code> isRecurringWeekly() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getWeeklyIndividualEffort() {
        return (this.activityUnit.getWeeklyIndividualEffort());
    }


    /**
     *  Tests if this activity unit has associated learning objectives. 
     *
     *  @return <code> true </code> if this activity unit has a learning 
     *          objective, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasLearningObjectives() {
        return (this.activityUnit.hasLearningObjectives());
    }


    /**
     *  Gets the overall learning objective <code> Ids </code> for this 
     *  activity unit. 
     *
     *  @return <code> Ids </code> of the <code> l </code> earning objectives 
     *  @throws org.osid.IllegalStateException <code> hasLearningObjectives() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getLearningObjectiveIds() {
        return (this.activityUnit.getLearningObjectiveIds());
    }


    /**
     *  Gets the overall learning objectives for this activity unit. 
     *
     *  @return the learning objectives 
     *  @throws org.osid.IllegalStateException <code> hasLearningObjectives() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveList getLearningObjectives()
        throws org.osid.OperationFailedException {

        return (this.activityUnit.getLearningObjectives());
    }


    /**
     *  Gets the activity unit record corresponding to the given <code> 
     *  ActivityUnit </code> record <code> Type. </code> This method is used 
     *  to retrieve an object implementing the requested record. The <code> 
     *  activityUnitRecordType </code> may be the <code> Type </code> returned 
     *  in <code> getRecordTypes() </code> or any of its parents in a <code> 
     *  Type </code> hierarchy where <code> 
     *  hasRecordType(activityUnitRecordType) </code> is <code> true </code> . 
     *
     *  @param  activityUnitRecordType the type of activity unit record to 
     *          retrieve 
     *  @return the activity unit record 
     *  @throws org.osid.NullArgumentException <code> activityUnitRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(activityUnitRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.records.ActivityUnitRecord getActivityUnitRecord(org.osid.type.Type activityUnitRecordType)
        throws org.osid.OperationFailedException {

        return (this.activityUnit.getActivityUnitRecord(activityUnitRecordType));
    }
}

