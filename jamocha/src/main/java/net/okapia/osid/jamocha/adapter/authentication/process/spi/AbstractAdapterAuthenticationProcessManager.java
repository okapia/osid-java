//
// AbstractAuthenticationProcessManager.java
//
//     An adapter for a AuthenticationProcessManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.authentication.process.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a AuthenticationProcessManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterAuthenticationProcessManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.authentication.process.AuthenticationProcessManager>
    implements org.osid.authentication.process.AuthenticationProcessManager {


    /**
     *  Constructs a new {@code AbstractAdapterAuthenticationProcessManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterAuthenticationProcessManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterAuthenticationProcessManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterAuthenticationProcessManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if authentication acquisition is supported. Authentication 
     *  acquisition is responsible for acquiring client side authentication 
     *  credentials. 
     *
     *  @return <code> true </code> if authentication acquisiiton is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthenticationAcquisition() {
        return (getAdapteeManager().supportsAuthenticationAcquisition());
    }


    /**
     *  Tests if authentication validation is supported. Authentication 
     *  validation verifies given authentication credentials and maps to an 
     *  agent identity. 
     *
     *  @return <code> true </code> if authentication validation is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthenticationValidation() {
        return (getAdapteeManager().supportsAuthenticationValidation());
    }


    /**
     *  Tests if a trust look up session is supported. 
     *
     *  @return <code> true </code> if trust lookup is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTrustLookup() {
        return (getAdapteeManager().supportsTrustLookup());
    }


    /**
     *  Tests if a session to examine agent and trust relationships is 
     *  supported. 
     *
     *  @return <code> true </code> if a circle of trust is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCircleOfTrust() {
        return (getAdapteeManager().supportsCircleOfTrust());
    }


    /**
     *  Tests if this authentication service supports a challenge-response 
     *  mechanism where credential validation service must implement a means 
     *  to generate challenge data. 
     *
     *  @return <code> true </code> if this is a challenge-response system, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChallenge() {
        return (getAdapteeManager().supportsChallenge());
    }


    /**
     *  Gets the supported authentication record types. 
     *
     *  @return a list containing the supported authentication record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuthenticationRecordTypes() {
        return (getAdapteeManager().getAuthenticationRecordTypes());
    }


    /**
     *  Tests if the given authentication record type is supported. 
     *
     *  @param  authenticationRecordType a <code> Type </code> indicating an 
     *          authentication record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> authenticationRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAuthenticationRecordType(org.osid.type.Type authenticationRecordType) {
        return (getAdapteeManager().supportsAuthenticationRecordType(authenticationRecordType));
    }


    /**
     *  Gets the supported authentication input record types. 
     *
     *  @return a list containing the supported authentication input record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuthenticationInputRecordTypes() {
        return (getAdapteeManager().getAuthenticationInputRecordTypes());
    }


    /**
     *  Tests if the given authentication input record type is supported. 
     *
     *  @param  authenticationInputRecordType a <code> Type </code> indicating 
     *          an authentication input record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          authenticationInputRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAuthenticationInputRecordType(org.osid.type.Type authenticationInputRecordType) {
        return (getAdapteeManager().supportsAuthenticationInputRecordType(authenticationInputRecordType));
    }


    /**
     *  Gets the supported challenge types. 
     *
     *  @return a list containing the supported challenge types 
     */

    @OSID @Override
    public org.osid.type.TypeList getChallengeRecordTypes() {
        return (getAdapteeManager().getChallengeRecordTypes());
    }


    /**
     *  Tests if the given challenge data type is supported. 
     *
     *  @param  challengeRecordType a <code> Type </code> indicating a 
     *          challenge record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> challengeRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsChallengeRecordType(org.osid.type.Type challengeRecordType) {
        return (getAdapteeManager().supportsChallengeRecordType(challengeRecordType));
    }


    /**
     *  Tests if <code> Authentication </code> objects can export serialzied 
     *  credentials for transport. 
     *
     *  @return <code> true </code> if the given credentials export is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialExport() {
        return (getAdapteeManager().supportsCredentialExport());
    }


    /**
     *  Gets the supported credential types. 
     *
     *  @return a list containing the supported credential types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCredentialTypes() {
        return (getAdapteeManager().getCredentialTypes());
    }


    /**
     *  Tests if the given credential type is supported. 
     *
     *  @param  credentialType a <code> Type </code> indicating a credential 
     *          type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> credentialType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCredentialType(org.osid.type.Type credentialType) {
        return (getAdapteeManager().supportsCredentialType(credentialType));
    }


    /**
     *  Gets the supported trust types. 
     *
     *  @return a list containing the supported trust types 
     */

    @OSID @Override
    public org.osid.type.TypeList getTrustTypes() {
        return (getAdapteeManager().getTrustTypes());
    }


    /**
     *  Tests if the given trust type is supported. 
     *
     *  @param  trustType a <code> Type </code> indicating a trust type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> trustType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean supportsTrustType(org.osid.type.Type trustType) {
        return (getAdapteeManager().supportsTrustType(trustType));
    }


    /**
     *  Gets an <code> AuthenticationAcquisitionSession </code> which is 
     *  responsible for acquiring authentication credentials on behalf of a 
     *  service client. 
     *
     *  @return an acquisition session for this service 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthenticationAcquisition() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.process.AuthenticationAcquisitionSession getAuthenticationAcquisitionSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuthenticationAcquisitionSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the <code> 
     *  AuthenticationValidation </code> service. 
     *
     *  @return an <code> AuthenticationValidationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthenticationValidation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.process.AuthenticationValidationSession getAuthenticationValidationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuthenticationValidationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trust lookup 
     *  service. 
     *
     *  @return a <code> TrustLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTrustLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.process.TrustLookupSession getTrustLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTrustLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trust lookup 
     *  service for the given agency. 
     *
     *  @param  agencyId the <code> Id </code> of the agency 
     *  @return a <code> TrustLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> agencyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> agencyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsTrustLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.process.TrustLookupSession getTrustLookupSessionForAgency(org.osid.id.Id agencyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTrustLookupSessionForAgency(agencyId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trust circle 
     *  service. 
     *
     *  @return a <code> CircleOfTrustSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCircleOfTrust() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.process.CircleOfTrustSession getCircleOfTrustSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCircleOfTrustSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trust circle 
     *  service for the given agency. 
     *
     *  @param  agencyId the <code> Id </code> of the agency 
     *  @return a <code> CircleOfTrustSession </code> 
     *  @throws org.osid.NotFoundException <code> agencyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> agencyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsCiirleOfTrust() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.process.CircleOfTrustSession getCircleOfTrustSessionForAgency(org.osid.id.Id agencyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCircleOfTrustSessionForAgency(agencyId));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
