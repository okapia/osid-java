//
// AbstractNodeChecklistHierarchySession.java
//
//     Defines a Checklist hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.checklist.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a checklist hierarchy session for delivering a hierarchy
 *  of checklists using the ChecklistNode interface.
 */

public abstract class AbstractNodeChecklistHierarchySession
    extends net.okapia.osid.jamocha.checklist.spi.AbstractChecklistHierarchySession
    implements org.osid.checklist.ChecklistHierarchySession {

    private java.util.Collection<org.osid.checklist.ChecklistNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root checklist <code> Ids </code> in this hierarchy.
     *
     *  @return the root checklist <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootChecklistIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.checklist.checklistnode.ChecklistNodeToIdList(this.roots));
    }


    /**
     *  Gets the root checklists in the checklist hierarchy. A node
     *  with no parents is an orphan. While all checklist <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root checklists 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistList getRootChecklists()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.checklist.checklistnode.ChecklistNodeToChecklistList(new net.okapia.osid.jamocha.checklist.checklistnode.ArrayChecklistNodeList(this.roots)));
    }


    /**
     *  Adds a root checklist node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootChecklist(org.osid.checklist.ChecklistNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root checklist nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootChecklists(java.util.Collection<org.osid.checklist.ChecklistNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root checklist node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootChecklist(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.checklist.ChecklistNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Checklist </code> has any parents. 
     *
     *  @param  checklistId a checklist <code> Id </code> 
     *  @return <code> true </code> if the checklist has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> checklistId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> checklistId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentChecklists(org.osid.id.Id checklistId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getChecklistNode(checklistId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  checklist.
     *
     *  @param  id an <code> Id </code> 
     *  @param  checklistId the <code> Id </code> of a checklist 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> checklistId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> checklistId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> checklistId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfChecklist(org.osid.id.Id id, org.osid.id.Id checklistId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.checklist.ChecklistNodeList parents = getChecklistNode(checklistId).getParentChecklistNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextChecklistNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given checklist. 
     *
     *  @param  checklistId a checklist <code> Id </code> 
     *  @return the parent <code> Ids </code> of the checklist 
     *  @throws org.osid.NotFoundException <code> checklistId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> checklistId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentChecklistIds(org.osid.id.Id checklistId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.checklist.checklist.ChecklistToIdList(getParentChecklists(checklistId)));
    }


    /**
     *  Gets the parents of the given checklist. 
     *
     *  @param  checklistId the <code> Id </code> to query 
     *  @return the parents of the checklist 
     *  @throws org.osid.NotFoundException <code> checklistId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> checklistId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistList getParentChecklists(org.osid.id.Id checklistId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.checklist.checklistnode.ChecklistNodeToChecklistList(getChecklistNode(checklistId).getParentChecklistNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  checklist.
     *
     *  @param  id an <code> Id </code> 
     *  @param  checklistId the Id of a checklist 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> checklistId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> checklistId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> checklistId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfChecklist(org.osid.id.Id id, org.osid.id.Id checklistId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfChecklist(id, checklistId)) {
            return (true);
        }

        try (org.osid.checklist.ChecklistList parents = getParentChecklists(checklistId)) {
            while (parents.hasNext()) {
                if (isAncestorOfChecklist(id, parents.getNextChecklist().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a checklist has any children. 
     *
     *  @param  checklistId a checklist <code> Id </code> 
     *  @return <code> true </code> if the <code> checklistId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> checklistId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> checklistId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildChecklists(org.osid.id.Id checklistId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getChecklistNode(checklistId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  checklist.
     *
     *  @param  id an <code> Id </code> 
     *  @param checklistId the <code> Id </code> of a 
     *         checklist
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> checklistId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> checklistId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> checklistId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfChecklist(org.osid.id.Id id, org.osid.id.Id checklistId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfChecklist(checklistId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  checklist.
     *
     *  @param  checklistId the <code> Id </code> to query 
     *  @return the children of the checklist 
     *  @throws org.osid.NotFoundException <code> checklistId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> checklistId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildChecklistIds(org.osid.id.Id checklistId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.checklist.checklist.ChecklistToIdList(getChildChecklists(checklistId)));
    }


    /**
     *  Gets the children of the given checklist. 
     *
     *  @param  checklistId the <code> Id </code> to query 
     *  @return the children of the checklist 
     *  @throws org.osid.NotFoundException <code> checklistId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> checklistId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistList getChildChecklists(org.osid.id.Id checklistId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.checklist.checklistnode.ChecklistNodeToChecklistList(getChecklistNode(checklistId).getChildChecklistNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  checklist.
     *
     *  @param  id an <code> Id </code> 
     *  @param checklistId the <code> Id </code> of a 
     *         checklist
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> checklistId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> checklistId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> checklistId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfChecklist(org.osid.id.Id id, org.osid.id.Id checklistId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfChecklist(checklistId, id)) {
            return (true);
        }

        try (org.osid.checklist.ChecklistList children = getChildChecklists(checklistId)) {
            while (children.hasNext()) {
                if (isDescendantOfChecklist(id, children.getNextChecklist().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  checklist.
     *
     *  @param  checklistId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified checklist node 
     *  @throws org.osid.NotFoundException <code> checklistId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> checklistId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getChecklistNodeIds(org.osid.id.Id checklistId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.checklist.checklistnode.ChecklistNodeToNode(getChecklistNode(checklistId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given checklist.
     *
     *  @param  checklistId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified checklist node 
     *  @throws org.osid.NotFoundException <code> checklistId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> checklistId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistNode getChecklistNodes(org.osid.id.Id checklistId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getChecklistNode(checklistId));
    }


    /**
     *  Closes this <code>ChecklistHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a checklist node.
     *
     *  @param checklistId the id of the checklist node
     *  @throws org.osid.NotFoundException <code>checklistId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>checklistId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.checklist.ChecklistNode getChecklistNode(org.osid.id.Id checklistId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(checklistId, "checklist Id");
        for (org.osid.checklist.ChecklistNode checklist : this.roots) {
            if (checklist.getId().equals(checklistId)) {
                return (checklist);
            }

            org.osid.checklist.ChecklistNode r = findChecklist(checklist, checklistId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(checklistId + " is not found");
    }


    protected org.osid.checklist.ChecklistNode findChecklist(org.osid.checklist.ChecklistNode node, 
                                                            org.osid.id.Id checklistId) 
	throws org.osid.OperationFailedException {

        try (org.osid.checklist.ChecklistNodeList children = node.getChildChecklistNodes()) {
            while (children.hasNext()) {
                org.osid.checklist.ChecklistNode checklist = children.getNextChecklistNode();
                if (checklist.getId().equals(checklistId)) {
                    return (checklist);
                }
                
                checklist = findChecklist(checklist, checklistId);
                if (checklist != null) {
                    return (checklist);
                }
            }
        }

        return (null);
    }
}
