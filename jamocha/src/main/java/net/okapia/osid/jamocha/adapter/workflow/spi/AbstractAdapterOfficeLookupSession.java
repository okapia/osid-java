//
// AbstractAdapterOfficeLookupSession.java
//
//    An Office lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.workflow.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Office lookup session adapter.
 */

public abstract class AbstractAdapterOfficeLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.workflow.OfficeLookupSession {

    private final org.osid.workflow.OfficeLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterOfficeLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterOfficeLookupSession(org.osid.workflow.OfficeLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Office} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupOffices() {
        return (this.session.canLookupOffices());
    }


    /**
     *  A complete view of the {@code Office} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeOfficeView() {
        this.session.useComparativeOfficeView();
        return;
    }


    /**
     *  A complete view of the {@code Office} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryOfficeView() {
        this.session.usePlenaryOfficeView();
        return;
    }

     
    /**
     *  Gets the {@code Office} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Office} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Office} and
     *  retained for compatibility.
     *
     *  @param officeId {@code Id} of the {@code Office}
     *  @return the office
     *  @throws org.osid.NotFoundException {@code officeId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code officeId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.Office getOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOffice(officeId));
    }


    /**
     *  Gets an {@code OfficeList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  offices specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Offices} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  officeIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Office} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code officeIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.OfficeList getOfficesByIds(org.osid.id.IdList officeIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOfficesByIds(officeIds));
    }


    /**
     *  Gets an {@code OfficeList} corresponding to the given
     *  office genus {@code Type} which does not include
     *  offices of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  offices or an error results. Otherwise, the returned list
     *  may contain only those offices that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  officeGenusType an office genus type 
     *  @return the returned {@code Office} list
     *  @throws org.osid.NullArgumentException
     *          {@code officeGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.OfficeList getOfficesByGenusType(org.osid.type.Type officeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOfficesByGenusType(officeGenusType));
    }


    /**
     *  Gets an {@code OfficeList} corresponding to the given
     *  office genus {@code Type} and include any additional
     *  offices with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  offices or an error results. Otherwise, the returned list
     *  may contain only those offices that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  officeGenusType an office genus type 
     *  @return the returned {@code Office} list
     *  @throws org.osid.NullArgumentException
     *          {@code officeGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.OfficeList getOfficesByParentGenusType(org.osid.type.Type officeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOfficesByParentGenusType(officeGenusType));
    }


    /**
     *  Gets an {@code OfficeList} containing the given
     *  office record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  offices or an error results. Otherwise, the returned list
     *  may contain only those offices that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  officeRecordType an office record type 
     *  @return the returned {@code Office} list
     *  @throws org.osid.NullArgumentException
     *          {@code officeRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.OfficeList getOfficesByRecordType(org.osid.type.Type officeRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOfficesByRecordType(officeRecordType));
    }


    /**
     *  Gets an {@code OfficeList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  offices or an error results. Otherwise, the returned list
     *  may contain only those offices that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Office} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.OfficeList getOfficesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOfficesByProvider(resourceId));
    }


    /**
     *  Gets all {@code Offices}. 
     *
     *  In plenary mode, the returned list contains all known
     *  offices or an error results. Otherwise, the returned list
     *  may contain only those offices that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Offices} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.OfficeList getOffices()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOffices());
    }
}
