//
// BankElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.bank.spi;

/**
 *  Ids for object elements for use in forms and queries.
 */

public class BankElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the BankElement Id.
     *
     *  @return the bank element Id
     */

    public static org.osid.id.Id getBankEntityId() {
        return (makeEntityId("osid.assessment.Bank"));
    }


    /**
     *  Gets the ItemId element Id.
     *
     *  @return the ItemId element Id
     */

    public static org.osid.id.Id getItemId() {
        return (makeQueryElementId("osid.assessment.bank.ItemId"));
    }


    /**
     *  Gets the Item element Id.
     *
     *  @return the Item element Id
     */

    public static org.osid.id.Id getItem() {
        return (makeQueryElementId("osid.assessment.bank.Item"));
    }


    /**
     *  Gets the AssessmentId element Id.
     *
     *  @return the AssessmentId element Id
     */

    public static org.osid.id.Id getAssessmentId() {
        return (makeQueryElementId("osid.assessment.bank.AssessmentId"));
    }


    /**
     *  Gets the Assessment element Id.
     *
     *  @return the Assessment element Id
     */

    public static org.osid.id.Id getAssessment() {
        return (makeQueryElementId("osid.assessment.bank.Assessment"));
    }


    /**
     *  Gets the AssessmentOfferedId element Id.
     *
     *  @return the AssessmentOfferedId element Id
     */

    public static org.osid.id.Id getAssessmentOfferedId() {
        return (makeQueryElementId("osid.assessment.bank.AssessmentOfferedId"));
    }


    /**
     *  Gets the AssessmentOffered element Id.
     *
     *  @return the AssessmentOffered element Id
     */

    public static org.osid.id.Id getAssessmentOffered() {
        return (makeQueryElementId("osid.assessment.bank.AssessmentOffered"));
    }


    /**
     *  Gets the AncestorBankId element Id.
     *
     *  @return the AncestorBankId element Id
     */

    public static org.osid.id.Id getAncestorBankId() {
        return (makeQueryElementId("osid.assessment.bank.AncestorBankId"));
    }


    /**
     *  Gets the AncestorBank element Id.
     *
     *  @return the AncestorBank element Id
     */

    public static org.osid.id.Id getAncestorBank() {
        return (makeQueryElementId("osid.assessment.bank.AncestorBank"));
    }


    /**
     *  Gets the DescendantBankId element Id.
     *
     *  @return the DescendantBankId element Id
     */

    public static org.osid.id.Id getDescendantBankId() {
        return (makeQueryElementId("osid.assessment.bank.DescendantBankId"));
    }


    /**
     *  Gets the DescendantBank element Id.
     *
     *  @return the DescendantBank element Id
     */

    public static org.osid.id.Id getDescendantBank() {
        return (makeQueryElementId("osid.assessment.bank.DescendantBank"));
    }
}
