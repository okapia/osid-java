//
// StoreElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.store.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class StoreElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the StoreElement Id.
     *
     *  @return the store element Id
     */

    public static org.osid.id.Id getStoreEntityId() {
        return (makeEntityId("osid.ordering.Store"));
    }


    /**
     *  Gets the OrderId element Id.
     *
     *  @return the OrderId element Id
     */

    public static org.osid.id.Id getOrderId() {
        return (makeQueryElementId("osid.ordering.store.OrderId"));
    }


    /**
     *  Gets the Order element Id.
     *
     *  @return the Order element Id
     */

    public static org.osid.id.Id getOrder() {
        return (makeQueryElementId("osid.ordering.store.Order"));
    }


    /**
     *  Gets the ProductId element Id.
     *
     *  @return the ProductId element Id
     */

    public static org.osid.id.Id getProductId() {
        return (makeQueryElementId("osid.ordering.store.ProductId"));
    }


    /**
     *  Gets the Product element Id.
     *
     *  @return the Product element Id
     */

    public static org.osid.id.Id getProduct() {
        return (makeQueryElementId("osid.ordering.store.Product"));
    }


    /**
     *  Gets the PriceScheduleId element Id.
     *
     *  @return the PriceScheduleId element Id
     */

    public static org.osid.id.Id getPriceScheduleId() {
        return (makeQueryElementId("osid.ordering.store.PriceScheduleId"));
    }


    /**
     *  Gets the PriceSchedule element Id.
     *
     *  @return the PriceSchedule element Id
     */

    public static org.osid.id.Id getPriceSchedule() {
        return (makeQueryElementId("osid.ordering.store.PriceSchedule"));
    }


    /**
     *  Gets the AncestorStoreId element Id.
     *
     *  @return the AncestorStoreId element Id
     */

    public static org.osid.id.Id getAncestorStoreId() {
        return (makeQueryElementId("osid.ordering.store.AncestorStoreId"));
    }


    /**
     *  Gets the AncestorStore element Id.
     *
     *  @return the AncestorStore element Id
     */

    public static org.osid.id.Id getAncestorStore() {
        return (makeQueryElementId("osid.ordering.store.AncestorStore"));
    }


    /**
     *  Gets the DescendantStoreId element Id.
     *
     *  @return the DescendantStoreId element Id
     */

    public static org.osid.id.Id getDescendantStoreId() {
        return (makeQueryElementId("osid.ordering.store.DescendantStoreId"));
    }


    /**
     *  Gets the DescendantStore element Id.
     *
     *  @return the DescendantStore element Id
     */

    public static org.osid.id.Id getDescendantStore() {
        return (makeQueryElementId("osid.ordering.store.DescendantStore"));
    }
}
