//
// AbstractIndexedMapPaymentLookupSession.java
//
//    A simple framework for providing a Payment lookup service
//    backed by a fixed collection of payments with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.billing.payment.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Payment lookup service backed by a
 *  fixed collection of payments. The payments are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some payments may be compatible
 *  with more types than are indicated through these payment
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Payments</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapPaymentLookupSession
    extends AbstractMapPaymentLookupSession
    implements org.osid.billing.payment.PaymentLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.billing.payment.Payment> paymentsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.billing.payment.Payment>());
    private final MultiMap<org.osid.type.Type, org.osid.billing.payment.Payment> paymentsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.billing.payment.Payment>());


    /**
     *  Makes a <code>Payment</code> available in this session.
     *
     *  @param  payment a payment
     *  @throws org.osid.NullArgumentException <code>payment<code> is
     *          <code>null</code>
     */

    @Override
    protected void putPayment(org.osid.billing.payment.Payment payment) {
        super.putPayment(payment);

        this.paymentsByGenus.put(payment.getGenusType(), payment);
        
        try (org.osid.type.TypeList types = payment.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.paymentsByRecord.put(types.getNextType(), payment);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a payment from this session.
     *
     *  @param paymentId the <code>Id</code> of the payment
     *  @throws org.osid.NullArgumentException <code>paymentId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removePayment(org.osid.id.Id paymentId) {
        org.osid.billing.payment.Payment payment;
        try {
            payment = getPayment(paymentId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.paymentsByGenus.remove(payment.getGenusType());

        try (org.osid.type.TypeList types = payment.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.paymentsByRecord.remove(types.getNextType(), payment);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removePayment(paymentId);
        return;
    }


    /**
     *  Gets a <code>PaymentList</code> corresponding to the given
     *  payment genus <code>Type</code> which does not include
     *  payments of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known payments or an error results. Otherwise,
     *  the returned list may contain only those payments that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  paymentGenusType a payment genus type 
     *  @return the returned <code>Payment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>paymentGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsByGenusType(org.osid.type.Type paymentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.billing.payment.payment.ArrayPaymentList(this.paymentsByGenus.get(paymentGenusType)));
    }


    /**
     *  Gets a <code>PaymentList</code> containing the given
     *  payment record <code>Type</code>. In plenary mode, the
     *  returned list contains all known payments or an error
     *  results. Otherwise, the returned list may contain only those
     *  payments that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  paymentRecordType a payment record type 
     *  @return the returned <code>payment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>paymentRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsByRecordType(org.osid.type.Type paymentRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.billing.payment.payment.ArrayPaymentList(this.paymentsByRecord.get(paymentRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.paymentsByGenus.clear();
        this.paymentsByRecord.clear();

        super.close();

        return;
    }
}
