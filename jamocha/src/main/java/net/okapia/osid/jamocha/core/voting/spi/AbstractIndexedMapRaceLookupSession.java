//
// AbstractIndexedMapRaceLookupSession.java
//
//    A simple framework for providing a Race lookup service
//    backed by a fixed collection of races with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Race lookup service backed by a
 *  fixed collection of races. The races are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some races may be compatible
 *  with more types than are indicated through these race
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Races</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapRaceLookupSession
    extends AbstractMapRaceLookupSession
    implements org.osid.voting.RaceLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.voting.Race> racesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.voting.Race>());
    private final MultiMap<org.osid.type.Type, org.osid.voting.Race> racesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.voting.Race>());


    /**
     *  Makes a <code>Race</code> available in this session.
     *
     *  @param  race a race
     *  @throws org.osid.NullArgumentException <code>race<code> is
     *          <code>null</code>
     */

    @Override
    protected void putRace(org.osid.voting.Race race) {
        super.putRace(race);

        this.racesByGenus.put(race.getGenusType(), race);
        
        try (org.osid.type.TypeList types = race.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.racesByRecord.put(types.getNextType(), race);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a race from this session.
     *
     *  @param raceId the <code>Id</code> of the race
     *  @throws org.osid.NullArgumentException <code>raceId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeRace(org.osid.id.Id raceId) {
        org.osid.voting.Race race;
        try {
            race = getRace(raceId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.racesByGenus.remove(race.getGenusType());

        try (org.osid.type.TypeList types = race.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.racesByRecord.remove(types.getNextType(), race);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeRace(raceId);
        return;
    }


    /**
     *  Gets a <code>RaceList</code> corresponding to the given
     *  race genus <code>Type</code> which does not include
     *  races of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known races or an error results. Otherwise,
     *  the returned list may contain only those races that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  raceGenusType a race genus type 
     *  @return the returned <code>Race</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>raceGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.RaceList getRacesByGenusType(org.osid.type.Type raceGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.voting.race.ArrayRaceList(this.racesByGenus.get(raceGenusType)));
    }


    /**
     *  Gets a <code>RaceList</code> containing the given
     *  race record <code>Type</code>. In plenary mode, the
     *  returned list contains all known races or an error
     *  results. Otherwise, the returned list may contain only those
     *  races that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  raceRecordType a race record type 
     *  @return the returned <code>race</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>raceRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.RaceList getRacesByRecordType(org.osid.type.Type raceRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.voting.race.ArrayRaceList(this.racesByRecord.get(raceRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.racesByGenus.clear();
        this.racesByRecord.clear();

        super.close();

        return;
    }
}
