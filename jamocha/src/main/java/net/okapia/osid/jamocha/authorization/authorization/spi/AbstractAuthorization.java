//
// AbstractAuthorization.java
//
//     Defines an Authorization.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authorization.authorization.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Authorization</code>.
 */

public abstract class AbstractAuthorization
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.authorization.Authorization {

    private boolean implicit = false;
    private org.osid.resource.Resource resource;
    private org.osid.authentication.process.Trust trust;
    private org.osid.authentication.Agent agent;
    private org.osid.authorization.Function function;
    private org.osid.authorization.Qualifier qualifier;

    private final java.util.Collection<org.osid.authorization.records.AuthorizationRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Tests if this authorization is implicit. 
     *
     *  @return <code> true </code> if this authorization is implicit,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean isImplicit() {
        return (this.implicit);
    }


    /**
     *  Sets the implicit flag.
     *
     *  @param implicit <code> true </code> if this authorization is
     *         implicit, <code> false </code> otherwise
     */

    protected void setImplicit(boolean implicit) {
        this.implicit = implicit;
        return;
    }


    /**
     *  Tests if this authorization has a <code> Resource. </code> 
     *
     *  @return <code> true </code> if this authorization has a <code>
     *          Resource, </code> <code> false </code> otherwise
     */

    @OSID @Override
    public boolean hasResource() {
        return (this.resource != null);
    }


    /**
     *  Gets the <code> resource Id </code> for this authorization. 
     *
     *  @return the <code> Resource Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasResource()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.id.Id getResourceId() {
        if (!hasResource()) {
            throw new org.osid.IllegalStateException("hasResource() is null");
        }

        return (this.resource.getId());
    }


    /**
     *  Gets the <code> Resource </code> for this authorization.
     *
     *  @return the <code> Resource </code> 
     *  @throws org.osid.IllegalStateException <code> hasResource()
     *          </code> is <code> false </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource()
        throws org.osid.OperationFailedException {

        if (!hasResource()) {
            throw new org.osid.IllegalStateException("hasResource() is null");
        }

        return (this.resource);
    }


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    protected void setResource(org.osid.resource.Resource resource) {
        nullarg(resource, "resource");
        this.resource = resource;
        return;
    }


    /**
     *  Tests if this authorization has a <code> Trust. </code> 
     *
     *  @return <code> true </code> if this authorization has a <code>
     *          Trust, </code> <code> false </code> otherwise
     */

    @OSID @Override
    public boolean hasTrust() {
        return (this.trust != null);
    }


    /**
     *  Gets the <code> Trust </code> <code> Id </code> for this 
     *  authorization. 
     *
     *  @return the trust <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasTrust() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getTrustId() {
        if (!hasTrust()) {
            throw new org.osid.IllegalStateException("hasTrust() is false");
        }

        return (this.trust.getId());
    }


    /**
     *  Gets the <code> Trust </code> for this authorization. 
     *
     *  @return the <code> Trust </code> 
     *  @throws org.osid.IllegalStateException <code> hasTrust()
     *          </code> is <code> false </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.process.Trust getTrust()
        throws org.osid.OperationFailedException {

        if (!hasTrust()) {
            throw new org.osid.IllegalStateException("hasTrust() is false");
        }

        return (this.trust);
    }


    /**
     *  Sets the trust.
     *
     *  @param trust a trust
     *  @throws org.osid.NullArgumentException <code>trust</code> is
     *          <code>null</code>
     */

    protected void setTrust(org.osid.authentication.process.Trust trust) {
        nullarg(trust, "trust");
        this.trust = trust;
        return;
    }


    /**
     *  Tests if this authorization has an <code> Agent. </code> An implied 
     *  authorization may have an <code> Agent </code> in addition to a 
     *  specified <code> Resource. </code> 
     *
     *  @return <code> true </code> if this authorization has an
     *          <code> Agent, </code> <code> false </code> otherwise
     */

    @OSID @Override
    public boolean hasAgent() {
        return (this.agent != null);
    }


    /**
     *  Gets the <code> Agent Id </code> for this authorization. 
     *
     *  @return the <code> Agent Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasAgent() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAgentId() {
        if (!hasAgent()) {
            throw new org.osid.IllegalStateException("hasAgent() is false");
        }

        return (this.agent.getId());
    }


    /**
     *  Gets the <code> Agent </code> for this authorization.
     *
     *  @return the <code> Agent </code> 
     *  @throws org.osid.IllegalStateException <code> hasAgent()
     *          </code> is <code> false </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getAgent()
        throws org.osid.OperationFailedException {

        if (!hasAgent()) {
            throw new org.osid.IllegalStateException("hasAgent() is false");
        }

        return (this.agent);
    }


    /**
     *  Sets the agent.
     *
     *  @param agent an agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    protected void setAgent(org.osid.authentication.Agent agent) {
        nullarg(agent, "agent");
        this.agent = agent;
        return;
    }


    /**
     *  Gets the <code> Function Id </code> for this authorization. 
     *
     *  @return the function <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getFunctionId() {
        return (this.function.getId());
    }


    /**
     *  Gets the <code> Function </code> for this authorization. 
     *
     *  @return the function 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authorization.Function getFunction()
        throws org.osid.OperationFailedException {

        return (this.function);
    }


    /**
     *  Sets the function.
     *
     *  @param function a function
     *  @throws org.osid.NullArgumentException <code>function</code>
     *          is <code>null</code>
     */

    protected void setFunction(org.osid.authorization.Function function) {
        nullarg(function, "function");
        this.function = function;
        return;
    }


    /**
     *  Gets the <code> Qualifier Id </code> for this authorization. 
     *
     *  @return the qualifier <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getQualifierId() {
        return (this.qualifier.getId());
    }


    /**
     *  Gets the qualifier for this authorization. 
     *
     *  @return the qualifier 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authorization.Qualifier getQualifier()
        throws org.osid.OperationFailedException {

        return (this.qualifier);
    }


    /**
     *  Sets the qualifier.
     *
     *  @param qualifier a qualifier
     *  @throws org.osid.NullArgumentException <code>qualifier</code>
     *          is <code>null</code>
     */

    protected void setQualifier(org.osid.authorization.Qualifier qualifier) {
        nullarg(qualifier, "qualifier");
        this.qualifier = qualifier;
        return;
    }


    /**
     *  Tests if this authorization supports the given record
     *  <code>Type</code>.
     *
     *  @param  authorizationRecordType an authorization record type 
     *  @return <code>true</code> if the authorizationRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type authorizationRecordType) {
        for (org.osid.authorization.records.AuthorizationRecord record : this.records) {
            if (record.implementsRecordType(authorizationRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Authorization</code> record <code>Type</code>.
     *
     *  @param  authorizationRecordType the authorization record type 
     *  @return the authorization record 
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(authorizationRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.authorization.records.AuthorizationRecord getAuthorizationRecord(org.osid.type.Type authorizationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authorization.records.AuthorizationRecord record : this.records) {
            if (record.implementsRecordType(authorizationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(authorizationRecordType + " is not supported");
    }


    /**
     *  Adds a record to this authorization. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param authorizationRecord the authorization record
     *  @param authorizationRecordType authorization record type
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationRecord</code> or
     *          <code>authorizationRecordTyp</code> is
     *          <code>null</code>
     */
            
    protected void addAuthorizationRecord(org.osid.authorization.records.AuthorizationRecord authorizationRecord, 
                                          org.osid.type.Type authorizationRecordType) {

        nullarg(authorizationRecord, "authorization record");
        addRecordType(authorizationRecordType);
        this.records.add(authorizationRecord);
        
        return;
    }
}
