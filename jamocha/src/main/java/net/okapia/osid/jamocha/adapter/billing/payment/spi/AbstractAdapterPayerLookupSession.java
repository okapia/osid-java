//
// AbstractAdapterPayerLookupSession.java
//
//    A Payer lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.billing.payment.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Payer lookup session adapter.
 */

public abstract class AbstractAdapterPayerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.billing.payment.PayerLookupSession {

    private final org.osid.billing.payment.PayerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterPayerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterPayerLookupSession(org.osid.billing.payment.PayerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Business/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Business Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.session.getBusinessId());
    }


    /**
     *  Gets the {@code Business} associated with this session.
     *
     *  @return the {@code Business} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.Business getBusiness()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getBusiness());
    }


    /**
     *  Tests if this user can perform {@code Payer} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupPayers() {
        return (this.session.canLookupPayers());
    }


    /**
     *  A complete view of the {@code Payer} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePayerView() {
        this.session.useComparativePayerView();
        return;
    }


    /**
     *  A complete view of the {@code Payer} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPayerView() {
        this.session.usePlenaryPayerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include payers in businesses which are children
     *  of this business in the business hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        this.session.useFederatedBusinessView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        this.session.useIsolatedBusinessView();
        return;
    }
    

    /**
     *  Only payers whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectivePayerView() {
        this.session.useEffectivePayerView();
        return;
    }
    

    /**
     *  All payers of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectivePayerView() {
        this.session.useAnyEffectivePayerView();
        return;
    }

     
    /**
     *  Gets the {@code Payer} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Payer} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Payer} and
     *  retained for compatibility.
     *
     *  In effective mode, payers are returned that are currently
     *  effective.  In any effective mode, effective payers and
     *  those currently expired are returned.
     *
     *  @param payerId {@code Id} of the {@code Payer}
     *  @return the payer
     *  @throws org.osid.NotFoundException {@code payerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code payerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.Payer getPayer(org.osid.id.Id payerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPayer(payerId));
    }


    /**
     *  Gets a {@code PayerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  payers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Payers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, payers are returned that are currently
     *  effective.  In any effective mode, effective payers and those
     *  currently expired are returned.
     *
     *  @param  payerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Payer} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code payerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerList getPayersByIds(org.osid.id.IdList payerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPayersByIds(payerIds));
    }


    /**
     *  Gets a {@code PayerList} corresponding to the given
     *  payer genus {@code Type} which does not include
     *  payers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  payers or an error results. Otherwise, the returned list
     *  may contain only those payers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, payers are returned that are currently
     *  effective.  In any effective mode, effective payers and
     *  those currently expired are returned.
     *
     *  @param  payerGenusType a payer genus type 
     *  @return the returned {@code Payer} list
     *  @throws org.osid.NullArgumentException
     *          {@code payerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerList getPayersByGenusType(org.osid.type.Type payerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPayersByGenusType(payerGenusType));
    }


    /**
     *  Gets a {@code PayerList} corresponding to the given
     *  payer genus {@code Type} and include any additional
     *  payers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  payers or an error results. Otherwise, the returned list
     *  may contain only those payers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, payers are returned that are currently
     *  effective.  In any effective mode, effective payers and
     *  those currently expired are returned.
     *
     *  @param  payerGenusType a payer genus type 
     *  @return the returned {@code Payer} list
     *  @throws org.osid.NullArgumentException
     *          {@code payerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerList getPayersByParentGenusType(org.osid.type.Type payerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPayersByParentGenusType(payerGenusType));
    }


    /**
     *  Gets a {@code PayerList} containing the given
     *  payer record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  payers or an error results. Otherwise, the returned list
     *  may contain only those payers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, payers are returned that are currently
     *  effective.  In any effective mode, effective payers and
     *  those currently expired are returned.
     *
     *  @param  payerRecordType a payer record type 
     *  @return the returned {@code Payer} list
     *  @throws org.osid.NullArgumentException
     *          {@code payerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerList getPayersByRecordType(org.osid.type.Type payerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPayersByRecordType(payerRecordType));
    }


    /**
     *  Gets a {@code PayerList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  payers or an error results. Otherwise, the returned list
     *  may contain only those payers that are accessible
     *  through this session.
     *  
     *  In active mode, payers are returned that are currently
     *  active. In any status mode, active and inactive payers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Payer} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.billing.payment.PayerList getPayersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPayersOnDate(from, to));
    }
        

    /**
     *  Gets a {@code PayerList} related to the given resource.
     *  
     *  In plenary mode, the returned list contains all known payers
     *  or an error results. Otherwise, the returned list may contain
     *  only those payers that are accessible through this session.
     *  
     *  In effective mode, payers are returned that are currently
     *  effective.  In any effective mode, effective payers and those
     *  currently expired are returned.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code PayerList} list 
     *  @throws org.osid.NullArgumentException {@code resourceId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerList getPayersByResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPayersByResource(resourceId));
    }


    /**
     *  Gets a {@code PayerList} of the given resource and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known payers
     *  or an error results. Otherwise, the returned list may contain
     *  only those payers that are accessible through this session.
     *  
     *  In effective mode, payers are returned that are currently
     *  effective in addition to being effective during the given date
     *  range. In any effective mode, effective payers and those
     *  currently expired are returned.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned {@code PayerList} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code resourceId, from, 
     *         } or {@code to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
    
    @OSID @Override
    public org.osid.billing.payment.PayerList getPayersByResourceOnDate(org.osid.id.Id resourceId, 
                                                                        org.osid.calendaring.DateTime from, 
                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPayersByResourceOnDate(resourceId, from, to));
    }


    /**
     *  Gets a {@code PayerList} related to the given payer
     *  customer.
     *  
     *  In plenary mode, the returned list contains all known payers
     *  or an error results. Otherwise, the returned list may contain
     *  only those payers that are accessible through this session.
     *  
     *  In effective mode, payers are returned that are currently
     *  effective.  In any effective mode, effective payers and those
     *  currently expired are returned.
     *
     *  @param  customerId a customer {@code Id} 
     *  @return the returned {@code PayerList} list 
     *  @throws org.osid.NullArgumentException {@code customerId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerList getPayersByCustomer(org.osid.id.Id customerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPayersByCustomer(customerId));
    }


    /**
     *  Gets a {@code PayerList} of the given customer and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known payers
     *  or an error results. Otherwise, the returned list may contain
     *  only those payers that are accessible through this session.
     *  
     *  In effective mode, payers are returned that are currently
     *  effective in addition to being effective during the given date
     *  range. In any effective mode, effective payers and those
     *  currently expired are returned.
     *
     *  @param  customerId a customer {@code Id} 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned {@code PayerList} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code customerId,
     *         from}, or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerList getPayersByCustomerOnDate(org.osid.id.Id customerId, 
                                                                        org.osid.calendaring.DateTime from, 
                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPayersByCustomerOnDate(customerId, from, to));
    }


    /**
     *  Gets all {@code Payers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  payers or an error results. Otherwise, the returned list
     *  may contain only those payers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, payers are returned that are currently
     *  effective.  In any effective mode, effective payers and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Payers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerList getPayers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPayers());
    }
}
