//
// AbstractAdapterScheduleLookupSession.java
//
//    A Schedule lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.calendaring.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Schedule lookup session adapter.
 */

public abstract class AbstractAdapterScheduleLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.calendaring.ScheduleLookupSession {

    private final org.osid.calendaring.ScheduleLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterScheduleLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterScheduleLookupSession(org.osid.calendaring.ScheduleLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Calendar/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Calendar Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.session.getCalendarId());
    }


    /**
     *  Gets the {@code Calendar} associated with this session.
     *
     *  @return the {@code Calendar} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCalendar());
    }


    /**
     *  Tests if this user can perform {@code Schedule} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupSchedules() {
        return (this.session.canLookupSchedules());
    }


    /**
     *  A complete view of the {@code Schedule} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeScheduleView() {
        this.session.useComparativeScheduleView();
        return;
    }


    /**
     *  A complete view of the {@code Schedule} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryScheduleView() {
        this.session.usePlenaryScheduleView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include schedules in calendars which are children
     *  of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        this.session.useFederatedCalendarView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        this.session.useIsolatedCalendarView();
        return;
    }
    
     
    /**
     *  Gets the {@code Schedule} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Schedule} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Schedule} and
     *  retained for compatibility.
     *
     *  @param scheduleId {@code Id} of the {@code Schedule}
     *  @return the schedule
     *  @throws org.osid.NotFoundException {@code scheduleId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code scheduleId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Schedule getSchedule(org.osid.id.Id scheduleId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSchedule(scheduleId));
    }


    /**
     *  Gets a {@code ScheduleList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  schedules specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Schedules} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  scheduleIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Schedule} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code scheduleIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedulesByIds(org.osid.id.IdList scheduleIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSchedulesByIds(scheduleIds));
    }


    /**
     *  Gets a {@code ScheduleList} corresponding to the given
     *  schedule genus {@code Type} which does not include
     *  schedules of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  schedules or an error results. Otherwise, the returned list
     *  may contain only those schedules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  scheduleGenusType a schedule genus type 
     *  @return the returned {@code Schedule} list
     *  @throws org.osid.NullArgumentException
     *          {@code scheduleGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedulesByGenusType(org.osid.type.Type scheduleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSchedulesByGenusType(scheduleGenusType));
    }


    /**
     *  Gets a {@code ScheduleList} corresponding to the given
     *  schedule genus {@code Type} and include any additional
     *  schedules with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  schedules or an error results. Otherwise, the returned list
     *  may contain only those schedules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  scheduleGenusType a schedule genus type 
     *  @return the returned {@code Schedule} list
     *  @throws org.osid.NullArgumentException
     *          {@code scheduleGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedulesByParentGenusType(org.osid.type.Type scheduleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSchedulesByParentGenusType(scheduleGenusType));
    }


    /**
     *  Gets a {@code ScheduleList} containing the given schedule
     *  record {@code Type}.
     * 
     *  In plenary mode, the returned list contains all known
     *  schedules or an error results. Otherwise, the returned list
     *  may contain only those schedules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  scheduleRecordType a schedule record type 
     *  @return the returned {@code Schedule} list
     *  @throws org.osid.NullArgumentException
     *          {@code scheduleRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedulesByRecordType(org.osid.type.Type scheduleRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSchedulesByRecordType(scheduleRecordType));
    }


    /**
     *  Gets a {@code ScheduleList} directly containing the
     *  given shedule slot. {@code} In plenary mode, the
     *  returned list contains all known schedule or an error
     *  results. Otherwise, the returned list may contain only those
     *  schedule that are accessible through this session.
     *
     *  @param  scheduleSlotId a schedule slot {@code Id} 
     *  @return the returned {@code Schedule} list 
     *  @throws org.osid.NullArgumentException {@code scheduleSlotId} 
     *          is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedulesByScheduleSlot(org.osid.id.Id scheduleSlotId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getSchedulesByScheduleSlot(scheduleSlotId));
    }


    /**
     *  Gets a {@code ScheduleList} containing the given
     *  location.  In plenary mode, the returned list contains all
     *  known schedule or an error results. Otherwise, the returned
     *  list may contain only those schedule that are accessible
     *  through this session.
     *
     *  @param  locationId a location {@code Id} 
     *  @return the returned {@code Schedule} list 
     *  @throws org.osid.NullArgumentException {@code locationId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedulesByLocation(org.osid.id.Id locationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSchedulesByLocation(locationId));
    }


    /**
     *  Gets a {@code ScheduleList} containing the given date.
     *  In plenary mode, the returned list contains all known schedule
     *  or an error results. Otherwise, the returned list may contain
     *  only those schedule that are accessible through this session.
     *
     *  @param  date a date 
     *  @return the returned {@code Schedule} list 
     *  @throws org.osid.NullArgumentException {@code date} is {@code 
     *          null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedulesByDate(org.osid.calendaring.DateTime date)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSchedulesByDate(date));
    }


    /**
     *  Gets a {@code ScheduleList} contained by the given date
     *  range inclusive. In plenary mode, the returned list contains
     *  all known schedule or an error results. Otherwise, the
     *  returned list may contain only those schedule that are
     *  accessible through this session.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Schedule} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code from} or {@code
     *          to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedulesByDateRange(org.osid.calendaring.DateTime from, 
                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSchedulesByDateRange(from, to));
    }


    /**
     *  Gets all {@code Schedules}. 
     *
     *  In plenary mode, the returned list contains all known
     *  schedules or an error results. Otherwise, the returned list
     *  may contain only those schedules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Schedules} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedules()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSchedules());
    }
}
