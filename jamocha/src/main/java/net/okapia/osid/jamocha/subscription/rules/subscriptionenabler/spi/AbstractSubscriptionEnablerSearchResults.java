//
// AbstractSubscriptionEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.subscription.rules.subscriptionenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractSubscriptionEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.subscription.rules.SubscriptionEnablerSearchResults {

    private org.osid.subscription.rules.SubscriptionEnablerList subscriptionEnablers;
    private final org.osid.subscription.rules.SubscriptionEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.subscription.rules.records.SubscriptionEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractSubscriptionEnablerSearchResults.
     *
     *  @param subscriptionEnablers the result set
     *  @param subscriptionEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>subscriptionEnablers</code>
     *          or <code>subscriptionEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractSubscriptionEnablerSearchResults(org.osid.subscription.rules.SubscriptionEnablerList subscriptionEnablers,
                                            org.osid.subscription.rules.SubscriptionEnablerQueryInspector subscriptionEnablerQueryInspector) {
        nullarg(subscriptionEnablers, "subscription enablers");
        nullarg(subscriptionEnablerQueryInspector, "subscription enabler query inspectpr");

        this.subscriptionEnablers = subscriptionEnablers;
        this.inspector = subscriptionEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the subscription enabler list resulting from a search.
     *
     *  @return a subscription enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionEnablerList getSubscriptionEnablers() {
        if (this.subscriptionEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.subscription.rules.SubscriptionEnablerList subscriptionEnablers = this.subscriptionEnablers;
        this.subscriptionEnablers = null;
	return (subscriptionEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.subscription.rules.SubscriptionEnablerQueryInspector getSubscriptionEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  subscription enabler search record <code> Type. </code> This method must
     *  be used to retrieve a subscriptionEnabler implementing the requested
     *  record.
     *
     *  @param subscriptionEnablerSearchRecordType a subscriptionEnabler search 
     *         record type 
     *  @return the subscription enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(subscriptionEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.subscription.rules.records.SubscriptionEnablerSearchResultsRecord getSubscriptionEnablerSearchResultsRecord(org.osid.type.Type subscriptionEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.subscription.rules.records.SubscriptionEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(subscriptionEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(subscriptionEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record subscription enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addSubscriptionEnablerRecord(org.osid.subscription.rules.records.SubscriptionEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "subscription enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
