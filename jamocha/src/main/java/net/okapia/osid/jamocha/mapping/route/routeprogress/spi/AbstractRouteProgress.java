//
// AbstractRouteProgress.java
//
//     Defines a RouteProgress.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.route.routeprogress.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>RouteProgress</code>.
 */

public abstract class AbstractRouteProgress
    extends net.okapia.osid.jamocha.spi.AbstractOsidCompendium
    implements org.osid.mapping.route.RouteProgress {

    private org.osid.resource.Resource resource;
    private org.osid.mapping.route.Route route;
    private org.osid.calendaring.DateTime timeStarted;
    private org.osid.mapping.Distance totalDistanceTraveled;
    private org.osid.calendaring.Duration totalTravelTime;
    private org.osid.calendaring.Duration totalIdleTime;
    private org.osid.calendaring.DateTime timeLastMoved;
    private org.osid.mapping.route.RouteSegment routeSegment;
    private org.osid.calendaring.Duration eta;
    private org.osid.mapping.Distance routeSegmentTraveled;
    private org.osid.calendaring.DateTime timeCompleted;

    private boolean isInMotion = false;
    private boolean isComplete = false;

    private final java.util.Collection<org.osid.mapping.route.records.RouteProgressRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the resource on the route. 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResourceId() {
        return (this.resource.getId());
    }


    /**
     *  Gets the resource on the route. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource()
        throws org.osid.OperationFailedException {

        return (this.resource);
    }


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @throws org.osid.NullArgumentException
     *          <code>resource</code> is <code>null</code>
     */

    protected void setResource(org.osid.resource.Resource resource) {
        nullarg(resource, "resource");
        this.resource = resource;
        return;
    }


    /**
     *  Gets the route <code> Id. </code> 
     *
     *  @return the route <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getRouteId() {
        return (this.route.getId());
    }


    /**
     *  Gets the route. 
     *
     *  @return the route 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.mapping.route.Route getRoute()
        throws org.osid.OperationFailedException {

        return (this.route);
    }


    /**
     *  Sets the route.
     *
     *  @param route a route
     *  @throws org.osid.NullArgumentException
     *          <code>route</code> is <code>null</code>
     */

    protected void setRoute(org.osid.mapping.route.Route route) {
        nullarg(route, "route");
        this.route = route;
        return;
    }


    /**
     *  Gets the starting time on this route. 
     *
     *  @return the starting time 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getTimeStarted() {
        return (this.timeStarted);
    }


    /**
     *  Sets the time started.
     *
     *  @param time the time started
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    protected void setTimeStarted(org.osid.calendaring.DateTime time) {
        nullarg(time, "time started");
        this.timeStarted = time;
        return;
    }


    /**
     *  Gets the total distance traveled. 
     *
     *  @return the distance 
     */

    @OSID @Override
    public org.osid.mapping.Distance getTotalDistanceTraveled() {
        return (this.totalDistanceTraveled);
    }


    /**
     *  Sets the total distance traveled.
     *
     *  @param distance the total distance traveled
     *  @throws org.osid.NullArgumentException <code>distance</code>
     *          is <code>null</code>
     */

    protected void setTotalDistanceTraveled(org.osid.mapping.Distance distance) {
        nullarg(distance, "distance");
        this.totalDistanceTraveled = distance;
        return;
    }


    /**
     *  Gets the total travel time. 
     *
     *  @return total travel time 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getTotalTravelTime() {
        return (this.totalTravelTime);
    }


    /**
     *  Sets the total travel time.
     *
     *  @param duration the total travel time
     *  @throws org.osid.NullArgumentException <code>duration</code>
     *          is <code>null</code>
     */

    protected void setTotalTravelTime(org.osid.calendaring.Duration duration) {
        nullarg(duration, "duration");
        this.totalTravelTime = duration;
        return;
    }


    /**
     *  Tests if the object is in motion. 
     *
     *  @return <code> true </code> if the object is in motion, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isInMotion() {
        return (this.isInMotion);
    }


    /**
     *  Sets the in motion.
     *
     *  @param inMotion <code> true </code> if the object is in
     *          motion, <code> false </code> otherwise
     */

    protected void setInMotion(boolean inMotion) {
        this.isInMotion = inMotion;
        return;
    }


    /**
     *  Gets the total idle time before completion of the route. 
     *
     *  @return total idle time 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getTotalIdleTime() {
        return (this.totalIdleTime);
    }


    /**
     *  Sets the total idle time.
     *
     *  @param duration the total idle time
     *  @throws org.osid.NullArgumentException <code>duration</code>
     *          is <code>null</code>
     */

    protected void setTotalIdleTime(org.osid.calendaring.Duration duration) {
        nullarg(duration, "duration");
        this.totalIdleTime = duration;
        return;
    }


    /**
     *  Get sthe time the object last moved. 
     *
     *  @return last moved time 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getTimeLastMoved() {
        return (this.timeLastMoved);
    }


    /**
     *  Sets the time last moved.
     *
     *  @param time the time last moved
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    protected void setTimeLastMoved(org.osid.calendaring.DateTime time) {
        nullarg(time, "time last moved");
        this.timeLastMoved = time;
        return;
    }


    /**
     *  Tests if the route has been completed. 
     *
     *  @return <code> true </code> if the route has been completed, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean isComplete() {
        return (this.isComplete);
    }


    /**
     *  Sets the complete flag.
     *
     *  @param complete <code> true </code> if the route has been
     *          completed, <code> false </code> otherwisee
     */

    protected void setComplete(boolean complete) {
        this.isComplete = complete;
        return;
    }


    /**
     *  Gets the current route segment <code> Id. </code> 
     *
     *  @return the route segment <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isComplete() </code> is 
     *          <code> true </code> 
     */

    @OSID @Override
    public org.osid.id.Id getRouteSegmentId() {
        if (isComplete()) {
            throw new org.osid.IllegalStateException("isComplete() is true");
        }

        return (this.routeSegment.getId());
    }


    /**
     *  Gets the current route segment. 
     *
     *  @return the route segment 
     *  @throws org.osid.IllegalStateException <code> isComplete()
     *          </code> is <code> true </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteSegment getRouteSegment()
        throws org.osid.OperationFailedException {

        if (isComplete()) {
            throw new org.osid.IllegalStateException("isComplete() is true");
        }

        return (this.routeSegment);
    }


    /**
     *  Sets the route segment.
     *
     *  @param routeSegment a route segment
     *  @throws org.osid.NullArgumentException
     *          <code>routeSegment</code> is <code>null</code>
     */

    protected void setRouteSegment(org.osid.mapping.route.RouteSegment routeSegment) {
        nullarg(routeSegment, "route segment");
        this.routeSegment = routeSegment;
        return;
    }


    /**
     *  Gets the estimated travel time to the next route segment.
     *
     *  @return the estimated travel time 
     *  @throws org.osid.IllegalStateException <code> isComplete()
     *          </code> is <code> true </code>
     */

    @OSID @Override
    public org.osid.calendaring.Duration getETAToNextSegment() {
        if (isComplete()) {
            throw new org.osid.IllegalStateException("isComplete() is true");
        }

        return (this.eta);
    }


    /**
     *  Sets the estimated travel time to next segment.
     *
     *  @param duration the estimated time to next segment
     *  @throws org.osid.NullArgumentException <code>duration</code> is
     *          <code>null</code>
     */

    protected void setETAToNextSegment(org.osid.calendaring.Duration duration) {
        nullarg(duration, "estimated travel time");
        this.eta = duration;
        return;
    }


    /**
     *  Gets the distance along the current route segment traveled.
     *
     *  @return distance along the current segment traveled 
     *  @throws org.osid.IllegalStateException <code> isComplete() </code> is 
     *          <code> true </code> 
     */

    @OSID @Override
    public org.osid.mapping.Distance getRouteSegmentTraveled() {
        if (isComplete()) {
            throw new org.osid.IllegalStateException("isComplete() is true");
        }

        return (this.routeSegmentTraveled);
    }


    /**
     *  Sets the distance laong the route segment traveled.
     *
     *  @param distance the travel distance
     *  @throws org.osid.NullArgumentException <code>distance</code>
     *          is <code>null</code>
     */

    protected void setRouteSegmentTraveled(org.osid.mapping.Distance distance) {
        nullarg(distance, "distance traveled");
        this.routeSegmentTraveled = distance;
        return;
    }


    /**
     *  Gets the ending time for this route. 
     *
     *  @return the ending time 
     *  @throws org.osid.IllegalStateException <code> isComplete() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getTimeCompleted() {
        if (!isComplete()) {
            throw new org.osid.IllegalStateException("isComplete() is true");
        }

        return (this.timeCompleted);
    }


    /**
     *  Sets the time completed.
     *
     *  @param time the time completed
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    protected void setTimeCompleted(org.osid.calendaring.DateTime time) {
        nullarg(time, "time completed");
        this.timeCompleted = time;
        return;
    }


    /**
     *  Tests if this routeProgress supports the given record
     *  <code>Type</code>.
     *
     *  @param  routeProgressRecordType a route progress record type 
     *  @return <code>true</code> if the routeProgressRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>routeProgressRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type routeProgressRecordType) {
        for (org.osid.mapping.route.records.RouteProgressRecord record : this.records) {
            if (record.implementsRecordType(routeProgressRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>RouteProgress</code> record <code>Type</code>.
     *
     *  @param routeProgressRecordType the route progress record type
     *  @return the route progress record 
     *  @throws org.osid.NullArgumentException
     *          <code>routeProgressRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(routeProgressRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.route.records.RouteProgressRecord getRouteProgressRecord(org.osid.type.Type routeProgressRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.route.records.RouteProgressRecord record : this.records) {
            if (record.implementsRecordType(routeProgressRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(routeProgressRecordType + " is not supported");
    }


    /**
     *  Adds a record to this route progress. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param routeProgressRecord the route progress record
     *  @param routeProgressRecordType route progress record type
     *  @throws org.osid.NullArgumentException
     *          <code>routeProgressRecord</code> or
     *          <code>routeProgressRecordTyperouteProgress</code> is
     *          <code>null</code>
     */
            
    protected void addRouteProgressRecord(org.osid.mapping.route.records.RouteProgressRecord routeProgressRecord, 
                                     org.osid.type.Type routeProgressRecordType) {

        addRecordType(routeProgressRecordType);
        this.records.add(routeProgressRecord);
        
        return;
    }
}
