//
// AbstractDeed.java
//
//     Defines a Deed.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.squatting.deed.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Deed</code>.
 */

public abstract class AbstractDeed
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.room.squatting.Deed {

    private org.osid.room.Building building;
    private org.osid.resource.Resource owner;

    private final java.util.Collection<org.osid.room.squatting.records.DeedRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the owner building. 
     *
     *  @return the building <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getBuildingId() {
        return (this.building.getId());
    }


    /**
     *  Gets the owner building. 
     *
     *  @return the building 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.room.Building getBuilding()
        throws org.osid.OperationFailedException {

        return (this.building);
    }


    /**
     *  Sets the building.
     *
     *  @param building a building
     *  @throws org.osid.NullArgumentException
     *          <code>building</code> is <code>null</code>
     */

    protected void setBuilding(org.osid.room.Building building) {
        nullarg(building, "building");
        this.building = building;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the owner resource. 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getOwnerId() {
        return (this.owner.getId());
    }


    /**
     *  Gets the owner resource. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getOwner()
        throws org.osid.OperationFailedException {

        return (this.owner);
    }


    /**
     *  Sets the owner.
     *
     *  @param owner an owner
     *  @throws org.osid.NullArgumentException
     *          <code>owner</code> is <code>null</code>
     */

    protected void setOwner(org.osid.resource.Resource owner) {
        nullarg(owner, "owner");
        this.owner = owner;
        return;
    }


    /**
     *  Tests if this deed supports the given record
     *  <code>Type</code>.
     *
     *  @param  deedRecordType a deed record type 
     *  @return <code>true</code> if the deedRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>deedRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type deedRecordType) {
        for (org.osid.room.squatting.records.DeedRecord record : this.records) {
            if (record.implementsRecordType(deedRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>Deed</code>
     *  record <code>Type</code>.
     *
     *  @param  deedRecordType the deed record type 
     *  @return the deed record 
     *  @throws org.osid.NullArgumentException
     *          <code>deedRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(deedRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.room.squatting.records.DeedRecord getDeedRecord(org.osid.type.Type deedRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.squatting.records.DeedRecord record : this.records) {
            if (record.implementsRecordType(deedRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(deedRecordType + " is not supported");
    }


    /**
     *  Adds a record to this deed. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param deedRecord the deed record
     *  @param deedRecordType deed record type
     *  @throws org.osid.NullArgumentException
     *          <code>deedRecord</code> or
     *          <code>deedRecordTypedeed</code> is
     *          <code>null</code>
     */
            
    protected void addDeedRecord(org.osid.room.squatting.records.DeedRecord deedRecord, 
                                     org.osid.type.Type deedRecordType) {

        nullarg(deedRecord, "deed record");
        addRecordType(deedRecordType);
        this.records.add(deedRecord);
        
        return;
    }
}
