//
// AbstractOffsetEventLookupSession.java
//
//    A starter implementation framework for providing an OffsetEvent
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an OffsetEvent
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getOffsetEvents(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractOffsetEventLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.calendaring.OffsetEventLookupSession {

    private boolean pedantic   = false;
    private boolean activeonly = false;
    private boolean federated  = false;
    private org.osid.calendaring.Calendar calendar = new net.okapia.osid.jamocha.nil.calendaring.calendar.UnknownCalendar();
    

    /**
     *  Gets the <code>Calendar/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Calendar Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.calendar.getId());
    }


    /**
     *  Gets the <code>Calendar</code> associated with this 
     *  session.
     *
     *  @return the <code>Calendar</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.calendar);
    }


    /**
     *  Sets the <code>Calendar</code>.
     *
     *  @param  calendar the calendar for this session
     *  @throws org.osid.NullArgumentException <code>calendar</code>
     *          is <code>null</code>
     */

    protected void setCalendar(org.osid.calendaring.Calendar calendar) {
        nullarg(calendar, "calendar");
        this.calendar = calendar;
        return;
    }

    /**
     *  Tests if this user can perform <code>OffsetEvent</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupOffsetEvents() {
        return (true);
    }


    /**
     *  A complete view of the <code>OffsetEvent</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeOffsetEventView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>OffsetEvent</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryOffsetEventView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include offset events in calendars which are
     *  children of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active offset events are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveOffsetEventView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive offset events are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusOffsetEventView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>OffsetEvent</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>OffsetEvent</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>OffsetEvent</code> and
     *  retained for compatibility.
     *
     *  In active mode, offset events are returned that are currently
     *  active. In any status mode, active and inactive offset events
     *  are returned.
     *
     *  @param  offsetEventId <code>Id</code> of the
     *          <code>OffsetEvent</code>
     *  @return the offset event
     *  @throws org.osid.NotFoundException <code>offsetEventId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>offsetEventId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEvent getOffsetEvent(org.osid.id.Id offsetEventId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.calendaring.OffsetEventList offsetEvents = getOffsetEvents()) {
            while (offsetEvents.hasNext()) {
                org.osid.calendaring.OffsetEvent offsetEvent = offsetEvents.getNextOffsetEvent();
                if (offsetEvent.getId().equals(offsetEventId)) {
                    return (offsetEvent);
                }
            }
        } 

        throw new org.osid.NotFoundException(offsetEventId + " not found");
    }


    /**
     *  Gets a <code>OffsetEventList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  offsetEvents specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>OffsetEvents</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, offset events are returned that are currently
     *  active. In any status mode, active and inactive offset events
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getOffsetEvents()</code>.
     *
     *  @param  offsetEventIds the list of <code>Ids</code> to rerieve 
     *  @return the returned <code>OffsetEvent</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>offsetEventIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventList getOffsetEventsByIds(org.osid.id.IdList offsetEventIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.calendaring.OffsetEvent> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = offsetEventIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getOffsetEvent(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("offset event " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.calendaring.offsetevent.LinkedOffsetEventList(ret));
    }


    /**
     *  Gets a <code>OffsetEventList</code> corresponding to the given
     *  offset event genus <code>Type</code> which does not include
     *  offset events of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  offset events or an error results. Otherwise, the returned list
     *  may contain only those offset events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, offset events are returned that are currently
     *  active. In any status mode, active and inactive offset events
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getOffsetEvents()</code>.
     *
     *  @param  offsetEventGenusType an offsetEvent genus type 
     *  @return the returned <code>OffsetEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>offsetEventGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventList getOffsetEventsByGenusType(org.osid.type.Type offsetEventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.offsetevent.OffsetEventGenusFilterList(getOffsetEvents(), offsetEventGenusType));
    }


    /**
     *  Gets a <code>OffsetEventList</code> corresponding to the given
     *  offset event genus <code>Type</code> and include any additional
     *  offset events with genus types derived from the specified
     *  <code>Type</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  offset events or an error results. Otherwise, the returned list
     *  may contain only those offset events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, offset events are returned that are currently
     *  active. In any status mode, active and inactive offset events
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getOffsetEvents()</code>.
     *
     *  @param  offsetEventGenusType an offsetEvent genus type 
     *  @return the returned <code>OffsetEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>offsetEventGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventList getOffsetEventsByParentGenusType(org.osid.type.Type offsetEventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getOffsetEventsByGenusType(offsetEventGenusType));
    }


    /**
     *  Gets a <code>OffsetEventList</code> containing the given
     *  offset event record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  offset events or an error results. Otherwise, the returned list
     *  may contain only those offset events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, offset events are returned that are currently
     *  active. In any status mode, active and inactive offset events
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getOffsetEvents()</code>.
     *
     *  @param  offsetEventRecordType an offsetEvent record type 
     *  @return the returned <code>OffsetEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>offsetEventRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventList getOffsetEventsByRecordType(org.osid.type.Type offsetEventRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.offsetevent.OffsetEventRecordFilterList(getOffsetEvents(), offsetEventRecordType));
    }

    
    /**
     *  Gets the <code> OffsetEvents </code> using the given event as
     *  a start or ending offset.
     *
     *  In plenary mode, the returned list contains all known offset
     *  events or an error results. Otherwise, the returned list may
     *  contain only those offset events that are accessible through
     *  this session.
     *
     *  In active mode, offset events are returned that are currently
     *  active.  In any status mode, active and inactive offset events
     *  are returned.
     *
     *  @param  eventId <code> Id </code> of the related event
     *  @return the offset events
     *  @throws org.osid.NullArgumentException <code> eventId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */
    
    @OSID @Override
    public org.osid.calendaring.OffsetEventList getOffsetEventsByEvent(org.osid.id.Id eventId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.offsetevent.OffsetEventFilterList(new EventFilter(eventId), getOffsetEvents()));
    }
        

    /**
     *  Gets all <code>OffsetEvents</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  offset events or an error results. Otherwise, the returned list
     *  may contain only those offset events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, offset events are returned that are currently
     *  active. In any status mode, active and inactive offset events
     *  are returned.
     *
     *  @return a list of <code>OffsetEvents</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.calendaring.OffsetEventList getOffsetEvents()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the offset event list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of offset events
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.calendaring.OffsetEventList filterOffsetEventsOnViews(org.osid.calendaring.OffsetEventList list)
        throws org.osid.OperationFailedException {
            
        org.osid.calendaring.OffsetEventList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.calendaring.offsetevent.ActiveOffsetEventFilterList(ret);
        }

        return (ret);
    }


    public static class EventFilter
        implements net.okapia.osid.jamocha.inline.filter.calendaring.offsetevent.OffsetEventFilter {
         
        private final org.osid.id.Id eventId;
         
         
        /**
         *  Constructs a new <code>EventFilter</code>.
         *
         *  @param eventId the event to filter
         *  @throws org.osid.NullArgumentException
         *          <code>eventId</code> is <code>null</code>
         */
        
        public EventFilter(org.osid.id.Id eventId) {
            nullarg(eventId, "event Id");
            this.eventId = eventId;
            return;
        }

         
        /**
         *  Used by the OffsetEventFilterList to filter the offset
         *  event list based on event.
         *
         *  @param offsetEvent the offset event
         *  @return <code>true</code> to pass the offset event,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.calendaring.OffsetEvent offsetEvent) {
            if (!offsetEvent.hasFixedStartTime()) {
                if (offsetEvent.getStartReferenceEventId().equals(this.eventId)) {
                    return (true);
                }
            }

            if (!offsetEvent.hasFixedDuration()) {
                if (offsetEvent.getEndReferenceEventId().equals(this.eventId)) {
                    return (true);
                }
            }

            return (false);
        }
    }
}
