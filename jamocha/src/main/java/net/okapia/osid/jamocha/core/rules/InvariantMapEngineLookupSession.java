//
// InvariantMapEngineLookupSession
//
//    Implements an Engine lookup service backed by a fixed collection of
//    engines.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.rules;


/**
 *  Implements an Engine lookup service backed by a fixed
 *  collection of engines. The engines are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapEngineLookupSession
    extends net.okapia.osid.jamocha.core.rules.spi.AbstractMapEngineLookupSession
    implements org.osid.rules.EngineLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapEngineLookupSession</code> with no
     *  engines.
     */

    public InvariantMapEngineLookupSession() {
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapEngineLookupSession</code> with a single
     *  engine.
     *  
     *  @throws org.osid.NullArgumentException {@code engine}
     *          is <code>null</code>
     */

    public InvariantMapEngineLookupSession(org.osid.rules.Engine engine) {
        putEngine(engine);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapEngineLookupSession</code> using an array
     *  of engines.
     *  
     *  @throws org.osid.NullArgumentException {@code engines}
     *          is <code>null</code>
     */

    public InvariantMapEngineLookupSession(org.osid.rules.Engine[] engines) {
        putEngines(engines);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapEngineLookupSession</code> using a
     *  collection of engines.
     *
     *  @throws org.osid.NullArgumentException {@code engines}
     *          is <code>null</code>
     */

    public InvariantMapEngineLookupSession(java.util.Collection<? extends org.osid.rules.Engine> engines) {
        putEngines(engines);
        return;
    }
}
