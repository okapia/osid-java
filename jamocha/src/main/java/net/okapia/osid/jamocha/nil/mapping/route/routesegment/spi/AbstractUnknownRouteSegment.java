//
// AbstractUnknownRouteSegment.java
//
//     Defines an unknown RouteSegment.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.mapping.route.routesegment.spi;


/**
 *  Defines an unknown <code>RouteSegment</code>.
 */

public abstract class AbstractUnknownRouteSegment
    extends net.okapia.osid.jamocha.mapping.route.routesegment.spi.AbstractRouteSegment
    implements org.osid.mapping.route.RouteSegment {

    protected static final String OBJECT = "osid.mapping.route.RouteSegment";


    /**
     *  Constructs a new <code>AbstractUnknownRouteSegment</code>.
     */

    public AbstractUnknownRouteSegment() {
        setId(net.okapia.osid.jamocha.nil.privateutil.UnknownId.valueOf(OBJECT));
        setDisplayName(net.okapia.osid.jamocha.nil.privateutil.DisplayName.valueOf(OBJECT));
        setDescription(net.okapia.osid.jamocha.nil.privateutil.Description.valueOf(OBJECT));

        setStartingInstructions(net.okapia.osid.jamocha.nil.privateutil.Text.valueOf("go"));
        setEndingInstructions(net.okapia.osid.jamocha.nil.privateutil.Text.valueOf("sto"));
        
        setDistance(new net.okapia.osid.primordium.mapping.Distance(0));
        setETA(net.okapia.osid.primordium.calendaring.GregorianUTCDuration.unknown());
        
        return;
    }


    /**
     *  Constructs a new <code>AbstractUnknownRouteSegment</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public AbstractUnknownRouteSegment(boolean optional) {
        this();
        setPath(new net.okapia.osid.jamocha.nil.mapping.path.path.UnknownPath());        
        return;
    }
}
