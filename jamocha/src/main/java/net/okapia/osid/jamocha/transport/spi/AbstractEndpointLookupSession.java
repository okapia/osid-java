//
// AbstractEndpointLookupSession.java
//
//    A starter implementation framework for providing an Endpoint
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.transport.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A starter implementation framework for providing an Endpoint
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getEndpoints(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractEndpointLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.transport.EndpointLookupSession {

    private boolean pedantic      = false;
    private org.osid.transport.Endpoint endpoint = new net.okapia.osid.jamocha.nil.transport.endpoint.UnknownEndpoint();
    


    /**
     *  Tests if this user can perform <code>Endpoint</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupEndpoints() {
        return (true);
    }


    /**
     *  A complete view of the <code>Endpoint</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeEndpointView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Endpoint</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryEndpointView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }

     
    /**
     *  Gets the <code>Endpoint</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Endpoint</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Endpoint</code> and
     *  retained for compatibility.
     *
     *  @param  endpointId <code>Id</code> of the
     *          <code>Endpoint</code>
     *  @return the endpoint
     *  @throws org.osid.NotFoundException <code>endpointId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>endpointId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.transport.Endpoint getEndpoint(org.osid.id.Id endpointId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.transport.EndpointList endpoints = getEndpoints()) {
            while (endpoints.hasNext()) {
                org.osid.transport.Endpoint endpoint = endpoints.getNextEndpoint();
                if (endpoint.getId().equals(endpointId)) {
                    return (endpoint);
                }
            }
        } 

        throw new org.osid.NotFoundException(endpointId + " not found");
    }


    /**
     *  Gets an <code>EndpointList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  endpoints specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Endpoints</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getEndpoints()</code>.
     *
     *  @param  endpointIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Endpoint</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>endpointIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.transport.EndpointList getEndpointsByIds(org.osid.id.IdList endpointIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.transport.Endpoint> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = endpointIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getEndpoint(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("endpoint " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.transport.endpoint.LinkedEndpointList(ret));
    }


    /**
     *  Gets an <code>EndpointList</code> corresponding to the given
     *  endpoint genus <code>Type</code> which does not include
     *  endpoints of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  endpoints or an error results. Otherwise, the returned list
     *  may contain only those endpoints that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getEndpoints()</code>.
     *
     *  @param  endpointGenusType an endpoint genus type 
     *  @return the returned <code>Endpoint</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>endpointGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.transport.EndpointList getEndpointsByGenusType(org.osid.type.Type endpointGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.transport.endpoint.EndpointGenusFilterList(getEndpoints(), endpointGenusType));
    }


    /**
     *  Gets an <code>EndpointList</code> corresponding to the given
     *  endpoint genus <code>Type</code> and include any additional
     *  endpoints with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  endpoints or an error results. Otherwise, the returned list
     *  may contain only those endpoints that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getEndpoints()</code>.
     *
     *  @param  endpointGenusType an endpoint genus type 
     *  @return the returned <code>Endpoint</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>endpointGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.transport.EndpointList getEndpointsByParentGenusType(org.osid.type.Type endpointGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getEndpointsByGenusType(endpointGenusType));
    }


    /**
     *  Gets an <code>EndpointList</code> containing the given
     *  endpoint record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  endpoints or an error results. Otherwise, the returned list
     *  may contain only those endpoints that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getEndpoints()</code>.
     *
     *  @param  endpointRecordType an endpoint record type 
     *  @return the returned <code>Endpoint</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>endpointRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.transport.EndpointList getEndpointsByRecordType(org.osid.type.Type endpointRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.transport.endpoint.EndpointRecordFilterList(getEndpoints(), endpointRecordType));
    }


    /**
     *  Gets an <code>EndpointList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known endpoints or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  endpoints that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Endpoint</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.transport.EndpointList getEndpointsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.transport.endpoint.EndpointProviderFilterList(getEndpoints(), resourceId));
    }


    /**
     *  Gets all <code>Endpoints</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  endpoints or an error results. Otherwise, the returned list
     *  may contain only those endpoints that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Endpoints</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.transport.EndpointList getEndpoints()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the endpoint list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of endpoints
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.transport.EndpointList filterEndpointsOnViews(org.osid.transport.EndpointList list)
        throws org.osid.OperationFailedException {

        org.osid.transport.EndpointList ret = list;

        return (ret);
    }
}
