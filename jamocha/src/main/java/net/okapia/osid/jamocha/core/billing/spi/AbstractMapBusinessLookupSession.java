//
// AbstractMapBusinessLookupSession
//
//    A simple framework for providing a Business lookup service
//    backed by a fixed collection of businesses.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.billing.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Business lookup service backed by a
 *  fixed collection of businesses. The businesses are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Businesses</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapBusinessLookupSession
    extends net.okapia.osid.jamocha.billing.spi.AbstractBusinessLookupSession
    implements org.osid.billing.BusinessLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.billing.Business> businesses = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.billing.Business>());


    /**
     *  Makes a <code>Business</code> available in this session.
     *
     *  @param  business a business
     *  @throws org.osid.NullArgumentException <code>business<code>
     *          is <code>null</code>
     */

    protected void putBusiness(org.osid.billing.Business business) {
        this.businesses.put(business.getId(), business);
        return;
    }


    /**
     *  Makes an array of businesses available in this session.
     *
     *  @param  businesses an array of businesses
     *  @throws org.osid.NullArgumentException <code>businesses<code>
     *          is <code>null</code>
     */

    protected void putBusinesses(org.osid.billing.Business[] businesses) {
        putBusinesses(java.util.Arrays.asList(businesses));
        return;
    }


    /**
     *  Makes a collection of businesses available in this session.
     *
     *  @param  businesses a collection of businesses
     *  @throws org.osid.NullArgumentException <code>businesses<code>
     *          is <code>null</code>
     */

    protected void putBusinesses(java.util.Collection<? extends org.osid.billing.Business> businesses) {
        for (org.osid.billing.Business business : businesses) {
            this.businesses.put(business.getId(), business);
        }

        return;
    }


    /**
     *  Removes a Business from this session.
     *
     *  @param  businessId the <code>Id</code> of the business
     *  @throws org.osid.NullArgumentException <code>businessId<code> is
     *          <code>null</code>
     */

    protected void removeBusiness(org.osid.id.Id businessId) {
        this.businesses.remove(businessId);
        return;
    }


    /**
     *  Gets the <code>Business</code> specified by its <code>Id</code>.
     *
     *  @param  businessId <code>Id</code> of the <code>Business</code>
     *  @return the business
     *  @throws org.osid.NotFoundException <code>businessId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>businessId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.Business getBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.billing.Business business = this.businesses.get(businessId);
        if (business == null) {
            throw new org.osid.NotFoundException("business not found: " + businessId);
        }

        return (business);
    }


    /**
     *  Gets all <code>Businesses</code>. In plenary mode, the returned
     *  list contains all known businesses or an error
     *  results. Otherwise, the returned list may contain only those
     *  businesses that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Businesses</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.BusinessList getBusinesses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.billing.business.ArrayBusinessList(this.businesses.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.businesses.clear();
        super.close();
        return;
    }
}
