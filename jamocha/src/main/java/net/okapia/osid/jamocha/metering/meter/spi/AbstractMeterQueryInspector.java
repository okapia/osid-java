//
// AbstractMeterQueryInspector.java
//
//     A template for making a MeterQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metering.meter.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for meters.
 */

public abstract class AbstractMeterQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.metering.MeterQueryInspector {

    private final java.util.Collection<org.osid.metering.records.MeterQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the utility <code> Id </code> terms. 
     *
     *  @return the utility <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getUtilityIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the utility terms. 
     *
     *  @return the utility terms 
     */

    @OSID @Override
    public org.osid.metering.UtilityQueryInspector[] getUtilityTerms() {
        return (new org.osid.metering.UtilityQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given meter query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a meter implementing the requested record.
     *
     *  @param meterRecordType a meter record type
     *  @return the meter query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>meterRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(meterRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.metering.records.MeterQueryInspectorRecord getMeterQueryInspectorRecord(org.osid.type.Type meterRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.metering.records.MeterQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(meterRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(meterRecordType + " is not supported");
    }


    /**
     *  Adds a record to this meter query. 
     *
     *  @param meterQueryInspectorRecord meter query inspector
     *         record
     *  @param meterRecordType meter record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addMeterQueryInspectorRecord(org.osid.metering.records.MeterQueryInspectorRecord meterQueryInspectorRecord, 
                                                   org.osid.type.Type meterRecordType) {

        addRecordType(meterRecordType);
        nullarg(meterRecordType, "meter record type");
        this.records.add(meterQueryInspectorRecord);        
        return;
    }
}
