//
// AbstractControlManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractControlManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.control.ControlManager,
               org.osid.control.ControlProxyManager {

    private final Types deviceRecordTypes                  = new TypeRefSet();
    private final Types deviceSearchRecordTypes            = new TypeRefSet();

    private final Types deviceReturnRecordTypes            = new TypeRefSet();
    private final Types controllerRecordTypes              = new TypeRefSet();
    private final Types controllerSearchRecordTypes        = new TypeRefSet();

    private final Types inputRecordTypes                   = new TypeRefSet();
    private final Types inputSearchRecordTypes             = new TypeRefSet();

    private final Types settingRecordTypes                 = new TypeRefSet();
    private final Types settingSearchRecordTypes           = new TypeRefSet();

    private final Types sceneRecordTypes                   = new TypeRefSet();
    private final Types sceneSearchRecordTypes             = new TypeRefSet();

    private final Types triggerRecordTypes                 = new TypeRefSet();
    private final Types triggerSearchRecordTypes           = new TypeRefSet();

    private final Types actionGroupRecordTypes             = new TypeRefSet();
    private final Types actionGroupSearchRecordTypes       = new TypeRefSet();

    private final Types actionRecordTypes                  = new TypeRefSet();
    private final Types systemRecordTypes                  = new TypeRefSet();
    private final Types systemSearchRecordTypes            = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractControlManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractControlManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any trigger federation is exposed. Federation is exposed when 
     *  a specific trigger may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  triggers appears as a single trigger. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if looking up devices is supported. 
     *
     *  @return <code> true </code> if device lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceLookup() {
        return (false);
    }


    /**
     *  Tests if querying devices is supported. 
     *
     *  @return <code> true </code> if device query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceQuery() {
        return (false);
    }


    /**
     *  Tests if searching devices is supported. 
     *
     *  @return <code> true </code> if device search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceSearch() {
        return (false);
    }


    /**
     *  Tests if a device administrative service is supported. 
     *
     *  @return <code> true </code> if device administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceAdmin() {
        return (false);
    }


    /**
     *  Tests if a device <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if device notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceNotification() {
        return (false);
    }


    /**
     *  Tests if a device system lookup service is supported. 
     *
     *  @return <code> true </code> if a device system lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceSystem() {
        return (false);
    }


    /**
     *  Tests if a device system assignment service is supported. 
     *
     *  @return <code> true </code> if a device to system assignment service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceSystemAssignment() {
        return (false);
    }


    /**
     *  Tests if a device smart system service is supported. 
     *
     *  @return <code> true </code> if an v smart system service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceSmartSystem() {
        return (false);
    }


    /**
     *  Tests if returning devices is supported. 
     *
     *  @return <code> true </code> if returning devices is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceReturn() {
        return (false);
    }


    /**
     *  Tests if looking up controllers is supported. 
     *
     *  @return <code> true </code> if controller lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsControllerLookup() {
        return (false);
    }


    /**
     *  Tests if querying controllers is supported. 
     *
     *  @return <code> true </code> if controller query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsControllerQuery() {
        return (false);
    }


    /**
     *  Tests if searching controllers is supported. 
     *
     *  @return <code> true </code> if controller search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsControllerSearch() {
        return (false);
    }


    /**
     *  Tests if controller administrative service is supported. 
     *
     *  @return <code> true </code> if controller administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsControllerAdmin() {
        return (false);
    }


    /**
     *  Tests if a controller notification service is supported. 
     *
     *  @return <code> true </code> if controller notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsControllerNotification() {
        return (false);
    }


    /**
     *  Tests if a controller trigger lookup service is supported. 
     *
     *  @return <code> true </code> if a controller trigger lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsControllerTrigger() {
        return (false);
    }


    /**
     *  Tests if a controller trigger service is supported. 
     *
     *  @return <code> true </code> if controller to trigger assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsControllerTriggerAssignment() {
        return (false);
    }


    /**
     *  Tests if a controller smart trigger lookup service is supported. 
     *
     *  @return <code> true </code> if a controller smart trigger service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsControllerSmartTrigger() {
        return (false);
    }


    /**
     *  Tests if looking up inputs is supported. 
     *
     *  @return <code> true </code> if input lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputLookup() {
        return (false);
    }


    /**
     *  Tests if querying inputs is supported. 
     *
     *  @return <code> true </code> if input query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputQuery() {
        return (false);
    }


    /**
     *  Tests if searching inputs is supported. 
     *
     *  @return <code> true </code> if input search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputSearch() {
        return (false);
    }


    /**
     *  Tests if an input administrative service is supported. 
     *
     *  @return <code> true </code> if input administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputAdmin() {
        return (false);
    }


    /**
     *  Tests if an input <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if input notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputNotification() {
        return (false);
    }


    /**
     *  Tests if an input system lookup service is supported. 
     *
     *  @return <code> true </code> if an input system lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputSystem() {
        return (false);
    }


    /**
     *  Tests if an input system assignment service is supported. 
     *
     *  @return <code> true </code> if an input to system assignment service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputSystemAssignment() {
        return (false);
    }


    /**
     *  Tests if an input smart system service is supported. 
     *
     *  @return <code> true </code> if a smart system service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputSmartSystem() {
        return (false);
    }


    /**
     *  Tests if looking up settings is supported. 
     *
     *  @return <code> true </code> if setting lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSettingLookup() {
        return (false);
    }


    /**
     *  Tests if querying settings is supported. 
     *
     *  @return <code> true </code> if setting query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSettingQuery() {
        return (false);
    }


    /**
     *  Tests if searching settings is supported. 
     *
     *  @return <code> true </code> if setting search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSettingSearch() {
        return (false);
    }


    /**
     *  Tests if setting <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if setting administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSettingAdmin() {
        return (false);
    }


    /**
     *  Tests if a setting <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if setting notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSettingNotification() {
        return (false);
    }


    /**
     *  Tests if a setting system lookup service is supported. 
     *
     *  @return <code> true </code> if a setting system lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSettingSystem() {
        return (false);
    }


    /**
     *  Tests if a setting system assignment service is supported. 
     *
     *  @return <code> true </code> if a setting to system assignment service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSettingSystemAssignment() {
        return (false);
    }


    /**
     *  Tests if a setting smart system service is supported. 
     *
     *  @return <code> true </code> if a setting smart system service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSettingSmartSystem() {
        return (false);
    }


    /**
     *  Tests if looking up scenes is supported. 
     *
     *  @return <code> true </code> if scene lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSceneLookup() {
        return (false);
    }


    /**
     *  Tests if querying scenes is supported. 
     *
     *  @return <code> true </code> if scene query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSceneQuery() {
        return (false);
    }


    /**
     *  Tests if searching scenes is supported. 
     *
     *  @return <code> true </code> if scene search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSceneSearch() {
        return (false);
    }


    /**
     *  Tests if scene <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if scene administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSceneAdmin() {
        return (false);
    }


    /**
     *  Tests if a scene <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if scene notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSceneNotification() {
        return (false);
    }


    /**
     *  Tests if a scene system lookup service is supported. 
     *
     *  @return <code> true </code> if a scene system lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSceneSystem() {
        return (false);
    }


    /**
     *  Tests if a scene system assignment service is supported. 
     *
     *  @return <code> true </code> if a scene to system assignment service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSceneSystemAssignment() {
        return (false);
    }


    /**
     *  Tests if a scene smart system service is supported. 
     *
     *  @return <code> true </code> if a scene smart system service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSceneSmartSystem() {
        return (false);
    }


    /**
     *  Tests if looking up triggers is supported. 
     *
     *  @return <code> true </code> if trigger lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerLookup() {
        return (false);
    }


    /**
     *  Tests if querying triggers is supported. 
     *
     *  @return <code> true </code> if a trigger query service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerQuery() {
        return (false);
    }


    /**
     *  Tests if searching triggers is supported. 
     *
     *  @return <code> true </code> if trigger search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerSearch() {
        return (false);
    }


    /**
     *  Tests if trigger administrative service is supported. 
     *
     *  @return <code> true </code> if trigger administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerAdmin() {
        return (false);
    }


    /**
     *  Tests if a trigger <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if trigger notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerNotification() {
        return (false);
    }


    /**
     *  Tests if a trigger system lookup service is supported. 
     *
     *  @return <code> true </code> if a trigger system lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerSystem() {
        return (false);
    }


    /**
     *  Tests if a trigger system service is supported. 
     *
     *  @return <code> true </code> if trigger to system assignment service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerSystemAssignment() {
        return (false);
    }


    /**
     *  Tests if a trigger smart system lookup service is supported. 
     *
     *  @return <code> true </code> if a trigger smart system service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerSmartSystem() {
        return (false);
    }


    /**
     *  Tests if looking up action groups is supported. 
     *
     *  @return <code> true </code> if action group lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionGroupLookup() {
        return (false);
    }


    /**
     *  Tests if querying action groups is supported. 
     *
     *  @return <code> true </code> if an action group query service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionGroupQuery() {
        return (false);
    }


    /**
     *  Tests if searching action groups is supported. 
     *
     *  @return <code> true </code> if action group search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionGroupSearch() {
        return (false);
    }


    /**
     *  Tests if action group administrative service is supported. 
     *
     *  @return <code> true </code> if action group administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionGroupAdmin() {
        return (false);
    }


    /**
     *  Tests if an action group <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if action group notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionGroupNotification() {
        return (false);
    }


    /**
     *  Tests if an action group system lookup service is supported. 
     *
     *  @return <code> true </code> if an action group system lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionGroupSystem() {
        return (false);
    }


    /**
     *  Tests if an action group system service is supported. 
     *
     *  @return <code> true </code> if action group to system assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionGroupSystemAssignment() {
        return (false);
    }


    /**
     *  Tests if an action group smart system lookup service is supported. 
     *
     *  @return <code> true </code> if an action group smart system service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionGroupSmartSystem() {
        return (false);
    }


    /**
     *  Tests if looking up systems is supported. 
     *
     *  @return <code> true </code> if system lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSystemLookup() {
        return (false);
    }


    /**
     *  Tests if querying systems is supported. 
     *
     *  @return <code> true </code> if a system query service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSystemQuery() {
        return (false);
    }


    /**
     *  Tests if searching systems is supported. 
     *
     *  @return <code> true </code> if system search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSystemSearch() {
        return (false);
    }


    /**
     *  Tests if system administrative service is supported. 
     *
     *  @return <code> true </code> if system administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSystemAdmin() {
        return (false);
    }


    /**
     *  Tests if a system <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if system notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSystemNotification() {
        return (false);
    }


    /**
     *  Tests for the availability of a system hierarchy traversal service. 
     *
     *  @return <code> true </code> if system hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSystemHierarchy() {
        return (false);
    }


    /**
     *  Tests for the availability of a system hierarchy design service. 
     *
     *  @return <code> true </code> if system hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSystemHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests for the availability of a control batch service. 
     *
     *  @return <code> true </code> if control batch service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsControlBatch() {
        return (false);
    }


    /**
     *  Tests for the availability of a control rules service. 
     *
     *  @return <code> true </code> if control rules service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsControlRules() {
        return (false);
    }


    /**
     *  Gets the supported <code> Device </code> record types. 
     *
     *  @return a list containing the supported <code> Device </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDeviceRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.deviceRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Device </code> record type is supported. 
     *
     *  @param  deviceRecordType a <code> Type </code> indicating a <code> 
     *          Device </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> deviceRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDeviceRecordType(org.osid.type.Type deviceRecordType) {
        return (this.deviceRecordTypes.contains(deviceRecordType));
    }


    /**
     *  Adds support for a device record type.
     *
     *  @param deviceRecordType a device record type
     *  @throws org.osid.NullArgumentException
     *  <code>deviceRecordType</code> is <code>null</code>
     */

    protected void addDeviceRecordType(org.osid.type.Type deviceRecordType) {
        this.deviceRecordTypes.add(deviceRecordType);
        return;
    }


    /**
     *  Removes support for a device record type.
     *
     *  @param deviceRecordType a device record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>deviceRecordType</code> is <code>null</code>
     */

    protected void removeDeviceRecordType(org.osid.type.Type deviceRecordType) {
        this.deviceRecordTypes.remove(deviceRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Device </code> search types. 
     *
     *  @return a list containing the supported <code> Device </code> search 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDeviceSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.deviceSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Device </code> search type is supported. 
     *
     *  @param  deviceSearchRecordType a <code> Type </code> indicating a 
     *          <code> Device </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> effiortSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDeviceSearchRecordType(org.osid.type.Type deviceSearchRecordType) {
        return (this.deviceSearchRecordTypes.contains(deviceSearchRecordType));
    }


    /**
     *  Adds support for a device search record type.
     *
     *  @param deviceSearchRecordType a device search record type
     *  @throws org.osid.NullArgumentException
     *  <code>deviceSearchRecordType</code> is <code>null</code>
     */

    protected void addDeviceSearchRecordType(org.osid.type.Type deviceSearchRecordType) {
        this.deviceSearchRecordTypes.add(deviceSearchRecordType);
        return;
    }


    /**
     *  Removes support for a device search record type.
     *
     *  @param deviceSearchRecordType a device search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>deviceSearchRecordType</code> is <code>null</code>
     */

    protected void removeDeviceSearchRecordType(org.osid.type.Type deviceSearchRecordType) {
        this.deviceSearchRecordTypes.remove(deviceSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> DeviceReturn </code> record types. 
     *
     *  @return a list containing the supported <code> DeviceReturn </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDeviceReturnRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.deviceReturnRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> DeviceReturn </code> record type is 
     *  supported. 
     *
     *  @param  deviceReturnRecordType a <code> Type </code> indicating a 
     *          <code> DeviceReturn </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> deviceReturnRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDeviceReturnRecordType(org.osid.type.Type deviceReturnRecordType) {
        return (this.deviceReturnRecordTypes.contains(deviceReturnRecordType));
    }


    /**
     *  Adds support for a device return record type.
     *
     *  @param deviceReturnRecordType a device return record type
     *  @throws org.osid.NullArgumentException
     *  <code>deviceReturnRecordType</code> is <code>null</code>
     */

    protected void addDeviceReturnRecordType(org.osid.type.Type deviceReturnRecordType) {
        this.deviceReturnRecordTypes.add(deviceReturnRecordType);
        return;
    }


    /**
     *  Removes support for a device return record type.
     *
     *  @param deviceReturnRecordType a device return record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>deviceReturnRecordType</code> is <code>null</code>
     */

    protected void removeDeviceReturnRecordType(org.osid.type.Type deviceReturnRecordType) {
        this.deviceReturnRecordTypes.remove(deviceReturnRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Controller </code> record types. 
     *
     *  @return a list containing the supported <code> Controller </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getControllerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.controllerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Controller </code> record type is supported. 
     *
     *  @param  controllerRecordType a <code> Type </code> indicating a <code> 
     *          Controller </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> controllerRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsControllerRecordType(org.osid.type.Type controllerRecordType) {
        return (this.controllerRecordTypes.contains(controllerRecordType));
    }


    /**
     *  Adds support for a controller record type.
     *
     *  @param controllerRecordType a controller record type
     *  @throws org.osid.NullArgumentException
     *  <code>controllerRecordType</code> is <code>null</code>
     */

    protected void addControllerRecordType(org.osid.type.Type controllerRecordType) {
        this.controllerRecordTypes.add(controllerRecordType);
        return;
    }


    /**
     *  Removes support for a controller record type.
     *
     *  @param controllerRecordType a controller record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>controllerRecordType</code> is <code>null</code>
     */

    protected void removeControllerRecordType(org.osid.type.Type controllerRecordType) {
        this.controllerRecordTypes.remove(controllerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Controller </code> search record types. 
     *
     *  @return a list containing the supported <code> Controller </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getControllerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.controllerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Controller </code> search record type is 
     *  supported. 
     *
     *  @param  controllerSearchRecordType a <code> Type </code> indicating a 
     *          <code> Controller </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          controllerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsControllerSearchRecordType(org.osid.type.Type controllerSearchRecordType) {
        return (this.controllerSearchRecordTypes.contains(controllerSearchRecordType));
    }


    /**
     *  Adds support for a controller search record type.
     *
     *  @param controllerSearchRecordType a controller search record type
     *  @throws org.osid.NullArgumentException
     *  <code>controllerSearchRecordType</code> is <code>null</code>
     */

    protected void addControllerSearchRecordType(org.osid.type.Type controllerSearchRecordType) {
        this.controllerSearchRecordTypes.add(controllerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a controller search record type.
     *
     *  @param controllerSearchRecordType a controller search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>controllerSearchRecordType</code> is <code>null</code>
     */

    protected void removeControllerSearchRecordType(org.osid.type.Type controllerSearchRecordType) {
        this.controllerSearchRecordTypes.remove(controllerSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Input </code> record types. 
     *
     *  @return a list containing the supported <code> Input </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getInputRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.inputRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Input </code> record type is supported. 
     *
     *  @param  inputRecordType a <code> Type </code> indicating an <code> 
     *          Input </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> inputRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsInputRecordType(org.osid.type.Type inputRecordType) {
        return (this.inputRecordTypes.contains(inputRecordType));
    }


    /**
     *  Adds support for an input record type.
     *
     *  @param inputRecordType an input record type
     *  @throws org.osid.NullArgumentException
     *  <code>inputRecordType</code> is <code>null</code>
     */

    protected void addInputRecordType(org.osid.type.Type inputRecordType) {
        this.inputRecordTypes.add(inputRecordType);
        return;
    }


    /**
     *  Removes support for an input record type.
     *
     *  @param inputRecordType an input record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>inputRecordType</code> is <code>null</code>
     */

    protected void removeInputRecordType(org.osid.type.Type inputRecordType) {
        this.inputRecordTypes.remove(inputRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Input </code> search types. 
     *
     *  @return a list containing the supported <code> Input </code> search 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getInputSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.inputSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Input </code> search type is supported. 
     *
     *  @param  inputSearchRecordType a <code> Type </code> indicating an 
     *          <code> Input </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> inputSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsInputSearchRecordType(org.osid.type.Type inputSearchRecordType) {
        return (this.inputSearchRecordTypes.contains(inputSearchRecordType));
    }


    /**
     *  Adds support for an input search record type.
     *
     *  @param inputSearchRecordType an input search record type
     *  @throws org.osid.NullArgumentException
     *  <code>inputSearchRecordType</code> is <code>null</code>
     */

    protected void addInputSearchRecordType(org.osid.type.Type inputSearchRecordType) {
        this.inputSearchRecordTypes.add(inputSearchRecordType);
        return;
    }


    /**
     *  Removes support for an input search record type.
     *
     *  @param inputSearchRecordType an input search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>inputSearchRecordType</code> is <code>null</code>
     */

    protected void removeInputSearchRecordType(org.osid.type.Type inputSearchRecordType) {
        this.inputSearchRecordTypes.remove(inputSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Setting </code> record types. 
     *
     *  @return a list containing the supported <code> Setting </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSettingRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.settingRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Setting </code> record type is supported. 
     *
     *  @param  settingRecordType a <code> Type </code> indicating a <code> 
     *          Setting </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> settingRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSettingRecordType(org.osid.type.Type settingRecordType) {
        return (this.settingRecordTypes.contains(settingRecordType));
    }


    /**
     *  Adds support for a setting record type.
     *
     *  @param settingRecordType a setting record type
     *  @throws org.osid.NullArgumentException
     *  <code>settingRecordType</code> is <code>null</code>
     */

    protected void addSettingRecordType(org.osid.type.Type settingRecordType) {
        this.settingRecordTypes.add(settingRecordType);
        return;
    }


    /**
     *  Removes support for a setting record type.
     *
     *  @param settingRecordType a setting record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>settingRecordType</code> is <code>null</code>
     */

    protected void removeSettingRecordType(org.osid.type.Type settingRecordType) {
        this.settingRecordTypes.remove(settingRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Setting </code> search types. 
     *
     *  @return a list containing the supported <code> Setting </code> search 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSettingSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.settingSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Setting </code> search type is supported. 
     *
     *  @param  settingSearchRecordType a <code> Type </code> indicating a 
     *          <code> Setting </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> settingSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSettingSearchRecordType(org.osid.type.Type settingSearchRecordType) {
        return (this.settingSearchRecordTypes.contains(settingSearchRecordType));
    }


    /**
     *  Adds support for a setting search record type.
     *
     *  @param settingSearchRecordType a setting search record type
     *  @throws org.osid.NullArgumentException
     *  <code>settingSearchRecordType</code> is <code>null</code>
     */

    protected void addSettingSearchRecordType(org.osid.type.Type settingSearchRecordType) {
        this.settingSearchRecordTypes.add(settingSearchRecordType);
        return;
    }


    /**
     *  Removes support for a setting search record type.
     *
     *  @param settingSearchRecordType a setting search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>settingSearchRecordType</code> is <code>null</code>
     */

    protected void removeSettingSearchRecordType(org.osid.type.Type settingSearchRecordType) {
        this.settingSearchRecordTypes.remove(settingSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Scene </code> record types. 
     *
     *  @return a list containing the supported <code> Scene </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSceneRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.sceneRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Scene </code> record type is supported. 
     *
     *  @param  sceneRecordType a <code> Type </code> indicating a <code> 
     *          Scene </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> sceneRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSceneRecordType(org.osid.type.Type sceneRecordType) {
        return (this.sceneRecordTypes.contains(sceneRecordType));
    }


    /**
     *  Adds support for a scene record type.
     *
     *  @param sceneRecordType a scene record type
     *  @throws org.osid.NullArgumentException
     *  <code>sceneRecordType</code> is <code>null</code>
     */

    protected void addSceneRecordType(org.osid.type.Type sceneRecordType) {
        this.sceneRecordTypes.add(sceneRecordType);
        return;
    }


    /**
     *  Removes support for a scene record type.
     *
     *  @param sceneRecordType a scene record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>sceneRecordType</code> is <code>null</code>
     */

    protected void removeSceneRecordType(org.osid.type.Type sceneRecordType) {
        this.sceneRecordTypes.remove(sceneRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Scene </code> search types. 
     *
     *  @return a list containing the supported <code> Scene </code> search 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSceneSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.sceneSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Scene </code> search type is supported. 
     *
     *  @param  sceneSearchRecordType a <code> Type </code> indicating a 
     *          <code> Scene </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> sceneSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSceneSearchRecordType(org.osid.type.Type sceneSearchRecordType) {
        return (this.sceneSearchRecordTypes.contains(sceneSearchRecordType));
    }


    /**
     *  Adds support for a scene search record type.
     *
     *  @param sceneSearchRecordType a scene search record type
     *  @throws org.osid.NullArgumentException
     *  <code>sceneSearchRecordType</code> is <code>null</code>
     */

    protected void addSceneSearchRecordType(org.osid.type.Type sceneSearchRecordType) {
        this.sceneSearchRecordTypes.add(sceneSearchRecordType);
        return;
    }


    /**
     *  Removes support for a scene search record type.
     *
     *  @param sceneSearchRecordType a scene search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>sceneSearchRecordType</code> is <code>null</code>
     */

    protected void removeSceneSearchRecordType(org.osid.type.Type sceneSearchRecordType) {
        this.sceneSearchRecordTypes.remove(sceneSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Trigger </code> record types. 
     *
     *  @return a list containing the supported <code> Trigger </code> types 
     */

    @OSID @Override
    public org.osid.type.TypeList getTriggerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.triggerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Trigger </code> record type is supported. 
     *
     *  @param  triggerRecordType a <code> Type </code> indicating a <code> 
     *          Trigger </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> triggerRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsTriggerRecordType(org.osid.type.Type triggerRecordType) {
        return (this.triggerRecordTypes.contains(triggerRecordType));
    }


    /**
     *  Adds support for a trigger record type.
     *
     *  @param triggerRecordType a trigger record type
     *  @throws org.osid.NullArgumentException
     *  <code>triggerRecordType</code> is <code>null</code>
     */

    protected void addTriggerRecordType(org.osid.type.Type triggerRecordType) {
        this.triggerRecordTypes.add(triggerRecordType);
        return;
    }


    /**
     *  Removes support for a trigger record type.
     *
     *  @param triggerRecordType a trigger record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>triggerRecordType</code> is <code>null</code>
     */

    protected void removeTriggerRecordType(org.osid.type.Type triggerRecordType) {
        this.triggerRecordTypes.remove(triggerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Trigger </code> search record types. 
     *
     *  @return a list containing the supported <code> Trigger </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getTriggerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.triggerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Trigger </code> search record type is 
     *  supported. 
     *
     *  @param  triggerSearchRecordType a <code> Type </code> indicating a 
     *          <code> Trigger </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> triggerSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsTriggerSearchRecordType(org.osid.type.Type triggerSearchRecordType) {
        return (this.triggerSearchRecordTypes.contains(triggerSearchRecordType));
    }


    /**
     *  Adds support for a trigger search record type.
     *
     *  @param triggerSearchRecordType a trigger search record type
     *  @throws org.osid.NullArgumentException
     *  <code>triggerSearchRecordType</code> is <code>null</code>
     */

    protected void addTriggerSearchRecordType(org.osid.type.Type triggerSearchRecordType) {
        this.triggerSearchRecordTypes.add(triggerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a trigger search record type.
     *
     *  @param triggerSearchRecordType a trigger search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>triggerSearchRecordType</code> is <code>null</code>
     */

    protected void removeTriggerSearchRecordType(org.osid.type.Type triggerSearchRecordType) {
        this.triggerSearchRecordTypes.remove(triggerSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> ActionGroup </code> record types. 
     *
     *  @return a list containing the supported <code> ActionGroup </code> 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getActionGroupRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.actionGroupRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> ActionGroup </code> record type is 
     *  supported. 
     *
     *  @param  actionGroupRecordType a <code> Type </code> indicating an 
     *          <code> ActionGroup </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> actionGroupRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsActionGroupRecordType(org.osid.type.Type actionGroupRecordType) {
        return (this.actionGroupRecordTypes.contains(actionGroupRecordType));
    }


    /**
     *  Adds support for an action group record type.
     *
     *  @param actionGroupRecordType an action group record type
     *  @throws org.osid.NullArgumentException
     *  <code>actionGroupRecordType</code> is <code>null</code>
     */

    protected void addActionGroupRecordType(org.osid.type.Type actionGroupRecordType) {
        this.actionGroupRecordTypes.add(actionGroupRecordType);
        return;
    }


    /**
     *  Removes support for an action group record type.
     *
     *  @param actionGroupRecordType an action group record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>actionGroupRecordType</code> is <code>null</code>
     */

    protected void removeActionGroupRecordType(org.osid.type.Type actionGroupRecordType) {
        this.actionGroupRecordTypes.remove(actionGroupRecordType);
        return;
    }


    /**
     *  Gets the supported <code> ActionGroup </code> search record types. 
     *
     *  @return a list containing the supported <code> ActionGroup </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getActionGroupSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.actionGroupSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> ActionGroup </code> search record type is 
     *  supported. 
     *
     *  @param  actionGroupSearchRecordType a <code> Type </code> indicating 
     *          an <code> ActionGroup </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          actionGroupSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsActionGroupSearchRecordType(org.osid.type.Type actionGroupSearchRecordType) {
        return (this.actionGroupSearchRecordTypes.contains(actionGroupSearchRecordType));
    }


    /**
     *  Adds support for an action group search record type.
     *
     *  @param actionGroupSearchRecordType an action group search record type
     *  @throws org.osid.NullArgumentException
     *  <code>actionGroupSearchRecordType</code> is <code>null</code>
     */

    protected void addActionGroupSearchRecordType(org.osid.type.Type actionGroupSearchRecordType) {
        this.actionGroupSearchRecordTypes.add(actionGroupSearchRecordType);
        return;
    }


    /**
     *  Removes support for an action group search record type.
     *
     *  @param actionGroupSearchRecordType an action group search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>actionGroupSearchRecordType</code> is <code>null</code>
     */

    protected void removeActionGroupSearchRecordType(org.osid.type.Type actionGroupSearchRecordType) {
        this.actionGroupSearchRecordTypes.remove(actionGroupSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Action </code> record types. 
     *
     *  @return a list containing the supported <code> Action </code> types 
     */

    @OSID @Override
    public org.osid.type.TypeList getActionRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.actionRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Action </code> record type is supported. 
     *
     *  @param  actionRecordType a <code> Type </code> indicating an <code> 
     *          Action </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> actionRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsActionRecordType(org.osid.type.Type actionRecordType) {
        return (this.actionRecordTypes.contains(actionRecordType));
    }


    /**
     *  Adds support for an action record type.
     *
     *  @param actionRecordType an action record type
     *  @throws org.osid.NullArgumentException
     *          <code>actionRecordType</code> is <code>null</code>
     */

    protected void addActionRecordType(org.osid.type.Type actionRecordType) {
        this.actionRecordTypes.add(actionRecordType);
        return;
    }


    /**
     *  Removes support for an action record type.
     *
     *  @param actionRecordType an action record type
     *
     *  @throws org.osid.NullArgumentException
     *          <code>actionRecordType</code> is <code>null</code>
     */

    protected void removeActionRecordType(org.osid.type.Type actionRecordType) {
        this.actionRecordTypes.remove(actionRecordType);
        return;
    }


    /**
     *  Gets the supported <code> System </code> record types. 
     *
     *  @return a list containing the supported <code> System </code> types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSystemRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.systemRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> System </code> record type is supported. 
     *
     *  @param  systemRecordType a <code> Type </code> indicating a <code> 
     *          System </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> systemRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSystemRecordType(org.osid.type.Type systemRecordType) {
        return (this.systemRecordTypes.contains(systemRecordType));
    }


    /**
     *  Adds support for a system record type.
     *
     *  @param systemRecordType a system record type
     *  @throws org.osid.NullArgumentException
     *  <code>systemRecordType</code> is <code>null</code>
     */

    protected void addSystemRecordType(org.osid.type.Type systemRecordType) {
        this.systemRecordTypes.add(systemRecordType);
        return;
    }


    /**
     *  Removes support for a system record type.
     *
     *  @param systemRecordType a system record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>systemRecordType</code> is <code>null</code>
     */

    protected void removeSystemRecordType(org.osid.type.Type systemRecordType) {
        this.systemRecordTypes.remove(systemRecordType);
        return;
    }


    /**
     *  Gets the supported <code> System </code> search record types. 
     *
     *  @return a list containing the supported <code> System </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSystemSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.systemSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> System </code> search record type is 
     *  supported. 
     *
     *  @param  systemSearchRecordType a <code> Type </code> indicating a 
     *          <code> System </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> systemSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSystemSearchRecordType(org.osid.type.Type systemSearchRecordType) {
        return (this.systemSearchRecordTypes.contains(systemSearchRecordType));
    }


    /**
     *  Adds support for a system search record type.
     *
     *  @param systemSearchRecordType a system search record type
     *  @throws org.osid.NullArgumentException
     *  <code>systemSearchRecordType</code> is <code>null</code>
     */

    protected void addSystemSearchRecordType(org.osid.type.Type systemSearchRecordType) {
        this.systemSearchRecordTypes.add(systemSearchRecordType);
        return;
    }


    /**
     *  Removes support for a system search record type.
     *
     *  @param systemSearchRecordType a system search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>systemSearchRecordType</code> is <code>null</code>
     */

    protected void removeSystemSearchRecordType(org.osid.type.Type systemSearchRecordType) {
        this.systemSearchRecordTypes.remove(systemSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device lookup 
     *  service. 
     *
     *  @return a <code> DeviceLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeviceLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceLookupSession getDeviceLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getDeviceLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DeviceLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeviceLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceLookupSession getDeviceLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getDeviceLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device lookup 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> DeviceLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeviceLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceLookupSession getDeviceLookupSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getDeviceLookupSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device lookup 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DeviceLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeviceLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceLookupSession getDeviceLookupSessionForSystem(org.osid.id.Id systemId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getDeviceLookupSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device query 
     *  service. 
     *
     *  @return a <code> DeviceQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeviceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceQuerySession getDeviceQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getDeviceQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DeviceQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeviceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceQuerySession getDeviceQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getDeviceQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device query 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> DeviceQuerySession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeviceQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceQuerySession getDeviceQuerySessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getDeviceQuerySessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device query 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DeviceQuerySession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeviceQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceQuerySession getDeviceQuerySessionForSystem(org.osid.id.Id systemId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getDeviceQuerySessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device search 
     *  service. 
     *
     *  @return a <code> DeviceSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeviceSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceSearchSession getDeviceSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getDeviceSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DeviceSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeviceSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceSearchSession getDeviceSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getDeviceSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device search 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> DeviceSearchSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeviceSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceSearchSession getDeviceSearchSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getDeviceSearchSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device search 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DeviceSearchSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeviceSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceSearchSession getDeviceSearchSessionForSystem(org.osid.id.Id systemId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getDeviceSearchSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device 
     *  administration service. 
     *
     *  @return a <code> DeviceAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeviceAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceAdminSession getDeviceAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getDeviceAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DeviceAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeviceAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceAdminSession getDeviceAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getDeviceAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device 
     *  administration service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> DeviceAdminSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeviceAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceAdminSession getDeviceAdminSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getDeviceAdminSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device 
     *  administration service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the system 
     *  @param  proxy a proxy 
     *  @return a <code> DeviceAdminSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeviceAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceAdminSession getDeviceAdminSessionForSystem(org.osid.id.Id systemId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getDeviceAdminSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device 
     *  notification service. 
     *
     *  @param  deviceReceiver the notification callback 
     *  @return a <code> DeviceNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> deviceReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceNotificationSession getDeviceNotificationSession(org.osid.control.DeviceReceiver deviceReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getDeviceNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device 
     *  notification service. 
     *
     *  @param  deviceReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> DeviceNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> deviceReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceNotificationSession getDeviceNotificationSession(org.osid.control.DeviceReceiver deviceReceiver, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getDeviceNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device 
     *  notification service for the given system. 
     *
     *  @param  deviceReceiver the notification callback 
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> DeviceNotificationSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> deviceReceiver </code> 
     *          or <code> systemId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceNotificationSession getDeviceNotificationSessionForSystem(org.osid.control.DeviceReceiver deviceReceiver, 
                                                                                            org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getDeviceNotificationSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device 
     *  notification service for the given system. 
     *
     *  @param  deviceReceiver the notification callback 
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DeviceNotificationSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> deviceReceiver, systemId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceNotificationSession getDeviceNotificationSessionForSystem(org.osid.control.DeviceReceiver deviceReceiver, 
                                                                                            org.osid.id.Id systemId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getDeviceNotificationSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup device/system mappings. 
     *
     *  @return a <code> DeviceSystemSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeviceSystem() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceSystemSession getDeviceSystemSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getDeviceSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup device/system mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DeviceSystemSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeviceSystem() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceSystemSession getDeviceSystemSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getDeviceSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning devices 
     *  to systems. 
     *
     *  @return a <code> DeviceSystemAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceSystemAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceSystemAssignmentSession getDeviceSystemAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getDeviceSystemAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning devices 
     *  to systems. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DeviceSystemAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceSystemAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceSystemAssignmentSession getDeviceSystemAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getDeviceSystemAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage device smart systems. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> DeviceSmartSystemSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceSmartSystem() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceSmartSystemSession getDeviceSmartSystemSession(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getDeviceSmartSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage device smart systems. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DeviceSmartSystemSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceSmartSystem() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceSmartSystemSession getDeviceSmartSystemSession(org.osid.id.Id systemId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getDeviceSmartSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the controller 
     *  lookup service. 
     *
     *  @return a <code> ControllerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerLookupSession getControllerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getControllerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the controller 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ControllerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerLookupSession getControllerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getControllerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the controller 
     *  lookup service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> ControllerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerLookupSession getControllerLookupSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getControllerLookupSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the controller 
     *  lookup service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ControllerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerLookupSession getControllerLookupSessionForSystem(org.osid.id.Id systemId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getControllerLookupSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the controller 
     *  query service. 
     *
     *  @return a <code> ControllerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerQuerySession getControllerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getControllerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the controller 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ControllerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerQuerySession getControllerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getControllerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the controller 
     *  query service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> ControllerQuerySession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerQuerySession getControllerQuerySessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getControllerQuerySessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the controller 
     *  query service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ControllerQuerySession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerQuerySession getControllerQuerySessionForSystem(org.osid.id.Id systemId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getControllerQuerySessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the controller 
     *  search service. 
     *
     *  @return a <code> ControllerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerSearchSession getControllerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getControllerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the controller 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ControllerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerSearchSession getControllerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getControllerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the controller 
     *  search service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> ControllerSearchSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerSearchSession getControllerSearchSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getControllerSearchSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the controller 
     *  search service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ControllerSearchSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerSearchSession getControllerSearchSessionForSystem(org.osid.id.Id systemId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getControllerSearchSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the controller 
     *  administration service. 
     *
     *  @return a <code> ControllerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerAdminSession getControllerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getControllerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the controller 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ControllerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerAdminSession getControllerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getControllerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the controller 
     *  administration service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> ControllerAdminSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerAdminSession getControllerAdminSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getControllerAdminSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the controller 
     *  administration service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the system 
     *  @param  proxy a proxy 
     *  @return a <code> ControllerAdminSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerAdminSession getControllerAdminSessionForSystem(org.osid.id.Id systemId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getControllerAdminSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the controller 
     *  notification service. 
     *
     *  @param  controllerReceiver the notification callback 
     *  @return a <code> ControllerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> controllerReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerNotificationSession getControllerNotificationSession(org.osid.control.ControllerReceiver controllerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getControllerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the controller 
     *  notification service. 
     *
     *  @param  controllerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> ControllerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> controllerReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerNotificationSession getControllerNotificationSession(org.osid.control.ControllerReceiver controllerReceiver, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getControllerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the controller 
     *  notification service for the given system. 
     *
     *  @param  controllerReceiver the notification callback 
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> ControllerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> controllerReceiver 
     *          </code> or <code> systemId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerNotificationSession getControllerNotificationSessionForSystem(org.osid.control.ControllerReceiver controllerReceiver, 
                                                                                                    org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getControllerNotificationSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the controller 
     *  notification service for the given system. 
     *
     *  @param  controllerReceiver the notification callback 
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ControllerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> controllerReceiver, 
     *          systemId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerNotificationSession getControllerNotificationSessionForSystem(org.osid.control.ControllerReceiver controllerReceiver, 
                                                                                                    org.osid.id.Id systemId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getControllerNotificationSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup controller/system 
     *  mappings. 
     *
     *  @return a <code> ControllerSystemSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerSystem() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerSystemSession getControllerSystemSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getControllerSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup controller/system 
     *  mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ControllerSystemSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerSystem() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerSystemSession getControllerSystemSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getControllerSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  controllers to systems. 
     *
     *  @return a <code> ControllerSystemAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerSystemAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerSystemAssignmentSession getControllerSystemAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getControllerSystemAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  controllers to systems. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ControllerSystemAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerSystemAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerSystemAssignmentSession getControllerSystemAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getControllerSystemAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage controller smart 
     *  systems. 
     *
     *  @param  systemId the <code> Id </code> of the system 
     *  @return a <code> ControllerSmartSystemSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerSmartSystem() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerSmartSystemSession getControllerSmartSystemSession(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getControllerSmartSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage controller smart 
     *  systems. 
     *
     *  @param  systemId the <code> Id </code> of the system 
     *  @param  proxy a proxy 
     *  @return a <code> ControllerSmartSystemSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerSmartSystem() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerSmartSystemSession getControllerSmartSystemSession(org.osid.id.Id systemId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getControllerSmartSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input lookup 
     *  service. 
     *
     *  @return an <code> InputLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInputLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.InputLookupSession getInputLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getInputLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InputLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInputLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.InputLookupSession getInputLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getInputLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input lookup 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> InputLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInputLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.InputLookupSession getInputLookupSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getInputLookupSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input lookup 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InputLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInputLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.InputLookupSession getInputLookupSessionForSystem(org.osid.id.Id systemId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getInputLookupSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input query 
     *  service. 
     *
     *  @return an <code> InputQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInputQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.InputQuerySession getInputQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getInputQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InputQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInputQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.InputQuerySession getInputQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getInputQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input query 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> InputQuerySession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInputQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.InputQuerySession getInputQuerySessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getInputQuerySessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input query 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InputQuerySession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInputQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.InputQuerySession getInputQuerySessionForSystem(org.osid.id.Id systemId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getInputQuerySessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input search 
     *  service. 
     *
     *  @return an <code> InputSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInputSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.InputSearchSession getInputSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getInputSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InputSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInputSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.InputSearchSession getInputSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getInputSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input search 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> InputSearchSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInputSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.InputSearchSession getInputSearchSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getInputSearchSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input search 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InputSearchSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInputSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.InputSearchSession getInputSearchSessionForSystem(org.osid.id.Id systemId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getInputSearchSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input 
     *  administration service. 
     *
     *  @return an <code> InputAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInputAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.InputAdminSession getInputAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getInputAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InputAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInputAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.InputAdminSession getInputAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getInputAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input 
     *  administration service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> InputAdminSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInputAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.InputAdminSession getInputAdminSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getInputAdminSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input 
     *  administration service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the system 
     *  @param  proxy a proxy 
     *  @return an <code> InputAdminSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInputAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.InputAdminSession getInputAdminSessionForSystem(org.osid.id.Id systemId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getInputAdminSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input 
     *  notification service. 
     *
     *  @param  inputReceiver the notification callback 
     *  @return an <code> InputNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> inputReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.InputNotificationSession getInputNotificationSession(org.osid.control.InputReceiver inputReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getInputNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input 
     *  notification service. 
     *
     *  @param  inputReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> InputNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> inputReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.InputNotificationSession getInputNotificationSession(org.osid.control.InputReceiver inputReceiver, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getInputNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input 
     *  notification service for the given system. 
     *
     *  @param  inputReceiver the notification callback 
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> InputNotificationSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> inputReceiver </code> or 
     *          <code> systemId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.InputNotificationSession getInputNotificationSessionForSystem(org.osid.control.InputReceiver inputReceiver, 
                                                                                          org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getInputNotificationSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input 
     *  notification service for the given system. 
     *
     *  @param  inputReceiver the notification callback 
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InputNotificationSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> inputReceiver, systemId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.InputNotificationSession getInputNotificationSessionForSystem(org.osid.control.InputReceiver inputReceiver, 
                                                                                          org.osid.id.Id systemId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getInputNotificationSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup input/system mappings. 
     *
     *  @return an <code> InputSystemSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInputSystem() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.InputSystemSession getInputSystemSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getInputSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup input/system mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InputSystemSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInputSystem() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.InputSystemSession getInputSystemSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getInputSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning inputs 
     *  to systems. 
     *
     *  @return an <code> InputSystemAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputSystemAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.InputSystemAssignmentSession getInputSystemAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getInputSystemAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning inputs 
     *  to systems. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InputSystemAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputSystemAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.InputSystemAssignmentSession getInputSystemAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getInputSystemAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage input smart systems. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> InputSmartSystemSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputSmartSystem() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.InputSmartSystemSession getInputSmartSystemSession(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getInputSmartSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage input smart systems. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InputSmartSystemSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputSmartSystem() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.InputSmartSystemSession getInputSmartSystemSession(org.osid.id.Id systemId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getInputSmartSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the setting lookup 
     *  service. 
     *
     *  @return a <code> SettingLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSettingLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingLookupSession getSettingLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getSettingLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the setting lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SettingLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSettingLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingLookupSession getSettingLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getSettingLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the setting lookup 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the system 
     *  @return a <code> SettingLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSettingLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingLookupSession getSettingLookupSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getSettingLookupSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the setting lookup 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the system 
     *  @param  proxy a proxy 
     *  @return a <code> SettingLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSettingLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingLookupSession getSettingLookupSessionForSystem(org.osid.id.Id systemId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getSettingLookupSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the setting query 
     *  service. 
     *
     *  @return a <code> SettingQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSettingQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingQuerySession getSettingQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getSettingQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the setting query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SettingQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSettingQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingQuerySession getSettingQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getSettingQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the setting query 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> SettingQuerySession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSettingQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingQuerySession getSettingQuerySessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getSettingQuerySessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the setting query 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SettingQuerySession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSettingQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingQuerySession getSettingQuerySessionForSystem(org.osid.id.Id systemId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getSettingQuerySessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the setting search 
     *  service. 
     *
     *  @return a <code> SettingSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSettingSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingSearchSession getSettingSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getSettingSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the setting search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SettingSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSettingSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingSearchSession getSettingSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getSettingSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the setting search 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> SettingSearchSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSettingSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingSearchSession getSettingSearchSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getSettingSearchSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the setting search 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SettingSearchSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSettingSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingSearchSession getSettingSearchSessionForSystem(org.osid.id.Id systemId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getSettingSearchSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the setting 
     *  administration service. 
     *
     *  @return a <code> SettingAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSettingAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingAdminSession getSettingAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getSettingAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the setting 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SettingAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSettingAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingAdminSession getSettingAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getSettingAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the setting 
     *  administration service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> Dostributor 
     *          </code> 
     *  @return a <code> SettingAdminSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSettingAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingAdminSession getSettingAdminSessionForInput(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getSettingAdminSessionForInput not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the setting 
     *  administration service for the given input. 
     *
     *  @param  inputId the <code> Id </code> of the <code> Input </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SettingAdminSession </code> 
     *  @throws org.osid.NotFoundException no input found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> inputId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSettingAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingAdminSession getSettingAdminSessionForInput(org.osid.id.Id inputId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getSettingAdminSessionForInput not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the setting 
     *  notification service. 
     *
     *  @param  settingReceiver the notification callback 
     *  @return a <code> SettingNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> settingReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSettingNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingNotificationSession getSettingNotificationSession(org.osid.control.SettingReceiver settingReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getSettingNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the setting 
     *  notification service. 
     *
     *  @param  settingReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> SettingNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> settingReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSettingNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingNotificationSession getSettingNotificationSession(org.osid.control.SettingReceiver settingReceiver, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getSettingNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the setting 
     *  notification service for the given system. 
     *
     *  @param  settingReceiver the notification callback 
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> SettingNotificationSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> settingReceiver </code> 
     *          or <code> systemId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSettingNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingNotificationSession getSettingNotificationSessionForSystem(org.osid.control.SettingReceiver settingReceiver, 
                                                                                              org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getSettingNotificationSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the setting 
     *  notification service for the given system. 
     *
     *  @param  settingReceiver the notification callback 
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SettingNotificationSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> settingReceiver, </code> 
     *          <code> systemId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSettingNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingNotificationSession getSettingNotificationSessionForSystem(org.osid.control.SettingReceiver settingReceiver, 
                                                                                              org.osid.id.Id systemId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getSettingNotificationSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup setting/system mappings. 
     *
     *  @return a <code> SettingSystemSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSettingSystem() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingSystemSession getSettingSystemSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getSettingSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup setting/system mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SettingSystemSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSettingSystem() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingSystemSession getSettingSystemSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getSettingSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning settings 
     *  to systems. 
     *
     *  @return a <code> SettingSystemAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSettingSystemAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.SettingSystemAssignmentSession getSettingSystemAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getSettingSystemAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning settings 
     *  to systems. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SettingSystemAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSettingSystemAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.SettingSystemAssignmentSession getSettingSystemAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getSettingSystemAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage locatin smart systems. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> SettingSmartSystemSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSettingSmartSystem() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingSmartSystemSession getSettingSmartSystemSession(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getSettingSmartSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage locatin smart systems. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SettingSmartSystemSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSettingSmartSystem() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingSmartSystemSession getSettingSmartSystemSession(org.osid.id.Id systemId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getSettingSmartSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the scene lookup 
     *  service. 
     *
     *  @return a <code> SceneLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSceneLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SceneLookupSession getSceneLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getSceneLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the scene lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SceneLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSceneLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SceneLookupSession getSceneLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getSceneLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the scene lookup 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the system 
     *  @return a <code> SceneLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSceneLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SceneLookupSession getSceneLookupSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getSceneLookupSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the scene lookup 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the system 
     *  @param  proxy a proxy 
     *  @return a <code> SceneLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSceneLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SceneLookupSession getSceneLookupSessionForSystem(org.osid.id.Id systemId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getSceneLookupSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the scene query 
     *  service. 
     *
     *  @return a <code> SceneQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSceneQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SceneQuerySession getSceneQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getSceneQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the scene query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SceneQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSceneQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SceneQuerySession getSceneQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getSceneQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the scene query 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> SceneQuerySession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSceneQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SceneQuerySession getSceneQuerySessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getSceneQuerySessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the scene query 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SceneQuerySession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSceneQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SceneQuerySession getSceneQuerySessionForSystem(org.osid.id.Id systemId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getSceneQuerySessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the scene search 
     *  service. 
     *
     *  @return a <code> SceneSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSceneSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SceneSearchSession getSceneSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getSceneSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the scene search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SceneSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSceneSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SceneSearchSession getSceneSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getSceneSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the scene search 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> SceneSearchSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSceneSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SceneSearchSession getSceneSearchSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getSceneSearchSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the scene search 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SceneSearchSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSceneSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SceneSearchSession getSceneSearchSessionForSystem(org.osid.id.Id systemId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getSceneSearchSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the scene 
     *  administration service. 
     *
     *  @return a <code> SceneAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSceneAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SceneAdminSession getSceneAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getSceneAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the scene 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SceneAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSceneAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SceneAdminSession getSceneAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getSceneAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the scene 
     *  administration service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> Dostributor 
     *          </code> 
     *  @return a <code> SceneAdminSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSceneAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SceneAdminSession getSceneAdminSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getSceneAdminSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the scene 
     *  administration service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SceneAdminSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSceneAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SceneAdminSession getSceneAdminSessionForSystem(org.osid.id.Id systemId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getSceneAdminSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the scene 
     *  notification service. 
     *
     *  @param  sceneReceiver the notification callback 
     *  @return a <code> SceneNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> sceneReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSceneNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SceneNotificationSession getSceneNotificationSession(org.osid.control.SceneReceiver sceneReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getSceneNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the scene 
     *  notification service. 
     *
     *  @param  sceneReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> SceneNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> sceneReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSceneNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SceneNotificationSession getSceneNotificationSession(org.osid.control.SceneReceiver sceneReceiver, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getSceneNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the scene 
     *  notification service for the given system. 
     *
     *  @param  sceneReceiver the notification callback 
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> SceneNotificationSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> sceneReceiver </code> or 
     *          <code> systemId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSceneNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SceneNotificationSession getSceneNotificationSessionForSystem(org.osid.control.SceneReceiver sceneReceiver, 
                                                                                          org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getSceneNotificationSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the scene 
     *  notification service for the given system. 
     *
     *  @param  sceneReceiver the notification callback 
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SceneNotificationSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> sceneReceiver, </code> 
     *          <code> systemId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSceneNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SceneNotificationSession getSceneNotificationSessionForSystem(org.osid.control.SceneReceiver sceneReceiver, 
                                                                                          org.osid.id.Id systemId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getSceneNotificationSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup scene/system mappings. 
     *
     *  @return a <code> SceneSystemSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSceneSystem() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SceneSystemSession getSceneSystemSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getSceneSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup scene/system mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SceneSystemSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSceneSystem() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SceneSystemSession getSceneSystemSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getSceneSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning scenes 
     *  to systems. 
     *
     *  @return a <code> SceneSystemAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSceneSystemAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.SceneSystemAssignmentSession getSceneSystemAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getSceneSystemAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning scenes 
     *  to systems. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SceneSystemAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSceneSystemAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.SceneSystemAssignmentSession getSceneSystemAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getSceneSystemAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage locatin smart systems. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> SceneSmartSystemSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSceneSmartSystem() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SceneSmartSystemSession getSceneSmartSystemSession(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getSceneSmartSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage locatin smart systems. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SceneSmartSystemSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSceneSmartSystem() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SceneSmartSystemSession getSceneSmartSystemSession(org.osid.id.Id systemId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getSceneSmartSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger lookup 
     *  service. 
     *
     *  @return a <code> TriggerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTriggerLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerLookupSession getTriggerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getTriggerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TriggerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTriggerLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerLookupSession getTriggerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getTriggerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger lookup 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> TriggerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTriggerLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerLookupSession getTriggerLookupSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getTriggerLookupSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger lookup 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TriggerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTriggerLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerLookupSession getTriggerLookupSessionForSystem(org.osid.id.Id systemId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getTriggerLookupSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger query 
     *  service. 
     *
     *  @return a <code> TriggerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTriggerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerQuerySession getTriggerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getTriggerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TriggerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTriggerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerQuerySession getTriggerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getTriggerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger query 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> TriggerQuerySession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTriggerQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerQuerySession getTriggerQuerySessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getTriggerQuerySessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger query 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TriggerQuerySession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTriggerQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerQuerySession getTriggerQuerySessionForSystem(org.osid.id.Id systemId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getTriggerQuerySessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger search 
     *  service. 
     *
     *  @return a <code> TriggerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTriggerSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerSearchSession getTriggerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getTriggerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TriggerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTriggerSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerSearchSession getTriggerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getTriggerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger search 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> TriggerSearchSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTriggerSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerSearchSession getTriggerSearchSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getTriggerSearchSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger search 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TriggerSearchSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTriggerSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerSearchSession getTriggerSearchSessionForSystem(org.osid.id.Id systemId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getTriggerSearchSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  administration service. 
     *
     *  @return a <code> TriggerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTriggerAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerAdminSession getTriggerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getTriggerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TriggerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTriggerAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerAdminSession getTriggerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getTriggerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  administration service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> TriggerAdminSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTriggerAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerAdminSession getTriggerAdminSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getTriggerAdminSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  administration service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TriggerAdminSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTriggerAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerAdminSession getTriggerAdminSessionForSystem(org.osid.id.Id systemId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getTriggerAdminSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  notification service. 
     *
     *  @param  triggerReceiver the notification callback 
     *  @return a <code> TriggerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> triggerReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerNotificationSession getTriggerNotificationSession(org.osid.control.TriggerReceiver triggerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getTriggerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  notification service. 
     *
     *  @param  triggerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> TriggerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> triggerReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerNotificationSession getTriggerNotificationSession(org.osid.control.TriggerReceiver triggerReceiver, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getTriggerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  notification service for the given system. 
     *
     *  @param  triggerReceiver the notification callback 
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> TriggerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> triggerReceiver </code> 
     *          or <code> systemId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerNotificationSession getTriggerNotificationSessionForSystem(org.osid.control.TriggerReceiver triggerReceiver, 
                                                                                              org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getTriggerNotificationSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  notification service for the given system. 
     *
     *  @param  triggerReceiver the notification callback 
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TriggerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> triggerReceiver, 
     *          systemId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerNotificationSession getTriggerNotificationSessionForSystem(org.osid.control.TriggerReceiver triggerReceiver, 
                                                                                              org.osid.id.Id systemId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getTriggerNotificationSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup trigger/system mappings. 
     *
     *  @return a <code> TriggerSystemSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTriggerSystem() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerSystemSession getTriggerSystemSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getTriggerSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup trigger/system mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TriggerSystemSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTriggerSystem() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerSystemSession getTriggerSystemSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getTriggerSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning triggers 
     *  to systems. 
     *
     *  @return a <code> TriggerSystemAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerSystemAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerSystemAssignmentSession getTriggerSystemAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getTriggerSystemAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning triggers 
     *  to systems. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TriggerSystemAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerSystemAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerSystemAssignmentSession getTriggerSystemAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getTriggerSystemAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage trigger smart systems. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> TriggerSmartSystemSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerSmartSystem() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerSmartSystemSession getTriggerSmartSystemSession(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getTriggerSmartSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage trigger smart systems. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TriggerSmartSystemSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerSmartSystem() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerSmartSystemSession getTriggerSmartSystemSession(org.osid.id.Id systemId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getTriggerSmartSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action group 
     *  lookup service. 
     *
     *  @return an <code> ActionGroupLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupLookupSession getActionGroupLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getActionGroupLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action group 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ActionGroupLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupLookupSession getActionGroupLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getActionGroupLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action group 
     *  lookup service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> ActionGroupLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupLookupSession getActionGroupLookupSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getActionGroupLookupSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action group 
     *  lookup service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ActionGroupLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupLookupSession getActionGroupLookupSessionForSystem(org.osid.id.Id systemId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getActionGroupLookupSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action group 
     *  query service. 
     *
     *  @return an <code> ActionGroupQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupQuerySession getActionGroupQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getActionGroupQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action group 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ActionGroupQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupQuerySession getActionGroupQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getActionGroupQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action group 
     *  query service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> ActionGroupQuerySession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupQuerySession getActionGroupQuerySessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getActionGroupQuerySessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action group 
     *  query service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ActionGroupQuerySession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupQuerySession getActionGroupQuerySessionForSystem(org.osid.id.Id systemId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getActionGroupQuerySessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action group 
     *  search service. 
     *
     *  @return an <code> ActionGroupSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupSearchSession getActionGroupSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getActionGroupSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action group 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ActionGroupSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupSearchSession getActionGroupSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getActionGroupSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action group 
     *  search service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> ActionGroupSearchSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupSearchSession getActionGroupSearchSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getActionGroupSearchSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action group 
     *  search service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ActionGroupSearchSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupSearchSession getActionGroupSearchSessionForSystem(org.osid.id.Id systemId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getActionGroupSearchSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action group 
     *  administration service. 
     *
     *  @return an <code> ActionGroupAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupAdminSession getActionGroupAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getActionGroupAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action group 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ActionGroupAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupAdminSession getActionGroupAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getActionGroupAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action group 
     *  administration service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> ActionGroupAdminSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupAdminSession getActionGroupAdminSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getActionGroupAdminSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action group 
     *  administration service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ActionGroupAdminSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupAdminSession getActionGroupAdminSessionForSystem(org.osid.id.Id systemId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getActionGroupAdminSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action group 
     *  notification service. 
     *
     *  @param  actionGroupReceiver the notification callback 
     *  @return an <code> ActionGroupNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> actionGroupReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupNotificationSession getActionGroupNotificationSession(org.osid.control.ActionGroupReceiver actionGroupReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getActionGroupNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action group 
     *  notification service. 
     *
     *  @param  actionGroupReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> ActionGroupNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> actionGroupReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupNotificationSession getActionGroupNotificationSession(org.osid.control.ActionGroupReceiver actionGroupReceiver, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getActionGroupNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action group 
     *  notification service for the given system. 
     *
     *  @param  actionGroupReceiver the notification callback 
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> ActionGroupNotificationSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> actionGroupReceiver 
     *          </code> or <code> systemId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupNotificationSession getActionGroupNotificationSessionForSystem(org.osid.control.ActionGroupReceiver actionGroupReceiver, 
                                                                                                      org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getActionGroupNotificationSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action group 
     *  notification service for the given system. 
     *
     *  @param  actionGroupReceiver the notification callback 
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ActionGroupNotificationSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> actionGroupReceiver, 
     *          systemId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupNotificationSession getActionGroupNotificationSessionForSystem(org.osid.control.ActionGroupReceiver actionGroupReceiver, 
                                                                                                      org.osid.id.Id systemId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getActionGroupNotificationSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup action group/system 
     *  mappings. 
     *
     *  @return an <code> ActionGroupSystemSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupSystem() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupSystemSession getActionGroupSystemSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getActionGroupSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup action group/system 
     *  mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ActionGroupSystemSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupSystem() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupSystemSession getActionGroupSystemSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getActionGroupSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning action 
     *  groups to systems. 
     *
     *  @return an <code> ActionGroupSystemAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupSystemAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupSystemAssignmentSession getActionGroupSystemAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getActionGroupSystemAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning action 
     *  groups to systems. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ActionGroupSystemAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupSystemAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupSystemAssignmentSession getActionGroupSystemAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getActionGroupSystemAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage action group smart 
     *  systems. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> ActionGroupSmartSystemSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupSmartSystem() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupSmartSystemSession getActionGroupSmartSystemSession(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getActionGroupSmartSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage action group smart 
     *  systems. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ActionGroupSmartSystemSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupSmartSystem() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupSmartSystemSession getActionGroupSmartSystemSession(org.osid.id.Id systemId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getActionGroupSmartSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the system lookup 
     *  service. 
     *
     *  @return a <code> SystemLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSystemLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SystemLookupSession getSystemLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getSystemLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the system lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SystemLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSystemLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SystemLookupSession getSystemLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getSystemLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the system query 
     *  service. 
     *
     *  @return a <code> SystemQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSystemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SystemQuerySession getSystemQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getSystemQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the system query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SystemQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSystemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SystemQuerySession getSystemQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getSystemQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the system search 
     *  service. 
     *
     *  @return a <code> SystemSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSystemSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SystemSearchSession getSystemSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getSystemSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the system search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SystemSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSystemSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SystemSearchSession getSystemSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getSystemSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the system 
     *  administrative service. 
     *
     *  @return a <code> SystemAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSystemAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SystemAdminSession getSystemAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getSystemAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the system 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SystemAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSystemAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SystemAdminSession getSystemAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getSystemAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the system 
     *  notification service. 
     *
     *  @param  systemReceiver the notification callback 
     *  @return a <code> SystemNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> systemReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSystemNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SystemNotificationSession getSystemNotificationSession(org.osid.control.SystemReceiver systemReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getSystemNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the system 
     *  notification service. 
     *
     *  @param  systemReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> SystemNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> systemReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSystemNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SystemNotificationSession getSystemNotificationSession(org.osid.control.SystemReceiver systemReceiver, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getSystemNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the system 
     *  hierarchy service. 
     *
     *  @return a <code> SystemHierarchySession </code> for systems 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSystemHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SystemHierarchySession getSystemHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getSystemHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the system 
     *  hierarchy service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SystemHierarchySession </code> for systems 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSystemHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SystemHierarchySession getSystemHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getSystemHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the system 
     *  hierarchy design service. 
     *
     *  @return a <code> HierarchyDesignSession </code> for systems 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSystemHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.SystemHierarchyDesignSession getSystemHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getSystemHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the system 
     *  hierarchy design service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HierarchyDesignSession </code> for systems 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSystemHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.SystemHierarchyDesignSession getSystemHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getSystemHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> ControlBatchManager. </code> 
     *
     *  @return a <code> ControlBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsControlBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.batch.ControlBatchManager getControlBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getControlBatchManager not implemented");
    }


    /**
     *  Gets the <code> ControlBatchProxyManager. </code> 
     *
     *  @return a <code> ControlBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsControlBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.batch.ControlBatchProxyManager getControlBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getControlBatchProxyManager not implemented");
    }


    /**
     *  Gets the <code> ControlRulesManager. </code> 
     *
     *  @return a <code> ControlRulesManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsControlRules() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ControlRulesManager getControlRulesManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlManager.getControlRulesManager not implemented");
    }


    /**
     *  Gets the <code> ControlRulesProxyManager. </code> 
     *
     *  @return a <code> ControlRulesProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsControlRules() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ControlRulesProxyManager getControlRulesProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.ControlProxyManager.getControlRulesProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.deviceRecordTypes.clear();
        this.deviceRecordTypes.clear();

        this.deviceSearchRecordTypes.clear();
        this.deviceSearchRecordTypes.clear();

        this.deviceReturnRecordTypes.clear();
        this.deviceReturnRecordTypes.clear();

        this.controllerRecordTypes.clear();
        this.controllerRecordTypes.clear();

        this.controllerSearchRecordTypes.clear();
        this.controllerSearchRecordTypes.clear();

        this.inputRecordTypes.clear();
        this.inputRecordTypes.clear();

        this.inputSearchRecordTypes.clear();
        this.inputSearchRecordTypes.clear();

        this.settingRecordTypes.clear();
        this.settingRecordTypes.clear();

        this.settingSearchRecordTypes.clear();
        this.settingSearchRecordTypes.clear();

        this.sceneRecordTypes.clear();
        this.sceneRecordTypes.clear();

        this.sceneSearchRecordTypes.clear();
        this.sceneSearchRecordTypes.clear();

        this.triggerRecordTypes.clear();
        this.triggerRecordTypes.clear();

        this.triggerSearchRecordTypes.clear();
        this.triggerSearchRecordTypes.clear();

        this.actionGroupRecordTypes.clear();
        this.actionGroupRecordTypes.clear();

        this.actionGroupSearchRecordTypes.clear();
        this.actionGroupSearchRecordTypes.clear();

        this.actionRecordTypes.clear();
        this.actionRecordTypes.clear();

        this.systemRecordTypes.clear();
        this.systemRecordTypes.clear();

        this.systemSearchRecordTypes.clear();
        this.systemSearchRecordTypes.clear();

        return;
    }
}
