//
// AbstractAssemblyProgramQuery.java
//
//     A ProgramQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.course.program.program.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ProgramQuery that stores terms.
 */

public abstract class AbstractAssemblyProgramQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOperableOsidObjectQuery
    implements org.osid.course.program.ProgramQuery,
               org.osid.course.program.ProgramQueryInspector,
               org.osid.course.program.ProgramSearchOrder {

    private final java.util.Collection<org.osid.course.program.records.ProgramQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.program.records.ProgramQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.program.records.ProgramSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyProgramQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyProgramQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Adds a title for this query. 
     *
     *  @param  title title string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> title </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> title </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchTitle(String title, org.osid.type.Type stringMatchType, 
                           boolean match) {
        getAssembler().addStringTerm(getTitleColumn(), title, stringMatchType, match);
        return;
    }


    /**
     *  Matches a title that has any value. 
     *
     *  @param  match <code> true </code> to match programs with any title, 
     *          <code> false </code> to match programs with no title 
     */

    @OSID @Override
    public void matchAnyTitle(boolean match) {
        getAssembler().addStringWildcardTerm(getTitleColumn(), match);
        return;
    }


    /**
     *  Clears the title terms. 
     */

    @OSID @Override
    public void clearTitleTerms() {
        getAssembler().clearTerms(getTitleColumn());
        return;
    }


    /**
     *  Gets the title query terms. 
     *
     *  @return the title query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getTitleTerms() {
        return (getAssembler().getStringTerms(getTitleColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by program title. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTitle(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTitleColumn(), style);
        return;
    }


    /**
     *  Gets the Title column name.
     *
     * @return the column name
     */

    protected String getTitleColumn() {
        return ("title");
    }


    /**
     *  Adds a programs number for this query. 
     *
     *  @param  number programs number string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> number </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> number </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchNumber(String number, org.osid.type.Type stringMatchType, 
                            boolean match) {
        getAssembler().addStringTerm(getNumberColumn(), number, stringMatchType, match);
        return;
    }


    /**
     *  Matches a programs number that has any value. 
     *
     *  @param  match <code> true </code> to match programs with any number, 
     *          <code> false </code> to match programs with no title 
     */

    @OSID @Override
    public void matchAnyNumber(boolean match) {
        getAssembler().addStringWildcardTerm(getNumberColumn(), match);
        return;
    }


    /**
     *  Clears the number terms. 
     */

    @OSID @Override
    public void clearNumberTerms() {
        getAssembler().clearTerms(getNumberColumn());
        return;
    }


    /**
     *  Gets the bumber query terms. 
     *
     *  @return the number query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getNumberTerms() {
        return (getAssembler().getStringTerms(getNumberColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by program number. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByNumber(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getNumberColumn(), style);
        return;
    }


    /**
     *  Gets the Number column name.
     *
     * @return the column name
     */

    protected String getNumberColumn() {
        return ("number");
    }


    /**
     *  Sets the resource <code> Id </code> for this query to match programs 
     *  that have a sponsor. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSponsorId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getSponsorIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the sponsor <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSponsorIdTerms() {
        getAssembler().clearTerms(getSponsorIdColumn());
        return;
    }


    /**
     *  Gets the sponsor <code> Id </code> query terms. 
     *
     *  @return the sponsor <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSponsorIdTerms() {
        return (getAssembler().getIdTerms(getSponsorIdColumn()));
    }


    /**
     *  Gets the SponsorId column name.
     *
     * @return the column name
     */

    protected String getSponsorIdColumn() {
        return ("sponsor_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSponsorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a sponsor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a resource query 
     *  @throws org.osid.UnimplementedException <code> supportsSponsorQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getSponsorQuery() {
        throw new org.osid.UnimplementedException("supportsSponsorQuery() is false");
    }


    /**
     *  Matches programs that have any sponsor. 
     *
     *  @param  match <code> true </code> to match programs with any sponsor, 
     *          <code> false </code> to match programs with no sponsors 
     */

    @OSID @Override
    public void matchAnySponsor(boolean match) {
        getAssembler().addIdWildcardTerm(getSponsorColumn(), match);
        return;
    }


    /**
     *  Clears the sponsor terms. 
     */

    @OSID @Override
    public void clearSponsorTerms() {
        getAssembler().clearTerms(getSponsorColumn());
        return;
    }


    /**
     *  Gets the sponsor query terms. 
     *
     *  @return the sponsor query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getSponsorTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the Sponsor column name.
     *
     * @return the column name
     */

    protected String getSponsorColumn() {
        return ("sponsor");
    }


    /**
     *  Matches programs with the prerequisites informational string. 
     *
     *  @param  requirementsInfo completion requirements string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> requirementsInfo 
     *          </code> not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> requirementsInfo </code> 
     *          or <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchCompletionRequirementsInfo(String requirementsInfo, 
                                                org.osid.type.Type stringMatchType, 
                                                boolean match) {
        getAssembler().addStringTerm(getCompletionRequirementsInfoColumn(), requirementsInfo, stringMatchType, match);
        return;
    }


    /**
     *  Matches a program that has any completion requirements information 
     *  assigned. 
     *
     *  @param  match <code> true </code> to match programs with any 
     *          completion requirements information, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public void matchAnyCompletionRequirementsInfo(boolean match) {
        getAssembler().addStringWildcardTerm(getCompletionRequirementsInfoColumn(), match);
        return;
    }


    /**
     *  Clears the completion requirements info terms. 
     */

    @OSID @Override
    public void clearCompletionRequirementsInfoTerms() {
        getAssembler().clearTerms(getCompletionRequirementsInfoColumn());
        return;
    }


    /**
     *  Gets the completion requirements query terms. 
     *
     *  @return the prereq query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getCompletionRequirementsInfoTerms() {
        return (getAssembler().getStringTerms(getCompletionRequirementsInfoColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by program 
     *  completion requirements. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCompletionRequirementsInfo(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCompletionRequirementsInfoColumn(), style);
        return;
    }


    /**
     *  Gets the CompletionRequirementsInfo column name.
     *
     * @return the column name
     */

    protected String getCompletionRequirementsInfoColumn() {
        return ("completion_requirements_info");
    }


    /**
     *  Sets the requisite <code> Id </code> for this query. 
     *
     *  @param  ruleId a rule <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> ruleId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchCompletionRequirementsId(org.osid.id.Id ruleId, 
                                              boolean match) {
        getAssembler().addIdTerm(getCompletionRequirementsIdColumn(), ruleId, match);
        return;
    }


    /**
     *  Clears the requisite <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCompletionRequirementsIdTerms() {
        getAssembler().clearTerms(getCompletionRequirementsIdColumn());
        return;
    }


    /**
     *  Gets the completion requirements requisite <code> Id </code> query 
     *  terms. 
     *
     *  @return the requisite <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCompletionRequirementsIdTerms() {
        return (getAssembler().getIdTerms(getCompletionRequirementsIdColumn()));
    }


    /**
     *  Gets the CompletionRequirementsId column name.
     *
     * @return the column name
     */

    protected String getCompletionRequirementsIdColumn() {
        return ("completion_requirements_id");
    }


    /**
     *  Tests if a <code> RequisiteQuery </code> is available. 
     *
     *  @return <code> true </code> if a requisite query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompletionRequirementsQuery() {
        return (false);
    }


    /**
     *  Gets the query for a requisite. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a requisite query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompletionRequirementsQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteQuery getCompletionRequirementsQuery() {
        throw new org.osid.UnimplementedException("supportsCompletionRequirementsQuery() is false");
    }


    /**
     *  Matches programs that have any completion requirement requisite. 
     *
     *  @param  match <code> true </code> to match programs with any 
     *          requisite, <code> false </code> to match programs with no 
     *          requisites 
     */

    @OSID @Override
    public void matchAnyCompletionRequirements(boolean match) {
        getAssembler().addIdWildcardTerm(getCompletionRequirementsColumn(), match);
        return;
    }


    /**
     *  Clears the requisite terms. 
     */

    @OSID @Override
    public void clearCompletionRequirementsTerms() {
        getAssembler().clearTerms(getCompletionRequirementsColumn());
        return;
    }


    /**
     *  Gets the completion requirements requisite query terms. 
     *
     *  @return the requisite query terms 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteQueryInspector[] getCompletionRequirementsTerms() {
        return (new org.osid.course.requisite.RequisiteQueryInspector[0]);
    }


    /**
     *  Gets the CompletionRequirements column name.
     *
     * @return the column name
     */

    protected String getCompletionRequirementsColumn() {
        return ("completion_requirements");
    }


    /**
     *  Sets the credential <code> Id </code> for this query. 
     *
     *  @param  credentialId a credential <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> credentialId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCredentialId(org.osid.id.Id credentialId, boolean match) {
        getAssembler().addIdTerm(getCredentialIdColumn(), credentialId, match);
        return;
    }


    /**
     *  Clears the credential <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCredentialIdTerms() {
        getAssembler().clearTerms(getCredentialIdColumn());
        return;
    }


    /**
     *  Gets the credential <code> Id </code> query terms. 
     *
     *  @return the grade system <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCredentialIdTerms() {
        return (getAssembler().getIdTerms(getCredentialIdColumn()));
    }


    /**
     *  Gets the CredentialId column name.
     *
     * @return the column name
     */

    protected String getCredentialIdColumn() {
        return ("credential_id");
    }


    /**
     *  Tests if a <code> CredentialQuery </code> is available. 
     *
     *  @return <code> true </code> if a credential query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialQuery() {
        return (false);
    }


    /**
     *  Gets the query for a credential. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a credential query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialQuery getCredentialQuery() {
        throw new org.osid.UnimplementedException("supportsCredentialQuery() is false");
    }


    /**
     *  Matches programs that have any credentials. 
     *
     *  @param  match <code> true </code> to match programs with any 
     *          credentials, <code> false </code> to match programs with no 
     *          credentials 
     */

    @OSID @Override
    public void matchAnyCredential(boolean match) {
        getAssembler().addIdWildcardTerm(getCredentialColumn(), match);
        return;
    }


    /**
     *  Clears the credential terms. 
     */

    @OSID @Override
    public void clearCredentialTerms() {
        getAssembler().clearTerms(getCredentialColumn());
        return;
    }


    /**
     *  Gets the credential query terms. 
     *
     *  @return the grade system terms 
     */

    @OSID @Override
    public org.osid.course.program.CredentialQueryInspector[] getCredentialTerms() {
        return (new org.osid.course.program.CredentialQueryInspector[0]);
    }


    /**
     *  Gets the Credential column name.
     *
     * @return the column name
     */

    protected String getCredentialColumn() {
        return ("credential");
    }


    /**
     *  Sets the program offering <code> Id </code> for this query to match 
     *  programs that have a related program offering. 
     *
     *  @param  programOfferingId a program offering <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> programOfferingId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchProgramOfferingId(org.osid.id.Id programOfferingId, 
                                       boolean match) {
        getAssembler().addIdTerm(getProgramOfferingIdColumn(), programOfferingId, match);
        return;
    }


    /**
     *  Clears the program offering <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearProgramOfferingIdTerms() {
        getAssembler().clearTerms(getProgramOfferingIdColumn());
        return;
    }


    /**
     *  Gets the program offering <code> Id </code> query terms. 
     *
     *  @return the program offering <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProgramOfferingIdTerms() {
        return (getAssembler().getIdTerms(getProgramOfferingIdColumn()));
    }


    /**
     *  Gets the ProgramOfferingId column name.
     *
     * @return the column name
     */

    protected String getProgramOfferingIdColumn() {
        return ("program_offering_id");
    }


    /**
     *  Tests if a <code> ProgramOfferingQuery </code> is available. 
     *
     *  @return <code> true </code> if a program offering query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramOfferingQuery() {
        return (false);
    }


    /**
     *  Gets the query for a program offering. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the program offering query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingQuery getProgramOfferingQuery() {
        throw new org.osid.UnimplementedException("supportsProgramOfferingQuery() is false");
    }


    /**
     *  Matches programs that have any program offering. 
     *
     *  @param  match <code> true </code> to match programs with any program 
     *          offering, <code> false </code> to match programs with no 
     *          program offering 
     */

    @OSID @Override
    public void matchAnyProgramOffering(boolean match) {
        getAssembler().addIdWildcardTerm(getProgramOfferingColumn(), match);
        return;
    }


    /**
     *  Clears the program offering terms. 
     */

    @OSID @Override
    public void clearProgramOfferingTerms() {
        getAssembler().clearTerms(getProgramOfferingColumn());
        return;
    }


    /**
     *  Gets the program offering query terms. 
     *
     *  @return the program offering query terms 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingQueryInspector[] getProgramOfferingTerms() {
        return (new org.osid.course.program.ProgramOfferingQueryInspector[0]);
    }


    /**
     *  Gets the ProgramOffering column name.
     *
     * @return the column name
     */

    protected String getProgramOfferingColumn() {
        return ("program_offering");
    }


    /**
     *  Sets the course catalog <code> Id </code> for this query to match 
     *  programs assigned to course catalogs. 
     *
     *  @param  courseCatalogId the course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        getAssembler().addIdTerm(getCourseCatalogIdColumn(), courseCatalogId, match);
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        getAssembler().clearTerms(getCourseCatalogIdColumn());
        return;
    }


    /**
     *  Gets the course catalog <code> Id </code> query terms. 
     *
     *  @return the course catalog <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseCatalogIdTerms() {
        return (getAssembler().getIdTerms(getCourseCatalogIdColumn()));
    }


    /**
     *  Gets the CourseCatalogId column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogIdColumn() {
        return ("course_catalog_id");
    }


    /**
     *  Tests if a <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        getAssembler().clearTerms(getCourseCatalogColumn());
        return;
    }


    /**
     *  Gets the course catalog query terms. 
     *
     *  @return the course catalog query terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }


    /**
     *  Gets the CourseCatalog column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogColumn() {
        return ("course_catalog");
    }


    /**
     *  Tests if this program supports the given record
     *  <code>Type</code>.
     *
     *  @param  programRecordType a program record type 
     *  @return <code>true</code> if the programRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>programRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type programRecordType) {
        for (org.osid.course.program.records.ProgramQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(programRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  programRecordType the program record type 
     *  @return the program query record 
     *  @throws org.osid.NullArgumentException
     *          <code>programRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(programRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.program.records.ProgramQueryRecord getProgramQueryRecord(org.osid.type.Type programRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.program.records.ProgramQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(programRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(programRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  programRecordType the program record type 
     *  @return the program query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>programRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(programRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.program.records.ProgramQueryInspectorRecord getProgramQueryInspectorRecord(org.osid.type.Type programRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.program.records.ProgramQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(programRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(programRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param programRecordType the program record type
     *  @return the program search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>programRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(programRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.program.records.ProgramSearchOrderRecord getProgramSearchOrderRecord(org.osid.type.Type programRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.program.records.ProgramSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(programRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(programRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this program. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param programQueryRecord the program query record
     *  @param programQueryInspectorRecord the program query inspector
     *         record
     *  @param programSearchOrderRecord the program search order record
     *  @param programRecordType program record type
     *  @throws org.osid.NullArgumentException
     *          <code>programQueryRecord</code>,
     *          <code>programQueryInspectorRecord</code>,
     *          <code>programSearchOrderRecord</code> or
     *          <code>programRecordTypeprogram</code> is
     *          <code>null</code>
     */
            
    protected void addProgramRecords(org.osid.course.program.records.ProgramQueryRecord programQueryRecord, 
                                      org.osid.course.program.records.ProgramQueryInspectorRecord programQueryInspectorRecord, 
                                      org.osid.course.program.records.ProgramSearchOrderRecord programSearchOrderRecord, 
                                      org.osid.type.Type programRecordType) {

        addRecordType(programRecordType);

        nullarg(programQueryRecord, "program query record");
        nullarg(programQueryInspectorRecord, "program query inspector record");
        nullarg(programSearchOrderRecord, "program search odrer record");

        this.queryRecords.add(programQueryRecord);
        this.queryInspectorRecords.add(programQueryInspectorRecord);
        this.searchOrderRecords.add(programSearchOrderRecord);
        
        return;
    }
}
