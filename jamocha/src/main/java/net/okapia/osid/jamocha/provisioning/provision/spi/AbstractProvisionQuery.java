//
// AbstractProvisionQuery.java
//
//     A template for making a Provision Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.provision.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for provisions.
 */

public abstract class AbstractProvisionQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.provisioning.ProvisionQuery {

    private final java.util.Collection<org.osid.provisioning.records.ProvisionQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the broker <code> Id </code> for this query. 
     *
     *  @param  brokerId the broker <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> brokerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBrokerId(org.osid.id.Id brokerId, boolean match) {
        return;
    }


    /**
     *  Clears the broker <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearBrokerIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BrokerQuery </code> is available. 
     *
     *  @return <code> true </code> if a broker query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a <code> Broker. </code> Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the broker query 
     *  @throws org.osid.UnimplementedException <code> supportsBrokerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerQuery getBrokerQuery() {
        throw new org.osid.UnimplementedException("supportsBrokerQuery() is false");
    }


    /**
     *  Clears the broker query terms. 
     */

    @OSID @Override
    public void clearBrokerTerms() {
        return;
    }


    /**
     *  Sets the provisionable <code> Id </code> for this query. 
     *
     *  @param  provisionableId the provisionable <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> provisionableId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchProvisionableId(org.osid.id.Id provisionableId, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the provisionable <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearProvisionableIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ProvisionableQuery </code> is available. 
     *
     *  @return <code> true </code> if a provisionable query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionableQuery() {
        return (false);
    }


    /**
     *  Gets the query for a <code> Provisionable. </code> Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the provisionable query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableQuery getProvisionableQuery() {
        throw new org.osid.UnimplementedException("supportsProvisionableQuery() is false");
    }


    /**
     *  Clears the provisionable query terms. 
     */

    @OSID @Override
    public void clearProvisionableTerms() {
        return;
    }


    /**
     *  Sets the recipient <code> Id </code> for this query. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRecipientId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the recipient <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRecipientIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> RecipientQuery </code> is available. 
     *
     *  @return <code> true </code> if a recipient query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipientQuery() {
        return (false);
    }


    /**
     *  Gets the query for a <code> Recipient. </code> Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecipientQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getRecipientQuery() {
        throw new org.osid.UnimplementedException("supportsRecipientQuery() is false");
    }


    /**
     *  Clears the recipient query terms. 
     */

    @OSID @Override
    public void clearRecipientTerms() {
        return;
    }


    /**
     *  Sets the request <code> Id </code> for this query. 
     *
     *  @param  requestId the request <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> requestId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRequestId(org.osid.id.Id requestId, boolean match) {
        return;
    }


    /**
     *  Clears the request <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRequestIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> RequestQuery </code> is available. 
     *
     *  @return <code> true </code> if a request query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequestQuery() {
        return (false);
    }


    /**
     *  Gets the query for a request. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the request query 
     *  @throws org.osid.UnimplementedException <code> supportsRequestQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestQuery getRequestQuery() {
        throw new org.osid.UnimplementedException("supportsRequestQuery() is false");
    }


    /**
     *  Matches provisions with any request. 
     *
     *  @param  match <code> true </code> to match provisions with a request, 
     *          <code> false </code> to match provisions with no requests 
     */

    @OSID @Override
    public void matchAnyRequest(boolean match) {
        return;
    }


    /**
     *  Clears the request query terms. 
     */

    @OSID @Override
    public void clearRequestTerms() {
        return;
    }


    /**
     *  Matches provisions with a provision date in the given range inclusive. 
     *
     *  @param  from the range start 
     *  @param  to the range end 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchProvisionDate(org.osid.calendaring.DateTime from, 
                                   org.osid.calendaring.DateTime to, 
                                   boolean match) {
        return;
    }


    /**
     *  Clears the provision date query terms. 
     */

    @OSID @Override
    public void clearProvisionDateTerms() {
        return;
    }


    /**
     *  Matches provisions that are leases. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchLeased(boolean match) {
        return;
    }


    /**
     *  Clears the leased query terms. 
     */

    @OSID @Override
    public void clearLeasedTerms() {
        return;
    }


    /**
     *  Matches provisions that must be returned. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchMustReturn(boolean match) {
        return;
    }


    /**
     *  Clears the must return query terms. 
     */

    @OSID @Override
    public void clearMustReturnTerms() {
        return;
    }


    /**
     *  Matches leased provisions with a due date within the given range 
     *  inclusive. 
     *
     *  @param  from the range start 
     *  @param  to the range end 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDueDate(org.osid.calendaring.DateTime from, 
                             org.osid.calendaring.DateTime to, boolean match) {
        return;
    }


    /**
     *  Matches provisions with any due date. 
     *
     *  @param  match <code> true </code> to match provisions with a due date, 
     *          <code> false </code> to match provisions with no due date 
     */

    @OSID @Override
    public void matchAnyDueDate(boolean match) {
        return;
    }


    /**
     *  Clears the due date query terms. 
     */

    @OSID @Override
    public void clearDueDateTerms() {
        return;
    }


    /**
     *  Matches leased provisions with a cost within the given range 
     *  inclusive. 
     *
     *  @param  from the range start 
     *  @param  to the range end 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     */

    @OSID @Override
    public void matchCost(org.osid.financials.Currency from, 
                          org.osid.financials.Currency to, boolean match) {
        return;
    }


    /**
     *  Matches provisions with any cost. 
     *
     *  @param  match <code> true </code> to match provisions with a cost, 
     *          <code> false </code> to match provisions with no cost 
     */

    @OSID @Override
    public void matchAnyCost(boolean match) {
        return;
    }


    /**
     *  Clears the cost query terms. 
     */

    @OSID @Override
    public void clearCostTerms() {
        return;
    }


    /**
     *  Matches leased provisions with a rate amount within the given range 
     *  inclusive. 
     *
     *  @param  from the range start 
     *  @param  to the range end 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     */

    @OSID @Override
    public void matchRateAmount(org.osid.financials.Currency from, 
                                org.osid.financials.Currency to, boolean match) {
        return;
    }


    /**
     *  Matches provisions with any rate amount. 
     *
     *  @param  match <code> true </code> to match provisions with a rate, 
     *          <code> false </code> to match provisions with no rate 
     */

    @OSID @Override
    public void matchAnyRateAmount(boolean match) {
        return;
    }


    /**
     *  Clears the rate amount query terms. 
     */

    @OSID @Override
    public void clearRateAmountTerms() {
        return;
    }


    /**
     *  Matches leased provisions with a rate period within the given range 
     *  inclusive. 
     *
     *  @param  from the range start 
     *  @param  to the range end 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchRatePeriod(org.osid.calendaring.Duration from, 
                                org.osid.calendaring.Duration to, 
                                boolean match) {
        return;
    }


    /**
     *  Matches provisions with any rate period. 
     *
     *  @param  match <code> true </code> to match provisions with a rate 
     *          period, <code> false </code> to match provisions with no rate 
     *          period 
     */

    @OSID @Override
    public void matchAnyRatePeriod(boolean match) {
        return;
    }


    /**
     *  Clears the rate period query terms. 
     */

    @OSID @Override
    public void clearRatePeriodTerms() {
        return;
    }


    /**
     *  Tests if a <code> ProvisionReturn </code> is available. 
     *
     *  @return <code> true </code> if a provision return query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionReturnQuery() {
        return (false);
    }


    /**
     *  Gets the query for a provision return. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the provision return query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionReturnQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionReturnQuery getProvisionReturnQuery() {
        throw new org.osid.UnimplementedException("supportsProvisionReturnQuery() is false");
    }


    /**
     *  Matches provisions with any provision return. 
     *
     *  @param  match <code> true </code> to match provisions with a provision 
     *          return, <code> false </code> to match provisions with no 
     *          provision return 
     */

    @OSID @Override
    public void matchAnyProvisionReturn(boolean match) {
        return;
    }


    /**
     *  Clears the provision return query terms. 
     */

    @OSID @Override
    public void clearProvisionReturnTerms() {
        return;
    }


    /**
     *  Sets the distributor <code> Id </code> for this query to match 
     *  provisions assigned to distributors. 
     *
     *  @param  distributorId the distributor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDistributorId(org.osid.id.Id distributorId, boolean match) {
        return;
    }


    /**
     *  Clears the distributor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDistributorIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> DistributorQuery </code> is available. 
     *
     *  @return <code> true </code> if a distributor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a distributor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the distributor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQuery getDistributorQuery() {
        throw new org.osid.UnimplementedException("supportsDistributorQuery() is false");
    }


    /**
     *  Clears the distributor query terms. 
     */

    @OSID @Override
    public void clearDistributorTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given provision query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a provision implementing the requested record.
     *
     *  @param provisionRecordType a provision record type
     *  @return the provision query record
     *  @throws org.osid.NullArgumentException
     *          <code>provisionRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(provisionRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.ProvisionQueryRecord getProvisionQueryRecord(org.osid.type.Type provisionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.ProvisionQueryRecord record : this.records) {
            if (record.implementsRecordType(provisionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(provisionRecordType + " is not supported");
    }


    /**
     *  Adds a record to this provision query. 
     *
     *  @param provisionQueryRecord provision query record
     *  @param provisionRecordType provision record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addProvisionQueryRecord(org.osid.provisioning.records.ProvisionQueryRecord provisionQueryRecord, 
                                          org.osid.type.Type provisionRecordType) {

        addRecordType(provisionRecordType);
        nullarg(provisionQueryRecord, "provision query record");
        this.records.add(provisionQueryRecord);        
        return;
    }
}
