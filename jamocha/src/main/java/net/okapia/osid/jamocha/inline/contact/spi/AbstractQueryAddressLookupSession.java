//
// AbstractQueryAddressLookupSession.java
//
//    An inline adapter that maps an AddressLookupSession to
//    an AddressQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.contact.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an AddressLookupSession to
 *  an AddressQuerySession.
 */

public abstract class AbstractQueryAddressLookupSession
    extends net.okapia.osid.jamocha.contact.spi.AbstractAddressLookupSession
    implements org.osid.contact.AddressLookupSession {

    private final org.osid.contact.AddressQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryAddressLookupSession.
     *
     *  @param querySession the underlying address query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryAddressLookupSession(org.osid.contact.AddressQuerySession querySession) {
        nullarg(querySession, "address query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>AddressBook</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>AddressBook Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAddressBookId() {
        return (this.session.getAddressBookId());
    }


    /**
     *  Gets the <code>AddressBook</code> associated with this 
     *  session.
     *
     *  @return the <code>AddressBook</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressBook getAddressBook()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getAddressBook());
    }


    /**
     *  Tests if this user can perform <code>Address</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAddresses() {
        return (this.session.canSearchAddresses());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include addresses in address books which are children
     *  of this address book in the address book hierarchy.
     */

    @OSID @Override
    public void useFederatedAddressBookView() {
        this.session.useFederatedAddressBookView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this address book only.
     */

    @OSID @Override
    public void useIsolatedAddressBookView() {
        this.session.useIsolatedAddressBookView();
        return;
    }
    
     
    /**
     *  Gets the <code>Address</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Address</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Address</code> and
     *  retained for compatibility.
     *
     *  @param  addressId <code>Id</code> of the
     *          <code>Address</code>
     *  @return the address
     *  @throws org.osid.NotFoundException <code>addressId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>addressId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.Address getAddress(org.osid.id.Id addressId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.contact.AddressQuery query = getQuery();
        query.matchId(addressId, true);
        org.osid.contact.AddressList addresses = this.session.getAddressesByQuery(query);
        if (addresses.hasNext()) {
            return (addresses.getNextAddress());
        } 
        
        throw new org.osid.NotFoundException(addressId + " not found");
    }


    /**
     *  Gets an <code>AddressList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  addresses specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Addresses</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  addressIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Address</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>addressIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressList getAddressesByIds(org.osid.id.IdList addressIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.contact.AddressQuery query = getQuery();

        try (org.osid.id.IdList ids = addressIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getAddressesByQuery(query));
    }


    /**
     *  Gets an <code>AddressList</code> corresponding to the given
     *  address genus <code>Type</code> which does not include
     *  addresses of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  addresses or an error results. Otherwise, the returned list
     *  may contain only those addresses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  addressGenusType an address genus type 
     *  @return the returned <code>Address</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>addressGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressList getAddressesByGenusType(org.osid.type.Type addressGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.contact.AddressQuery query = getQuery();
        query.matchGenusType(addressGenusType, true);
        return (this.session.getAddressesByQuery(query));
    }


    /**
     *  Gets an <code>AddressList</code> corresponding to the given
     *  address genus <code>Type</code> and include any additional
     *  addresses with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  addresses or an error results. Otherwise, the returned list
     *  may contain only those addresses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  addressGenusType an address genus type 
     *  @return the returned <code>Address</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>addressGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressList getAddressesByParentGenusType(org.osid.type.Type addressGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.contact.AddressQuery query = getQuery();
        query.matchParentGenusType(addressGenusType, true);
        return (this.session.getAddressesByQuery(query));
    }


    /**
     *  Gets an <code>AddressList</code> containing the given
     *  address record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  addresses or an error results. Otherwise, the returned list
     *  may contain only those addresses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  addressRecordType an address record type 
     *  @return the returned <code>Address</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>addressRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressList getAddressesByRecordType(org.osid.type.Type addressRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.contact.AddressQuery query = getQuery();
        query.matchRecordType(addressRecordType, true);
        return (this.session.getAddressesByQuery(query));
    }


    /**
     *  Gets an <code> AddressList </code> for the given
     *  resource. <code> </code> In plenary mode, the returned list
     *  contains all known addresses or an error results. Otherwise,
     *  the returned list may contain only those addresses that are
     *  accessible through this session.
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @return the returned <code> Address </code> list 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.contact.AddressList getAddressesByResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.contact.AddressQuery query = getQuery();
        query.matchResourceId(resourceId, true);
        return (this.session.getAddressesByQuery(query));
    }

    
    /**
     *  Gets all <code>Addresses</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  addresses or an error results. Otherwise, the returned list
     *  may contain only those addresses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Addresses</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressList getAddresses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {


        org.osid.contact.AddressQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getAddressesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.contact.AddressQuery getQuery() {
        org.osid.contact.AddressQuery query = this.session.getAddressQuery();
        
        return (query);
    }
}
