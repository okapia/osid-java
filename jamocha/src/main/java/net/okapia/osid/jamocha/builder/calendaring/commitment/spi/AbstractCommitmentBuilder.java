//
// AbstractCommitment.java
//
//     Defines a Commitment builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.calendaring.commitment.spi;


/**
 *  Defines a <code>Commitment</code> builder.
 */

public abstract class AbstractCommitmentBuilder<T extends AbstractCommitmentBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.calendaring.commitment.CommitmentMiter commitment;


    /**
     *  Constructs a new <code>AbstractCommitmentBuilder</code>.
     *
     *  @param commitment the commitment to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractCommitmentBuilder(net.okapia.osid.jamocha.builder.calendaring.commitment.CommitmentMiter commitment) {
        super(commitment);
        this.commitment = commitment;
        return;
    }


    /**
     *  Builds the commitment.
     *
     *  @return the new commitment
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.calendaring.Commitment build() {
        (new net.okapia.osid.jamocha.builder.validator.calendaring.commitment.CommitmentValidator(getValidations())).validate(this.commitment);
        return (new net.okapia.osid.jamocha.builder.calendaring.commitment.ImmutableCommitment(this.commitment));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the commitment miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.calendaring.commitment.CommitmentMiter getMiter() {
        return (this.commitment);
    }


    /**
     *  Sets the event.
     *
     *  @param event an event
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>event</code> is <code>null</code>
     */

    public T event(org.osid.calendaring.Event event) {
        getMiter().setEvent(event);
        return (self());
    }


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>resource</code> is <code>null</code>
     */

    public T resource(org.osid.resource.Resource resource) {
        getMiter().setResource(resource);
        return (self());
    }


    /**
     *  Adds a Commitment record.
     *
     *  @param record a commitment record
     *  @param recordType the type of commitment record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.calendaring.records.CommitmentRecord record, org.osid.type.Type recordType) {
        getMiter().addCommitmentRecord(record, recordType);
        return (self());
    }
}       


