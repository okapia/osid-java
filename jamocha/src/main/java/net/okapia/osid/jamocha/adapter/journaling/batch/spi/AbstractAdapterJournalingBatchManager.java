//
// AbstractJournalingBatchManager.java
//
//     An adapter for a JournalingBatchManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.journaling.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a JournalingBatchManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterJournalingBatchManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.journaling.batch.JournalingBatchManager>
    implements org.osid.journaling.batch.JournalingBatchManager {


    /**
     *  Constructs a new {@code AbstractAdapterJournalingBatchManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterJournalingBatchManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterJournalingBatchManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterJournalingBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of journal entries is available. 
     *
     *  @return <code> true </code> if a journal entry bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJournalEntryBatchAdmin() {
        return (getAdapteeManager().supportsJournalEntryBatchAdmin());
    }


    /**
     *  Tests if bulk administration of branches is available. 
     *
     *  @return <code> true </code> if a branch bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBranchBatchAdmin() {
        return (getAdapteeManager().supportsBranchBatchAdmin());
    }


    /**
     *  Tests if bulk administration of journals is available. 
     *
     *  @return <code> true </code> if a journal bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJournalBatchAdmin() {
        return (getAdapteeManager().supportsJournalBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk journal 
     *  entry administration service. 
     *
     *  @return a <code> JournalEntryBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalEntryBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.journaling.batch.JournalEntryBatchAdminSession getJournalEntryBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJournalEntryBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk journal 
     *  entry administration service for the given journal. 
     *
     *  @param  journalId the <code> Id </code> of the <code> Journal </code> 
     *  @return a <code> JournalEntryBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Journal </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> journalId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalEntryBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.batch.JournalEntryBatchAdminSession getJournalEntryBatchAdminSessionForJournal(org.osid.id.Id journalId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getJournalEntryBatchAdminSessionForJournal(journalId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk branch 
     *  administration service. 
     *
     *  @return a <code> BranchBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBranchBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.batch.BranchBatchAdminSession getBranchBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBranchBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk branch 
     *  administration service for the given journal. 
     *
     *  @param  journalId the <code> Id </code> of the <code> Journal </code> 
     *  @return a <code> BranchBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Journal </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> journalId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBranchBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.batch.BranchBatchAdminSession getBranchBatchAdminSessionForJournal(org.osid.id.Id journalId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBranchBatchAdminSessionForJournal(journalId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk journal 
     *  administration service. 
     *
     *  @return a <code> JournalBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.batch.JournalBatchAdminSession getJournalBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJournalBatchAdminSession());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
