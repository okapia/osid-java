//
// AbstractOrderQuery.java
//
//     A template for making an Order Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.order.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for orders.
 */

public abstract class AbstractOrderQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.ordering.OrderQuery {

    private final java.util.Collection<org.osid.ordering.records.OrderQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the item <code> Id </code> for this query to match orders by 
     *  customers. 
     *
     *  @param  customerId a customer <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> itemId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchCustomerId(org.osid.id.Id customerId, boolean match) {
        return;
    }


    /**
     *  Clears the customer <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCustomerIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CustomerQuery </code> is available. 
     *
     *  @return <code> true </code> if a customer query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCustomerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a customer query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the customer query 
     *  @throws org.osid.UnimplementedException <code> supportsCustomerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getCustomerQuery() {
        throw new org.osid.UnimplementedException("supportsCustomerQuery() is false");
    }


    /**
     *  Clears the customer terms. 
     */

    @OSID @Override
    public void clearCustomerTerms() {
        return;
    }


    /**
     *  Sets the item <code> Id </code> for this query to match orders 
     *  assigned to items. 
     *
     *  @param  itemId an item <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> itemId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchItemId(org.osid.id.Id itemId, boolean match) {
        return;
    }


    /**
     *  Clears the item <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearItemIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> ItemQuery </code> is available. 
     *
     *  @return <code> true </code> if an item query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemQuery() {
        return (false);
    }


    /**
     *  Gets the query for an item query. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the item query 
     *  @throws org.osid.UnimplementedException <code> supportsItemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ItemQuery getItemQuery() {
        throw new org.osid.UnimplementedException("supportsItemQuery() is false");
    }


    /**
     *  Matches orders with any item. 
     *
     *  @param  match <code> true </code> for a to matc h odrers with any 
     *          items, <code> false </code> to match orders with no items 
     */

    @OSID @Override
    public void matchAnyItem(boolean match) {
        return;
    }


    /**
     *  Clears the item terms. 
     */

    @OSID @Override
    public void clearItemTerms() {
        return;
    }


    /**
     *  Matches total costs between the given range inclusive. 
     *
     *  @param  low low range 
     *  @param  high high range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchTotalCost(org.osid.financials.Currency low, 
                               org.osid.financials.Currency high, 
                               boolean match) {
        return;
    }


    /**
     *  Clears the total cost terms. 
     */

    @OSID @Override
    public void clearTotalCostTerms() {
        return;
    }


    /**
     *  Matches total costs greter than or equal to the given cost. 
     *
     *  @param  cost a cost 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchMinimumTotalCost(org.osid.financials.Currency cost, 
                                      boolean match) {
        return;
    }


    /**
     *  Clears the minimum total cost terms. 
     */

    @OSID @Override
    public void clearMinimumTotalCostTerms() {
        return;
    }


    /**
     *  Matches atomic orders. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAtomic(boolean match) {
        return;
    }


    /**
     *  Clears the atomic terms. 
     */

    @OSID @Override
    public void clearAtomicTerms() {
        return;
    }


    /**
     *  Matches orders submitted between the given time range inclusive. 
     *
     *  @param  from starting time range 
     *  @param  to ending time range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> itemId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchSubmitDate(org.osid.calendaring.DateTime from, 
                                org.osid.calendaring.DateTime to, 
                                boolean match) {
        return;
    }


    /**
     *  Clears the submit date terms. 
     */

    @OSID @Override
    public void clearSubmitDateTerms() {
        return;
    }


    /**
     *  Sets a submitting resource <code> Id. </code> 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSubmitterId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the submitter <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSubmitterIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubmitterQuery() {
        return (false);
    }


    /**
     *  Gets the query for a submitting agent query. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubmitterQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getSubmitterQuery() {
        throw new org.osid.UnimplementedException("supportsSubmitterQuery() is false");
    }


    /**
     *  Matches any submitted order. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnySubmitter(boolean match) {
        return;
    }


    /**
     *  Clears the submitter terms. 
     */

    @OSID @Override
    public void clearSubmitterTerms() {
        return;
    }


    /**
     *  Sets a submitting agent <code> Id. </code> 
     *
     *  @param  agentId an agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSubmittingAgentId(org.osid.id.Id agentId, boolean match) {
        return;
    }


    /**
     *  Clears the submitter <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSubmittingAgentIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubmittingAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for a submitting agent query. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubmittingAgentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getSubmittingAgentQuery() {
        throw new org.osid.UnimplementedException("supportsSubmittingAgentQuery() is false");
    }


    /**
     *  Matches any submitted order. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnySubmittingAgent(boolean match) {
        return;
    }


    /**
     *  Clears the submitter terms. 
     */

    @OSID @Override
    public void clearSubmittingAgentTerms() {
        return;
    }


    /**
     *  Matches orders closed between the given time range inclusive. 
     *
     *  @param  from starting time range 
     *  @param  to ending time range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> itemId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchClosedDate(org.osid.calendaring.DateTime from, 
                                org.osid.calendaring.DateTime to, 
                                boolean match) {
        return;
    }


    /**
     *  Clears the closed date terms. 
     */

    @OSID @Override
    public void clearClosedDateTerms() {
        return;
    }


    /**
     *  Sets a closer resource <code> Id. </code> 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCloserId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the closer <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCloserIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCloserQuery() {
        return (false);
    }


    /**
     *  Gets the query for a closer resource query. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsCloserQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getCloserQuery() {
        throw new org.osid.UnimplementedException("supportsCloserQuery() is false");
    }


    /**
     *  Matches any closed order. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnyCloser(boolean match) {
        return;
    }


    /**
     *  Clears the closer terms. 
     */

    @OSID @Override
    public void clearCloserTerms() {
        return;
    }


    /**
     *  Sets a closing agent <code> Id. </code> 
     *
     *  @param  agentId an agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchClosingAgentId(org.osid.id.Id agentId, boolean match) {
        return;
    }


    /**
     *  Clears the closer <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearClosingAgentIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsClosingAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for a closing agent query. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsClosingAgentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getClosingAgentQuery() {
        throw new org.osid.UnimplementedException("supportsClosingAgentQuery() is false");
    }


    /**
     *  Matches any closed order. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnyClosingAgent(boolean match) {
        return;
    }


    /**
     *  Clears the closer terms. 
     */

    @OSID @Override
    public void clearClosingAgentTerms() {
        return;
    }


    /**
     *  Sets the item <code> Id </code> for this query to match orders 
     *  assigned to stores. 
     *
     *  @param  storeId a store <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchStoreId(org.osid.id.Id storeId, boolean match) {
        return;
    }


    /**
     *  Clears the store <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearStoreIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> StoreQuery </code> is available. 
     *
     *  @return <code> true </code> if a store query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStoreQuery() {
        return (false);
    }


    /**
     *  Gets the query for a store query. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the store query 
     *  @throws org.osid.UnimplementedException <code> supportsStoreQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.StoreQuery getStoreQuery() {
        throw new org.osid.UnimplementedException("supportsStoreQuery() is false");
    }


    /**
     *  Clears the store terms. 
     */

    @OSID @Override
    public void clearStoreTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given order query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an order implementing the requested record.
     *
     *  @param orderRecordType an order record type
     *  @return the order query record
     *  @throws org.osid.NullArgumentException
     *          <code>orderRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(orderRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.OrderQueryRecord getOrderQueryRecord(org.osid.type.Type orderRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.records.OrderQueryRecord record : this.records) {
            if (record.implementsRecordType(orderRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(orderRecordType + " is not supported");
    }


    /**
     *  Adds a record to this order query. 
     *
     *  @param orderQueryRecord order query record
     *  @param orderRecordType order record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addOrderQueryRecord(org.osid.ordering.records.OrderQueryRecord orderQueryRecord, 
                                          org.osid.type.Type orderRecordType) {

        addRecordType(orderRecordType);
        nullarg(orderQueryRecord, "order query record");
        this.records.add(orderQueryRecord);        
        return;
    }
}
