//
// AbstractQueryBrokerLookupSession.java
//
//    An inline adapter that maps a BrokerLookupSession to
//    a BrokerQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.provisioning.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a BrokerLookupSession to
 *  a BrokerQuerySession.
 */

public abstract class AbstractQueryBrokerLookupSession
    extends net.okapia.osid.jamocha.provisioning.spi.AbstractBrokerLookupSession
    implements org.osid.provisioning.BrokerLookupSession {

    private boolean activeonly    = false;
    private final org.osid.provisioning.BrokerQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryBrokerLookupSession.
     *
     *  @param querySession the underlying broker query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryBrokerLookupSession(org.osid.provisioning.BrokerQuerySession querySession) {
        nullarg(querySession, "broker query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Distributor</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Distributor Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.session.getDistributorId());
    }


    /**
     *  Gets the <code>Distributor</code> associated with this 
     *  session.
     *
     *  @return the <code>Distributor</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getDistributor());
    }


    /**
     *  Tests if this user can perform <code>Broker</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupBrokers() {
        return (this.session.canSearchBrokers());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include brokers in distributors which are children
     *  of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        this.session.useFederatedDistributorView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        this.session.useIsolatedDistributorView();
        return;
    }
    

    /**
     *  Only active brokers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveBrokerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive brokers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusBrokerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>Broker</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Broker</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Broker</code> and
     *  retained for compatibility.
     *
     *  In active mode, brokers are returned that are currently
     *  active. In any status mode, active and inactive brokers
     *  are returned.
     *
     *  @param  brokerId <code>Id</code> of the
     *          <code>Broker</code>
     *  @return the broker
     *  @throws org.osid.NotFoundException <code>brokerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>brokerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Broker getBroker(org.osid.id.Id brokerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.BrokerQuery query = getQuery();
        query.matchId(brokerId, true);
        org.osid.provisioning.BrokerList brokers = this.session.getBrokersByQuery(query);
        if (brokers.hasNext()) {
            return (brokers.getNextBroker());
        } 
        
        throw new org.osid.NotFoundException(brokerId + " not found");
    }


    /**
     *  Gets a <code>BrokerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  brokers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Brokers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, brokers are returned that are currently
     *  active. In any status mode, active and inactive brokers
     *  are returned.
     *
     *  @param  brokerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Broker</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>brokerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerList getBrokersByIds(org.osid.id.IdList brokerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.BrokerQuery query = getQuery();

        try (org.osid.id.IdList ids = brokerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getBrokersByQuery(query));
    }


    /**
     *  Gets a <code>BrokerList</code> corresponding to the given
     *  broker genus <code>Type</code> which does not include
     *  brokers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  brokers or an error results. Otherwise, the returned list
     *  may contain only those brokers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, brokers are returned that are currently
     *  active. In any status mode, active and inactive brokers
     *  are returned.
     *
     *  @param  brokerGenusType a broker genus type 
     *  @return the returned <code>Broker</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>brokerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerList getBrokersByGenusType(org.osid.type.Type brokerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.BrokerQuery query = getQuery();
        query.matchGenusType(brokerGenusType, true);
        return (this.session.getBrokersByQuery(query));
    }


    /**
     *  Gets a <code>BrokerList</code> corresponding to the given
     *  broker genus <code>Type</code> and include any additional
     *  brokers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  brokers or an error results. Otherwise, the returned list
     *  may contain only those brokers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, brokers are returned that are currently
     *  active. In any status mode, active and inactive brokers
     *  are returned.
     *
     *  @param  brokerGenusType a broker genus type 
     *  @return the returned <code>Broker</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>brokerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerList getBrokersByParentGenusType(org.osid.type.Type brokerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.BrokerQuery query = getQuery();
        query.matchParentGenusType(brokerGenusType, true);
        return (this.session.getBrokersByQuery(query));
    }


    /**
     *  Gets a <code>BrokerList</code> containing the given
     *  broker record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  brokers or an error results. Otherwise, the returned list
     *  may contain only those brokers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, brokers are returned that are currently
     *  active. In any status mode, active and inactive brokers
     *  are returned.
     *
     *  @param  brokerRecordType a broker record type 
     *  @return the returned <code>Broker</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>brokerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerList getBrokersByRecordType(org.osid.type.Type brokerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.BrokerQuery query = getQuery();
        query.matchRecordType(brokerRecordType, true);
        return (this.session.getBrokersByQuery(query));
    }


    /**
     *  Gets a <code>BrokerList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known brokers or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  brokers that are accessible through this session. 
     *
     *  In active mode, brokers are returned that are currently
     *  active. In any status mode, active and inactive brokers
     *  are returned.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Broker</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerList getBrokersByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.BrokerQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getBrokersByQuery(query));        
    }

    
    /**
     *  Gets all <code>Brokers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  brokers or an error results. Otherwise, the returned list
     *  may contain only those brokers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, brokers are returned that are currently
     *  active. In any status mode, active and inactive brokers
     *  are returned.
     *
     *  @return a list of <code>Brokers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerList getBrokers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.BrokerQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getBrokersByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.provisioning.BrokerQuery getQuery() {
        org.osid.provisioning.BrokerQuery query = this.session.getBrokerQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
