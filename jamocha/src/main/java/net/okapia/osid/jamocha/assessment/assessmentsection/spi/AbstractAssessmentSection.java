//
// AbstractAssessmentSection.java
//
//     Defines an AssessmentSection.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.assessmentsection.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>AssessmentSection</code>.
 */

public abstract class AbstractAssessmentSection
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.assessment.AssessmentSection {

    private org.osid.calendaring.Duration duration;
    private boolean shuffled = false;
    private boolean sequential = true;
    private final java.util.Collection<org.osid.assessment.records.AssessmentSectionRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Tests if this section must be completed within an allocated
     *  time.
     *
     *  @return <code> true </code> if this section has an allocated
     *          time, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean hasAllocatedTime() {
        return (this.duration != null);
    }


    /**
     *  Gets the allocated time for this section. 
     *
     *  @return allocated time 
     *  @throws org.osid.IllegalStateException <code>
     *          hasAllocatedTime() </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.calendaring.Duration getAllocatedTime() {
        if (!hasAllocatedTime()) {
            throw new org.osid.IllegalStateException("hasAllocatedTime() is false");
        }

        return (this.duration);
    }


    /**
     *  Sets the allocated time.
     *
     *  @param duration an allocated time
     *  @throws org.osid.NullArgumentException
     *          <code>duration</code> is <code>null</code>
     */

    protected void setAllocatedTime(org.osid.calendaring.Duration duration) {
        nullarg(duration, "allocated time");
        this.duration = duration;
        return;
    }


    /**
     *  Tests if the items or parts in this section are taken sequentially. 
     *
     *  @return <code> true </code> if the items are taken sequentially, 
     *          <code> false </code> if the items can be skipped and revisited 
     */

    @OSID @Override
    public boolean areItemsSequential() {
        return (this.sequential);
    }


    /**
     *  Sets the sequential flag.
     *
     *  @param sequential <code> true </code> if the items are taken
     *         sequentially, <code> false </code> if the items can be
     *         skipped and revisited
     */

    protected void setItemsSequential(boolean sequential) {
        this.sequential = sequential;
        return;
    }


    /**
     *  Tests if the items or parts appear in a random order.
     *
     *  @return <code> true </code> if the items appear in a random
     *          order, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean areItemsShuffled() {
        return (this.shuffled);
    }


    /**
     *  Sets the are items shuffled.
     *
     *  @param shuffled <code>true</code> if items are shuffled,
     *         <code>false</code> otherwise
     */

    protected void setItemsShuffled(boolean shuffled) {
        this.shuffled = shuffled;
        return;
    }


    /**
     *  Tests if this assessment section supports the given record
     *  <code>Type</code>.
     *
     *  @param assessmentSectionRecordType an assessment section
     *         record type
     *  @return <code>true</code> if the assessmentSectionRecordType
     *          is supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentSectionRecordType</code> is
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type assessmentSectionRecordType) {
        for (org.osid.assessment.records.AssessmentSectionRecord record : this.records) {
            if (record.implementsRecordType(assessmentSectionRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>AssessmentSection</code> record <code>Type</code>.
     *
     *  @param assessmentSectionRecordType the assessment section
     *         record type
     *  @return the assessment section record 
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentSectionRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assessmentSectionRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.AssessmentSectionRecord getAssessmentSectionRecord(org.osid.type.Type assessmentSectionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.records.AssessmentSectionRecord record : this.records) {
            if (record.implementsRecordType(assessmentSectionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assessmentSectionRecordType + " is not supported");
    }


    /**
     *  Adds a record to this assessment section. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param assessmentSectionRecord the assessment section record
     *  @param assessmentSectionRecordType assessment section record type
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentSectionRecord</code> or
     *          <code>assessmentSectionRecordType</code> is
     *          <code>null</code>
     */
            
    protected void addAssessmentSectionRecord(org.osid.assessment.records.AssessmentSectionRecord assessmentSectionRecord, 
                                              org.osid.type.Type assessmentSectionRecordType) {
        
        nullarg(assessmentSectionRecord, "assessment section record");
        addRecordType(assessmentSectionRecordType);
        this.records.add(assessmentSectionRecord);
        
        return;
    }
}
