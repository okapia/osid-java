//
// MutableMapProxyAuctionLookupSession
//
//    Implements an Auction lookup service backed by a collection of
//    auctions that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.bidding;


/**
 *  Implements an Auction lookup service backed by a collection of
 *  auctions. The auctions are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of auctions can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyAuctionLookupSession
    extends net.okapia.osid.jamocha.core.bidding.spi.AbstractMapAuctionLookupSession
    implements org.osid.bidding.AuctionLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyAuctionLookupSession}
     *  with no auctions.
     *
     *  @param auctionHouse the auction house
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code auctionHouse} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyAuctionLookupSession(org.osid.bidding.AuctionHouse auctionHouse,
                                                  org.osid.proxy.Proxy proxy) {
        setAuctionHouse(auctionHouse);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyAuctionLookupSession} with a
     *  single auction.
     *
     *  @param auctionHouse the auction house
     *  @param auction an auction
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code auctionHouse},
     *          {@code auction}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyAuctionLookupSession(org.osid.bidding.AuctionHouse auctionHouse,
                                                org.osid.bidding.Auction auction, org.osid.proxy.Proxy proxy) {
        this(auctionHouse, proxy);
        putAuction(auction);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyAuctionLookupSession} using an
     *  array of auctions.
     *
     *  @param auctionHouse the auction house
     *  @param auctions an array of auctions
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code auctionHouse},
     *          {@code auctions}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyAuctionLookupSession(org.osid.bidding.AuctionHouse auctionHouse,
                                                org.osid.bidding.Auction[] auctions, org.osid.proxy.Proxy proxy) {
        this(auctionHouse, proxy);
        putAuctions(auctions);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyAuctionLookupSession} using a
     *  collection of auctions.
     *
     *  @param auctionHouse the auction house
     *  @param auctions a collection of auctions
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code auctionHouse},
     *          {@code auctions}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyAuctionLookupSession(org.osid.bidding.AuctionHouse auctionHouse,
                                                java.util.Collection<? extends org.osid.bidding.Auction> auctions,
                                                org.osid.proxy.Proxy proxy) {
   
        this(auctionHouse, proxy);
        setSessionProxy(proxy);
        putAuctions(auctions);
        return;
    }

    
    /**
     *  Makes a {@code Auction} available in this session.
     *
     *  @param auction an auction
     *  @throws org.osid.NullArgumentException {@code auction{@code 
     *          is {@code null}
     */

    @Override
    public void putAuction(org.osid.bidding.Auction auction) {
        super.putAuction(auction);
        return;
    }


    /**
     *  Makes an array of auctions available in this session.
     *
     *  @param auctions an array of auctions
     *  @throws org.osid.NullArgumentException {@code auctions{@code 
     *          is {@code null}
     */

    @Override
    public void putAuctions(org.osid.bidding.Auction[] auctions) {
        super.putAuctions(auctions);
        return;
    }


    /**
     *  Makes collection of auctions available in this session.
     *
     *  @param auctions
     *  @throws org.osid.NullArgumentException {@code auction{@code 
     *          is {@code null}
     */

    @Override
    public void putAuctions(java.util.Collection<? extends org.osid.bidding.Auction> auctions) {
        super.putAuctions(auctions);
        return;
    }


    /**
     *  Removes a Auction from this session.
     *
     *  @param auctionId the {@code Id} of the auction
     *  @throws org.osid.NullArgumentException {@code auctionId{@code  is
     *          {@code null}
     */

    @Override
    public void removeAuction(org.osid.id.Id auctionId) {
        super.removeAuction(auctionId);
        return;
    }    
}
