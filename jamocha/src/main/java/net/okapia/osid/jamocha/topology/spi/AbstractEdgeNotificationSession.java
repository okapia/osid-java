//
// AbstractEdgeNotificationSession.java
//
//     A template for making EdgeNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.topology.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Edge} objects. This session is intended for
 *  consumers needing to synchronize their state with this service
 *  without the use of polling. Notifications are cancelled when this
 *  session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Edge} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for edge entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractEdgeNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.topology.EdgeNotificationSession {

    private boolean federated = false;
    private org.osid.topology.Graph graph = new net.okapia.osid.jamocha.nil.topology.graph.UnknownGraph();


    /**
     *  Gets the {@code Graph/code> {@code Id} associated with this
     *  session.
     *
     *  @return the {@code Graph Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getGraphId() {
        return (this.graph.getId());
    }

    
    /**
     *  Gets the {@code Graph} associated with this session.
     *
     *  @return the {@code Graph} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.Graph getGraph()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.graph);
    }


    /**
     *  Sets the {@code Graph}.
     *
     *  @param graph the graph for this session
     *  @throws org.osid.NullArgumentException {@code graph}
     *          is {@code null}
     */

    protected void setGraph(org.osid.topology.Graph graph) {
        nullarg(graph, "graph");
        this.graph = graph;
        return;
    }


    /**
     *  Tests if this user can register for {@code Edge}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForEdgeNotifications() {
        return (true);
    }


    /**
     *  Reliable notifications are desired. In reliable mode, notifications 
     *  are to be acknowledged using <code> acknowledgeEdgeNotification() 
     *  </code>. 
     */

    @OSID @Override
    public void reliableEdgeNotifications() {
        return;
    }


    /**
     *  Unreliable notifications are desired. In unreliable mode, 
     *  notifications do not need to be acknowledged. 
     */

    @OSID @Override
    public void unreliableEdgeNotifications() {
        return;
    }


    /**
     *  Acknowledge an edge notification. 
     *
     *  @param  notificationId the <code> Id </code> of the notification 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void acknowledgeEdgeNotification(org.osid.id.Id notificationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include edges in graphs which are children of this
     *  graph in the graph hierarchy.
     */

    @OSID @Override
    public void useFederatedGraphView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this graph only.
     */

    @OSID @Override
    public void useIsolatedGraphView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return {@codetrue</code> if federated view,
     *          {@codefalse</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new edges. {@code
     *  EdgeReceiver.newEdge()} is invoked when an new {@code Edge} is
     *  created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewEdges()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new edges by genus type. {@code
     *  EdgeReceiver.newEdgeConnection()} is invoked when a new {@code
     *  Edge} is connected to the specified node.
     *
     *  @param  edgeGenusType an edge genus type 
     *  @throws org.osid.NullArgumentException {@code edgeGenusType} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForNewEdgesByGenusType(org.osid.type.Type edgeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new edges for the given source
     *  node {@code Id}. {@code EdgeReceiver.newEdge()} is invoked
     *  when a new {@code Edge} is created.
     *
     *  @param  sourceNodeId the {@code Id} of the source node to monitor
     *  @throws org.osid.NullArgumentException {@code sourceNodeId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewEdgesForSourceNode(org.osid.id.Id sourceNodeId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /** 
     *  Register for notifications of new edges for the given
     *  destination node {@code Id}. {@code EdgeReceiver.newEdge()} is
     *  invoked when a new {@code Edge} is created.
     *
     *  @param  destinationNodeId the {@code Id} of the destination node to monitor
     *  @throws org.osid.NullArgumentException {@code destinationNodeId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewEdgesForDestinationNode(org.osid.id.Id destinationNodeId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated edges. {@code
     *  EdgeReceiver.changedEdge()} is invoked when an edge is
     *  changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedEdges()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated edges by genus
     *  type. {@code EdgeReceiver.changedEdgeConnection()} is invoked
     *  when an {@code Edge} in this graph is changed.
     *
     *  @param edgeGenusType an edge genus type
     *  @throws org.osid.NullArgumentException {@code edgeGenusType}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedEdgesByGenusType(org.osid.type.Type edgeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of updated edges for the given
     *  source node {@code Id}. {@code EdgeReceiver.changedEdge()} is
     *  invoked when a {@code Edge} in this graph is changed.
     *
     *  @param  sourceNodeId the {@code Id} of the source node to monitor
     *  @throws org.osid.NullArgumentException {@code sourceNodeId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedEdgesForSourceNode(org.osid.id.Id sourceNodeId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated edges for the given
     *  destination node {@code Id}. {@code
     *  EdgeReceiver.changedEdge()} is invoked when a {@code Edge} in
     *  this graph is changed.
     *
     *  @param  destinationNodeId the {@code Id} of the destination node to monitor
     *  @throws org.osid.NullArgumentException {@code destinationNodeId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedEdgesForDestinationNode(org.osid.id.Id destinationNodeId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated edge. {@code
     *  EdgeReceiver.changedEdge()} is invoked when the specified edge
     *  is changed.
     *
     *  @param edgeId the {@code Id} of the {@code Edge} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code edgeId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedEdge(org.osid.id.Id edgeId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted edges. {@code
     *  EdgeReceiver.deletedEdge()} is invoked when an edge is
     *  deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedEdges()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of deleted edges by genus
     *  type. {@code EdgeReceiver.deletedEdgeConnection()} is invoked
     *  when an {@code Edge} in this graph is removed.
     *
     *  @param  edgeGenusType an edge genus type 
     *  @throws org.osid.NullArgumentException {@code edgeGenusType}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForDeletedEdgesByGenusType(org.osid.type.Type edgeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of deleted edges for the given
     *  source node {@code Id}. {@code EdgeReceiver.deletedEdge()} is
     *  invoked when a {@code Edge} is deleted or removed from this
     *  graph.
     *
     *  @param  sourceNodeId the {@code Id} of the source node to monitor
     *  @throws org.osid.NullArgumentException {@code sourceNodeId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */
      
    @OSID @Override
    public void registerForDeletedEdgesForSourceNode(org.osid.id.Id sourceNodeId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of deleted edges for the given
     *  destination node {@code Id}. {@code
     *  EdgeReceiver.deletedEdge()} is invoked when a {@code Edge} is
     *  deleted or removed from this graph.
     *
     *  @param  destinationNodeId the {@code Id} of the destination node to monitor
     *  @throws org.osid.NullArgumentException {@code destinationNodeId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForDeletedEdgesForDestinationNode(org.osid.id.Id destinationNodeId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of a deleted edge. {@code
     *  EdgeReceiver.deletedEdge()} is invoked when the specified edge
     *  is deleted.
     *
     *  @param edgeId the {@code Id} of the
     *          {@code Edge} to monitor
     *  @throws org.osid.NullArgumentException {@code edgeId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedEdge(org.osid.id.Id edgeId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
