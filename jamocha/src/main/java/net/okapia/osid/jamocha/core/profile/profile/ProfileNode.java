//
// ProfileNode.java
//
//     Defines a Profile node within an in code hierarchy.
//
//
// Tom Coppeto
// Okapia
// 8 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.profile;


/**
 *  A class for managing a hierarchy of profile nodes in core.
 */

public final class ProfileNode
    extends net.okapia.osid.jamocha.core.profile.spi.AbstractProfileNode
    implements org.osid.profile.ProfileNode {


    /**
     *  Constructs a new <code>ProfileNode</code> from a single
     *  profile.
     *
     *  @param profile the profile
     *  @throws org.osid.NullArgumentException <code>profile</code> is 
     *          <code>null</code>.
     */

    public ProfileNode(org.osid.profile.Profile profile) {
        super(profile);
        return;
    }


    /**
     *  Constructs a new <code>ProfileNode</code>.
     *
     *  @param profile the profile
     *  @param root <code>true</code> if this node is a root, 
     *         <code>false</code> otherwise
     *  @param leaf <code>true</code> if this node is a leaf, 
     *         <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>profile</code>
     *          is <code>null</code>.
     */

    public ProfileNode(org.osid.profile.Profile profile, boolean root, boolean leaf) {
        super(profile, root, leaf);
        return;
    }


    /**
     *  Adds a parent to this profile.
     *
     *  @param node the parent to add
     *  @throws org.osid.IllegalStateException this is a root
     *  @throws org.osid.NullArgumentException <code>node</code>
     *          is <code>null</code>
     */

    @Override
    public void addParent(org.osid.profile.ProfileNode node) {
        super.addParent(node);
        return;
    }


    /**
     *  Adds a parent to this profile.
     *
     *  @param profile the profile to add as a parent
     *  @throws org.osid.NullArgumentException <code>profile</code>
     *          is <code>null</code>
     */

    public void addParent(org.osid.profile.Profile profile) {
        addParent(new ProfileNode(profile));
        return;
    }


    /**
     *  Adds a child to this profile.
     *
     *  @param node the child node to add
     *  @throws org.osid.NullArgumentException <code>node</code>
     *          is <code>null</code>
     */

    @Override
    public void addChild(org.osid.profile.ProfileNode node) {
        super.addChild(node);
        return;
    }


    /**
     *  Adds a child to this profile.
     *
     *  @param profile the profile to add as a child
     *  @throws org.osid.NullArgumentException <code>profile</code>
     *          is <code>null</code>
     */

    public void addChild(org.osid.profile.Profile profile) {
        addChild(new ProfileNode(profile));
        return;
    }
}
