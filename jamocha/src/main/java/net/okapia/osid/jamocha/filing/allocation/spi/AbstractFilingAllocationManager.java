//
// AbstractFilingAllocationManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.filing.allocation.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractFilingAllocationManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.filing.allocation.FilingAllocationManager,
               org.osid.filing.allocation.FilingAllocationProxyManager {

    private final Types allocationRecordTypes = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractFilingAllocationManager</code>.
     *
     *  @param provider the provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractFilingAllocationManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any dictionary federation is exposed. Federation is exposed 
     *  when a specific dictionary may be identified, selected and used to 
     *  create a lookup or admin session. Federation is not exposed when a set 
     *  of dictionaries appears as a single dictionary. 
     *
     *  @return <code> true </code> if federation is visible <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if filing allocation is supported. 
     *
     *  @return <code> true </code> if a <code> AllocationSession </code> is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAllocation() {
        return (false);
    }


    /**
     *  Tests if filing allocation lookup is supported. 
     *
     *  @return <code> true </code> if a <code> AllocationLookupSession 
     *          </code> is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAllocationLookup() {
        return (false);
    }


    /**
     *  Tests if quota administration is supported. 
     *
     *  @return <code> true </code> if a <code> AllocationAdminSession </code> 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAllocationAdmin() {
        return (false);
    }


    /**
     *  Tests if an allocation <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if a <code> AllocationNotificationSession 
     *          </code> is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAllocationNotification() {
        return (false);
    }


    /**
     *  Gets the supported allocation record types. 
     *
     *  @return a list containing the supported <code> Allocation </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAllocationRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.allocationRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given allocation record type is supported. 
     *
     *  @param  allocationRecordType a <code> Type </code> indicating an 
     *          allocation record type 
     *  @return <code> true </code> if the given record <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> allocationRecordType 
     *          </code> is null 
     */

    @OSID @Override
    public boolean supportsAllocationRecordType(org.osid.type.Type allocationRecordType) {
        return (this.allocationRecordTypes.contains(allocationRecordType));
    }


    /**
     *  Adds support for an allocation record type.
     *
     *  @param allocationRecordType an allocation record type
     *  @throws org.osid.NullArgumentException
     *  <code>allocationRecordType</code> is <code>null</code>
     */

    protected void addAllocationRecordType(org.osid.type.Type allocationRecordType) {
        this.allocationRecordTypes.add(allocationRecordType);
        return;
    }


    /**
     *  Removes support for an allocation record type.
     *
     *  @param allocationRecordType an allocation record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>allocationRecordType</code> is <code>null</code>
     */

    protected void removeAllocationRecordType(org.osid.type.Type allocationRecordType) {
        this.allocationRecordTypes.remove(allocationRecordType);
        return;
    }


    /**
     *  Gets the session for accessing usage and quotas. 
     *
     *  @return an <code> AllocationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAllocation() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.allocation.AllocationSession getAllocationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.allocation.FilingAllocationManager.getAllocationSession not implemented");
    }


    /**
     *  Gets the session for accessing usage and quotas. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AllocationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAllocation() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.allocation.AllocationSession getAllocationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.allocation.FilingAllocationProxyManager.getAllocationSession not implemented");
    }


    /**
     *  Gets the session for accessing usage and quotas for a given directory. 
     *  If the path is an alias, the target directory is used. The path 
     *  indicates the file alias and the real path indicates the target 
     *  directory. 
     *
     *  @param  directoryPath the pathname to the directory 
     *  @return an <code> AllocationSession </code> 
     *  @throws org.osid.InvalidArgumentException <code> directoryPath </code> 
     *          is not a directory or an alias to a directory 
     *  @throws org.osid.NotFoundException <code> directoryPath </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryPath </code> is 
     *          null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAllocation() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.allocation.AllocationSession getAllocationSessionForDirectory(String directoryPath)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.allocation.FilingAllocationManager.getAllocationSessionForDirectory not implemented");
    }


    /**
     *  Gets the session for accessing usage and quotas for a given directory. 
     *  If the path is an alias, the target directory is used. The path 
     *  indicates the file alias and the real path indicates the target 
     *  directory. 
     *
     *  @param  directoryPath the pathname to the directory 
     *  @param  proxy a proxy 
     *  @return an <code> AllocationSession </code> 
     *  @throws org.osid.InvalidArgumentException <code> directoryPath </code> 
     *          is not a directory or an alias to a directory 
     *  @throws org.osid.NotFoundException <code> directoryPath </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryPath </code> or 
     *          <code> proxy </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAllocation() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.allocation.AllocationSession getAllocationSessionForDirectory(String directoryPath, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.allocation.FilingAllocationProxyManager.getAllocationSessionForDirectory not implemented");
    }


    /**
     *  Gets the session for accessing usage and quotas. 
     *
     *  @return an <code> AllocationLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAllocationLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.allocation.AllocationLookupSession getAllocationLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.allocation.FilingAllocationManager.getAllocationLookupSession not implemented");
    }


    /**
     *  Gets the session for accessing usage and quotas. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AllocationLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAllocationLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.allocation.AllocationLookupSession getAllocationLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.allocation.FilingAllocationProxyManager.getAllocationLookupSession not implemented");
    }


    /**
     *  Gets the session for accessing usage and quotas for a given directory. 
     *  If the path is an alias, the target directory is used. The path 
     *  indicates the file alias and the real path indicates the target 
     *  directory. 
     *
     *  @param  directoryPath the pathname to the directory 
     *  @return an <code> AllocationLookupSession </code> 
     *  @throws org.osid.InvalidArgumentException <code> directoryPath </code> 
     *          is not a directory or an alias to a directory 
     *  @throws org.osid.NotFoundException <code> directoryPath </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryPath </code> is 
     *          null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAllocationLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.allocation.AllocationLookupSession getAllocationLookupSessionForDirectory(String directoryPath)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.allocation.FilingAllocationManager.getAllocationLookupSessionForDirectory not implemented");
    }


    /**
     *  Gets the session for accessing usage and quotas for a given directory. 
     *  If the path is an alias, the target directory is used. The path 
     *  indicates the file alias and the real path indicates the target 
     *  directory. 
     *
     *  @param  directoryPath the pathname to the directory 
     *  @param  proxy a proxy 
     *  @return an <code> AllocationLookupSession </code> 
     *  @throws org.osid.InvalidArgumentException <code> directoryPath </code> 
     *          is not a directory or an alias to a directory 
     *  @throws org.osid.NotFoundException <code> directoryPath </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryPath </code> or 
     *          <code> proxy </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAllocationLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.allocation.AllocationLookupSession getAllocationLookupSessionForDirectory(String directoryPath, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.allocation.FilingAllocationProxyManager.getAllocationLookupSessionForDirectory not implemented");
    }


    /**
     *  Gets the session for assigning quotas. 
     *
     *  @return an <code> AllocationAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAllocationAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.allocation.AllocationAdminSession getAllocationAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.allocation.FilingAllocationManager.getAllocationAdminSession not implemented");
    }


    /**
     *  Gets the session for assigning quotas. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AllocationAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAllocationAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.allocation.AllocationAdminSession getAllocationAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.allocation.FilingAllocationProxyManager.getAllocationAdminSession not implemented");
    }


    /**
     *  Gets the session for assigning quotas for the given directory. If the 
     *  path is an alias, the target directory is used. The path indicates the 
     *  file alias and the real path indicates the target directory. 
     *
     *  @param  directoryPath the pathname to the directory 
     *  @return an <code> AllocationAdminSession </code> 
     *  @throws org.osid.InvalidArgumentException <code> directoryPath </code> 
     *          is not a directory or an alias to a directory 
     *  @throws org.osid.NotFoundException <code> directoryPath </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryPath </code> is 
     *          null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAllocationAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.allocation.AllocationAdminSession getAllocationAdminSessionForDirectory(String directoryPath)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.allocation.FilingAllocationManager.getAllocationAdminSessionForDirectory not implemented");
    }


    /**
     *  Gets the session for assigning quotas for the given directory. If the 
     *  path is an alias, the target directory is used. The path indicates the 
     *  file alias and the real path indicates the target directory. 
     *
     *  @param  directoryPath the pathname to the directory 
     *  @param  proxy a proxy 
     *  @return an <code> AllocationAdminSession </code> 
     *  @throws org.osid.InvalidArgumentException <code> directoryPath </code> 
     *          is not a directory or an alias to a directory 
     *  @throws org.osid.NotFoundException <code> directoryPath </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryPath </code> or 
     *          <code> proxy </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAllocationAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.allocation.AllocationAdminSession getAllocationAdminSessionForDirectory(String directoryPath, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.allocation.FilingAllocationProxyManager.getAllocationAdminSessionForDirectory not implemented");
    }


    /**
     *  Gets the session for receiving messages about changes to directories. 
     *
     *  @param  allocationReceiver the notification callback 
     *  @return an <code> AllocationNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> allocationReceiver 
     *          </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAllocationNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.filing.allocation.AllocationNotificationSession getAllocationNotificationSession(org.osid.filing.allocation.AllocationReceiver allocationReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.allocation.FilingAllocationManager.getAllocationNotificationSession not implemented");
    }


    /**
     *  Gets the session for receiving messages about changes to directories. 
     *
     *  @param  allocationReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> AllocationNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> allocationReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAllocationNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.filing.allocation.AllocationNotificationSession getAllocationNotificationSession(org.osid.filing.allocation.AllocationReceiver allocationReceiver, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.allocation.FilingAllocationProxyManager.getAllocationNotificationSession not implemented");
    }


    /**
     *  Gets the session for receiving messages about usage warnings and quota 
     *  changes for the given directory. If the path is an alias, the target 
     *  directory is used. The path indicates the file alias and the real path 
     *  indicates the target directory. 
     *
     *  @param  allocationReceiver the notification callback 
     *  @param  directoryPath the pathname to the directory 
     *  @return an <code> AllocationNotificationSession </code> 
     *  @throws org.osid.InvalidArgumentException <code> directoryPath </code> 
     *          is not a directory or an alias to a directory 
     *  @throws org.osid.NotFoundException <code> directoryPath </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> allocationReceiver 
     *          </code> or <code> directoryPath </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAllocationNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.filing.allocation.AllocationNotificationSession getAllocationNotificationSessionForDirectory(org.osid.filing.allocation.AllocationReceiver allocationReceiver, 
                                                                                                                 String directoryPath)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.allocation.FilingAllocationManager.getAllocationNotificationSessionForDirectory not implemented");
    }


    /**
     *  Gets the session for receiving messages about usage warnings and quota 
     *  changes for the given directory. If the path is an alias, the target 
     *  directory is used. The path indicates the file alias and the real path 
     *  indicates the target directory. 
     *
     *  @param  allocationReceiver the notification callback 
     *  @param  directoryPath the pathname to the directory 
     *  @param  proxy a proxy 
     *  @return an <code> AllocationNotificationSession </code> 
     *  @throws org.osid.InvalidArgumentException <code> directoryPath </code> 
     *          is not a directory or an alias to a directory 
     *  @throws org.osid.NotFoundException <code> directoryPath </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> allocationReceiver, 
     *          directoryPath </code> or <code> proxy </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAllocationNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.filing.allocation.AllocationNotificationSession getAllocationNotificationSessionForDirectory(org.osid.filing.allocation.AllocationReceiver allocationReceiver, 
                                                                                                                 String directoryPath, 
                                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.filing.allocation.FilingAllocationProxyManager.getAllocationNotificationSessionForDirectory not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.allocationRecordTypes.clear();
        return;
    }
}
