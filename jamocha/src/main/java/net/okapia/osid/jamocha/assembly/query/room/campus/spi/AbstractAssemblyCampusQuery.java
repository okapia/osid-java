//
// AbstractAssemblyCampusQuery.java
//
//     A CampusQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.room.campus.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A CampusQuery that stores terms.
 */

public abstract class AbstractAssemblyCampusQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.room.CampusQuery,
               org.osid.room.CampusQueryInspector,
               org.osid.room.CampusSearchOrder {

    private final java.util.Collection<org.osid.room.records.CampusQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.room.records.CampusQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.room.records.CampusSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyCampusQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyCampusQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the room <code> Id </code> for this query to match rooms assigned 
     *  to campuses. 
     *
     *  @param  roomId a room <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> roomId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchRoomId(org.osid.id.Id roomId, boolean match) {
        getAssembler().addIdTerm(getRoomIdColumn(), roomId, match);
        return;
    }


    /**
     *  Clears the room <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRoomIdTerms() {
        getAssembler().clearTerms(getRoomIdColumn());
        return;
    }


    /**
     *  Gets the room <code> Id </code> terms. 
     *
     *  @return the room <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRoomIdTerms() {
        return (getAssembler().getIdTerms(getRoomIdColumn()));
    }


    /**
     *  Gets the RoomId column name.
     *
     * @return the column name
     */

    protected String getRoomIdColumn() {
        return ("room_id");
    }


    /**
     *  Tests if a room query is available. 
     *
     *  @return <code> true </code> if a room query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRoomQuery() {
        return (false);
    }


    /**
     *  Gets the query for a room. 
     *
     *  @return the room query 
     *  @throws org.osid.UnimplementedException <code> supportsRoomQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomQuery getRoomQuery() {
        throw new org.osid.UnimplementedException("supportsRoomQuery() is false");
    }


    /**
     *  Matches campuses with any room. 
     *
     *  @param  match <code> true </code> to match campuses with any room, 
     *          <code> false </code> to match campuses with no rooms 
     */

    @OSID @Override
    public void matchAnyRoom(boolean match) {
        getAssembler().addIdWildcardTerm(getRoomColumn(), match);
        return;
    }


    /**
     *  Clears the room terms. 
     */

    @OSID @Override
    public void clearRoomTerms() {
        getAssembler().clearTerms(getRoomColumn());
        return;
    }


    /**
     *  Gets the room terms. 
     *
     *  @return the room terms 
     */

    @OSID @Override
    public org.osid.room.RoomQueryInspector[] getRoomTerms() {
        return (new org.osid.room.RoomQueryInspector[0]);
    }


    /**
     *  Gets the Room column name.
     *
     * @return the column name
     */

    protected String getRoomColumn() {
        return ("room");
    }


    /**
     *  Sets the floor <code> Id </code> for this query to match rooms 
     *  assigned to campuses. 
     *
     *  @param  floorId a floor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> floorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchFloorId(org.osid.id.Id floorId, boolean match) {
        getAssembler().addIdTerm(getFloorIdColumn(), floorId, match);
        return;
    }


    /**
     *  Clears the floor <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearFloorIdTerms() {
        getAssembler().clearTerms(getFloorIdColumn());
        return;
    }


    /**
     *  Gets the floor <code> Id </code> terms. 
     *
     *  @return the floor <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFloorIdTerms() {
        return (getAssembler().getIdTerms(getFloorIdColumn()));
    }


    /**
     *  Gets the FloorId column name.
     *
     * @return the column name
     */

    protected String getFloorIdColumn() {
        return ("floor_id");
    }


    /**
     *  Tests if a floor query is available. 
     *
     *  @return <code> true </code> if a room query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFloorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a floor. 
     *
     *  @return the floor query 
     *  @throws org.osid.UnimplementedException <code> supportsFloorQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.FloorQuery getFloorQuery() {
        throw new org.osid.UnimplementedException("supportsFloorQuery() is false");
    }


    /**
     *  Matches campuses with any floor. 
     *
     *  @param  match <code> true </code> to match campuses with any floor, 
     *          <code> false </code> to match campuses with no floors 
     */

    @OSID @Override
    public void matchAnyFloor(boolean match) {
        getAssembler().addIdWildcardTerm(getFloorColumn(), match);
        return;
    }


    /**
     *  Clears the floor terms. 
     */

    @OSID @Override
    public void clearFloorTerms() {
        getAssembler().clearTerms(getFloorColumn());
        return;
    }


    /**
     *  Gets the floor terms. 
     *
     *  @return the floor terms 
     */

    @OSID @Override
    public org.osid.room.FloorQueryInspector[] getFloorTerms() {
        return (new org.osid.room.FloorQueryInspector[0]);
    }


    /**
     *  Gets the Floor column name.
     *
     * @return the column name
     */

    protected String getFloorColumn() {
        return ("floor");
    }


    /**
     *  Sets the building <code> Id </code> for this query to match rooms 
     *  assigned to buildings. 
     *
     *  @param  buildingId a building <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> buildingId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBuildingId(org.osid.id.Id buildingId, boolean match) {
        getAssembler().addIdTerm(getBuildingIdColumn(), buildingId, match);
        return;
    }


    /**
     *  Clears the building <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBuildingIdTerms() {
        getAssembler().clearTerms(getBuildingIdColumn());
        return;
    }


    /**
     *  Gets the building <code> Id </code> terms. 
     *
     *  @return the building <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBuildingIdTerms() {
        return (getAssembler().getIdTerms(getBuildingIdColumn()));
    }


    /**
     *  Gets the BuildingId column name.
     *
     * @return the column name
     */

    protected String getBuildingIdColumn() {
        return ("building_id");
    }


    /**
     *  Tests if a <code> BuildingQuery </code> is available. 
     *
     *  @return <code> true </code> if a building query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBuildingQuery() {
        return (false);
    }


    /**
     *  Gets the query for a building query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the building query 
     *  @throws org.osid.UnimplementedException <code> supportsBuildingQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingQuery getBuildingQuery() {
        throw new org.osid.UnimplementedException("supportsBuildingQuery() is false");
    }


    /**
     *  Matches campuses with any building. 
     *
     *  @param  match <code> true </code> to match campuses with any building, 
     *          <code> false </code> to match campuses with no buildings 
     */

    @OSID @Override
    public void matchAnyBuilding(boolean match) {
        getAssembler().addIdWildcardTerm(getBuildingColumn(), match);
        return;
    }


    /**
     *  Clears the building terms. 
     */

    @OSID @Override
    public void clearBuildingTerms() {
        getAssembler().clearTerms(getBuildingColumn());
        return;
    }


    /**
     *  Gets the building terms. 
     *
     *  @return the building terms 
     */

    @OSID @Override
    public org.osid.room.BuildingQueryInspector[] getBuildingTerms() {
        return (new org.osid.room.BuildingQueryInspector[0]);
    }


    /**
     *  Gets the Building column name.
     *
     * @return the column name
     */

    protected String getBuildingColumn() {
        return ("building");
    }


    /**
     *  Sets the campus <code> Id </code> for this query to match campuses 
     *  that have the specified campus as an ancestor. 
     *
     *  @param  campusId a campus <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorCampusId(org.osid.id.Id campusId, boolean match) {
        getAssembler().addIdTerm(getAncestorCampusIdColumn(), campusId, match);
        return;
    }


    /**
     *  Clears the ancestor campus <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorCampusIdTerms() {
        getAssembler().clearTerms(getAncestorCampusIdColumn());
        return;
    }


    /**
     *  Gets the ancestor campus <code> Id </code> terms. 
     *
     *  @return the ancestor campus <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorCampusIdTerms() {
        return (getAssembler().getIdTerms(getAncestorCampusIdColumn()));
    }


    /**
     *  Gets the AncestorCampusId column name.
     *
     * @return the column name
     */

    protected String getAncestorCampusIdColumn() {
        return ("ancestor_campus_id");
    }


    /**
     *  Tests if a <code> CampusQuery </code> is available. 
     *
     *  @return <code> true </code> if a campus query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorCampusQuery() {
        return (false);
    }


    /**
     *  Gets the query for a campus. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the campus query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorCampusQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.CampusQuery getAncestorCampusQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorCampusQuery() is false");
    }


    /**
     *  Matches campuses with any ancestor. 
     *
     *  @param  match <code> true </code> to match campuses with any ancestor, 
     *          <code> false </code> to match root campuses 
     */

    @OSID @Override
    public void matchAnyAncestorCampus(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorCampusColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor campus terms. 
     */

    @OSID @Override
    public void clearAncestorCampusTerms() {
        getAssembler().clearTerms(getAncestorCampusColumn());
        return;
    }


    /**
     *  Gets the ancestor campus terms. 
     *
     *  @return the ancestor campus terms 
     */

    @OSID @Override
    public org.osid.room.CampusQueryInspector[] getAncestorCampusTerms() {
        return (new org.osid.room.CampusQueryInspector[0]);
    }


    /**
     *  Gets the AncestorCampus column name.
     *
     * @return the column name
     */

    protected String getAncestorCampusColumn() {
        return ("ancestor_campus");
    }


    /**
     *  Sets the campus <code> Id </code> for this query to match campuses 
     *  that have the specified campus as a descendant. 
     *
     *  @param  campusId a campus <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantCampusId(org.osid.id.Id campusId, boolean match) {
        getAssembler().addIdTerm(getDescendantCampusIdColumn(), campusId, match);
        return;
    }


    /**
     *  Clears the descendant campus <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantCampusIdTerms() {
        getAssembler().clearTerms(getDescendantCampusIdColumn());
        return;
    }


    /**
     *  Gets the descendant campus <code> Id </code> terms. 
     *
     *  @return the descendant campus <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantCampusIdTerms() {
        return (getAssembler().getIdTerms(getDescendantCampusIdColumn()));
    }


    /**
     *  Gets the DescendantCampusId column name.
     *
     * @return the column name
     */

    protected String getDescendantCampusIdColumn() {
        return ("descendant_campus_id");
    }


    /**
     *  Tests if a <code> CampusQuery </code> is available. 
     *
     *  @return <code> true </code> if a campus query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantCampusQuery() {
        return (false);
    }


    /**
     *  Gets the query for a campus. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the campus query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantCampusQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.room.CampusQuery getDescendantCampusQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantCampusQuery() is false");
    }


    /**
     *  Matches campuses with any descendant. 
     *
     *  @param  match <code> true </code> to match campuses with any 
     *          descendant, <code> false </code> to match leaf campuses 
     */

    @OSID @Override
    public void matchAnyDescendantCampus(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantCampusColumn(), match);
        return;
    }


    /**
     *  Clears the descendant campus terms. 
     */

    @OSID @Override
    public void clearDescendantCampusTerms() {
        getAssembler().clearTerms(getDescendantCampusColumn());
        return;
    }


    /**
     *  Gets the descendant campus terms. 
     *
     *  @return the descendant campus terms 
     */

    @OSID @Override
    public org.osid.room.CampusQueryInspector[] getDescendantCampusTerms() {
        return (new org.osid.room.CampusQueryInspector[0]);
    }


    /**
     *  Gets the DescendantCampus column name.
     *
     * @return the column name
     */

    protected String getDescendantCampusColumn() {
        return ("descendant_campus");
    }


    /**
     *  Tests if this campus supports the given record
     *  <code>Type</code>.
     *
     *  @param  campusRecordType a campus record type 
     *  @return <code>true</code> if the campusRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>campusRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type campusRecordType) {
        for (org.osid.room.records.CampusQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(campusRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  campusRecordType the campus record type 
     *  @return the campus query record 
     *  @throws org.osid.NullArgumentException
     *          <code>campusRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(campusRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.room.records.CampusQueryRecord getCampusQueryRecord(org.osid.type.Type campusRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.records.CampusQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(campusRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(campusRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  campusRecordType the campus record type 
     *  @return the campus query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>campusRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(campusRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.room.records.CampusQueryInspectorRecord getCampusQueryInspectorRecord(org.osid.type.Type campusRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.records.CampusQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(campusRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(campusRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param campusRecordType the campus record type
     *  @return the campus search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>campusRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(campusRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.room.records.CampusSearchOrderRecord getCampusSearchOrderRecord(org.osid.type.Type campusRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.records.CampusSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(campusRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(campusRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this campus. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param campusQueryRecord the campus query record
     *  @param campusQueryInspectorRecord the campus query inspector
     *         record
     *  @param campusSearchOrderRecord the campus search order record
     *  @param campusRecordType campus record type
     *  @throws org.osid.NullArgumentException
     *          <code>campusQueryRecord</code>,
     *          <code>campusQueryInspectorRecord</code>,
     *          <code>campusSearchOrderRecord</code> or
     *          <code>campusRecordTypecampus</code> is
     *          <code>null</code>
     */
            
    protected void addCampusRecords(org.osid.room.records.CampusQueryRecord campusQueryRecord, 
                                      org.osid.room.records.CampusQueryInspectorRecord campusQueryInspectorRecord, 
                                      org.osid.room.records.CampusSearchOrderRecord campusSearchOrderRecord, 
                                      org.osid.type.Type campusRecordType) {

        addRecordType(campusRecordType);

        nullarg(campusQueryRecord, "campus query record");
        nullarg(campusQueryInspectorRecord, "campus query inspector record");
        nullarg(campusSearchOrderRecord, "campus search odrer record");

        this.queryRecords.add(campusQueryRecord);
        this.queryInspectorRecords.add(campusQueryInspectorRecord);
        this.searchOrderRecords.add(campusSearchOrderRecord);
        
        return;
    }
}
