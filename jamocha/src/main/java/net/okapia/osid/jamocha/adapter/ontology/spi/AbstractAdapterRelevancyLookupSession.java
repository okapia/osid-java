//
// AbstractAdapterRelevancyLookupSession.java
//
//    A Relevancy lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.ontology.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Relevancy lookup session adapter.
 */

public abstract class AbstractAdapterRelevancyLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.ontology.RelevancyLookupSession {

    private final org.osid.ontology.RelevancyLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterRelevancyLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterRelevancyLookupSession(org.osid.ontology.RelevancyLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Ontology/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Ontology Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOntologyId() {
        return (this.session.getOntologyId());
    }


    /**
     *  Gets the {@code Ontology} associated with this session.
     *
     *  @return the {@code Ontology} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.Ontology getOntology()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getOntology());
    }


    /**
     *  Tests if this user can perform {@code Relevancy} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupRelevancies() {
        return (this.session.canLookupRelevancies());
    }


    /**
     *  A complete view of the {@code Relevancy} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRelevancyView() {
        this.session.useComparativeRelevancyView();
        return;
    }


    /**
     *  A complete view of the {@code Relevancy} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRelevancyView() {
        this.session.usePlenaryRelevancyView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include relevancies in ontologies which are children
     *  of this ontology in the ontology hierarchy.
     */

    @OSID @Override
    public void useFederatedOntologyView() {
        this.session.useFederatedOntologyView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this ontology only.
     */

    @OSID @Override
    public void useIsolatedOntologyView() {
        this.session.useIsolatedOntologyView();
        return;
    }
    

    /**
     *  Only relevancies whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveRelevancyView() {
        this.session.useEffectiveRelevancyView();
        return;
    }
    

    /**
     *  All relevancies of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveRelevancyView() {
        this.session.useAnyEffectiveRelevancyView();
        return;
    }

     
    /**
     *  Gets the {@code Relevancy} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Relevancy} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Relevancy} and
     *  retained for compatibility.
     *
     *  In effective mode, relevancies are returned that are currently
     *  effective.  In any effective mode, effective relevancies and
     *  those currently expired are returned.
     *
     *  @param relevancyId {@code Id} of the {@code Relevancy}
     *  @return the relevancy
     *  @throws org.osid.NotFoundException {@code relevancyId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code relevancyId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.Relevancy getRelevancy(org.osid.id.Id relevancyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelevancy(relevancyId));
    }


    /**
     *  Gets a {@code RelevancyList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  relevancies specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Relevancies} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, relevancies are returned that are currently
     *  effective.  In any effective mode, effective relevancies and
     *  those currently expired are returned.
     *
     *  @param  relevancyIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Relevancy} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code relevancyIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesByIds(org.osid.id.IdList relevancyIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelevanciesByIds(relevancyIds));
    }


    /**
     *  Gets a {@code RelevancyList} corresponding to the given
     *  relevancy genus {@code Type} which does not include
     *  relevancies of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  relevancies or an error results. Otherwise, the returned list
     *  may contain only those relevancies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, relevancies are returned that are currently
     *  effective.  In any effective mode, effective relevancies and
     *  those currently expired are returned.
     *
     *  @param  relevancyGenusType a relevancy genus type 
     *  @return the returned {@code Relevancy} list
     *  @throws org.osid.NullArgumentException
     *          {@code relevancyGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesByGenusType(org.osid.type.Type relevancyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelevanciesByGenusType(relevancyGenusType));
    }


    /**
     *  Gets a {@code RelevancyList} corresponding to the given
     *  relevancy genus {@code Type} and include any additional
     *  relevancies with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  relevancies or an error results. Otherwise, the returned list
     *  may contain only those relevancies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, relevancies are returned that are currently
     *  effective.  In any effective mode, effective relevancies and
     *  those currently expired are returned.
     *
     *  @param  relevancyGenusType a relevancy genus type 
     *  @return the returned {@code Relevancy} list
     *  @throws org.osid.NullArgumentException
     *          {@code relevancyGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesByParentGenusType(org.osid.type.Type relevancyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelevanciesByParentGenusType(relevancyGenusType));
    }


    /**
     *  Gets a {@code RelevancyList} containing the given
     *  relevancy record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  relevancies or an error results. Otherwise, the returned list
     *  may contain only those relevancies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, relevancies are returned that are currently
     *  effective.  In any effective mode, effective relevancies and
     *  those currently expired are returned.
     *
     *  @param  relevancyRecordType a relevancy record type 
     *  @return the returned {@code Relevancy} list
     *  @throws org.osid.NullArgumentException
     *          {@code relevancyRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesByRecordType(org.osid.type.Type relevancyRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelevanciesByRecordType(relevancyRecordType));
    }


    /**
     *  Gets a {@code RelevancyList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  relevancies or an error results. Otherwise, the returned list
     *  may contain only those relevancies that are accessible
     *  through this session.
     *  
     *  In active mode, relevancies are returned that are currently
     *  active. In any status mode, active and inactive relevancies
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Relevancy} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesOnDate(org.osid.calendaring.DateTime from, 
                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelevanciesOnDate(from, to));
    }


    /**
     *  Gets a {@code RelevancyList} by genus type and effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  relevancies or an error results. Otherwise, the returned list
     *  may contain only those relevancies that are accessible
     *  through this session.
     *  
     *  In active mode, relevancies are returned that are currently
     *  active. In any status mode, active and inactive relevancies
     *  are returned.
     *
     *  @param relevancyGenusType a relevancy genus type
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Relevancy} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code
     *          relevancyGenusType}, {@code from}, or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesByGenusTypeOnDate(org.osid.type.Type relevancyGenusType,
                                                                           org.osid.calendaring.DateTime from, 
                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelevanciesByGenusTypeOnDate(relevancyGenusType, from, to));
    }
        

    /**
     *  Gets a list of relevancies corresponding to a subject
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  relevancies or an error results. Otherwise, the returned list
     *  may contain only those relevancies that are accessible through
     *  this session.
     *
     *  In effective mode, relevancies are returned that are
     *  currently effective.  In any effective mode, effective
     *  relevancies and those currently expired are returned.
     *
     *  @param  subjectId the {@code Id} of the subject
     *  @return the returned {@code RelevancyList}
     *  @throws org.osid.NullArgumentException {@code subjectId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesForSubject(org.osid.id.Id subjectId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelevanciesForSubject(subjectId));
    }


    /**
     *  Gets a list of relevancies corresponding to a subject
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  relevancies or an error results. Otherwise, the returned list
     *  may contain only those relevancies that are accessible
     *  through this session.
     *
     *  In effective mode, relevancies are returned that are
     *  currently effective.  In any effective mode, effective
     *  relevancies and those currently expired are returned.
     *
     *  @param  subjectId the {@code Id} of the subject
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code RelevancyList}
     *  @throws org.osid.NullArgumentException {@code subjectId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesForSubjectOnDate(org.osid.id.Id subjectId,
                                                                          org.osid.calendaring.DateTime from,
                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelevanciesForSubjectOnDate(subjectId, from, to));
    }


    /**
     *  Gets the {@code Relevancy} mapped to a subject {@code Id} and
     *  relevancy {@code genus Type.} Genus {@code Types} derived from
     *  the given genus {@code Typ} e are included.
     *  
     *  In plenary mode, the exact {@code Id} is found or a {@code
     *  NOT_FOUND} results. Otherwise, the returned {@code Relevancy}
     *  may have a different {@code Id} than requested, such as the
     *  case where a duplicate {@code Id} was assigned to a {@code
     *  Relevancy} and retained for compatibility.
     *  
     *  In effective mode, relevancies are returned that are currently
     *  effective. In any effective mode, effective relevancies and
     *  those currently expired are returned.
     *
     *  @param  subjectId the subject {@code Id} 
     *  @param  relevancyGenusType relevancy genus type 
     *  @return list of relevancies 
     *  @throws org.osid.NullArgumentException {@code subjectId} or 
     *          {@code relevancyGenusType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesByGenusTypeForSubject(org.osid.id.Id subjectId, 
                                                                               org.osid.type.Type relevancyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelevanciesByGenusTypeForSubject(subjectId, relevancyGenusType));
    }


    /**
     *  Gets a {@code RelevancyList} of the given genus type for the
     *  given subject effective during the entire given date range
     *  inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  relevancies or an error results. Otherwise, the returned list
     *  may contain only those relevancies that are accessible through
     *  this session.
     *  
     *  In effective mode, relevancies are returned that are currently
     *  effective. In any effective mode, effective relevancies and
     *  those currently expired are returned.
     *
     *  @param  subjectId the subject {@code Id} 
     *  @param  relevancyGenusType relevancy genus type 
     *  @param  from a starting date 
     *  @param  to an ending date 
     *  @return list of relevancies 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code subjectId, 
     *          relevancyGenusType, from} or {@code to} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesByGenusTypeForSubjectOnDate(org.osid.id.Id subjectId, 
                                                                                     org.osid.type.Type relevancyGenusType, 
                                                                                     org.osid.calendaring.DateTime from, 
                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelevanciesByGenusTypeForSubjectOnDate(subjectId, relevancyGenusType, from, to));
    }
    
    
    /**
     *  Gets the relevancies for the given subject {@code Ids}. 
     *  
     *  In plenary mode, the returned list contains all of the
     *  relevancies specified in the subject {@code Id} list, in the
     *  order of the list, including duplicates, or an error results
     *  if a relevancy {@code Id} in the supplied list is not found or
     *  inaccessible.  Otherwise, inaccessible relevancies may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *  
     *  In effective mode, relevancies are returned that are currently 
     *  effective. In any effective mode, effective relevancies and those 
     *  currently expired are returned. 
     *
     *  @param  subjectIds list of subject {@code Ids} 
     *  @return list of relevancies 
     *  @throws org.osid.NullArgumentException {@code subjectIds} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesForSubjects(org.osid.id.IdList subjectIds)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelevanciesForSubjects(subjectIds));
    }


    /**
     *  Gets a list of relevancies corresponding to a reference
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  relevancies or an error results. Otherwise, the returned list
     *  may contain only those relevancies that are accessible
     *  through this session.
     *
     *  In effective mode, relevancies are returned that are
     *  currently effective.  In any effective mode, effective
     *  relevancies and those currently expired are returned.
     *
     *  @param  referenceId the {@code Id} of the reference
     *  @return the returned {@code RelevancyList}
     *  @throws org.osid.NullArgumentException {@code referenceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesForMappedId(org.osid.id.Id referenceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelevanciesForMappedId(referenceId));
    }


    /**
     *  Gets a list of relevancies corresponding to a reference
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  relevancies or an error results. Otherwise, the returned list
     *  may contain only those relevancies that are accessible
     *  through this session.
     *
     *  In effective mode, relevancies are returned that are
     *  currently effective.  In any effective mode, effective
     *  relevancies and those currently expired are returned.
     *
     *  @param  referenceId the {@code Id} of the reference
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code RelevancyList}
     *  @throws org.osid.NullArgumentException {@code referenceId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesForMappedIdOnDate(org.osid.id.Id referenceId,
                                                                           org.osid.calendaring.DateTime from,
                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelevanciesForMappedIdOnDate(referenceId, from, to));
    }


    /**
     *  Gets the {@code Relevancy} elements mapped to an {@code Id} of
     *  the given relevancy genus {@code Type} which includes derived
     *  genus {@code Types.}
     *  
     *  In plenary mode, the exact {@code Id} is found or a {@code
     *  NOT_FOUND} results. Otherwise, the returned {@code Relevancy}
     *  may have a different {@code Id} than requested, such as the
     *  case where a duplicate {@code Id} was assigned to a {@code
     *  Relevancy} and retained for compatibility.
     *  
     *  In effective mode, relevancies are returned that are currently
     *  effective. In any effective mode, effective relevancies and
     *  those currently expired are returned.
     *
     *  @param  id an {@code Id} 
     *  @param  relevancyGenusType relevancy genus type 
     *  @return list of relevancies 
     *  @throws org.osid.NullArgumentException {@code id} or {@code 
     *          relevancyGenusType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesByGenusTypeForMappedId(org.osid.id.Id id, 
                                                                                org.osid.type.Type relevancyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelevanciesByGenusTypeForMappedId(id, relevancyGenusType));
    }


    /**
     *  Gets a {@code RelevancyList} of the given genus type for the
     *  given mapped {@code Id} effective during the entire given date
     *  range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  relevancies or an error results. Otherwise, the returned list
     *  may contain only those relevancies that are accessible through
     *  this session.
     *  
     *  In effective mode, relevancies are returned that are currently
     *  effective. In any effective mode, effective relevancies and
     *  those currently expired are returned.
     *
     *  @param  id an {@code Id} 
     *  @param  relevancyGenusType relevancy genus type 
     *  @param  from a starting date 
     *  @param  to an ending date 
     *  @return list of relevancies 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code id, relevancyGenusType, 
     *          from} or {@code to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesByGenusTypeForMappedIdOnDate(org.osid.id.Id id, 
                                                                                      org.osid.type.Type relevancyGenusType, 
                                                                                      org.osid.calendaring.DateTime from, 
                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelevanciesByGenusTypeForMappedIdOnDate(id, relevancyGenusType, from, to));
    }


    /**
     *  Gets the relevancies for the given mapped {@code Ids.} 
     *  
     *  In plenary mode, the returned list contains all of the
     *  relevancies mapped to the {@code Id} or an error results if an
     *  {@code Id} in the supplied list is not found or inaccessible.
     *  Otherwise, inaccessible relevancies may be omitted from the
     *  list and may present the elements in any order including
     *  returning a unique set.
     *  
     *  In effective mode, relevancies are returned that are currently
     *  effective. In any effective mode, effective relevancies and
     *  those currently expired are returned.
     *
     *  @param  ids a list of {@code Ids} 
     *  @return list of relevancies 
     *  @throws org.osid.NullArgumentException {@code ids} is {@code 
     *          null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesForMappedIds(org.osid.id.IdList ids)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getRelevanciesForMappedIds(ids));
    }


    /**
     *  Gets a list of relevancies corresponding to subject and
     *  reference {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known
     *  relevancies or an error results. Otherwise, the returned list
     *  may contain only those relevancies that are accessible
     *  through this session.
     *
     *  In effective mode, relevancies are returned that are
     *  currently effective.  In any effective mode, effective
     *  relevancies and those currently expired are returned.
     *
     *  @param  subjectId the {@code Id} of the subject
     *  @param  referenceId the {@code Id} of the reference
     *  @return the returned {@code RelevancyList}
     *  @throws org.osid.NullArgumentException {@code subjectId},
     *          {@code referenceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesForSubjectAndMappedId(org.osid.id.Id subjectId,
                                                                                org.osid.id.Id referenceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelevanciesForSubjectAndMappedId(subjectId, referenceId));
    }


    /**
     *  Gets a list of relevancies corresponding to subject and reference
     *  {@code Ids} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  relevancies or an error results. Otherwise, the returned list
     *  may contain only those relevancies that are accessible
     *  through this session.
     *
     *  In effective mode, relevancies are returned that are currently
     *  effective. In any effective mode, effective relevancies and
     *  those currently expired are returned.
     *
     *  @param  referenceId the {@code Id} of the reference
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code RelevancyList}
     *  @throws org.osid.NullArgumentException {@code subjectId},
     *          {@code referenceId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesForSubjectAndMappedIdOnDate(org.osid.id.Id subjectId,
                                                                                     org.osid.id.Id referenceId,
                                                                                     org.osid.calendaring.DateTime from,
                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelevanciesForSubjectAndMappedIdOnDate(subjectId, referenceId, from, to));
    }


    /**
     *  Gets the {@code Relevancy} of the given genus type and mapped
     *  to a subject and mapped {@code Id}.
     *  
     *  In plenary mode, the exact {@code Id} is found or a {@code
     *  NOT_FOUND} results. Otherwise, the returned {@code Relevancy}
     *  may have a different {@code Id} than requested, such as the
     *  case where a duplicate {@code Id} was assigned to a {@code
     *  Relevancy} and retained for compatibility.
     *  
     *  In effective mode, relevancies are returned that are currently
     *  effective. In any effective mode, effective relevancies and
     *  those currently expired are returned.
     *
     *  @param  subjectId the subject {@code Id} 
     *  @param  id the mapped {@code Id} 
     *  @param  relevancyGenusType relevancy genus type 
     *  @return list of relevancies 
     *  @throws org.osid.NullArgumentException {@code subjectId, id} , 
     *          or {@code relevancyGenusType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesByGenusTypeForSubjectAndMappedId(org.osid.id.Id subjectId, 
                                                                                          org.osid.id.Id id, 
                                                                                          org.osid.type.Type relevancyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelevanciesByGenusTypeForSubjectAndMappedId(subjectId, id, relevancyGenusType));
    }


    /**
     *  Gets a {@code RelevancyList} of the given genus type and
     *  related to the given subject and mapped {@code Id} effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  relevancies or an error results. Otherwise, the returned list
     *  may contain only those relevancies that are accessible through
     *  this session.
     *  
     *  In effective mode, relevancies are returned that are currently
     *  effective. In any effective mode, effective relevancies and
     *  those currently expired are returned.
     *
     *  @param  subjectId the subject {@code Id} 
     *  @param  id the mapped {@code Id} 
     *  @param  relevancyGenusType relevancy genus type 
     *  @param  from a starting date 
     *  @param  to an ending date 
     *  @return list of relevancies 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code subjectId, id, 
     *          relevancyGenusTYpe, from} or {@code to} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesByGenusTypeForSubjectAndMappedIdOnDate(org.osid.id.Id subjectId, 
                                                                                                org.osid.id.Id id, 
                                                                                                org.osid.type.Type relevancyGenusType, 
                                                                                                org.osid.calendaring.DateTime from, 
                                                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelevanciesByGenusTypeForSubjectAndMappedIdOnDate(subjectId, id, relevancyGenusType, from, to));
    }


    /**
     *  Gets all {@code Relevancies}. 
     *
     *  In plenary mode, the returned list contains all known
     *  relevancies or an error results. Otherwise, the returned list
     *  may contain only those relevancies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, relevancies are returned that are currently
     *  effective.  In any effective mode, effective relevancies and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Relevancies} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevancies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelevancies());
    }
}
