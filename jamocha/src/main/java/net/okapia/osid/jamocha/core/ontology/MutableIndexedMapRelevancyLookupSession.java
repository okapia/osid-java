//
// MutableIndexedMapRelevancyLookupSession
//
//    Implements a Relevancy lookup service backed by a collection of
//    relevancies indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.ontology;


/**
 *  Implements a Relevancy lookup service backed by a collection of
 *  relevancies. The relevancies are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some relevancies may be compatible
 *  with more types than are indicated through these relevancy
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of relevancies can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapRelevancyLookupSession
    extends net.okapia.osid.jamocha.core.ontology.spi.AbstractIndexedMapRelevancyLookupSession
    implements org.osid.ontology.RelevancyLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapRelevancyLookupSession} with no relevancies.
     *
     *  @param ontology the ontology
     *  @throws org.osid.NullArgumentException {@code ontology}
     *          is {@code null}
     */

      public MutableIndexedMapRelevancyLookupSession(org.osid.ontology.Ontology ontology) {
        setOntology(ontology);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapRelevancyLookupSession} with a
     *  single relevancy.
     *  
     *  @param ontology the ontology
     *  @param  relevancy a single relevancy
     *  @throws org.osid.NullArgumentException {@code ontology} or
     *          {@code relevancy} is {@code null}
     */

    public MutableIndexedMapRelevancyLookupSession(org.osid.ontology.Ontology ontology,
                                                  org.osid.ontology.Relevancy relevancy) {
        this(ontology);
        putRelevancy(relevancy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapRelevancyLookupSession} using an
     *  array of relevancies.
     *
     *  @param ontology the ontology
     *  @param  relevancies an array of relevancies
     *  @throws org.osid.NullArgumentException {@code ontology} or
     *          {@code relevancies} is {@code null}
     */

    public MutableIndexedMapRelevancyLookupSession(org.osid.ontology.Ontology ontology,
                                                  org.osid.ontology.Relevancy[] relevancies) {
        this(ontology);
        putRelevancies(relevancies);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapRelevancyLookupSession} using a
     *  collection of relevancies.
     *
     *  @param ontology the ontology
     *  @param  relevancies a collection of relevancies
     *  @throws org.osid.NullArgumentException {@code ontology} or
     *          {@code relevancies} is {@code null}
     */

    public MutableIndexedMapRelevancyLookupSession(org.osid.ontology.Ontology ontology,
                                                  java.util.Collection<? extends org.osid.ontology.Relevancy> relevancies) {

        this(ontology);
        putRelevancies(relevancies);
        return;
    }
    

    /**
     *  Makes a {@code Relevancy} available in this session.
     *
     *  @param  relevancy a relevancy
     *  @throws org.osid.NullArgumentException {@code relevancy{@code  is
     *          {@code null}
     */

    @Override
    public void putRelevancy(org.osid.ontology.Relevancy relevancy) {
        super.putRelevancy(relevancy);
        return;
    }


    /**
     *  Makes an array of relevancies available in this session.
     *
     *  @param  relevancies an array of relevancies
     *  @throws org.osid.NullArgumentException {@code relevancies{@code 
     *          is {@code null}
     */

    @Override
    public void putRelevancies(org.osid.ontology.Relevancy[] relevancies) {
        super.putRelevancies(relevancies);
        return;
    }


    /**
     *  Makes collection of relevancies available in this session.
     *
     *  @param  relevancies a collection of relevancies
     *  @throws org.osid.NullArgumentException {@code relevancy{@code  is
     *          {@code null}
     */

    @Override
    public void putRelevancies(java.util.Collection<? extends org.osid.ontology.Relevancy> relevancies) {
        super.putRelevancies(relevancies);
        return;
    }


    /**
     *  Removes a Relevancy from this session.
     *
     *  @param relevancyId the {@code Id} of the relevancy
     *  @throws org.osid.NullArgumentException {@code relevancyId{@code  is
     *          {@code null}
     */

    @Override
    public void removeRelevancy(org.osid.id.Id relevancyId) {
        super.removeRelevancy(relevancyId);
        return;
    }    
}
