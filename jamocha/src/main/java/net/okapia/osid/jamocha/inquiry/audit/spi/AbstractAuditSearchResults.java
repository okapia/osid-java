//
// AbstractAuditSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inquiry.audit.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractAuditSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.inquiry.AuditSearchResults {

    private org.osid.inquiry.AuditList audits;
    private final org.osid.inquiry.AuditQueryInspector inspector;
    private final java.util.Collection<org.osid.inquiry.records.AuditSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractAuditSearchResults.
     *
     *  @param audits the result set
     *  @param auditQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>audits</code>
     *          or <code>auditQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractAuditSearchResults(org.osid.inquiry.AuditList audits,
                                            org.osid.inquiry.AuditQueryInspector auditQueryInspector) {
        nullarg(audits, "audits");
        nullarg(auditQueryInspector, "audit query inspectpr");

        this.audits = audits;
        this.inspector = auditQueryInspector;

        return;
    }


    /**
     *  Gets the audit list resulting from a search.
     *
     *  @return an audit list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.inquiry.AuditList getAudits() {
        if (this.audits == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.inquiry.AuditList audits = this.audits;
        this.audits = null;
	return (audits);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.inquiry.AuditQueryInspector getAuditQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  audit search record <code> Type. </code> This method must
     *  be used to retrieve an audit implementing the requested
     *  record.
     *
     *  @param auditSearchRecordType an audit search 
     *         record type 
     *  @return the audit search
     *  @throws org.osid.NullArgumentException
     *          <code>auditSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(auditSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inquiry.records.AuditSearchResultsRecord getAuditSearchResultsRecord(org.osid.type.Type auditSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.inquiry.records.AuditSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(auditSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(auditSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record audit search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addAuditRecord(org.osid.inquiry.records.AuditSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "audit record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
