//
// AbstractMapSignalEnablerLookupSession
//
//    A simple framework for providing a SignalEnabler lookup service
//    backed by a fixed collection of signal enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.path.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a SignalEnabler lookup service backed by a
 *  fixed collection of signal enablers. The signal enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>SignalEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapSignalEnablerLookupSession
    extends net.okapia.osid.jamocha.mapping.path.rules.spi.AbstractSignalEnablerLookupSession
    implements org.osid.mapping.path.rules.SignalEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.mapping.path.rules.SignalEnabler> signalEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.mapping.path.rules.SignalEnabler>());


    /**
     *  Makes a <code>SignalEnabler</code> available in this session.
     *
     *  @param  signalEnabler a signal enabler
     *  @throws org.osid.NullArgumentException <code>signalEnabler<code>
     *          is <code>null</code>
     */

    protected void putSignalEnabler(org.osid.mapping.path.rules.SignalEnabler signalEnabler) {
        this.signalEnablers.put(signalEnabler.getId(), signalEnabler);
        return;
    }


    /**
     *  Makes an array of signal enablers available in this session.
     *
     *  @param  signalEnablers an array of signal enablers
     *  @throws org.osid.NullArgumentException <code>signalEnablers<code>
     *          is <code>null</code>
     */

    protected void putSignalEnablers(org.osid.mapping.path.rules.SignalEnabler[] signalEnablers) {
        putSignalEnablers(java.util.Arrays.asList(signalEnablers));
        return;
    }


    /**
     *  Makes a collection of signal enablers available in this session.
     *
     *  @param  signalEnablers a collection of signal enablers
     *  @throws org.osid.NullArgumentException <code>signalEnablers<code>
     *          is <code>null</code>
     */

    protected void putSignalEnablers(java.util.Collection<? extends org.osid.mapping.path.rules.SignalEnabler> signalEnablers) {
        for (org.osid.mapping.path.rules.SignalEnabler signalEnabler : signalEnablers) {
            this.signalEnablers.put(signalEnabler.getId(), signalEnabler);
        }

        return;
    }


    /**
     *  Removes a SignalEnabler from this session.
     *
     *  @param  signalEnablerId the <code>Id</code> of the signal enabler
     *  @throws org.osid.NullArgumentException <code>signalEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeSignalEnabler(org.osid.id.Id signalEnablerId) {
        this.signalEnablers.remove(signalEnablerId);
        return;
    }


    /**
     *  Gets the <code>SignalEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  signalEnablerId <code>Id</code> of the <code>SignalEnabler</code>
     *  @return the signalEnabler
     *  @throws org.osid.NotFoundException <code>signalEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>signalEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnabler getSignalEnabler(org.osid.id.Id signalEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.mapping.path.rules.SignalEnabler signalEnabler = this.signalEnablers.get(signalEnablerId);
        if (signalEnabler == null) {
            throw new org.osid.NotFoundException("signalEnabler not found: " + signalEnablerId);
        }

        return (signalEnabler);
    }


    /**
     *  Gets all <code>SignalEnablers</code>. In plenary mode, the returned
     *  list contains all known signalEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  signalEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>SignalEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerList getSignalEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.mapping.path.rules.signalenabler.ArraySignalEnablerList(this.signalEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.signalEnablers.clear();
        super.close();
        return;
    }
}
