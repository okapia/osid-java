//
// AbstractFederatingEntryLookupSession.java
//
//     An abstract federating adapter for an EntryLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.dictionary.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  EntryLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingEntryLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.dictionary.EntryLookupSession>
    implements org.osid.dictionary.EntryLookupSession {

    private boolean parallel = false;
    private org.osid.dictionary.Dictionary dictionary = new net.okapia.osid.jamocha.nil.dictionary.dictionary.UnknownDictionary();


    /**
     *  Constructs a new <code>AbstractFederatingEntryLookupSession</code>.
     */

    protected AbstractFederatingEntryLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.dictionary.EntryLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Dictionary/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Dictionary Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDictionaryId() {
        return (this.dictionary.getId());
    }


    /**
     *  Gets the <code>Dictionary</code> associated with this 
     *  session.
     *
     *  @return the <code>Dictionary</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.Dictionary getDictionary()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.dictionary);
    }


    /**
     *  Sets the <code>Dictionary</code>.
     *
     *  @param  dictionary the dictionary for this session
     *  @throws org.osid.NullArgumentException <code>dictionary</code>
     *          is <code>null</code>
     */

    protected void setDictionary(org.osid.dictionary.Dictionary dictionary) {
        nullarg(dictionary, "dictionary");
        this.dictionary = dictionary;
        return;
    }


    /**
     *  Tests if this user can perform <code>Entry</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupEntries() {
        for (org.osid.dictionary.EntryLookupSession session : getSessions()) {
            if (session.canLookupEntries()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Entry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeEntryView() {
        for (org.osid.dictionary.EntryLookupSession session : getSessions()) {
            session.useComparativeEntryView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Entry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryEntryView() {
        for (org.osid.dictionary.EntryLookupSession session : getSessions()) {
            session.usePlenaryEntryView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include entries in dictionaries which are children
     *  of this dictionary in the dictionary hierarchy.
     */

    @OSID @Override
    public void useFederatedDictionaryView() {
        for (org.osid.dictionary.EntryLookupSession session : getSessions()) {
            session.useFederatedDictionaryView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this dictionary only.
     */

    @OSID @Override
    public void useIsolatedDictionaryView() {
        for (org.osid.dictionary.EntryLookupSession session : getSessions()) {
            session.useIsolatedDictionaryView();
        }

        return;
    }


    /**
     *  Gets the <code> Dictionary </code> entry associated with the given key 
     *  and types. The <code> keyType </code> indicates the key object type 
     *  and the <code> valueType </code> indicates the value object return. 
     *
     *  @param  key the key of the entry to retrieve 
     *  @param  keyType the key type of the entry to retrieve 
     *  @param  valueType the value type of the entry to retrieve 
     *  @return the returned <code> object </code> 
     *  @throws org.osid.InvalidArgumentException <code> key </code> is not of 
     *          <code> keyType </code> 
     *  @throws org.osid.NotFoundException no entry found 
     *  @throws org.osid.NullArgumentException <code> key, keyType </code> or 
     *          <code> valueType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public java.lang.Object retrieveEntry(java.lang.Object key, 
                                          org.osid.type.Type keyType, 
                                          org.osid.type.Type valueType)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.dictionary.EntryLookupSession session : getSessions()) {
            try {
                return (session.retrieveEntry(key, keyType, valueType));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException("entry not found");
    }

    
    /**
     *  Gets the <code>Entry</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Entry</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Entry</code> and
     *  retained for compatibility.
     *
     *  @param  entryId <code>Id</code> of the
     *          <code>Entry</code>
     *  @return the entry
     *  @throws org.osid.NotFoundException <code>entryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>entryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.Entry getEntry(org.osid.id.Id entryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.dictionary.EntryLookupSession session : getSessions()) {
            try {
                return (session.getEntry(entryId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(entryId + " not found");
    }


    /**
     *  Gets an <code>EntryList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  entries specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Entries</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getEntries()</code>.
     *
     *  @param  entryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Entry</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>entryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.EntryList getEntriesByIds(org.osid.id.IdList entryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.dictionary.entry.MutableEntryList ret = new net.okapia.osid.jamocha.dictionary.entry.MutableEntryList();

        try (org.osid.id.IdList ids = entryIds) {
            while (ids.hasNext()) {
                ret.addEntry(getEntry(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>EntryList</code> corresponding to the given
     *  entry genus <code>Type</code> which does not include
     *  entries of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  entries or an error results. Otherwise, the returned list
     *  may contain only those entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getEntries()</code>.
     *
     *  @param  entryGenusType an entry genus type 
     *  @return the returned <code>Entry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>entryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.EntryList getEntriesByGenusType(org.osid.type.Type entryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.dictionary.entry.FederatingEntryList ret = getEntryList();

        for (org.osid.dictionary.EntryLookupSession session : getSessions()) {
            ret.addEntryList(session.getEntriesByGenusType(entryGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>EntryList</code> corresponding to the given
     *  entry genus <code>Type</code> and include any additional
     *  entries with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  entries or an error results. Otherwise, the returned list
     *  may contain only those entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getEntries()</code>.
     *
     *  @param  entryGenusType an entry genus type 
     *  @return the returned <code>Entry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>entryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.EntryList getEntriesByParentGenusType(org.osid.type.Type entryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.dictionary.entry.FederatingEntryList ret = getEntryList();

        for (org.osid.dictionary.EntryLookupSession session : getSessions()) {
            ret.addEntryList(session.getEntriesByParentGenusType(entryGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>EntryList</code> containing the given
     *  entry record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  entries or an error results. Otherwise, the returned list
     *  may contain only those entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getEntries()</code>.
     *
     *  @param  entryRecordType an entry record type 
     *  @return the returned <code>Entry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>entryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.EntryList getEntriesByRecordType(org.osid.type.Type entryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.dictionary.entry.FederatingEntryList ret = getEntryList();

        for (org.osid.dictionary.EntryLookupSession session : getSessions()) {
            ret.addEntryList(session.getEntriesByRecordType(entryRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all the <code> Dictionary </code> entries matching the
     *  given key <code> Type. </code> In plenary mode, the returned
     *  list contains all of the entries specified in the Id list, in
     *  the order of the list, including duplicates, or an error
     *  results if an Id in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible Entry elements may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  keyType the type of the key to match 
     *  @return the list of entries matching <code> keyType </code> 
     *  @throws org.osid.NullArgumentException <code> keyType </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.dictionary.EntryList getEntriesByKeyType(org.osid.type.Type keyType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.dictionary.entry.FederatingEntryList ret = getEntryList();

        for (org.osid.dictionary.EntryLookupSession session : getSessions()) {
            ret.addEntryList(session.getEntriesByKeyType(keyType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }

    
    /**
     *  Gets all the <code> Dictionary </code> entries matching the
     *  given key and value <code> Type. </code> In plenary mode, the
     *  returned list contains all of the entries specified in the Id
     *  list, in the order of the list, including duplicates, or an
     *  error results if an Id in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible Entry elements may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  keyType the type of the key to match 
     *  @param  valueType the type of the value to match 
     *  @return the list of entries matching <code> keyType </code> 
     *  @throws org.osid.NullArgumentException <code> keyType </code> or 
     *          <code> valueType </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.dictionary.EntryList getEntriesByKeyTypeAndValueType(org.osid.type.Type keyType, 
                                                                         org.osid.type.Type valueType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.dictionary.entry.FederatingEntryList ret = getEntryList();

        for (org.osid.dictionary.EntryLookupSession session : getSessions()) {
            ret.addEntryList(session.getEntriesByKeyTypeAndValueType(keyType, valueType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all the <code> Dictionary </code> entries matching the
     *  given key and key <code> Type. I </code> n plenary mode, the
     *  returned list contains all of the entries specified in the Id
     *  list, in the order of the list, including duplicates, or an
     *  error results if an Id in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible Entry elements may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  key the key to match 
     *  @param  keyType the type of the value to match 
     *  @return the list of entries matching <code> keyType </code> 
     *  @throws org.osid.NullArgumentException <code> key </code> or <code> 
     *          keyType </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
    
    @OSID @Override
    public org.osid.dictionary.EntryList getEntriesByKeyAndKeyType(java.lang.Object key, 
                                                                   org.osid.type.Type keyType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.dictionary.entry.FederatingEntryList ret = getEntryList();

        for (org.osid.dictionary.EntryLookupSession session : getSessions()) {
            ret.addEntryList(session.getEntriesByKeyAndKeyType(key, keyType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Entries</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  entries or an error results. Otherwise, the returned list
     *  may contain only those entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Entries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.EntryList getEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.dictionary.entry.FederatingEntryList ret = getEntryList();

        for (org.osid.dictionary.EntryLookupSession session : getSessions()) {
            ret.addEntryList(session.getEntries());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.dictionary.entry.FederatingEntryList getEntryList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.dictionary.entry.ParallelEntryList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.dictionary.entry.CompositeEntryList());
        }
    }
}
