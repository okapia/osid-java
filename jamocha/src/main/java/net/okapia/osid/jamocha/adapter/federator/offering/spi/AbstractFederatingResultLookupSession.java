//
// AbstractFederatingResultLookupSession.java
//
//     An abstract federating adapter for a ResultLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.offering.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  ResultLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingResultLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.offering.ResultLookupSession>
    implements org.osid.offering.ResultLookupSession {

    private boolean parallel = false;
    private org.osid.offering.Catalogue catalogue = new net.okapia.osid.jamocha.nil.offering.catalogue.UnknownCatalogue();


    /**
     *  Constructs a new <code>AbstractFederatingResultLookupSession</code>.
     */

    protected AbstractFederatingResultLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.offering.ResultLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Catalogue/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Catalogue Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCatalogueId() {
        return (this.catalogue.getId());
    }


    /**
     *  Gets the <code>Catalogue</code> associated with this 
     *  session.
     *
     *  @return the <code>Catalogue</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Catalogue getCatalogue()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.catalogue);
    }


    /**
     *  Sets the <code>Catalogue</code>.
     *
     *  @param  catalogue the catalogue for this session
     *  @throws org.osid.NullArgumentException <code>catalogue</code>
     *          is <code>null</code>
     */

    protected void setCatalogue(org.osid.offering.Catalogue catalogue) {
        nullarg(catalogue, "catalogue");
        this.catalogue = catalogue;
        return;
    }


    /**
     *  Tests if this user can perform <code>Result</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupResults() {
        for (org.osid.offering.ResultLookupSession session : getSessions()) {
            if (session.canLookupResults()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Result</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeResultView() {
        for (org.osid.offering.ResultLookupSession session : getSessions()) {
            session.useComparativeResultView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Result</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryResultView() {
        for (org.osid.offering.ResultLookupSession session : getSessions()) {
            session.usePlenaryResultView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include results in catalogues which are children
     *  of this catalogue in the catalogue hierarchy.
     */

    @OSID @Override
    public void useFederatedCatalogueView() {
        for (org.osid.offering.ResultLookupSession session : getSessions()) {
            session.useFederatedCatalogueView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this catalogue only.
     */

    @OSID @Override
    public void useIsolatedCatalogueView() {
        for (org.osid.offering.ResultLookupSession session : getSessions()) {
            session.useIsolatedCatalogueView();
        }

        return;
    }


    /**
     *  Only results whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveResultView() {
        for (org.osid.offering.ResultLookupSession session : getSessions()) {
            session.useEffectiveResultView();
        }

        return;
    }


    /**
     *  All results of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveResultView() {
        for (org.osid.offering.ResultLookupSession session : getSessions()) {
            session.useAnyEffectiveResultView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Result</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Result</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Result</code> and
     *  retained for compatibility.
     *
     *  In effective mode, results are returned that are currently
     *  effective.  In any effective mode, effective results and
     *  those currently expired are returned.
     *
     *  @param  resultId <code>Id</code> of the
     *          <code>Result</code>
     *  @return the result
     *  @throws org.osid.NotFoundException <code>resultId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>resultId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Result getResult(org.osid.id.Id resultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.offering.ResultLookupSession session : getSessions()) {
            try {
                return (session.getResult(resultId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(resultId + " not found");
    }


    /**
     *  Gets a <code>ResultList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  results specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Results</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, results are returned that are currently
     *  effective.  In any effective mode, effective results and those
     *  currently expired are returned.
     *
     *  @param  resultIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Result</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>resultIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ResultList getResultsByIds(org.osid.id.IdList resultIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.offering.result.MutableResultList ret = new net.okapia.osid.jamocha.offering.result.MutableResultList();

        try (org.osid.id.IdList ids = resultIds) {
            while (ids.hasNext()) {
                ret.addResult(getResult(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>ResultList</code> corresponding to the given
     *  result genus <code>Type</code> which does not include results
     *  of types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known results
     *  or an error results. Otherwise, the returned list may contain
     *  only those results that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  In effective mode, results are returned that are currently
     *  effective.  In any effective mode, effective results and those
     *  currently expired are returned.
     *
     *  @param  resultGenusType a result genus type 
     *  @return the returned <code>Result</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>resultGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ResultList getResultsByGenusType(org.osid.type.Type resultGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.result.FederatingResultList ret = getResultList();

        for (org.osid.offering.ResultLookupSession session : getSessions()) {
            ret.addResultList(session.getResultsByGenusType(resultGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ResultList</code> corresponding to the given
     *  result genus <code>Type</code> and include any additional
     *  results with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known results
     *  or an error results. Otherwise, the returned list may contain
     *  only those results that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  In effective mode, results are returned that are currently
     *  effective.  In any effective mode, effective results and those
     *  currently expired are returned.
     *
     *  @param  resultGenusType a result genus type 
     *  @return the returned <code>Result</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>resultGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ResultList getResultsByParentGenusType(org.osid.type.Type resultGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.result.FederatingResultList ret = getResultList();

        for (org.osid.offering.ResultLookupSession session : getSessions()) {
            ret.addResultList(session.getResultsByParentGenusType(resultGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ResultList</code> containing the given
     *  result record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  results or an error results. Otherwise, the returned list
     *  may contain only those results that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, results are returned that are currently
     *  effective.  In any effective mode, effective results and
     *  those currently expired are returned.
     *
     *  @param  resultRecordType a result record type 
     *  @return the returned <code>Result</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>resultRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ResultList getResultsByRecordType(org.osid.type.Type resultRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.result.FederatingResultList ret = getResultList();

        for (org.osid.offering.ResultLookupSession session : getSessions()) {
            ret.addResultList(session.getResultsByRecordType(resultRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ResultList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  results or an error results. Otherwise, the returned list
     *  may contain only those results that are accessible
     *  through this session.
     *  
     *  In active mode, results are returned that are currently
     *  active. In any status mode, active and inactive results
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Result</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.offering.ResultList getResultsOnDate(org.osid.calendaring.DateTime from, 
                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.result.FederatingResultList ret = getResultList();

        for (org.osid.offering.ResultLookupSession session : getSessions()) {
            ret.addResultList(session.getResultsOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets an <code> ResultList </code> by genus type effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known results
     *  or an error results. Otherwise, the returned list may contain
     *  only those results that are accessible through this session.
     *  
     *  In effective mode, results are returned that are currently
     *  effective.  In any effective mode, effective results and those
     *  currently expired are returned.
     *
     *  @param  resultGenusType a results genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code> ResultList </code> 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> resultGenusType, from or 
     *          to </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ResultList getResultsByGenusTypeOnDate(org.osid.type.Type resultGenusType, 
                                                                    org.osid.calendaring.DateTime from, 
                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.result.FederatingResultList ret = getResultList();

        for (org.osid.offering.ResultLookupSession session : getSessions()) {
            ret.addResultList(session.getResultsByGenusTypeOnDate(resultGenusType, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code> ResultList </code> for the given participant
     *  <code>Id</code>.
     *  
     *  In plenary mode, the returned list contains all of the results
     *  corresponding to the given participant, including duplicates,
     *  or an error results if an result is inaccessible. Otherwise,
     *  inaccessible <code> Results </code> may be omitted from the
     *  list.
     *  
     *  In effective mode, results are returned that are currently
     *  effective.  In any effective mode, effective results and those
     *  currently expired are returned.
     *
     *  @param  participantId a participant <code> Id </code> 
     *  @return the returned <code> ResultList </code> 
     *  @throws org.osid.NullArgumentException <code> participantId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ResultList getResultsForParticipant(org.osid.id.Id participantId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.result.FederatingResultList ret = getResultList();

        for (org.osid.offering.ResultLookupSession session : getSessions()) {
            ret.addResultList(session.getResultsForParticipant(participantId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code> ResultList </code> for the given participant
     *  <code>Id</code>.
     *  
     *  In plenary mode, the returned list contains all of the results
     *  corresponding to the given participant, including duplicates,
     *  or an error results if an result is inaccessible. Otherwise,
     *  inaccessible <code> Results </code> may be omitted from the
     *  list.
     *  
     *  In effective mode, results are returned that are currently
     *  effective.  In any effective mode, effective results and those
     *  currently expired are returned.
     *
     *  @param  participantId a participant <code> Id </code> 
     *  @param  resultGenusType a results genus type 
     *  @return the returned <code> ResultList </code> 
     *  @throws org.osid.NullArgumentException <code> participantId
     *          </code> or <code> resultGenusType </code> is <code>
     *          null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ResultList getResultsByGenusTypeForParticipant(org.osid.id.Id participantId, 
                                                                            org.osid.type.Type resultGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.result.FederatingResultList ret = getResultList();

        for (org.osid.offering.ResultLookupSession session : getSessions()) {
            ret.addResultList(session.getResultsByGenusTypeForParticipant(participantId, resultGenusType));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of results for a participant and effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known results
     *  or an error results. Otherwise, the returned list may contain
     *  only those results that are accessible through this session.
     *  
     *  In effective mode, results are returned that are currently
     *  effective.  In any effective mode, effective results and those
     *  currently expired are returned.
     *
     *  @param  participantId a participant <code> Id </code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code> ResultList </code> 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> participantId,
     *          from, </code> or <code> to </code> is <code> null
     *          </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ResultList getResultsForParticipantOnDate(org.osid.id.Id participantId, 
                                                                       org.osid.calendaring.DateTime from, 
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.result.FederatingResultList ret = getResultList();

        for (org.osid.offering.ResultLookupSession session : getSessions()) {
            ret.addResultList(session.getResultsForParticipantOnDate(participantId, from, to));
        }

        ret.noMore();
        return (ret);
    }
        
        
    /**
     *  Gets a list of results for a participant and effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known results
     *  or an error results. Otherwise, the returned list may contain
     *  only those results that are accessible through this session.
     *  
     *  In effective mode, results are returned that are currently
     *  effective.  In any effective mode, effective results and those
     *  currently expired are returned.
     *
     *  @param  participantId a participant <code> Id </code> 
     *  @param  resultGenusType a results genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code> ResultList </code> 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> participantId,
     *          resultGenusType, from, </code> or <code> to </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.ResultList getResultsByGenusTypeForParticipantOnDate(org.osid.id.Id participantId, 
                                                                                  org.osid.type.Type resultGenusType, 
                                                                                  org.osid.calendaring.DateTime from, 
                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.result.FederatingResultList ret = getResultList();

        for (org.osid.offering.ResultLookupSession session : getSessions()) {
            ret.addResultList(session.getResultsByGenusTypeForParticipantOnDate(participantId, resultGenusType, from, to));
        }

        ret.noMore();
        return (ret);
    }
        
    
    /**
     *  Gets all <code>Results</code>. 
     *
     *  In plenary mode, the returned list contains all known results
     *  or an error results. Otherwise, the returned list may contain
     *  only those results that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  In effective mode, results are returned that are currently
     *  effective.  In any effective mode, effective results and those
     *  currently expired are returned.
     *
     *  @return a list of <code>Results</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ResultList getResults()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.result.FederatingResultList ret = getResultList();

        for (org.osid.offering.ResultLookupSession session : getSessions()) {
            ret.addResultList(session.getResults());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.offering.result.FederatingResultList getResultList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.offering.result.ParallelResultList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.offering.result.CompositeResultList());
        }
    }
}
