//
// AbstractProvision.java
//
//     Defines a Provision.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.provision.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Provision</code>.
 */

public abstract class AbstractProvision
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.provisioning.Provision {

    private org.osid.provisioning.Broker broker;
    private org.osid.provisioning.Provisionable provisionable;
    private org.osid.resource.Resource recipient;
    private org.osid.provisioning.Request request;
    private org.osid.calendaring.DateTime provisionDate;
    private org.osid.calendaring.DateTime dueDate;
    private org.osid.financials.Currency cost;
    private org.osid.financials.Currency rateAmount;
    private org.osid.calendaring.Duration ratePeriod;
    private org.osid.provisioning.ProvisionReturn provisionReturn;

    private boolean leased     = false;
    private boolean mustReturn = false;

    private final java.util.Collection<org.osid.provisioning.records.ProvisionRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the broker. 
     *
     *  @return the broker <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getBrokerId() {
        return (this.broker.getId());
    }


    /**
     *  Gets the broker. 
     *
     *  @return the broker 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.provisioning.Broker getBroker()
        throws org.osid.OperationFailedException {

        return (this.broker);
    }


    /**
     *  Sets the broker.
     *
     *  @param broker a broker
     *  @throws org.osid.NullArgumentException
     *          <code>broker</code> is <code>null</code>
     */

    protected void setBroker(org.osid.provisioning.Broker broker) {
        nullarg(broker, "broker");
        this.broker = broker;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the provisionable. 
     *
     *  @return the provisionable <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getProvisionableId() {
        return (this.provisionable.getId());
    }


    /**
     *  Gets the provisionable. 
     *
     *  @return the provisionable 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.provisioning.Provisionable getProvisionable()
        throws org.osid.OperationFailedException {

        return (this.provisionable);
    }


    /**
     *  Sets the provisionable.
     *
     *  @param provisionable a provisionable
     *  @throws org.osid.NullArgumentException
     *          <code>provisionable</code> is <code>null</code>
     */

    protected void setProvisionable(org.osid.provisioning.Provisionable provisionable) {
        nullarg(provisionable, "provisionable");
        this.provisionable = provisionable;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the recipient. 
     *
     *  @return the recipient <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getRecipientId() {
        return (this.recipient.getId());
    }


    /**
     *  Gets the recipient. 
     *
     *  @return the recipient 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getRecipient()
        throws org.osid.OperationFailedException {

        return (this.recipient);
    }


    /**
     *  Sets the recipient.
     *
     *  @param recipient a recipient
     *  @throws org.osid.NullArgumentException <code>recipient</code>
     *          is <code>null</code>
     */

    protected void setRecipient(org.osid.resource.Resource recipient) {
        nullarg(recipient, "recipient");
        this.recipient = recipient;
        return;
    }


    /**
     *  Tests if a request is available.
     *
     *  @return <code>true</code> if a request is available,
     *          <code>false</code> otherwise
     */

    @OSID @Override
    public boolean provisionedByRequest() {
        if (this.request == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the <code> Id </code> of the request. 
     *
     *  @return the request <code> Id </code> 
     *  @throws org.osid.IllegalStateException
     *          <code>provisionedByRequst()</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.id.Id getRequestId() {
        if (!provisionedByRequest()) {
            throw new org.osid.IllegalStateException("provisionedByRequest() is false");
        }

        return (this.request.getId());
    }


    /**
     *  Gets the request. 
     *
     *  @return the request
     *  @throws org.osid.IllegalStateException
     *          <code>provisionedByRequst()</code> is
     *          <code>false</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.provisioning.Request getRequest()
        throws org.osid.OperationFailedException {

        if (!provisionedByRequest()) {
            throw new org.osid.IllegalStateException("provisionedByRequest() is false");
        }

        return (this.request);
    }


    /**
     *  Sets the request.
     *
     *  @param request a request
     *  @throws org.osid.NullArgumentException
     *          <code>request</code> is <code>null</code>
     */

    protected void setRequest(org.osid.provisioning.Request request) {
        nullarg(request, "request");
        this.request = request;
        return;
    }


    /**
     *  Gets the date this was provisioned. The provision date may
     *  differ from the effective dates of this provision.
     *
     *  @return the provision date
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getProvisionDate() {
        return (this.provisionDate);
    }


    /**
     *  Sets the provision date.
     *
     *  @param date the provision date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    protected void setProvisionDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "provision date");
        this.provisionDate = date;
        return;
    }


    /**
     *  Tests if this provision is temporary. The lease ends when the 
     *  provision expires or is returned. 
     *
     *  @return <code>true</code> if this is a lease,
     *          <code>false</code> if the provision is permanent
     */

    @OSID @Override
    public boolean isLeased() {
        return (this.leased);
    }


    /**
     *  Sets the leased flag.
     *
     *  @param leased <code>true</code> if this is a lease,
     *         <code>false</code> if the provision is permanent
     */

    protected void setLeased(boolean leased) {
        this.leased = leased;
        return;
    }


    /**
     *  Tests if this provision is must be returned. 
     *
     *  @return <code>true</code> if this is must be returned,
     *          <code>false</code> otherwise
     *  @throws org.osid.IllegalStateException <code> isLeased()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public boolean mustReturn() {
        return (this.mustReturn);
    }


    /**
     *  Sets the must return flag.
     *
     *  @param mustReturn <code>true</code> if this is must be
     *         returned, <code>false</code> otherwise
     */

    protected void setMustReturn(boolean mustReturn) {
        this.mustReturn = mustReturn;
        return;
    }


    /**
     *  Tests if this provision is must be returned and has a due
     *  date.
     *
     *  @return <code>true</code> if this is must be returned,
     *          <code>false</code> otherwise
     *  @throws org.osid.IllegalStateException <code> mustReturn()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public boolean hasDueDate() {
        if (!mustReturn()) {
            throw new org.osid.IllegalStateException("mustReturn() is false");
        }

        return (this.dueDate != null);
    }


    /**
     *  Gets the due date for the return. 
     *
     *  @return the due date 
     *  @throws org.osid.IllegalStateException <code> hasDueDate()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getDueDate() {
        if (!hasDueDate()) {
            throw new org.osid.IllegalStateException("hasDueDate() is false");
        }

        return (this.dueDate);
    }


    /**
     *  Sets the due date.
     *
     *  @param date a due date
     *  @throws org.osid.NullArgumentException
     *          <code>date</code> is <code>null</code>
     */

    protected void setDueDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "due date");
        this.dueDate = dueDate;
        return;
    }


    /**
     *  Tests if this provision has a total cost. If this is a lease
     *  with an expected return than it is the cost for the time
     *  period.
     *
     *  @return <code>true</code> if there is a cost,
     *          <code>false</code> otherwise
     */

    @OSID @Override
    public boolean hasCost() {
        return (this.cost != null);
    }


    /**
     *  Gets the cost. 
     *
     *  @return the cost 
     *  @throws org.osid.IllegalStateException <code> hasCost() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.Currency getCost() {
        if (!hasCost()) {
            throw new org.osid.IllegalStateException("hasCost() is false");
        }

        return (this.cost);
    }


    /**
     *  Sets the cost.
     *
     *  @param cost a cost
     *  @throws org.osid.NullArgumentException
     *          <code>cost</code> is <code>null</code>
     */

    protected void setCost(org.osid.financials.Currency cost) {
        nullarg(cost, "cost");
        this.cost = cost;
        return;
    }


    /**
     *  Tests if this provision has a rate per time period. 
     *
     *  @return <code>true</code> if there is a rat,e false otherwise 
     *  @throws org.osid.IllegalStateException <code> hasRate() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public boolean hasRate() {
        return (this.rateAmount != null);
    }


    /**
     *  Gets the rate amount. 
     *
     *  @return the rate amount 
     *  @throws org.osid.IllegalStateException <code> hasCost()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.financials.Currency getRateAmount() {
        if (!hasRate()) {
            throw new org.osid.IllegalStateException("hasRate() is false");
        }

        return (this.rateAmount);
    }


    /**
     *  Sets the rate amount.
     *
     *  @param amount a rate amount
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    protected void setRateAmount(org.osid.financials.Currency amount) {
        nullarg(amount, "amount");
        this.rateAmount = rateAmount;
        return;
    }


    /**
     *  Gets the rate period. 
     *
     *  @return the time period 
     *  @throws org.osid.IllegalStateException <code> hasRate() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getRatePeriod() {
        if (!hasRate()) {
            throw new org.osid.IllegalStateException("hasRate() is false");
        }

        return (this.ratePeriod);
    }


    /**
     *  Sets the rate period.
     *
     *  @param period a rate period
     *  @throws org.osid.NullArgumentException
     *          <code>period</code> is <code>null</code>
     */

    protected void setRatePeriod(org.osid.calendaring.Duration period) {
        nullarg(period, "period");
        this.ratePeriod = ratePeriod;
        return;
    }


    /**
     *  Tests if this provision has been returned. 
     *
     *  @return <code>true</code> if this provision has been returned,
     *          <code>false</code> otherwise
     */

    @OSID @Override
    public boolean isReturned() {
        return (this.provisionReturn != null);
    }


    /**
     *  Gets the provision return that has any data that may have been 
     *  captured in the return process. 
     *
     *  @return the return 
     *  @throws org.osid.IllegalStateException <code> isReturned() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionReturn getProvisionReturn() {
        if (!isReturned()) {
            throw new org.osid.IllegalStateException("isReturned() is false");
        }

        return (this.provisionReturn);
    }


    /**
     *  Sets the provision return.
     *
     *  @param provisionReturn a provision return
     *  @throws org.osid.NullArgumentException
     *          <code>provisionReturn</code> is <code>null</code>
     */

    protected void setProvisionReturn(org.osid.provisioning.ProvisionReturn provisionReturn) {
        nullarg(provisionReturn, "provision return");
        this.provisionReturn = provisionReturn;
        return;
    }


    /**
     *  Tests if this provision supports the given record
     *  <code>Type</code>.
     *
     *  @param  provisionRecordType a provision record type 
     *  @return <code>true</code> if the provisionRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>provisionRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type provisionRecordType) {
        for (org.osid.provisioning.records.ProvisionRecord record : this.records) {
            if (record.implementsRecordType(provisionRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Provision</code> record <code>Type</code>.
     *
     *  @param  provisionRecordType the provision record type 
     *  @return the provision record 
     *  @throws org.osid.NullArgumentException
     *          <code>provisionRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(provisionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.ProvisionRecord getProvisionRecord(org.osid.type.Type provisionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.ProvisionRecord record : this.records) {
            if (record.implementsRecordType(provisionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(provisionRecordType + " is not supported");
    }


    /**
     *  Adds a record to this provision. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param provisionRecord the provision record
     *  @param provisionRecordType provision record type
     *  @throws org.osid.NullArgumentException
     *          <code>provisionRecord</code> or
     *          <code>provisionRecordTypeprovision</code> is
     *          <code>null</code>
     */
            
    protected void addProvisionRecord(org.osid.provisioning.records.ProvisionRecord provisionRecord, 
                                      org.osid.type.Type provisionRecordType) {

        nullarg(provisionRecord, "provision record");
        addRecordType(provisionRecordType);
        this.records.add(provisionRecord);
        
        return;
    }
}
