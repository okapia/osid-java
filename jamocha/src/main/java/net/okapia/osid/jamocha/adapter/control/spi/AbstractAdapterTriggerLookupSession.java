//
// AbstractAdapterTriggerLookupSession.java
//
//    A Trigger lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.control.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Trigger lookup session adapter.
 */

public abstract class AbstractAdapterTriggerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.control.TriggerLookupSession {

    private final org.osid.control.TriggerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterTriggerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterTriggerLookupSession(org.osid.control.TriggerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code System/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code System Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getSystemId() {
        return (this.session.getSystemId());
    }


    /**
     *  Gets the {@code System} associated with this session.
     *
     *  @return the {@code System} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.System getSystem()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getSystem());
    }


    /**
     *  Tests if this user can perform {@code Trigger} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupTriggers() {
        return (this.session.canLookupTriggers());
    }


    /**
     *  A complete view of the {@code Trigger} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeTriggerView() {
        this.session.useComparativeTriggerView();
        return;
    }


    /**
     *  A complete view of the {@code Trigger} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryTriggerView() {
        this.session.usePlenaryTriggerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include triggers in systems which are children
     *  of this system in the system hierarchy.
     */

    @OSID @Override
    public void useFederatedSystemView() {
        this.session.useFederatedSystemView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this system only.
     */

    @OSID @Override
    public void useIsolatedSystemView() {
        this.session.useIsolatedSystemView();
        return;
    }
    

    /**
     *  Only active triggers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveTriggerView() {
        this.session.useActiveTriggerView();
        return;
    }


    /**
     *  Active and inactive triggers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusTriggerView() {
        this.session.useAnyStatusTriggerView();
        return;
    }
    
     
    /**
     *  Gets the {@code Trigger} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Trigger} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Trigger} and
     *  retained for compatibility.
     *
     *  In active mode, triggers are returned that are currently
     *  active. In any status mode, active and inactive triggers
     *  are returned.
     *
     *  @param triggerId {@code Id} of the {@code Trigger}
     *  @return the trigger
     *  @throws org.osid.NotFoundException {@code triggerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code triggerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.Trigger getTrigger(org.osid.id.Id triggerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTrigger(triggerId));
    }


    /**
     *  Gets a {@code TriggerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  triggers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Triggers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, triggers are returned that are currently
     *  active. In any status mode, active and inactive triggers
     *  are returned.
     *
     *  @param  triggerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Trigger} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code triggerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.TriggerList getTriggersByIds(org.osid.id.IdList triggerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTriggersByIds(triggerIds));
    }


    /**
     *  Gets a {@code TriggerList} corresponding to the given
     *  trigger genus {@code Type} which does not include
     *  triggers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  triggers or an error results. Otherwise, the returned list
     *  may contain only those triggers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, triggers are returned that are currently
     *  active. In any status mode, active and inactive triggers
     *  are returned.
     *
     *  @param  triggerGenusType a trigger genus type 
     *  @return the returned {@code Trigger} list
     *  @throws org.osid.NullArgumentException
     *          {@code triggerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.TriggerList getTriggersByGenusType(org.osid.type.Type triggerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTriggersByGenusType(triggerGenusType));
    }


    /**
     *  Gets a {@code TriggerList} corresponding to the given
     *  trigger genus {@code Type} and include any additional
     *  triggers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  triggers or an error results. Otherwise, the returned list
     *  may contain only those triggers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, triggers are returned that are currently
     *  active. In any status mode, active and inactive triggers
     *  are returned.
     *
     *  @param  triggerGenusType a trigger genus type 
     *  @return the returned {@code Trigger} list
     *  @throws org.osid.NullArgumentException
     *          {@code triggerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.TriggerList getTriggersByParentGenusType(org.osid.type.Type triggerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTriggersByParentGenusType(triggerGenusType));
    }


    /**
     *  Gets a {@code TriggerList} containing the given
     *  trigger record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  triggers or an error results. Otherwise, the returned list
     *  may contain only those triggers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, triggers are returned that are currently
     *  active. In any status mode, active and inactive triggers
     *  are returned.
     *
     *  @param  triggerRecordType a trigger record type 
     *  @return the returned {@code Trigger} list
     *  @throws org.osid.NullArgumentException
     *          {@code triggerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.TriggerList getTriggersByRecordType(org.osid.type.Type triggerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTriggersByRecordType(triggerRecordType));
    }


    /**
     *  Gets a list of triggers for the given controller.
     *  
     *  In plenary mode, the returned list contains all known triggers
     *  or an error results. Otherwise, the returned list may contain
     *  only those triggers that are accessible through this session.
     *  
     *  In active mode, triggers are returned that are currently
     *  active. In any status mode, active and inactive triggers are
     *  returned.
     *
     *  @param  controllerId a controller {@code Id} 
     *  @return the returned {@code Trigger} list 
     *  @throws org.osid.NullArgumentException {@code controllerId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
    
    @OSID @Override
    public org.osid.control.TriggerList getTriggersForController(org.osid.id.Id controllerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTriggersForController(controllerId));
    }


    /**
     *  Gets all {@code Triggers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  triggers or an error results. Otherwise, the returned list
     *  may contain only those triggers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, triggers are returned that are currently
     *  active. In any status mode, active and inactive triggers
     *  are returned.
     *
     *  @return a list of {@code Triggers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.TriggerList getTriggers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTriggers());
    }
}
