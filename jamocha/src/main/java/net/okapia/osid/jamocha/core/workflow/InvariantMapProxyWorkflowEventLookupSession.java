//
// InvariantMapProxyWorkflowEventLookupSession
//
//    Implements a WorkflowEvent lookup service backed by a fixed
//    collection of workflowEvents. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.workflow;


/**
 *  Implements a WorkflowEvent lookup service backed by a fixed
 *  collection of workflow events. The workflow events are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyWorkflowEventLookupSession
    extends net.okapia.osid.jamocha.core.workflow.spi.AbstractMapWorkflowEventLookupSession
    implements org.osid.workflow.WorkflowEventLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyWorkflowEventLookupSession} with no
     *  workflow events.
     *
     *  @param office the office
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code office} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyWorkflowEventLookupSession(org.osid.workflow.Office office,
                                                  org.osid.proxy.Proxy proxy) {
        setOffice(office);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyWorkflowEventLookupSession} with a single
     *  workflow event.
     *
     *  @param office the office
     *  @param workflowEvent a single workflow event
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code office},
     *          {@code workflowEvent} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyWorkflowEventLookupSession(org.osid.workflow.Office office,
                                                  org.osid.workflow.WorkflowEvent workflowEvent, org.osid.proxy.Proxy proxy) {

        this(office, proxy);
        putWorkflowEvent(workflowEvent);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyWorkflowEventLookupSession} using
     *  an array of workflow events.
     *
     *  @param office the office
     *  @param workflowEvents an array of workflow events
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code office},
     *          {@code workflowEvents} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyWorkflowEventLookupSession(org.osid.workflow.Office office,
                                                  org.osid.workflow.WorkflowEvent[] workflowEvents, org.osid.proxy.Proxy proxy) {

        this(office, proxy);
        putWorkflowEvents(workflowEvents);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyWorkflowEventLookupSession} using a
     *  collection of workflow events.
     *
     *  @param office the office
     *  @param workflowEvents a collection of workflow events
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code office},
     *          {@code workflowEvents} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyWorkflowEventLookupSession(org.osid.workflow.Office office,
                                                  java.util.Collection<? extends org.osid.workflow.WorkflowEvent> workflowEvents,
                                                  org.osid.proxy.Proxy proxy) {

        this(office, proxy);
        putWorkflowEvents(workflowEvents);
        return;
    }
}
