//
// AbstractMutableRoom.java
//
//     Defines a mutable Room.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.room.room.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Room</code>.
 */

public abstract class AbstractMutableRoom
    extends net.okapia.osid.jamocha.room.room.spi.AbstractRoom
    implements org.osid.room.Room,
               net.okapia.osid.jamocha.builder.room.room.RoomMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this room. 
     *
     *  @param record room record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addRoomRecord(org.osid.room.records.RoomRecord record, org.osid.type.Type recordType) {
        super.addRoomRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the start date for when this room is effective.
     *
     *  @param date the start date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setStartDate(org.osid.calendaring.DateTime date) {
        super.setStartDate(date);
        return;
    }


    /**
     *  Sets the end date for when this room ceases to be
     *  effective.
     *
     *  @param date the end date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setEndDate(org.osid.calendaring.DateTime date) {
        super.setEndDate(date);
        return;
    }


    /**
     *  Sets the display name for this room.
     *
     *  @param displayName the name for this room
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this room.
     *
     *  @param description the description of this room
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the building.
     *
     *  @param building a building
     *  @throws org.osid.NullArgumentException <code>building</code>
     *          is <code>null</code>
     */

    @Override
    public void setBuilding(org.osid.room.Building building) {
        super.setBuilding(building);
        return;
    }


    /**
     *  Sets the floor.
     *
     *  @param floor a floor
     *  @throws org.osid.NullArgumentException <code>floor</code> is
     *          <code>null</code>
     */

    @Override
    public void setFloor(org.osid.room.Floor floor) {
        super.setFloor(floor);
        return;
    }


    /**
     *  Sets the enclosing room.
     *
     *  @param room an enclosing room
     *  @throws org.osid.NullArgumentException <code>room</code> is
     *          <code>null</code>
     */

    @Override
    public void setEnclosingRoom(org.osid.room.Room room) {
        super.setEnclosingRoom(room);
        return;
    }


    /**
     *  Adds a subdivision.
     *
     *  @param subdivision a subdivision
     *  @throws org.osid.NullArgumentException
     *          <code>subdivision</code> is <code>null</code>
     */

    @Override
    public void addSubdivision(org.osid.room.Room subdivision) {
        super.addSubdivision(subdivision);
        return;
    }


    /**
     *  Sets all the subdivisions.
     *
     *  @param subdivisions a collection of subdivisions
     *  @throws org.osid.NullArgumentException
     *          <code>subdivisions</code> is <code>null</code>
     */

    @Override
    public void setSubdivisions(java.util.Collection<org.osid.room.Room> subdivisions) {
        super.setSubdivisions(subdivisions);
        return;
    }


    /**
     *  Sets the designated name.
     *
     *  @param name a designated name
     *  @throws org.osid.NullArgumentException <code>name</code> is
     *          <code>null</code>
     */

    @Override
    public void setDesignatedName(org.osid.locale.DisplayText name) {
        super.setDesignatedName(name);
        return;
    }


    /**
     *  Sets the room number.
     *
     *  @param number a room number
     *  @throws org.osid.NullArgumentException <code>number</code> is
     *          <code>null</code>
     */

    public void setRoomNumber(String number) {
        super.setRoomNumber(number);
        return;
    }


    /**
     *  Sets the code.
     *
     *  @param code a code
     *  @throws org.osid.NullArgumentException <code>code</code> is
     *          <code>null</code>
     */

    @Override
    public void setCode(String code) {
        super.setCode(code);
        return;
    }


    /**
     *  Sets the area.
     *
     *  @param area an area
     *  @throws org.osid.InvalidArgumentException <code>area</code> is
     *          negative
     *  @throws org.osid.NullArgumentException <code>area</code> is
     *          <code>null</code>
     */

    @Override
    public void setArea(java.math.BigDecimal area) {
        super.setArea(area);
        return;
    }


    /**
     *  Sets the occupancy limit.
     *
     *  @param limit an occupancy limit
     *  @throws org.osid.InvalidArgumentException <code>limit</code>
     *          is negative
     */

    @Override
    public void setOccupancyLimit(long limit) {
        super.setOccupancyLimit(limit);
        return;
    }


    /**
     *  Adds a resource.
     *
     *  @param resource a resource
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    @Override
    public void addResource(org.osid.resource.Resource resource) {
        super.addResource(resource);
        return;
    }


    /**
     *  Sets all the resources.
     *
     *  @param resources a collection of resources
     *  @throws org.osid.NullArgumentException
     *          <code>resources</code> is <code>null</code>
     */

    @Override
    public void setResources(java.util.Collection<org.osid.resource.Resource> resources) {
        super.setResources(resources);
        return;
    }
}

