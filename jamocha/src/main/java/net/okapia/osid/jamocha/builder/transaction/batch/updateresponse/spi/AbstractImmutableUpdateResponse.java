//
// AbstractImmutableUpdateResponse.java
//
//     Wraps a mutable UpdateResponse to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.transaction.batch.updateresponse.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>UpdateResponse</code> to hide modifiers. This
 *  wrapper provides an immutized UpdateResponse from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying updateResponse whose state changes are visible.
 */

public abstract class AbstractImmutableUpdateResponse
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutable
    implements org.osid.transaction.batch.UpdateResponse {

    private final org.osid.transaction.batch.UpdateResponse updateResponse;


    /**
     *  Constructs a new <code>AbstractImmutableUpdateResponse</code>.
     *
     *  @param updateResponse the update response to immutablize
     *  @throws org.osid.NullArgumentException <code>updateResponse</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableUpdateResponse(org.osid.transaction.batch.UpdateResponse updateResponse) {
        super(updateResponse);
        this.updateResponse = updateResponse;
        return;
    }


    /**
     *  Gets the form <code> Id </code> corresponding to this response. 
     *
     *  @return the form <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getFormId() {
        return (this.updateResponse.getFormId());
    }


    /**
     *  Gets the reference <code> Id </code> of the object on which this item 
     *  within the update operation operated. 
     *
     *  @return the reference object <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getUpdatedId() {
        return (this.updateResponse.getUpdatedId());
    }


    /**
     *  Tests if this item within the update operation was successful. 
     *
     *  @return <code> true </code> if the update operation was successful, 
     *          <code> false </code> if it was not successful 
     */

    @OSID @Override
    public boolean isSuccessful() {
        return (this.updateResponse.isSuccessful());
    }


    /**
     *  Gets the error message for an unsuccessful item within the update 
     *  operation. 
     *
     *  @return the message 
     *  @throws org.osid.IllegalStateException <code> isSuccessful() </code> 
     *          is <code> true </code> 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getErrorMessage() {
        return (this.updateResponse.getErrorMessage());
    }
}

