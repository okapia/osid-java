//
// AbstractAuctionConstrainer.java
//
//     Defines an AuctionConstrainer builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.bidding.rules.auctionconstrainer.spi;


/**
 *  Defines an <code>AuctionConstrainer</code> builder.
 */

public abstract class AbstractAuctionConstrainerBuilder<T extends AbstractAuctionConstrainerBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidConstrainerBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.bidding.rules.auctionconstrainer.AuctionConstrainerMiter auctionConstrainer;


    /**
     *  Constructs a new <code>AbstractAuctionConstrainerBuilder</code>.
     *
     *  @param auctionConstrainer the auction constrainer to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractAuctionConstrainerBuilder(net.okapia.osid.jamocha.builder.bidding.rules.auctionconstrainer.AuctionConstrainerMiter auctionConstrainer) {
        super(auctionConstrainer);
        this.auctionConstrainer = auctionConstrainer;
        return;
    }


    /**
     *  Builds the auction constrainer.
     *
     *  @return the new auction constrainer
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.bidding.rules.AuctionConstrainer build() {
        (new net.okapia.osid.jamocha.builder.validator.bidding.rules.auctionconstrainer.AuctionConstrainerValidator(getValidations())).validate(this.auctionConstrainer);
        return (new net.okapia.osid.jamocha.builder.bidding.rules.auctionconstrainer.ImmutableAuctionConstrainer(this.auctionConstrainer));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the auction constrainer miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.bidding.rules.auctionconstrainer.AuctionConstrainerMiter getMiter() {
        return (this.auctionConstrainer);
    }


    /**
     *  Adds an AuctionConstrainer record.
     *
     *  @param record an auction constrainer record
     *  @param recordType the type of auction constrainer record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.bidding.rules.records.AuctionConstrainerRecord record, org.osid.type.Type recordType) {
        getMiter().addAuctionConstrainerRecord(record, recordType);
        return (self());
    }
}       


