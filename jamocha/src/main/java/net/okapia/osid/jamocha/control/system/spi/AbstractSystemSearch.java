//
// AbstractSystemSearch.java
//
//     A template for making a System Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.system.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing system searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractSystemSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.control.SystemSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.control.records.SystemSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.control.SystemSearchOrder systemSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of systems. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  systemIds list of systems
     *  @throws org.osid.NullArgumentException
     *          <code>systemIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongSystems(org.osid.id.IdList systemIds) {
        while (systemIds.hasNext()) {
            try {
                this.ids.add(systemIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongSystems</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of system Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getSystemIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  systemSearchOrder system search order 
     *  @throws org.osid.NullArgumentException
     *          <code>systemSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>systemSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderSystemResults(org.osid.control.SystemSearchOrder systemSearchOrder) {
	this.systemSearchOrder = systemSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.control.SystemSearchOrder getSystemSearchOrder() {
	return (this.systemSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given system search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a system implementing the requested record.
     *
     *  @param systemSearchRecordType a system search record
     *         type
     *  @return the system search record
     *  @throws org.osid.NullArgumentException
     *          <code>systemSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(systemSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.SystemSearchRecord getSystemSearchRecord(org.osid.type.Type systemSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.control.records.SystemSearchRecord record : this.records) {
            if (record.implementsRecordType(systemSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(systemSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this system search. 
     *
     *  @param systemSearchRecord system search record
     *  @param systemSearchRecordType system search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addSystemSearchRecord(org.osid.control.records.SystemSearchRecord systemSearchRecord, 
                                           org.osid.type.Type systemSearchRecordType) {

        addRecordType(systemSearchRecordType);
        this.records.add(systemSearchRecord);        
        return;
    }
}
