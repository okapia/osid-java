//
// InlineStepProcessorEnablerNotifier.java
//
//     A callback interface for performing step processor enabler
//     notifications.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.workflow.rules.spi;


/**
 *  A callback interface for performing step processor enabler
 *  notifications.
 */

public interface InlineStepProcessorEnablerNotifier {



    /**
     *  Notifies the creation of a new step processor enabler.
     *
     *  @param stepProcessorEnablerId the {@code Id} of the new 
     *         step processor enabler
     *  @throws org.osid.NullArgumentException {@code [objectId]}
     *          is {@code null}
     */

    public void newStepProcessorEnabler(org.osid.id.Id stepProcessorEnablerId);


    /**
     *  Notifies the change of an updated step processor enabler.
     *
     *  @param stepProcessorEnablerId the {@code Id} of the changed
     *         step processor enabler
     *  @throws org.osid.NullArgumentException {@code [objectId]}
     *          is {@code null}
     */
      
    public void changedStepProcessorEnabler(org.osid.id.Id stepProcessorEnablerId);


    /**
     *  Notifies the deletion of an step processor enabler.
     *
     *  @param stepProcessorEnablerId the {@code Id} of the deleted
     *         step processor enabler
     *  @throws org.osid.NullArgumentException {@code [objectId]} is
     *          {@code null}
     */

    public void deletedStepProcessorEnabler(org.osid.id.Id stepProcessorEnablerId);

}
