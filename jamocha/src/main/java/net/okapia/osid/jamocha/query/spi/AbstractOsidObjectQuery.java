//
// AbstractOsidObjectQuery.java
//
//     An OisdObjectQuery with stored terms.
//
//
// Tom Coppeto
// Okapia
// 20 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.query.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An OsidObjectQuery with stored terms.
 */

public abstract class AbstractOsidObjectQuery
    extends AbstractOsidIdentifiableQuery
    implements org.osid.OsidObjectQuery {

    private final java.util.Collection<org.osid.search.terms.StringTerm> displayNameTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.search.terms.StringTerm> descriptionTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.search.terms.TypeTerm> genusTypeTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.search.terms.TypeTerm> parentGenusTypeTerms = new java.util.LinkedHashSet<>();

    private final java.util.Collection<org.osid.search.terms.IdTerm> subjectIdTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.search.terms.IdTerm> stateIdTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.search.terms.IdTerm> commentIdTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.search.terms.IdTerm> journalEntryIdTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.search.terms.IdTerm> statisticIdTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.search.terms.IdTerm> creditIdTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.search.terms.IdTerm> relationshipIdTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.search.terms.IdTerm> relationshipPeerIdTerms = new java.util.LinkedHashSet<>();

    private final OsidBrowsableQuery query;


    /**
     *  Constructs a new <code>AbstractOsidObjectQuery</code>.
     *
     *  @param factory the term factory
     *  @throws org.osid.NullArgumentException <code>factory</code> is
     *          <code>null</code>
     */

    protected AbstractOsidObjectQuery(net.okapia.osid.jamocha.query.TermFactory factory) {
        super(factory);
        this.query = new OsidBrowsableQuery(factory);
        return;
    }


    /**
     *  Adds a display name to match. Multiple display name matches can be 
     *  added to perform a boolean <code> OR </code> among them. 
     *
     *  @param  displayName display name to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> displayName </code> 
     *          is not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> displayName </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchDisplayName(String displayName, 
                                 org.osid.type.Type stringMatchType, 
                                 boolean match) {

        this.displayNameTerms.add(getTermFactory().createStringTerm(displayName, stringMatchType, match));
        return;
    }


    /**
     *  Matches any object with a display name. 
     *
     *  @param  match <code> true </code> to match any display name, <code> 
     *          false </code> to match objects with no display name 
     */

    @OSID @Override
    public void matchAnyDisplayName(boolean match) {
        this.displayNameTerms.add(getTermFactory().createStringWildcardTerm(match));
        return;
    }


    /**
     *  Clears all display name terms. 
     */

    @OSID @Override
    public void clearDisplayNameTerms() {
        this.displayNameTerms.clear();
        return;
    }


    /**
     *  Gets all the display name query terms.
     *
     *  @return a collection of the display name query terms
     */

    protected java.util.Collection<org.osid.search.terms.StringTerm> getDisplayNameTerms() {
        return (java.util.Collections.unmodifiableCollection(this.displayNameTerms));
    }


    /**
     *  Adds a description to match. Multiple description matches can be 
     *  added to perform a boolean <code> OR </code> among them. 
     *
     *  @param  description description to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> description </code> 
     *          is not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> description </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchDescription(String description, 
                                 org.osid.type.Type stringMatchType, 
                                 boolean match) {

        this.descriptionTerms.add(getTermFactory().createStringTerm(description, stringMatchType, match));
        return;
    }


    /**
     *  Matches any object with a description. 
     *
     *  @param  match <code> true </code> to match any description, <code> 
     *          false </code> to match objects with no description 
     */

    @OSID @Override
    public void matchAnyDescription(boolean match) {
        this.descriptionTerms.add(getTermFactory().createStringWildcardTerm(match));
        return;
    }


    /**
     *  Clears all description terms. 
     */

    @OSID @Override
    public void clearDescriptionTerms() {
        this.descriptionTerms.clear();
        return;
    }


    /**
     *  Gets all the description query terms.
     *
     *  @return a collection of the description query terms
     */

    protected java.util.Collection<org.osid.search.terms.StringTerm> getDescriptionTerms() {
        return (java.util.Collections.unmodifiableCollection(this.descriptionTerms));
    }


    /**
     *  Sets a <code> Type </code> for querying objects of a given genus. A 
     *  genus type matches if the specified type is the same genus as the 
     *  object genus type. 
     *
     *  @param  genusType the object genus type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> genusType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGenusType(org.osid.type.Type genusType, boolean match) {
        this.genusTypeTerms.add(getTermFactory().createTypeTerm(genusType, match));
        return;
    }


    /**
     *  Clears all genus type terms. 
     */

    @OSID @Override
    public void clearGenusTypeTerms() {
        this.genusTypeTerms.clear();
        return;
    }


    /**
     *  Gets all the genus type query terms.
     *
     *  @return a collection of the genus type query terms
     */

    protected java.util.Collection<org.osid.search.terms.TypeTerm> getGenusTypeTerms() {
        return (java.util.Collections.unmodifiableCollection(this.genusTypeTerms));
    }


    /**
     *  Sets a <code> Type </code> for querying objects of a given genus. A 
     *  genus type matches if the specified type is the same genus as the 
     *  object or if the specified type is an ancestor of the object genus in 
     *  a type hierarchy. 
     *
     *  @param  genusType the object genus type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> genusType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchParentGenusType(org.osid.type.Type genusType, boolean match) {
        this.parentGenusTypeTerms.add(getTermFactory().createTypeTerm(genusType, match));
        return;
    }


    /**
     *  Matches an object that has any genus type. 
     *
     *  @param  match <code> true </code> to match any genus type, <code> 
     *          false </code> to match objects with no genus type 
     */

    @OSID @Override
    public void matchAnyGenusType(boolean match) {
        this.genusTypeTerms.add(getTermFactory().createTypeWildcardTerm(match));
        return;
    }


    /**
     *  Gets all the parent genus type query terms.
     *
     *  @return a collection of the parent genus type query terms
     */

    protected java.util.Collection<org.osid.search.terms.TypeTerm> getParentGenusTypeTerms() {
        return (java.util.Collections.unmodifiableCollection(this.parentGenusTypeTerms));
    }


    /**
     *  Clears all parent genus type terms. 
     */

    @OSID @Override
    public void clearParentGenusTypeTerms() {
        this.parentGenusTypeTerms.clear();
        return;
    }


    /**
     *  Matches an object with a relationship to the given subject. 
     *
     *  @param  subjectId a subject <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> subjectId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSubjectId(org.osid.id.Id subjectId, boolean match) {
        this.subjectIdTerms.add(getTermFactory().createIdTerm(subjectId, match));
        return;
    }


    /**
     *  Clears all subject <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSubjectIdTerms() {
        this.subjectIdTerms.clear();
        return;
    }


    /**
     *  Gets all the subject Id query terms.
     *
     *  @return a collection of the subject Id query terms
     */

    protected java.util.Collection<org.osid.search.terms.IdTerm> getSubjectIdTerms() {
        return (java.util.Collections.unmodifiableCollection(this.subjectIdTerms));
    }


    /**
     *  Tests if a <code> SubjectQuery </code> is available. 
     *
     *  @return <code> true </code> if a subject query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubjectQuery() {
        return (false);
    }


    /**
     *  Gets the query for a subject. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the subject query 
     *  @throws org.osid.UnimplementedException <code> supportsSubjectQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectQuery getSubjectQuery() {
        throw new org.osid.UnimplementedException("supportsSubjectQuery() is false");
    }


    /**
     *  Matches an object that has any relationship to a <code> Subject. 
     *  </code> 
     *
     *  @param  match <code> true </code> to match any subject, <code> false 
     *          </code> to match objects with no subjects 
     */

    @OSID @Override
    public void matchAnySubject(boolean match) {
        this.subjectIdTerms.add(getTermFactory().createIdWildcardTerm(match));
        return;
    }


    /**
     *  Clears all subject terms. 
     */

    @OSID @Override
    public void clearSubjectTerms() {
        clearWildcardTerms(subjectIdTerms);    
        return;
    }


    /**
     *  Tests if a <code> RelevancyQuery </code> is available to provide
     *  queries about the relationships to <code> Subjects. </code>
     *
     *  @return <code> true </code> if a relevancy entry query is available,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean supportsSubjectRelevancyQuery() {
        return (false);
    }


    /**
     *  Gets the query for a subject relevancy. Multiple retrievals produce a
     *  nested <code> OR </code> term.
     *
     *  @return the relevancy query
     *  @throws org.osid.UnimplementedException <code>
     *          supportsSubjectRelevancyQuery() </code> is <code> false
     *          </code>
     */

    @OSID @Override
    public org.osid.ontology.RelevancyQuery getSubjectRelevancyQuery() {
        throw new org.osid.UnimplementedException("supportsRelevancyQuery() is false");
    }


    /**
     *  Clears all subject relevancy terms.
     */

    @OSID @Override
    public void clearSubjectRelevancyTerms() {
        return;
    }


    /**
     *  Matches an object mapped to the given state.
     *
     *  @param  stateId a state <code> Id </code>
     *  @param  match <code> true </code> for a positive match, <code> false
     *          </code> for a negative match
     *  @throws org.osid.NullArgumentException <code> stateId </code> is
     *          <code> null </code>
     */

    @OSID @Override
    public void matchStateId(org.osid.id.Id stateId, boolean match) {
        this.stateIdTerms.add(getTermFactory().createIdTerm(stateId, match));
        return;
    }


    /**
     *  Clears all state <code> Id </code> terms.
     */

    @OSID @Override
    public void clearStateIdTerms() {
        this.stateIdTerms.clear();
        return;
    }


    /**
     *  Gets all the state Id query terms.
     *
     *  @return a collection of the state Id query terms
     */

    protected java.util.Collection<org.osid.search.terms.IdTerm> getStateIdTerms() {
        return (java.util.Collections.unmodifiableCollection(this.stateIdTerms));
    }


    /**
     *  Tests if a <code> StateQuery </code> is available to provide queries
     *  of processed objects.
     *
     *  @return <code> true </code> if a state query is available, <code>
     *          false </code> otherwise
     */

    @OSID @Override
    public boolean supportsStateQuery() {
        return (false);
    }


    /**
     *  Gets the query for a state. Multiple retrievals produce a nested
     *  <code> OR </code> term.
     *
     *  @return the journal entry query
     *  @throws org.osid.UnimplementedException <code> supportsStateQuery()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.process.StateQuery getStateQuery() {
        throw new org.osid.UnimplementedException("supportsStateQuery() is false");
    }


    /**
     *  Matches an object that has any mapping to a <code> State </code>.
     *
     *  @param  match <code> true </code> to match any state, <code> false
     *          </code> to match objects with no states
     */

    @OSID @Override
    public void matchAnyState(boolean match) {
        this.stateIdTerms.add(getTermFactory().createIdWildcardTerm(match));
        return;
    }


    /**
     *  Clears all state terms.
     */

    @OSID @Override
    public void clearStateTerms() {
        clearWildcardTerms(stateIdTerms);
        return;
    }


    /**
     *  Matches an object that has the given comment.
     *
     *  @param  commentId a comment <code> Id </code>
     *  @param  match <code> true </code> for a positive match, <code> false
     *          </code> for a negative match
     *  @throws org.osid.NullArgumentException <code> commentId </code> is
     *          <code> null </code>
     */

    @OSID @Override
    public void matchCommentId(org.osid.id.Id commentId, boolean match) {
        this.commentIdTerms.add(getTermFactory().createIdTerm(commentId, match));
        return;
    }


    /**
     *  Clears all comment <code> Id </code> terms.
     */

    @OSID @Override
    public void clearCommentIdTerms() {
        this.commentIdTerms.clear();
        return;
    }


    /**
     *  Gets all the comment Id query terms.
     *
     *  @return a collection of the comment Id query terms
     */

    protected java.util.Collection<org.osid.search.terms.IdTerm> getCommentIdTerms() {
        return (java.util.Collections.unmodifiableCollection(this.commentIdTerms));
    }


    /**
     *  Tests if a <code> CommentQuery </code> is available.
     *
     *  @return <code> true </code> if a comment query is available, <code>
     *          false </code> otherwise
     */

    @OSID @Override
    public boolean supportsCommentQuery() {
        return (false);
    }


    /**
     *  Gets the query for a comment. Multiple retrievals produce a nested
     *  <code> OR </code> term.
     *
     *  @return the comment query
     *  @throws org.osid.UnimplementedException <code> supportsCommentQuery()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.commenting.CommentQuery getCommentQuery() {
        throw new org.osid.UnimplementedException("supportsCommentQuery() is false");
    }


    /**
     *  Matches an object that has any <code> Comment </code>.
     *
     *  @param  match <code> true </code> to match any comment, <code> false
     *          </code> to match objects with no comments
     */

    @OSID @Override
    public void matchAnyComment(boolean match) {
        this.commentIdTerms.add(getTermFactory().createIdWildcardTerm(match));
        return;
    }


    /**
     *  Clears all comment terms.
     */

    @OSID @Override
    public void clearCommentTerms() {
        clearWildcardTerms(commentIdTerms);
        return;
    }


    /**
     *  Matches an object that has the given journal entry.
     *
     *  @param  journalEntryId a journal entry <code> Id </code>
     *  @param  match <code> true </code> for a positive match, <code> false
     *          </code> for a negative match
     *  @throws org.osid.NullArgumentException <code> journalEntryId </code>
     *          is <code> null </code>
     */

    @OSID @Override
    public void matchJournalEntryId(org.osid.id.Id journalEntryId, boolean match) {
        this.journalEntryIdTerms.add(getTermFactory().createIdTerm(journalEntryId, match));
        return;
    }


    /**
     *  Clears all journal entry <code> Id </code> terms.
     */

    @OSID @Override
    public void clearJournalEntryIdTerms() {
        this.journalEntryIdTerms.clear();
        return;
    }


    /**
     *  Gets all the journal entry Id query terms.
     *
     *  @return a collection of the journal entry Id query terms
     */

    protected java.util.Collection<org.osid.search.terms.IdTerm> getJournalEntryIdTerms() {
        return (java.util.Collections.unmodifiableCollection(this.journalEntryIdTerms));
    }


    /**
     *  Tests if a <code> JournalEntry </code> is available to provide queries
     *  of journaled <code> OsidObjects. </code>
     *
     *  @return <code> true </code> if a journal entry query is available,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean supportsJournalEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a journal entry. Multiple retrievals produce a
     *  nested <code> OR </code> term.
     *
     *  @return the journal entry query
     *  @throws org.osid.UnimplementedException <code>
     *          supportsJournalEntryQuery() </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryQuery getJournalEntryQuery() {
        throw new org.osid.UnimplementedException("supportsJournalEntryQuery() is false");
    }


    /**
     *  Matches an object that has any <code> JournalEntry </code> in
     *  the given <code> Journal. </code>
     *
     *  @param match <code> true </code> to match any journal entry,
     *         <code> false </code> to match objects with no journal
     *         entries
     */

    @OSID @Override
    public void matchAnyJournalEntry(boolean match) {
        this.journalEntryIdTerms.add(getTermFactory().createIdWildcardTerm(match));
        return;
    }


    /**
     *  Clears all journal entry terms.
     */

    @OSID @Override
    public void clearJournalEntryTerms() {
        clearWildcardTerms(journalEntryIdTerms);
        return;
    }


    /**
     *  Tests if a <code> StatisticQuery </code> is available to provide
     *  statistical queries.
     *
     *  @return <code> true </code> if a statistic query is available, <code>
     *          false </code> otherwise
     */

    @OSID @Override
    public boolean supportsStatisticQuery() {
        return (false);
    }


    /**
     *  Gets the query for a statistic. Multiple retrievals produce a nested
     *  <code> OR </code> term.
     *
     *  @return the statistic query
     *  @throws org.osid.UnimplementedException <code>
     *          supportsStatisticQuery() </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.metering.StatisticQuery getStatisticQuery() {
        throw new org.osid.UnimplementedException("supportsStatisticQuery() is false");
    }


    /**
     *  Matches an object that has any <code> Statistic </code>.
     *
     *  @param  match <code> true </code> to match any statistics, <code> false
     *          </code> to match objects with no statistics
     */

    @OSID @Override
    public void matchAnyStatistic(boolean match) {
        this.statisticIdTerms.add(getTermFactory().createIdWildcardTerm(match));
        return;
    }


    /**
     *  Clears all statistic terms.
     */

    @OSID @Override
    public void clearStatisticTerms() {
        clearWildcardTerms(statisticIdTerms);
        return;
    }


    /**
     *  Matches an object that has the given credit.
     *
     *  @param  creditId a credit <code> Id </code>
     *  @param  match <code> true </code> for a positive match, <code> false
     *          </code> for a negative match
     *  @throws org.osid.NullArgumentException <code> creditId </code> is
     *          <code> null </code>
     */

    @OSID @Override
    public void matchCreditId(org.osid.id.Id creditId, boolean match) {
        this.creditIdTerms.add(getTermFactory().createIdTerm(creditId, match));
        return;
    }


    /**
     *  Clears all credit <code> Id </code> terms.
     */

    @OSID @Override
    public void clearCreditIdTerms() {
        this.creditIdTerms.clear();
        return;
    }


    /**
     *  Gets all the credit Id query terms.
     *
     *  @return a collection of the credit Id query terms
     */

    protected java.util.Collection<org.osid.search.terms.IdTerm> getCreditIdTerms() {
        return (java.util.Collections.unmodifiableCollection(this.creditIdTerms));
    }


    /**
     *  Tests if a <code> CreditQuery </code> is available to provide queries
     *  of related acknowledgements.
     *
     *  @return <code> true </code> if a credit query is available, <code>
     *          false </code> otherwise
     */

    @OSID @Override
    public boolean supportsCreditQuery() {
        return (false);
    }


    /**
     *  Gets the query for an ackowledgement credit. Multiple retrievals
     *  produce a nested <code> OR </code> term.
     *
     *  @return the credit query
     *  @throws org.osid.UnimplementedException <code> supportsCreditQuery()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditQuery getCreditQuery() {
        throw new org.osid.UnimplementedException("supportsCreditQuery() is false");
    }


    /**
     *  Matches an object that has any <code> Credit </code> for the given
     *  <code> Meter. </code>
     *
     *  @param  match <code> true </code> to match any credit, <code> false
     *          </code> to match objects with no credits
     */

    @OSID @Override
    public void matchAnyCredit(boolean match) {
        this.creditIdTerms.add(getTermFactory().createIdWildcardTerm(match));
        return;
    }


    /**
     *  Clears all credit terms.
     */

    @OSID @Override
    public void clearCreditTerms() {
        clearWildcardTerms(creditIdTerms);
        return;
    }


    /**
     *  Matches an object that has the given relationship.
     *
     *  @param  relationshipId a relationship <code> Id </code>
     *  @param  match <code> true </code> for a positive match, <code> false
     *          </code> for a negative match
     *  @throws org.osid.NullArgumentException <code> relationshipId </code>
     *          is <code> null </code>
     */

    @OSID @Override
    public void matchRelationshipId(org.osid.id.Id relationshipId, boolean match) {
        this.relationshipIdTerms.add(getTermFactory().createIdTerm(relationshipId, match));
        return;
    }


    /**
     *  Clears all relationship <code> Id </code> terms.
     */

    @OSID @Override
    public void clearRelationshipIdTerms() {
        this.relationshipIdTerms.clear();
        return;
    }


    /**
     *  Gets all the relationship Id query terms.
     *
     *  @return a collection of the relationship Id query terms
     */

    protected java.util.Collection<org.osid.search.terms.IdTerm> getRelationshipIdTerms() {
        return (java.util.Collections.unmodifiableCollection(this.relationshipIdTerms));
    }


    /**
     *  Tests if a <code> RelationshipQuery </code> is available.
     *
     *  @return <code> true </code> if a relationship query is available,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean supportsRelationshipQuery() {
        return (false);
    }


    /**
     *  Gets the query for relationship. Multiple retrievals produce a nested
     *  <code> OR </code> term.
     *
     *  @return the relationship query
     *  @throws org.osid.UnimplementedException <code>
     *          supportsRelationshipQuery() </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.relationship.RelationshipQuery getRelationshipQuery() {
        throw new org.osid.UnimplementedException("supportsRelationshipQuery() is false");
    }


    /**
     *  Matches an object that has any <code> Relationship </code>.
     *
     *  @param  match <code> true </code> to match any state, <code> false
     *          </code> to match objects with no states
     */

    @OSID @Override
    public void matchAnyRelationship(boolean match) {
        this.relationshipIdTerms.add(getTermFactory().createIdWildcardTerm(match));
        return;
    }


    /**
     *  Clears all relationship terms.
     */

    @OSID @Override
    public void clearRelationshipTerms() {
        clearWildcardTerms(relationshipIdTerms);
        return;
    }


    /**
     *  Matches an object that has a relationship to the given peer <code> Id.
     *  </code>
     *
     *  @param  peerId a relationship peer <code> Id </code>
     *  @param  match <code> true </code> for a positive match, <code> false
     *          </code> for a negative match
     *  @throws org.osid.NullArgumentException <code> peerId </code> is <code>
     *          null </code>
     */

    @OSID @Override
    public void matchRelationshipPeerId(org.osid.id.Id peerId, boolean match) {
        this.relationshipIdTerms.add(getTermFactory().createIdTerm(peerId, match));
        return;
    }


    /**
     * Clears all relationship <code> Id </code> terms.
     */

    @OSID @Override
    public void clearRelationshipPeerIdTerms() {
        this.relationshipPeerIdTerms.clear();
        return;
    }


    /**
     *  Gets all the relationship peer Id query terms.
     *
     *  @return a collection of the relationship peer Id query terms
     */

    protected java.util.Collection<org.osid.search.terms.IdTerm> getRelationshipPeerIdTerms() {
        return (java.util.Collections.unmodifiableCollection(this.relationshipPeerIdTerms));
    }


    /**
     *  Sets a <code> Type </code> for querying objects having records 
     *  implementing a given record type. 
     *
     *  @param  recordType a record type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> recordType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRecordType(org.osid.type.Type recordType, boolean match) {
        this.query.matchRecordType(recordType, match);
        return;
    }


    /**
     *  Matches an object that has any record. 
     *
     *  @param  match <code> true </code> to match any record, <code> false 
     *          </code> to match objects with no records 
     */

    @OSID @Override
    public void matchAnyRecord(boolean match) {
        this.query.matchAnyRecord(match);
        return;
    }

    
    /**
     *  Clears all record <code> Type </code> terms. 
     */
    
    @OSID @Override
    public void clearRecordTerms() {
        this.query.clearRecordTerms();
        return;
    }

    
    /**
     *  Gets all the record type query terms.
     *
     *  @return a collection of the record type query terms
     */

    protected java.util.Collection<org.osid.search.terms.TypeTerm> getRecordTypeTerms() {
        return (this.query.getRecordTypeTerms());
    }


    /**
     *  Gets the record types available in this object.
     *
     *  @return the record types
     */

    @OSID @Override
    public org.osid.type.TypeList getRecordTypes() {
        return (this.query.getRecordTypes());
    }


    /**
     *  Tests if this object supports the given record <code>
     *  Type. </code>
     *
     *  @param  recordType a type 
     *  @return <code> false </code>
     *  @throws org.osid.NullArgumentException <code> recordType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type recordType) {
        return (this.query.hasRecordType(recordType));
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    protected void addRecordType(org.osid.type.Type recordType) {
        this.query.addRecordType(recordType);
        return;
    }


    protected class OsidBrowsableQuery
        extends AbstractOsidBrowsableQuery
        implements org.osid.OsidBrowsableQuery {


        /**
         *  Constructs a new <code>OsidBrowsableQuery</code>.
         *
         *  @param factory the term factory
         *  @throws org.osid.NullArgumentException <code>factory</code> is
         *          <code>null</code>
         */

        protected OsidBrowsableQuery(net.okapia.osid.jamocha.query.TermFactory factory) {
            super(factory);
            return;
        }


        /**
         *  Gets all the record type query terms.
         *
         *  @return a collection of the record type query terms
         */

        protected java.util.Collection<org.osid.search.terms.TypeTerm> getRecordTypeTerms() {
            return (super.getRecordTypeTerms());
        }


        /**
         *  Adds a record type.
         *
         *  @param recordType
         *  @throws org.osid.NullArgumentException <code>recordType</code>
         *          is <code>null</code>
         */

        @Override
        protected void addRecordType(org.osid.type.Type recordType) {
            super.addRecordType(recordType);
            return;
        }    
    }
}