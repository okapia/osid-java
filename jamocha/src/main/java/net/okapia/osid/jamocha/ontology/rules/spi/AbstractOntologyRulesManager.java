//
// AbstractOntologyRulesManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ontology.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractOntologyRulesManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.ontology.rules.OntologyRulesManager,
               org.osid.ontology.rules.OntologyRulesProxyManager {

    private final Types relevancyEnablerRecordTypes        = new TypeRefSet();
    private final Types relevancyEnablerSearchRecordTypes  = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractOntologyRulesManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractOntologyRulesManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if looking up relevancy enablers is supported. 
     *
     *  @return <code> true </code> if relevancy enabler lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelevancyEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying relevancy enablers is supported. 
     *
     *  @return <code> true </code> if relevancy enabler query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelevancyEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching relevancy enablers is supported. 
     *
     *  @return <code> true </code> if relevancy enabler search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelevancyEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if a relevancy enabler administrative service is supported. 
     *
     *  @return <code> true </code> if relevancy enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelevancyEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if a relevancy enabler notification service is supported. 
     *
     *  @return <code> true </code> if relevancy enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelevancyEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if a relevancy enabler ontology lookup service is supported. 
     *
     *  @return <code> true </code> if a relevancy enabler ontology lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelevancyEnablerOntology() {
        return (false);
    }


    /**
     *  Tests if a relevancy enabler ontology service is supported. 
     *
     *  @return <code> true </code> if relevancy enabler ontology assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelevancyEnablerOntologyAssignment() {
        return (false);
    }


    /**
     *  Tests if a relevancy enabler ontology lookup service is supported. 
     *
     *  @return <code> true </code> if a relevancy enabler ontology service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelevancyEnablerSmartOntology() {
        return (false);
    }


    /**
     *  Tests if a relevancy enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a enabler relevancy rule lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelevancyEnablerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if a relevancy enabler rule application service is supported. 
     *
     *  @return <code> true </code> if relevancy enabler rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelevancyEnablerRuleApplication() {
        return (false);
    }


    /**
     *  Gets the supported <code> RelevancyEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> RelevancyEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRelevancyEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.relevancyEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> RelevancyEnabler </code> record type is 
     *  supported. 
     *
     *  @param  relevancyEnablerRecordType a <code> Type </code> indicating a 
     *          <code> RelevancyEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          relevancyEnablerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRelevancyEnablerRecordType(org.osid.type.Type relevancyEnablerRecordType) {
        return (this.relevancyEnablerRecordTypes.contains(relevancyEnablerRecordType));
    }


    /**
     *  Adds support for a relevancy enabler record type.
     *
     *  @param relevancyEnablerRecordType a relevancy enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>relevancyEnablerRecordType</code> is <code>null</code>
     */

    protected void addRelevancyEnablerRecordType(org.osid.type.Type relevancyEnablerRecordType) {
        this.relevancyEnablerRecordTypes.add(relevancyEnablerRecordType);
        return;
    }


    /**
     *  Removes support for a relevancy enabler record type.
     *
     *  @param relevancyEnablerRecordType a relevancy enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>relevancyEnablerRecordType</code> is <code>null</code>
     */

    protected void removeRelevancyEnablerRecordType(org.osid.type.Type relevancyEnablerRecordType) {
        this.relevancyEnablerRecordTypes.remove(relevancyEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> RelevancyEnabler </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> RelevancyEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRelevancyEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.relevancyEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> RelevancyEnabler </code> search record type 
     *  is supported. 
     *
     *  @param  relevancyEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> RelevancyEnabler </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          relevancyEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsRelevancyEnablerSearchRecordType(org.osid.type.Type relevancyEnablerSearchRecordType) {
        return (this.relevancyEnablerSearchRecordTypes.contains(relevancyEnablerSearchRecordType));
    }


    /**
     *  Adds support for a relevancy enabler search record type.
     *
     *  @param relevancyEnablerSearchRecordType a relevancy enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>relevancyEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addRelevancyEnablerSearchRecordType(org.osid.type.Type relevancyEnablerSearchRecordType) {
        this.relevancyEnablerSearchRecordTypes.add(relevancyEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a relevancy enabler search record type.
     *
     *  @param relevancyEnablerSearchRecordType a relevancy enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>relevancyEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removeRelevancyEnablerSearchRecordType(org.osid.type.Type relevancyEnablerSearchRecordType) {
        this.relevancyEnablerSearchRecordTypes.remove(relevancyEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  enabler lookup service. 
     *
     *  @return a <code> RelevancyEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerLookupSession getRelevancyEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.rules.OntologyRulesManager.getRelevancyEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerLookupSession getRelevancyEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.rules.OntologyRulesProxyManager.getRelevancyEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  enabler lookup service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the <code> Ontology 
     *          </code> 
     *  @return a <code> RelevancyEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Ontology </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerLookupSession getRelevancyEnablerLookupSessionForOntology(org.osid.id.Id ontologyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.rules.OntologyRulesManager.getRelevancyEnablerLookupSessionForOntology not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  enabler lookup service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the <code> Ontology 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Ontology </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> ontologyId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerLookupSession getRelevancyEnablerLookupSessionForOntology(org.osid.id.Id ontologyId, 
                                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.rules.OntologyRulesProxyManager.getRelevancyEnablerLookupSessionForOntology not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  enabler query service. 
     *
     *  @return a <code> RelevancyEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerQuerySession getRelevancyEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.rules.OntologyRulesManager.getRelevancyEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerQuerySession getRelevancyEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.rules.OntologyRulesProxyManager.getRelevancyEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  enabler query service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the <code> Ontology 
     *          </code> 
     *  @return a <code> RelevancyEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Ontology </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerQuerySession getRelevancyEnablerQuerySessionForOntology(org.osid.id.Id ontologyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.rules.OntologyRulesManager.getRelevancyEnablerQuerySessionForOntology not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  enabler query service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the <code> Ontology 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Ontology </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> ontologyId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerQuerySession getRelevancyEnablerQuerySessionForOntology(org.osid.id.Id ontologyId, 
                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.rules.OntologyRulesProxyManager.getRelevancyEnablerQuerySessionForOntology not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  enabler search service. 
     *
     *  @return a <code> RelevancyEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerSearchSession getRelevancyEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.rules.OntologyRulesManager.getRelevancyEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerSearchSession getRelevancyEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.rules.OntologyRulesProxyManager.getRelevancyEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  enablers earch service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the <code> Ontology 
     *          </code> 
     *  @return a <code> RelevancyEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Ontology </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerSearchSession getRelevancyEnablerSearchSessionForOntology(org.osid.id.Id ontologyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.rules.OntologyRulesManager.getRelevancyEnablerSearchSessionForOntology not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  enablers earch service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the <code> Ontology 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Ontology </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> ontologyId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerSearchSession getRelevancyEnablerSearchSessionForOntology(org.osid.id.Id ontologyId, 
                                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.rules.OntologyRulesProxyManager.getRelevancyEnablerSearchSessionForOntology not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  enabler administration service. 
     *
     *  @return a <code> RelevancyEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerAdminSession getRelevancyEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.rules.OntologyRulesManager.getRelevancyEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerAdminSession getRelevancyEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.rules.OntologyRulesProxyManager.getRelevancyEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  enabler administration service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the <code> Ontology 
     *          </code> 
     *  @return a <code> RelevancyEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Ontology </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerAdminSession getRelevancyEnablerAdminSessionForOntology(org.osid.id.Id ontologyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.rules.OntologyRulesManager.getRelevancyEnablerAdminSessionForOntology not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  enabler administration service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the <code> Ontology 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Ontology </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> ontologyId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerAdminSession getRelevancyEnablerAdminSessionForOntology(org.osid.id.Id ontologyId, 
                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.rules.OntologyRulesProxyManager.getRelevancyEnablerAdminSessionForOntology not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  enabler notification service. 
     *
     *  @param  relevancyEnablerReceiver the notification callback 
     *  @return a <code> RelevancyEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> relevancyEnablerReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerNotificationSession getRelevancyEnablerNotificationSession(org.osid.ontology.rules.RelevancyEnablerReceiver relevancyEnablerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.rules.OntologyRulesManager.getRelevancyEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  enabler notification service. 
     *
     *  @param  relevancyEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> relevancyEnablerReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerNotificationSession getRelevancyEnablerNotificationSession(org.osid.ontology.rules.RelevancyEnablerReceiver relevancyEnablerReceiver, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.rules.OntologyRulesProxyManager.getRelevancyEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  enabler notification service for the given ontology. 
     *
     *  @param  relevancyEnablerReceiver the notification callback 
     *  @param  ontologyId the <code> Id </code> of the <code> Ontology 
     *          </code> 
     *  @return a <code> RelevancyEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no ontology found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> relevancyEnablerReceiver 
     *          </code> or <code> ontologyId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerNotificationSession getRelevancyEnablerNotificationSessionForOntology(org.osid.ontology.rules.RelevancyEnablerReceiver relevancyEnablerReceiver, 
                                                                                                                         org.osid.id.Id ontologyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.rules.OntologyRulesManager.getRelevancyEnablerNotificationSessionForOntology not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  enabler notification service for the given ontology. 
     *
     *  @param  relevancyEnablerReceiver the notification callback 
     *  @param  ontologyId the <code> Id </code> of the <code> Ontology 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no ontology found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          relevancyEnablerReceiver, ontologyId </code> or <code> proxy 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerNotificationSession getRelevancyEnablerNotificationSessionForOntology(org.osid.ontology.rules.RelevancyEnablerReceiver relevancyEnablerReceiver, 
                                                                                                                         org.osid.id.Id ontologyId, 
                                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.rules.OntologyRulesProxyManager.getRelevancyEnablerNotificationSessionForOntology not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup relevancy 
     *  enabler/ontology mappings for relevancy enablers. 
     *
     *  @return a <code> RelevancyEnablerOntologySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerOntology() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerOntologySession getRelevancyEnablerOntologySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.rules.OntologyRulesManager.getRelevancyEnablerOntologySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup relevancy 
     *  enabler/ontology mappings for relevancy enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyEnablerOntologySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerOntology() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerOntologySession getRelevancyEnablerOntologySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.rules.OntologyRulesProxyManager.getRelevancyEnablerOntologySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  relevancy enablers to ontologies for relevancy. 
     *
     *  @return a <code> RelevancyEnablerOntologyAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerOntologyAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerOntologyAssignmentSession getRelevancyEnablerOntologyAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.rules.OntologyRulesManager.getRelevancyEnablerOntologyAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  relevancy enablers to ontologies for relevancy. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyEnablerOntologyAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerOntologyAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerOntologyAssignmentSession getRelevancyEnablerOntologyAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.rules.OntologyRulesProxyManager.getRelevancyEnablerOntologyAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage relevancy enabler smart 
     *  ontologies. 
     *
     *  @param  ontologyId the <code> Id </code> of the <code> Ontology 
     *          </code> 
     *  @return a <code> RelevancyEnablerSmartOntologySession </code> 
     *  @throws org.osid.NotFoundException no <code> Ontology </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerSmartOntology() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerSmartOntologySession getRelevancyEnablerSmartOntologySession(org.osid.id.Id ontologyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.rules.OntologyRulesManager.getRelevancyEnablerSmartOntologySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage relevancy enabler smart 
     *  ontologies. 
     *
     *  @param  ontologyId the <code> Id </code> of the <code> Ontology 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyEnablerSmartOntologySession </code> 
     *  @throws org.osid.NotFoundException no <code> Ontology </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerSmartOntology() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerSmartOntologySession getRelevancyEnablerSmartOntologySession(org.osid.id.Id ontologyId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.rules.OntologyRulesProxyManager.getRelevancyEnablerSmartOntologySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  enabler mapping lookup service. 
     *
     *  @return a <code> RelevancyEnablerRuleSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerRuleLookupSession getRelevancyEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.rules.OntologyRulesManager.getRelevancyEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  enabler relevancy mapping lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerRuleLookupSession getRelevancyEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.rules.OntologyRulesProxyManager.getRelevancyEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  enabler mapping lookup service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the <code> Ontology 
     *          </code> 
     *  @return a <code> RelevancyEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Ontology </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerRuleLookupSession getRelevancyEnablerRuleLookupSessionForOntology(org.osid.id.Id ontologyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.rules.OntologyRulesManager.getRelevancyEnablerRuleLookupSessionForOntology not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  enabler relevancy mapping lookup service. 
     *
     *  @param  ontologyId the <code> Id </code> of the <code> Ontology 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Ontology </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerRuleLookupSession getRelevancyEnablerRuleLookupSessionForOntology(org.osid.id.Id ontologyId, 
                                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.rules.OntologyRulesProxyManager.getRelevancyEnablerRuleLookupSessionForOntology not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  enabler assignment service. 
     *
     *  @return a <code> RelevancyEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerRuleApplicationSession getRelevancyEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.rules.OntologyRulesManager.getRelevancyEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  enabler assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerRuleApplicationSession getRelevancyEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.rules.OntologyRulesProxyManager.getRelevancyEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  enabler assignment service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the <code> Ontology 
     *          </code> 
     *  @return a <code> RelevancyEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Ontology </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerRuleApplicationSession getRelevancyEnablerRuleApplicationSessionForOntology(org.osid.id.Id ontologyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.rules.OntologyRulesManager.getRelevancyEnablerRuleApplicationSessionForOntology not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relevancy 
     *  enabler assignment service. 
     *
     *  @param  ontologyId the <code> Id </code> of the <code> Ontology 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Ontology </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerRuleApplicationSession getRelevancyEnablerRuleApplicationSessionForOntology(org.osid.id.Id ontologyId, 
                                                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.rules.OntologyRulesProxyManager.getRelevancyEnablerRuleApplicationSessionForOntology not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.relevancyEnablerRecordTypes.clear();
        this.relevancyEnablerRecordTypes.clear();

        this.relevancyEnablerSearchRecordTypes.clear();
        this.relevancyEnablerSearchRecordTypes.clear();

        return;
    }
}
