//
// AbstractCanonicalUnitEnablerQueryInspector.java
//
//     A template for making a CanonicalUnitEnablerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.rules.canonicalunitenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for canonical unit enablers.
 */

public abstract class AbstractCanonicalUnitEnablerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQueryInspector
    implements org.osid.offering.rules.CanonicalUnitEnablerQueryInspector {

    private final java.util.Collection<org.osid.offering.rules.records.CanonicalUnitEnablerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the offering constrainer <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledOfferingConstrainerIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the offering constrainer query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerQueryInspector[] getRuledOfferingConstrainerTerms() {
        return (new org.osid.offering.rules.OfferingConstrainerQueryInspector[0]);
    }


    /**
     *  Gets the catalogue <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCatalogueIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the catalogue query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.offering.CatalogueQueryInspector[] getCatalogueTerms() {
        return (new org.osid.offering.CatalogueQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given canonical unit enabler query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a canonical unit enabler implementing the requested record.
     *
     *  @param canonicalUnitEnablerRecordType a canonical unit enabler record type
     *  @return the canonical unit enabler query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(canonicalUnitEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.rules.records.CanonicalUnitEnablerQueryInspectorRecord getCanonicalUnitEnablerQueryInspectorRecord(org.osid.type.Type canonicalUnitEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.rules.records.CanonicalUnitEnablerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(canonicalUnitEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(canonicalUnitEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this canonical unit enabler query. 
     *
     *  @param canonicalUnitEnablerQueryInspectorRecord canonical unit enabler query inspector
     *         record
     *  @param canonicalUnitEnablerRecordType canonicalUnitEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCanonicalUnitEnablerQueryInspectorRecord(org.osid.offering.rules.records.CanonicalUnitEnablerQueryInspectorRecord canonicalUnitEnablerQueryInspectorRecord, 
                                                   org.osid.type.Type canonicalUnitEnablerRecordType) {

        addRecordType(canonicalUnitEnablerRecordType);
        nullarg(canonicalUnitEnablerRecordType, "canonical unit enabler record type");
        this.records.add(canonicalUnitEnablerQueryInspectorRecord);        
        return;
    }
}
