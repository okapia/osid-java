//
// AbstractRouteSegmentSearchOdrer.java
//
//     Defines a RouteSegmentSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.route.routesegment.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code RouteSegmentSearchOrder}.
 */

public abstract class AbstractRouteSegmentSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectSearchOrder
    implements org.osid.mapping.route.RouteSegmentSearchOrder {

    private final java.util.Collection<org.osid.mapping.route.records.RouteSegmentSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for oredering the results by starting 
     *  instructions. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStartingInstructions(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for oredering the results by ending 
     *  instructions. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByEndingInstructions(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for oredering the results by distance. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDistance(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for oredering the results by estimated travel 
     *  time. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByETA(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for oredering the results by path. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRoute(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a search order for the route is available. 
     *
     *  @return <code> true </code> if a route search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRouteSearchOrder() {
        return (false);
    }


    /**
     *  Gets a route search order. 
     *
     *  @return the path search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRouteSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteSearchOrder getRouteSearchOrder() {
        throw new org.osid.UnimplementedException("supportsRouteSearchOrder() is false");
    }


    /**
     *  Specifies a preference for oredering the results by path. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPath(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a search order for the path is available. 
     *
     *  @return <code> true </code> if a path search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathSearchOrder() {
        return (false);
    }


    /**
     *  Gets a path search order. 
     *
     *  @return the path search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPathSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathSearchOrder getPathSearchOrder() {
        throw new org.osid.UnimplementedException("supportsPathSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  routeSegmentRecordType a route segment record type 
     *  @return {@code true} if the routeSegmentRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code routeSegmentRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type routeSegmentRecordType) {
        for (org.osid.mapping.route.records.RouteSegmentSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(routeSegmentRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  routeSegmentRecordType the route segment record type 
     *  @return the route segment search order record
     *  @throws org.osid.NullArgumentException
     *          {@code routeSegmentRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(routeSegmentRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.mapping.route.records.RouteSegmentSearchOrderRecord getRouteSegmentSearchOrderRecord(org.osid.type.Type routeSegmentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.route.records.RouteSegmentSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(routeSegmentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(routeSegmentRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this route segment. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param routeSegmentRecord the route segment search odrer record
     *  @param routeSegmentRecordType route segment record type
     *  @throws org.osid.NullArgumentException
     *          {@code routeSegmentRecord} or
     *          {@code routeSegmentRecordTyperouteSegment} is
     *          {@code null}
     */
            
    protected void addRouteSegmentRecord(org.osid.mapping.route.records.RouteSegmentSearchOrderRecord routeSegmentSearchOrderRecord, 
                                     org.osid.type.Type routeSegmentRecordType) {

        addRecordType(routeSegmentRecordType);
        this.records.add(routeSegmentSearchOrderRecord);
        
        return;
    }
}
