//
// AbstractMapSearch.java
//
//     A template for making a Map Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.map.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing map searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractMapSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.mapping.MapSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.mapping.records.MapSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.mapping.MapSearchOrder mapSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of maps. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  mapIds list of maps
     *  @throws org.osid.NullArgumentException
     *          <code>mapIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongMaps(org.osid.id.IdList mapIds) {
        while (mapIds.hasNext()) {
            try {
                this.ids.add(mapIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongMaps</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of map Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getMapIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  mapSearchOrder map search order 
     *  @throws org.osid.NullArgumentException
     *          <code>mapSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>mapSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderMapResults(org.osid.mapping.MapSearchOrder mapSearchOrder) {
	this.mapSearchOrder = mapSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.mapping.MapSearchOrder getMapSearchOrder() {
	return (this.mapSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given map search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a map implementing the requested record.
     *
     *  @param mapSearchRecordType a map search record
     *         type
     *  @return the map search record
     *  @throws org.osid.NullArgumentException
     *          <code>mapSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(mapSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.records.MapSearchRecord getMapSearchRecord(org.osid.type.Type mapSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.mapping.records.MapSearchRecord record : this.records) {
            if (record.implementsRecordType(mapSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(mapSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this map search. 
     *
     *  @param mapSearchRecord map search record
     *  @param mapSearchRecordType map search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addMapSearchRecord(org.osid.mapping.records.MapSearchRecord mapSearchRecord, 
                                           org.osid.type.Type mapSearchRecordType) {

        addRecordType(mapSearchRecordType);
        this.records.add(mapSearchRecord);        
        return;
    }
}
