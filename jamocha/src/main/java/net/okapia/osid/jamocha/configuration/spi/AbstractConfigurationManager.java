//
// AbstractConfigurationManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.configuration.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractConfigurationManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.configuration.ConfigurationManager,
               org.osid.configuration.ConfigurationProxyManager {

    private final Types valueConditionRecordTypes          = new TypeRefSet();
    private final Types valueRecordTypes                   = new TypeRefSet();
    private final Types valueSearchRecordTypes             = new TypeRefSet();

    private final Types parameterRecordTypes               = new TypeRefSet();
    private final Types parameterSearchRecordTypes         = new TypeRefSet();

    private final Types configurationRecordTypes           = new TypeRefSet();
    private final Types configurationSearchRecordTypes     = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractConfigurationManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractConfigurationManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible for this service. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests for the availability of a configuration value retrieval service. 
     *
     *  @return <code> true </code> if value retrieval is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsValueRetrieval() {
        return (false);
    }


    /**
     *  Tests for the availability of a configuration value lookup service. 
     *
     *  @return <code> true </code> if value lookup is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsValueLookup() {
        return (false);
    }


    /**
     *  Tests for the availability of a configuration value query service. 
     *
     *  @return <code> true </code> if value query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsValueQuery() {
        return (false);
    }


    /**
     *  Tests for the availability of a configuration value search service. 
     *
     *  @return <code> true </code> if value search is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsValueSearch() {
        return (false);
    }


    /**
     *  Tests for the availability of a configuration value administration 
     *  service. 
     *
     *  @return <code> true </code> if value administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsValueAdmin() {
        return (false);
    }


    /**
     *  Tests for the availability of a configuration value notification 
     *  service. 
     *
     *  @return <code> true </code> if value notification is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsValueNotification() {
        return (false);
    }


    /**
     *  Tests for the availability of a parameter lookup service. 
     *
     *  @return <code> true </code> if parameter lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterLookup() {
        return (false);
    }


    /**
     *  Tests for the availability of a parameter query service. 
     *
     *  @return <code> true </code> if parameter query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterQuery() {
        return (false);
    }


    /**
     *  Tests for the availability of a parameter search service. 
     *
     *  @return <code> true </code> if parameter search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterSearch() {
        return (false);
    }


    /**
     *  Tests for the availability of a parameter update service. 
     *
     *  @return <code> true </code> if parameter update is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterAdmin() {
        return (false);
    }


    /**
     *  Tests for the availability of a parameter notification service. 
     *
     *  @return <code> true </code> if parameter notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterNotification() {
        return (false);
    }


    /**
     *  Tests for the availability of a service to lookup mappings of 
     *  parameters to configurations. 
     *
     *  @return <code> true </code> if parameter configuration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterConfiguration() {
        return (false);
    }


    /**
     *  Tests for the availability of a service to map parameters to 
     *  configurations. 
     *
     *  @return <code> true </code> if parameter configuration assignment is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterConfigurationAssignment() {
        return (false);
    }


    /**
     *  Tests for the availability of a parameter smart configuration service. 
     *
     *  @return <code> true </code> if parameter smart configuration service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterSmartConfiguration() {
        return (false);
    }


    /**
     *  Tests for the availability of a configuration lookup service. 
     *
     *  @return <code> true </code> if configuration lookup is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConfigurationLookup() {
        return (false);
    }


    /**
     *  Tests for the availability of a configuration query service. 
     *
     *  @return <code> true </code> if configuration query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConfigurationQuery() {
        return (false);
    }


    /**
     *  Tests for the availability of a configuration search service. 
     *
     *  @return <code> true </code> if configuration search is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConfigurationSearch() {
        return (false);
    }


    /**
     *  Tests for the availability of a configuration admin service. 
     *
     *  @return <code> true </code> if configuration admin is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConfigurationAdmin() {
        return (false);
    }


    /**
     *  Tests for the availability of a notification service for subscribing 
     *  to changes to configurations. 
     *
     *  @return <code> true </code> if a configuration notification service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConfigurationNotification() {
        return (false);
    }


    /**
     *  Tests for the availability of a configuration hierarchy traversal 
     *  service. 
     *
     *  @return <code> true </code> if a configuration hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConfigurationHierarchy() {
        return (false);
    }


    /**
     *  Tests for the availability of a configuration hierarchy design 
     *  service. 
     *
     *  @return <code> true </code> if a configuration hierarchy design is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConfigurationHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests for the availability of a configuration batch service. 
     *
     *  @return <code> true </code> if a configuration batch service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConfigurationBatch() {
        return (false);
    }


    /**
     *  Tests for the availability of a configuration rules service. 
     *
     *  @return <code> true </code> if a configuration rules service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConfigurationRules() {
        return (false);
    }


    /**
     *  Gets the supported value condition record types. 
     *
     *  @return a list containing the supported <code> ValueCondition </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getValueConditionRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.valueConditionRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> ValueCondition </code> record type is 
     *  supported. 
     *
     *  @param  valueConditionRecordType a <code> Type </code> indicating a 
     *          <code> ValueCondition </code> record type 
     *  @return <code> true </code> if the given value condition record <code> 
     *          Type </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> valueConditionRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsValueConditionRecordType(org.osid.type.Type valueConditionRecordType) {
        return (this.valueConditionRecordTypes.contains(valueConditionRecordType));
    }


    /**
     *  Adds support for a value condition record type.
     *
     *  @param valueConditionRecordType a value condition record type
     *  @throws org.osid.NullArgumentException
     *  <code>valueConditionRecordType</code> is <code>null</code>
     */

    protected void addValueConditionRecordType(org.osid.type.Type valueConditionRecordType) {
        this.valueConditionRecordTypes.add(valueConditionRecordType);
        return;
    }


    /**
     *  Removes support for a value condition record type.
     *
     *  @param valueConditionRecordType a value condition record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>valueConditionRecordType</code> is <code>null</code>
     */

    protected void removeValueConditionRecordType(org.osid.type.Type valueConditionRecordType) {
        this.valueConditionRecordTypes.remove(valueConditionRecordType);
        return;
    }


    /**
     *  Gets all the value record types supported. 
     *
     *  @return the list of supported value record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getValueRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.valueRecordTypes.toCollection()));
    }


    /**
     *  Tests if a given value record type is supported. 
     *
     *  @param  valueRecordType the value record type 
     *  @return <code> true </code> if the value record type is supported 
     *          <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> valueRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsValueRecordType(org.osid.type.Type valueRecordType) {
        return (this.valueRecordTypes.contains(valueRecordType));
    }


    /**
     *  Adds support for a value record type.
     *
     *  @param valueRecordType a value record type
     *  @throws org.osid.NullArgumentException
     *  <code>valueRecordType</code> is <code>null</code>
     */

    protected void addValueRecordType(org.osid.type.Type valueRecordType) {
        this.valueRecordTypes.add(valueRecordType);
        return;
    }


    /**
     *  Removes support for a value record type.
     *
     *  @param valueRecordType a value record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>valueRecordType</code> is <code>null</code>
     */

    protected void removeValueRecordType(org.osid.type.Type valueRecordType) {
        this.valueRecordTypes.remove(valueRecordType);
        return;
    }


    /**
     *  Gets all the value search record types supported. 
     *
     *  @return the list of supported value search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getValueSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.valueSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if a given value search type is supported. 
     *
     *  @param  valueSearchRecordType the value search record type 
     *  @return <code> true </code> if the value search record type is 
     *          supported <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> valueSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsValueSearchRecordType(org.osid.type.Type valueSearchRecordType) {
        return (this.valueSearchRecordTypes.contains(valueSearchRecordType));
    }


    /**
     *  Adds support for a value search record type.
     *
     *  @param valueSearchRecordType a value search record type
     *  @throws org.osid.NullArgumentException
     *  <code>valueSearchRecordType</code> is <code>null</code>
     */

    protected void addValueSearchRecordType(org.osid.type.Type valueSearchRecordType) {
        this.valueSearchRecordTypes.add(valueSearchRecordType);
        return;
    }


    /**
     *  Removes support for a value search record type.
     *
     *  @param valueSearchRecordType a value search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>valueSearchRecordType</code> is <code>null</code>
     */

    protected void removeValueSearchRecordType(org.osid.type.Type valueSearchRecordType) {
        this.valueSearchRecordTypes.remove(valueSearchRecordType);
        return;
    }


    /**
     *  Gets all the parameter record types supported. 
     *
     *  @return the list of supported parameter record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getParameterRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.parameterRecordTypes.toCollection()));
    }


    /**
     *  Tests if a given parameter record type is supported. 
     *
     *  @param  parameterRecordType a parameter record type 
     *  @return <code> true </code> if the parameter record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> parameterRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsParameterRecordType(org.osid.type.Type parameterRecordType) {
        return (this.parameterRecordTypes.contains(parameterRecordType));
    }


    /**
     *  Adds support for a parameter record type.
     *
     *  @param parameterRecordType a parameter record type
     *  @throws org.osid.NullArgumentException
     *  <code>parameterRecordType</code> is <code>null</code>
     */

    protected void addParameterRecordType(org.osid.type.Type parameterRecordType) {
        this.parameterRecordTypes.add(parameterRecordType);
        return;
    }


    /**
     *  Removes support for a parameter record type.
     *
     *  @param parameterRecordType a parameter record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>parameterRecordType</code> is <code>null</code>
     */

    protected void removeParameterRecordType(org.osid.type.Type parameterRecordType) {
        this.parameterRecordTypes.remove(parameterRecordType);
        return;
    }


    /**
     *  Gets all the parameter search record types supported. 
     *
     *  @return the list of supported parameter search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getParameterSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.parameterSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if a given parameter search record type is supported. 
     *
     *  @param  parameterSearchRecordType the value search type 
     *  @return <code> true </code> if the parameter search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          parameterSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsParameterSearchRecordType(org.osid.type.Type parameterSearchRecordType) {
        return (this.parameterSearchRecordTypes.contains(parameterSearchRecordType));
    }


    /**
     *  Adds support for a parameter search record type.
     *
     *  @param parameterSearchRecordType a parameter search record type
     *  @throws org.osid.NullArgumentException
     *  <code>parameterSearchRecordType</code> is <code>null</code>
     */

    protected void addParameterSearchRecordType(org.osid.type.Type parameterSearchRecordType) {
        this.parameterSearchRecordTypes.add(parameterSearchRecordType);
        return;
    }


    /**
     *  Removes support for a parameter search record type.
     *
     *  @param parameterSearchRecordType a parameter search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>parameterSearchRecordType</code> is <code>null</code>
     */

    protected void removeParameterSearchRecordType(org.osid.type.Type parameterSearchRecordType) {
        this.parameterSearchRecordTypes.remove(parameterSearchRecordType);
        return;
    }


    /**
     *  Gets all the configuration record types supported. 
     *
     *  @return the list of supported configuration record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getConfigurationRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.configurationRecordTypes.toCollection()));
    }


    /**
     *  Tests if a given configuration record type is supported. 
     *
     *  @param  configurationRecordType a configuration record type 
     *  @return <code> true </code> if the configuration record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> configurationRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsConfigurationRecordType(org.osid.type.Type configurationRecordType) {
        return (this.configurationRecordTypes.contains(configurationRecordType));
    }


    /**
     *  Adds support for a configuration record type.
     *
     *  @param configurationRecordType a configuration record type
     *  @throws org.osid.NullArgumentException
     *  <code>configurationRecordType</code> is <code>null</code>
     */

    protected void addConfigurationRecordType(org.osid.type.Type configurationRecordType) {
        this.configurationRecordTypes.add(configurationRecordType);
        return;
    }


    /**
     *  Removes support for a configuration record type.
     *
     *  @param configurationRecordType a configuration record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>configurationRecordType</code> is <code>null</code>
     */

    protected void removeConfigurationRecordType(org.osid.type.Type configurationRecordType) {
        this.configurationRecordTypes.remove(configurationRecordType);
        return;
    }


    /**
     *  Gets all the configuration search record types supported. 
     *
     *  @return the list of supported configuration search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getConfigurationSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.configurationSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if a given configuration search record type is supported. 
     *
     *  @param  configurationSearchRecordType the configuration search record 
     *          type 
     *  @return <code> true </code> if the configuration search record type is 
     *          support <code> e </code> d, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          configurationSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsConfigurationSearchRecordType(org.osid.type.Type configurationSearchRecordType) {
        return (this.configurationSearchRecordTypes.contains(configurationSearchRecordType));
    }


    /**
     *  Adds support for a configuration search record type.
     *
     *  @param configurationSearchRecordType a configuration search record type
     *  @throws org.osid.NullArgumentException
     *  <code>configurationSearchRecordType</code> is <code>null</code>
     */

    protected void addConfigurationSearchRecordType(org.osid.type.Type configurationSearchRecordType) {
        this.configurationSearchRecordTypes.add(configurationSearchRecordType);
        return;
    }


    /**
     *  Removes support for a configuration search record type.
     *
     *  @param configurationSearchRecordType a configuration search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>configurationSearchRecordType</code> is <code>null</code>
     */

    protected void removeConfigurationSearchRecordType(org.osid.type.Type configurationSearchRecordType) {
        this.configurationSearchRecordTypes.remove(configurationSearchRecordType);
        return;
    }


    /**
     *  Gets a configuration value retrieval session. 
     *
     *  @return a <code> ValueRetrievalSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueRetrieval() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ValueRetrievalSession getValueRetrievalSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationManager.getValueRetrievalSession not implemented");
    }


    /**
     *  Gets a configuration value retrieval session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ValueRetrievalSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueRetrieval() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ValueRetrievalSession getValueRetrievalSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationProxyManager.getValueRetrievalSession not implemented");
    }


    /**
     *  Gets a configuration value retrieval session using the supplied 
     *  configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> to use 
     *  @return a <code> ValueRetrievalSession </code> 
     *  @throws org.osid.NotFoundException <code> configurationId </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueRetrieval() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ValueRetrievalSession getValueRetrievalSessionForConfiguration(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationManager.getValueRetrievalSessionForConfiguration not implemented");
    }


    /**
     *  Gets a configuration value retrieval session using the supplied 
     *  configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> to use 
     *  @param  proxy a proxy 
     *  @return a <code> ValueRetrievalSession </code> 
     *  @throws org.osid.NotFoundException <code> configurationId </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueRetrieval() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> False </code> 
     */

    @OSID @Override
    public org.osid.configuration.ValueRetrievalSession getValueRetrievalSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationProxyManager.getValueRetrievalSessionForConfiguration not implemented");
    }


    /**
     *  Gets a configuration value lookup session. 
     *
     *  @return a <code> ValueLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsValueLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ValueLookupSession getValueLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationManager.getValueLookupSession not implemented");
    }


    /**
     *  Gets a configuration value lookup session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ValueLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsValueLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ValueLookupSession getValueLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationProxyManager.getValueLookupSession not implemented");
    }


    /**
     *  Gets a configuration value lookup session using the supplied 
     *  configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> to use 
     *  @return a <code> ValueLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> configurationId </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsValueLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ValueLookupSession getValueLookupSessionForConfiguration(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationManager.getValueLookupSessionForConfiguration not implemented");
    }


    /**
     *  Gets a configuration value lookup session using the supplied 
     *  configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> to use 
     *  @param  proxy a proxy 
     *  @return a <code> ValueLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> configurationId </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsValueLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> False </code> 
     */

    @OSID @Override
    public org.osid.configuration.ValueLookupSession getValueLookupSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationProxyManager.getValueLookupSessionForConfiguration not implemented");
    }


    /**
     *  Gets a configuration value query session. 
     *
     *  @return a <code> ValueQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsValueQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ValueQuerySession getValueQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationManager.getValueQuerySession not implemented");
    }


    /**
     *  Gets a configuration value query session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ValueQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsValueQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ValueQuerySession getValueQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationProxyManager.getValueQuerySession not implemented");
    }


    /**
     *  Gets a configuration value query session using the supplied 
     *  configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> to use 
     *  @return a <code> ValueQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> configurationId </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsValueQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ValueQuerySession getValueQuerySessionForConfiguration(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationManager.getValueQuerySessionForConfiguration not implemented");
    }


    /**
     *  Gets a configuration value query session using the supplied 
     *  configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> to use 
     *  @param  proxy a proxy 
     *  @return a <code> ValueQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> configurationId </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsValueQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> False </code> 
     */

    @OSID @Override
    public org.osid.configuration.ValueQuerySession getValueQuerySessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationProxyManager.getValueQuerySessionForConfiguration not implemented");
    }


    /**
     *  Gets a configuration value search session 
     *
     *  @return a <code> ValueSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsValueSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ValueSearchSession getValueSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationManager.getValueSearchSession not implemented");
    }


    /**
     *  Gets a configuration value search session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ValueSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsValueSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ValueSearchSession getValueSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationProxyManager.getValueSearchSession not implemented");
    }


    /**
     *  Gets a configuration value search session using the supplied 
     *  configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> to use 
     *  @return a <code> ValueSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> configurationId </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsValueSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ValueSearchSession getValueSearchSessionForConfiguration(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationManager.getValueSearchSessionForConfiguration not implemented");
    }


    /**
     *  Gets a configuration value search session using the supplied 
     *  configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> to use 
     *  @param  proxy a proxy 
     *  @return a <code> ValueSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> configurationId </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsValueSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> False </code> 
     */

    @OSID @Override
    public org.osid.configuration.ValueSearchSession getValueSearchSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationProxyManager.getValueSearchSessionForConfiguration not implemented");
    }


    /**
     *  Gets a configuration value administration session. 
     *
     *  @return a <code> ValueAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsValueAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ValueAdminSession getValueAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationManager.getValueAdminSession not implemented");
    }


    /**
     *  Gets a configuration value administration session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ValueAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsValueAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ValueAdminSession getValueAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationProxyManager.getValueAdminSession not implemented");
    }


    /**
     *  Gets a value administration session using the supplied configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> to use 
     *  @return a <code> ValueAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> configurationId </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> supportsValueAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.UnimplementedException <code> supportsValueAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ValueAdminSession getValueAdminSessionForConfiguration(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationManager.getValueAdminSessionForConfiguration not implemented");
    }


    /**
     *  Gets a value administration session using the supplied configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> to use 
     *  @param  proxy a proxy 
     *  @return a <code> ValueAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> configurationId </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsValueSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ValueAdminSession getValueAdminSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationProxyManager.getValueAdminSessionForConfiguration not implemented");
    }


    /**
     *  Gets a value notification session. 
     *
     *  @param  valueReceiver the notification callback 
     *  @return a <code> ValueNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> valueReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ValueNotificationSession getValueNotificationSession(org.osid.configuration.ValueReceiver valueReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationManager.getValueNotificationSession not implemented");
    }


    /**
     *  Gets a value notification session. 
     *
     *  @param  valueReceiver notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> ValueNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> valueReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ValueNotificationSession getValueNotificationSession(org.osid.configuration.ValueReceiver valueReceiver, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationProxyManager.getValueNotificationSession not implemented");
    }


    /**
     *  Gets a value notification session using the specified configuration 
     *
     *  @param  valueReceiver the notification callback 
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> to use 
     *  @return a <code> ValueNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> configurationId </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> valueReceiver </code> or 
     *          <code> configurationId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ValueNotificationSession getValueNotificationSessionForConfiguration(org.osid.configuration.ValueReceiver valueReceiver, 
                                                                                                       org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationManager.getValueNotificationSessionForConfiguration not implemented");
    }


    /**
     *  Gets a value notification session using the specified configuration 
     *
     *  @param  valueReceiver notification callback 
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> to use 
     *  @param  proxy a proxy 
     *  @return a <code> ValueNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> configurationId </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> valueReceiver, 
     *          configurationId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ValueNotificationSession getValueNotificationSessionForConfiguration(org.osid.configuration.ValueReceiver valueReceiver, 
                                                                                                       org.osid.id.Id configurationId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationProxyManager.getValueNotificationSessionForConfiguration not implemented");
    }


    /**
     *  Gets a parameter lookup session 
     *
     *  @return a <code> ParameterLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterLookupSession getParameterLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationManager.getParameterLookupSession not implemented");
    }


    /**
     *  Gets a parameter lookup session 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParameterLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterLookupSession getParameterLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationProxyManager.getParameterLookupSession not implemented");
    }


    /**
     *  Gets a parameter lookup session using the supplied configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> to use 
     *  @return a <code> ParamaterLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> configurationId </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterLookupSession getParameterLookupSessionForConfiguration(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationManager.getParameterLookupSessionForConfiguration not implemented");
    }


    /**
     *  Gets a parameter lookup session using the supplied configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> to use 
     *  @param  proxy a proxy 
     *  @return a <code> ParamaterLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> configurationId </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.configuration.ParameterLookupSession getParameterLookupSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationProxyManager.getParameterLookupSessionForConfiguration not implemented");
    }


    /**
     *  Gets a parameter query session 
     *
     *  @return a <code> ParameterQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterQuerySession getParameterQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationManager.getParameterQuerySession not implemented");
    }


    /**
     *  Gets a parameter query session 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParameterQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterQuerySession getParameterQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationProxyManager.getParameterQuerySession not implemented");
    }


    /**
     *  Gets a parameter search session using the supplied configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> to use 
     *  @return a <code> ParamaterQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> configurationId </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterQuerySession getParameterQuerySessionForConfiguration(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationManager.getParameterQuerySessionForConfiguration not implemented");
    }


    /**
     *  Gets a parameter query session using the supplied configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> to use 
     *  @param  proxy a proxy 
     *  @return a <code> ParamaterQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> configurationId </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.configuration.ParameterQuerySession getParameterQuerySessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationProxyManager.getParameterQuerySessionForConfiguration not implemented");
    }


    /**
     *  Gets a parameter search session 
     *
     *  @return a <code> ParameterSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterSearchSession getParameterSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationManager.getParameterSearchSession not implemented");
    }


    /**
     *  Gets a parameter search session 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParameterSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterSearchSession getParameterSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationProxyManager.getParameterSearchSession not implemented");
    }


    /**
     *  Gets a parameter search session using the supplied configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> to use 
     *  @return a <code> ParamaterSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> configurationId </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterSearchSession getParameterSearchSessionForConfiguration(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationManager.getParameterSearchSessionForConfiguration not implemented");
    }


    /**
     *  Gets a parameter search session using the supplied configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> to use 
     *  @param  proxy a proxy 
     *  @return a <code> ParamaterSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> configurationId </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterSearchSession getParameterSearchSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationProxyManager.getParameterSearchSessionForConfiguration not implemented");
    }


    /**
     *  Gets a parameter administration session. 
     *
     *  @return a <code> ParameterAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterAdminSession getParameterAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationManager.getParameterAdminSession not implemented");
    }


    /**
     *  Gets a parameter administration session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParameterAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterAdminSession getParameterAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationProxyManager.getParameterAdminSession not implemented");
    }


    /**
     *  Gets a parameter administration session using the supplied 
     *  configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> to use 
     *  @return a <code> ParameterAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> configurationId </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterAdminSession getParameterAdminSessionForConfiguration(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationManager.getParameterAdminSessionForConfiguration not implemented");
    }


    /**
     *  Gets a parameter administration session using the supplied 
     *  configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> to use 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> configurationId </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterAdminSession getParameterAdminSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationProxyManager.getParameterAdminSessionForConfiguration not implemented");
    }


    /**
     *  Gets a parameter notification session. 
     *
     *  @param  parameterReceiver the notification callback 
     *  @return a <code> ParameterNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> parameterReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterNotificationSession getParameterNotificationSession(org.osid.configuration.ParameterReceiver parameterReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationManager.getParameterNotificationSession not implemented");
    }


    /**
     *  Gets a parameter notification session. 
     *
     *  @param  parameterReceiver notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> parameterReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterNotificationSession getParameterNotificationSession(org.osid.configuration.ParameterReceiver parameterReceiver, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationProxyManager.getParameterNotificationSession not implemented");
    }


    /**
     *  Gets a parameter notification session using the specified 
     *  configuration. 
     *
     *  @param  parameterReceiver the notification callback 
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> to use 
     *  @return a <code> ParameterNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> configurationId </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> parameterReceiver 
     *          </code> or <code> configurationId </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterNotificationSession getParameterNotificationSessionForConfiguration(org.osid.configuration.ParameterReceiver parameterReceiver, 
                                                                                                               org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationManager.getParameterNotificationSessionForConfiguration not implemented");
    }


    /**
     *  Gets a parameter notification session using the specified 
     *  configuration. 
     *
     *  @param  parameterReceiver notification callback 
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> to use 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> registryId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> parameterReceiver, 
     *          configurationId, </code> or <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterNotificationSession getParameterNotificationSessionForConfiguration(org.osid.configuration.ParameterReceiver parameterReceiver, 
                                                                                                               org.osid.id.Id configurationId, 
                                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationProxyManager.getParameterNotificationSessionForConfiguration not implemented");
    }


    /**
     *  Gets a session for looking up mappings of parameters to 
     *  configurations. 
     *
     *  @return a <code> ParameterConfigurationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterConfiguration() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterConfigurationSession getParameterConfigurationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationManager.getParameterConfigurationSession not implemented");
    }


    /**
     *  Gets a session for examining mappings of parameters to configurations. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParameterConfigurationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterConfiguration() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterConfigurationSession getParameterConfigurationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationProxyManager.getParameterConfigurationSession not implemented");
    }


    /**
     *  Gets a session for managing mappings of parameters to configurations. 
     *
     *  @return a <code> ParameterConfigurationAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterConfigurationAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterConfigurationAssignmentSession getParameterConfigurationAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationManager.getParameterConfigurationAssignmentSession not implemented");
    }


    /**
     *  Gets a session for managing mappings of parameters to configurations. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParameterConfigurationAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterConfigurationAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterConfigurationAssignmentSession getParameterConfigurationAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationProxyManager.getParameterConfigurationAssignmentSession not implemented");
    }


    /**
     *  Gets a session for managing smart configurations. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> to use 
     *  @return a <code> ParameterSmartConfigurationSession </code> 
     *  @throws org.osid.NotFoundException <code> configurationId </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterSmartConfiguration() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterSmartConfigurationSession getParameterSmartConfigurationSession(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationManager.getParameterSmartConfigurationSession not implemented");
    }


    /**
     *  Gets a session for managing smart configurations of parameters. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> to use 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterSmartConfigurationSession </code> 
     *  @throws org.osid.NotFoundException <code> configurationId </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> configuratinId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterSmartConfiguration() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterSmartConfigurationSession getParameterSmartConfigurationSession(org.osid.id.Id configurationId, 
                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationProxyManager.getParameterSmartConfigurationSession not implemented");
    }


    /**
     *  Gets a configuration lookup session. 
     *
     *  @return a <code> ConfigurationLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConfigurationLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationLookupSession getConfigurationLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationManager.getConfigurationLookupSession not implemented");
    }


    /**
     *  Gets a configuration lookup session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ConfigurationLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConfigurationLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationLookupSession getConfigurationLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationProxyManager.getConfigurationLookupSession not implemented");
    }


    /**
     *  Gets a configuration query session. 
     *
     *  @return a <code> ConfigurationQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConfigurationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationQuerySession getConfigurationQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationManager.getConfigurationQuerySession not implemented");
    }


    /**
     *  Gets a configuration query session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ConfigurationQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConfigurationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationQuerySession getConfigurationQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationProxyManager.getConfigurationQuerySession not implemented");
    }


    /**
     *  Gets a configuration search session. 
     *
     *  @return a <code> ConfigurationSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConfigurationSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationSearchSession getConfigurationSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationManager.getConfigurationSearchSession not implemented");
    }


    /**
     *  Gets a configuration search session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ConfigurationSearchSession </code> 
     *  @throws org.osid.OperationFailedException <code> proxy </code> is 
     *          <code> null </code> 
     *  @throws org.osid.NullArgumentException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConfigurationSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationSearchSession getConfigurationSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationProxyManager.getConfigurationSearchSession not implemented");
    }


    /**
     *  Gets a configuration administration session. 
     *
     *  @return a <code> ConfigurationAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConfigurationAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationAdminSession getConfigurationAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationManager.getConfigurationAdminSession not implemented");
    }


    /**
     *  Gets a configuration administration session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ConfigurationAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConfigurationAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationAdminSession getConfigurationAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationProxyManager.getConfigurationAdminSession not implemented");
    }


    /**
     *  Gets the notification session for subscribing to changes to 
     *  configurations. 
     *
     *  @param  configurationReceiver the notification callback 
     *  @return a <code> ConfigurationNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> configurationReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConfigurationNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationNotificationSession getConfigurationNotificationSession(org.osid.configuration.ConfigurationReceiver configurationReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationManager.getConfigurationNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for subscribing to changes to 
     *  configurations. 
     *
     *  @param  configurationReceiver notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> ConfigurationNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> configurationReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConfigurationNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationNotificationSession getConfigurationNotificationSession(org.osid.configuration.ConfigurationReceiver configurationReceiver, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationProxyManager.getConfigurationNotificationSession not implemented");
    }


    /**
     *  Gets a hierarchy traversal service for configurations. 
     *
     *  @return a <code> ConfigurationHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConfigurationHierarchy() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationHierarchySession getConfigurationHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationManager.getConfigurationHierarchySession not implemented");
    }


    /**
     *  Gets a hierarchy traversal service for configurations. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ConfiguraqtionHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConfigurationHierarchy() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationHierarchySession getConfigurationHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationProxyManager.getConfigurationHierarchySession not implemented");
    }


    /**
     *  Gets a hierarchy design service for configurations. 
     *
     *  @return a <code> ConfigurationHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConfigurationHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationHierarchyDesignSession getConfigurationHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationManager.getConfigurationHierarchyDesignSession not implemented");
    }


    /**
     *  Gets a hierarchy design service for configurations. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ConfigurationHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConfigurationHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationHierarchyDesignSession getConfigurationHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationProxyManager.getConfigurationHierarchyDesignSession not implemented");
    }


    /**
     *  Gets a <code> ConfigurationBatchManager. </code> 
     *
     *  @return a <code> ConfigurationBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConfigurationBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.batch.ConfigurationBatchManager getConfigurationBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationManager.getConfigurationBatchManager not implemented");
    }


    /**
     *  Gets a <code> ConfigurationProxyManager. </code> 
     *
     *  @return a <code> ConfigurationBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConfigurationBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.batch.ConfigurationBatchProxyManager getConfigurationBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationProxyManager.getConfigurationBatchProxyManager not implemented");
    }


    /**
     *  Gets a <code> ConfigurationRulesManager. </code> 
     *
     *  @return a <code> ConfigurationRulesManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConfigurationRules() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ConfigurationRulesManager getConfigurationRulesManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationManager.getConfigurationRulesManager not implemented");
    }


    /**
     *  Gets a <code> ConfigurationProxyManager. </code> 
     *
     *  @return a <code> ConfigurationRulesProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConfigurationRules() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ConfigurationRulesProxyManager getConfigurationRulesProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.configuration.ConfigurationProxyManager.getConfigurationRulesProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.valueConditionRecordTypes.clear();
        this.valueConditionRecordTypes.clear();

        this.valueRecordTypes.clear();
        this.valueRecordTypes.clear();

        this.valueSearchRecordTypes.clear();
        this.valueSearchRecordTypes.clear();

        this.parameterRecordTypes.clear();
        this.parameterRecordTypes.clear();

        this.parameterSearchRecordTypes.clear();
        this.parameterSearchRecordTypes.clear();

        this.configurationRecordTypes.clear();
        this.configurationRecordTypes.clear();

        this.configurationSearchRecordTypes.clear();
        this.configurationSearchRecordTypes.clear();

        return;
    }
}
