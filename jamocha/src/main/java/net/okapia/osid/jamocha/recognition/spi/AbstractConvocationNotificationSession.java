//
// AbstractConvocationNotificationSession.java
//
//     A template for making ConvocationNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recognition.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Convocation} objects. This session is
 *  intended for consumers needing to synchronize their state with
 *  this service without the use of polling. Notifications are
 *  cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Convocation} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for convocation entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractConvocationNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.recognition.ConvocationNotificationSession {

    private boolean federated = false;
    private org.osid.recognition.Academy academy = new net.okapia.osid.jamocha.nil.recognition.academy.UnknownAcademy();


    /**
     *  Gets the {@code Academy/code> {@code Id} associated with this
     *  session.
     *
     *  @return the {@code Academy Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getAcademyId() {
        return (this.academy.getId());
    }

    
    /**
     *  Gets the {@code Academy} associated with this session.
     *
     *  @return the {@code Academy} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.Academy getAcademy()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.academy);
    }


    /**
     *  Sets the {@code Academy}.
     *
     *  @param academy the academy for this session
     *  @throws org.osid.NullArgumentException {@code academy}
     *          is {@code null}
     */

    protected void setAcademy(org.osid.recognition.Academy academy) {
        nullarg(academy, "academy");
        this.academy = academy;
        return;
    }


    /**
     *  Tests if this user can register for {@code Convocation}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForConvocationNotifications() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include convocations in academies which are children
     *  of this academy in the academy hierarchy.
     */

    @OSID @Override
    public void useFederatedAcademyView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this academy only.
     */

    @OSID @Override
    public void useIsolatedAcademyView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new convocations. {@code
     *  ConvocationReceiver.newConvocation()} is invoked when a new
     *  {@code Convocation} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewConvocations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of updated convocations. {@code
     *  ConvocationReceiver.changedConvocation()} is invoked when a
     *  convocation is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedConvocations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated convocation. {@code
     *  ConvocationReceiver.changedConvocation()} is invoked when the
     *  specified convocation is changed.
     *
     *  @param convocationId the {@code Id} of the {@code Convocation} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code convocationId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedConvocation(org.osid.id.Id convocationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted convocations. {@code
     *  ConvocationReceiver.deletedConvocation()} is invoked when a
     *  convocation is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedConvocations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted convocation. {@code
     *  ConvocationReceiver.deletedConvocation()} is invoked when the
     *  specified convocation is deleted.
     *
     *  @param convocationId the {@code Id} of the
     *          {@code Convocation} to monitor
     *  @throws org.osid.NullArgumentException {@code convocationId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedConvocation(org.osid.id.Id convocationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
