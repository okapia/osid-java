//
// AbstractMutableScheduleSlot.java
//
//     Defines a mutable ScheduleSlot.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.calendaring.scheduleslot.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>ScheduleSlot</code>.
 */

public abstract class AbstractMutableScheduleSlot
    extends net.okapia.osid.jamocha.calendaring.scheduleslot.spi.AbstractScheduleSlot
    implements org.osid.calendaring.ScheduleSlot,
               net.okapia.osid.jamocha.builder.calendaring.scheduleslot.ScheduleSlotMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Sets the sequestered flag.
     *
     *  @param sequestered <code> true </code> if this containable is
     *         sequestered, <code> false </code> if this containable
     *         may appear outside its aggregate
     */

    @Override
    public void setSequestered(boolean sequestered) {
        super.setSequestered(sequestered);
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this schedule slot. 
     *
     *  @param record schedule slot record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addScheduleSlotRecord(org.osid.calendaring.records.ScheduleSlotRecord record, org.osid.type.Type recordType) {
        super.addScheduleSlotRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the display name for this schedule slot.
     *
     *  @param displayName the name for this schedule slot
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this schedule slot.
     *
     *  @param description the description of this schedule slot
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Adds a schedule slot.
     *
     *  @param scheduleSlot a schedule slot
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleSlot</code> is <code>null</code>
     */

    @Override
    public void addScheduleSlot(org.osid.calendaring.ScheduleSlot scheduleSlot) {
        super.addScheduleSlot(scheduleSlot);
        return;
    }


    /**
     *  Sets all the schedule slots.
     *
     *  @param scheduleSlots a collection of schedule slots
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleSlots</code> is <code>null</code>
     */

    @Override
    public void setScheduleSlots(java.util.Collection<org.osid.calendaring.ScheduleSlot> scheduleSlots) {
        super.setScheduleSlots(scheduleSlots);
        return;
    }


    /**
     *  Adds a weekday.
     *
     *  @param weekday a weekday
     */

    @Override
    public void addWeekday(long weekday) {
        super.addWeekday(weekday);
        return;
    }


    /**
     *  Sets the weekdays.
     *
     *  @param weekdays a set of weekdays
     *  @throws org.osid.NullArgumentException <code>weekdays</code>
     *          is <code>null</code>
     */

    @Override
    public void setWeekdays(java.util.Collection<Long> weekdays) {
        super.setWeekdays(weekdays);
        return;
    }


    /**
     *  Sets the weekly interval.
     *
     *  @param interval a weekly interval
     *  @throws org.osid.NullArgumentException <code>interval</code>
     *          is <code>null</code>
     */

    @Override
    public void setWeeklyInterval(long interval) {
        super.setWeeklyInterval(interval);
        return;
    }


   /**
     *  Sets the week of month.
     *
     *  @param month a week of month
     *  @throws org.osid.NullArgumentException <code>month</code> is
     *          <code>null</code>
     */

    @Override
    public void setWeekOfMonth(long month) {
        super.setWeekOfMonth(month);
        return;
    }


    /**
     *  Sets the weekday time.
     *
     *  @param time a weekday time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    @Override
    public void setWeekdayTime(org.osid.calendaring.Time time) {
        super.setWeekdayTime(time);
        return;
    }


    /**
     *  Sets the fixed interval.
     *
     *  @param interval a fixed interval
     *  @throws org.osid.NullArgumentException <code>interval</code>
     *          is <code>null</code>
     */

    @Override
    public void setFixedInterval(org.osid.calendaring.Duration interval) {
        super.setFixedInterval(interval);
        return;
    }


    /**
     *  Sets the duration.
     *
     *  @param duration the duration
     *  @throws org.osid.NullArgumentException <code>duration</code> is
     *          <code>null</code>
     */

    @Override
    public void setDuration(org.osid.calendaring.Duration duration) {
        super.setDuration(duration);
        return;
    }
}

