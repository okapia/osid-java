//
// AbstractParticipantQuery.java
//
//     A template for making a Participant Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.participant.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for participants.
 */

public abstract class AbstractParticipantQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.offering.ParticipantQuery {

    private final java.util.Collection<org.osid.offering.records.ParticipantQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets an offering <code> Id. </code> 
     *
     *  @param  offeringId an offering <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> offeringId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOfferingId(org.osid.id.Id offeringId, boolean match) {
        return;
    }


    /**
     *  Clears all offering <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearOfferingIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> OfferingQuery </code> is available. 
     *
     *  @return <code> true </code> if an offering query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingQuery() {
        return (false);
    }


    /**
     *  Gets the query for an offering query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the offering query 
     *  @throws org.osid.UnimplementedException <code> supportsOfferingQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingQuery getOfferingQuery() {
        throw new org.osid.UnimplementedException("supportsOfferingQuery() is false");
    }


    /**
     *  Clears all offering terms. 
     */

    @OSID @Override
    public void clearOfferingTerms() {
        return;
    }


    /**
     *  Sets a resource <code> Id. </code> 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResourceId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears all resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearResourceIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResourceQuery() {
        throw new org.osid.UnimplementedException("supportsResourceQuery() is false");
    }


    /**
     *  Clears all resource terms. 
     */

    @OSID @Override
    public void clearResourceTerms() {
        return;
    }


    /**
     *  Sets the time period <code> Id </code> for this query to match 
     *  participants that have a related term. 
     *
     *  @param  timePeriodId a time period <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> timePeriodId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchTimePeriodId(org.osid.id.Id timePeriodId, boolean match) {
        return;
    }


    /**
     *  Clears the time period <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearTimePeriodIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> TimePeriodQuery </code> is available. 
     *
     *  @return <code> true </code> if a time period query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTimePeriodQuery() {
        return (false);
    }


    /**
     *  Gets the query for a time period. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the time period query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodQuery getTimePeriodQuery() {
        throw new org.osid.UnimplementedException("supportsTimePeriodQuery() is false");
    }


    /**
     *  Clears the time period terms. 
     */

    @OSID @Override
    public void clearTimePeriodTerms() {
        return;
    }


    /**
     *  Sets the grade system <code> Id </code> for this query. 
     *
     *  @param  gradeSystemId a grade system <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeSystemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResultOptionId(org.osid.id.Id gradeSystemId, 
                                    boolean match) {
        return;
    }


    /**
     *  Clears the grade system <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearResultOptionIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradeSystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade system query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResultOptionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grading option. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a grade system query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResultOptionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQuery getResultOptionQuery() {
        throw new org.osid.UnimplementedException("supportsResultOptionQuery() is false");
    }


    /**
     *  Matches participants that have any grading option. 
     *
     *  @param  match <code> true </code> to match participants with any 
     *          grading option, <code> false </code> to match participants 
     *          with no grading options 
     */

    @OSID @Override
    public void matchAnyResultOption(boolean match) {
        return;
    }


    /**
     *  Clears the grading option terms. 
     */

    @OSID @Override
    public void clearResultOptionTerms() {
        return;
    }


    /**
     *  Sets the catalogue <code> Id </code> for this query to match 
     *  participants assigned to catalogues. 
     *
     *  @param  catalogueId a catalogue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCatalogueId(org.osid.id.Id catalogueId, boolean match) {
        return;
    }


    /**
     *  Clears all catalogue <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCatalogueIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CatalogueQuery </code> is available. 
     *
     *  @return <code> true </code> if a catalogue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a catalogue query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the catalogue query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogueQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CatalogueQuery getCatalogueQuery() {
        throw new org.osid.UnimplementedException("supportsCatalogueQuery() is false");
    }


    /**
     *  Clears all catalogue terms. 
     */

    @OSID @Override
    public void clearCatalogueTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given participant query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a participant implementing the requested record.
     *
     *  @param participantRecordType a participant record type
     *  @return the participant query record
     *  @throws org.osid.NullArgumentException
     *          <code>participantRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(participantRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.records.ParticipantQueryRecord getParticipantQueryRecord(org.osid.type.Type participantRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.records.ParticipantQueryRecord record : this.records) {
            if (record.implementsRecordType(participantRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(participantRecordType + " is not supported");
    }


    /**
     *  Adds a record to this participant query. 
     *
     *  @param participantQueryRecord participant query record
     *  @param participantRecordType participant record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addParticipantQueryRecord(org.osid.offering.records.ParticipantQueryRecord participantQueryRecord, 
                                          org.osid.type.Type participantRecordType) {

        addRecordType(participantRecordType);
        nullarg(participantQueryRecord, "participant query record");
        this.records.add(participantQueryRecord);        
        return;
    }
}
