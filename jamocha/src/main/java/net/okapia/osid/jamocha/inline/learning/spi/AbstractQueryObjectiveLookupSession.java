//
// AbstractQueryObjectiveLookupSession.java
//
//    An inline adapter that maps an ObjectiveLookupSession to
//    an ObjectiveQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.learning.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an ObjectiveLookupSession to
 *  an ObjectiveQuerySession.
 */

public abstract class AbstractQueryObjectiveLookupSession
    extends net.okapia.osid.jamocha.learning.spi.AbstractObjectiveLookupSession
    implements org.osid.learning.ObjectiveLookupSession {

    private final org.osid.learning.ObjectiveQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryObjectiveLookupSession.
     *
     *  @param querySession the underlying objective query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryObjectiveLookupSession(org.osid.learning.ObjectiveQuerySession querySession) {
        nullarg(querySession, "objective query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>ObjectiveBank</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>ObjectiveBank Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getObjectiveBankId() {
        return (this.session.getObjectiveBankId());
    }


    /**
     *  Gets the <code>ObjectiveBank</code> associated with this 
     *  session.
     *
     *  @return the <code>ObjectiveBank</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBank getObjectiveBank()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getObjectiveBank());
    }


    /**
     *  Tests if this user can perform <code>Objective</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupObjectives() {
        return (this.session.canSearchObjectives());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include objectives in objective banks which are children
     *  of this objective bank in the objective bank hierarchy.
     */

    @OSID @Override
    public void useFederatedObjectiveBankView() {
        this.session.useFederatedObjectiveBankView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this objective bank only.
     */

    @OSID @Override
    public void useIsolatedObjectiveBankView() {
        this.session.useIsolatedObjectiveBankView();
        return;
    }
    
     
    /**
     *  Gets the <code>Objective</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Objective</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Objective</code> and
     *  retained for compatibility.
     *
     *  @param  objectiveId <code>Id</code> of the
     *          <code>Objective</code>
     *  @return the objective
     *  @throws org.osid.NotFoundException <code>objectiveId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>objectiveId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.Objective getObjective(org.osid.id.Id objectiveId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.learning.ObjectiveQuery query = getQuery();
        query.matchId(objectiveId, true);
        org.osid.learning.ObjectiveList objectives = this.session.getObjectivesByQuery(query);
        if (objectives.hasNext()) {
            return (objectives.getNextObjective());
        } 
        
        throw new org.osid.NotFoundException(objectiveId + " not found");
    }


    /**
     *  Gets an <code>ObjectiveList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  objectives specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Objectives</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  objectiveIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Objective</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveList getObjectivesByIds(org.osid.id.IdList objectiveIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.learning.ObjectiveQuery query = getQuery();

        try (org.osid.id.IdList ids = objectiveIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getObjectivesByQuery(query));
    }


    /**
     *  Gets an <code>ObjectiveList</code> corresponding to the given
     *  objective genus <code>Type</code> which does not include
     *  objectives of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  objectives or an error results. Otherwise, the returned list
     *  may contain only those objectives that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  objectiveGenusType an objective genus type 
     *  @return the returned <code>Objective</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveList getObjectivesByGenusType(org.osid.type.Type objectiveGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.learning.ObjectiveQuery query = getQuery();
        query.matchGenusType(objectiveGenusType, true);
        return (this.session.getObjectivesByQuery(query));
    }


    /**
     *  Gets an <code>ObjectiveList</code> corresponding to the given
     *  objective genus <code>Type</code> and include any additional
     *  objectives with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  objectives or an error results. Otherwise, the returned list
     *  may contain only those objectives that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  objectiveGenusType an objective genus type 
     *  @return the returned <code>Objective</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveList getObjectivesByParentGenusType(org.osid.type.Type objectiveGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.learning.ObjectiveQuery query = getQuery();
        query.matchParentGenusType(objectiveGenusType, true);
        return (this.session.getObjectivesByQuery(query));
    }


    /**
     *  Gets an <code>ObjectiveList</code> containing the given
     *  objective record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  objectives or an error results. Otherwise, the returned list
     *  may contain only those objectives that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  objectiveRecordType an objective record type 
     *  @return the returned <code>Objective</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveList getObjectivesByRecordType(org.osid.type.Type objectiveRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.learning.ObjectiveQuery query = getQuery();
        query.matchRecordType(objectiveRecordType, true);
        return (this.session.getObjectivesByQuery(query));
    }

    
    /**
     *  Gets all <code>Objectives</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  objectives or an error results. Otherwise, the returned list
     *  may contain only those objectives that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Objectives</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveList getObjectives()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.learning.ObjectiveQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getObjectivesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.learning.ObjectiveQuery getQuery() {
        org.osid.learning.ObjectiveQuery query = this.session.getObjectiveQuery();
        return (query);
    }
}
