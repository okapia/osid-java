//
// AbstractMapGradeEntryLookupSession
//
//    A simple framework for providing a GradeEntry lookup service
//    backed by a fixed collection of grade entries.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.grading.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a GradeEntry lookup service backed by a
 *  fixed collection of grade entries. The grade entries are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>GradeEntries</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapGradeEntryLookupSession
    extends net.okapia.osid.jamocha.grading.spi.AbstractGradeEntryLookupSession
    implements org.osid.grading.GradeEntryLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.grading.GradeEntry> gradeEntries = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.grading.GradeEntry>());


    /**
     *  Makes a <code>GradeEntry</code> available in this session.
     *
     *  @param  gradeEntry a grade entry
     *  @throws org.osid.NullArgumentException <code>gradeEntry<code>
     *          is <code>null</code>
     */

    protected void putGradeEntry(org.osid.grading.GradeEntry gradeEntry) {
        this.gradeEntries.put(gradeEntry.getId(), gradeEntry);
        return;
    }


    /**
     *  Makes an array of grade entries available in this session.
     *
     *  @param  gradeEntries an array of grade entries
     *  @throws org.osid.NullArgumentException <code>gradeEntries<code>
     *          is <code>null</code>
     */

    protected void putGradeEntries(org.osid.grading.GradeEntry[] gradeEntries) {
        putGradeEntries(java.util.Arrays.asList(gradeEntries));
        return;
    }


    /**
     *  Makes a collection of grade entries available in this session.
     *
     *  @param  gradeEntries a collection of grade entries
     *  @throws org.osid.NullArgumentException <code>gradeEntries<code>
     *          is <code>null</code>
     */

    protected void putGradeEntries(java.util.Collection<? extends org.osid.grading.GradeEntry> gradeEntries) {
        for (org.osid.grading.GradeEntry gradeEntry : gradeEntries) {
            this.gradeEntries.put(gradeEntry.getId(), gradeEntry);
        }

        return;
    }


    /**
     *  Removes a GradeEntry from this session.
     *
     *  @param  gradeEntryId the <code>Id</code> of the grade entry
     *  @throws org.osid.NullArgumentException <code>gradeEntryId<code> is
     *          <code>null</code>
     */

    protected void removeGradeEntry(org.osid.id.Id gradeEntryId) {
        this.gradeEntries.remove(gradeEntryId);
        return;
    }


    /**
     *  Gets the <code>GradeEntry</code> specified by its <code>Id</code>.
     *
     *  @param  gradeEntryId <code>Id</code> of the <code>GradeEntry</code>
     *  @return the gradeEntry
     *  @throws org.osid.NotFoundException <code>gradeEntryId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>gradeEntryId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeEntry getGradeEntry(org.osid.id.Id gradeEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.grading.GradeEntry gradeEntry = this.gradeEntries.get(gradeEntryId);
        if (gradeEntry == null) {
            throw new org.osid.NotFoundException("gradeEntry not found: " + gradeEntryId);
        }

        return (gradeEntry);
    }


    /**
     *  Gets all <code>GradeEntries</code>. In plenary mode, the returned
     *  list contains all known gradeEntries or an error
     *  results. Otherwise, the returned list may contain only those
     *  gradeEntries that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>GradeEntries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.grading.gradeentry.ArrayGradeEntryList(this.gradeEntries.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.gradeEntries.clear();
        super.close();
        return;
    }
}
