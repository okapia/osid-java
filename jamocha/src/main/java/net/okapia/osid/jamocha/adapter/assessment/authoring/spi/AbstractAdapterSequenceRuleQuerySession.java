//
// AbstractQuerySequenceRuleLookupSession.java
//
//    A SequenceRuleQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.assessment.authoring.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A SequenceRuleQuerySession adapter.
 */

public abstract class AbstractAdapterSequenceRuleQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.assessment.authoring.SequenceRuleQuerySession {

    private final org.osid.assessment.authoring.SequenceRuleQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterSequenceRuleQuerySession.
     *
     *  @param session the underlying sequence rule query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterSequenceRuleQuerySession(org.osid.assessment.authoring.SequenceRuleQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeBank</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeBank Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBankId() {
        return (this.session.getBankId());
    }


    /**
     *  Gets the {@codeBank</code> associated with this 
     *  session.
     *
     *  @return the {@codeBank</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.Bank getBank()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getBank());
    }


    /**
     *  Tests if this user can perform {@codeSequenceRule</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchSequenceRules() {
        return (this.session.canSearchSequenceRules());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include sequence rules in banks which are children
     *  of this bank in the bank hierarchy.
     */

    @OSID @Override
    public void useFederatedBankView() {
        this.session.useFederatedBankView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this bank only.
     */
    
    @OSID @Override
    public void useIsolatedBankView() {
        this.session.useIsolatedBankView();
        return;
    }
    
      
    /**
     *  Gets a sequence rule query. The returned query will not have an
     *  extension query.
     *
     *  @return the sequence rule query 
     */
      
    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleQuery getSequenceRuleQuery() {
        return (this.session.getSequenceRuleQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  sequenceRuleQuery the sequence rule query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code sequenceRuleQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code sequenceRuleQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleList getSequenceRulesByQuery(org.osid.assessment.authoring.SequenceRuleQuery sequenceRuleQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getSequenceRulesByQuery(sequenceRuleQuery));
    }
}
