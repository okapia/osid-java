//
// InvariantIndexedMapAuthorizationEnablerLookupSession
//
//    Implements an AuthorizationEnabler lookup service backed by a fixed
//    collection of authorizationEnablers indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authorization.rules;


/**
 *  Implements an AuthorizationEnabler lookup service backed by a fixed
 *  collection of authorization enablers. The authorization enablers are indexed by
 *  {@code Id}, genus and record types.
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some authorization enablers may be compatible
 *  with more types than are indicated through these authorization enabler
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapAuthorizationEnablerLookupSession
    extends net.okapia.osid.jamocha.core.authorization.rules.spi.AbstractIndexedMapAuthorizationEnablerLookupSession
    implements org.osid.authorization.rules.AuthorizationEnablerLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapAuthorizationEnablerLookupSession} using an
     *  array of authorizationEnablers.
     *
     *  @param vault the vault
     *  @param authorizationEnablers an array of authorization enablers
     *  @throws org.osid.NullArgumentException {@code vault},
     *          {@code authorizationEnablers} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapAuthorizationEnablerLookupSession(org.osid.authorization.Vault vault,
                                                    org.osid.authorization.rules.AuthorizationEnabler[] authorizationEnablers) {

        setVault(vault);
        putAuthorizationEnablers(authorizationEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapAuthorizationEnablerLookupSession} using a
     *  collection of authorization enablers.
     *
     *  @param vault the vault
     *  @param authorizationEnablers a collection of authorization enablers
     *  @throws org.osid.NullArgumentException {@code vault},
     *          {@code authorizationEnablers} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapAuthorizationEnablerLookupSession(org.osid.authorization.Vault vault,
                                                    java.util.Collection<? extends org.osid.authorization.rules.AuthorizationEnabler> authorizationEnablers) {

        setVault(vault);
        putAuthorizationEnablers(authorizationEnablers);
        return;
    }
}
