//
// AbstractFederatingQueueConstrainerLookupSession.java
//
//     An abstract federating adapter for a QueueConstrainerLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.tracking.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  QueueConstrainerLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingQueueConstrainerLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.tracking.rules.QueueConstrainerLookupSession>
    implements org.osid.tracking.rules.QueueConstrainerLookupSession {

    private boolean parallel = false;
    private org.osid.tracking.FrontOffice frontOffice = new net.okapia.osid.jamocha.nil.tracking.frontoffice.UnknownFrontOffice();


    /**
     *  Constructs a new <code>AbstractFederatingQueueConstrainerLookupSession</code>.
     */

    protected AbstractFederatingQueueConstrainerLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.tracking.rules.QueueConstrainerLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>FrontOffice/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>FrontOffice Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFrontOfficeId() {
        return (this.frontOffice.getId());
    }


    /**
     *  Gets the <code>FrontOffice</code> associated with this 
     *  session.
     *
     *  @return the <code>FrontOffice</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOffice getFrontOffice()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.frontOffice);
    }


    /**
     *  Sets the <code>FrontOffice</code>.
     *
     *  @param  frontOffice the front office for this session
     *  @throws org.osid.NullArgumentException <code>frontOffice</code>
     *          is <code>null</code>
     */

    protected void setFrontOffice(org.osid.tracking.FrontOffice frontOffice) {
        nullarg(frontOffice, "front office");
        this.frontOffice = frontOffice;
        return;
    }


    /**
     *  Tests if this user can perform <code>QueueConstrainer</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupQueueConstrainers() {
        for (org.osid.tracking.rules.QueueConstrainerLookupSession session : getSessions()) {
            if (session.canLookupQueueConstrainers()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>QueueConstrainer</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeQueueConstrainerView() {
        for (org.osid.tracking.rules.QueueConstrainerLookupSession session : getSessions()) {
            session.useComparativeQueueConstrainerView();
        }

        return;
    }


    /**
     *  A complete view of the <code>QueueConstrainer</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryQueueConstrainerView() {
        for (org.osid.tracking.rules.QueueConstrainerLookupSession session : getSessions()) {
            session.usePlenaryQueueConstrainerView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include queue constrainers in front offices which are children
     *  of this front office in the front office hierarchy.
     */

    @OSID @Override
    public void useFederatedFrontOfficeView() {
        for (org.osid.tracking.rules.QueueConstrainerLookupSession session : getSessions()) {
            session.useFederatedFrontOfficeView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this front office only.
     */

    @OSID @Override
    public void useIsolatedFrontOfficeView() {
        for (org.osid.tracking.rules.QueueConstrainerLookupSession session : getSessions()) {
            session.useIsolatedFrontOfficeView();
        }

        return;
    }


    /**
     *  Only active queue constrainers are returned by methods in this
     *  session.
     */
     
    @OSID @Override
    public void useActiveQueueConstrainerView() {
        for (org.osid.tracking.rules.QueueConstrainerLookupSession session : getSessions()) {
            session.useActiveQueueConstrainerView();
        }

        return;
    }


    /**
     *  Active and inactive queue constrainers are returned by methods
     *  in this session.
     */
    
    @OSID @Override
    public void useAnyStatusQueueConstrainerView() {
        for (org.osid.tracking.rules.QueueConstrainerLookupSession session : getSessions()) {
            session.useAnyStatusQueueConstrainerView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>QueueConstrainer</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>QueueConstrainer</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>QueueConstrainer</code> and
     *  retained for compatibility.
     *
     *  In active mode, queue constrainers are returned that are currently
     *  active. In any status mode, active and inactive queue constrainers
     *  are returned.
     *
     *  @param  queueConstrainerId <code>Id</code> of the
     *          <code>QueueConstrainer</code>
     *  @return the queue constrainer
     *  @throws org.osid.NotFoundException <code>queueConstrainerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>queueConstrainerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainer getQueueConstrainer(org.osid.id.Id queueConstrainerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.tracking.rules.QueueConstrainerLookupSession session : getSessions()) {
            try {
                return (session.getQueueConstrainer(queueConstrainerId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(queueConstrainerId + " not found");
    }


    /**
     *  Gets a <code>QueueConstrainerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  queueConstrainers specified in the <code>Id</code> list, in
     *  the order of the list, including duplicates, or an error
     *  results if an <code>Id</code> in the supplied list is not
     *  found or inaccessible. Otherwise, inaccessible
     *  <code>QueueConstrainers</code> may be omitted from the list
     *  and may present the elements in any order including returning
     *  a unique set.
     *
     *  In active mode, queue constrainers are returned that are
     *  currently active. In any status mode, active and inactive
     *  queue constrainers are returned.
     *
     *  @param  queueConstrainerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>QueueConstrainer</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerList getQueueConstrainersByIds(org.osid.id.IdList queueConstrainerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.tracking.rules.queueconstrainer.MutableQueueConstrainerList ret = new net.okapia.osid.jamocha.tracking.rules.queueconstrainer.MutableQueueConstrainerList();

        try (org.osid.id.IdList ids = queueConstrainerIds) {
            while (ids.hasNext()) {
                ret.addQueueConstrainer(getQueueConstrainer(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>QueueConstrainerList</code> corresponding to the
     *  given queue constrainer genus <code>Type</code> which does not
     *  include queue constrainers of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known queue
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those queue constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, queue constrainers are returned that are
     *  currently active. In any status mode, active and inactive
     *  queue constrainers are returned.
     *
     *  @param  queueConstrainerGenusType a queueConstrainer genus type 
     *  @return the returned <code>QueueConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerList getQueueConstrainersByGenusType(org.osid.type.Type queueConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.tracking.rules.queueconstrainer.FederatingQueueConstrainerList ret = getQueueConstrainerList();

        for (org.osid.tracking.rules.QueueConstrainerLookupSession session : getSessions()) {
            ret.addQueueConstrainerList(session.getQueueConstrainersByGenusType(queueConstrainerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>QueueConstrainerList</code> corresponding to the
     *  given queue constrainer genus <code>Type</code> and include
     *  any additional queue constrainers with genus types derived
     *  from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known queue
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those queue constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, queue constrainers are returned that are
     *  currently active. In any status mode, active and inactive
     *  queue constrainers are returned.
     *
     *  @param  queueConstrainerGenusType a queueConstrainer genus type 
     *  @return the returned <code>QueueConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerList getQueueConstrainersByParentGenusType(org.osid.type.Type queueConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.tracking.rules.queueconstrainer.FederatingQueueConstrainerList ret = getQueueConstrainerList();

        for (org.osid.tracking.rules.QueueConstrainerLookupSession session : getSessions()) {
            ret.addQueueConstrainerList(session.getQueueConstrainersByParentGenusType(queueConstrainerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>QueueConstrainerList</code> containing the given
     *  queue constrainer record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known queue
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those queue constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, queue constrainers are returned that are
     *  currently active. In any status mode, active and inactive
     *  queue constrainers are returned.
     *
     *  @param  queueConstrainerRecordType a queueConstrainer record type 
     *  @return the returned <code>QueueConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerList getQueueConstrainersByRecordType(org.osid.type.Type queueConstrainerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.tracking.rules.queueconstrainer.FederatingQueueConstrainerList ret = getQueueConstrainerList();

        for (org.osid.tracking.rules.QueueConstrainerLookupSession session : getSessions()) {
            ret.addQueueConstrainerList(session.getQueueConstrainersByRecordType(queueConstrainerRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>QueueConstrainers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  queue constrainers or an error results. Otherwise, the returned list
     *  may contain only those queue constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, queue constrainers are returned that are currently
     *  active. In any status mode, active and inactive queue constrainers
     *  are returned.
     *
     *  @return a list of <code>QueueConstrainers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerList getQueueConstrainers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.tracking.rules.queueconstrainer.FederatingQueueConstrainerList ret = getQueueConstrainerList();

        for (org.osid.tracking.rules.QueueConstrainerLookupSession session : getSessions()) {
            ret.addQueueConstrainerList(session.getQueueConstrainers());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.tracking.rules.queueconstrainer.FederatingQueueConstrainerList getQueueConstrainerList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.tracking.rules.queueconstrainer.ParallelQueueConstrainerList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.tracking.rules.queueconstrainer.CompositeQueueConstrainerList());
        }
    }
}
