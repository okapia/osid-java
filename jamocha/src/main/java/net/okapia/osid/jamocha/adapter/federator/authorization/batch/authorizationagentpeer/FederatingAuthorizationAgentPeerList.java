//
// FederatingAuthorizationAgentPeerList.java
//
//     Interface for federating lists.
//
//
// Tom Coppeto
// Okapia
// 17 September 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.authorization.batch.authorizationagentpeer;


/**
 *  Federating authorizationagentpeer lists.
 */

public interface FederatingAuthorizationAgentPeerList
    extends org.osid.authorization.batch.AuthorizationAgentPeerList {


    /**
     *  Adds an <code>AuthorizationAgentPeerList</code> to this
     *  <code>AuthorizationAgentPeerList</code> using the default buffer size.
     *
     *  @param authorizationAgentPeerList the <code>AuthorizationAgentPeerList</code> to add
     *  @throws org.osid.IllegalStateException this list already closed
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationAgentPeerList</code> is <code>null</code>
     */

    public void addAuthorizationAgentPeerList(org.osid.authorization.batch.AuthorizationAgentPeerList authorizationAgentPeerList);


    /**
     *  Adds a collection of <code>AuthorizationAgentPeerLists</code> to this
     *  <code>AuthorizationAgentPeerList</code>.
     *
     *  @param authorizationAgentPeerLists the <code>AuthorizationAgentPeerList</code> collection
     *         to add
     *  @throws org.osid.IllegalStateException this list already closed
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationAgentPeerLists</code> is <code>null</code>
     */

    public void addAuthorizationAgentPeerLists(java.util.Collection<org.osid.authorization.batch.AuthorizationAgentPeerList> authorizationAgentPeerLists);

        
    /**
     *  No more. Invoked when no more lists are to be added.
     */

    public void noMore();


    /**
     *  Tests if the list is operational. A list is operational if
     *  <code>close()</code> has not been invoked.
     *
     *  @return <code>true</code> if the list is operational,
     *          <code>false</code> otherwise.
     */

    public boolean isOperational();


    /**
     *  Tests if the list is empty.
     *
     *  @return <code>true</code> if the list is empty
     *          retrieved, <code>false</code> otherwise.
     */

    public boolean isEmpty();
}
