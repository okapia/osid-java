//
// AbstractQuerySubscriptionLookupSession.java
//
//    A SubscriptionQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.subscription.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A SubscriptionQuerySession adapter.
 */

public abstract class AbstractAdapterSubscriptionQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.subscription.SubscriptionQuerySession {

    private final org.osid.subscription.SubscriptionQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterSubscriptionQuerySession.
     *
     *  @param session the underlying subscription query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterSubscriptionQuerySession(org.osid.subscription.SubscriptionQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codePublisher</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codePublisher Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getPublisherId() {
        return (this.session.getPublisherId());
    }


    /**
     *  Gets the {@codePublisher</code> associated with this 
     *  session.
     *
     *  @return the {@codePublisher</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.Publisher getPublisher()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getPublisher());
    }


    /**
     *  Tests if this user can perform {@codeSubscription</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchSubscriptions() {
        return (this.session.canSearchSubscriptions());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include subscriptions in publishers which are children
     *  of this publisher in the publisher hierarchy.
     */

    @OSID @Override
    public void useFederatedPublisherView() {
        this.session.useFederatedPublisherView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this publisher only.
     */
    
    @OSID @Override
    public void useIsolatedPublisherView() {
        this.session.useIsolatedPublisherView();
        return;
    }
    
      
    /**
     *  Gets a subscription query. The returned query will not have an
     *  extension query.
     *
     *  @return the subscription query 
     */
      
    @OSID @Override
    public org.osid.subscription.SubscriptionQuery getSubscriptionQuery() {
        return (this.session.getSubscriptionQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  subscriptionQuery the subscription query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code subscriptionQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code subscriptionQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptionsByQuery(org.osid.subscription.SubscriptionQuery subscriptionQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getSubscriptionsByQuery(subscriptionQuery));
    }
}
