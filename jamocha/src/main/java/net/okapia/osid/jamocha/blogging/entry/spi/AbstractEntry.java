//
// AbstractEntry.java
//
//     Defines an Entry.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.blogging.entry.spi;

import org.osid.binding.java.annotation.OSID;

import net.okapia.osid.primordium.locale.text.eng.us.Plain;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Entry</code>.
 */

public abstract class AbstractEntry
    extends net.okapia.osid.jamocha.spi.AbstractSourceableOsidObject
    implements org.osid.blogging.Entry {

    private org.osid.calendaring.DateTime timestamp;
    private org.osid.resource.Resource poster;
    private org.osid.authentication.Agent postingAgent;
    private org.osid.locale.DisplayText subjectLine;
    private org.osid.locale.DisplayText summary;
    private org.osid.locale.DisplayText text;
    private org.osid.locale.DisplayText copyright = Plain.valueOf("");

    private final java.util.Collection<org.osid.blogging.records.EntryRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the time of this entry. 
     *
     *  @return the time 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getTimestamp() {
        return (this.timestamp);
    }


    /**
     *  Sets the timestamp.
     *
     *  @param timestamp a timestamp
     *  @throws org.osid.NullArgumentException <code>timestamp</code>
     *          is <code>null</code>
     */

    protected void setTimestamp(org.osid.calendaring.DateTime timestamp) {
        nullarg(timestamp, "timestamp");
        this.timestamp = timestamp;
        return;
    }


    /**
     *  Gets the poster <code> Id </code> of this entry. 
     *
     *  @return the poster resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getPosterId() {
        return (this.poster.getId());
    }


    /**
     *  Gets the poster of this entry. 
     *
     *  @return the poster resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getPoster()
        throws org.osid.OperationFailedException {

        return (this.poster);
    }


    /**
     *  Sets the poster.
     *
     *  @param poster a poster
     *  @throws org.osid.NullArgumentException <code>poster</code> is
     *          <code>null</code>
     */

    protected void setPoster(org.osid.resource.Resource poster) {
        nullarg(poster, "poster");
        this.poster = poster;
        return;
    }


    /**
     *  Gets the posting <code> Id </code> of this entry. 
     *
     *  @return the posting agent <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getPostingAgentId() {
        return (this.postingAgent.getId());
    }


    /**
     *  Gets the posting of this entry. 
     *
     *  @return the posting agent 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getPostingAgent()
        throws org.osid.OperationFailedException {

        return (this.postingAgent);
    }


    /**
     *  Sets the posting agent.
     *
     *  @param agent a posting agent
     *  @throws org.osid.NullArgumentException
     *          <code>agent</code> is <code>null</code>
     */

    protected void setPostingAgent(org.osid.authentication.Agent agent) {
        nullarg(agent, "agent");
        this.postingAgent = agent;
        return;
    }


    /**
     *  Gets the subject line of this entry. 
     *
     *  @return the subject 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getSubjectLine() {
        return (this.subjectLine);
    }


    /**
     *  Sets the subject line.
     *
     *  @param subjectLine a subject line
     *  @throws org.osid.NullArgumentException
     *          <code>subjectLine</code> is <code>null</code>
     */

    protected void setSubjectLine(org.osid.locale.DisplayText subjectLine) {
        nullarg(subjectLine, "subject line");
        this.subjectLine = subjectLine;
        return;
    }


    /**
     *  Gets the summary or excerpt of this entry. 
     *
     *  @return the summary 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getSummary() {
        return (this.summary);
    }


    /**
     *  Sets the summary.
     *
     *  @param summary a summary
     *  @throws org.osid.NullArgumentException <code>summary</code> is
     *          <code>null</code>
     */

    protected void setSummary(org.osid.locale.DisplayText summary) {
        nullarg(summary, "summary");
        this.summary = summary;
        return;
    }


    /**
     *  Gets the text of the entry. 
     *
     *  @return the entry text 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getText() {
        return (this.text);
    }


    /**
     *  Sets the text.
     *
     *  @param text a text
     *  @throws org.osid.NullArgumentException <code>text</code> is
     *          <code>null</code>
     */

    protected void setText(org.osid.locale.DisplayText text) {
        nullarg(text, "text");
        this.text = text;
        return;
    }


    /**
     *  Gets the copyright statement for this entry. 
     *
     *  @return the copyright statement 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getCopyright() {
        return (this.copyright);
    }


    /**
     *  Sets the copyright.
     *
     *  @param copyright a copyright
     *  @throws org.osid.NullArgumentException <code>copyright</code>
     *          is <code>null</code>
     */

    protected void setCopyright(org.osid.locale.DisplayText copyright) {
        nullarg(copyright, "copyright");
        this.copyright = copyright;
        return;
    }


    /**
     *  Tests if this entry supports the given record
     *  <code>Type</code>.
     *
     *  @param  entryRecordType an entry record type 
     *  @return <code>true</code> if the entryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>entryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type entryRecordType) {
        for (org.osid.blogging.records.EntryRecord record : this.records) {
            if (record.implementsRecordType(entryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Entry</code> record <code>Type</code>.
     *
     *  @param  entryRecordType the entry record type 
     *  @return the entry record 
     *  @throws org.osid.NullArgumentException
     *          <code>entryRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(entryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.blogging.records.EntryRecord getEntryRecord(org.osid.type.Type entryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.blogging.records.EntryRecord record : this.records) {
            if (record.implementsRecordType(entryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(entryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this entry. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param entryRecord the entry record
     *  @param entryRecordType entry record type
     *  @throws org.osid.NullArgumentException
     *          <code>entryRecord</code> or
     *          <code>entryRecordType</code> is
     *          <code>null</code>
     */
            
    protected void addEntryRecord(org.osid.blogging.records.EntryRecord entryRecord, 
                                  org.osid.type.Type entryRecordType) {

        nullarg(entryRecord, "entry record");
        addRecordType(entryRecordType);
        this.records.add(entryRecord);
        
        return;
    }
}
