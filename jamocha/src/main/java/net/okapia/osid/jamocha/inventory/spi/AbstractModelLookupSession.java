//
// AbstractModelLookupSession.java
//
//    A starter implementation framework for providing a Model
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Model
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getModels(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractModelLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.inventory.ModelLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.inventory.Warehouse warehouse = new net.okapia.osid.jamocha.nil.inventory.warehouse.UnknownWarehouse();
    

    /**
     *  Gets the <code>Warehouse/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Warehouse Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getWarehouseId() {
        return (this.warehouse.getId());
    }


    /**
     *  Gets the <code>Warehouse</code> associated with this 
     *  session.
     *
     *  @return the <code>Warehouse</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Warehouse getWarehouse()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.warehouse);
    }


    /**
     *  Sets the <code>Warehouse</code>.
     *
     *  @param  warehouse the warehouse for this session
     *  @throws org.osid.NullArgumentException <code>warehouse</code>
     *          is <code>null</code>
     */

    protected void setWarehouse(org.osid.inventory.Warehouse warehouse) {
        nullarg(warehouse, "warehouse");
        this.warehouse = warehouse;
        return;
    }


    /**
     *  Tests if this user can perform <code>Model</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupModels() {
        return (true);
    }


    /**
     *  A complete view of the <code>Model</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeModelView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Model</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryModelView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include models in warehouses which are children
     *  of this warehouse in the warehouse hierarchy.
     */

    @OSID @Override
    public void useFederatedWarehouseView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this warehouse only.
     */

    @OSID @Override
    public void useIsolatedWarehouseView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>Model</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Model</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Model</code> and
     *  retained for compatibility.
     *
     *  @param  modelId <code>Id</code> of the
     *          <code>Model</code>
     *  @return the model
     *  @throws org.osid.NotFoundException <code>modelId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>modelId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Model getModel(org.osid.id.Id modelId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.inventory.ModelList models = getModels()) {
            while (models.hasNext()) {
                org.osid.inventory.Model model = models.getNextModel();
                if (model.getId().equals(modelId)) {
                    return (model);
                }
            }
        } 

        throw new org.osid.NotFoundException(modelId + " not found");
    }


    /**
     *  Gets a <code>ModelList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  models specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Models</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getModels()</code>.
     *
     *  @param  modelIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Model</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>modelIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ModelList getModelsByIds(org.osid.id.IdList modelIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.inventory.Model> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = modelIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getModel(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("model " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.inventory.model.LinkedModelList(ret));
    }


    /**
     *  Gets a <code>ModelList</code> corresponding to the given
     *  model genus <code>Type</code> which does not include
     *  models of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  models or an error results. Otherwise, the returned list
     *  may contain only those models that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getModels()</code>.
     *
     *  @param  modelGenusType a model genus type 
     *  @return the returned <code>Model</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>modelGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ModelList getModelsByGenusType(org.osid.type.Type modelGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.inventory.model.ModelGenusFilterList(getModels(), modelGenusType));
    }


    /**
     *  Gets a <code>ModelList</code> corresponding to the given
     *  model genus <code>Type</code> and include any additional
     *  models with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  models or an error results. Otherwise, the returned list
     *  may contain only those models that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getModels()</code>.
     *
     *  @param  modelGenusType a model genus type 
     *  @return the returned <code>Model</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>modelGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ModelList getModelsByParentGenusType(org.osid.type.Type modelGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getModelsByGenusType(modelGenusType));
    }


    /**
     *  Gets a <code>ModelList</code> containing the given
     *  model record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  models or an error results. Otherwise, the returned list
     *  may contain only those models that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getModels()</code>.
     *
     *  @param  modelRecordType a model record type 
     *  @return the returned <code>Model</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>modelRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ModelList getModelsByRecordType(org.osid.type.Type modelRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.inventory.model.ModelRecordFilterList(getModels(), modelRecordType));
    }


    /**
     *  Gets a <code>ModelList</code> for the given resource
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known models
     *  or an error results. Otherwise, the returned list may contain
     *  only those models that are accessible through this session.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Model</code> list 
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.ModelList getModelsByManufacturer(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.inventory.model.ModelFilterList(new ManufacturerFilter(resourceId), getModels()));
    }


    /**
     *  Gets all <code>Models</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  models or an error results. Otherwise, the returned list
     *  may contain only those models that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Models</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.inventory.ModelList getModels()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the model list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of models
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.inventory.ModelList filterModelsOnViews(org.osid.inventory.ModelList list)
        throws org.osid.OperationFailedException {

        return (list);
    }


    public static class ManufacturerFilter
        implements net.okapia.osid.jamocha.inline.filter.inventory.model.ModelFilter {
         
        private final org.osid.id.Id resourceId;
         
         
        /**
         *  Constructs a new <code>ManufacturerFilter</code>.
         *
         *  @param resourceId the manufacturer to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */
        
        public ManufacturerFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "resource Id");
            this.resourceId = resourceId;
            return;
        }

         
        /**
         *  Used by the ModelFilterList to filter the model list based
         *  on manufacturer.
         *
         *  @param model the model
         *  @return <code>true</code> to pass the model,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.inventory.Model model) {
            return (model.getManufacturerId().equals(this.resourceId));
        }
    }
}
