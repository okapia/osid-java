//
// InvariantMapAuctionProcessorLookupSession
//
//    Implements an AuctionProcessor lookup service backed by a fixed collection of
//    auctionProcessors.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.bidding.rules;


/**
 *  Implements an AuctionProcessor lookup service backed by a fixed
 *  collection of auction processors. The auction processors are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapAuctionProcessorLookupSession
    extends net.okapia.osid.jamocha.core.bidding.rules.spi.AbstractMapAuctionProcessorLookupSession
    implements org.osid.bidding.rules.AuctionProcessorLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapAuctionProcessorLookupSession</code> with no
     *  auction processors.
     *  
     *  @param auctionHouse the auction house
     *  @throws org.osid.NullArgumnetException {@code auctionHouse} is
     *          {@code null}
     */

    public InvariantMapAuctionProcessorLookupSession(org.osid.bidding.AuctionHouse auctionHouse) {
        setAuctionHouse(auctionHouse);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAuctionProcessorLookupSession</code> with a single
     *  auction processor.
     *  
     *  @param auctionHouse the auction house
     *  @param auctionProcessor an single auction processor
     *  @throws org.osid.NullArgumentException {@code auctionHouse} or
     *          {@code auctionProcessor} is <code>null</code>
     */

      public InvariantMapAuctionProcessorLookupSession(org.osid.bidding.AuctionHouse auctionHouse,
                                               org.osid.bidding.rules.AuctionProcessor auctionProcessor) {
        this(auctionHouse);
        putAuctionProcessor(auctionProcessor);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAuctionProcessorLookupSession</code> using an array
     *  of auction processors.
     *  
     *  @param auctionHouse the auction house
     *  @param auctionProcessors an array of auction processors
     *  @throws org.osid.NullArgumentException {@code auctionHouse} or
     *          {@code auctionProcessors} is <code>null</code>
     */

      public InvariantMapAuctionProcessorLookupSession(org.osid.bidding.AuctionHouse auctionHouse,
                                               org.osid.bidding.rules.AuctionProcessor[] auctionProcessors) {
        this(auctionHouse);
        putAuctionProcessors(auctionProcessors);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAuctionProcessorLookupSession</code> using a
     *  collection of auction processors.
     *
     *  @param auctionHouse the auction house
     *  @param auctionProcessors a collection of auction processors
     *  @throws org.osid.NullArgumentException {@code auctionHouse} or
     *          {@code auctionProcessors} is <code>null</code>
     */

      public InvariantMapAuctionProcessorLookupSession(org.osid.bidding.AuctionHouse auctionHouse,
                                               java.util.Collection<? extends org.osid.bidding.rules.AuctionProcessor> auctionProcessors) {
        this(auctionHouse);
        putAuctionProcessors(auctionProcessors);
        return;
    }
}
