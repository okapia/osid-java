//
// AbstractTemporalOsidObjectQueryInspector.java
//
//     Defines a temporal OsidObjectQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 1 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.query.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a simple OsidQueryInspector to extend. 
 */

public abstract class AbstractTemporalOsidObjectQueryInspector
    extends AbstractOsidObjectQueryInspector
    implements org.osid.OsidTemporalQueryInspector,
               org.osid.OsidObjectQueryInspector {

    private final OsidTemporalQueryInspector inspector = new OsidTemporalQueryInspector();


    /**
     *  Gets the effective query terms.
     *
     *  @return the effective terms
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getEffectiveTerms() {
        return (this.inspector.getEffectiveTerms());
    }


    /**
     *  Adds an effective term.
     *
     *  @param term an effective term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addEffectiveTerm(org.osid.search.terms.BooleanTerm term) {
        this.inspector.addEffectiveTerm(term);
        return;
    }


    /**
     *  Adds a collection of effective terms.
     *
     *  @param terms a collection of effective terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is
     *          <code>null</code>
     */

    protected void addEffectiveTerms(java.util.Collection<org.osid.search.terms.BooleanTerm> terms) {
        this.inspector.addEffectiveTerms(terms);
        return;
    }


    /**
     *  Adds an effective term.
     *
     *  @param effective the effective flag
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     */

    protected void addEffectiveTerm(boolean effective, boolean match) {
        this.inspector.addEffectiveTerm(effective, match);
        return;
    }


    /**
     *  Gets the start date query terms.
     *
     *  @return the start date terms
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getStartDateTerms() {
        return (this.inspector.getStartDateTerms());
    }

    
    /**
     *  Adds a start date term.
     *
     *  @param term a start date
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addStartDateTerm(org.osid.search.terms.DateTimeRangeTerm term) {
        this.inspector.addStartDateTerm(term);
        return;
    }


    /**
     *  Adds a collection of start date terms.
     *
     *  @param terms a collection of start date terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addStartDateTerms(java.util.Collection<org.osid.search.terms.DateTimeRangeTerm> terms) {
        this.inspector.addStartDateTerms(terms);
        return;
    }


    /**
     *  Adds a start date term.
     *
     *  @param start start of date range
     *  @param end end of date range
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>start</code> or
     *          <code>end</code> or is <code>null</code>
     */

    protected void addStartDateTerm(org.osid.calendaring.DateTime start, org.osid.calendaring.DateTime end, 
                                    boolean match) {
        this.inspector.addStartDateTerm(start, end, match);
        return;
    }


    /**
     *  Gets the end date query terms.
     *
     *  @return the end date terms
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getEndDateTerms() {
        return (this.inspector.getEndDateTerms());
    }

    
    /**
     *  Adds an end date term.
     *
     *  @param term an end date
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addEndDateTerm(org.osid.search.terms.DateTimeRangeTerm term) {
        this.inspector.addEndDateTerm(term);
        return;
    }


    /**
     *  Adds a collection of end date terms.
     *
     *  @param terms a collection of end date terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addEndDateTerms(java.util.Collection<org.osid.search.terms.DateTimeRangeTerm> terms) {
        this.inspector.addEndDateTerms(terms);
        return;
    }


    /**
     *  Adds an end date term.
     *
     *  @param start start of date range
     *  @param end end of date range
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>start</code> or
     *          <code>end</code> or is <code>null</code>
     */

    protected void addEndDateTerm(org.osid.calendaring.DateTime start, org.osid.calendaring.DateTime end, 
                                    boolean match) {
        this.inspector.addEndDateTerm(start, end, match);
        return;
    }


    /**
     *  Gets the date query terms.
     *
     *  @return the date terms
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getDateTerms() {
        return (this.inspector.getDateTerms());
    }

    
    /**
     *  Adds a date term.
     *
     *  @param term a date term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addDateTerm(org.osid.search.terms.DateTimeRangeTerm term) {
        this.inspector.addDateTerm(term);
        return;
    }


    /**
     *  Adds a collection of date terms.
     *
     *  @param terms a collection of date terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addDateTerms(java.util.Collection<org.osid.search.terms.DateTimeRangeTerm> terms) {
        this.inspector.addDateTerms(terms);
        return;
    }


    /**
     *  Adds a date term.
     *
     *  @param from start date
     *  @param to end date
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     */

    protected void addDateTerm(org.osid.calendaring.DateTime from,
                               org.osid.calendaring.DateTime to,
                               boolean match) {
        this.inspector.addDateTerm(from, to, match);
        return;
    }

    
    protected class OsidTemporalQueryInspector
        extends AbstractOsidTemporalQueryInspector
        implements org.osid.OsidTemporalQueryInspector {


        /**
         *  Adds an effective term.
         *
         *  @param term an effective term
         *  @throws org.osid.NullArgumentException <code>term</code> is 
         *          <code>null</code>
         */

        @Override
        protected void addEffectiveTerm(org.osid.search.terms.BooleanTerm term) {
            super.addEffectiveTerm(term);
            return;
        }
        

        /**
         *  Adds a collection of effective terms.
         *
         *  @param terms a collection of effective terms
         *  @throws org.osid.NullArgumentException <code>terms</code> is
         *          <code>null</code>
         */
        
        @Override
        protected void addEffectiveTerms(java.util.Collection<org.osid.search.terms.BooleanTerm> terms) {
            super.addEffectiveTerms(terms);
            return;
        }
        
        
        /**
         *  Adds an effective term.
         *
         *  @param effective the effective flag
         *  @param match <code>true</code> for a positive match,
         *         <code>false</code> for a negative match
         */
        
        @Override
        protected void addEffectiveTerm(boolean effective, boolean match) {
            super.addEffectiveTerm(effective, match);
            return;
        }
        
    
        /**
         *  Adds a start date term.
         *
         *  @param term a start date
         *  @throws org.osid.NullArgumentException <code>term</code> is 
         *          <code>null</code>
         */

        @Override
        protected void addStartDateTerm(org.osid.search.terms.DateTimeRangeTerm term) {
            super.addStartDateTerm(term);
            return;
        }
        
        
        /**
         *  Adds a collection of start date terms.
         *
         *  @param terms a collection of start date terms
         *  @throws org.osid.NullArgumentException <code>terms</code> is 
         *          <code>null</code>
         */
        
        @Override
        protected void addStartDateTerms(java.util.Collection<org.osid.search.terms.DateTimeRangeTerm> terms) {
            super.addStartDateTerms(terms);
            return;
        }
        
        
        /**
         *  Adds a start date term.
         *
         *  @param start start of date range
         *  @param end end of date range
         *  @param match <code>true</code> for a positive match,
         *         <code>false</code> for a negative match
         *  @throws org.osid.NullArgumentException <code>start</code> or
         *          <code>end</code> or is <code>null</code>
         */
        
        @Override
        protected void addStartDateTerm(org.osid.calendaring.DateTime start, org.osid.calendaring.DateTime end, 
                                        boolean match) {
            super.addStartDateTerm(start, end, match);
            return;
        }
        
            
        /**
         *  Adds an end date term.
         *
         *  @param term an end date
         *  @throws org.osid.NullArgumentException <code>term</code> is 
         *          <code>null</code>
         */
        
        @Override
        protected void addEndDateTerm(org.osid.search.terms.DateTimeRangeTerm term) {
            super.addEndDateTerm(term);
            return;
        }
        
        
        /**
         *  Adds a collection of end date terms.
         *
         *  @param terms a collection of end date terms
         *  @throws org.osid.NullArgumentException <code>terms</code> is 
         *          <code>null</code>
         */

        @Override
        protected void addEndDateTerms(java.util.Collection<org.osid.search.terms.DateTimeRangeTerm> terms) {
            super.addEndDateTerms(terms);
            return;
        }
        
        
        /**
         *  Adds an end date term.
         *
         *  @param start start of date range
         *  @param end end of date range
         *  @param match <code>true</code> for a positive match,
         *         <code>false</code> for a negative match
         *  @throws org.osid.NullArgumentException <code>start</code> or
         *          <code>end</code> or is <code>null</code>
         */

        @Override
        protected void addEndDateTerm(org.osid.calendaring.DateTime start, org.osid.calendaring.DateTime end, 
                                      boolean match) {
            super.addEndDateTerm(start, end, match);
            return;
        }
        

        /**
         *  Adds a date term.
         *
         *  @param term a date term
         *  @throws org.osid.NullArgumentException <code>term</code> is 
         *          <code>null</code>
         */

        @Override
        protected void addDateTerm(org.osid.search.terms.DateTimeRangeTerm term) {
            super.addDateTerm(term);
            return;
        }
        
        
        /**
         *  Adds a collection of date terms.
         *
         *  @param terms a collection of date terms
         *  @throws org.osid.NullArgumentException <code>terms</code> is 
         *          <code>null</code>
         */
        
        @Override
        protected void addDateTerms(java.util.Collection<org.osid.search.terms.DateTimeRangeTerm> terms) {
            super.addDateTerms(terms);
            return;
        }
        
        
        /**
         *  Adds a date term.
         *
         *  @param from start date
         *  @param to end date
         *  @param match <code>true</code> for a positive match,
         *         <code>false</code> for a negative match
         *  @throws org.osid.InvalidArgumentException
         *          <code>from</code> is greater than <code>to</code>
         *  @throws org.osid.NullArgumentException <code>from</code>
         *          or <code>to</code> is <code>null</code>
         */
        
        @Override
        protected void addDateTerm(org.osid.calendaring.DateTime from,
                                   org.osid.calendaring.DateTime to,
                                   boolean match) {
            super.addDateTerm(from, to, match);
            return;
        }
    }
}
