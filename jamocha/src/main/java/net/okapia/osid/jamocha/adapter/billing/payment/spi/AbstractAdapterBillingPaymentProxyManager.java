//
// AbstractBillingPaymentProxyManager.java
//
//     An adapter for a BillingPaymentProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.billing.payment.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a BillingPaymentProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterBillingPaymentProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.billing.payment.BillingPaymentProxyManager>
    implements org.osid.billing.payment.BillingPaymentProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterBillingPaymentProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterBillingPaymentProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterBillingPaymentProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterBillingPaymentProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any business federation is exposed. Federation is exposed 
     *  when a specific business may be identified, selected and used to 
     *  create a lookup or admin session. Federation is not exposed when a set 
     *  of catalogs appears as a single catalog. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up payers is supported. 
     *
     *  @return <code> true </code> if payer lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPayerLookup() {
        return (getAdapteeManager().supportsPayerLookup());
    }


    /**
     *  Tests if querying payers is supported. 
     *
     *  @return <code> true </code> if payer query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPayerQuery() {
        return (getAdapteeManager().supportsPayerQuery());
    }


    /**
     *  Tests if searching payers is supported. 
     *
     *  @return <code> true </code> if payer search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPayerSearch() {
        return (getAdapteeManager().supportsPayerSearch());
    }


    /**
     *  Tests if payer <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if payer administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPayerAdmin() {
        return (getAdapteeManager().supportsPayerAdmin());
    }


    /**
     *  Tests if a payer <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if payer notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPayerNotification() {
        return (getAdapteeManager().supportsPayerNotification());
    }


    /**
     *  Tests if a businessing service is supported. 
     *
     *  @return <code> true </code> if businessing is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPayerBusiness() {
        return (getAdapteeManager().supportsPayerBusiness());
    }


    /**
     *  Tests if a businessing service is supported. A businessing service 
     *  maps payers to catalogs. 
     *
     *  @return <code> true </code> if businessing is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPayerBusinessAssignment() {
        return (getAdapteeManager().supportsPayerBusinessAssignment());
    }


    /**
     *  Tests if a payer smart business session is available. 
     *
     *  @return <code> true </code> if a payer smart business session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPayerSmartBusiness() {
        return (getAdapteeManager().supportsPayerSmartBusiness());
    }


    /**
     *  Tests if looking up payments is supported. 
     *
     *  @return <code> true </code> if payment lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPaymentLookup() {
        return (getAdapteeManager().supportsPaymentLookup());
    }


    /**
     *  Tests if querying payments is supported. 
     *
     *  @return <code> true </code> if payment query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPaymentQuery() {
        return (getAdapteeManager().supportsPaymentQuery());
    }


    /**
     *  Tests if searching payments is supported. 
     *
     *  @return <code> true </code> if payment search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPaymentSearch() {
        return (getAdapteeManager().supportsPaymentSearch());
    }


    /**
     *  Tests if payment <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if payment administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPaymentAdmin() {
        return (getAdapteeManager().supportsPaymentAdmin());
    }


    /**
     *  Tests if a payment <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if payment notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPaymentNotification() {
        return (getAdapteeManager().supportsPaymentNotification());
    }


    /**
     *  Tests if a payment cataloging service is supported. 
     *
     *  @return <code> true </code> if payment catalog is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPaymentBusiness() {
        return (getAdapteeManager().supportsPaymentBusiness());
    }


    /**
     *  Tests if a payment cataloging service is supported. A cataloging 
     *  service maps payments to catalogs. 
     *
     *  @return <code> true </code> if payment cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPaymentBusinessAssignment() {
        return (getAdapteeManager().supportsPaymentBusinessAssignment());
    }


    /**
     *  Tests if a payment smart business session is available. 
     *
     *  @return <code> true </code> if a payment smart business session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPaymentSmartBusiness() {
        return (getAdapteeManager().supportsPaymentSmartBusiness());
    }


    /**
     *  Tests if a payment summary session is available. 
     *
     *  @return <code> true </code> if a summary session is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSummary() {
        return (getAdapteeManager().supportsSummary());
    }


    /**
     *  Tests if a payment batch service is available. 
     *
     *  @return <code> true </code> if a payment batch service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBillingPaymentBatch() {
        return (getAdapteeManager().supportsBillingPaymentBatch());
    }


    /**
     *  Gets the supported <code> Payer </code> record types. 
     *
     *  @return a list containing the supported <code> Payer </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPayerRecordTypes() {
        return (getAdapteeManager().getPayerRecordTypes());
    }


    /**
     *  Tests if the given <code> Payer </code> record type is supported. 
     *
     *  @param  payerRecordType a <code> Type </code> indicating a <code> 
     *          Payer </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> payerRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPayerRecordType(org.osid.type.Type payerRecordType) {
        return (getAdapteeManager().supportsPayerRecordType(payerRecordType));
    }


    /**
     *  Gets the supported <code> Payer </code> search record types. 
     *
     *  @return a list containing the supported <code> Payer </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPayerSearchRecordTypes() {
        return (getAdapteeManager().getPayerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Payer </code> search record type is 
     *  supported. 
     *
     *  @param  payerSearchRecordType a <code> Type </code> indicating a 
     *          <code> Payer </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> payerSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPayerSearchRecordType(org.osid.type.Type payerSearchRecordType) {
        return (getAdapteeManager().supportsPayerSearchRecordType(payerSearchRecordType));
    }


    /**
     *  Gets the supported <code> Payment </code> record types. 
     *
     *  @return a list containing the supported <code> Payment </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPaymentRecordTypes() {
        return (getAdapteeManager().getPaymentRecordTypes());
    }


    /**
     *  Tests if the given <code> Payment </code> record type is supported. 
     *
     *  @param  paymentRecordType a <code> Type </code> indicating a <code> 
     *          Payment </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> paymentRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPaymentRecordType(org.osid.type.Type paymentRecordType) {
        return (getAdapteeManager().supportsPaymentRecordType(paymentRecordType));
    }


    /**
     *  Gets the supported <code> Payment </code> search record types. 
     *
     *  @return a list containing the supported <code> Payment </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPaymentSearchRecordTypes() {
        return (getAdapteeManager().getPaymentSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Payment </code> search record type is 
     *  supported. 
     *
     *  @param  paymentSearchRecordType a <code> Type </code> indicating a 
     *          <code> Payment </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> paymentSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPaymentSearchRecordType(org.osid.type.Type paymentSearchRecordType) {
        return (getAdapteeManager().supportsPaymentSearchRecordType(paymentSearchRecordType));
    }


    /**
     *  Gets the supported <code> Summary </code> record types. 
     *
     *  @return a list containing the supported <code> Summary </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSummaryRecordTypes() {
        return (getAdapteeManager().getSummaryRecordTypes());
    }


    /**
     *  Tests if the given <code> Summary </code> record type is supported. 
     *
     *  @param  summaryRecordType a <code> Type </code> indicating a <code> 
     *          Summary </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> summaryRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSummaryRecordType(org.osid.type.Type summaryRecordType) {
        return (getAdapteeManager().supportsSummaryRecordType(summaryRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payer lookup 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> PayerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPayerLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerLookupSession getPayerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPayerLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payer lookup 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the business 
     *  @param  proxy proxy 
     *  @return a <code> PayerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPayerLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerLookupSession getPayerLookupSessionForBusiness(org.osid.id.Id businessId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPayerLookupSessionForBusiness(businessId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payer query 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> PayerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPayerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerQuerySession getPayerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPayerQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payer query 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> PayerQuerySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPayerQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerQuerySession getPayerQuerySessionForBusiness(org.osid.id.Id businessId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPayerQuerySessionForBusiness(businessId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payer search 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> PayerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPayerSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerSearchSession getPayerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPayerSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payer search 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> PayerSearchSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPayerSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerSearchSession getPayerSearchSessionForBusiness(org.osid.id.Id businessId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPayerSearchSessionForBusiness(businessId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payer 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> PayerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPayerAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerAdminSession getPayerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPayerAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payer 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> PayerAdminSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPayerAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerAdminSession getPayerAdminSessionForBusiness(org.osid.id.Id businessId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPayerAdminSessionForBusiness(businessId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payer 
     *  notification service. 
     *
     *  @param  payerReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return a <code> PayerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> payerReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPayerNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerNotificationSession getPayerNotificationSession(org.osid.billing.payment.PayerReceiver payerReceiver, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPayerNotificationSession(payerReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payer 
     *  notification service for the given business. 
     *
     *  @param  payerReceiver the notification callback 
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> PayerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> payerReceiver, 
     *          businessId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPayerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerNotificationSession getPayerNotificationSessionForBusiness(org.osid.billing.payment.PayerReceiver payerReceiver, 
                                                                                                    org.osid.id.Id businessId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPayerNotificationSessionForBusiness(payerReceiver, businessId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup payer/catalog mappings. 
     *
     *  @param  proxy proxy 
     *  @return a <code> PayerBusinessSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPayerBusiness() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerBusinessSession getPayerBusinessSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPayerBusinessSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning payers 
     *  to businesses. 
     *
     *  @param  proxy proxy 
     *  @return a <code> PayerBusinessAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPayerBusinessAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerBusinessAssignmentSession getPayerBusinessAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPayerBusinessAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payer smart 
     *  business service. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> PayerSmartBusinessSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPayerSmartBusiness() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerSmartBusinessSession getPayerSmartBusinessSession(org.osid.id.Id businessId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPayerSmartBusinessSession(businessId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payment lookup 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> PaymentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPaymentLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentLookupSession getPaymentLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPaymentLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payment lookup 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the business 
     *  @param  proxy proxy 
     *  @return a <code> PaymentLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPaymentLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentLookupSession getPaymentLookupSessionForBusiness(org.osid.id.Id businessId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPaymentLookupSessionForBusiness(businessId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payment query 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> PaymentQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPaymentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentQuerySession getPaymentQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPaymentQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payment query 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> PaymentQuerySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPaymentQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentQuerySession getPaymentQuerySessionForBusiness(org.osid.id.Id businessId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPaymentQuerySessionForBusiness(businessId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payment search 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> PaymentSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPaymentSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentSearchSession getPaymentSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPaymentSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payment search 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> PaymentSearchSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPaymentSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentSearchSession getPaymentSearchSessionForBusiness(org.osid.id.Id businessId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPaymentSearchSessionForBusiness(businessId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payment 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> PaymentAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPaymentAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentAdminSession getPaymentAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPaymentAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payment 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> PaymentAdminSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          businessId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPaymentAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentAdminSession getPaymentAdminSessionForBusiness(org.osid.id.Id businessId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPaymentAdminSessionForBusiness(businessId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payment 
     *  notification service. 
     *
     *  @param  paymentReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return a <code> PaymentNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> paymentReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPaymentNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentNotificationSession getPaymentNotificationSession(org.osid.billing.payment.PaymentReceiver paymentReceiver, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPaymentNotificationSession(paymentReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payment 
     *  notification service for the given business. 
     *
     *  @param  paymentReceiver the notification callback 
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> PaymentNotificationSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> paymentReceiver, 
     *          businessId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPaymentNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentNotificationSession getPaymentNotificationSessionForBusiness(org.osid.billing.payment.PaymentReceiver paymentReceiver, 
                                                                                                        org.osid.id.Id businessId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPaymentNotificationSessionForBusiness(paymentReceiver, businessId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup payment/catalog 
     *  mappings. 
     *
     *  @param  proxy proxy 
     *  @return a <code> PaymentBusinessSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPaymentBusiness() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentBusinessSession getPaymentBusinessSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPaymentBusinessSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning payments 
     *  to businesses. 
     *
     *  @param  proxy proxy 
     *  @return a <code> PaymentBusinessAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPaymentBusinessAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentBusinessAssignmentSession getPaymentBusinessAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPaymentBusinessAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payment smart 
     *  business service. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> PaymentSmartBusinessSession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPaymentSmartBusiness() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentSmartBusinessSession getPaymentSmartBusinessSession(org.osid.id.Id businessId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPaymentSmartBusinessSession(businessId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the payment 
     *  summary service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> SummarySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSummary() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.SummarySession getSummarySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSummarySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the summary 
     *  service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @param  proxy proxy 
     *  @return a <code> SummarySession </code> 
     *  @throws org.osid.NotFoundException no business found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSummary() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.SummarySession getSummarySessionForBusiness(org.osid.id.Id businessId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSummarySessionForBusiness(businessId, proxy));
    }


    /**
     *  Gets a <code> BillingPaymentBatchProxyManager. </code> 
     *
     *  @return a <code> BillingPaymentBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBillingPaymentBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.batch.BillingPaymentBatchProxyManager getBillingPaymentBatchProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBillingPaymentBatchProxyManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
