//
// AbstractMapDirectionLookupSession
//
//    A simple framework for providing a Direction lookup service
//    backed by a fixed collection of directions.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.recipe.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Direction lookup service backed by a
 *  fixed collection of directions. The directions are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Directions</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapDirectionLookupSession
    extends net.okapia.osid.jamocha.recipe.spi.AbstractDirectionLookupSession
    implements org.osid.recipe.DirectionLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.recipe.Direction> directions = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.recipe.Direction>());


    /**
     *  Makes a <code>Direction</code> available in this session.
     *
     *  @param  direction a direction
     *  @throws org.osid.NullArgumentException <code>direction<code>
     *          is <code>null</code>
     */

    protected void putDirection(org.osid.recipe.Direction direction) {
        this.directions.put(direction.getId(), direction);
        return;
    }


    /**
     *  Makes an array of directions available in this session.
     *
     *  @param  directions an array of directions
     *  @throws org.osid.NullArgumentException <code>directions<code>
     *          is <code>null</code>
     */

    protected void putDirections(org.osid.recipe.Direction[] directions) {
        putDirections(java.util.Arrays.asList(directions));
        return;
    }


    /**
     *  Makes a collection of directions available in this session.
     *
     *  @param  directions a collection of directions
     *  @throws org.osid.NullArgumentException <code>directions<code>
     *          is <code>null</code>
     */

    protected void putDirections(java.util.Collection<? extends org.osid.recipe.Direction> directions) {
        for (org.osid.recipe.Direction direction : directions) {
            this.directions.put(direction.getId(), direction);
        }

        return;
    }


    /**
     *  Removes a Direction from this session.
     *
     *  @param  directionId the <code>Id</code> of the direction
     *  @throws org.osid.NullArgumentException <code>directionId<code> is
     *          <code>null</code>
     */

    protected void removeDirection(org.osid.id.Id directionId) {
        this.directions.remove(directionId);
        return;
    }


    /**
     *  Gets the <code>Direction</code> specified by its <code>Id</code>.
     *
     *  @param  directionId <code>Id</code> of the <code>Direction</code>
     *  @return the direction
     *  @throws org.osid.NotFoundException <code>directionId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>directionId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.Direction getDirection(org.osid.id.Id directionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.recipe.Direction direction = this.directions.get(directionId);
        if (direction == null) {
            throw new org.osid.NotFoundException("direction not found: " + directionId);
        }

        return (direction);
    }


    /**
     *  Gets all <code>Directions</code>. In plenary mode, the returned
     *  list contains all known directions or an error
     *  results. Otherwise, the returned list may contain only those
     *  directions that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Directions</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.DirectionList getDirections()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.recipe.direction.ArrayDirectionList(this.directions.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.directions.clear();
        super.close();
        return;
    }
}
