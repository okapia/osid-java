//
// GradeMiter.java
//
//     Defines a Grade miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.grading.grade;


/**
 *  Defines a <code>Grade</code> miter for use with the builders.
 */

public interface GradeMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.grading.Grade {


    /**
     *  Sets the grade system.
     *
     *  @param system a grade system
     *  @throws org.osid.NullArgumentException <code>system</code> is
     *          <code>null</code>
     */

    public void setGradeSystem(org.osid.grading.GradeSystem system);


    /**
     *  Sets the input score start range.
     *
     *  @param start an input score start range
     *  @throws org.osid.NullArgumentException <code>start</code> is
     *          <code>null</code>
     */

    public void setInputScoreStartRange(java.math.BigDecimal start);


    /**
     *  Sets the input score end range.
     *
     *  @param end an input score end range
     *  @throws org.osid.NullArgumentException <code>end</code> is
     *          <code>null</code>
     */

    public void setInputScoreEndRange(java.math.BigDecimal end);


    /**
     *  Sets the output score.
     *
     *  @param score an output score
     *  @throws org.osid.NullArgumentException <code>score</code> is
     *          <code>null</code>
     */

    public void setOutputScore(java.math.BigDecimal score);


    /**
     *  Adds a Grade record.
     *
     *  @param record a grade record
     *  @param recordType the type of grade record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addGradeRecord(org.osid.grading.records.GradeRecord record, org.osid.type.Type recordType);
}       


