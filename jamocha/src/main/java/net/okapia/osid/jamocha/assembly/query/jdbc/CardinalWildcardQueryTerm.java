//
// CardinalWildcardQueryTerm.java
//
//     A cardinal wildcard query term.
//
//
// Tom Coppeto
// Okapia
// 20 April 2013
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.jdbc;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A cardinal query term.
 */

public class CardinalWildcardQueryTerm 
    extends net.okapia.osid.primordium.terms.spi.AbstractCardinalWildcardTerm
    implements org.osid.search.terms.CardinalTerm,
               net.okapia.osid.jamocha.assembly.query.QueryTerm {

    private final String column;


    /**
     *  Constructs a new <code>CardinalWildcardQueryTerm</code>.
     *
     *  @param column name of query column
     *  @param match <code>true</code> if a positive term,
     *         <code>false</code> for a negative term
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    public CardinalWildcardQueryTerm(String column, boolean match) {
        super(match);
        nullarg(column, "column");
        this.column = column;

        return;
    }

    
    /**
     *  Gets the query string for this term.
     *
     *  @return the query string
     */

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append(getColumn());
        sb.append(getOperator());
        sb.append(getValue());

        return (sb.toString());
    }


    /**
     *  Gets the query column.
     *
     *  @return the column
     */

    @Override
    public String getColumn() {
        return (this.column);
    }


    /**
     *  Gets the query operator.
     *
     *  @return the operator
     */

    protected String getOperator() {
        if (isPositive()) {
            return ("=");
        } else {
            return ("!=");
        }
    }


    /**
     *  Gets the query value.
     *
     *  @return the value
     */

    protected String getValue() {
        return ("*");
    }
}
