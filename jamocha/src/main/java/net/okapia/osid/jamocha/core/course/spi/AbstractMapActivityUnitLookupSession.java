//
// AbstractMapActivityUnitLookupSession
//
//    A simple framework for providing an ActivityUnit lookup service
//    backed by a fixed collection of activity units.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an ActivityUnit lookup service backed by a
 *  fixed collection of activity units. The activity units are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>ActivityUnits</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapActivityUnitLookupSession
    extends net.okapia.osid.jamocha.course.spi.AbstractActivityUnitLookupSession
    implements org.osid.course.ActivityUnitLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.course.ActivityUnit> activityUnits = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.course.ActivityUnit>());


    /**
     *  Makes an <code>ActivityUnit</code> available in this session.
     *
     *  @param  activityUnit an activity unit
     *  @throws org.osid.NullArgumentException <code>activityUnit<code>
     *          is <code>null</code>
     */

    protected void putActivityUnit(org.osid.course.ActivityUnit activityUnit) {
        this.activityUnits.put(activityUnit.getId(), activityUnit);
        return;
    }


    /**
     *  Makes an array of activity units available in this session.
     *
     *  @param  activityUnits an array of activity units
     *  @throws org.osid.NullArgumentException <code>activityUnits<code>
     *          is <code>null</code>
     */

    protected void putActivityUnits(org.osid.course.ActivityUnit[] activityUnits) {
        putActivityUnits(java.util.Arrays.asList(activityUnits));
        return;
    }


    /**
     *  Makes a collection of activity units available in this session.
     *
     *  @param  activityUnits a collection of activity units
     *  @throws org.osid.NullArgumentException <code>activityUnits<code>
     *          is <code>null</code>
     */

    protected void putActivityUnits(java.util.Collection<? extends org.osid.course.ActivityUnit> activityUnits) {
        for (org.osid.course.ActivityUnit activityUnit : activityUnits) {
            this.activityUnits.put(activityUnit.getId(), activityUnit);
        }

        return;
    }


    /**
     *  Removes an ActivityUnit from this session.
     *
     *  @param  activityUnitId the <code>Id</code> of the activity unit
     *  @throws org.osid.NullArgumentException <code>activityUnitId<code> is
     *          <code>null</code>
     */

    protected void removeActivityUnit(org.osid.id.Id activityUnitId) {
        this.activityUnits.remove(activityUnitId);
        return;
    }


    /**
     *  Gets the <code>ActivityUnit</code> specified by its <code>Id</code>.
     *
     *  @param  activityUnitId <code>Id</code> of the <code>ActivityUnit</code>
     *  @return the activityUnit
     *  @throws org.osid.NotFoundException <code>activityUnitId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>activityUnitId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.ActivityUnit getActivityUnit(org.osid.id.Id activityUnitId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.course.ActivityUnit activityUnit = this.activityUnits.get(activityUnitId);
        if (activityUnit == null) {
            throw new org.osid.NotFoundException("activityUnit not found: " + activityUnitId);
        }

        return (activityUnit);
    }


    /**
     *  Gets all <code>ActivityUnits</code>. In plenary mode, the returned
     *  list contains all known activityUnits or an error
     *  results. Otherwise, the returned list may contain only those
     *  activityUnits that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>ActivityUnits</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitList getActivityUnits()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.activityunit.ArrayActivityUnitList(this.activityUnits.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.activityUnits.clear();
        super.close();
        return;
    }
}
