//
// AbstractAssessmentTakenQueryInspector.java
//
//     A template for making an AssessmentTakenQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.assessmenttaken.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for assessments taken.
 */

public abstract class AbstractAssessmentTakenQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.assessment.AssessmentTakenQueryInspector {

    private final java.util.Collection<org.osid.assessment.records.AssessmentTakenQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the assessment offered <code> Id </code> query terms. 
     *
     *  @return the assessment offered <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssessmentOfferedIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the assessment offered query terms. 
     *
     *  @return the assessment offered terms 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedQueryInspector[] getAssessmentOfferedTerms() {
        return (new org.osid.assessment.AssessmentOfferedQueryInspector[0]);
    }


    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the resource <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getTakerIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the resource query terms. 
     *
     *  @return the resource terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getTakerTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the agent <code> Id </code> query terms. 
     *
     *  @return the agent <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getTakingAgentIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the agent query terms. 
     *
     *  @return the agent terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getTakingAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Gets the start time query terms. 
     *
     *  @return the start time terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeTerm[] getActualStartTimeTerms() {
        return (new org.osid.search.terms.DateTimeTerm[0]);
    }


    /**
     *  Gets the completion time query terms. 
     *
     *  @return the completion time terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeTerm[] getCompletionTimeTerms() {
        return (new org.osid.search.terms.DateTimeTerm[0]);
    }


    /**
     *  Gets the time spent query terms. 
     *
     *  @return the time spent terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationTerm[] getTimeSpentTerms() {
        return (new org.osid.search.terms.DurationTerm[0]);
    }


    /**
     *  Gets the grade system <code> Id </code> query terms. 
     *
     *  @return the grade system <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getScoreSystemIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the grade system query terms. 
     *
     *  @return the grade system terms 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQueryInspector[] getScoreSystemTerms() {
        return (new org.osid.grading.GradeSystemQueryInspector[0]);
    }


    /**
     *  Gets the score query terms. 
     *
     *  @return the score terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getScoreTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the grade <code> Id </code> query terms. 
     *
     *  @return the grade <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradeIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the grade query terms. 
     *
     *  @return the grade terms 
     */

    @OSID @Override
    public org.osid.grading.GradeQueryInspector[] getGradeTerms() {
        return (new org.osid.grading.GradeQueryInspector[0]);
    }


    /**
     *  Gets the comment query terms. 
     *
     *  @return the comment terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getFeedbackTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the assessment taken <code> Id </code> query terms. 
     *
     *  @return the assessment taken <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRubricIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the assessment taken query terms. 
     *
     *  @return the assessment taken terms 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenQueryInspector[] getRubricTerms() {
        return (new org.osid.assessment.AssessmentTakenQueryInspector[0]);
    }


    /**
     *  Gets the bank <code> Id </code> query terms. 
     *
     *  @return the bank <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBankIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the bank query terms. 
     *
     *  @return the bank terms 
     */

    @OSID @Override
    public org.osid.assessment.BankQueryInspector[] getBankTerms() {
        return (new org.osid.assessment.BankQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given assessment taken query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an assessment taken implementing the requested record.
     *
     *  @param assessmentTakenRecordType an assessment taken record type
     *  @return the assessment taken query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentTakenRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assessmentTakenRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.AssessmentTakenQueryInspectorRecord getAssessmentTakenQueryInspectorRecord(org.osid.type.Type assessmentTakenRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.records.AssessmentTakenQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(assessmentTakenRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assessmentTakenRecordType + " is not supported");
    }


    /**
     *  Adds a record to this assessment taken query. 
     *
     *  @param assessmentTakenQueryInspectorRecord assessment taken query inspector
     *         record
     *  @param assessmentTakenRecordType assessmentTaken record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAssessmentTakenQueryInspectorRecord(org.osid.assessment.records.AssessmentTakenQueryInspectorRecord assessmentTakenQueryInspectorRecord, 
                                                   org.osid.type.Type assessmentTakenRecordType) {

        addRecordType(assessmentTakenRecordType);
        nullarg(assessmentTakenRecordType, "assessment taken record type");
        this.records.add(assessmentTakenQueryInspectorRecord);        
        return;
    }
}
