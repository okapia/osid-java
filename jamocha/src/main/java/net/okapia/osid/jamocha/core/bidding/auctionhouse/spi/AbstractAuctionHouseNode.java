//
// AbstractAuctionHouse.java
//
//     Defines an AuctionHouseNode within an in core hierarchy.
//
//
// Tom Coppeto
// Okapia
// 8 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.bidding.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract class for managing a hierarchy of auction house
 *  nodes in core.
 */

public abstract class AbstractAuctionHouseNode
    extends net.okapia.osid.jamocha.spi.AbstractOsidNode
    implements org.osid.bidding.AuctionHouseNode,
               org.osid.hierarchy.Node {

    private final org.osid.bidding.AuctionHouse auctionHouse;
    private final java.util.Collection<org.osid.bidding.AuctionHouseNode> parents  = new java.util.HashSet<org.osid.bidding.AuctionHouseNode>();
    private final java.util.Collection<org.osid.bidding.AuctionHouseNode> children = new java.util.HashSet<org.osid.bidding.AuctionHouseNode>();


    /**
     *  Constructs a new <code>AbstractAuctionHouseNode</code> from a
     *  single auction house.
     *
     *  @param auctionHouse the auction house
     *  @throws org.osid.NullArgumentException <code>auctionHouse</code> is 
     *          <code>null</code>.
     */

    protected AbstractAuctionHouseNode(org.osid.bidding.AuctionHouse auctionHouse) {
        setId(auctionHouse.getId());
        this.auctionHouse = auctionHouse;
        return;
    }


    /**
     *  Constructs a new <code>AbstractAuctionHouseNode</code> from a
     *  single auction house.
     *
     *  @param auctionHouse the auction house
     *  @param root <code>true</code> if this node is a root, 
     *         <code>false</code> otherwise
     *  @param leaf <code>true</code> if this node is a leaf, 
     *         <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>auctionHouse</code> is 
     *          <code>null</code>.
     */

    protected AbstractAuctionHouseNode(org.osid.bidding.AuctionHouse auctionHouse, boolean root, boolean leaf) {
        this(auctionHouse);

        if (root) {
            root();
        } else {
            unroot();
        }

        if (leaf) {
            leaf();
        } else {
            unleaf();
        }
        
        return;
    }


    /**
     *  Adds a parent to this auction house.
     *
     *  @param node the parent auction house node to add
     *  @throws org.osid.IllegalStateException this is a root
     *  @throws org.osid.NullArgumentException <code>node</code> is
     *          <code>null</code>
     */

    protected void addParent(org.osid.bidding.AuctionHouseNode node) {
        nullarg(node, "node");

        if (isRoot()) {
            throw new org.osid.IllegalStateException(getId() + " is a root");
        }

        this.parents.add(node);
        return;
    }


    /**
     *  Adds a child to this auction house.
     *
     *  @param node the child auction house node to add
     *  @throws org.osid.NullArgumentException <code>node</code> is
     *          <code>null</code>
     */

    public void addChild(org.osid.bidding.AuctionHouseNode node) {
        nullarg(node, "auction house node");
        this.children.add(node);
        return;
    }


    /**
     *  Gets the <code> AuctionHouse </code> at this node.
     *
     *  @return the auction house represented by this node
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouse getAuctionHouse() {
        return (this.auctionHouse);
    }


    /**
     *  Tests if any parents are available in this node structure. There may 
     *  be no more parents in this node structure however there may be parents 
     *  that exist in the hierarchy. 
     *
     *  @return <code> true </code> if this node has parents, <code> false
     *          </code> otherwise
     */

    @OSID @Override
    public boolean hasParents() {
        return (this.parents.size() > 0);
    }


    /**
     *  Tests if any children are available in this node structure. There may 
     *  be no more children available in this node structure but this node may 
     *  have children in the hierarchy. 
     *
     *  @return <code> true </code> if this node has children, <code>
     *          false </code> otherwise
     */
    
    @OSID @Override
    public boolean hasChildren() {
        return (this.children.size() > 0);
    }


    /**
     *  Gets the parents of this node.
     *
     *  @return the parents of this node
     */

    @OSID @Override
    public org.osid.id.IdList getParentIds() {
        return (new net.okapia.osid.jamocha.adapter.converter.bidding.auctionhousenode.AuctionHouseNodeToIdList(this.parents));
    }


    /**
     *  Gets the parents of this node.
     *
     *  @return the parents of the <code> id </code>
     */

    @OSID @Override
    public org.osid.hierarchy.NodeList getParents() {
        return (new net.okapia.osid.jamocha.adapter.converter.bidding.auctionhousenode.AuctionHouseNodeToNodeList(getParentAuctionHouseNodes()));
    }


    /**
     *  Gets the parents of this node.
     *
     *  @return the parents of the <code> id </code>
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseNodeList getParentAuctionHouseNodes() {
        return (new net.okapia.osid.jamocha.bidding.auctionhousenode.ArrayAuctionHouseNodeList(this.parents));
    }


    /**
     *  Gets the children of this node.
     *
     *  @return the children of this node
     */

    @OSID @Override
    public org.osid.id.IdList getChildIds() {
        return (new net.okapia.osid.jamocha.adapter.converter.bidding.auctionhousenode.AuctionHouseNodeToIdList(this.children));
    }


    /**
     *  Gets the children of this node.
     *
     *  @return the children of the <code> id </code>
     */

    @OSID @Override
    public org.osid.hierarchy.NodeList getChildren() {
        return (new net.okapia.osid.jamocha.adapter.converter.bidding.auctionhousenode.AuctionHouseNodeToNodeList(getChildAuctionHouseNodes()));
    }


    /**
     *  Gets the child nodes of this auction house.
     *
     *  @return the child nodes of this auction house
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseNodeList getChildAuctionHouseNodes() {
        return (new net.okapia.osid.jamocha.bidding.auctionhousenode.ArrayAuctionHouseNodeList(this.children));
    }
}
