//
// AbstractMutableCheck.java
//
//     Defines a mutable Check.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.rules.check.check.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Check</code>.
 */

public abstract class AbstractMutableCheck
    extends net.okapia.osid.jamocha.rules.check.check.spi.AbstractCheck
    implements org.osid.rules.check.Check,
               net.okapia.osid.jamocha.builder.rules.check.check.CheckMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this check. 
     *
     *  @param record check record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addCheckRecord(org.osid.rules.check.records.CheckRecord record, org.osid.type.Type recordType) {
        super.addCheckRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Enables this check. Enabling an operable overrides any
     *  enabling rule that may exist.
     *  
     *  @param enabled <code>true</code> if enabled, <code>false<code>
     *         otherwise
     */
    
    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        return;
    }


    /**
     *  Disables this check. Disabling an operable overrides any
     *  enabling rule that may exist.
     *
     *  @param disabled <code> true </code> if this object is
     *          disabled, <code> false </code> otherwise
     */
    
    @Override
    public void setDisabled(boolean disabled) {
        super.setDisabled(disabled);
        return;
    }


    /**
     *  Sets the operational flag.
     *
     *  @param operational <code>true</code>if operational,
     *         <code>false</code> if not operational
     */
    
    @Override
    public void setOperational(boolean operational) {
        super.setOperational(operational);
        return;
    }


    /**
     *  Sets the display name for this check.
     *
     *  @param displayName the name for this check
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this check.
     *
     *  @param description the description of this check
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException <code>genusType</code>
     *          is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the rule.
     *
     *  @param rule the rule
     *  @throws org.osid.NullArgumentException <code>rule</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setRule(org.osid.rules.Rule rule) {
        super.setRule(rule);
        return;
    }


    /**
     *  Sets the start date for a time check.
     *
     *  @param date a time check date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setTimeCheckStartDate(org.osid.calendaring.DateTime date) {
        super.setTimeCheckStartDate(date);
        return;
    }


    /**
     *  Sets the end date for a time check.
     *
     *  @param date a time check date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setTimeCheckEndDate(org.osid.calendaring.DateTime date) {
        super.setTimeCheckEndDate(date);
        return;
    }


    /**
     *  Sets the event for a time check.
     *
     *  @param event an event
     *  @throws org.osid.NullArgumentException <code>event</code> is
     *          <code>null</code>
     */

    @Override
    public void setTimeCheckEvent(org.osid.calendaring.Event event) {
        super.setTimeCheckEvent(event);
        return;
    }


    /**
     *  Sets the cyclic event for a time check.
     *
     *  @param cyclicEvent a time check cyclic event
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicEvent</code> is <code>null</code>
     */

    @Override
    public void setTimeCheckCyclicEvent(org.osid.calendaring.cycle.CyclicEvent cyclicEvent) {
        super.setTimeCheckCyclicEvent(cyclicEvent);
        return;
    }


    /**
     *  Sets the block for a hold check.
     *
     *  @param block a block
     *  @throws org.osid.NullArgumentException <code>block</code> is
     *          <code>null</code>
     */

    @Override
    public void setHoldCheckBlock(org.osid.hold.Block block) {
        super.setHoldCheckBlock(block);
        return;
    }


    /**
     *  Sets the audit for a inquiry check.
     *
     *  @param audit an inquiry check audit
     *  @throws org.osid.NullArgumentException <code>audit</code> is
     *          <code>null</code>
     */

    @Override
    public void setInquiryCheckAudit(org.osid.inquiry.Audit audit) {
        super.setInquiryCheckAudit(audit);
        return;
    }


    /**
     *  Sets the agenda for a process check.
     *
     *  @param agenda a process check agenda
     *  @throws org.osid.NullArgumentException <code>agenda</code> is
     *          <code>null</code>
     */

    @Override
    public void setProcessCheckAgenda(org.osid.rules.check.Agenda agenda) {
        super.setProcessCheckAgenda(agenda);
        return;
    }
}

