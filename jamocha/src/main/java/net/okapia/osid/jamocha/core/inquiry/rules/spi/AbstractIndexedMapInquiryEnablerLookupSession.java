//
// AbstractIndexedMapInquiryEnablerLookupSession.java
//
//    A simple framework for providing an InquiryEnabler lookup service
//    backed by a fixed collection of inquiry enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inquiry.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an InquiryEnabler lookup service backed by a
 *  fixed collection of inquiry enablers. The inquiry enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some inquiry enablers may be compatible
 *  with more types than are indicated through these inquiry enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>InquiryEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapInquiryEnablerLookupSession
    extends AbstractMapInquiryEnablerLookupSession
    implements org.osid.inquiry.rules.InquiryEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.inquiry.rules.InquiryEnabler> inquiryEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.inquiry.rules.InquiryEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.inquiry.rules.InquiryEnabler> inquiryEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.inquiry.rules.InquiryEnabler>());


    /**
     *  Makes an <code>InquiryEnabler</code> available in this session.
     *
     *  @param  inquiryEnabler an inquiry enabler
     *  @throws org.osid.NullArgumentException <code>inquiryEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putInquiryEnabler(org.osid.inquiry.rules.InquiryEnabler inquiryEnabler) {
        super.putInquiryEnabler(inquiryEnabler);

        this.inquiryEnablersByGenus.put(inquiryEnabler.getGenusType(), inquiryEnabler);
        
        try (org.osid.type.TypeList types = inquiryEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.inquiryEnablersByRecord.put(types.getNextType(), inquiryEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }

    
    /**
     *  Makes an array of inquiry enablers available in this session.
     *
     *  @param  inquiryEnablers an array of inquiry enablers
     *  @throws org.osid.NullArgumentException <code>inquiryEnablers<code>
     *          is <code>null</code>
     */

    @Override
    protected void putInquiryEnablers(org.osid.inquiry.rules.InquiryEnabler[] inquiryEnablers) {
        for (org.osid.inquiry.rules.InquiryEnabler inquiryEnabler : inquiryEnablers) {
            putInquiryEnabler(inquiryEnabler);
        }

        return;
    }


    /**
     *  Makes a collection of inquiry enablers available in this session.
     *
     *  @param  inquiryEnablers a collection of inquiry enablers
     *  @throws org.osid.NullArgumentException <code>inquiryEnablers<code>
     *          is <code>null</code>
     */

    @Override
    protected void putInquiryEnablers(java.util.Collection<? extends org.osid.inquiry.rules.InquiryEnabler> inquiryEnablers) {
        for (org.osid.inquiry.rules.InquiryEnabler inquiryEnabler : inquiryEnablers) {
            putInquiryEnabler(inquiryEnabler);
        }

        return;
    }


    /**
     *  Removes an inquiry enabler from this session.
     *
     *  @param inquiryEnablerId the <code>Id</code> of the inquiry enabler
     *  @throws org.osid.NullArgumentException <code>inquiryEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeInquiryEnabler(org.osid.id.Id inquiryEnablerId) {
        org.osid.inquiry.rules.InquiryEnabler inquiryEnabler;
        try {
            inquiryEnabler = getInquiryEnabler(inquiryEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.inquiryEnablersByGenus.remove(inquiryEnabler.getGenusType());

        try (org.osid.type.TypeList types = inquiryEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.inquiryEnablersByRecord.remove(types.getNextType(), inquiryEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeInquiryEnabler(inquiryEnablerId);
        return;
    }


    /**
     *  Gets an <code>InquiryEnablerList</code> corresponding to the given
     *  inquiry enabler genus <code>Type</code> which does not include
     *  inquiry enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known inquiry enablers or an error results. Otherwise,
     *  the returned list may contain only those inquiry enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  inquiryEnablerGenusType an inquiry enabler genus type 
     *  @return the returned <code>InquiryEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>inquiryEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerList getInquiryEnablersByGenusType(org.osid.type.Type inquiryEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inquiry.rules.inquiryenabler.ArrayInquiryEnablerList(this.inquiryEnablersByGenus.get(inquiryEnablerGenusType)));
    }


    /**
     *  Gets an <code>InquiryEnablerList</code> containing the given
     *  inquiry enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known inquiry enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  inquiry enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  inquiryEnablerRecordType an inquiry enabler record type 
     *  @return the returned <code>inquiryEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>inquiryEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerList getInquiryEnablersByRecordType(org.osid.type.Type inquiryEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inquiry.rules.inquiryenabler.ArrayInquiryEnablerList(this.inquiryEnablersByRecord.get(inquiryEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.inquiryEnablersByGenus.clear();
        this.inquiryEnablersByRecord.clear();

        super.close();

        return;
    }
}
