//
// InvariantMapGradebookColumnCalculationLookupSession
//
//    Implements a GradebookColumnCalculation lookup service backed by a fixed collection of
//    gradebookColumnCalculations.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.grading.calculation;


/**
 *  Implements a GradebookColumnCalculation lookup service backed by a fixed
 *  collection of gradebook column calculations. The gradebook column calculations are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapGradebookColumnCalculationLookupSession
    extends net.okapia.osid.jamocha.core.grading.calculation.spi.AbstractMapGradebookColumnCalculationLookupSession
    implements org.osid.grading.calculation.GradebookColumnCalculationLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapGradebookColumnCalculationLookupSession</code> with no
     *  gradebook column calculations.
     *  
     *  @param gradebook the gradebook
     *  @throws org.osid.NullArgumnetException {@code gradebook} is
     *          {@code null}
     */

    public InvariantMapGradebookColumnCalculationLookupSession(org.osid.grading.Gradebook gradebook) {
        setGradebook(gradebook);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapGradebookColumnCalculationLookupSession</code> with a single
     *  gradebook column calculation.
     *  
     *  @param gradebook the gradebook
     *  @param gradebookColumnCalculation a single gradebook column calculation
     *  @throws org.osid.NullArgumentException {@code gradebook} or
     *          {@code gradebookColumnCalculation} is <code>null</code>
     */

      public InvariantMapGradebookColumnCalculationLookupSession(org.osid.grading.Gradebook gradebook,
                                               org.osid.grading.calculation.GradebookColumnCalculation gradebookColumnCalculation) {
        this(gradebook);
        putGradebookColumnCalculation(gradebookColumnCalculation);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapGradebookColumnCalculationLookupSession</code> using an array
     *  of gradebook column calculations.
     *  
     *  @param gradebook the gradebook
     *  @param gradebookColumnCalculations an array of gradebook column calculations
     *  @throws org.osid.NullArgumentException {@code gradebook} or
     *          {@code gradebookColumnCalculations} is <code>null</code>
     */

      public InvariantMapGradebookColumnCalculationLookupSession(org.osid.grading.Gradebook gradebook,
                                               org.osid.grading.calculation.GradebookColumnCalculation[] gradebookColumnCalculations) {
        this(gradebook);
        putGradebookColumnCalculations(gradebookColumnCalculations);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapGradebookColumnCalculationLookupSession</code> using a
     *  collection of gradebook column calculations.
     *
     *  @param gradebook the gradebook
     *  @param gradebookColumnCalculations a collection of gradebook column calculations
     *  @throws org.osid.NullArgumentException {@code gradebook} or
     *          {@code gradebookColumnCalculations} is <code>null</code>
     */

      public InvariantMapGradebookColumnCalculationLookupSession(org.osid.grading.Gradebook gradebook,
                                               java.util.Collection<? extends org.osid.grading.calculation.GradebookColumnCalculation> gradebookColumnCalculations) {
        this(gradebook);
        putGradebookColumnCalculations(gradebookColumnCalculations);
        return;
    }
}
