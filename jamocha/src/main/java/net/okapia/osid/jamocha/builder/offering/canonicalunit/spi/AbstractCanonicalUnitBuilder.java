//
// AbstractCanonicalUnit.java
//
//     Defines a CanonicalUnit builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.offering.canonicalunit.spi;


/**
 *  Defines a <code>CanonicalUnit</code> builder.
 */

public abstract class AbstractCanonicalUnitBuilder<T extends AbstractCanonicalUnitBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOperableOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.offering.canonicalunit.CanonicalUnitMiter canonicalUnit;


    /**
     *  Constructs a new <code>AbstractCanonicalUnitBuilder</code>.
     *
     *  @param canonicalUnit the canonical unit to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractCanonicalUnitBuilder(net.okapia.osid.jamocha.builder.offering.canonicalunit.CanonicalUnitMiter canonicalUnit) {
        super(canonicalUnit);
        this.canonicalUnit = canonicalUnit;
        return;
    }


    /**
     *  Builds the canonical unit.
     *
     *  @return the new canonical unit
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.NullArgumentException <code>canonicalUnit</code>
     *          is <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.offering.CanonicalUnit build() {
        (new net.okapia.osid.jamocha.builder.validator.offering.canonicalunit.CanonicalUnitValidator(getValidations())).validate(this.canonicalUnit);
        return (new net.okapia.osid.jamocha.builder.offering.canonicalunit.ImmutableCanonicalUnit(this.canonicalUnit));
    }


    /**
     *  Gets the canonical unit. This method is used to get the miter
     *  interface for further updates. Use <code>build()</code> to
     *  finalize and validate construction.
     *
     *  @return the new canonicalUnit
     */

    @Override
    public net.okapia.osid.jamocha.builder.offering.canonicalunit.CanonicalUnitMiter getMiter() {
        return (this.canonicalUnit);
    }


    /**
     *  Sets the title.
     *
     *  @param title a title
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>title</code> is <code>null</code>
     */

    public T title(org.osid.locale.DisplayText title) {
        getMiter().setTitle(title);
        return (self());
    }


    /**
     *  Sets the code.
     *
     *  @param code a code
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>code</code> is <code>null</code>
     */

    public T code(String code) {
        getMiter().setCode(code);
        return (self());
    }


    /**
     *  Adds an offered cyclic time period.
     *
     *  @param offeredCyclicTimePeriod an offered cyclic time period
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>offeredCyclicTimePeriod</code> is <code>null</code>
     */

    public T offeredCyclicTimePeriod(org.osid.calendaring.cycle.CyclicTimePeriod offeredCyclicTimePeriod) {
        getMiter().addOfferedCyclicTimePeriod(offeredCyclicTimePeriod);
        return (self());
    }


    /**
     *  Sets all the offered cyclic time periods.
     *
     *  @param offeredCyclicTimePeriods a collection of offered cyclic time periods
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>offeredCyclicTimePeriods</code> is <code>null</code>
     */

    public T offeredCyclicTimePeriods(java.util.Collection<org.osid.calendaring.cycle.CyclicTimePeriod> offeredCyclicTimePeriods) {
        getMiter().setOfferedCyclicTimePeriods(offeredCyclicTimePeriods);
        return (self());
    }


    /**
     *  Adds a result option.
     *
     *  @param resultOption a result option
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>resultOption</code> is <code>null</code>
     */

    public T resultOption(org.osid.grading.GradeSystem resultOption) {
        getMiter().addResultOption(resultOption);
        return (self());
    }


    /**
     *  Sets all the result options.
     *
     *  @param resultOptions a collection of result options
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>resultOptions</code> is <code>null</code>
     */

    public T resultOptions(java.util.Collection<org.osid.grading.GradeSystem> resultOptions) {
        getMiter().setResultOptions(resultOptions);
        return (self());
    }


    /**
     *  Adds a sponsor.
     *
     *  @param sponsor a sponsor
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>sponsor</code> is <code>null</code>
     */

    public T sponsor(org.osid.resource.Resource sponsor) {
        getMiter().addSponsor(sponsor);
        return (self());
    }


    /**
     *  Sets all the sponsors.
     *
     *  @param sponsors a collection of sponsors
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>sponsors</code> is <code>null</code>
     */

    public T sponsors(java.util.Collection<org.osid.resource.Resource> sponsors) {
        getMiter().setSponsors(sponsors);
        return (self());
    }


    /**
     *  Adds a CanonicalUnit record.
     *
     *  @param record a canonical unit record
     *  @param recordType the type of canonical unit record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.offering.records.CanonicalUnitRecord record, org.osid.type.Type recordType) {
        getMiter().addCanonicalUnitRecord(record, recordType);
        return (self());
    }
}       


