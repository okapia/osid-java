//
// AbstractAssemblySignalQuery.java
//
//     A SignalQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.mapping.path.signal.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A SignalQuery that stores terms.
 */

public abstract class AbstractAssemblySignalQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRuleQuery
    implements org.osid.mapping.path.SignalQuery,
               org.osid.mapping.path.SignalQueryInspector,
               org.osid.mapping.path.SignalSearchOrder {

    private final java.util.Collection<org.osid.mapping.path.records.SignalQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.mapping.path.records.SignalQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.mapping.path.records.SignalSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblySignalQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblySignalQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the path <code> Id </code> for this query to match speed zones 
     *  along the given path. 
     *
     *  @param  pathId the path <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> pathId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchPathId(org.osid.id.Id pathId, boolean match) {
        getAssembler().addIdTerm(getPathIdColumn(), pathId, match);
        return;
    }


    /**
     *  Clears the path <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearPathIdTerms() {
        getAssembler().clearTerms(getPathIdColumn());
        return;
    }


    /**
     *  Gets the path <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPathIdTerms() {
        return (getAssembler().getIdTerms(getPathIdColumn()));
    }


    /**
     *  Specifies a preference for oredering the results by path. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPath(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getPathColumn(), style);
        return;
    }


    /**
     *  Gets the PathId column name.
     *
     * @return the column name
     */

    protected String getPathIdColumn() {
        return ("path_id");
    }


    /**
     *  Tests if a <code> PathQuery </code> is available. 
     *
     *  @return <code> true </code> if a path query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathQuery() {
        return (false);
    }


    /**
     *  Gets the query for a path. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the path query 
     *  @throws org.osid.UnimplementedException <code> supportsPathQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathQuery getPathQuery() {
        throw new org.osid.UnimplementedException("supportsPathQuery() is false");
    }


    /**
     *  Clears the path query terms. 
     */

    @OSID @Override
    public void clearPathTerms() {
        getAssembler().clearTerms(getPathColumn());
        return;
    }


    /**
     *  Gets the path query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.path.PathQueryInspector[] getPathTerms() {
        return (new org.osid.mapping.path.PathQueryInspector[0]);
    }


    /**
     *  Tests if a search order for the path is available. 
     *
     *  @return <code> true </code> if a path search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathSearchOrder() {
        return (false);
    }


    /**
     *  Gets a path search order. 
     *
     *  @return the path search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPathSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathSearchOrder getPathSearchOrder() {
        throw new org.osid.UnimplementedException("supportsPathSearchOrder() is false");
    }


    /**
     *  Gets the Path column name.
     *
     * @return the column name
     */

    protected String getPathColumn() {
        return ("path");
    }


    /**
     *  Matches signals containing the specified <code> Coordinate. </code> 
     *
     *  @param  coordinate a coordinate 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> coordinate </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCoordinate(org.osid.mapping.Coordinate coordinate, 
                                boolean match) {
        getAssembler().addCoordinateTerm(getCoordinateColumn(), coordinate, match);
        return;
    }


    /**
     *  Matches signals that have any coordinate assignment. 
     *
     *  @param  match <code> true </code> to match signals with any 
     *          coordinate, <code> false </code> to match signals with no 
     *          coordinates 
     */

    @OSID @Override
    public void matchAnyCoordinate(boolean match) {
        getAssembler().addCoordinateWildcardTerm(getCoordinateColumn(), match);
        return;
    }


    /**
     *  Clears the coordinate query terms. 
     */

    @OSID @Override
    public void clearCoordinateTerms() {
        getAssembler().clearTerms(getCoordinateColumn());
        return;
    }


    /**
     *  Gets the coordinate query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CoordinateTerm[] getCoordinateTerms() {
        return (getAssembler().getCoordinateTerms(getCoordinateColumn()));
    }


    /**
     *  Gets the Coordinate column name.
     *
     * @return the column name
     */

    protected String getCoordinateColumn() {
        return ("coordinate");
    }


    /**
     *  Matches signals within the specified <code>
     *  SpatialUnit. </code>
     *
     *  @param  spatialUnit a spatial unit 
     *  @param match <code> true </code> for a positive match, <code>
     *          false </code> for a negative match
     *  @throws org.osid.NullArgumentException <code> spatialUnit
     *          </code> is <code> null </code>
     */

    @OSID @Override
    public void matchContainingSpatialUnit(org.osid.mapping.SpatialUnit spatialUnit, 
                                           boolean match) {
        getAssembler().addSpatialUnitTerm(getContainingSpatialUnitColumn(), spatialUnit, match);
        return;
    }


    /**
     *  Clears the spatial unit terms. 
     */

    @OSID @Override
    public void clearContainingSpatialUnitTerms() {
        getAssembler().clearTerms(getContainingSpatialUnitColumn());
        return;
    }


    /**
     *  Gets the containing spatial unit query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.SpatialUnitTerm[] getContainingSpatialUnitTerms() {
        return (getAssembler().getSpatialUnitTerms(getContainingSpatialUnitColumn()));
    }


    /**
     *  Gets the ContainingSpatialUnit column name.
     *
     * @return the column name
     */

    protected String getContainingSpatialUnitColumn() {
        return ("containing_spatial_unit");
    }


    /**
     *  Sets the state <code> Id </code> for this query. 
     *
     *  @param  stateId the state <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stateId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchStateId(org.osid.id.Id stateId, boolean match) {
        getAssembler().addIdTerm(getStateIdColumn(), stateId, match);
        return;
    }


    /**
     *  Clears the state <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearStateIdTerms() {
        getAssembler().clearTerms(getStateIdColumn());
        return;
    }


    /**
     *  Gets the state <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStateIdTerms() {
        return (getAssembler().getIdTerms(getStateIdColumn()));
    }


    /**
     *  Gets the StateId column name.
     *
     * @return the column name
     */

    protected String getStateIdColumn() {
        return ("state_id");
    }


    /**
     *  Tests if a <code> StateQuery </code> is available. 
     *
     *  @return <code> true </code> if a state query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStateQuery() {
        return (false);
    }


    /**
     *  Gets the query for a state. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the state query 
     *  @throws org.osid.UnimplementedException <code> supportsStateQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateQuery getStateQuery() {
        throw new org.osid.UnimplementedException("supportsStateQuery() is false");
    }


    /**
     *  Clears the state query terms. 
     */

    @OSID @Override
    public void clearStateTerms() {
        getAssembler().clearTerms(getStateColumn());
        return;
    }


    /**
     *  Gets the state query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.process.StateQueryInspector[] getStateTerms() {
        return (new org.osid.process.StateQueryInspector[0]);
    }


    /**
     *  Gets the State column name.
     *
     * @return the column name
     */

    protected String getStateColumn() {
        return ("state");
    }


    /**
     *  Sets the map <code> Id </code> for this query. 
     *
     *  @param  mapId the map <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchMapId(org.osid.id.Id mapId, boolean match) {
        getAssembler().addIdTerm(getMapIdColumn(), mapId, match);
        return;
    }


    /**
     *  Clears the map <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearMapIdTerms() {
        getAssembler().clearTerms(getMapIdColumn());
        return;
    }


    /**
     *  Gets the map <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getMapIdTerms() {
        return (getAssembler().getIdTerms(getMapIdColumn()));
    }


    /**
     *  Gets the MapId column name.
     *
     * @return the column name
     */

    protected String getMapIdColumn() {
        return ("map_id");
    }


    /**
     *  Tests if a <code> MapQuery </code> is available. 
     *
     *  @return <code> true </code> if a map query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMapQuery() {
        return (false);
    }


    /**
     *  Gets the query for a map. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the map query 
     *  @throws org.osid.UnimplementedException <code> supportsMapQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MapQuery getMapQuery() {
        throw new org.osid.UnimplementedException("supportsMapQuery() is false");
    }


    /**
     *  Clears the map query terms. 
     */

    @OSID @Override
    public void clearMapTerms() {
        getAssembler().clearTerms(getMapColumn());
        return;
    }


    /**
     *  Gets the map query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.MapQueryInspector[] getMapTerms() {
        return (new org.osid.mapping.MapQueryInspector[0]);
    }


    /**
     *  Gets the Map column name.
     *
     * @return the column name
     */

    protected String getMapColumn() {
        return ("map");
    }


    /**
     *  Tests if this signal supports the given record
     *  <code>Type</code>.
     *
     *  @param  signalRecordType a signal record type 
     *  @return <code>true</code> if the signalRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>signalRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type signalRecordType) {
        for (org.osid.mapping.path.records.SignalQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(signalRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  signalRecordType the signal record type 
     *  @return the signal query record 
     *  @throws org.osid.NullArgumentException
     *          <code>signalRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(signalRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.records.SignalQueryRecord getSignalQueryRecord(org.osid.type.Type signalRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.path.records.SignalQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(signalRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(signalRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  signalRecordType the signal record type 
     *  @return the signal query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>signalRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(signalRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.records.SignalQueryInspectorRecord getSignalQueryInspectorRecord(org.osid.type.Type signalRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.path.records.SignalQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(signalRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(signalRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param signalRecordType the signal record type
     *  @return the signal search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>signalRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(signalRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.records.SignalSearchOrderRecord getSignalSearchOrderRecord(org.osid.type.Type signalRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.path.records.SignalSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(signalRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(signalRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this signal. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param signalQueryRecord the signal query record
     *  @param signalQueryInspectorRecord the signal query inspector
     *         record
     *  @param signalSearchOrderRecord the signal search order record
     *  @param signalRecordType signal record type
     *  @throws org.osid.NullArgumentException
     *          <code>signalQueryRecord</code>,
     *          <code>signalQueryInspectorRecord</code>,
     *          <code>signalSearchOrderRecord</code> or
     *          <code>signalRecordTypesignal</code> is
     *          <code>null</code>
     */
            
    protected void addSignalRecords(org.osid.mapping.path.records.SignalQueryRecord signalQueryRecord, 
                                      org.osid.mapping.path.records.SignalQueryInspectorRecord signalQueryInspectorRecord, 
                                      org.osid.mapping.path.records.SignalSearchOrderRecord signalSearchOrderRecord, 
                                      org.osid.type.Type signalRecordType) {

        addRecordType(signalRecordType);

        nullarg(signalQueryRecord, "signal query record");
        nullarg(signalQueryInspectorRecord, "signal query inspector record");
        nullarg(signalSearchOrderRecord, "signal search odrer record");

        this.queryRecords.add(signalQueryRecord);
        this.queryInspectorRecords.add(signalQueryInspectorRecord);
        this.searchOrderRecords.add(signalSearchOrderRecord);
        
        return;
    }
}
