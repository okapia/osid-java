//
// AbstractAdapterSignalLookupSession.java
//
//    A Signal lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.mapping.path.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Signal lookup session adapter.
 */

public abstract class AbstractAdapterSignalLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.mapping.path.SignalLookupSession {

    private final org.osid.mapping.path.SignalLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterSignalLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterSignalLookupSession(org.osid.mapping.path.SignalLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Map/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Map Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getMapId() {
        return (this.session.getMapId());
    }


    /**
     *  Gets the {@code Map} associated with this session.
     *
     *  @return the {@code Map} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.Map getMap()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getMap());
    }


    /**
     *  Tests if this user can perform {@code Signal} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupSignals() {
        return (this.session.canLookupSignals());
    }


    /**
     *  A complete view of the {@code Signal} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeSignalView() {
        this.session.useComparativeSignalView();
        return;
    }


    /**
     *  A complete view of the {@code Signal} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenarySignalView() {
        this.session.usePlenarySignalView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include signals in maps which are children
     *  of this map in the map hierarchy.
     */

    @OSID @Override
    public void useFederatedMapView() {
        this.session.useFederatedMapView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this map only.
     */

    @OSID @Override
    public void useIsolatedMapView() {
        this.session.useIsolatedMapView();
        return;
    }
    

    /**
     *  Only active signals are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveSignalView() {
        this.session.useActiveSignalView();
        return;
    }


    /**
     *  Active and inactive signals are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusSignalView() {
        this.session.useAnyStatusSignalView();
        return;
    }
    
     
    /**
     *  Gets the {@code Signal} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Signal} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Signal} and
     *  retained for compatibility.
     *
     *  In active mode, signals are returned that are currently
     *  active. In any status mode, active and inactive signals
     *  are returned.
     *
     *  @param signalId {@code Id} of the {@code Signal}
     *  @return the signal
     *  @throws org.osid.NotFoundException {@code signalId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code signalId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.Signal getSignal(org.osid.id.Id signalId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSignal(signalId));
    }


    /**
     *  Gets a {@code SignalList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  signals specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Signals} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, signals are returned that are currently
     *  active. In any status mode, active and inactive signals
     *  are returned.
     *
     *  @param  signalIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Signal} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code signalIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalList getSignalsByIds(org.osid.id.IdList signalIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSignalsByIds(signalIds));
    }


    /**
     *  Gets a {@code SignalList} corresponding to the given
     *  signal genus {@code Type} which does not include
     *  signals of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  signals or an error results. Otherwise, the returned list
     *  may contain only those signals that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, signals are returned that are currently
     *  active. In any status mode, active and inactive signals
     *  are returned.
     *
     *  @param  signalGenusType a signal genus type 
     *  @return the returned {@code Signal} list
     *  @throws org.osid.NullArgumentException
     *          {@code signalGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalList getSignalsByGenusType(org.osid.type.Type signalGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSignalsByGenusType(signalGenusType));
    }


    /**
     *  Gets a {@code SignalList} corresponding to the given
     *  signal genus {@code Type} and include any additional
     *  signals with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  signals or an error results. Otherwise, the returned list
     *  may contain only those signals that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, signals are returned that are currently
     *  active. In any status mode, active and inactive signals
     *  are returned.
     *
     *  @param  signalGenusType a signal genus type 
     *  @return the returned {@code Signal} list
     *  @throws org.osid.NullArgumentException
     *          {@code signalGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalList getSignalsByParentGenusType(org.osid.type.Type signalGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSignalsByParentGenusType(signalGenusType));
    }


    /**
     *  Gets a {@code SignalList} containing the given
     *  signal record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  signals or an error results. Otherwise, the returned list
     *  may contain only those signals that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, signals are returned that are currently
     *  active. In any status mode, active and inactive signals
     *  are returned.
     *
     *  @param  signalRecordType a signal record type 
     *  @return the returned {@code Signal} list
     *  @throws org.osid.NullArgumentException
     *          {@code signalRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalList getSignalsByRecordType(org.osid.type.Type signalRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSignalsByRecordType(signalRecordType));
    }


    /**
     *  Gets a {@code SignalList} containing the given path. 
     *  
     *  In plenary mode, the returned list contains all known signals
     *  or an error results. Otherwise, the returned list may contain
     *  only those signals that are accessible through this session.
     *  
     *  In active mode, signals are returned that are currently
     *  active. In any status mode, active and inactive signals are
     *  returned.
     *
     *  @param  pathId a path {@code Id} 
     *  @return the returned {@code Signal} list 
     *  @throws org.osid.NullArgumentException {@code pathId} is {@code 
     *          null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalList getSignalsForPath(org.osid.id.Id pathId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSignalsForPath(pathId));
    }

    
    /**
     *  Gets a {@code SignalList} containing the given path between the 
     *  given coordinates inclusive. 
     *  
     *  In plenary mode, the returned list contains all known signals or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  signals that are accessible through this session. 
     *  
     *  In active mode, signals are returned that are currently active. In any 
     *  status mode, active and inactive signals are returned. 
     *
     *  @param  pathId a path {@code Id} 
     *  @param  coordinate starting coordinate 
     *  @param  distance a distance from coordinate 
     *  @return the returned {@code Signal} list 
     *  @throws org.osid.NullArgumentException {@code pathId,
     *         coordinate} or {@code distance} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalList getSignalsForPathAtCoordinate(org.osid.id.Id pathId, 
                                                                          org.osid.mapping.Coordinate coordinate, 
                                                                          org.osid.mapping.Distance distance)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSignalsForPathAtCoordinate(pathId, coordinate, distance));
    }

    
    /**
     *  Gets all {@code Signals}. 
     *
     *  In plenary mode, the returned list contains all known
     *  signals or an error results. Otherwise, the returned list
     *  may contain only those signals that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, signals are returned that are currently
     *  active. In any status mode, active and inactive signals
     *  are returned.
     *
     *  @return a list of {@code Signals} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalList getSignals()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSignals());
    }
}
