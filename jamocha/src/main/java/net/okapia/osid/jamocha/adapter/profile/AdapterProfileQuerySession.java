//
// AdapterProfileQuerySession.java
//
//    A ProfileQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.profile;


/**
 *  A ProfileQuerySession adapter.
 */

public final class AdapterProfileQuerySession
    extends net.okapia.osid.jamocha.adapter.profile.spi.AbstractAdapterProfileQuerySession
    implements org.osid.profile.ProfileQuerySession {


    /**
     *  Constructs a new AdapterProfileQuerySession.
     *
     *  @param session the underlying profile query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    public AdapterProfileQuerySession(org.osid.profile.ProfileQuerySession session) {
        super(session);
        return;
    }


    /**
     *  Constructs a new AdapterProfileQuerySession.
     *
     *  @param session the underlying profile query session
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code session} or
     *          {@code proxy} is {@code null}
     */

    public AdapterProfileQuerySession(org.osid.profile.ProfileQuerySession session,
                                       org.osid.proxy.Proxy proxy) {
        super(session);
        setSessionProxy(proxy);
        return;
    }
}
