//
// ProgramRequirementElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.requisite.programrequirement.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ProgramRequirementElements
    extends net.okapia.osid.jamocha.spi.OsidRuleElements {


    /**
     *  Gets the ProgramRequirementElement Id.
     *
     *  @return the program requirement element Id
     */

    public static org.osid.id.Id getProgramRequirementEntityId() {
        return (makeEntityId("osid.course.requisite.ProgramRequirement"));
    }


    /**
     *  Gets the AltRequisites element Id.
     *
     *  @return the AltRequisites element Id
     */

    public static org.osid.id.Id getAltRequisites() {
        return (makeElementId("osid.course.requisite.programrequirement.AltRequisites"));
    }


    /**
     *  Gets the ProgramId element Id.
     *
     *  @return the ProgramId element Id
     */

    public static org.osid.id.Id getProgramId() {
        return (makeElementId("osid.course.requisite.programrequirement.ProgramId"));
    }


    /**
     *  Gets the Program element Id.
     *
     *  @return the Program element Id
     */

    public static org.osid.id.Id getProgram() {
        return (makeElementId("osid.course.requisite.programrequirement.Program"));
    }


    /**
     *  Gets the Timeframe element Id.
     *
     *  @return the Timeframe element Id
     */

    public static org.osid.id.Id getTimeframe() {
        return (makeElementId("osid.course.requisite.programrequirement.Timeframe"));
    }


    /**
     *  Gets the MinimumGPASystemId element Id.
     *
     *  @return the MinimumGPASystemId element Id
     */

    public static org.osid.id.Id getMinimumGPASystemId() {
        return (makeElementId("osid.course.requisite.programrequirement.MinimumGPASystemId"));
    }


    /**
     *  Gets the MinimumGPASystem element Id.
     *
     *  @return the MinimumGPASystem element Id
     */

    public static org.osid.id.Id getMinimumGPASystem() {
        return (makeElementId("osid.course.requisite.programrequirement.MinimumGPASystem"));
    }


    /**
     *  Gets the MinimumGPA element Id.
     *
     *  @return the MinimumGPA element Id
     */

    public static org.osid.id.Id getMinimumGPA() {
        return (makeElementId("osid.course.requisite.programrequirement.MinimumGPA"));
    }


    /**
     *  Gets the MinimumEarnedCredits element Id.
     *
     *  @return the MinimumEarnedCredits element Id
     */

    public static org.osid.id.Id getMinimumEarnedCredits() {
        return (makeElementId("osid.course.requisite.programrequirement.MinimumEarnedCredits"));
    }


    /**
     *  Gets the RequiresCompletion element Id.
     *
     *  @return the RequiresCompletion element Id
     */

    public static org.osid.id.Id getRequiresCompletion() {
        return (makeElementId("osid.course.requisite.programrequirement.RequiresCompletion"));
    }
}
