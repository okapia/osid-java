//
// AbstractQueryBidLookupSession.java
//
//    An inline adapter that maps a BidLookupSession to
//    a BidQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.bidding.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a BidLookupSession to
 *  a BidQuerySession.
 */

public abstract class AbstractQueryBidLookupSession
    extends net.okapia.osid.jamocha.bidding.spi.AbstractBidLookupSession
    implements org.osid.bidding.BidLookupSession {

    private boolean effectiveonly = false;
    private final org.osid.bidding.BidQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryBidLookupSession.
     *
     *  @param querySession the underlying bid query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryBidLookupSession(org.osid.bidding.BidQuerySession querySession) {
        nullarg(querySession, "bid query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>AuctionHouse</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>AuctionHouse Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAuctionHouseId() {
        return (this.session.getAuctionHouseId());
    }


    /**
     *  Gets the <code>AuctionHouse</code> associated with this 
     *  session.
     *
     *  @return the <code>AuctionHouse</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouse getAuctionHouse()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getAuctionHouse());
    }


    /**
     *  Tests if this user can perform <code>Bid</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupBids() {
        return (this.session.canSearchBids());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include bids in auction houses which are children
     *  of this auction house in the auction house hierarchy.
     */

    @OSID @Override
    public void useFederatedAuctionHouseView() {
        this.session.useFederatedAuctionHouseView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this auction house only.
     */

    @OSID @Override
    public void useIsolatedAuctionHouseView() {
        this.session.useIsolatedAuctionHouseView();
        return;
    }
    

    /**
     *  Only bids whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveBidView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All bids of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveBidView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Bid</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Bid</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Bid</code> and
     *  retained for compatibility.
     *
     *  In effective mode, bids are returned that are currently
     *  effective.  In any effective mode, effective bids and
     *  those currently expired are returned.
     *
     *  @param  bidId <code>Id</code> of the
     *          <code>Bid</code>
     *  @return the bid
     *  @throws org.osid.NotFoundException <code>bidId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>bidId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.Bid getBid(org.osid.id.Id bidId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.BidQuery query = getQuery();
        query.matchId(bidId, true);
        org.osid.bidding.BidList bids = this.session.getBidsByQuery(query);
        if (bids.hasNext()) {
            return (bids.getNextBid());
        } 
        
        throw new org.osid.NotFoundException(bidId + " not found");
    }


    /**
     *  Gets a <code>BidList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  bids specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Bids</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, bids are returned that are currently effective.
     *  In any effective mode, effective bids and those currently expired
     *  are returned.
     *
     *  @param  bidIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Bid</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>bidIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.BidList getBidsByIds(org.osid.id.IdList bidIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.BidQuery query = getQuery();

        try (org.osid.id.IdList ids = bidIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getBidsByQuery(query));
    }


    /**
     *  Gets a <code>BidList</code> corresponding to the given
     *  bid genus <code>Type</code> which does not include
     *  bids of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  bids or an error results. Otherwise, the returned list
     *  may contain only those bids that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, bids are returned that are currently effective.
     *  In any effective mode, effective bids and those currently expired
     *  are returned.
     *
     *  @param  bidGenusType a bid genus type 
     *  @return the returned <code>Bid</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>bidGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.BidList getBidsByGenusType(org.osid.type.Type bidGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.BidQuery query = getQuery();
        query.matchGenusType(bidGenusType, true);
        return (this.session.getBidsByQuery(query));
    }


    /**
     *  Gets a <code>BidList</code> corresponding to the given
     *  bid genus <code>Type</code> and include any additional
     *  bids with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  bids or an error results. Otherwise, the returned list
     *  may contain only those bids that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, bids are returned that are currently
     *  effective.  In any effective mode, effective bids and
     *  those currently expired are returned.
     *
     *  @param  bidGenusType a bid genus type 
     *  @return the returned <code>Bid</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>bidGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.BidList getBidsByParentGenusType(org.osid.type.Type bidGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.BidQuery query = getQuery();
        query.matchParentGenusType(bidGenusType, true);
        return (this.session.getBidsByQuery(query));
    }


    /**
     *  Gets a <code>BidList</code> containing the given
     *  bid record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  bids or an error results. Otherwise, the returned list
     *  may contain only those bids that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, bids are returned that are currently
     *  effective.  In any effective mode, effective bids and
     *  those currently expired are returned.
     *
     *  @param  bidRecordType a bid record type 
     *  @return the returned <code>Bid</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>bidRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.BidList getBidsByRecordType(org.osid.type.Type bidRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.BidQuery query = getQuery();
        query.matchRecordType(bidRecordType, true);
        return (this.session.getBidsByQuery(query));
    }


    /**
     *  Gets a <code>BidList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  bids or an error results. Otherwise, the returned list
     *  may contain only those bids that are accessible
     *  through this session.
     *  
     *  In effective mode, bids are returned that are currently
     *  effective.  In any effective mode, effective bids and
     *  those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Bid</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.bidding.BidList getBidsOnDate(org.osid.calendaring.DateTime from, 
                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.BidQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getBidsByQuery(query));
    }
        

    /**
     *  Gets a list of bids corresponding to an auction
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  bids or an error results. Otherwise, the returned list
     *  may contain only those bids that are accessible
     *  through this session.
     *
     *  In effective mode, bids are returned that are
     *  currently effective.  In any effective mode, effective
     *  bids and those currently expired are returned.
     *
     *  @param  auctionId the <code>Id</code> of the auction
     *  @return the returned <code>BidList</code>
     *  @throws org.osid.NullArgumentException <code>auctionId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.bidding.BidList getBidsForAuction(org.osid.id.Id auctionId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.bidding.BidQuery query = getQuery();
        query.matchAuctionId(auctionId, true);
        return (this.session.getBidsByQuery(query));
    }


    /**
     *  Gets a list of bids corresponding to an auction
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  bids or an error results. Otherwise, the returned list
     *  may contain only those bids that are accessible
     *  through this session.
     *
     *  In effective mode, bids are returned that are
     *  currently effective.  In any effective mode, effective
     *  bids and those currently expired are returned.
     *
     *  @param  auctionId the <code>Id</code> of the auction
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>BidList</code>
     *  @throws org.osid.NullArgumentException <code>auctionId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.bidding.BidList getBidsForAuctionOnDate(org.osid.id.Id auctionId,
                                                            org.osid.calendaring.DateTime from,
                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.BidQuery query = getQuery();
        query.matchAuctionId(auctionId, true);
        query.matchDate(from, to, true);
        return (this.session.getBidsByQuery(query));
    }


    /**
     *  Gets a list of bids corresponding to a bidder
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  bids or an error results. Otherwise, the returned list
     *  may contain only those bids that are accessible
     *  through this session.
     *
     *  In effective mode, bids are returned that are
     *  currently effective.  In any effective mode, effective
     *  bids and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>BidList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.bidding.BidList getBidsForBidder(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.bidding.BidQuery query = getQuery();
        query.matchBidderId(resourceId, true);
        return (this.session.getBidsByQuery(query));
    }


    /**
     *  Gets a list of bids corresponding to a bidder
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  bids or an error results. Otherwise, the returned list
     *  may contain only those bids that are accessible
     *  through this session.
     *
     *  In effective mode, bids are returned that are
     *  currently effective.  In any effective mode, effective
     *  bids and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>BidList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.bidding.BidList getBidsForBidderOnDate(org.osid.id.Id resourceId,
                                                           org.osid.calendaring.DateTime from,
                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.BidQuery query = getQuery();
        query.matchBidderId(resourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getBidsByQuery(query));
    }


    /**
     *  Gets a list of bids corresponding to auction and bidder
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  bids or an error results. Otherwise, the returned list
     *  may contain only those bids that are accessible
     *  through this session.
     *
     *  In effective mode, bids are returned that are
     *  currently effective.  In any effective mode, effective
     *  bids and those currently expired are returned.
     *
     *  @param  auctionId the <code>Id</code> of the auction
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>BidList</code>
     *  @throws org.osid.NullArgumentException <code>auctionId</code>,
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.bidding.BidList getBidsForAuctionAndBidder(org.osid.id.Id auctionId,
                                                               org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.bidding.BidQuery query = getQuery();
        query.matchAuctionId(auctionId, true);
        query.matchBidderId(resourceId, true);
        return (this.session.getBidsByQuery(query));
    }


    /**
     *  Gets a list of bids corresponding to auction and bidder
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  bids or an error results. Otherwise, the returned list
     *  may contain only those bids that are accessible
     *  through this session.
     *
     *  In effective mode, bids are returned that are
     *  currently effective.  In any effective mode, effective
     *  bids and those currently expired are returned.
     *
     *  @param  auctionId the <code>Id</code> of the auction
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>BidList</code>
     *  @throws org.osid.NullArgumentException <code>auctionId</code>,
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.bidding.BidList getBidsForAuctionAndBidderOnDate(org.osid.id.Id auctionId,
                                                                     org.osid.id.Id resourceId,
                                                                     org.osid.calendaring.DateTime from,
                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.bidding.BidQuery query = getQuery();
        query.matchAuctionId(auctionId, true);
        query.matchBidderId(resourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getBidsByQuery(query));
    }


    /**
     *  Gets all winning <code> Bids. </code> 
     *  
     *  <code> </code> In plenary mode, the returned list contains all
     *  known bids or an error results. Otherwise, the returned list
     *  may contain only those bids that are accessible through this
     *  session. In both cases, the order of the set is by start
     *  effective date.
     *  
     *  In effective mode, bids are returned that are currently
     *  effective. In any effective mode, effective bids and those
     *  currently expired are returned.
     *
     *  @return a list of <code> Bids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.bidding.BidList getWinningBids()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.bidding.BidQuery query = getQuery();
        query.matchWinner(true);
        return (this.session.getBidsByQuery(query));
    }


    /**
     *  Gets a list of all winning bids effective during the entire given date 
     *  range inclusive but not confined to the date range. 
     *  
     *  In plenary mode, the returned list contains all known bids or
     *  an error results. Otherwise, the returned list may contain
     *  only those bids that are accessible through this session. In
     *  both cases, the order of the set is by the start of the
     *  effective date.
     *  
     *  In effective mode, bids are returned that are currently
     *  effective. In any effective mode, effective bids and those
     *  currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code> Bid </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.bidding.BidList getWinningBidsOnDate(org.osid.calendaring.DateTime from, 
                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.BidQuery query = getQuery();
        query.matchWinner(true);
        query.matchDate(from, to, true);
        return (this.session.getBidsByQuery(query));
    }


    /**
     *  Gets a list of all winning bids for an auction. 
     *  
     *  In plenary mode, the returned list contains all known auctions
     *  or an error results. Otherwise, the returned list may contain
     *  only those auctions that are accessible through this session.
     *  
     *  In effective mode, bids are returned that are currently
     *  effective. In any effective mode, effective bids and those
     *  currently expired are returned.
     *
     *  @param  auctionId an auction <code> Id </code> 
     *  @return the returned <code> Bid </code> list 
     *  @throws org.osid.NullArgumentException <code> auctionId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.bidding.BidList getWinningBidsForAuction(org.osid.id.Id auctionId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.BidQuery query = getQuery();
        query.matchWinner(true);
        query.matchAuctionId(auctionId, true);
        return (this.session.getBidsByQuery(query));
    }


    /**
     *  Gets a list of winning bids for an auction and effectiveduring the 
     *  entire given date range inclusive but not confined to the date range. 
     *  
     *  In plenary mode, the returned list contains all known bids or an error 
     *  results. Otherwise, the returned list may contain only those bids that 
     *  are accessible through this session. 
     *  
     *  In effective mode, bids are returned that are currently effective. In 
     *  any effective mode, effective bids and those currently expired are 
     *  returned. 
     *
     *  @param  auctionId an auction <code> Id </code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code> Bid </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> auctionId, from, </code> 
     *          or <code> to </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.bidding.BidList getWinningBidsForAuctionOnDate(org.osid.id.Id auctionId, 
                                                                   org.osid.calendaring.DateTime from, 
                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.BidQuery query = getQuery();
        query.matchWinner(true);
        query.matchAuctionId(auctionId, true);
        query.matchDate(from, to, true);
        return (this.session.getBidsByQuery(query));
    }

    
    /**
     *  Gets a list of all winning bids for a bidder. 
     *  
     *  In plenary mode, the returned list contains all known auctions
     *  or an error results. Otherwise, the returned list may contain
     *  only those auctions that are accessible through this session.
     *  
     *  In effective mode, bids are returned that are currently
     *  effective. In any effective mode, effective bids and those
     *  currently expired are returned.
     *
     *  @param  resourceId a resourceId <code> Id </code> 
     *  @return the returned <code> Bid </code> list 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.bidding.BidList getWinningBidsForBidder(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.BidQuery query = getQuery();
        query.matchWinner(true);
        query.matchBidderId(resourceId, true);
        return (this.session.getBidsByQuery(query));
    }


    /**
     *  Gets a list of winning bids for a bidder and effective during the 
     *  entire given date range inclusive but not confined to the date range. 
     *  
     *  In plenary mode, the returned list contains all known bids or an error 
     *  results. Otherwise, the returned list may contain only those bids that 
     *  are accessible through this session. 
     *  
     *  In effective mode, bids are returned that are currently effective. In 
     *  any effective mode, effective bids and those currently expired are 
     *  returned. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned C <code> ommission </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> resourceId, from, 
     *          </code> or <code> to </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.bidding.BidList getWinningBidsForBidderOnDate(org.osid.id.Id resourceId, 
                                                                  org.osid.calendaring.DateTime from, 
                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.BidQuery query = getQuery();
        query.matchWinner(true);
        query.matchBidderId(resourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getBidsByQuery(query));
    }


    /**
     *  Gets all <code>Bids</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  bids or an error results. Otherwise, the returned list
     *  may contain only those bids that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, bids are returned that are currently
     *  effective.  In any effective mode, effective bids and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Bids</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.BidList getBids()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.BidQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getBidsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.bidding.BidQuery getQuery() {
        org.osid.bidding.BidQuery query = this.session.getBidQuery();
        
        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }

        return (query);
    }
}
