//
// BlockElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.hold.block.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class BlockElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the BlockElement Id.
     *
     *  @return the block element Id
     */

    public static org.osid.id.Id getBlockEntityId() {
        return (makeEntityId("osid.hold.Block"));
    }


    /**
     *  Gets the IssueIds element Id.
     *
     *  @return the IssueIds element Id
     */

    public static org.osid.id.Id getIssueIds() {
        return (makeElementId("osid.hold.block.IssueIds"));
    }


    /**
     *  Gets the Issues element Id.
     *
     *  @return the Issues element Id
     */

    public static org.osid.id.Id getIssues() {
        return (makeElementId("osid.hold.block.Issues"));
    }


    /**
     *  Gets the OublietteId element Id.
     *
     *  @return the OublietteId element Id
     */

    public static org.osid.id.Id getOublietteId() {
        return (makeQueryElementId("osid.hold.block.OublietteId"));
    }


    /**
     *  Gets the Oubliette element Id.
     *
     *  @return the Oubliette element Id
     */

    public static org.osid.id.Id getOubliette() {
        return (makeQueryElementId("osid.hold.block.Oubliette"));
    }
}
