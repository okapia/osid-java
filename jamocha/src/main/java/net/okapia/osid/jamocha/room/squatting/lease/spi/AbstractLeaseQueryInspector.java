//
// AbstractLeaseQueryInspector.java
//
//     A template for making a LeaseQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.squatting.lease.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for leases.
 */

public abstract class AbstractLeaseQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQueryInspector
    implements org.osid.room.squatting.LeaseQueryInspector {

    private final java.util.Collection<org.osid.room.squatting.records.LeaseQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the room <code> Id </code> terms. 
     *
     *  @return the room <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRoomIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the room terms. 
     *
     *  @return the room terms 
     */

    @OSID @Override
    public org.osid.room.RoomQueryInspector[] getRoomTerms() {
        return (new org.osid.room.RoomQueryInspector[0]);
    }


    /**
     *  Gets the tenant <code> Id </code> terms. 
     *
     *  @return the tenant <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getTenantIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the tenant terms. 
     *
     *  @return the tenant terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getTenantTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the campus <code> Id </code> terms. 
     *
     *  @return the campus <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCampusIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the campus terms. 
     *
     *  @return the campus terms 
     */

    @OSID @Override
    public org.osid.room.CampusQueryInspector[] getCampusTerms() {
        return (new org.osid.room.CampusQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given lease query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a lease implementing the requested record.
     *
     *  @param leaseRecordType a lease record type
     *  @return the lease query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>leaseRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(leaseRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.room.squatting.records.LeaseQueryInspectorRecord getLeaseQueryInspectorRecord(org.osid.type.Type leaseRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.squatting.records.LeaseQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(leaseRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(leaseRecordType + " is not supported");
    }


    /**
     *  Adds a record to this lease query. 
     *
     *  @param leaseQueryInspectorRecord lease query inspector
     *         record
     *  @param leaseRecordType lease record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addLeaseQueryInspectorRecord(org.osid.room.squatting.records.LeaseQueryInspectorRecord leaseQueryInspectorRecord, 
                                                   org.osid.type.Type leaseRecordType) {

        addRecordType(leaseRecordType);
        nullarg(leaseRecordType, "lease record type");
        this.records.add(leaseQueryInspectorRecord);        
        return;
    }
}
