//
// JobBatchFormFilterList.java
//
//     Implements a filtering JobBatchFormList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.resourcing.batch.jobbatchform;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filtering JobBatchFormList.
 */

public final class JobBatchFormFilterList
    extends net.okapia.osid.jamocha.inline.filter.resourcing.batch.jobbatchform.spi.AbstractJobBatchFormFilterList
    implements org.osid.resourcing.batch.JobBatchFormList,
               JobBatchFormFilter {

    private final JobBatchFormFilter filter;


    /**
     *  Creates a new <code>JobBatchFormFilterList</code>.
     *
     *  @param filter an inline query filter
     *  @param list a <code>JobBatchFormList</code>
     *  @throws org.osid.NullArgumentException <code>filter</code> or
     *          <code>list</code> is <code>null</code>
     */

    public JobBatchFormFilterList(JobBatchFormFilter filter, org.osid.resourcing.batch.JobBatchFormList list) {
        super(list);

        nullarg(filter, "inline query filter");
        this.filter = filter;

        return;
    }    

    
    /**
     *  Filters JobBatchForms.
     *
     *  @param jobBatchForm the job batch form to filter
     *  @return <code>true</code> if the job batch form passes the filter,
     *          <code>false</code> if the job batch form should be filtered
     */

    @Override
    public boolean pass(org.osid.resourcing.batch.JobBatchForm jobBatchForm) {
        return (this.filter.pass(jobBatchForm));
    }
}
