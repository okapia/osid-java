//
// AbstractMappingPathProxyManager.java
//
//     An adapter for a MappingPathProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.mapping.path.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a MappingPathProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterMappingPathProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.mapping.path.MappingPathProxyManager>
    implements org.osid.mapping.path.MappingPathProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterMappingPathProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterMappingPathProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterMappingPathProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterMappingPathProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any map federation is exposed. Federation is exposed when a 
     *  specific map may be identified, selected and used to create a lookup 
     *  or admin session. Federation is not exposed when a set of maps appears 
     *  as a single map. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up paths is supported. 
     *
     *  @return <code> true </code> if path lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathLookup() {
        return (getAdapteeManager().supportsPathLookup());
    }


    /**
     *  Tests if querying paths is supported. 
     *
     *  @return <code> true </code> if path query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathQuery() {
        return (getAdapteeManager().supportsPathQuery());
    }


    /**
     *  Tests if searching paths is supported. 
     *
     *  @return <code> true </code> if path search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathSearch() {
        return (getAdapteeManager().supportsPathSearch());
    }


    /**
     *  Tests if path <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if path administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathAdmin() {
        return (getAdapteeManager().supportsPathAdmin());
    }


    /**
     *  Tests if a path <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if path notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathNotification() {
        return (getAdapteeManager().supportsPathNotification());
    }


    /**
     *  Tests if a path map lookup service is supported. 
     *
     *  @return <code> true </code> if a path map lookup service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathMap() {
        return (getAdapteeManager().supportsPathMap());
    }


    /**
     *  Tests if a path map service is supported. 
     *
     *  @return <code> true </code> if path to map assignment service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathMapAssignment() {
        return (getAdapteeManager().supportsPathMapAssignment());
    }


    /**
     *  Tests if a path smart map lookup service is supported. 
     *
     *  @return <code> true </code> if a path smart map service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathSmartMap() {
        return (getAdapteeManager().supportsPathSmartMap());
    }


    /**
     *  Tests if a path spatial lookup service is supported. 
     *
     *  @return <code> true </code> if a path spatial service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathSpatial() {
        return (getAdapteeManager().supportsPathSpatial());
    }


    /**
     *  Tests if a path spatial design service is supported. 
     *
     *  @return <code> true </code> if a path spatial design service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathSpatialDesign() {
        return (getAdapteeManager().supportsPathSpatialDesign());
    }


    /**
     *  Tests if a path travel service is supported. 
     *
     *  @return <code> true </code> if a path travel service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathTravel() {
        return (getAdapteeManager().supportsPathTravel());
    }


    /**
     *  Tests if a resource path notification service is supported. 
     *
     *  @return <code> true </code> if a resource path notification service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourcePathNotification() {
        return (getAdapteeManager().supportsResourcePathNotification());
    }


    /**
     *  Tests if a resource velocity service is supported. 
     *
     *  @return <code> true </code> if a resource velocity service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceVelocity() {
        return (getAdapteeManager().supportsResourceVelocity());
    }


    /**
     *  Tests if a resource velocity update service is supported. 
     *
     *  @return <code> true </code> if a resource velocity update service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceVelocityUpdate() {
        return (getAdapteeManager().supportsResourceVelocityUpdate());
    }


    /**
     *  Tests if a resource velocity notification service is supported. 
     *
     *  @return <code> true </code> if a resource velocity notification 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceVelocityNotification() {
        return (getAdapteeManager().supportsResourceVelocityNotification());
    }


    /**
     *  Tests if a my path service is supported. 
     *
     *  @return <code> true </code> if a my path service is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMyPath() {
        return (getAdapteeManager().supportsMyPath());
    }


    /**
     *  Tests if an intersection lookup service is supported. 
     *
     *  @return <code> true </code> if an intersection lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIntersectionLookup() {
        return (getAdapteeManager().supportsIntersectionLookup());
    }


    /**
     *  Tests if querying intersections is supported. 
     *
     *  @return <code> true </code> if intersection query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIntersectionQuery() {
        return (getAdapteeManager().supportsIntersectionQuery());
    }


    /**
     *  Tests if searching intersections is supported. 
     *
     *  @return <code> true </code> if intersection search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIntersectionSearch() {
        return (getAdapteeManager().supportsIntersectionSearch());
    }


    /**
     *  Tests if an intersection administrative service is supported. 
     *
     *  @return <code> true </code> if an intersection administrative service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIntersectionAdmin() {
        return (getAdapteeManager().supportsIntersectionAdmin());
    }


    /**
     *  Tests if an intersection <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if intersection notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIntersectionNotification() {
        return (getAdapteeManager().supportsIntersectionNotification());
    }


    /**
     *  Tests if an intersection map lookup service is supported. 
     *
     *  @return <code> true </code> if an intersection map lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIntersectionMap() {
        return (getAdapteeManager().supportsIntersectionMap());
    }


    /**
     *  Tests if an intersection map service is supported. 
     *
     *  @return <code> true </code> if intersection to map assignment service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIntersectionMapAssignment() {
        return (getAdapteeManager().supportsIntersectionMapAssignment());
    }


    /**
     *  Tests if an intersection smart map lookup service is supported. 
     *
     *  @return <code> true </code> if an intersection smart map service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIntersectionSmartMap() {
        return (getAdapteeManager().supportsIntersectionSmartMap());
    }


    /**
     *  Tests if looking up speed zones is supported. 
     *
     *  @return <code> true </code> if speed zone lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSpeedZoneLookup() {
        return (getAdapteeManager().supportsSpeedZoneLookup());
    }


    /**
     *  Tests if querying speed zones is supported. 
     *
     *  @return <code> true </code> if speed zone query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSpeedZoneQuery() {
        return (getAdapteeManager().supportsSpeedZoneQuery());
    }


    /**
     *  Tests if searching speed zones is supported. 
     *
     *  @return <code> true </code> if speed zone search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSpeedZoneSearch() {
        return (getAdapteeManager().supportsSpeedZoneSearch());
    }


    /**
     *  Tests if speed zone <code> </code> administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if speed zone administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSpeedZoneAdmin() {
        return (getAdapteeManager().supportsSpeedZoneAdmin());
    }


    /**
     *  Tests if a speed zone <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if speed zone notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSpeedZoneNotification() {
        return (getAdapteeManager().supportsSpeedZoneNotification());
    }


    /**
     *  Tests if a speed zone map lookup service is supported. 
     *
     *  @return <code> true </code> if a speed zone map lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSpeedZoneMap() {
        return (getAdapteeManager().supportsSpeedZoneMap());
    }


    /**
     *  Tests if a speed zone map assignment service is supported. 
     *
     *  @return <code> true </code> if a speed zone to map assignment service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSpeedZoneMapAssignment() {
        return (getAdapteeManager().supportsSpeedZoneMapAssignment());
    }


    /**
     *  Tests if a speed zone smart map service is supported. 
     *
     *  @return <code> true </code> if a speed zone smart map service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSpeedZoneSmartMap() {
        return (getAdapteeManager().supportsSpeedZoneSmartMap());
    }


    /**
     *  Tests if looking up signals is supported. 
     *
     *  @return <code> true </code> if signal lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSignalLookup() {
        return (getAdapteeManager().supportsSignalLookup());
    }


    /**
     *  Tests if querying signals is supported. 
     *
     *  @return <code> true </code> if signal query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSignalQuery() {
        return (getAdapteeManager().supportsSignalQuery());
    }


    /**
     *  Tests if searching signals is supported. 
     *
     *  @return <code> true </code> if signal search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSignalSearch() {
        return (getAdapteeManager().supportsSignalSearch());
    }


    /**
     *  Tests if signal <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if signal administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSignalAdmin() {
        return (getAdapteeManager().supportsSignalAdmin());
    }


    /**
     *  Tests if a signal <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if signal notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSignalNotification() {
        return (getAdapteeManager().supportsSignalNotification());
    }


    /**
     *  Tests if a signal map lookup service is supported. 
     *
     *  @return <code> true </code> if a signal map lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSignalMap() {
        return (getAdapteeManager().supportsSignalMap());
    }


    /**
     *  Tests if a signal map assignment service is supported. 
     *
     *  @return <code> true </code> if a signal to map assignment service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSignalMapAssignment() {
        return (getAdapteeManager().supportsSignalMapAssignment());
    }


    /**
     *  Tests if a signal smart map service is supported. 
     *
     *  @return <code> true </code> if a signal smart map service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSignalSmartMap() {
        return (getAdapteeManager().supportsSignalSmartMap());
    }


    /**
     *  Tests if a resource signal notification service is supported. 
     *
     *  @return <code> true </code> if a resource signal notification service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceSignalNotification() {
        return (getAdapteeManager().supportsResourceSignalNotification());
    }


    /**
     *  Tests if looking up obstacles is supported. 
     *
     *  @return <code> true </code> if obstacle lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObstacleLookup() {
        return (getAdapteeManager().supportsObstacleLookup());
    }


    /**
     *  Tests if querying obstacles is supported. 
     *
     *  @return <code> true </code> if obstacle query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObstacleQuery() {
        return (getAdapteeManager().supportsObstacleQuery());
    }


    /**
     *  Tests if searching obstacles is supported. 
     *
     *  @return <code> true </code> if obstacle search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObstacleSearch() {
        return (getAdapteeManager().supportsObstacleSearch());
    }


    /**
     *  Tests if obstacle administrative service is supported. 
     *
     *  @return <code> true </code> if obstacle administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObstacleAdmin() {
        return (getAdapteeManager().supportsObstacleAdmin());
    }


    /**
     *  Tests if an obstacle <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if obstacle notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObstacleNotification() {
        return (getAdapteeManager().supportsObstacleNotification());
    }


    /**
     *  Tests if an obstacle <code> </code> hierarchy service is supported. 
     *
     *  @return <code> true </code> if obstacle hierarchy is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObstacleHierarchy() {
        return (getAdapteeManager().supportsObstacleHierarchy());
    }


    /**
     *  Tests if an obstacle hierarchy design service is supported. 
     *
     *  @return <code> true </code> if obstacle hierarchy design is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObstacleHierarchyDesign() {
        return (getAdapteeManager().supportsObstacleHierarchyDesign());
    }


    /**
     *  Tests if an obstacle map lookup service is supported. 
     *
     *  @return <code> true </code> if an obstacle map lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObstacleMap() {
        return (getAdapteeManager().supportsObstacleMap());
    }


    /**
     *  Tests if an obstacle map assignment service is supported. 
     *
     *  @return <code> true </code> if an obstacle to map assignment service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObstacleMapAssignment() {
        return (getAdapteeManager().supportsObstacleMapAssignment());
    }


    /**
     *  Tests if an obstacle smart map service is supported. 
     *
     *  @return <code> true </code> if an obstacle smart map service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObstacleSmartMap() {
        return (getAdapteeManager().supportsObstacleSmartMap());
    }


    /**
     *  Tests if a batch mapping path service is supported. 
     *
     *  @return <code> true </code> if a mapping path batch service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMappingPathBatch() {
        return (getAdapteeManager().supportsMappingPathBatch());
    }


    /**
     *  Tests if a mapping path rules service is supported. 
     *
     *  @return <code> true </code> if a mapping path rules service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMappingPathRules() {
        return (getAdapteeManager().supportsMappingPathRules());
    }


    /**
     *  Gets the supported <code> Path </code> record types. 
     *
     *  @return a list containing the supported <code> Path </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPathRecordTypes() {
        return (getAdapteeManager().getPathRecordTypes());
    }


    /**
     *  Tests if the given <code> Path </code> record type is supported. 
     *
     *  @param  pathRecordType a <code> Type </code> indicating a <code> Path 
     *          </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> pathRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPathRecordType(org.osid.type.Type pathRecordType) {
        return (getAdapteeManager().supportsPathRecordType(pathRecordType));
    }


    /**
     *  Gets the supported <code> Path </code> search record types. 
     *
     *  @return a list containing the supported <code> Path </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPathSearchRecordTypes() {
        return (getAdapteeManager().getPathSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Path </code> search record type is 
     *  supported. 
     *
     *  @param  pathSearchRecordType a <code> Type </code> indicating a <code> 
     *          Path </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> pathSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPathSearchRecordType(org.osid.type.Type pathSearchRecordType) {
        return (getAdapteeManager().supportsPathSearchRecordType(pathSearchRecordType));
    }


    /**
     *  Gets the supported <code> Intersection </code> record types. 
     *
     *  @return a list containing the supported <code> Intersection </code> 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getIntersectionRecordTypes() {
        return (getAdapteeManager().getIntersectionRecordTypes());
    }


    /**
     *  Tests if the given <code> Intersection </code> record type is 
     *  supported. 
     *
     *  @param  intersectionRecordType a <code> Type </code> indicating an 
     *          <code> Intersection </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> intersectionRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsIntersectionRecordType(org.osid.type.Type intersectionRecordType) {
        return (getAdapteeManager().supportsIntersectionRecordType(intersectionRecordType));
    }


    /**
     *  Gets the supported <code> Intersection </code> search record types. 
     *
     *  @return a list containing the supported <code> Intersection </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getIntersectionSearchRecordTypes() {
        return (getAdapteeManager().getIntersectionSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Intersection </code> search record type is 
     *  supported. 
     *
     *  @param  intersectionSearchRecordType a <code> Type </code> indicating 
     *          an <code> Intersection </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          intersectionSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsIntersectionSearchRecordType(org.osid.type.Type intersectionSearchRecordType) {
        return (getAdapteeManager().supportsIntersectionSearchRecordType(intersectionSearchRecordType));
    }


    /**
     *  Gets the supported <code> SpeedZone </code> record types. 
     *
     *  @return a list containing the supported <code> SpeedZone </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSpeedZoneRecordTypes() {
        return (getAdapteeManager().getSpeedZoneRecordTypes());
    }


    /**
     *  Tests if the given <code> SpeedZone </code> record type is supported. 
     *
     *  @param  speedZoneRecordType a <code> Type </code> indicating a <code> 
     *          SpeedZone </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> speedZoneRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSpeedZoneRecordType(org.osid.type.Type speedZoneRecordType) {
        return (getAdapteeManager().supportsSpeedZoneRecordType(speedZoneRecordType));
    }


    /**
     *  Gets the supported <code> SpeedZone </code> search types. 
     *
     *  @return a list containing the supported <code> SpeedZone </code> 
     *          search types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSpeedZoneSearchRecordTypes() {
        return (getAdapteeManager().getSpeedZoneSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> SpeedZone </code> search type is supported. 
     *
     *  @param  speedZoneSearchRecordType a <code> Type </code> indicating a 
     *          <code> SpeedZone </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          speedZoneSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSpeedZoneSearchRecordType(org.osid.type.Type speedZoneSearchRecordType) {
        return (getAdapteeManager().supportsSpeedZoneSearchRecordType(speedZoneSearchRecordType));
    }


    /**
     *  Gets the supported <code> Signal </code> record types. 
     *
     *  @return a list containing the supported <code> Signal </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSignalRecordTypes() {
        return (getAdapteeManager().getSignalRecordTypes());
    }


    /**
     *  Tests if the given <code> Signal </code> record type is supported. 
     *
     *  @param  signalRecordType a <code> Type </code> indicating a <code> 
     *          Signal </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> signalRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSignalRecordType(org.osid.type.Type signalRecordType) {
        return (getAdapteeManager().supportsSignalRecordType(signalRecordType));
    }


    /**
     *  Gets the supported <code> Signal </code> search types. 
     *
     *  @return a list containing the supported <code> Signal </code> search 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSignalSearchRecordTypes() {
        return (getAdapteeManager().getSignalSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Signal </code> search type is supported. 
     *
     *  @param  signalSearchRecordType a <code> Type </code> indicating a 
     *          <code> Signal </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> signalSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSignalSearchRecordType(org.osid.type.Type signalSearchRecordType) {
        return (getAdapteeManager().supportsSignalSearchRecordType(signalSearchRecordType));
    }


    /**
     *  Gets the supported <code> Obstacle </code> record types. 
     *
     *  @return a list containing the supported <code> Obstacle </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getObstacleRecordTypes() {
        return (getAdapteeManager().getObstacleRecordTypes());
    }


    /**
     *  Tests if the given <code> Obstacle </code> record type is supported. 
     *
     *  @param  obstacleRecordType a <code> Type </code> indicating an <code> 
     *          Obstacle </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> obstacleRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsObstacleRecordType(org.osid.type.Type obstacleRecordType) {
        return (getAdapteeManager().supportsObstacleRecordType(obstacleRecordType));
    }


    /**
     *  Gets the supported <code> Obstacle </code> search types. 
     *
     *  @return a list containing the supported <code> Obstacle </code> search 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getObstacleSearchRecordTypes() {
        return (getAdapteeManager().getObstacleSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Obstacle </code> search type is supported. 
     *
     *  @param  obstacleSearchRecordType a <code> Type </code> indicating an 
     *          <code> Obstacle </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> obstacleSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsObstacleSearchRecordType(org.osid.type.Type obstacleSearchRecordType) {
        return (getAdapteeManager().supportsObstacleSearchRecordType(obstacleSearchRecordType));
    }


    /**
     *  Gets the supported <code> ResourceVelocity </code> record types. 
     *
     *  @return a list containing the supported <code> ResourceVelocity 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getResourceVelocityRecordTypes() {
        return (getAdapteeManager().getResourceVelocityRecordTypes());
    }


    /**
     *  Tests if the given <code> ResourceVelocity </code> record type is 
     *  supported. 
     *
     *  @param  resourceVelocityRecordType a <code> Type </code> indicating a 
     *          <code> ResourceVelocity </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          resourceVelocityRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsResourceVelocityRecordType(org.osid.type.Type resourceVelocityRecordType) {
        return (getAdapteeManager().supportsResourceVelocityRecordType(resourceVelocityRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PathLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathLookupSession getPathLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPathLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path lookup 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PathLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathLookupSession getPathLookupSessionForMap(org.osid.id.Id mapId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPathLookupSessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PathQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathQuerySession getPathQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPathQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path query 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PathQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathQuerySession getPathQuerySessionForMap(org.osid.id.Id mapId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPathQuerySessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PathSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathSearchSession getPathSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPathSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path search 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PathSearchSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathSearchSession getPathSearchSessionForMap(org.osid.id.Id mapId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPathSearchSessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PathAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathAdminSession getPathAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPathAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path 
     *  administration service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PathAdminSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathAdminSession getPathAdminSessionForMap(org.osid.id.Id mapId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPathAdminSessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path 
     *  notification service. 
     *
     *  @param  pathReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> PathNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> pathReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPathNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathNotificationSession getPathNotificationSession(org.osid.mapping.path.PathReceiver pathReceiver, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPathNotificationSession(pathReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path 
     *  notification service for the given map. 
     *
     *  @param  pathReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PathNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> pathReceiver, mapId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPathNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathNotificationSession getPathNotificationSessionForMap(org.osid.mapping.path.PathReceiver pathReceiver, 
                                                                                          org.osid.id.Id mapId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPathNotificationSessionForMap(pathReceiver, mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup path/map mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PathMapSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathMap() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathMapSession getPathMapSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPathMapSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning paths to 
     *  maps. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PathMapAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPathMapAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathMapAssignmentSession getPathMapAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPathMapAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage path smart maps. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PathSmartMapSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathSmartMap() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathSmartMapSession getPathSmartMapSession(org.osid.id.Id mapId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPathSmartMapSession(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path spatial 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PathSpatialSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathSpatial() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathSpatialSession getPathSpatialSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPathSpatialSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path spatial 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PathSpatialSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathSpatial() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathSpatialSession getPathSpatialSessionForMap(org.osid.id.Id mapId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPathSpatialSessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path spatial 
     *  design service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PathSpatialDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPathSpatialDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathSpatialDesignSession getPathSpatialDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPathSpatialDesignSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path spatial 
     *  design service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PathSpatialDesignSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPathSpatialDesign() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathSpatialDesignSession getPathSpatialDesignSessionForMap(org.osid.id.Id mapId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPathSpatialDesignSessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path travel 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PathTravelSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathTravel() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathTravelSession getPathTravelSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPathTravelSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path travel 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PathTravelSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathTravel() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathTravelSession getPathTravelSessionForMap(org.osid.id.Id mapId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPathTravelSessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource path 
     *  notification service. 
     *
     *  @param  resourcePathReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> ResourcePathNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> resourcePathReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourcePathNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ResourcePathNotificationSession getResourcePathNotificationSession(org.osid.mapping.path.ResourcePathReceiver resourcePathReceiver, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResourcePathNotificationSession(resourcePathReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource path 
     *  notification service for the given map. 
     *
     *  @param  resourcePathReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ResourcePathNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> resourcePathReceiver, 
     *          mapId </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourcePathNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ResourcePathNotificationSession getResourcePathNotificationSessionForMap(org.osid.mapping.path.ResourcePathReceiver resourcePathReceiver, 
                                                                                                          org.osid.id.Id mapId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getResourcePathNotificationSessionForMap(resourcePathReceiver, mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  velocity service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ResourceVelocitySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceVelocity() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ResourceVelocitySession getResourceVelocitySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceVelocitySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  velocity service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ResourceVelocitySession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceVelocity() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ResourceVelocitySession getResourceVelocitySessionForMap(org.osid.id.Id mapId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceVelocitySessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  velocity update service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ResourceVelocityUpdateSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceVelocityUpdate() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ResourceVelocityUpdateSession getResourceVelocityUpdateSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceVelocityUpdateSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  velocity update service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ResourceVelocityUpdateSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceVelocityUpdate() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ResourceVelocityUpdateSession getResourceVelocityUpdateSessionForMap(org.osid.id.Id mapId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceVelocityUpdateSessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  velocity notification service. 
     *
     *  @param  resourceVelocityReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> ResourceVelocityNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> resourceVelocityReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceVelocityNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ResourceVelocityNotificationSession getResourceVelocityNotificationSession(org.osid.mapping.path.ResourceVelocityReceiver resourceVelocityReceiver, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceVelocityNotificationSession(resourceVelocityReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  velocity notification service for the given map. 
     *
     *  @param  resourceVelocityReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ResourceVelocityNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          resourceVelocityReceiver, mapId </code> or <code> proxy 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceVelocityNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ResourceVelocityNotificationSession getResourceVelocityNotificationSessionForMap(org.osid.mapping.path.ResourceVelocityReceiver resourceVelocityReceiver, 
                                                                                                                  org.osid.id.Id mapId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceVelocityNotificationSessionForMap(resourceVelocityReceiver, mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my path 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MyPathSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyPath() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.MyPathSession getMyPathSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMyPathSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my path 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> MyPathSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyPath() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.MyPathSession getMyPathSessionForMap(org.osid.id.Id mapId, 
                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getMyPathSessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the intersection 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IntersectionLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectionLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionLookupSession getIntersectionLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getIntersectionLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the intersection 
     *  lookup service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return an <code> IntersectionLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectionLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionLookupSession getIntersectionLookupSessionForMap(org.osid.id.Id mapId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getIntersectionLookupSessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the intersection 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IntersectionQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionQuerySession getIntersectionQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getIntersectionQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the intersection 
     *  query service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return an <code> IntersectionQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectionQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionQuerySession getIntersectionQuerySessionForMap(org.osid.id.Id mapId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getIntersectionQuerySessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the intersection 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IntersectionSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectionSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionSearchSession getIntersectionSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getIntersectionSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the intersection 
     *  search service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return an <code> IntersectionSearchSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectionSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionSearchSession getIntersectionSearchSessionForMap(org.osid.id.Id mapId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getIntersectionSearchSessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the intersection 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IntersectionAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectionAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionAdminSession getIntersectionAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getIntersectionAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the intersection 
     *  administrative service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return an <code> IntersectionAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectionAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionAdminSession getIntersectionAdminSessionForMap(org.osid.id.Id mapId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getIntersectionAdminSessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the intersection 
     *  notification service. 
     *
     *  @param  intersectionReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> IntersectionNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> intersectionReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectionNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionNotificationSession getIntersectionNotificationSession(org.osid.mapping.path.IntersectionReceiver intersectionReceiver, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getIntersectionNotificationSession(intersectionReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the intersection 
     *  notification service for the given map. 
     *
     *  @param  intersectionReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return an <code> IntersectionNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> intersectionReceiver, 
     *          mapId </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectionNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionNotificationSession getIntersectionNotificationSessionForMap(org.osid.mapping.path.IntersectionReceiver intersectionReceiver, 
                                                                                                          org.osid.id.Id mapId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getIntersectionNotificationSessionForMap(intersectionReceiver, mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup intersection/map 
     *  mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IntersectionMapSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectionMap() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionMapSession getIntersectionMapSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getIntersectionMapSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  intersections to maps. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IntersectionMapAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectionMapAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionMapAssignmentSession getIntersectionMapAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getIntersectionMapAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage intersection smart maps. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return an <code> IntersectionSmartMapSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectionSmartMap() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionSmartMapSession getIntersectionSmartMapSession(org.osid.id.Id mapId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getIntersectionSmartMapSession(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneLookupSession getSpeedZoneLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSpeedZoneLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  lookup service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the map 
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneLookupSession getSpeedZoneLookupSessionForMap(org.osid.id.Id mapId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSpeedZoneLookupSessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneQuerySession getSpeedZoneQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSpeedZoneQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  query service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the map 
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneQuerySession getSpeedZoneQuerySessionForMap(org.osid.id.Id mapId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSpeedZoneQuerySessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneSearchSession getSpeedZoneSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSpeedZoneSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  search service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneSearchSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneSearchSession getSpeedZoneSearchSessionForMap(org.osid.id.Id mapId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSpeedZoneSearchSessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneAdminSession getSpeedZoneAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSpeedZoneAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  administration service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneAdminSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneAdminSession getSpeedZoneAdminSessionForMap(org.osid.id.Id mapId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSpeedZoneAdminSessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  notification service. 
     *
     *  @param  speedZoneReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> speedZoneReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneNotificationSession getSpeedZoneNotificationSession(org.osid.mapping.path.SpeedZoneReceiver speedZoneReceiver, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSpeedZoneNotificationSession(speedZoneReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  notification service for the given map. 
     *
     *  @param  speedZoneReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> speedZoneReceiver, mapId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneNotificationSession getSpeedZoneNotificationSessionForMap(org.osid.mapping.path.SpeedZoneReceiver speedZoneReceiver, 
                                                                                                    org.osid.id.Id mapId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSpeedZoneNotificationSessionForMap(speedZoneReceiver, mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup speed zone/map mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneMapSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSpeedZoneMap() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneMapSession getSpeedZoneMapSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSpeedZoneMapSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning speed 
     *  zones to maps. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneMapAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneMapAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneMapAssignmentSession getSpeedZoneMapAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSpeedZoneMapAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage speed zone smart maps. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneSmartMapSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneSmartMap() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneSmartMapSession getSpeedZoneSmartMapSession(org.osid.id.Id mapId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSpeedZoneSmartMapSession(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SignalLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSignalLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalLookupSession getSignalLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSignalLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal lookup 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the map 
     *  @param  proxy a proxy 
     *  @return a <code> SignalLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSignalLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalLookupSession getSignalLookupSessionForMap(org.osid.id.Id mapId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSignalLookupSessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SignalQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSignalQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalQuerySession getSignalQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSignalQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal query 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the map 
     *  @param  proxy a proxy 
     *  @return a <code> SignalQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSignalQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalQuerySession getSignalQuerySessionForMap(org.osid.id.Id mapId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSignalQuerySessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SignalSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSignalSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalSearchSession getSignalSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSignalSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal search 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SignalSearchSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSignalSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalSearchSession getSignalSearchSessionForMap(org.osid.id.Id mapId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSignalSearchSessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SignalAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSignalAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalAdminSession getSignalAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSignalAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal 
     *  administration service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SignalAdminSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSignalAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalAdminSession getSignalAdminSessionForMap(org.osid.id.Id mapId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSignalAdminSessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal 
     *  notification service. 
     *
     *  @param  signalReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> SignalNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> signalReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalNotificationSession getSignalNotificationSession(org.osid.mapping.path.SignalReceiver signalReceiver, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSignalNotificationSession(signalReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal 
     *  notification service for the given map. 
     *
     *  @param  signalReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SignalNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> signalReceiver, mapId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalNotificationSession getSignalNotificationSessionForMap(org.osid.mapping.path.SignalReceiver signalReceiver, 
                                                                                              org.osid.id.Id mapId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSignalNotificationSessionForMap(signalReceiver, mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup signal/map mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SignalMapSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSignalMap() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalMapSession getSignalMapSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSignalMapSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning signals 
     *  to maps. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SignalMapAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalMapAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalMapAssignmentSession getSignalMapAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSignalMapAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage signal smart maps. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SignalSmartMapSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalSmartMap() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalSmartMapSession getSignalSmartMapSession(org.osid.id.Id mapId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSignalSmartMapSession(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  signal notification service. 
     *
     *  @param  resourceSignaReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> ResourceSignaNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> resourceSignaReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceSignaNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ResourceSignalNotificationSession getResourceSignalNotificationSession(org.osid.mapping.path.ResourceVelocityReceiver resourceSignaReceiver, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceSignalNotificationSession(resourceSignaReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  signal notification service for the given map. 
     *
     *  @param  resourceSignaReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ResourceSignaNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> resourceSignaReceiver, 
     *          </code> <code> mapId, </code> or <code> proxy </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceSignaNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ResourceSignalNotificationSession getResourceSignaNotificationSessionForMap(org.osid.mapping.path.ResourceVelocityReceiver resourceSignaReceiver, 
                                                                                                             org.osid.id.Id mapId, 
                                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceSignaNotificationSessionForMap(resourceSignaReceiver, mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleLookupSession getObstacleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getObstacleLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  lookup service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the map 
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleLookupSession getObstacleLookupSessionForMap(org.osid.id.Id mapId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getObstacleLookupSessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsObstacleQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleQuerySession getObstacleQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getObstacleQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle query 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleQuerySession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsObstacleQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleQuerySession getObstacleQuerySessionForMap(org.osid.id.Id mapId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getObstacleQuerySessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleSearchSession getObstacleSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getObstacleSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  search service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleSearchSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleSearchSession getObstacleSearchSessionForMap(org.osid.id.Id mapId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getObstacleSearchSessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsObstacleAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleAdminSession getObstacleAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getObstacleAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  administration service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleAdminSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsObstacleAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleAdminSession getObstacleAdminSessionForMap(org.osid.id.Id mapId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getObstacleAdminSessionForMap(mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  notification service. 
     *
     *  @param  obstacleReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> obstacleReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleNotificationSession getObstacleNotificationSession(org.osid.mapping.path.ObstacleReceiver obstacleReceiver, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getObstacleNotificationSession(obstacleReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  notification service for the given map. 
     *
     *  @param  obstacleReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> obstacleReceiver, mapId, 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleNotificationSession getObstacleNotificationSessionForMap(org.osid.mapping.path.ObstacleReceiver obstacleReceiver, 
                                                                                                  org.osid.id.Id mapId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getObstacleNotificationSessionForMap(obstacleReceiver, mapId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup obstacle/map mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleMapSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsObstacleMap() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleMapSession getObstacleMapSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getObstacleMapSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  obstacles to maps. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleMapAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleMapAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleMapAssignmentSession getObstacleMapAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getObstacleMapAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage locatin smart maps. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleSmartMapSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleSmartMap() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleSmartMapSession getObstacleSmartMapSession(org.osid.id.Id mapId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getObstacleSmartMapSession(mapId, proxy));
    }


    /**
     *  Gets a <code> MappingPathBatchproxyManager. </code> 
     *
     *  @return a <code> MappingPathBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMappingPathBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.batch.MappingPathBatchProxyManager getMappingPathBatchProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMappingPathBatchProxyManager());
    }


    /**
     *  Gets a <code> MappingPathRulesProxyManager. </code> 
     *
     *  @return a <code> MappingPathRulesProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMappingPathRules() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.MappingPathRulesProxyManager getMappingPathRulesProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMappingPathRulesProxyManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
