//
// AbstractSetting.java
//
//     Defines a Setting builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.control.setting.spi;


/**
 *  Defines a <code>Setting</code> builder.
 */

public abstract class AbstractSettingBuilder<T extends AbstractSettingBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.control.setting.SettingMiter setting;


    /**
     *  Constructs a new <code>AbstractSettingBuilder</code>.
     *
     *  @param setting the setting to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractSettingBuilder(net.okapia.osid.jamocha.builder.control.setting.SettingMiter setting) {
        super(setting);
        this.setting = setting;
        return;
    }


    /**
     *  Builds the setting.
     *
     *  @return the new setting
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.NullArgumentException <code>setting</code>
     *          is <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.control.Setting build() {
        (new net.okapia.osid.jamocha.builder.validator.control.setting.SettingValidator(getValidations())).validate(this.setting);
        return (new net.okapia.osid.jamocha.builder.control.setting.ImmutableSetting(this.setting));
    }


    /**
     *  Gets the setting. This method is used to get the miter
     *  interface for further updates. Use <code>build()</code> to
     *  finalize and validate construction.
     *
     *  @return the new setting
     */

    @Override
    public net.okapia.osid.jamocha.builder.control.setting.SettingMiter getMiter() {
        return (this.setting);
    }


    /**
     *  Sets the controller.
     *
     *  @param controller a controller
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>controller</code> is <code>null</code>
     */

    public T controller(org.osid.control.Controller controller) {
        getMiter().setController(controller);
        return (self());
    }


    /**
     *  Sets the on flag.
     *
     *  @param on {@code true} is on, {@code false} if off or unknown
     */

    public T on(boolean on) {
        getMiter().setOn(on);
        return (self());
    }


    /**
     *  Sets the off flag.
     *
     *  @param off {@code true} is off, {@code false} if on or unknown
     */

    public T off(boolean off) {
        getMiter().setOff(off);
        return (self());
    }


    /**
     *  Sets the variable amount.
     *
     *  @param amount a variable amount
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    public T variableAmount(java.math.BigDecimal amount) {
        getMiter().setVariableAmount(amount);
        return (self());
    }


    /**
     *  Sets the discreet state.
     *
     *  @param state a discreet state
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>state</code> is
     *          <code>null</code>
     */

    public T discreetState(org.osid.process.State state) {
        getMiter().setDiscreetState(state);
        return (self());
    }


    /**
     *  Sets the ramp rate.
     *
     *  @param rate a ramp rate
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>rate</code> is
     *          <code>null</code>
     */

    public T rampRate(org.osid.calendaring.Duration rate) {
        getMiter().setRampRate(rate);
        return (self());
    }


    /**
     *  Adds a Setting record.
     *
     *  @param record a setting record
     *  @param recordType the type of setting record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.control.records.SettingRecord record, org.osid.type.Type recordType) {
        getMiter().addSettingRecord(record, recordType);
        return (self());
    }
}       


