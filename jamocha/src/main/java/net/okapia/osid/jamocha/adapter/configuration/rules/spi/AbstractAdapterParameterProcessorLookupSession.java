//
// AbstractAdapterParameterProcessorLookupSession.java
//
//    A ParameterProcessor lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.configuration.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A ParameterProcessor lookup session adapter.
 */

public abstract class AbstractAdapterParameterProcessorLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.configuration.rules.ParameterProcessorLookupSession {

    private final org.osid.configuration.rules.ParameterProcessorLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterParameterProcessorLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterParameterProcessorLookupSession(org.osid.configuration.rules.ParameterProcessorLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Configuration/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Configuration Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getConfigurationId() {
        return (this.session.getConfigurationId());
    }


    /**
     *  Gets the {@code Configuration} associated with this session.
     *
     *  @return the {@code Configuration} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.Configuration getConfiguration()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getConfiguration());
    }


    /**
     *  Tests if this user can perform {@code ParameterProcessor} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupParameterProcessors() {
        return (this.session.canLookupParameterProcessors());
    }


    /**
     *  A complete view of the {@code ParameterProcessor} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeParameterProcessorView() {
        this.session.useComparativeParameterProcessorView();
        return;
    }


    /**
     *  A complete view of the {@code ParameterProcessor} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryParameterProcessorView() {
        this.session.usePlenaryParameterProcessorView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include parameter processors in configurations which are children
     *  of this configuration in the configuration hierarchy.
     */

    @OSID @Override
    public void useFederatedConfigurationView() {
        this.session.useFederatedConfigurationView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this configuration only.
     */

    @OSID @Override
    public void useIsolatedConfigurationView() {
        this.session.useIsolatedConfigurationView();
        return;
    }
    

    /**
     *  Only active parameter processors are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveParameterProcessorView() {
        this.session.useActiveParameterProcessorView();
        return;
    }


    /**
     *  Active and inactive parameter processors are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusParameterProcessorView() {
        this.session.useAnyStatusParameterProcessorView();
        return;
    }
    
     
    /**
     *  Gets the {@code ParameterProcessor} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code ParameterProcessor} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code ParameterProcessor} and
     *  retained for compatibility.
     *
     *  In active mode, parameter processors are returned that are currently
     *  active. In any status mode, active and inactive parameter processors
     *  are returned.
     *
     *  @param parameterProcessorId {@code Id} of the {@code ParameterProcessor}
     *  @return the parameter processor
     *  @throws org.osid.NotFoundException {@code parameterProcessorId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code parameterProcessorId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessor getParameterProcessor(org.osid.id.Id parameterProcessorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getParameterProcessor(parameterProcessorId));
    }


    /**
     *  Gets a {@code ParameterProcessorList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  parameterProcessors specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code ParameterProcessors} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, parameter processors are returned that are currently
     *  active. In any status mode, active and inactive parameter processors
     *  are returned.
     *
     *  @param  parameterProcessorIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code ParameterProcessor} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code parameterProcessorIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorList getParameterProcessorsByIds(org.osid.id.IdList parameterProcessorIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getParameterProcessorsByIds(parameterProcessorIds));
    }


    /**
     *  Gets a {@code ParameterProcessorList} corresponding to the given
     *  parameter processor genus {@code Type} which does not include
     *  parameter processors of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  parameter processors or an error results. Otherwise, the returned list
     *  may contain only those parameter processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, parameter processors are returned that are currently
     *  active. In any status mode, active and inactive parameter processors
     *  are returned.
     *
     *  @param  parameterProcessorGenusType a parameterProcessor genus type 
     *  @return the returned {@code ParameterProcessor} list
     *  @throws org.osid.NullArgumentException
     *          {@code parameterProcessorGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorList getParameterProcessorsByGenusType(org.osid.type.Type parameterProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getParameterProcessorsByGenusType(parameterProcessorGenusType));
    }


    /**
     *  Gets a {@code ParameterProcessorList} corresponding to the given
     *  parameter processor genus {@code Type} and include any additional
     *  parameter processors with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  parameter processors or an error results. Otherwise, the returned list
     *  may contain only those parameter processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, parameter processors are returned that are currently
     *  active. In any status mode, active and inactive parameter processors
     *  are returned.
     *
     *  @param  parameterProcessorGenusType a parameterProcessor genus type 
     *  @return the returned {@code ParameterProcessor} list
     *  @throws org.osid.NullArgumentException
     *          {@code parameterProcessorGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorList getParameterProcessorsByParentGenusType(org.osid.type.Type parameterProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getParameterProcessorsByParentGenusType(parameterProcessorGenusType));
    }


    /**
     *  Gets a {@code ParameterProcessorList} containing the given
     *  parameter processor record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  parameter processors or an error results. Otherwise, the returned list
     *  may contain only those parameter processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, parameter processors are returned that are currently
     *  active. In any status mode, active and inactive parameter processors
     *  are returned.
     *
     *  @param  parameterProcessorRecordType a parameterProcessor record type 
     *  @return the returned {@code ParameterProcessor} list
     *  @throws org.osid.NullArgumentException
     *          {@code parameterProcessorRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorList getParameterProcessorsByRecordType(org.osid.type.Type parameterProcessorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getParameterProcessorsByRecordType(parameterProcessorRecordType));
    }


    /**
     *  Gets all {@code ParameterProcessors}. 
     *
     *  In plenary mode, the returned list contains all known
     *  parameter processors or an error results. Otherwise, the returned list
     *  may contain only those parameter processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, parameter processors are returned that are currently
     *  active. In any status mode, active and inactive parameter processors
     *  are returned.
     *
     *  @return a list of {@code ParameterProcessors} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorList getParameterProcessors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getParameterProcessors());
    }
}
