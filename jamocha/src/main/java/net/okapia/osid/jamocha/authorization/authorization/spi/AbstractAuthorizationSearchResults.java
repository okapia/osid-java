//
// AbstractAuthorizationSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authorization.authorization.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractAuthorizationSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.authorization.AuthorizationSearchResults {

    private org.osid.authorization.AuthorizationList authorizations;
    private final org.osid.authorization.AuthorizationQueryInspector inspector;
    private final java.util.Collection<org.osid.authorization.records.AuthorizationSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractAuthorizationSearchResults.
     *
     *  @param authorizations the result set
     *  @param authorizationQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>authorizations</code>
     *          or <code>authorizationQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractAuthorizationSearchResults(org.osid.authorization.AuthorizationList authorizations,
                                            org.osid.authorization.AuthorizationQueryInspector authorizationQueryInspector) {
        nullarg(authorizations, "authorizations");
        nullarg(authorizationQueryInspector, "authorization query inspectpr");

        this.authorizations = authorizations;
        this.inspector = authorizationQueryInspector;

        return;
    }


    /**
     *  Gets the authorization list resulting from a search.
     *
     *  @return an authorization list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizations() {
        if (this.authorizations == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.authorization.AuthorizationList authorizations = this.authorizations;
        this.authorizations = null;
	return (authorizations);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.authorization.AuthorizationQueryInspector getAuthorizationQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  authorization search record <code> Type. </code> This method must
     *  be used to retrieve an authorization implementing the requested
     *  record.
     *
     *  @param authorizationSearchRecordType an authorization search 
     *         record type 
     *  @return the authorization search
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(authorizationSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.authorization.records.AuthorizationSearchResultsRecord getAuthorizationSearchResultsRecord(org.osid.type.Type authorizationSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.authorization.records.AuthorizationSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(authorizationSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(authorizationSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record authorization search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addAuthorizationRecord(org.osid.authorization.records.AuthorizationSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "authorization record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
