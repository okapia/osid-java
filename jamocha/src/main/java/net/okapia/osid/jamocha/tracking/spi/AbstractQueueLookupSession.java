//
// AbstractQueueLookupSession.java
//
//    A starter implementation framework for providing a Queue
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.tracking.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Queue
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getQueues(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractQueueLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.tracking.QueueLookupSession {

    private boolean pedantic   = false;
    private boolean activeonly = false;
    private boolean federated  = false;
    private org.osid.tracking.FrontOffice frontOffice = new net.okapia.osid.jamocha.nil.tracking.frontoffice.UnknownFrontOffice();
    

    /**
     *  Gets the <code>FrontOffice/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>FrontOffice Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFrontOfficeId() {
        return (this.frontOffice.getId());
    }


    /**
     *  Gets the <code>FrontOffice</code> associated with this
     *  session.
     *
     *  @return the <code>FrontOffice</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOffice getFrontOffice()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.frontOffice);
    }


    /**
     *  Sets the <code>FrontOffice</code>.
     *
     *  @param  frontOffice the front office for this session
     *  @throws org.osid.NullArgumentException <code>frontOffice</code>
     *          is <code>null</code>
     */

    protected void setFrontOffice(org.osid.tracking.FrontOffice frontOffice) {
        nullarg(frontOffice, "front office");
        this.frontOffice = frontOffice;
        return;
    }


    /**
     *  Tests if this user can perform <code>Queue</code> lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupQueues() {
        return (true);
    }


    /**
     *  A complete view of the <code>Queue</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeQueueView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Queue</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryQueueView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include queues in front offices which are children
     *  of this front office in the front office hierarchy.
     */

    @OSID @Override
    public void useFederatedFrontOfficeView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this front office only.
     */

    @OSID @Override
    public void useIsolatedFrontOfficeView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active queues are returned by methods in this session.
     */
     
    @OSID @Override
    public void useActiveQueueView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive queues are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusQueueView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>Queue</code> specified by its <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Queue</code> may have a different <code>Id</code> than
     *  requested, such as the case where a duplicate <code>Id</code>
     *  was assigned to a <code>Queue</code> and retained for
     *  compatibility.
     *
     *  In active mode, queues are returned that are currently
     *  active. In any status mode, active and inactive queues are
     *  returned.
     *
     *  @param queueId <code>Id</code> of the <code>Queue</code>
     *  @return the queue
     *  @throws org.osid.NotFoundException <code>queueId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>queueId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.Queue getQueue(org.osid.id.Id queueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.tracking.QueueList queues = getQueues()) {
            while (queues.hasNext()) {
                org.osid.tracking.Queue queue = queues.getNextQueue();
                if (queue.getId().equals(queueId)) {
                    return (queue);
                }
            }
        } 

        throw new org.osid.NotFoundException(queueId + " not found");
    }


    /**
     *  Gets a <code>QueueList</code> corresponding to the given
     *  <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the queues
     *  specified in the <code>Id</code> list, in the order of the
     *  list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Queues</code> may
     *  be omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, queues are returned that are currently
     *  active. In any status mode, active and inactive queues are
     *  returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getQueues()</code>.
     *
     *  @param queueIds the list of <code>Ids</code> to retrieve
     *  @return the returned <code>Queue</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>queueIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.QueueList getQueuesByIds(org.osid.id.IdList queueIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.tracking.Queue> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = queueIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getQueue(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("queue " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.tracking.queue.LinkedQueueList(ret));
    }


    /**
     *  Gets a <code>QueueList</code> corresponding to the given queue
     *  genus <code>Type</code> which does not include queues of types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known queues
     *  or an error results. Otherwise, the returned list may contain
     *  only those queues that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In active mode, queues are returned that are currently
     *  active. In any status mode, active and inactive queues are
     *  returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getQueues()</code>.
     *
     *  @param queueGenusType a queue genus type
     *  @return the returned <code>Queue</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>queueGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.QueueList getQueuesByGenusType(org.osid.type.Type queueGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.tracking.queue.QueueGenusFilterList(getQueues(), queueGenusType));
    }


    /**
     *  Gets a <code>QueueList</code> corresponding to the given queue
     *  genus <code>Type</code> and include any additional queues with
     *  genus types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known queues
     *  or an error results. Otherwise, the returned list may contain
     *  only those queues that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In active mode, queues are returned that are currently
     *  active. In any status mode, active and inactive queues are
     *  returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getQueues()</code>.
     *
     *  @param queueGenusType a queue genus type
     *  @return the returned <code>Queue</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>queueGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.QueueList getQueuesByParentGenusType(org.osid.type.Type queueGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getQueuesByGenusType(queueGenusType));
    }


    /**
     *  Gets a <code>QueueList</code> containing the given queue
     *  record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known queues
     *  or an error results. Otherwise, the returned list may contain
     *  only those queues that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In active mode, queues are returned that are currently
     *  active. In any status mode, active and inactive queues are
     *  returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getQueues()</code>.
     *
     *  @param  queueRecordType a queue record type 
     *  @return the returned <code>Queue</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>queueRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.QueueList getQueuesByRecordType(org.osid.type.Type queueRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.tracking.queue.QueueRecordFilterList(getQueues(), queueRecordType));
    }


    /**
     *  Gets a <code>QueueList</code> from the given provider.
     *  
     *  In plenary mode, the returned list contains all known queues
     *  or an error results. Otherwise, the returned list may contain
     *  only those queues that are accessible through this session.
     *
     *  In active mode, queues are returned that are currently
     *  active. In any status mode, active and inactive queues are
     *  returned.
     *
     *  @param resourceId a resource <code>Id</code>
     *  @return the returned <code>Queue</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.tracking.QueueList getQueuesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.tracking.queue.QueueProviderFilterList(getQueues(), resourceId));
    }


    /**
     *  Gets all <code>Queues</code>.
     *
     *  In plenary mode, the returned list contains all known queues
     *  or an error results. Otherwise, the returned list may contain
     *  only those queues that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In active mode, queues are returned that are currently
     *  active. In any status mode, active and inactive queues are
     *  returned.
     *
     *  @return a list of <code>Queues</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.tracking.QueueList getQueues()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the queue list for active and effective views. Should
     *  be called by <code>getObjects()</code> if no filtering is
     *  already performed.
     *
     *  @param list the list of queues
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.tracking.QueueList filterQueuesOnViews(org.osid.tracking.QueueList list)
        throws org.osid.OperationFailedException {

        org.osid.tracking.QueueList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.tracking.queue.ActiveQueueFilterList(ret);
        }

        return (ret);
    }
}
