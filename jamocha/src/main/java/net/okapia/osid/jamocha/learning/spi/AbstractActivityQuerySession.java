//
// AbstractActivityQuerySession.java
//
//     A template for making ActivityQuerySessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.learning.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A basic template for query sessions.
 */

public abstract class AbstractActivityQuerySession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.learning.ActivityQuerySession {

    private boolean federated = false;
    private org.osid.learning.ObjectiveBank objectiveBank = new net.okapia.osid.jamocha.nil.learning.objectivebank.UnknownObjectiveBank();


      /**
       *  Gets the <code>ObjectiveBank/code> <code>Id</code> associated
       *  with this session.
       *
       *  @return the <code>ObjectiveBank Id</code> associated with
       *          this session
       *  @throws org.osid.IllegalStateException this session has been
       *          closed
       */

    @OSID @Override
      public org.osid.id.Id getObjectiveBankId() {
        return (this.objectiveBank.getId());
    }


    /**
     *  Gets the <code>ObjectiveBank</code> associated with this 
     *  session.
     *
     *  @return the <code>ObjectiveBank</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBank getObjectiveBank()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.objectiveBank);
    }


    /**
     *  Sets the <code>ObjectiveBank</code>.
     *
     *  @param  objectiveBank the objective bank for this session
     *  @throws org.osid.NullArgumentException <code>objectiveBank</code>
     *          is <code>null</code>
     */

    protected void setObjectiveBank(org.osid.learning.ObjectiveBank objectiveBank) {
        nullarg(objectiveBank, "objective bank");
        this.objectiveBank = objectiveBank;
        return;
    }

    /**
     *  Federates the view for methods in this session. A federated
     *  view will include activities in objective banks which are children
     *  of this objective bank in the objective bank hierarchy.
     */

    @OSID @Override
    public void useFederatedObjectiveBankView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this objective bank only.
     */

    @OSID @Override
    public void useIsolatedObjectiveBankView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Tests if this user can perform <code> Activity </code>
     *  searches. A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a <code>
     *  PERMISSION_DENIED. </code> This is intended as a hint to an
     *  application that may opt not to offer search operations to
     *  unauthorized users.
     *
     *  @return <code> false </code> if search methods are not authorized, 
     *          <code> true </code> otherwise 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canSearchActivities() {
        return (true);
    }
}
