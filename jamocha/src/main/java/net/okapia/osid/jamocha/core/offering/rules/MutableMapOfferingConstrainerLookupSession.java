//
// MutableMapOfferingConstrainerLookupSession
//
//    Implements an OfferingConstrainer lookup service backed by a collection of
//    offeringConstrainers that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.offering.rules;


/**
 *  Implements an OfferingConstrainer lookup service backed by a collection of
 *  offering constrainers. The offering constrainers are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of offering constrainers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapOfferingConstrainerLookupSession
    extends net.okapia.osid.jamocha.core.offering.rules.spi.AbstractMapOfferingConstrainerLookupSession
    implements org.osid.offering.rules.OfferingConstrainerLookupSession {


    /**
     *  Constructs a new {@code MutableMapOfferingConstrainerLookupSession}
     *  with no offering constrainers.
     *
     *  @param catalogue the catalogue
     *  @throws org.osid.NullArgumentException {@code catalogue} is
     *          {@code null}
     */

      public MutableMapOfferingConstrainerLookupSession(org.osid.offering.Catalogue catalogue) {
        setCatalogue(catalogue);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapOfferingConstrainerLookupSession} with a
     *  single offeringConstrainer.
     *
     *  @param catalogue the catalogue  
     *  @param offeringConstrainer an offering constrainer
     *  @throws org.osid.NullArgumentException {@code catalogue} or
     *          {@code offeringConstrainer} is {@code null}
     */

    public MutableMapOfferingConstrainerLookupSession(org.osid.offering.Catalogue catalogue,
                                           org.osid.offering.rules.OfferingConstrainer offeringConstrainer) {
        this(catalogue);
        putOfferingConstrainer(offeringConstrainer);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapOfferingConstrainerLookupSession}
     *  using an array of offering constrainers.
     *
     *  @param catalogue the catalogue
     *  @param offeringConstrainers an array of offering constrainers
     *  @throws org.osid.NullArgumentException {@code catalogue} or
     *          {@code offeringConstrainers} is {@code null}
     */

    public MutableMapOfferingConstrainerLookupSession(org.osid.offering.Catalogue catalogue,
                                           org.osid.offering.rules.OfferingConstrainer[] offeringConstrainers) {
        this(catalogue);
        putOfferingConstrainers(offeringConstrainers);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapOfferingConstrainerLookupSession}
     *  using a collection of offering constrainers.
     *
     *  @param catalogue the catalogue
     *  @param offeringConstrainers a collection of offering constrainers
     *  @throws org.osid.NullArgumentException {@code catalogue} or
     *          {@code offeringConstrainers} is {@code null}
     */

    public MutableMapOfferingConstrainerLookupSession(org.osid.offering.Catalogue catalogue,
                                           java.util.Collection<? extends org.osid.offering.rules.OfferingConstrainer> offeringConstrainers) {

        this(catalogue);
        putOfferingConstrainers(offeringConstrainers);
        return;
    }

    
    /**
     *  Makes an {@code OfferingConstrainer} available in this session.
     *
     *  @param offeringConstrainer an offering constrainer
     *  @throws org.osid.NullArgumentException {@code offeringConstrainer{@code  is
     *          {@code null}
     */

    @Override
    public void putOfferingConstrainer(org.osid.offering.rules.OfferingConstrainer offeringConstrainer) {
        super.putOfferingConstrainer(offeringConstrainer);
        return;
    }


    /**
     *  Makes an array of offering constrainers available in this session.
     *
     *  @param offeringConstrainers an array of offering constrainers
     *  @throws org.osid.NullArgumentException {@code offeringConstrainers{@code 
     *          is {@code null}
     */

    @Override
    public void putOfferingConstrainers(org.osid.offering.rules.OfferingConstrainer[] offeringConstrainers) {
        super.putOfferingConstrainers(offeringConstrainers);
        return;
    }


    /**
     *  Makes collection of offering constrainers available in this session.
     *
     *  @param offeringConstrainers a collection of offering constrainers
     *  @throws org.osid.NullArgumentException {@code offeringConstrainers{@code  is
     *          {@code null}
     */

    @Override
    public void putOfferingConstrainers(java.util.Collection<? extends org.osid.offering.rules.OfferingConstrainer> offeringConstrainers) {
        super.putOfferingConstrainers(offeringConstrainers);
        return;
    }


    /**
     *  Removes an OfferingConstrainer from this session.
     *
     *  @param offeringConstrainerId the {@code Id} of the offering constrainer
     *  @throws org.osid.NullArgumentException {@code offeringConstrainerId{@code 
     *          is {@code null}
     */

    @Override
    public void removeOfferingConstrainer(org.osid.id.Id offeringConstrainerId) {
        super.removeOfferingConstrainer(offeringConstrainerId);
        return;
    }    
}
