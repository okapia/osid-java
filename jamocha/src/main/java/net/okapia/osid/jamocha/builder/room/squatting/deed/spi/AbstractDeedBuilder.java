//
// AbstractDeed.java
//
//     Defines a Deed builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.room.squatting.deed.spi;


/**
 *  Defines a <code>Deed</code> builder.
 */

public abstract class AbstractDeedBuilder<T extends AbstractDeedBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.room.squatting.deed.DeedMiter deed;


    /**
     *  Constructs a new <code>AbstractDeedBuilder</code>.
     *
     *  @param deed the deed to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractDeedBuilder(net.okapia.osid.jamocha.builder.room.squatting.deed.DeedMiter deed) {
        super(deed);
        this.deed = deed;
        return;
    }


    /**
     *  Builds the deed.
     *
     *  @return the new deed
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.room.squatting.Deed build() {
        (new net.okapia.osid.jamocha.builder.validator.room.squatting.deed.DeedValidator(getValidations())).validate(this.deed);
        return (new net.okapia.osid.jamocha.builder.room.squatting.deed.ImmutableDeed(this.deed));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the deed miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.room.squatting.deed.DeedMiter getMiter() {
        return (this.deed);
    }


    /**
     *  Sets the building.
     *
     *  @param building a building
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>building</code>
     *          is <code>null</code>
     */

    public T building(org.osid.room.Building building) {
        getMiter().setBuilding(building);
        return (self());
    }


    /**
     *  Sets the owner.
     *
     *  @param owner an owner
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>owner</code> is
     *          <code>null</code>
     */

    public T owner(org.osid.resource.Resource owner) {
        getMiter().setOwner(owner);
        return (self());
    }


    /**
     *  Adds a Deed record.
     *
     *  @param record a deed record
     *  @param recordType the type of deed record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.room.squatting.records.DeedRecord record, org.osid.type.Type recordType) {
        getMiter().addDeedRecord(record, recordType);
        return (self());
    }
}       


