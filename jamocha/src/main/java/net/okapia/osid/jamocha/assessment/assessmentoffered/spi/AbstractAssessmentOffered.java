//
// AbstractAssessmentOffered.java
//
//     Defines an AssessmentOffered.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.assessmentoffered.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>AssessmentOffered</code>.
 */

public abstract class AbstractAssessmentOffered
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.assessment.AssessmentOffered {

    private org.osid.assessment.Assessment assessment;
    private org.osid.grading.Grade level;
    private boolean sequential = true;
    private boolean shuffled = false;
    private org.osid.calendaring.DateTime startTime;
    private org.osid.calendaring.DateTime deadline;
    private org.osid.calendaring.Duration duration;
    private org.osid.grading.GradeSystem scoreSystem;
    private org.osid.grading.GradeSystem gradeSystem;
    private org.osid.assessment.AssessmentOffered rubric;

    private final java.util.Collection<org.osid.assessment.records.AssessmentOfferedRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the assessment <code> Id </code> corresponding to this assessment 
     *  offering. 
     *
     *  @return the assessment id 
     */

    @OSID @Override
    public org.osid.id.Id getAssessmentId() {
        return (this.assessment.getId());
    }


    /**
     *  Gets the assessment corresponding to this assessment offereng. 
     *
     *  @return the assessment 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.assessment.Assessment getAssessment()
        throws org.osid.OperationFailedException {

        return (this.assessment);
    }


    /**
     *  Sets the assessment.
     *
     *  @param assessment an assessment
     *  @throws org.osid.NullArgumentException <code>assessment</code>
     *          is <code>null</code>
     */

    protected void setAssessment(org.osid.assessment.Assessment assessment) {
        nullarg(assessment, "assessment");
        this.assessment = assessment;
        return;
    }


    /**
     *  Gets the <code> Id </code> of a <code> Grade </code> corresponding to 
     *  the assessment difficulty. 
     *
     *  @return a grade id 
     */

    @OSID @Override
    public org.osid.id.Id getLevelId() {
        return (this.level.getId());
    }


    /**
     *  Gets the <code> Grade </code> corresponding to the assessment 
     *  difficulty. 
     *
     *  @return the level 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.Grade getLevel()
        throws org.osid.OperationFailedException {

        return (this.level);
    }


    /**
     *  Sets the level.
     *
     *  @param level a level
     *  @throws org.osid.NullArgumentException <code>level</code> is
     *          <code>null</code>
     */

    protected void setLevel(org.osid.grading.Grade level) {
        nullarg(level, "level");
        this.level = level;
        return;
    }


    /**
     *  Tests if the items or parts in this assessment are taken sequentially. 
     *
     *  @return <code> true </code> if the items are taken sequentially, 
     *          <code> false </code> if the items can be skipped and revisited 
     */

    @OSID @Override
    public boolean areItemsSequential() {
        return (this.sequential);
    }


    /**
     *  Sets the sequential flag.
     *
     *  @param sequential <code> true </code> if the items are taken
     *         sequentially, <code> false </code> if the items can be
     *         skipped and revisited
     */

    protected void setItemsSequential(boolean sequential) {
        this.sequential = sequential;
        return;
    }


    /**
     *  Tests if the items or parts appear in a random order. 
     *
     *  @return <code> true </code> if the items appear in a random order, 
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean areItemsShuffled() {
        return (this.shuffled);
    }


    /**
     *  Sets the shuffled flag.
     *
     *  @param shuffled <code> true </code> if the items appear in a
     *         random order, <code> false </code> otherwise
     */

    protected void setItemsShuffled(boolean shuffled) {
        this.shuffled = shuffled;
        return;
    }


    /**
     *  Tests if there is a fixed start time for this assessment.
     *
     *  @return <code> true </code> if there is a fixed start time,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean hasStartTime() {
        return (this.startTime != null); 
    }


    /**
     *  Gets the start time for this assessment. 
     *
     *  @return the designated start time 
     *  @throws org.osid.IllegalStateException <code> hasStartTime()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getStartTime() {
        if (!hasStartTime()) {
            throw new org.osid.IllegalStateException("hasStartTime() is false");
        }

        return (this.startTime);
    }


    /**
     *  Sets the start time.
     *
     *  @param time a start time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    protected void setStartTime(org.osid.calendaring.DateTime time) {
        nullarg(time, "start time");
        this.startTime = time;
        return;
    }


    /**
     *  Tests if there is a fixed end time for this assessment. 
     *
     *  @return <code> true </code> if there is a fixed end time, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasDeadline() {
        return (this.deadline != null);
    }


    /**
     *  Gets the end time for this assessment. 
     *
     *  @return the designated end time 
     *  @throws org.osid.IllegalStateException <code> hasDeadline()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getDeadline() {
        if (!hasDeadline()) {
            throw new org.osid.IllegalStateException("hasDeadline() is false");
        }

        return (this.deadline);
    }


    /**
     *  Sets the deadline.
     *
     *  @param deadline a deadline
     *  @throws org.osid.NullArgumentException <code>deadline</code>
     *          is <code>null</code>
     */

    protected void setDeadline(org.osid.calendaring.DateTime deadline) {
        nullarg(deadline, "deadline");
        this.deadline = deadline;
        return;
    }


    /**
     *  Tests if there is a fixed duration for this assessment. 
     *
     *  @return <code> true </code> if there is a fixed duration, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasDuration() {
        return (this.duration != null);
    }


    /**
     *  Gets the duration for this assessment. 
     *
     *  @return the duration 
     *  @throws org.osid.IllegalStateException <code> hasDuration()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.calendaring.Duration getDuration() {
        if (!hasDuration()) {
            throw new org.osid.IllegalStateException("hasDuration() is false");
        }

        return (this.duration);
    }


    /**
     *  Sets the duration.
     *
     *  @param duration a duration
     *  @throws org.osid.NullArgumentException <code>duration</code>
     *          is <code>null</code>
     */

    protected void setDuration(org.osid.calendaring.Duration duration) {
        nullarg(duration, "duration");
        this.duration = duration;
        return;
    }


    /**
     *  Tests if this assessment will be scored. 
     *
     *  @return <code> true </code> if this assessment will be scored <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean isScored() {
        if (this.scoreSystem == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the grade system Id for the score. 
     *
     *  @return the grade system Id
     *  @throws org.osid.IllegalStateException <code> isScored() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getScoreSystemId() {        
        if (!isScored()) {
            throw new org.osid.IllegalStateException("isScored() is false");
        }

        return (this.scoreSystem.getId());
    }


    /**
     *  Gets the grade system for the score. 
     *
     *  @return the grade system 
     *  @throws org.osid.IllegalStateException <code> isScored() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeSystem getScoreSystem()
        throws org.osid.OperationFailedException {
        
        if (!isScored()) {
            throw new org.osid.IllegalStateException("isScored() is false");
        }

        return (this.scoreSystem);
    }


    /**
     *  Sets the score system.
     *
     *  @param gradeSystem the scoring system
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystem</code> is <code>null</code>
     */

    protected void setScoreSystem(org.osid.grading.GradeSystem gradeSystem) {
        nullarg(gradeSystem, "scoring system");
        this.scoreSystem = gradeSystem;
        return;
    }


    /**
     *  Tests if this assessment will be graded. 
     *
     *  @return <code> true </code> if this assessment will be graded, <code> 
     *          false </code> otherwise 
     */
    
    @OSID @Override
    public boolean isGraded() {
        if (this.gradeSystem == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the grade system Id for the grade. 
     *
     *  @return the grade system Id
     *  @throws org.osid.IllegalStateException <code> isGraded() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getGradeSystemId() {        
        if (!isGraded()) {
            throw new org.osid.IllegalStateException("isGraded() is false");
        }

        return (this.gradeSystem.getId());
    }        


    /**
     *  Gets the grade system for the grade. 
     *
     *  @return the grade system 
     *  @throws org.osid.IllegalStateException <code> isGraded() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeSystem getGradeSystem()
        throws org.osid.OperationFailedException {
        
        if (!isGraded()) {
            throw new org.osid.IllegalStateException("isGraded() is false");
        }

        return (this.gradeSystem);
    }        


    /**
     *  Sets the grade system.
     *
     *  @param gradeSystem the scoring system
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystem</code> is <code>null</code>
     */

    protected void setGradeSystem(org.osid.grading.GradeSystem gradeSystem) {
        nullarg(gradeSystem, "scoring system");
        this.gradeSystem = gradeSystem;
        return;
    }


    /**
     *  Tests if a rubric assessment offered is associated with this
     *  assessment offered.
     *
     *  @return <code> true </code> if a rubric is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasRubric() {
        if (this.rubric == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the <code> Id </code> of the rubric. 
     *
     *  @return an assessment offered <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasRubric() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getRubricId() {
        if (!hasRubric()) {
            throw new org.osid.IllegalStateException("hasRubric() is false");
        }

        return (this.rubric.getId());
    }


    /**
     *  Gets the rubric. 
     *
     *  @return the assessment  offered
     *  @throws org.osid.IllegalStateException <code> hasRubric() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOffered getRubric() {
        if (!hasRubric()) {
            throw new org.osid.IllegalStateException("hasRubric() is false");
        }

        return (this.rubric);
    }


    /**
     *  Sets the rubric.
     *
     *  @param assessmentOffered the rubric assessmentOffered
     *  @throws org.osid.NullArgumentException
     *          <code>assessment</code> is <code>null</code>
     */

    protected void setRubric(org.osid.assessment.AssessmentOffered assessmentOffered) {
        nullarg(assessmentOffered, "rubric assessment offered");
        this.rubric = assessmentOffered;
        return;
    }


    /**
     *  Tests if this assessment offered supports the given record
     *  <code>Type</code>.
     *
     *  @param assessmentOfferedRecordType an assessment offered
     *         record type
     *  @return <code>true</code> if the assessmentOfferedRecordType
     *          is supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentOfferedRecordType</code> is
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type assessmentOfferedRecordType) {
        for (org.osid.assessment.records.AssessmentOfferedRecord record : this.records) {
            if (record.implementsRecordType(assessmentOfferedRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>AssessmentOffered</code> record <code>Type</code>.
     *
     *  @param assessmentOfferedRecordType the assessment offered
     *         record type
     *  @return the assessment offered record 
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentOfferedRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable
     *          to complete request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assessmentOfferedRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.AssessmentOfferedRecord getAssessmentOfferedRecord(org.osid.type.Type assessmentOfferedRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.records.AssessmentOfferedRecord record : this.records) {
            if (record.implementsRecordType(assessmentOfferedRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assessmentOfferedRecordType + " is not supported");
    }


    /**
     *  Adds a record to this assessment offered. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param assessmentOfferedRecord the assessment offered record
     *  @param assessmentOfferedRecordType assessment offered record type
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentOfferedRecord</code> or
     *          <code>assessmentOfferedRecordType</code> is
     *          <code>null</code>
     */
            
    protected void addAssessmentOfferedRecord(org.osid.assessment.records.AssessmentOfferedRecord assessmentOfferedRecord, 
                                              org.osid.type.Type assessmentOfferedRecordType) {

        nullarg(assessmentOfferedRecord, "assessment offered record");
        addRecordType(assessmentOfferedRecordType);
        this.records.add(assessmentOfferedRecord);
        
        return;
    }
}
