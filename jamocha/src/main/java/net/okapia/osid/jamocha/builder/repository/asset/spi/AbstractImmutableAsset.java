//
// AbstractImmutableAsset.java
//
//     Wraps a mutable Asset to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.repository.asset.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Asset</code> to hide modifiers. This
 *  wrapper provides an immutized Asset from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying asset whose state changes are visible.
 */

public abstract class AbstractImmutableAsset
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableSourceableOsidObject
    implements org.osid.repository.Asset {

    private final org.osid.repository.Asset asset;


    /**
     *  Constructs a new <code>AbstractImmutableAsset</code>.
     *
     *  @param asset the asset to immutablize
     *  @throws org.osid.NullArgumentException <code>asset</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableAsset(org.osid.repository.Asset asset) {
        super(asset);
        this.asset = asset;
        return;
    }


    /**
     *  Gets the proper title of this asset. This may be the same as the 
     *  display name or the display name may be used for a less formal label. 
     *
     *  @return the title of this asset 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getTitle() {
        return (this.asset.getTitle());
    }


    /**
     *  Tests if the copyright status is known. 
     *
     *  @return <code> true </code> if the copyright status of this asset is 
     *          known, <code> false </code> otherwise. If <code> false, 
     *          isPublicDomain(), </code> <code> canDistributeVerbatim(), 
     *          canDistributeAlterations() and canDistributeCompositions() 
     *          </code> may also be <code> false. </code> 
     */

    @OSID @Override
    public boolean isCopyrightStatusKnown() {
        return (this.asset.isCopyrightStatusKnown());
    }


    /**
     *  Tests if this asset is in the public domain. An asset is in the public 
     *  domain if copyright is not applicable, the copyright has expired, or 
     *  the copyright owner has expressly relinquished the copyright. 
     *
     *  @return <code> true </code> if this asset is in the public domain, 
     *          <code> false </code> otherwise. If <code> true, </code> <code> 
     *          canDistributeVerbatim(), canDistributeAlterations() and 
     *          canDistributeCompositions() </code> must also be <code> true. 
     *          </code> 
     *  @throws org.osid.IllegalStateException <code> isCopyrightStatusKnown() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public boolean isPublicDomain() {
        return (this.asset.isPublicDomain());
    }


    /**
     *  Gets the copyright statement and of this asset which identifies the 
     *  current copyright holder. For an asset in the public domain, this 
     *  method may return the original copyright statement although it may be 
     *  no longer valid. 
     *
     *  @return the copyright statement or an empty string if none available. 
     *          An empty string does not imply the asset is not protected by 
     *          copyright. 
     *  @throws org.osid.IllegalStateException <code> isCopyrightStatusKnown() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getCopyright() {
        return (this.asset.getCopyright());
    }


    /**
     *  Gets the copyright registration information for this asset. 
     *
     *  @return the copyright registration. An empty string means the 
     *          registration status isn't known. 
     *  @throws org.osid.IllegalStateException <code> isCopyrightStatusKnown() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public String getCopyrightRegistration() {
        return (this.asset.getCopyrightRegistration());
    }


    /**
     *  Tests if there are any license restrictions on this asset that 
     *  restrict the distribution, re-publication or public display of this 
     *  asset, commercial or otherwise, without modification, alteration, or 
     *  inclusion in other works. This method is intended to offer consumers a 
     *  means of filtering out search results that restrict distribution for 
     *  any purpose. The scope of this method does not include licensing that 
     *  describes warranty disclaimers or attribution requirements. This 
     *  method is intended for informational purposes only and does not 
     *  replace or override the terms specified in a license agreement which 
     *  may specify exceptions or additional restrictions. 
     *
     *  @return <code> true </code> if the asset can be distributed verbatim, 
     *          <code> false </code> otherwise. 
     *  @throws org.osid.IllegalStateException <code> isCopyrightStatusKnown() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public boolean canDistributeVerbatim() {
        return (this.asset.canDistributeVerbatim());
    }


    /**
     *  Tests if there are any license restrictions on this asset that 
     *  restrict the distribution, re-publication or public display of any 
     *  alterations or modifications to this asset, commercial or otherwise, 
     *  for any purpose. This method is intended to offer consumers a means of 
     *  filtering out search results that restrict the distribution or public 
     *  display of any modification or alteration of the content or its 
     *  metadata of any kind, including editing, translation, resampling, 
     *  resizing and cropping. The scope of this method does not include 
     *  licensing that describes warranty disclaimers or attribution 
     *  requirements. This method is intended for informational purposes only 
     *  and does not replace or override the terms specified in a license 
     *  agreement which may specify exceptions or additional restrictions. 
     *
     *  @return <code> true </code> if the asset can be modified, <code> false 
     *          </code> otherwise. If <code> true, </code> <code> 
     *          canDistributeVerbatim() </code> must also be <code> true. 
     *          </code> 
     *  @throws org.osid.IllegalStateException <code> isCopyrightStatusKnown() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public boolean canDistributeAlterations() {
        return (this.asset.canDistributeAlterations());
    }


    /**
     *  Tests if there are any license restrictions on this asset that 
     *  restrict the distribution, re-publication or public display of this 
     *  asset as an inclusion within other content or composition, commercial 
     *  or otherwise, for any purpose, including restrictions upon the 
     *  distribution or license of the resulting composition. This method is 
     *  intended to offer consumers a means of filtering out search results 
     *  that restrict the use of this asset within compositions. The scope of 
     *  this method does not include licensing that describes warranty 
     *  disclaimers or attribution requirements. This method is intended for 
     *  informational purposes only and does not replace or override the terms 
     *  specified in a license agreement which may specify exceptions or 
     *  additional restrictions. 
     *
     *  @return <code> true </code> if the asset can be part of a larger 
     *          composition <code> false </code> otherwise. If <code> true, 
     *          </code> <code> canDistributeVerbatim() </code> must also be 
     *          <code> true. </code> 
     *  @throws org.osid.IllegalStateException <code> isCopyrightStatusKnown() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public boolean canDistributeCompositions() {
        return (this.asset.canDistributeCompositions());
    }


    /**
     *  Gets the <code> Resource Id </code> of the source of this asset. The 
     *  source is the original owner of the copyright of this asset and may 
     *  differ from the creator of this asset. The source for a published book 
     *  written by Margaret Mitchell would be Macmillan. The source for an 
     *  unpublished painting by Arthur Goodwin would be Arthur Goodwin. 
     *  
     *  An <code> Asset </code> is <code> Sourceable </code> and also contains 
     *  a provider identity. The provider is the entity that makes this 
     *  digital asset available in this repository but may or may not be the 
     *  publisher of the contents depicted in the asset. For example, a map 
     *  published by Ticknor and Fields in 1848 may have a provider of Library 
     *  of Congress and a source of Ticknor and Fields. If copied from a 
     *  repository at Middlebury College, the provider would be Middlebury 
     *  College and a source of Ticknor and Fields. 
     *
     *  @return the source <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSourceId() {
        return (this.asset.getSourceId());
    }


    /**
     *  Gets the <code> Resource </code> of the source of this asset. The 
     *  source is the original owner of the copyright of this asset and may 
     *  differ from the creator of this asset. The source for a published book 
     *  written by Margaret Mitchell would be Macmillan. The source for an 
     *  unpublished painting by Arthur Goodwin would be Arthur Goodwin. 
     *
     *  @return the source 
     */

    @OSID @Override
    public org.osid.resource.Resource getSource() {
        return (this.asset.getSource());
    }


    /**
     *  Gets the resource <code> Ids </code> representing the source of this 
     *  asset in order from the most recent provider to the originating 
     *  source. 
     *
     *  @return the provider <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getProviderLinkIds() {
        return (this.asset.getProviderLinkIds());
    }


    /**
     *  Gets the <code> Resources </code> representing the source of this 
     *  asset in order from the most recent provider to the originating 
     *  source. 
     *
     *  @return the provider chain 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getProviderLinks()
        throws org.osid.OperationFailedException {

        return (this.asset.getProviderLinks());
    }


    /**
     *  Gets the created date of this asset, which is generally not related to 
     *  when the object representing the asset was created. The date returned 
     *  may indicate that not much is known. 
     *
     *  @return the created date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getCreatedDate() {
        return (this.asset.getCreatedDate());
    }


    /**
     *  Tests if this asset has been published. Not all assets viewable in 
     *  this repository may have been published. The source of a published 
     *  asset indicates the publisher. 
     *
     *  @return true if this asset has been published, <code> false </code> if 
     *          unpublished or its published status is not known 
     */

    @OSID @Override
    public boolean isPublished() {
        return (this.asset.isPublished());
    }


    /**
     *  Gets the published date of this asset. Unpublished assets have no 
     *  published date. A published asset has a date available, however the 
     *  date returned may indicate that not much is known. 
     *
     *  @return the published date 
     *  @throws org.osid.IllegalStateException <code> isPublished() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getPublishedDate() {
        return (this.asset.getPublishedDate());
    }


    /**
     *  Gets the credits of the principal people involved in the production of 
     *  this asset as a display string. 
     *
     *  @return the principal credits 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getPrincipalCreditString() {
        return (this.asset.getPrincipalCreditString());
    }


    /**
     *  Gets the content <code> Ids </code> of this asset. 
     *
     *  @return the asset content <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getAssetContentIds() {
        return (this.asset.getAssetContentIds());
    }


    /**
     *  Gets the content of this asset. 
     *
     *  @return the asset contents 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.repository.AssetContentList getAssetContents()
        throws org.osid.OperationFailedException {

        return (this.asset.getAssetContents());
    }


    /**
     *  Tetss if this asset is a representation of a composition of assets. 
     *
     *  @return true if this asset is a composition, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean isComposition() {
        return (this.asset.isComposition());
    }


    /**
     *  Gets the <code> Composition </code> <code> Id </code> corresponding to 
     *  this asset. 
     *
     *  @return the composiiton <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isComposition() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCompositionId() {
        return (this.asset.getCompositionId());
    }


    /**
     *  Gets the Composition corresponding to this asset. 
     *
     *  @return the composiiton 
     *  @throws org.osid.IllegalStateException <code> isComposition() </code> 
     *          is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.repository.Composition getComposition()
        throws org.osid.OperationFailedException {

        return (this.asset.getComposition());
    }


    /**
     *  Gets the asset record corresponding to the given <code> Asset </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> assetRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(assetRecordType) </code> is <code> true </code> . 
     *
     *  @param  assetRecordType an asset record type 
     *  @return the asset record 
     *  @throws org.osid.NullArgumentException <code> assetRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(assetRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.records.AssetRecord getAssetRecord(org.osid.type.Type assetRecordType)
        throws org.osid.OperationFailedException {

        return (this.asset.getAssetRecord(assetRecordType));
    }
}

