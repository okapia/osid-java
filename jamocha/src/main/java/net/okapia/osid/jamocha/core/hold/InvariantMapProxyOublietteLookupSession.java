//
// InvariantMapProxyOublietteLookupSession
//
//    Implements an Oubliette lookup service backed by a fixed
//    collection of oubliettes. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.hold;


/**
 *  Implements an Oubliette lookup service backed by a fixed
 *  collection of oubliettes. The oubliettes are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyOublietteLookupSession
    extends net.okapia.osid.jamocha.core.hold.spi.AbstractMapOublietteLookupSession
    implements org.osid.hold.OublietteLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyOublietteLookupSession} with no
     *  oubliettes.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public InvariantMapProxyOublietteLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyOublietteLookupSession} with a
     *  single oubliette.
     *
     *  @param oubliette an single oubliette
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code oubliette} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyOublietteLookupSession(org.osid.hold.Oubliette oubliette, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putOubliette(oubliette);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyOublietteLookupSession} using
     *  an array of oubliettes.
     *
     *  @param oubliettes an array of oubliettes
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code oubliettes} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyOublietteLookupSession(org.osid.hold.Oubliette[] oubliettes, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putOubliettes(oubliettes);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyOublietteLookupSession} using a
     *  collection of oubliettes.
     *
     *  @param oubliettes a collection of oubliettes
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code oubliettes} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyOublietteLookupSession(java.util.Collection<? extends org.osid.hold.Oubliette> oubliettes,
                                                  org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putOubliettes(oubliettes);
        return;
    }
}
