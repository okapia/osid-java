//
// AbstractSourceableOsidObject.java
//
//     Defines a sourceable OsidObject.
//
//
// Tom Coppeto
// Okapia
// 5 October 2011
//
//
// Copyright (c) 2011 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a sourceable <code>OsidObject</code>.
 *  <code>setProvider()</code> should be used to set the provider
 *  information by subclasses.
 */

public abstract class AbstractSourceableOsidObject
    extends AbstractOsidObject
    implements org.osid.Sourceable,
               org.osid.OsidObject {

    private final Sourceable sourceable = new Sourceable();


    /**
     *  Gets the <code> Id </code> of the <code> Provider </code> of this 
     *  <code> Catalog. </code> 
     *
     *  @return the <code> Provider Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getProviderId() {
        return (this.sourceable.getProviderId());
    }


    /**
     *  Gets the <code> Resource </code> representing the provider of this 
     *  catalog. 
     *
     *  @return the provider 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getProvider()
        throws org.osid.OperationFailedException {
        
        return (this.sourceable.getProvider());
    }


    /**
     *  Sets the provider for this catalog.
     *
     *  @param provider the new provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected void setProvider(net.okapia.osid.provider.Provider provider) {
        this.sourceable.setProvider(provider);
        return;
    }


    /**
     *  Sets the provider.
     *
     *  @param provider the new provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected void setProvider(org.osid.resource.Resource provider) {
        this.sourceable.setProvider(provider);
        return;
    }


    /**
     *  Gets the branding asset Ids, such as an image or logo,
     *  expressed using the <code> Asset </code> interface.
     *
     *  @return a list of asset Ids
     */

    @OSID @Override
    public org.osid.id.IdList getBrandingIds() {
        return (this.sourceable.getBrandingIds());
    }


    /**
     *  Gets a branding, such as an image or logo, expressed using the <code> 
     *  Asset </code> interface. 
     *
     *  @return a list of assets 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.repository.AssetList getBranding()
        throws org.osid.OperationFailedException {

        return (this.sourceable.getBranding());
    }


    /**
     *  Adds an asset to the branding.
     *
     *  @param asset an asset to add
     *  @throws org.osid.NullArgumentException <code>asset</code>
     *          is <code>null</code>
     */

    protected void addAssetToBranding(org.osid.repository.Asset asset) {
        this.sourceable.addAssetToBranding(asset);
        return;
    }


    /**
     *  Adds assets to the branding.
     *
     *  @param assets array of assets to add
     *  @throws org.osid.NullArgumentException <code>asset</code>
     *          is <code>null</code>
     */

    protected void addAssetsToBranding(java.util.Collection<org.osid.repository.Asset> assets) {
        this.sourceable.addAssetsToBranding(assets);
        return;
    }


    /**
     *  Sets the branding to the given asset.
     *
     *  @param asset an asset to set
     *  @throws org.osid.NullArgumentException <code>asset</code>
     *          is <code>null</code>
     */

    protected void setBranding(org.osid.repository.Asset asset) {
        this.sourceable.setBranding(asset);
        return;
    }


    /**
     *  Sets the branding to the given assets.
     *
     *  @param assets an array of assets to setd
     *  @throws org.osid.NullArgumentException <code>assets</code> is
     *          <code>null</code>
     */

    protected void setBranding(java.util.Collection<org.osid.repository.Asset> assets) {
        this.sourceable.setBranding(assets);
        return;
    }


    /**
     *  Gets the terms of usage. An empty license means the terms are
     *  unknown.
     *
     *  @return the license 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getLicense() {
        return (this.sourceable.getLicense());
    }


    /**
     *  Sets the license.
     *
     *  @param license the license
     *  @throws org.osid.NullArgumentException <code>license</code> is
     *          <code>null</code>
     */

    protected void setLicense(org.osid.locale.DisplayText license) {
        this.sourceable.setLicense(license);
        return;
    }


    protected class Sourceable
        extends AbstractSourceable
        implements org.osid.Sourceable {

        /**
         *  Sets the provider for this catalog.
         *
         *  @param provider the new provider
         *  @throws org.osid.NullArgumentException <code>provider</code>
         *          is <code>null</code>
         */

        @Override        
        protected void setProvider(net.okapia.osid.provider.Provider provider) {
            super.setProvider(provider);
            return;
        }


        /**
         *  Sets the provider.
         *
         *  @param provider the new provider
         *  @throws org.osid.NullArgumentException <code>provider</code>
         *          is <code>null</code>
         */
        
        @Override
        protected void setProvider(org.osid.resource.Resource provider) {
            super.setProvider(provider);
            return;
        }


        /**
         *  Adds assets to the service branding.
         *
         *  @param assets array of assets to add
         *  @throws org.osid.NullArgumentException <code>asset</code>
         *          is <code>null</code>
         */
        
        @Override
        protected void addAssetsToBranding(java.util.Collection<org.osid.repository.Asset> assets) {
            super.addAssetsToBranding(assets);
            return;
        }


        /**
         *  Adds an asset to the branding.
         *
         *  @param asset an asset to add
         *  @throws org.osid.NullArgumentException <code>asset</code>
         *          is <code>null</code>
         */
        
        @Override
        protected void addAssetToBranding(org.osid.repository.Asset asset) {
            super.addAssetToBranding(asset);
            return;
        }
        
        
        /**
         *  Sets the branding to the given asset.
         *
         *  @param asset an asset to set
         *  @throws org.osid.NullArgumentException <code>asset</code>
         *          is <code>null</code>
         */
        
        @Override
        protected void setBranding(org.osid.repository.Asset asset) {
            super.setBranding(asset);
            return;
        }


        /**
         *  Sets the branding to the given collection of assets.
         *
         *  @param assets an array of assets to set
         *  @throws org.osid.NullArgumentException <code>assets</code>
         *          is <code>null</code>
         */
        
        @Override
        protected void setBranding(java.util.Collection<org.osid.repository.Asset> assets) {
            super.setBranding(assets);
            return;
        }
        
        
        /**
         *  Sets the license.
         *
         *  @param license the license
         *  @throws org.osid.NullArgumentException <code>license</code> is
         *          <code>null</code>
         */

        @Override
        protected void setLicense(org.osid.locale.DisplayText license) {
            super.setLicense(license);
            return;
        }
    }
}
