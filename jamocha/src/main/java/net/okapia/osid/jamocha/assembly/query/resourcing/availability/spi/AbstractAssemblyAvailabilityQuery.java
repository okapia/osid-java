//
// AbstractAssemblyAvailabilityQuery.java
//
//     An AvailabilityQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.resourcing.availability.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An AvailabilityQuery that stores terms.
 */

public abstract class AbstractAssemblyAvailabilityQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.resourcing.AvailabilityQuery,
               org.osid.resourcing.AvailabilityQueryInspector,
               org.osid.resourcing.AvailabilitySearchOrder {

    private final java.util.Collection<org.osid.resourcing.records.AvailabilityQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resourcing.records.AvailabilityQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resourcing.records.AvailabilitySearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyAvailabilityQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyAvailabilityQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the resource <code> Id </code> for this query. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResourceId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getResourceIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the resource <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearResourceIdTerms() {
        getAssembler().clearTerms(getResourceIdColumn());
        return;
    }


    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResourceIdTerms() {
        return (getAssembler().getIdTerms(getResourceIdColumn()));
    }


    /**
     *  Orders the results by resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByResource(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getResourceColumn(), style);
        return;
    }


    /**
     *  Gets the ResourceId column name.
     *
     * @return the column name
     */

    protected String getResourceIdColumn() {
        return ("resource_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResourceQuery() {
        throw new org.osid.UnimplementedException("supportsResourceQuery() is false");
    }


    /**
     *  Clears the resource query terms. 
     */

    @OSID @Override
    public void clearResourceTerms() {
        getAssembler().clearTerms(getResourceColumn());
        return;
    }


    /**
     *  Gets the resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsResourceSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getResourceSearchOrder() {
        throw new org.osid.UnimplementedException("supportsResourceSearchOrder() is false");
    }


    /**
     *  Gets the Resource column name.
     *
     * @return the column name
     */

    protected String getResourceColumn() {
        return ("resource");
    }


    /**
     *  Sets the job <code> Id </code> for this query. 
     *
     *  @param  jobId the job <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> jobId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchJobId(org.osid.id.Id jobId, boolean match) {
        getAssembler().addIdTerm(getJobIdColumn(), jobId, match);
        return;
    }


    /**
     *  Clears the job <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearJobIdTerms() {
        getAssembler().clearTerms(getJobIdColumn());
        return;
    }


    /**
     *  Gets the job <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getJobIdTerms() {
        return (getAssembler().getIdTerms(getJobIdColumn()));
    }


    /**
     *  Orders the results by job. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByJob(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getJobColumn(), style);
        return;
    }


    /**
     *  Gets the JobId column name.
     *
     * @return the column name
     */

    protected String getJobIdColumn() {
        return ("job_id");
    }


    /**
     *  Tests if a <code> JobQuery </code> is available. 
     *
     *  @return <code> true </code> if a job query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobQuery() {
        return (false);
    }


    /**
     *  Gets the query for a job. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the job query 
     *  @throws org.osid.UnimplementedException <code> supportsJobQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobQuery getJobQuery() {
        throw new org.osid.UnimplementedException("supportsJobQuery() is false");
    }


    /**
     *  Clears the job query terms. 
     */

    @OSID @Override
    public void clearJobTerms() {
        getAssembler().clearTerms(getJobColumn());
        return;
    }


    /**
     *  Gets the job query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.JobQueryInspector[] getJobTerms() {
        return (new org.osid.resourcing.JobQueryInspector[0]);
    }


    /**
     *  Tests if a job search order is available. 
     *
     *  @return <code> true </code> if a job search order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobSearchOrder() {
        return (false);
    }


    /**
     *  Gets the job search order. 
     *
     *  @return the job search order 
     *  @throws org.osid.IllegalStateException <code> supportsJobSearchOrder() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobSearchOrder getJobSearchOrder() {
        throw new org.osid.UnimplementedException("supportsJobSearchOrder() is false");
    }


    /**
     *  Gets the Job column name.
     *
     * @return the column name
     */

    protected String getJobColumn() {
        return ("job");
    }


    /**
     *  Sets the competency <code> Id </code> for this query. 
     *
     *  @param  competencyId the competency <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> competencyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCompetencyId(org.osid.id.Id competencyId, boolean match) {
        getAssembler().addIdTerm(getCompetencyIdColumn(), competencyId, match);
        return;
    }


    /**
     *  Clears the competency <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCompetencyIdTerms() {
        getAssembler().clearTerms(getCompetencyIdColumn());
        return;
    }


    /**
     *  Gets the competency <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCompetencyIdTerms() {
        return (getAssembler().getIdTerms(getCompetencyIdColumn()));
    }


    /**
     *  Orders the results by competency. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCompetency(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCompetencyColumn(), style);
        return;
    }


    /**
     *  Gets the CompetencyId column name.
     *
     * @return the column name
     */

    protected String getCompetencyIdColumn() {
        return ("competency_id");
    }


    /**
     *  Tests if a <code> CompetencyQuery </code> is available. 
     *
     *  @return <code> true </code> if a competency query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompetencyQuery() {
        return (false);
    }


    /**
     *  Gets the query for a competency. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the competency query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencyQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyQuery getCompetencyQuery() {
        throw new org.osid.UnimplementedException("supportsCompetencyQuery() is false");
    }


    /**
     *  Clears the competency query terms. 
     */

    @OSID @Override
    public void clearCompetencyTerms() {
        getAssembler().clearTerms(getCompetencyColumn());
        return;
    }


    /**
     *  Gets the competency query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyQueryInspector[] getCompetencyTerms() {
        return (new org.osid.resourcing.CompetencyQueryInspector[0]);
    }


    /**
     *  Tests if a competency search order is available. 
     *
     *  @return <code> true </code> if a competency search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompetencySearchOrder() {
        return (false);
    }


    /**
     *  Gets the competency search order. 
     *
     *  @return the competency search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsCompetencySearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencySearchOrder getCompetencySearchOrder() {
        throw new org.osid.UnimplementedException("supportsCompetencySearchOrder() is false");
    }


    /**
     *  Gets the Competency column name.
     *
     * @return the column name
     */

    protected String getCompetencyColumn() {
        return ("competency");
    }


    /**
     *  Matches percentages within the given range inclusive. 
     *
     *  @param  low start range 
     *  @param  high end range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchPercentage(long low, long high, boolean match) {
        getAssembler().addCardinalRangeTerm(getPercentageColumn(), low, high, match);
        return;
    }


    /**
     *  Clears the percentage query terms. 
     */

    @OSID @Override
    public void clearPercentageTerms() {
        getAssembler().clearTerms(getPercentageColumn());
        return;
    }


    /**
     *  Gets the percentage availability query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getPercentageTerms() {
        return (getAssembler().getCardinalRangeTerms(getPercentageColumn()));
    }


    /**
     *  Orders the results by percentage availability. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPercentage(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getPercentageColumn(), style);
        return;
    }


    /**
     *  Gets the Percentage column name.
     *
     * @return the column name
     */

    protected String getPercentageColumn() {
        return ("percentage");
    }


    /**
     *  Sets the foundry <code> Id </code> for this query to match 
     *  availabilities assigned to foundries. 
     *
     *  @param  foundryId the foundry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchFoundryId(org.osid.id.Id foundryId, boolean match) {
        getAssembler().addIdTerm(getFoundryIdColumn(), foundryId, match);
        return;
    }


    /**
     *  Clears the foundry <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearFoundryIdTerms() {
        getAssembler().clearTerms(getFoundryIdColumn());
        return;
    }


    /**
     *  Gets the foundry <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFoundryIdTerms() {
        return (getAssembler().getIdTerms(getFoundryIdColumn()));
    }


    /**
     *  Gets the FoundryId column name.
     *
     * @return the column name
     */

    protected String getFoundryIdColumn() {
        return ("foundry_id");
    }


    /**
     *  Tests if a <code> FoundryQuery </code> is available. 
     *
     *  @return <code> true </code> if a foundry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFoundryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a foundry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the foundry query 
     *  @throws org.osid.UnimplementedException <code> supportsFoundryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQuery getFoundryQuery() {
        throw new org.osid.UnimplementedException("supportsFoundryQuery() is false");
    }


    /**
     *  Clears the foundry query terms. 
     */

    @OSID @Override
    public void clearFoundryTerms() {
        getAssembler().clearTerms(getFoundryColumn());
        return;
    }


    /**
     *  Gets the foundry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQueryInspector[] getFoundryTerms() {
        return (new org.osid.resourcing.FoundryQueryInspector[0]);
    }


    /**
     *  Gets the Foundry column name.
     *
     * @return the column name
     */

    protected String getFoundryColumn() {
        return ("foundry");
    }


    /**
     *  Tests if this availability supports the given record
     *  <code>Type</code>.
     *
     *  @param  availabilityRecordType an availability record type 
     *  @return <code>true</code> if the availabilityRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>availabilityRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type availabilityRecordType) {
        for (org.osid.resourcing.records.AvailabilityQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(availabilityRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  availabilityRecordType the availability record type 
     *  @return the availability query record 
     *  @throws org.osid.NullArgumentException
     *          <code>availabilityRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(availabilityRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.AvailabilityQueryRecord getAvailabilityQueryRecord(org.osid.type.Type availabilityRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.records.AvailabilityQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(availabilityRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(availabilityRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  availabilityRecordType the availability record type 
     *  @return the availability query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>availabilityRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(availabilityRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.AvailabilityQueryInspectorRecord getAvailabilityQueryInspectorRecord(org.osid.type.Type availabilityRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.records.AvailabilityQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(availabilityRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(availabilityRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param availabilityRecordType the availability record type
     *  @return the availability search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>availabilityRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(availabilityRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.AvailabilitySearchOrderRecord getAvailabilitySearchOrderRecord(org.osid.type.Type availabilityRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.records.AvailabilitySearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(availabilityRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(availabilityRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this availability. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param availabilityQueryRecord the availability query record
     *  @param availabilityQueryInspectorRecord the availability query inspector
     *         record
     *  @param availabilitySearchOrderRecord the availability search order record
     *  @param availabilityRecordType availability record type
     *  @throws org.osid.NullArgumentException
     *          <code>availabilityQueryRecord</code>,
     *          <code>availabilityQueryInspectorRecord</code>,
     *          <code>availabilitySearchOrderRecord</code> or
     *          <code>availabilityRecordTypeavailability</code> is
     *          <code>null</code>
     */
            
    protected void addAvailabilityRecords(org.osid.resourcing.records.AvailabilityQueryRecord availabilityQueryRecord, 
                                      org.osid.resourcing.records.AvailabilityQueryInspectorRecord availabilityQueryInspectorRecord, 
                                      org.osid.resourcing.records.AvailabilitySearchOrderRecord availabilitySearchOrderRecord, 
                                      org.osid.type.Type availabilityRecordType) {

        addRecordType(availabilityRecordType);

        nullarg(availabilityQueryRecord, "availability query record");
        nullarg(availabilityQueryInspectorRecord, "availability query inspector record");
        nullarg(availabilitySearchOrderRecord, "availability search odrer record");

        this.queryRecords.add(availabilityQueryRecord);
        this.queryInspectorRecords.add(availabilityQueryInspectorRecord);
        this.searchOrderRecords.add(availabilitySearchOrderRecord);
        
        return;
    }
}
