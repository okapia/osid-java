//
// MutableMapProxyCanonicalUnitLookupSession
//
//    Implements a CanonicalUnit lookup service backed by a collection of
//    canonicalUnits that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.offering;


/**
 *  Implements a CanonicalUnit lookup service backed by a collection of
 *  canonicalUnits. The canonicalUnits are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of canonical units can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyCanonicalUnitLookupSession
    extends net.okapia.osid.jamocha.core.offering.spi.AbstractMapCanonicalUnitLookupSession
    implements org.osid.offering.CanonicalUnitLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyCanonicalUnitLookupSession}
     *  with no canonical units.
     *
     *  @param catalogue the catalogue
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code catalogue} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyCanonicalUnitLookupSession(org.osid.offering.Catalogue catalogue,
                                                  org.osid.proxy.Proxy proxy) {
        setCatalogue(catalogue);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyCanonicalUnitLookupSession} with a
     *  single canonical unit.
     *
     *  @param catalogue the catalogue
     *  @param canonicalUnit a canonical unit
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code catalogue},
     *          {@code canonicalUnit}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyCanonicalUnitLookupSession(org.osid.offering.Catalogue catalogue,
                                                org.osid.offering.CanonicalUnit canonicalUnit, org.osid.proxy.Proxy proxy) {
        this(catalogue, proxy);
        putCanonicalUnit(canonicalUnit);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyCanonicalUnitLookupSession} using an
     *  array of canonical units.
     *
     *  @param catalogue the catalogue
     *  @param canonicalUnits an array of canonical units
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code catalogue},
     *          {@code canonicalUnits}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyCanonicalUnitLookupSession(org.osid.offering.Catalogue catalogue,
                                                org.osid.offering.CanonicalUnit[] canonicalUnits, org.osid.proxy.Proxy proxy) {
        this(catalogue, proxy);
        putCanonicalUnits(canonicalUnits);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyCanonicalUnitLookupSession} using a
     *  collection of canonical units.
     *
     *  @param catalogue the catalogue
     *  @param canonicalUnits a collection of canonical units
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code catalogue},
     *          {@code canonicalUnits}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyCanonicalUnitLookupSession(org.osid.offering.Catalogue catalogue,
                                                java.util.Collection<? extends org.osid.offering.CanonicalUnit> canonicalUnits,
                                                org.osid.proxy.Proxy proxy) {
   
        this(catalogue, proxy);
        setSessionProxy(proxy);
        putCanonicalUnits(canonicalUnits);
        return;
    }

    
    /**
     *  Makes a {@code CanonicalUnit} available in this session.
     *
     *  @param canonicalUnit an canonical unit
     *  @throws org.osid.NullArgumentException {@code canonicalUnit{@code 
     *          is {@code null}
     */

    @Override
    public void putCanonicalUnit(org.osid.offering.CanonicalUnit canonicalUnit) {
        super.putCanonicalUnit(canonicalUnit);
        return;
    }


    /**
     *  Makes an array of canonicalUnits available in this session.
     *
     *  @param canonicalUnits an array of canonical units
     *  @throws org.osid.NullArgumentException {@code canonicalUnits{@code 
     *          is {@code null}
     */

    @Override
    public void putCanonicalUnits(org.osid.offering.CanonicalUnit[] canonicalUnits) {
        super.putCanonicalUnits(canonicalUnits);
        return;
    }


    /**
     *  Makes collection of canonical units available in this session.
     *
     *  @param canonicalUnits
     *  @throws org.osid.NullArgumentException {@code canonicalUnit{@code 
     *          is {@code null}
     */

    @Override
    public void putCanonicalUnits(java.util.Collection<? extends org.osid.offering.CanonicalUnit> canonicalUnits) {
        super.putCanonicalUnits(canonicalUnits);
        return;
    }


    /**
     *  Removes a CanonicalUnit from this session.
     *
     *  @param canonicalUnitId the {@code Id} of the canonical unit
     *  @throws org.osid.NullArgumentException {@code canonicalUnitId{@code  is
     *          {@code null}
     */

    @Override
    public void removeCanonicalUnit(org.osid.id.Id canonicalUnitId) {
        super.removeCanonicalUnit(canonicalUnitId);
        return;
    }    
}
