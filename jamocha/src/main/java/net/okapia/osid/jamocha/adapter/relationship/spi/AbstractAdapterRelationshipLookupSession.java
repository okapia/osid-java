//
// AbstractAdapterRelationshipLookupSession.java
//
//    A Relationship lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.relationship.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Relationship lookup session adapter.
 */

public abstract class AbstractAdapterRelationshipLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.relationship.RelationshipLookupSession {

    private final org.osid.relationship.RelationshipLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterRelationshipLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterRelationshipLookupSession(org.osid.relationship.RelationshipLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Family/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Family Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFamilyId() {
        return (this.session.getFamilyId());
    }


    /**
     *  Gets the {@code Family} associated with this session.
     *
     *  @return the {@code Family} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.Family getFamily()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getFamily());
    }


    /**
     *  Tests if this user can perform {@code Relationship} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupRelationships() {
        return (this.session.canLookupRelationships());
    }


    /**
     *  A complete view of the {@code Relationship} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRelationshipView() {
        this.session.useComparativeRelationshipView();
        return;
    }


    /**
     *  A complete view of the {@code Relationship} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRelationshipView() {
        this.session.usePlenaryRelationshipView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include relationships in families which are children
     *  of this family in the family hierarchy.
     */

    @OSID @Override
    public void useFederatedFamilyView() {
        this.session.useFederatedFamilyView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this family only.
     */

    @OSID @Override
    public void useIsolatedFamilyView() {
        this.session.useIsolatedFamilyView();
        return;
    }
    

    /**
     *  Only relationships whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveRelationshipView() {
        this.session.useEffectiveRelationshipView();
        return;
    }
    

    /**
     *  All relationships of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveRelationshipView() {
        this.session.useAnyEffectiveRelationshipView();
        return;
    }

     
    /**
     *  Gets the {@code Relationship} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Relationship} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Relationship} and
     *  retained for compatibility.
     *
     *  In effective mode, relationships are returned that are currently
     *  effective.  In any effective mode, effective relationships and
     *  those currently expired are returned.
     *
     *  @param relationshipId {@code Id} of the {@code Relationship}
     *  @return the relationship
     *  @throws org.osid.NotFoundException {@code relationshipId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code relationshipId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.Relationship getRelationship(org.osid.id.Id relationshipId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelationship(relationshipId));
    }


    /**
     *  Gets a {@code RelationshipList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  relationships specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Relationships} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, relationships are returned that are currently
     *  effective.  In any effective mode, effective relationships and
     *  those currently expired are returned.
     *
     *  @param  relationshipIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Relationship} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code relationshipIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByIds(org.osid.id.IdList relationshipIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelationshipsByIds(relationshipIds));
    }


    /**
     *  Gets a {@code RelationshipList} corresponding to the given
     *  relationship genus {@code Type} which does not include
     *  relationships of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned list
     *  may contain only those relationships that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, relationships are returned that are currently
     *  effective.  In any effective mode, effective relationships and
     *  those currently expired are returned.
     *
     *  @param  relationshipGenusType a relationship genus type 
     *  @return the returned {@code Relationship} list
     *  @throws org.osid.NullArgumentException
     *          {@code relationshipGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByGenusType(org.osid.type.Type relationshipGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelationshipsByGenusType(relationshipGenusType));
    }


    /**
     *  Gets a {@code RelationshipList} corresponding to the given
     *  relationship genus {@code Type} and include any additional
     *  relationships with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned list
     *  may contain only those relationships that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, relationships are returned that are currently
     *  effective.  In any effective mode, effective relationships and
     *  those currently expired are returned.
     *
     *  @param  relationshipGenusType a relationship genus type 
     *  @return the returned {@code Relationship} list
     *  @throws org.osid.NullArgumentException
     *          {@code relationshipGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByParentGenusType(org.osid.type.Type relationshipGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelationshipsByParentGenusType(relationshipGenusType));
    }


    /**
     *  Gets a {@code RelationshipList} containing the given
     *  relationship record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned list
     *  may contain only those relationships that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, relationships are returned that are currently
     *  effective.  In any effective mode, effective relationships and
     *  those currently expired are returned.
     *
     *  @param  relationshipRecordType a relationship record type 
     *  @return the returned {@code Relationship} list
     *  @throws org.osid.NullArgumentException
     *          {@code relationshipRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByRecordType(org.osid.type.Type relationshipRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelationshipsByRecordType(relationshipRecordType));
    }


    /**
     *  Gets a {@code RelationshipList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those relationships that are accessible
     *  through this session.
     *  
     *  In active mode, relationships are returned that are currently
     *  active. In any status mode, active and inactive relationships
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Relationship} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsOnDate(org.osid.calendaring.DateTime from, 
                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelationshipsOnDate(from, to));
    }
        

    /**
     *  Gets a list of relationships corresponding to a source {@code
     *  Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those relationships that are accessible
     *  through this session.
     *
     *  In effective mode, relationships are returned that are
     *  currently effective.  In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param  sourceId the {@code Id} of the source
     *  @return the returned {@code RelationshipList}
     *  @throws org.osid.NullArgumentException {@code sourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsForSource(org.osid.id.Id sourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelationshipsForSource(sourceId));
    }


    /**
     *  Gets a list of relationships corresponding to a source {@code
     *  Id} and effective during the entire given date range inclusive
     *  but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those relationships that are accessible
     *  through this session.
     *
     *  In effective mode, relationships are returned that are
     *  currently effective.  In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param  sourceId the {@code Id} of the source
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code RelationshipList}
     *  @throws org.osid.NullArgumentException {@code sourceId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsForSourceOnDate(org.osid.id.Id sourceId,
                                                                                  org.osid.calendaring.DateTime from,
                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelationshipsForSourceOnDate(sourceId, from, to));
    }


    /**
     *  Gets a {@code RelationshipList} corresponding to the given
     *  peer {@code Id} and relationship genus {@code
     *  Type. Relationships} of any genus derived from the given genus
     *  are returned.
     *  
     *  In plenary mode, the returned list contains all of the
     *  relationships corresponding to the given peer, including
     *  duplicates, or an error results if a relationship is
     *  inaccessible. Otherwise, inaccessible {@code Relationships}
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *  
     *  In effective mode, relationships are returned that are
     *  currently effective. In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param  sourceId a peer {@code Id} 
     *  @param  relationshipGenusType a relationship genus type 
     *  @return the relationships 
     *  @throws org.osid.NullArgumentException {@code sourceId} or 
     *          {@code relationshipGenusType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByGenusTypeForSource(org.osid.id.Id sourceId, 
                                                                                       org.osid.type.Type relationshipGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelationshipsByGenusTypeForSource(sourceId, relationshipGenusType));
    }


    /**
     *  Gets a {@code RelationshipList} corresponding to the given
     *  peer {@code Id} and relationship genus {@code Type} and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all of the
     *  relationships corresponding to the given peer or an error
     *  results if a relationship is inaccessible. Otherwise,
     *  inaccessible {@code Relationships} may be omitted from the
     *  list.
     *  
     *  In effective mode, relationships are returned that are currently 
     *  effective in addition to being effective during the given dates. In 
     *  any effective mode, effective relationships and those currently 
     *  expired are returned. 
     *
     *  @param  sourceId a peer {@code Id} 
     *  @param  relationshipGenusType a relationship genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the relationships 
     *  @throws org.osid.InvalidArgumentException {@code from is greater than 
     *          to} 
     *  @throws org.osid.NullArgumentException {@code sourceId, 
     *          relationshipGenusType, from} or {@code to} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByGenusTypeForSourceOnDate(org.osid.id.Id sourceId, 
                                                                                             org.osid.type.Type relationshipGenusType, 
                                                                                             org.osid.calendaring.DateTime from, 
                                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelationshipsByGenusTypeForSourceOnDate(sourceId, relationshipGenusType, from, to));
    }


    /**
     *  Gets a list of relationships corresponding to a destination
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those relationships that are accessible
     *  through this session.
     *
     *  In effective mode, relationships are returned that are
     *  currently effective.  In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param  destinationId the {@code Id} of the destination
     *  @return the returned {@code RelationshipList}
     *  @throws org.osid.NullArgumentException {@code destinationId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsForDestination(org.osid.id.Id destinationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelationshipsForDestination(destinationId));
    }


    /**
     *  Gets a list of relationships corresponding to a destination
     *  {@code Id} and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those relationships that are accessible
     *  through this session.
     *
     *  In effective mode, relationships are returned that are
     *  currently effective.  In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param  destinationId the {@code Id} of the destination
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code RelationshipList}
     *  @throws org.osid.NullArgumentException {@code destinationId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsForDestinationOnDate(org.osid.id.Id destinationId,
                                                                                       org.osid.calendaring.DateTime from,
                                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelationshipsForDestinationOnDate(destinationId, from, to));
    }


    /**
     *  Gets a {@code RelationshipList} corresponding to the given
     *  peer {@code Id} and relationship genus {@code
     *  Type. Relationships} of any genus derived from the given genus
     *  are returned.
     *  
     *  In plenary mode, the returned list contains all of the
     *  relationships corresponding to the given peer, including
     *  duplicates, or an error results if a relationship is
     *  inaccessible. Otherwise, inaccessible {@code Relationships}
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *  
     *  In effective mode, relationships are returned that are
     *  currently effective. In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param  destinationId a peer {@code Id} 
     *  @param  relationshipGenusType a relationship genus type 
     *  @return the relationships 
     *  @throws org.osid.NullArgumentException {@code destinationId} or 
     *          {@code relationshipGenusType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByGenusTypeForDestination(org.osid.id.Id destinationId, 
                                                                                            org.osid.type.Type relationshipGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getRelationshipsByGenusTypeForDestination(destinationId, relationshipGenusType));
    }


    /**
     *  Gets a {@code RelationshipList} corresponding to the given
     *  peer {@code Id} and relationship genus {@code Type} and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all of the
     *  relationships corresponding to the given peer or an error
     *  results if a relationship is inaccessible. Otherwise,
     *  inaccessible {@code Relationships} may be omitted from the
     *  list.
     *  
     *  In effective mode, relationships are returned that are
     *  currently effective in addition to being effective during the
     *  given dates. In any effective mode, effective relationships
     *  and those currently expired are returned.
     *
     *  @param  destinationId a peer {@code Id} 
     *  @param  relationshipGenusType a relationship genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the relationships 
     *  @throws org.osid.InvalidArgumentException {@code from is greater than 
     *          to} 
     *  @throws org.osid.NullArgumentException {@code destinationId, 
     *          relationshipGenusType, from} or {@code to} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
    
    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByGenusTypeForDestinationOnDate(org.osid.id.Id destinationId, 
                                                                                                  org.osid.type.Type relationshipGenusType, 
                                                                                                  org.osid.calendaring.DateTime from, 
                                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelationshipsByGenusTypeForDestinationOnDate(destinationId, relationshipGenusType, from, to));
    }


    /**
     *  Gets a list of relationships corresponding to source and
     *  destination {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those relationships that are accessible
     *  through this session.
     *
     *  In effective mode, relationships are returned that are
     *  currently effective.  In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param  sourceId the {@code Id} of the source
     *  @param  destinationId the {@code Id} of the destination
     *  @return the returned {@code RelationshipList}
     *  @throws org.osid.NullArgumentException {@code sourceId},
     *          {@code destinationId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsForPeers(org.osid.id.Id sourceId,
                                                                           org.osid.id.Id destinationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelationshipsForPeers(sourceId, destinationId));
    }


    /**
     *  Gets a list of relationships corresponding to source and
     *  destination {@code Ids} and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those relationships that are accessible
     *  through this session.
     *
     *  In effective mode, relationships are returned that are
     *  currently effective. In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param  destinationId the {@code Id} of the destination
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code RelationshipList}
     *  @throws org.osid.NullArgumentException {@code sourceId},
     *          {@code destinationId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsForPeersOnDate(org.osid.id.Id sourceId,
                                                                                 org.osid.id.Id destinationId,
                                                                                 org.osid.calendaring.DateTime from,
                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelationshipsForPeersOnDate(sourceId, destinationId, from, to));
    }


    /**
     *  Gets a {@code RelationshipList} corresponding between the
     *  given peer {@code Ids} and relationship genus {@code Type.
     *  Relationships} of any genus derived from the given genus are
     *  returned.
     *  
     *  In plenary mode, the returned list contains all of the
     *  relationships corresponding to the given peer or an error
     *  results if a relationship is inaccessible. Otherwise,
     *  inaccessible {@code Relationships} may be omitted from the
     *  list.
     *  
     *  In effective mode, relationships are returned that are
     *  currently effective. In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param  sourceId a peer {@code Id} 
     *  @param  destinationId a related peer {@code Id} 
     *  @param  relationshipGenusType a relationship genus type 
     *  @return the relationships 
     *  @throws org.osid.NullArgumentException {@code sourceId,
     *         destinationId}, or {@code relationshipGenusType} is
     *         {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByGenusTypeForPeers(org.osid.id.Id sourceId, 
                                                                                      org.osid.id.Id destinationId, 
                                                                                      org.osid.type.Type relationshipGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getRelationshipsByGenusTypeForPeers(sourceId, destinationId, relationshipGenusType));
    }


    /**
     *  Gets a {@code RelationshipList} effective during the entire
     *  given date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all of the
     *  relationships corresponding to the given peer set or an error
     *  results if a relationship is inaccessible. Otherwise,
     *  inaccessible {@code Relationships} may be omitted from the
     *  list.
     *  
     *  In effective mode, relationships are returned that are
     *  currently effective in addition to being effective during the
     *  given dates. In any effective mode, effective relationships
     *  and those currently expired are returned.
     *
     *  @param  sourceId a peer {@code Id} 
     *  @param  destinationId a related peer {@code Id} 
     *  @param  relationshipGenusType a relationship genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the relationships 
     *  @throws org.osid.InvalidArgumentException {@code from is
     *          greater than to}
     *  @throws org.osid.NullArgumentException {@code sourceId, destinationId, 
     *          relationshipGenusType, from} or {@code to} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByGenusTypeForPeersOnDate(org.osid.id.Id sourceId, 
                                                                                            org.osid.id.Id destinationId, 
                                                                                            org.osid.type.Type relationshipGenusType, 
                                                                                            org.osid.calendaring.DateTime from, 
                                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getRelationshipsByGenusTypeForPeersOnDate(sourceId, destinationId, relationshipGenusType, from, to));
    }

    
    /**
     *  Gets all {@code Relationships}. 
     *
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned list
     *  may contain only those relationships that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, relationships are returned that are currently
     *  effective.  In any effective mode, effective relationships and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Relationships} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationships()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelationships());
    }
}
