//
// AbstractEntryLookupSession.java
//
//    A starter implementation framework for providing an Entry
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.blogging.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an Entry
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getEntries(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractEntryLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.blogging.EntryLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.blogging.Blog blog = new net.okapia.osid.jamocha.nil.blogging.blog.UnknownBlog();
    

    /**
     *  Gets the <code>Blog/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Blog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBlogId() {
        return (this.blog.getId());
    }


    /**
     *  Gets the <code>Blog</code> associated with this 
     *  session.
     *
     *  @return the <code>Blog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.Blog getBlog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.blog);
    }


    /**
     *  Sets the <code>Blog</code>.
     *
     *  @param  blog the blog for this session
     *  @throws org.osid.NullArgumentException <code>blog</code>
     *          is <code>null</code>
     */

    protected void setBlog(org.osid.blogging.Blog blog) {
        nullarg(blog, "blog");
        this.blog = blog;
        return;
    }

    /**
     *  Tests if this user can perform <code>Entry</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupEntries() {
        return (true);
    }


    /**
     *  A complete view of the <code>Entry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeEntryView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Entry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryEntryView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include entries in blogs which are
     *  children of this blog in the blog hierarchy.
     */

    @OSID @Override
    public void useFederatedBlogView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this blog only.
     */

    @OSID @Override
    public void useIsolatedBlogView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>Entry</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Entry</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Entry</code> and
     *  retained for compatibility.
     *
     *  @param  entryId <code>Id</code> of the
     *          <code>Entry</code>
     *  @return the entry
     *  @throws org.osid.NotFoundException <code>entryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>entryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.Entry getEntry(org.osid.id.Id entryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.blogging.EntryList entries = getEntries()) {
            while (entries.hasNext()) {
                org.osid.blogging.Entry entry = entries.getNextEntry();
                if (entry.getId().equals(entryId)) {
                    return (entry);
                }
            }
        } 

        throw new org.osid.NotFoundException(entryId + " not found");
    }


    /**
     *  Gets a <code>EntryList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  entries specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Entries</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getEntries()</code>.
     *
     *  @param  entryIds the list of <code>Ids</code> to rerieve 
     *  @return the returned <code>Entry</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>entryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.EntryList getEntriesByIds(org.osid.id.IdList entryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.blogging.Entry> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = entryIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getEntry(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("entry " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.blogging.entry.LinkedEntryList(ret));
    }


    /**
     *  Gets a <code>EntryList</code> corresponding to the given
     *  entry genus <code>Type</code> which does not include
     *  entries of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  entries or an error results. Otherwise, the returned list
     *  may contain only those entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getEntries()</code>.
     *
     *  @param  entryGenusType an entry genus type 
     *  @return the returned <code>Entry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>entryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.EntryList getEntriesByGenusType(org.osid.type.Type entryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.blogging.entry.EntryGenusFilterList(getEntries(), entryGenusType));
    }


    /**
     *  Gets a <code>EntryList</code> corresponding to the given
     *  entry genus <code>Type</code> and include any additional
     *  entries with genus types derived from the specified
     *  <code>Type</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  entries or an error results. Otherwise, the returned list
     *  may contain only those entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getEntries()</code>.
     *
     *  @param  entryGenusType an entry genus type 
     *  @return the returned <code>Entry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>entryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.EntryList getEntriesByParentGenusType(org.osid.type.Type entryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getEntriesByGenusType(entryGenusType));
    }


    /**
     *  Gets a <code>EntryList</code> containing the given
     *  entry record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  entries or an error results. Otherwise, the returned list
     *  may contain only those entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getEntries()</code>.
     *
     *  @param  entryRecordType an entry record type 
     *  @return the returned <code>Entry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>entryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.EntryList getEntriesByRecordType(org.osid.type.Type entryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.blogging.entry.EntryRecordFilterList(getEntries(), entryRecordType));
    }


    /**
     *  Gets an <code> EntryList </code> posted within the specified
     *  range inclusive. In plenary mode, the returned list contains
     *  all known entries or an error results. Otherwise, the returned
     *  list may contain only those entries that are accessible
     *  through this session.
     *
     *  @param  from starting date
     *  @param  to ending date
     *  @return the returned <code> Entry </code> list
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less
     *          than <code> from </code>
     *  @throws org.osid.NullArgumentException <code> from </code> or <code>
     *          to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.blogging.EntryList getEntriesByDate(org.osid.calendaring.DateTime from,
                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.blogging.entry.EntryFilterList(new TimestampFilter(from, to), getEntries()));
    }    


    /**
     *  Gets an <code> EntryList </code> for the given poster resource
     *  <code> Id. </code> In plenary mode, the returned list contains
     *  all known entries or an error results. Otherwise, the returned
     *  list may contain only those entries that are accessible
     *  through this session.
     *
     *  @param  resourceId a poster <code> Id </code>
     *  @return the returned <code> Entry </code> list
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.blogging.EntryList getEntriesForPoster(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.blogging.entry.EntryFilterList(new PosterFilter(resourceId), getEntries()));
    }


    /**
     *  Gets an <code> EntryList </code> for a poster resource <code> Id
     *  </code> posted within the specified range inclusive. In plenary mode,
     *  the returned list contains all known entries or an error results.
     *  Otherwise, the returned list may contain only those entries that are
     *  accessible through this session.
     *
     *  @param  resourceId a poster <code> Id </code>
     *  @param  from starting date
     *  @param  to ending date
     *  @return the returned <code> Entry </code> list
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less
     *          than <code> from </code>
     *  @throws org.osid.NullArgumentException <code> resourceId, from </code>
     *          or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.blogging.EntryList getEntriesByDateForPoster(org.osid.id.Id resourceId,
                                                                 org.osid.calendaring.DateTime from,
                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.blogging.entry.EntryFilterList(new TimestampFilter(from, to), getEntriesForPoster(resourceId)));
    }


    /**
     *  Gets an <code>EntryList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known entries or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  entries that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Entry</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.blogging.EntryList getEntriesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.blogging.entry.EntryProviderFilterList(getEntries(), resourceId));        
    }


    /**
     *  Gets all <code>Entries</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  entries or an error results. Otherwise, the returned list
     *  may contain only those entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Entries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.blogging.EntryList getEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the entry list for active and effective views. Should
     *  be called by <code>getObjects()</code> if no filtering is
     *  already performed.
     *
     *  @param list the list of entries
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.blogging.EntryList filterEntriesOnViews(org.osid.blogging.EntryList list)
        throws org.osid.OperationFailedException {
            
        return (list);
    }


    public static class TimestampFilter
        implements net.okapia.osid.jamocha.inline.filter.blogging.entry.EntryFilter {

        private final org.osid.calendaring.DateTime from;
        private final org.osid.calendaring.DateTime to;


        /**
         *  Constructs a new <code>PosterFilter</code>.
         *
         *  @param from start of date range
         *  @param to end of date range
         *  @throws org.osid.NullArgumentException <code>from</code>
         *          or <code>to</code> is <code>null</code>
         */

        public TimestampFilter(org.osid.calendaring.DateTime from, org.osid.calendaring.DateTime to) {
            nullarg(from, "starting date");
            nullarg(to, "ending date");

            this.from = from;
            this.to = to;

            return;
        }


        /**
         *  Used by the EntryFilterList to filter the
         *  entry list based on poster.
         *
         *  @param entry the entry
         *  @return <code>true</code> to pass the entry,
         *          <code>false</code> to filter it
         */
       
        @Override
        public boolean pass(org.osid.blogging.Entry entry) {
            if (entry.getTimestamp().isLess(this.from)) {
                return (false);
            }

            if (entry.getTimestamp().isGreater(this.to)) {
                return (false);
            }

            return (true);
        }
    }    


    public static class PosterFilter
        implements net.okapia.osid.jamocha.inline.filter.blogging.entry.EntryFilter {

        private final org.osid.id.Id posterId;


        /**
         *  Constructs a new <code>PosterFilter</code>.
         *
         *  @param posterId the poster to filter
         *  @throws org.osid.NullArgumentException
         *          <code>posterId</code> is <code>null</code>
         */

        public PosterFilter(org.osid.id.Id posterId) {
            nullarg(posterId, "poster Id");
            this.posterId = posterId;
            return;
        }


        /**
         *  Used by the EntryFilterList to filter the
         *  entry list based on poster.
         *
         *  @param entry the entry
         *  @return <code>true</code> to pass the entry,
         *          <code>false</code> to filter it
         */
       
        @Override
        public boolean pass(org.osid.blogging.Entry entry) {
            return (entry.getPosterId().equals(this.posterId));
        }
    }    
}
