//
// AbstractQueryCommentLookupSession.java
//
//    An inline adapter that maps a CommentLookupSession to
//    a CommentQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.commenting.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a CommentLookupSession to
 *  a CommentQuerySession.
 */

public abstract class AbstractQueryCommentLookupSession
    extends net.okapia.osid.jamocha.commenting.spi.AbstractCommentLookupSession
    implements org.osid.commenting.CommentLookupSession {

    private boolean effectiveonly = false;
    private final org.osid.commenting.CommentQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryCommentLookupSession.
     *
     *  @param querySession the underlying comment query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryCommentLookupSession(org.osid.commenting.CommentQuerySession querySession) {
        nullarg(querySession, "comment query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Book</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Book Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBookId() {
        return (this.session.getBookId());
    }


    /**
     *  Gets the <code>Book</code> associated with this 
     *  session.
     *
     *  @return the <code>Book</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.Book getBook()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getBook());
    }


    /**
     *  Tests if this user can perform <code>Comment</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupComments() {
        return (this.session.canSearchComments());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include comments in books which are children
     *  of this book in the book hierarchy.
     */

    @OSID @Override
    public void useFederatedBookView() {
        this.session.useFederatedBookView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this book only.
     */

    @OSID @Override
    public void useIsolatedBookView() {
        this.session.useIsolatedBookView();
        return;
    }
    

    /**
     *  Only comments whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveCommentView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All comments of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveCommentView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Comment</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Comment</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Comment</code> and
     *  retained for compatibility.
     *
     *  In effective mode, comments are returned that are currently
     *  effective.  In any effective mode, effective comments and
     *  those currently expired are returned.
     *
     *  @param  commentId <code>Id</code> of the
     *          <code>Comment</code>
     *  @return the comment
     *  @throws org.osid.NotFoundException <code>commentId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>commentId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.Comment getComment(org.osid.id.Id commentId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.commenting.CommentQuery query = getQuery();
        query.matchId(commentId, true);
        org.osid.commenting.CommentList comments = this.session.getCommentsByQuery(query);
        if (comments.hasNext()) {
            return (comments.getNextComment());
        } 
        
        throw new org.osid.NotFoundException(commentId + " not found");
    }


    /**
     *  Gets a <code>CommentList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  comments specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Comments</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, comments are returned that are currently effective.
     *  In any effective mode, effective comments and those currently expired
     *  are returned.
     *
     *  @param  commentIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Comment</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>commentIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsByIds(org.osid.id.IdList commentIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.commenting.CommentQuery query = getQuery();

        try (org.osid.id.IdList ids = commentIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getCommentsByQuery(query));
    }


    /**
     *  Gets a <code>CommentList</code> corresponding to the given
     *  comment genus <code>Type</code> which does not include
     *  comments of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  comments or an error results. Otherwise, the returned list
     *  may contain only those comments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, comments are returned that are currently effective.
     *  In any effective mode, effective comments and those currently expired
     *  are returned.
     *
     *  @param  commentGenusType a comment genus type 
     *  @return the returned <code>Comment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commentGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsByGenusType(org.osid.type.Type commentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.commenting.CommentQuery query = getQuery();
        query.matchGenusType(commentGenusType, true);
        return (this.session.getCommentsByQuery(query));
    }


    /**
     *  Gets a <code>CommentList</code> corresponding to the given
     *  comment genus <code>Type</code> and include any additional
     *  comments with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  comments or an error results. Otherwise, the returned list
     *  may contain only those comments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, comments are returned that are currently
     *  effective.  In any effective mode, effective comments and
     *  those currently expired are returned.
     *
     *  @param  commentGenusType a comment genus type 
     *  @return the returned <code>Comment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commentGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsByParentGenusType(org.osid.type.Type commentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.commenting.CommentQuery query = getQuery();
        query.matchParentGenusType(commentGenusType, true);
        return (this.session.getCommentsByQuery(query));
    }


    /**
     *  Gets a <code>CommentList</code> containing the given
     *  comment record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  comments or an error results. Otherwise, the returned list
     *  may contain only those comments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, comments are returned that are currently
     *  effective.  In any effective mode, effective comments and
     *  those currently expired are returned.
     *
     *  @param  commentRecordType a comment record type 
     *  @return the returned <code>Comment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commentRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsByRecordType(org.osid.type.Type commentRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.commenting.CommentQuery query = getQuery();
        query.matchRecordType(commentRecordType, true);
        return (this.session.getCommentsByQuery(query));
    }


    /**
     *  Gets a <code>CommentList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  comments or an error results. Otherwise, the returned list
     *  may contain only those comments that are accessible
     *  through this session.
     *  
     *  In effective mode, comments are returned that are currently
     *  effective.  In any effective mode, effective comments and
     *  those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Comment</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.commenting.CommentList getCommentsOnDate(org.osid.calendaring.DateTime from, 
                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.commenting.CommentQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getCommentsByQuery(query));
    }
        

    /**
     *  Gets a <code> CommentList </code> of a given genus type and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known comments
     *  or an error results. Otherwise, the returned list may contain
     *  only those comments that are accessible through this session.
     *  
     *  In effective mode, comments are returned that are currently
     *  effective.  In any effective mode, effective comments and
     *  those currently expired are returned.
     *
     *  @param  commentGenusType a comment genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code> Comment </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> commentGenusType, from, 
     *          </code> or <code> to </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsByGenusTypeOnDate(org.osid.type.Type commentGenusType, 
                                                                        org.osid.calendaring.DateTime from, 
                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.commenting.CommentQuery query = getQuery();
        query.matchGenusType(commentGenusType, true);
        query.matchDate(from, to, true);
        return (this.session.getCommentsByQuery(query));
    }

    
    /**
     *  Gets a list of comments corresponding to a commentor
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  comments or an error results. Otherwise, the returned list
     *  may contain only those comments that are accessible
     *  through this session.
     *
     *  In effective mode, comments are returned that are
     *  currently effective.  In any effective mode, effective
     *  comments and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the commentor
     *  @return the returned <code>CommentList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.commenting.CommentList getCommentsForCommentor(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.commenting.CommentQuery query = getQuery();
        query.matchCommentorId(resourceId, true);
        return (this.session.getCommentsByQuery(query));
    }


    /**
     *  Gets a list of comments corresponding to a commentor
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  comments or an error results. Otherwise, the returned list
     *  may contain only those comments that are accessible
     *  through this session.
     *
     *  In effective mode, comments are returned that are
     *  currently effective.  In any effective mode, effective
     *  comments and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the commentor
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>CommentList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsForCommentorOnDate(org.osid.id.Id resourceId,
                                                                         org.osid.calendaring.DateTime from,
                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.commenting.CommentQuery query = getQuery();
        query.matchCommentorId(resourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getCommentsByQuery(query));
    }


    /**
     *  Gets a list of comments of the given genus type corresponding
     *  to a resource <code> Id. </code>
     *  
     *  In plenary mode, the returned list contains all known comments
     *  or an error results. Otherwise, the returned list may contain
     *  only those comments that are accessible through this session.
     *  
     *  In effective mode, comments are returned that are currently
     *  effective.  In any effective mode, effective comments and
     *  those currently expired are returned.
     *
     *  @param  resourceId the <code> Id </code> of the resource 
     *  @param  commentGenusType the comment genus type 
     *  @return the returned <code> CommentList </code> 
     *  @throws org.osid.NullArgumentException <code> resourceId
     *          </code> or <code> commentGenusType </code> is <code>
     *          null </code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsByGenusTypeForCommentor(org.osid.id.Id resourceId, 
                                                                              org.osid.type.Type commentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.commenting.CommentQuery query = getQuery();
        query.matchCommentorId(resourceId, true);
        query.matchGenusType(commentGenusType, true);
        return (this.session.getCommentsByQuery(query));
    }


    /**
     *  Gets a list of all comments of the given genus type
     *  corresponding to a resource <code> Id </code> and effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known comments
     *  or an error results. Otherwise, the returned list may contain
     *  only those comments that are accessible through this session.
     *  
     *  In effective mode, comments are returned that are currently
     *  effective.  In any effective mode, effective comments and
     *  those currently expired are returned.
     *
     *  @param  resourceId the <code> Id </code> of the resource 
     *  @param  commentGenusType the comment genus type 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned <code> CommentList </code> 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     *  @throws org.osid.NullArgumentException <code> resourceId, 
     *          commentGenusType, from, </code> or <code> to </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsByGenusTypeForCommentorOnDate(org.osid.id.Id resourceId, 
                                                                                    org.osid.type.Type commentGenusType, 
                                                                                    org.osid.calendaring.DateTime from, 
                                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.commenting.CommentQuery query = getQuery();
        query.matchCommentorId(resourceId, true);
        query.matchGenusType(commentGenusType, true);
        query.matchDate(from, to, true);
        return (this.session.getCommentsByQuery(query));
    }


    /**
     *  Gets a list of comments corresponding to a reference
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  comments or an error results. Otherwise, the returned list
     *  may contain only those comments that are accessible
     *  through this session.
     *
     *  In effective mode, comments are returned that are
     *  currently effective.  In any effective mode, effective
     *  comments and those currently expired are returned.
     *
     *  @param  referenceId the <code>Id</code> of the reference
     *  @return the returned <code>CommentList</code>
     *  @throws org.osid.NullArgumentException <code>referenceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.commenting.CommentList getCommentsForReference(org.osid.id.Id referenceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.commenting.CommentQuery query = getQuery();
        query.matchReferenceId(referenceId, true);
        return (this.session.getCommentsByQuery(query));
    }


    /**
     *  Gets a list of comments corresponding to a reference
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  comments or an error results. Otherwise, the returned list
     *  may contain only those comments that are accessible
     *  through this session.
     *
     *  In effective mode, comments are returned that are
     *  currently effective.  In any effective mode, effective
     *  comments and those currently expired are returned.
     *
     *  @param  referenceId the <code>Id</code> of the reference
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>CommentList</code>
     *  @throws org.osid.NullArgumentException <code>referenceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsForReferenceOnDate(org.osid.id.Id referenceId,
                                                                         org.osid.calendaring.DateTime from,
                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.commenting.CommentQuery query = getQuery();
        query.matchReferenceId(referenceId, true);
        query.matchDate(from, to, true);
        return (this.session.getCommentsByQuery(query));
    }


    /**
     *  Gets a list of comments of the given genus type corresponding
     *  to a reference <code> Id. </code>
     *  
     *  In plenary mode, the returned list contains all known comments
     *  or an error results. Otherwise, the returned list may contain
     *  only those comments that are accessible through this session.
     *  
     *  In effective mode, comments are returned that are currently
     *  effective.  In any effective mode, effective comments and
     *  those currently expired are returned.
     *
     *  @param  referenceId the <code> Id </code> of the reference 
     *  @param  commentGenusType the comment genus type 
     *  @return the returned <code> CommentList </code> 
     *  @throws org.osid.NullArgumentException <code> referenceId
     *          </code> or <code> commentGenusType </code> is <code>
     *          null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsByGenusTypeForReference(org.osid.id.Id referenceId, 
                                                                              org.osid.type.Type commentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.commenting.CommentQuery query = getQuery();
        query.matchReferenceId(referenceId, true);
        query.matchGenusType(commentGenusType, true);
        return (this.session.getCommentsByQuery(query));
    }


    /**
     *  Gets a list of all comments of the given genus type
     *  corresponding to a reference <code> Id </code> and effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known comments
     *  or an error results. Otherwise, the returned list may contain
     *  only those comments that are accessible through this session.
     *  
     *  In effective mode, comments are returned that are currently
     *  effective.  In any effective mode, effective comments and
     *  those currently expired are returned.
     *
     *  @param  referenceId a reference <code> Id </code> 
     *  @param  commentGenusType the comment genus type 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned <code> CommentList </code> 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     *  @throws org.osid.NullArgumentException <code> referenceId, 
     *          commentGenusType, from, </code> or <code> to </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsByGenusTypeForReferenceOnDate(org.osid.id.Id referenceId, 
                                                                                    org.osid.type.Type commentGenusType, 
                                                                                    org.osid.calendaring.DateTime from, 
                                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.commenting.CommentQuery query = getQuery();
        query.matchReferenceId(referenceId, true);
        query.matchGenusType(commentGenusType, true);
        query.matchDate(from, to, true);
        return (this.session.getCommentsByQuery(query));
    }


    /**
     *  Gets a list of comments corresponding to commentor and reference
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  comments or an error results. Otherwise, the returned list
     *  may contain only those comments that are accessible
     *  through this session.
     *
     *  In effective mode, comments are returned that are
     *  currently effective.  In any effective mode, effective
     *  comments and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the commentor
     *  @param  referenceId the <code>Id</code> of the reference
     *  @return the returned <code>CommentList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>referenceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsForCommentorAndReference(org.osid.id.Id resourceId,
                                                                               org.osid.id.Id referenceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.commenting.CommentQuery query = getQuery();
        query.matchCommentorId(resourceId, true);
        query.matchReferenceId(referenceId, true);
        return (this.session.getCommentsByQuery(query));
    }


    /**
     *  Gets a list of comments corresponding to commentor and reference
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  comments or an error results. Otherwise, the returned list
     *  may contain only those comments that are accessible
     *  through this session.
     *
     *  In effective mode, comments are returned that are
     *  currently effective.  In any effective mode, effective
     *  comments and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the commentor
     *  @param  referenceId the <code>Id</code> of the reference
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>CommentList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>referenceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsForCommentorAndReferenceOnDate(org.osid.id.Id resourceId,
                                                                                     org.osid.id.Id referenceId,
                                                                                     org.osid.calendaring.DateTime from,
                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.commenting.CommentQuery query = getQuery();
        query.matchCommentorId(resourceId, true);
        query.matchReferenceId(referenceId, true);
        query.matchDate(from, to, true);
        return (this.session.getCommentsByQuery(query));
    }


    /**
     *  Gets a list of comments of the given genus type corresponding
     *  to a resource and reference <code> Id. </code>
     *  
     *  In plenary mode, the returned list contains all known comments
     *  or an error results. Otherwise, the returned list may contain
     *  only those comments that are accessible through this session.
     *  
     *  In effective mode, comments are returned that are currently
     *  effective.  In any effective mode, effective comments and
     *  those currently expired are returned.
     *
     *  @param  resourceId the <code> Id </code> of the resource 
     *  @param  referenceId the <code> Id </code> of the reference 
     *  @param  commentGenusType the comment genus type 
     *  @return the returned <code> CommentList </code> 
     *  @throws org.osid.NullArgumentException <code> resourceId, referenceId 
     *          </code> or <code> commentGenusType </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsByGenusTypeForCommentorAndReference(org.osid.id.Id resourceId, 
                                                                                          org.osid.id.Id referenceId, 
                                                                                          org.osid.type.Type commentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.commenting.CommentQuery query = getQuery();
        query.matchCommentorId(resourceId, true);
        query.matchReferenceId(referenceId, true);
        query.matchGenusType(commentGenusType, true);
        return (this.session.getCommentsByQuery(query));
    }


    /**
     *  Gets a list of all comments corresponding to a resource and
     *  reference <code> Id </code> and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known comments
     *  or an error results. Otherwise, the returned list may contain
     *  only those comments that are accessible through this session.
     *  
     *  In effective mode, comments are returned that are currently
     *  effective.  In any effective mode, effective comments and
     *  those currently expired are returned.
     *
     *  @param  resourceId the <code> Id </code> of the resource 
     *  @param  referenceId a reference <code> Id </code> 
     *  @param  commentGenusType the comment genus type 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned <code> CommentList </code> 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     *  @throws org.osid.NullArgumentException <code> resourceId, referenceId, 
     *          commentGenusType, from, </code> or <code> to </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsByGenusTypeForCommentorAndReferenceOnDate(org.osid.id.Id resourceId, 
                                                                                                org.osid.id.Id referenceId, 
                                                                                                org.osid.type.Type commentGenusType, 
                                                                                                org.osid.calendaring.DateTime from, 
                                                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.commenting.CommentQuery query = getQuery();
        query.matchCommentorId(resourceId, true);
        query.matchReferenceId(referenceId, true);
        query.matchGenusType(commentGenusType, true);
        query.matchDate(from, to, true);
        return (this.session.getCommentsByQuery(query));
    }


    /**
     *  Gets all <code>Comments</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  comments or an error results. Otherwise, the returned list
     *  may contain only those comments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, comments are returned that are currently
     *  effective.  In any effective mode, effective comments and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Comments</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.CommentList getComments()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.commenting.CommentQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getCommentsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.commenting.CommentQuery getQuery() {
        org.osid.commenting.CommentQuery query = this.session.getCommentQuery();
        
        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }

        return (query);
    }
}
