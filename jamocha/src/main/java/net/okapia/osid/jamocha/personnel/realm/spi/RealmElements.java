//
// RealmElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.personnel.realm.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class RealmElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the RealmElement Id.
     *
     *  @return the realm element Id
     */

    public static org.osid.id.Id getRealmEntityId() {
        return (makeEntityId("osid.personnel.Realm"));
    }


    /**
     *  Gets the PersonId element Id.
     *
     *  @return the PersonId element Id
     */

    public static org.osid.id.Id getPersonId() {
        return (makeQueryElementId("osid.personnel.realm.PersonId"));
    }


    /**
     *  Gets the Person element Id.
     *
     *  @return the Person element Id
     */

    public static org.osid.id.Id getPerson() {
        return (makeQueryElementId("osid.personnel.realm.Person"));
    }


    /**
     *  Gets the OrganizationId element Id.
     *
     *  @return the OrganizationId element Id
     */

    public static org.osid.id.Id getOrganizationId() {
        return (makeQueryElementId("osid.personnel.realm.OrganizationId"));
    }


    /**
     *  Gets the Organization element Id.
     *
     *  @return the Organization element Id
     */

    public static org.osid.id.Id getOrganization() {
        return (makeQueryElementId("osid.personnel.realm.Organization"));
    }


    /**
     *  Gets the PositionId element Id.
     *
     *  @return the PositionId element Id
     */

    public static org.osid.id.Id getPositionId() {
        return (makeQueryElementId("osid.personnel.realm.PositionId"));
    }


    /**
     *  Gets the Position element Id.
     *
     *  @return the Position element Id
     */

    public static org.osid.id.Id getPosition() {
        return (makeQueryElementId("osid.personnel.realm.Position"));
    }


    /**
     *  Gets the AppointmentId element Id.
     *
     *  @return the AppointmentId element Id
     */

    public static org.osid.id.Id getAppointmentId() {
        return (makeQueryElementId("osid.personnel.realm.AppointmentId"));
    }


    /**
     *  Gets the Appointment element Id.
     *
     *  @return the Appointment element Id
     */

    public static org.osid.id.Id getAppointment() {
        return (makeQueryElementId("osid.personnel.realm.Appointment"));
    }


    /**
     *  Gets the AncestorRealmId element Id.
     *
     *  @return the AncestorRealmId element Id
     */

    public static org.osid.id.Id getAncestorRealmId() {
        return (makeQueryElementId("osid.personnel.realm.AncestorRealmId"));
    }


    /**
     *  Gets the AncestorRealm element Id.
     *
     *  @return the AncestorRealm element Id
     */

    public static org.osid.id.Id getAncestorRealm() {
        return (makeQueryElementId("osid.personnel.realm.AncestorRealm"));
    }


    /**
     *  Gets the DescendantRealmId element Id.
     *
     *  @return the DescendantRealmId element Id
     */

    public static org.osid.id.Id getDescendantRealmId() {
        return (makeQueryElementId("osid.personnel.realm.DescendantRealmId"));
    }


    /**
     *  Gets the DescendantRealm element Id.
     *
     *  @return the DescendantRealm element Id
     */

    public static org.osid.id.Id getDescendantRealm() {
        return (makeQueryElementId("osid.personnel.realm.DescendantRealm"));
    }
}
