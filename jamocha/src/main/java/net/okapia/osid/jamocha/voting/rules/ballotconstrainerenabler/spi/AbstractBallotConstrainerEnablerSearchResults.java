//
// AbstractBallotConstrainerEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.rules.ballotconstrainerenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractBallotConstrainerEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.voting.rules.BallotConstrainerEnablerSearchResults {

    private org.osid.voting.rules.BallotConstrainerEnablerList ballotConstrainerEnablers;
    private final org.osid.voting.rules.BallotConstrainerEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.voting.rules.records.BallotConstrainerEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractBallotConstrainerEnablerSearchResults.
     *
     *  @param ballotConstrainerEnablers the result set
     *  @param ballotConstrainerEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>ballotConstrainerEnablers</code>
     *          or <code>ballotConstrainerEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractBallotConstrainerEnablerSearchResults(org.osid.voting.rules.BallotConstrainerEnablerList ballotConstrainerEnablers,
                                            org.osid.voting.rules.BallotConstrainerEnablerQueryInspector ballotConstrainerEnablerQueryInspector) {
        nullarg(ballotConstrainerEnablers, "ballot constrainer enablers");
        nullarg(ballotConstrainerEnablerQueryInspector, "ballot constrainer enabler query inspectpr");

        this.ballotConstrainerEnablers = ballotConstrainerEnablers;
        this.inspector = ballotConstrainerEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the ballot constrainer enabler list resulting from a search.
     *
     *  @return a ballot constrainer enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerList getBallotConstrainerEnablers() {
        if (this.ballotConstrainerEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.voting.rules.BallotConstrainerEnablerList ballotConstrainerEnablers = this.ballotConstrainerEnablers;
        this.ballotConstrainerEnablers = null;
	return (ballotConstrainerEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.voting.rules.BallotConstrainerEnablerQueryInspector getBallotConstrainerEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  ballot constrainer enabler search record <code> Type. </code> This method must
     *  be used to retrieve a ballotConstrainerEnabler implementing the requested
     *  record.
     *
     *  @param ballotConstrainerEnablerSearchRecordType a ballotConstrainerEnabler search 
     *         record type 
     *  @return the ballot constrainer enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(ballotConstrainerEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.rules.records.BallotConstrainerEnablerSearchResultsRecord getBallotConstrainerEnablerSearchResultsRecord(org.osid.type.Type ballotConstrainerEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.voting.rules.records.BallotConstrainerEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(ballotConstrainerEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(ballotConstrainerEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record ballot constrainer enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addBallotConstrainerEnablerRecord(org.osid.voting.rules.records.BallotConstrainerEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "ballot constrainer enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
