//
// AbstractImmutableSequenceRuleEnabler.java
//
//     Wraps a mutable SequenceRuleEnabler to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.assessment.authoring.sequenceruleenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>SequenceRuleEnabler</code> to hide modifiers. This
 *  wrapper provides an immutized SequenceRuleEnabler from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying sequenceRuleEnabler whose state changes are visible.
 */

public abstract class AbstractImmutableSequenceRuleEnabler
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidEnabler
    implements org.osid.assessment.authoring.SequenceRuleEnabler {

    private final org.osid.assessment.authoring.SequenceRuleEnabler sequenceRuleEnabler;


    /**
     *  Constructs a new <code>AbstractImmutableSequenceRuleEnabler</code>.
     *
     *  @param sequenceRuleEnabler the sequence rule enabler to immutablize
     *  @throws org.osid.NullArgumentException <code>sequenceRuleEnabler</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableSequenceRuleEnabler(org.osid.assessment.authoring.SequenceRuleEnabler sequenceRuleEnabler) {
        super(sequenceRuleEnabler);
        this.sequenceRuleEnabler = sequenceRuleEnabler;
        return;
    }


    /**
     *  Gets the sequence rule enabler record corresponding to the given 
     *  <code> SequenceRuleEnabler </code> record <code> Type. </code> This 
     *  method is used to retrieve an object implementing the requested 
     *  record. The <code> sequenceRuleEnablerRecordType </code> may be the 
     *  <code> Type </code> returned in <code> getRecordTypes() </code> or any 
     *  of its parents in a <code> Type </code> hierarchy where <code> 
     *  hasRecordType(sequenceRuleEnablerRecordType) </code> is <code> true 
     *  </code> . 
     *
     *  @param  sequenceRuleEnablerRecordType the type of sequence rule 
     *          enabler record to retrieve 
     *  @return the sequence rule enabler record 
     *  @throws org.osid.NullArgumentException <code> 
     *          sequenceRuleEnablerRecordType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(sequenceRuleEnablerRecordType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.records.SequenceRuleEnablerRecord getSequenceRuleEnablerRecord(org.osid.type.Type sequenceRuleEnablerRecordType)
        throws org.osid.OperationFailedException {

        return (this.sequenceRuleEnabler.getSequenceRuleEnablerRecord(sequenceRuleEnablerRecordType));
    }
}

