//
// AbstractBusinessQuery.java
//
//     A template for making a Business Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.business.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for businesses.
 */

public abstract class AbstractBusinessQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.financials.BusinessQuery {

    private final java.util.Collection<org.osid.financials.records.BusinessQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the account <code> Id </code> for this query. 
     *
     *  @param  accountId an account <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> accountId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAccountId(org.osid.id.Id accountId, boolean match) {
        return;
    }


    /**
     *  Clears the account <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAccountIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AccountQuery </code> is available. 
     *
     *  @return <code> true </code> if an account query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAccountQuery() {
        return (false);
    }


    /**
     *  Gets the query for an account. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the account query 
     *  @throws org.osid.UnimplementedException <code> supportsAccountQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountQuery getAccountQuery() {
        throw new org.osid.UnimplementedException("supportsAccountQuery() is false");
    }


    /**
     *  Matches businesses that have any account. 
     *
     *  @param  match <code> true </code> to match businesses with any 
     *          account, <code> false </code> to match businesses with no 
     *          accounts 
     */

    @OSID @Override
    public void matchAnyAccount(boolean match) {
        return;
    }


    /**
     *  Clears the account query terms. 
     */

    @OSID @Override
    public void clearAccountTerms() {
        return;
    }


    /**
     *  Sets the activity <code> Id </code> for this query. 
     *
     *  @param  activityId an activity <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> activityId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchActivityId(org.osid.id.Id activityId, boolean match) {
        return;
    }


    /**
     *  Clears the activity <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearActivityIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> ActivityQuery </code> is available. 
     *
     *  @return <code> true </code> if an activity query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityQuery() {
        return (false);
    }


    /**
     *  Gets the query for an activity. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the activity query 
     *  @throws org.osid.UnimplementedException <code> supportsActivityQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityQuery getActivityQuery() {
        throw new org.osid.UnimplementedException("supportsActivityQuery() is false");
    }


    /**
     *  Matches businesses that have any activity. 
     *
     *  @param  match <code> true </code> to match businesses with any 
     *          activity, <code> false </code> to match businesses with no 
     *          activities 
     */

    @OSID @Override
    public void matchAnyActivity(boolean match) {
        return;
    }


    /**
     *  Clears the activity query terms. 
     */

    @OSID @Override
    public void clearActivityTerms() {
        return;
    }


    /**
     *  Sets the fiscal period <code> Id </code> for this query. 
     *
     *  @param  fiscalPeriodId a fiscal period <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> fiscalPeriodId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchFiscalPeriodId(org.osid.id.Id fiscalPeriodId, 
                                    boolean match) {
        return;
    }


    /**
     *  Clears the fiscal period <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearFiscalPeriodIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> FiscalPeriodQuery </code> is available. 
     *
     *  @return <code> true </code> if a fiscal period query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFiscalPeriodQuery() {
        return (false);
    }


    /**
     *  Gets the query for a fiscal period. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the fiscal period query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodQuery getFiscalPeriodQuery() {
        throw new org.osid.UnimplementedException("supportsFiscalPeriodQuery() is false");
    }


    /**
     *  Matches businesses that have any fiscal period. 
     *
     *  @param  match <code> true </code> to match businesses with any fiscal 
     *          period, <code> false </code> to match businesses with no 
     *          fiscal period 
     */

    @OSID @Override
    public void matchAnyFiscalPeriod(boolean match) {
        return;
    }


    /**
     *  Clears the fiscal period query terms. 
     */

    @OSID @Override
    public void clearFiscalPeriodTerms() {
        return;
    }


    /**
     *  Sets the business <code> Id </code> for this query to match businesses 
     *  that have the specified business as an ancestor. 
     *
     *  @param  businessId a business <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorBusinessId(org.osid.id.Id businessId, 
                                        boolean match) {
        return;
    }


    /**
     *  Clears the ancestor business <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorBusinessIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BusinessQuery </code> is available. 
     *
     *  @return <code> true </code> if a business query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorBusinessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a business. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the business query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorBusinessQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.BusinessQuery getAncestorBusinessQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorBusinessQuery() is false");
    }


    /**
     *  Matches businesses with any business ancestor. 
     *
     *  @param  match <code> true </code> to match businesses with any 
     *          ancestor, <code> false </code> to match root businesses 
     */

    @OSID @Override
    public void matchAnyAncestorBusiness(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor business query terms. 
     */

    @OSID @Override
    public void clearAncestorBusinessTerms() {
        return;
    }


    /**
     *  Sets the business <code> Id </code> for this query to match businesses 
     *  that have the specified business as an descendant. 
     *
     *  @param  businessId a business <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantBusinessId(org.osid.id.Id businessId, 
                                          boolean match) {
        return;
    }


    /**
     *  Clears the descendant business <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantBusinessIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BusinessQuery </code> is available. 
     *
     *  @return <code> true </code> if a business query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantBusinessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a business. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the business query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantBusinessQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.BusinessQuery getDescendantBusinessQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantBusinessQuery() is false");
    }


    /**
     *  Matches agencies with any descendant business. 
     *
     *  @param  match <code> true </code> to match businesses with any 
     *          descendant, <code> false </code> to match leaf businesses 
     */

    @OSID @Override
    public void matchAnyDescendantBusiness(boolean match) {
        return;
    }


    /**
     *  Clears the descendant business query terms. 
     */

    @OSID @Override
    public void clearDescendantBusinessTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given business query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a business implementing the requested record.
     *
     *  @param businessRecordType a business record type
     *  @return the business query record
     *  @throws org.osid.NullArgumentException
     *          <code>businessRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(businessRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.records.BusinessQueryRecord getBusinessQueryRecord(org.osid.type.Type businessRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.records.BusinessQueryRecord record : this.records) {
            if (record.implementsRecordType(businessRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(businessRecordType + " is not supported");
    }


    /**
     *  Adds a record to this business query. 
     *
     *  @param businessQueryRecord business query record
     *  @param businessRecordType business record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBusinessQueryRecord(org.osid.financials.records.BusinessQueryRecord businessQueryRecord, 
                                          org.osid.type.Type businessRecordType) {

        addRecordType(businessRecordType);
        nullarg(businessQueryRecord, "business query record");
        this.records.add(businessQueryRecord);        
        return;
    }
}
