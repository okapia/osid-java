//
// AbstractAssetContent.java
//
//     Defines an AssetContent builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.repository.assetcontent.spi;


/**
 *  Defines an <code>AssetContent</code> builder.
 */

public abstract class AbstractAssetContentBuilder<T extends AbstractAssetContentBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.repository.assetcontent.AssetContentMiter assetContent;


    /**
     *  Constructs a new <code>AbstractAssetContentBuilder</code>.
     *
     *  @param assetContent the asset content to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractAssetContentBuilder(net.okapia.osid.jamocha.builder.repository.assetcontent.AssetContentMiter assetContent) {
        super(assetContent);
        this.assetContent = assetContent;
        return;
    }


    /**
     *  Builds the asset content.
     *
     *  @return the new asset content
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.repository.AssetContent build() {
        (new net.okapia.osid.jamocha.builder.validator.repository.assetcontent.AssetContentValidator(getValidations())).validate(this.assetContent);
        return (new net.okapia.osid.jamocha.builder.repository.assetcontent.ImmutableAssetContent(this.assetContent));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the asset content miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.repository.assetcontent.AssetContentMiter getMiter() {
        return (this.assetContent);
    }


    /**
     *  Sets the asset.
     *
     *  @param asset an asset
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>asset</code> is
     *          <code>null</code>
     */

    public T asset(org.osid.repository.Asset asset) {
        getMiter().setAsset(asset);
        return (self());
    }


    /**
     *  Adds an accessibility type.
     *
     *  @param accessibilityType an accessibility type
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>accessibilityType</code> is <code>null</code>
     */

    public T accessibilityType(org.osid.type.Type accessibilityType) {
        getMiter().addAccessibilityType(accessibilityType);
        return (self());
    }


    /**
     *  Sets all the accessibility types.
     *
     *  @param accessibilityTypes a collection of accessibility types
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>accessibilityTypes</code> is <code>null</code>
     */

    public T accessibilityTypes(java.util.Collection<org.osid.type.Type> accessibilityTypes) {
        getMiter().setAccessibilityTypes(accessibilityTypes);
        return (self());
    }


    /**
     *  Sets the data length. This length overrides the length of the
     *  data buffer.
     *
     *  @param length the data length
     *  @return the builder
     *  @throws org.osid.InvalidArgumentException <code>length</code>
     *          is negative
     */

    public T length(long length) {
        getMiter().setDataLength(length);
        return (self());
    }


    /**
     *  Sets the asset content data. 
     *
     *  @param data the flipped data buffer
     *  @throws org.osid.NullArgumentException <code>data</code> is
     *          <code>null</code>
     */

    public T data(java.nio.ByteBuffer data) {
        getMiter().setData(data);
        return(self());
    }


    /**
     *  Sets the url.
     *
     *  @param url a url
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>url</code> is
     *          <code>null</code>
     */

    public T url(String url) {
        getMiter().setURL(url);
        return (self());
    }


    /**
     *  Adds an AssetContent record.
     *
     *  @param record an asset content record
     *  @param recordType the type of asset content record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.repository.records.AssetContentRecord record, org.osid.type.Type recordType) {
        getMiter().addAssetContentRecord(record, recordType);
        return (self());
    }
}       


