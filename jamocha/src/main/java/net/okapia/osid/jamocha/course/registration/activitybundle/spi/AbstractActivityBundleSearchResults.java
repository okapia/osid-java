//
// AbstractActivityBundleSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.registration.activitybundle.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractActivityBundleSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.course.registration.ActivityBundleSearchResults {

    private org.osid.course.registration.ActivityBundleList activityBundles;
    private final org.osid.course.registration.ActivityBundleQueryInspector inspector;
    private final java.util.Collection<org.osid.course.registration.records.ActivityBundleSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractActivityBundleSearchResults.
     *
     *  @param activityBundles the result set
     *  @param activityBundleQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>activityBundles</code>
     *          or <code>activityBundleQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractActivityBundleSearchResults(org.osid.course.registration.ActivityBundleList activityBundles,
                                            org.osid.course.registration.ActivityBundleQueryInspector activityBundleQueryInspector) {
        nullarg(activityBundles, "activity bundles");
        nullarg(activityBundleQueryInspector, "activity bundle query inspectpr");

        this.activityBundles = activityBundles;
        this.inspector = activityBundleQueryInspector;

        return;
    }


    /**
     *  Gets the activity bundle list resulting from a search.
     *
     *  @return an activity bundle list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleList getActivityBundles() {
        if (this.activityBundles == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.course.registration.ActivityBundleList activityBundles = this.activityBundles;
        this.activityBundles = null;
	return (activityBundles);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.course.registration.ActivityBundleQueryInspector getActivityBundleQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  activity bundle search record <code> Type. </code> This method must
     *  be used to retrieve an activityBundle implementing the requested
     *  record.
     *
     *  @param activityBundleSearchRecordType an activityBundle search 
     *         record type 
     *  @return the activity bundle search
     *  @throws org.osid.NullArgumentException
     *          <code>activityBundleSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(activityBundleSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.registration.records.ActivityBundleSearchResultsRecord getActivityBundleSearchResultsRecord(org.osid.type.Type activityBundleSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.course.registration.records.ActivityBundleSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(activityBundleSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(activityBundleSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record activity bundle search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addActivityBundleRecord(org.osid.course.registration.records.ActivityBundleSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "activity bundle record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
