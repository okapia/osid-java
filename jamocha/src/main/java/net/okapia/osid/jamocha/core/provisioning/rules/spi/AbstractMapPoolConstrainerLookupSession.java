//
// AbstractMapPoolConstrainerLookupSession
//
//    A simple framework for providing a PoolConstrainer lookup service
//    backed by a fixed collection of pool constrainers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a PoolConstrainer lookup service backed by a
 *  fixed collection of pool constrainers. The pool constrainers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>PoolConstrainers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapPoolConstrainerLookupSession
    extends net.okapia.osid.jamocha.provisioning.rules.spi.AbstractPoolConstrainerLookupSession
    implements org.osid.provisioning.rules.PoolConstrainerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.provisioning.rules.PoolConstrainer> poolConstrainers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.provisioning.rules.PoolConstrainer>());


    /**
     *  Makes a <code>PoolConstrainer</code> available in this session.
     *
     *  @param  poolConstrainer a pool constrainer
     *  @throws org.osid.NullArgumentException <code>poolConstrainer<code>
     *          is <code>null</code>
     */

    protected void putPoolConstrainer(org.osid.provisioning.rules.PoolConstrainer poolConstrainer) {
        this.poolConstrainers.put(poolConstrainer.getId(), poolConstrainer);
        return;
    }


    /**
     *  Makes an array of pool constrainers available in this session.
     *
     *  @param  poolConstrainers an array of pool constrainers
     *  @throws org.osid.NullArgumentException <code>poolConstrainers<code>
     *          is <code>null</code>
     */

    protected void putPoolConstrainers(org.osid.provisioning.rules.PoolConstrainer[] poolConstrainers) {
        putPoolConstrainers(java.util.Arrays.asList(poolConstrainers));
        return;
    }


    /**
     *  Makes a collection of pool constrainers available in this session.
     *
     *  @param  poolConstrainers a collection of pool constrainers
     *  @throws org.osid.NullArgumentException <code>poolConstrainers<code>
     *          is <code>null</code>
     */

    protected void putPoolConstrainers(java.util.Collection<? extends org.osid.provisioning.rules.PoolConstrainer> poolConstrainers) {
        for (org.osid.provisioning.rules.PoolConstrainer poolConstrainer : poolConstrainers) {
            this.poolConstrainers.put(poolConstrainer.getId(), poolConstrainer);
        }

        return;
    }


    /**
     *  Removes a PoolConstrainer from this session.
     *
     *  @param  poolConstrainerId the <code>Id</code> of the pool constrainer
     *  @throws org.osid.NullArgumentException <code>poolConstrainerId<code> is
     *          <code>null</code>
     */

    protected void removePoolConstrainer(org.osid.id.Id poolConstrainerId) {
        this.poolConstrainers.remove(poolConstrainerId);
        return;
    }


    /**
     *  Gets the <code>PoolConstrainer</code> specified by its <code>Id</code>.
     *
     *  @param  poolConstrainerId <code>Id</code> of the <code>PoolConstrainer</code>
     *  @return the poolConstrainer
     *  @throws org.osid.NotFoundException <code>poolConstrainerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>poolConstrainerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainer getPoolConstrainer(org.osid.id.Id poolConstrainerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.provisioning.rules.PoolConstrainer poolConstrainer = this.poolConstrainers.get(poolConstrainerId);
        if (poolConstrainer == null) {
            throw new org.osid.NotFoundException("poolConstrainer not found: " + poolConstrainerId);
        }

        return (poolConstrainer);
    }


    /**
     *  Gets all <code>PoolConstrainers</code>. In plenary mode, the returned
     *  list contains all known poolConstrainers or an error
     *  results. Otherwise, the returned list may contain only those
     *  poolConstrainers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>PoolConstrainers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerList getPoolConstrainers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.rules.poolconstrainer.ArrayPoolConstrainerList(this.poolConstrainers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.poolConstrainers.clear();
        super.close();
        return;
    }
}
