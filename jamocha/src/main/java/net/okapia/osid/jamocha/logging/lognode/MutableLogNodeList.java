//
// MutableLogNodeList.java
//
//     Implements a LogNodeList. This list allows LogNodes to be
//     added after this list has been created.
//
//
// Tom Coppeto
// OnTapSolutions
// 29 June 2008
//
//
// Copyright (c) 2008, 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.logging.lognode;


/**
 *  <p>Implements a LogNodeList. This list allows LogNodes to be
 *  added after this logNode has been created. One this list has been
 *  returned to the consumer, all subsequent additions occur in a
 *  separate processing thread.  The creator of this logNode must
 *  invoke <code>eol()</code> when there are no more logNodes to be
 *  added.</p>
 *
 *  <p> If the consumer of the <code>LogNodeList</code> interface
 *  reaches the end of the internal buffer before <code>eol()</code>,
 *  then methods will block until more logNodes are added or
 *  <code>eol()</code> is invoked.</p>
 *
 *  <p><code>available()</code> never blocks but may return
 *  <code>0</code> if waiting for more logNodes to be added.</p>
 */

public final class MutableLogNodeList
    extends net.okapia.osid.jamocha.logging.lognode.spi.AbstractMutableLogNodeList
    implements org.osid.logging.LogNodeList {


    /**
     *  Creates a new empty <code>MutableLogNodeList</code>.
     */

    public MutableLogNodeList() {
        super();
    }


    /**
     *  Creates a new <code>MutableLogNodeList</code>.
     *
     *  @param logNode a <code>LogNode</code>
     *  @throws org.osid.NullArgumentException <code>logNode</code>
     *          is <code>null</code>
     */

    public MutableLogNodeList(org.osid.logging.LogNode logNode) {
        super(logNode);
        return;
    }


    /**
     *  Creates a new <code>MutableLogNodeList</code>.
     *
     *  @param array an array of lognodes
     *  @throws org.osid.NullArgumentException <code>array</code>
     *          is <code>null</code>
     */

    public MutableLogNodeList(org.osid.logging.LogNode[] array) {
        super(array);
        return;
    }


    /**
     *  Creates a new <code>MutableLogNodeList</code>.
     *
     *  @param collection a java.util.Collection of lognodes
     *  @throws org.osid.NullArgumentException <code>collection</code>
     *          is <code>null</code>
     */

    public MutableLogNodeList(java.util.Collection<org.osid.logging.LogNode> collection) {
        super(collection);
        return;
    }
}
