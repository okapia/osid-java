//
// AbstractIndexedMapQueueLookupSession.java
//
//    A simple framework for providing a Queue lookup service
//    backed by a fixed collection of queues with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Queue lookup service backed by a
 *  fixed collection of queues. The queues are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some queues may be compatible
 *  with more types than are indicated through these queue
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Queues</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapQueueLookupSession
    extends AbstractMapQueueLookupSession
    implements org.osid.provisioning.QueueLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.provisioning.Queue> queuesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.provisioning.Queue>());
    private final MultiMap<org.osid.type.Type, org.osid.provisioning.Queue> queuesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.provisioning.Queue>());


    /**
     *  Makes a <code>Queue</code> available in this session.
     *
     *  @param  queue a queue
     *  @throws org.osid.NullArgumentException <code>queue<code> is
     *          <code>null</code>
     */

    @Override
    protected void putQueue(org.osid.provisioning.Queue queue) {
        super.putQueue(queue);

        this.queuesByGenus.put(queue.getGenusType(), queue);
        
        try (org.osid.type.TypeList types = queue.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.queuesByRecord.put(types.getNextType(), queue);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a queue from this session.
     *
     *  @param queueId the <code>Id</code> of the queue
     *  @throws org.osid.NullArgumentException <code>queueId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeQueue(org.osid.id.Id queueId) {
        org.osid.provisioning.Queue queue;
        try {
            queue = getQueue(queueId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.queuesByGenus.remove(queue.getGenusType());

        try (org.osid.type.TypeList types = queue.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.queuesByRecord.remove(types.getNextType(), queue);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeQueue(queueId);
        return;
    }


    /**
     *  Gets a <code>QueueList</code> corresponding to the given
     *  queue genus <code>Type</code> which does not include
     *  queues of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known queues or an error results. Otherwise,
     *  the returned list may contain only those queues that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  queueGenusType a queue genus type 
     *  @return the returned <code>Queue</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>queueGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.QueueList getQueuesByGenusType(org.osid.type.Type queueGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.queue.ArrayQueueList(this.queuesByGenus.get(queueGenusType)));
    }


    /**
     *  Gets a <code>QueueList</code> containing the given
     *  queue record <code>Type</code>. In plenary mode, the
     *  returned list contains all known queues or an error
     *  results. Otherwise, the returned list may contain only those
     *  queues that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  queueRecordType a queue record type 
     *  @return the returned <code>queue</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>queueRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.QueueList getQueuesByRecordType(org.osid.type.Type queueRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.queue.ArrayQueueList(this.queuesByRecord.get(queueRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.queuesByGenus.clear();
        this.queuesByRecord.clear();

        super.close();

        return;
    }
}
