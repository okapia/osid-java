//
// AbstractFederatingQualifierLookupSession.java
//
//     An abstract federating adapter for a QualifierLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.authorization.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  QualifierLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingQualifierLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.authorization.QualifierLookupSession>
    implements org.osid.authorization.QualifierLookupSession {

    private boolean parallel = false;
    private org.osid.authorization.Vault vault = new net.okapia.osid.jamocha.nil.authorization.vault.UnknownVault();


    /**
     *  Constructs a new <code>AbstractFederatingQualifierLookupSession</code>.
     */

    protected AbstractFederatingQualifierLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.authorization.QualifierLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Vault/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Vault Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getVaultId() {
        return (this.vault.getId());
    }


    /**
     *  Gets the <code>Vault</code> associated with this 
     *  session.
     *
     *  @return the <code>Vault</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.Vault getVault()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.vault);
    }


    /**
     *  Sets the <code>Vault</code>.
     *
     *  @param  vault the vault for this session
     *  @throws org.osid.NullArgumentException <code>vault</code>
     *          is <code>null</code>
     */

    protected void setVault(org.osid.authorization.Vault vault) {
        nullarg(vault, "vault");
        this.vault = vault;
        return;
    }


    /**
     *  Tests if this user can perform <code>Qualifier</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupQualifiers() {
        for (org.osid.authorization.QualifierLookupSession session : getSessions()) {
            if (session.canLookupQualifiers()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Qualifier</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeQualifierView() {
        for (org.osid.authorization.QualifierLookupSession session : getSessions()) {
            session.useComparativeQualifierView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Qualifier</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryQualifierView() {
        for (org.osid.authorization.QualifierLookupSession session : getSessions()) {
            session.usePlenaryQualifierView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include qualifiers in vaults which are children
     *  of this vault in the vault hierarchy.
     */

    @OSID @Override
    public void useFederatedVaultView() {
        for (org.osid.authorization.QualifierLookupSession session : getSessions()) {
            session.useFederatedVaultView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this vault only.
     */

    @OSID @Override
    public void useIsolatedVaultView() {
        for (org.osid.authorization.QualifierLookupSession session : getSessions()) {
            session.useIsolatedVaultView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Qualifier</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Qualifier</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Qualifier</code> and
     *  retained for compatibility.
     *
     *  @param  qualifierId <code>Id</code> of the
     *          <code>Qualifier</code>
     *  @return the qualifier
     *  @throws org.osid.NotFoundException <code>qualifierId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>qualifierId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.Qualifier getQualifier(org.osid.id.Id qualifierId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.authorization.QualifierLookupSession session : getSessions()) {
            try {
                return (session.getQualifier(qualifierId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(qualifierId + " not found");
    }


    /**
     *  Gets a <code>QualifierList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  qualifiers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Qualifiers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  qualifierIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Qualifier</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>qualifierIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.QualifierList getQualifiersByIds(org.osid.id.IdList qualifierIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.authorization.qualifier.MutableQualifierList ret = new net.okapia.osid.jamocha.authorization.qualifier.MutableQualifierList();

        try (org.osid.id.IdList ids = qualifierIds) {
            while (ids.hasNext()) {
                ret.addQualifier(getQualifier(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>QualifierList</code> corresponding to the given
     *  qualifier genus <code>Type</code> which does not include
     *  qualifiers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  qualifiers or an error results. Otherwise, the returned list
     *  may contain only those qualifiers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  qualifierGenusType a qualifier genus type 
     *  @return the returned <code>Qualifier</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>qualifierGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.QualifierList getQualifiersByGenusType(org.osid.type.Type qualifierGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authorization.qualifier.FederatingQualifierList ret = getQualifierList();

        for (org.osid.authorization.QualifierLookupSession session : getSessions()) {
            ret.addQualifierList(session.getQualifiersByGenusType(qualifierGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>QualifierList</code> corresponding to the given
     *  qualifier genus <code>Type</code> and include any additional
     *  qualifiers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  qualifiers or an error results. Otherwise, the returned list
     *  may contain only those qualifiers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  qualifierGenusType a qualifier genus type 
     *  @return the returned <code>Qualifier</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>qualifierGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.QualifierList getQualifiersByParentGenusType(org.osid.type.Type qualifierGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authorization.qualifier.FederatingQualifierList ret = getQualifierList();

        for (org.osid.authorization.QualifierLookupSession session : getSessions()) {
            ret.addQualifierList(session.getQualifiersByParentGenusType(qualifierGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>QualifierList</code> containing the given
     *  qualifier record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  qualifiers or an error results. Otherwise, the returned list
     *  may contain only those qualifiers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  qualifierRecordType a qualifier record type 
     *  @return the returned <code>Qualifier</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>qualifierRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.QualifierList getQualifiersByRecordType(org.osid.type.Type qualifierRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authorization.qualifier.FederatingQualifierList ret = getQualifierList();

        for (org.osid.authorization.QualifierLookupSession session : getSessions()) {
            ret.addQualifierList(session.getQualifiersByRecordType(qualifierRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Qualifiers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  qualifiers or an error results. Otherwise, the returned list
     *  may contain only those qualifiers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Qualifiers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.QualifierList getQualifiers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authorization.qualifier.FederatingQualifierList ret = getQualifierList();

        for (org.osid.authorization.QualifierLookupSession session : getSessions()) {
            ret.addQualifierList(session.getQualifiers());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.authorization.qualifier.FederatingQualifierList getQualifierList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.authorization.qualifier.ParallelQualifierList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.authorization.qualifier.CompositeQualifierList());
        }
    }
}
