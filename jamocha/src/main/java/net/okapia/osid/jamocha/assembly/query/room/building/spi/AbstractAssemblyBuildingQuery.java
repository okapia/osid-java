//
// AbstractAssemblyBuildingQuery.java
//
//     A BuildingQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.room.building.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A BuildingQuery that stores terms.
 */

public abstract class AbstractAssemblyBuildingQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyTemporalOsidObjectQuery
    implements org.osid.room.BuildingQuery,
               org.osid.room.BuildingQueryInspector,
               org.osid.room.BuildingSearchOrder {

    private final java.util.Collection<org.osid.room.records.BuildingQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.room.records.BuildingQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.room.records.BuildingSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyBuildingQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyBuildingQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets an address <code> Id. </code> 
     *
     *  @param  addressId an address <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> addressId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAddressId(org.osid.id.Id addressId, boolean match) {
        getAssembler().addIdTerm(getAddressIdColumn(), addressId, match);
        return;
    }


    /**
     *  Clears the address <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAddressIdTerms() {
        getAssembler().clearTerms(getAddressIdColumn());
        return;
    }


    /**
     *  Gets the address <code> Id </code> terms. 
     *
     *  @return the address <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAddressIdTerms() {
        return (getAssembler().getIdTerms(getAddressIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the address. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAddress(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAddressColumn(), style);
        return;
    }


    /**
     *  Gets the AddressId column name.
     *
     * @return the column name
     */

    protected String getAddressIdColumn() {
        return ("address_id");
    }


    /**
     *  Tests if an <code> AddressQuery </code> is available. 
     *
     *  @return <code> true </code> if an address query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAddressQuery() {
        return (false);
    }


    /**
     *  Gets the query for an address query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the address query 
     *  @throws org.osid.UnimplementedException <code> supportsAddressQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressQuery getAddressQuery() {
        throw new org.osid.UnimplementedException("supportsAddressQuery() is false");
    }


    /**
     *  Matches any address. 
     *
     *  @param  match <code> true </code> to match buildings with any address, 
     *          <code> false </code> to match buildings with no address 
     */

    @OSID @Override
    public void matchAnyAddress(boolean match) {
        getAssembler().addIdWildcardTerm(getAddressColumn(), match);
        return;
    }


    /**
     *  Clears the address terms. 
     */

    @OSID @Override
    public void clearAddressTerms() {
        getAssembler().clearTerms(getAddressColumn());
        return;
    }


    /**
     *  Gets the address terms. 
     *
     *  @return the address terms 
     */

    @OSID @Override
    public org.osid.contact.AddressQueryInspector[] getAddressTerms() {
        return (new org.osid.contact.AddressQueryInspector[0]);
    }


    /**
     *  Tests if an address order is available. 
     *
     *  @return <code> true </code> if a address order is available,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean supportsAddressSearchOrder() {
        return (false);
    }


    /**
     *  Gets the address search order. 
     *
     *  @return the address search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAddressSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressSearchOrder getAddressSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAddressSearchOrder() is false");
    }


    /**
     *  Gets the Address column name.
     *
     * @return the column name
     */

    protected String getAddressColumn() {
        return ("address");
    }


    /**
     *  Sets a name. 
     *
     *  @param  name an official name 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> name </code> is not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> name </code> is <code> 
     *          null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchOfficialName(String name, 
                                  org.osid.type.Type stringMatchType, 
                                  boolean match) {
        getAssembler().addStringTerm(getOfficialNameColumn(), name, stringMatchType, match);
        return;
    }


    /**
     *  Matches any official name. 
     *
     *  @param  match <code> true </code> to match buildings with any official 
     *          name, <code> false </code> to match buildings with no official 
     *          name 
     */

    @OSID @Override
    public void matchAnyOfficialName(boolean match) {
        getAssembler().addStringWildcardTerm(getOfficialNameColumn(), match);
        return;
    }


    /**
     *  Clears the official name terms. 
     */

    @OSID @Override
    public void clearOfficialNameTerms() {
        getAssembler().clearTerms(getOfficialNameColumn());
        return;
    }


    /**
     *  Gets the offical name terms. 
     *
     *  @return the name terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getOfficialNameTerms() {
        return (getAssembler().getStringTerms(getOfficialNameColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the offical 
     *  name. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByOfficialName(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getOfficialNameColumn(), style);
        return;
    }


    /**
     *  Gets the OfficialName column name.
     *
     * @return the column name
     */

    protected String getOfficialNameColumn() {
        return ("official_name");
    }


    /**
     *  Sets a bulding number. 
     *
     *  @param  number a number 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> number </code> is not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> number </code> is <code> 
     *          null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchNumber(String number, org.osid.type.Type stringMatchType, 
                            boolean match) {
        getAssembler().addStringTerm(getNumberColumn(), number, stringMatchType, match);
        return;
    }


    /**
     *  Matches any building number. 
     *
     *  @param  match <code> true </code> to match buildings with any number, 
     *          <code> false </code> to match buildings with no number 
     */

    @OSID @Override
    public void matchAnyNumber(boolean match) {
        getAssembler().addStringWildcardTerm(getNumberColumn(), match);
        return;
    }


    /**
     *  Clears the building number terms. 
     */

    @OSID @Override
    public void clearNumberTerms() {
        getAssembler().clearTerms(getNumberColumn());
        return;
    }


    /**
     *  Gets the building number terms. 
     *
     *  @return the number terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getNumberTerms() {
        return (getAssembler().getStringTerms(getNumberColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the building 
     *  number. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByNumber(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getNumberColumn(), style);
        return;
    }


    /**
     *  Gets the Number column name.
     *
     * @return the column name
     */

    protected String getNumberColumn() {
        return ("number");
    }


    /**
     *  Sets a building <code> Id. </code> 
     *
     *  @param  buildingId a building <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> addressId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEnclosingBuildingId(org.osid.id.Id buildingId, 
                                         boolean match) {
        getAssembler().addIdTerm(getEnclosingBuildingIdColumn(), buildingId, match);
        return;
    }


    /**
     *  Clears the building <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearEnclosingBuildingIdTerms() {
        getAssembler().clearTerms(getEnclosingBuildingIdColumn());
        return;
    }


    /**
     *  Gets the enclosing building <code> Id </code> terms. 
     *
     *  @return the building <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEnclosingBuildingIdTerms() {
        return (getAssembler().getIdTerms(getEnclosingBuildingIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the 
     *  subdivisions. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByEnclosingBuilding(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getEnclosingBuildingColumn(), style);
        return;
    }


    /**
     *  Gets the EnclosingBuildingId column name.
     *
     * @return the column name
     */

    protected String getEnclosingBuildingIdColumn() {
        return ("enclosing_building_id");
    }


    /**
     *  Tests if a <code> BuildingQuery </code> is available. 
     *
     *  @return <code> true </code> if a building query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEnclosingBuildingQuery() {
        return (false);
    }


    /**
     *  Gets the query for a building query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the building query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnclosingBuildingQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingQuery getEnclosingBuildingQuery() {
        throw new org.osid.UnimplementedException("supportsEnclosingBuildingQuery() is false");
    }


    /**
     *  Matches any building. 
     *
     *  @param  match <code> true </code> to match buildings with any 
     *          enclosing building, <code> false </code> to match buildings 
     *          with no enclosing building 
     */

    @OSID @Override
    public void matchAnyEnclosingBuilding(boolean match) {
        getAssembler().addIdWildcardTerm(getEnclosingBuildingColumn(), match);
        return;
    }


    /**
     *  Clears the enclosing building terms. 
     */

    @OSID @Override
    public void clearEnclosingBuildingTerms() {
        getAssembler().clearTerms(getEnclosingBuildingColumn());
        return;
    }


    /**
     *  Gets the enclosing building terms. 
     *
     *  @return the building terms 
     */

    @OSID @Override
    public org.osid.room.BuildingQueryInspector[] getEnclosingBuildingTerms() {
        return (new org.osid.room.BuildingQueryInspector[0]);
    }


    /**
     *  Tests if an enclosing building search order is available. 
     *
     *  @return <code> true </code> if a building search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEnclosingBuildingSearchOrder() {
        return (false);
    }


    /**
     *  Gets the enclosing building search order. 
     *
     *  @return the building search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnclosingBuildingSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingSearchOrder getEnclosingBuildingSearchOrder() {
        throw new org.osid.UnimplementedException("supportsEnclosingBuildingSearchOrder() is false");
    }


    /**
     *  Gets the EnclosingBuilding column name.
     *
     * @return the column name
     */

    protected String getEnclosingBuildingColumn() {
        return ("enclosing_building");
    }


    /**
     *  Matches an area within the given range inclusive. 
     *
     *  @param  low start of range 
     *  @param  high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchGrossArea(java.math.BigDecimal low, 
                               java.math.BigDecimal high, boolean match) {
        getAssembler().addDecimalRangeTerm(getGrossAreaColumn(), low, high, match);
        return;
    }


    /**
     *  Matches any area. 
     *
     *  @param  match <code> true </code> to match buildings with any area, 
     *          <code> false </code> to match buildings with no area assigned 
     */

    @OSID @Override
    public void matchAnyGrossArea(boolean match) {
        getAssembler().addDecimalRangeWildcardTerm(getGrossAreaColumn(), match);
        return;
    }


    /**
     *  Clears the area terms. 
     */

    @OSID @Override
    public void clearGrossAreaTerms() {
        getAssembler().clearTerms(getGrossAreaColumn());
        return;
    }


    /**
     *  Gets the gross area terms. 
     *
     *  @return the gross area terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getGrossAreaTerms() {
        return (getAssembler().getDecimalRangeTerms(getGrossAreaColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the gross area. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByGrossArea(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getGrossAreaColumn(), style);
        return;
    }


    /**
     *  Gets the GrossArea column name.
     *
     * @return the column name
     */

    protected String getGrossAreaColumn() {
        return ("gross_area");
    }


    /**
     *  Sets the room <code> Id </code> for this query to match rooms assigned 
     *  to buildings. 
     *
     *  @param  roomId a room <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> roomId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchRoomId(org.osid.id.Id roomId, boolean match) {
        getAssembler().addIdTerm(getRoomIdColumn(), roomId, match);
        return;
    }


    /**
     *  Clears the room <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRoomIdTerms() {
        getAssembler().clearTerms(getRoomIdColumn());
        return;
    }


    /**
     *  Gets the room <code> Id </code> terms. 
     *
     *  @return the room <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRoomIdTerms() {
        return (getAssembler().getIdTerms(getRoomIdColumn()));
    }


    /**
     *  Gets the RoomId column name.
     *
     * @return the column name
     */

    protected String getRoomIdColumn() {
        return ("room_id");
    }


    /**
     *  Tests if a room query is available. 
     *
     *  @return <code> true </code> if a room query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRoomQuery() {
        return (false);
    }


    /**
     *  Gets the query for a building. 
     *
     *  @return the room query 
     *  @throws org.osid.UnimplementedException <code> supportsRoomQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomQuery getRoomQuery() {
        throw new org.osid.UnimplementedException("supportsRoomQuery() is false");
    }


    /**
     *  Matches buildings with any room. 
     *
     *  @param  match <code> true </code> to match buildings with any room, 
     *          <code> false </code> to match buildings with no rooms 
     */

    @OSID @Override
    public void matchAnyRoom(boolean match) {
        getAssembler().addIdWildcardTerm(getRoomColumn(), match);
        return;
    }


    /**
     *  Clears the room terms. 
     */

    @OSID @Override
    public void clearRoomTerms() {
        getAssembler().clearTerms(getRoomColumn());
        return;
    }


    /**
     *  Gets the room terms. 
     *
     *  @return the room terms 
     */

    @OSID @Override
    public org.osid.room.RoomQueryInspector[] getRoomTerms() {
        return (new org.osid.room.RoomQueryInspector[0]);
    }


    /**
     *  Gets the Room column name.
     *
     * @return the column name
     */

    protected String getRoomColumn() {
        return ("room");
    }


    /**
     *  Sets the building <code> Id </code> for this query to match rooms 
     *  assigned to campuses. 
     *
     *  @param  campusId a campus <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCampusId(org.osid.id.Id campusId, boolean match) {
        getAssembler().addIdTerm(getCampusIdColumn(), campusId, match);
        return;
    }


    /**
     *  Clears the campus <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCampusIdTerms() {
        getAssembler().clearTerms(getCampusIdColumn());
        return;
    }


    /**
     *  Gets the campus <code> Id </code> terms. 
     *
     *  @return the campus <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCampusIdTerms() {
        return (getAssembler().getIdTerms(getCampusIdColumn()));
    }


    /**
     *  Gets the CampusId column name.
     *
     * @return the column name
     */

    protected String getCampusIdColumn() {
        return ("campus_id");
    }


    /**
     *  Tests if a <code> CampusQuery </code> is available. 
     *
     *  @return <code> true </code> if a campus query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCampusQuery() {
        return (false);
    }


    /**
     *  Gets the query for a campus query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the campus query 
     *  @throws org.osid.UnimplementedException <code> supportsCampusQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.CampusQuery getCampusQuery() {
        throw new org.osid.UnimplementedException("supportsCampusQuery() is false");
    }


    /**
     *  Clears the campus terms. 
     */

    @OSID @Override
    public void clearCampusTerms() {
        getAssembler().clearTerms(getCampusColumn());
        return;
    }


    /**
     *  Gets the campus terms. 
     *
     *  @return the campus terms 
     */

    @OSID @Override
    public org.osid.room.CampusQueryInspector[] getCampusTerms() {
        return (new org.osid.room.CampusQueryInspector[0]);
    }


    /**
     *  Gets the Campus column name.
     *
     * @return the column name
     */

    protected String getCampusColumn() {
        return ("campus");
    }


    /**
     *  Tests if this building supports the given record
     *  <code>Type</code>.
     *
     *  @param  buildingRecordType a building record type 
     *  @return <code>true</code> if the buildingRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>buildingRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type buildingRecordType) {
        for (org.osid.room.records.BuildingQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(buildingRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  buildingRecordType the building record type 
     *  @return the building query record 
     *  @throws org.osid.NullArgumentException
     *          <code>buildingRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(buildingRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.room.records.BuildingQueryRecord getBuildingQueryRecord(org.osid.type.Type buildingRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.records.BuildingQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(buildingRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(buildingRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  buildingRecordType the building record type 
     *  @return the building query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>buildingRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(buildingRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.room.records.BuildingQueryInspectorRecord getBuildingQueryInspectorRecord(org.osid.type.Type buildingRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.records.BuildingQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(buildingRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(buildingRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param buildingRecordType the building record type
     *  @return the building search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>buildingRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(buildingRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.room.records.BuildingSearchOrderRecord getBuildingSearchOrderRecord(org.osid.type.Type buildingRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.records.BuildingSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(buildingRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(buildingRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this building. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param buildingQueryRecord the building query record
     *  @param buildingQueryInspectorRecord the building query inspector
     *         record
     *  @param buildingSearchOrderRecord the building search order record
     *  @param buildingRecordType building record type
     *  @throws org.osid.NullArgumentException
     *          <code>buildingQueryRecord</code>,
     *          <code>buildingQueryInspectorRecord</code>,
     *          <code>buildingSearchOrderRecord</code> or
     *          <code>buildingRecordTypebuilding</code> is
     *          <code>null</code>
     */
            
    protected void addBuildingRecords(org.osid.room.records.BuildingQueryRecord buildingQueryRecord, 
                                      org.osid.room.records.BuildingQueryInspectorRecord buildingQueryInspectorRecord, 
                                      org.osid.room.records.BuildingSearchOrderRecord buildingSearchOrderRecord, 
                                      org.osid.type.Type buildingRecordType) {

        addRecordType(buildingRecordType);

        nullarg(buildingQueryRecord, "building query record");
        nullarg(buildingQueryInspectorRecord, "building query inspector record");
        nullarg(buildingSearchOrderRecord, "building search odrer record");

        this.queryRecords.add(buildingQueryRecord);
        this.queryInspectorRecords.add(buildingQueryInspectorRecord);
        this.searchOrderRecords.add(buildingSearchOrderRecord);
        
        return;
    }
}
