//
// AbstractInstallationSearch.java
//
//     A template for making an Installation Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.installation.installation.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing installation searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractInstallationSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.installation.InstallationSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.installation.records.InstallationSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.installation.InstallationSearchOrder installationSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of installations. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  installationIds list of installations
     *  @throws org.osid.NullArgumentException
     *          <code>installationIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongInstallations(org.osid.id.IdList installationIds) {
        while (installationIds.hasNext()) {
            try {
                this.ids.add(installationIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongInstallations</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of installation Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getInstallationIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  installationSearchOrder installation search order 
     *  @throws org.osid.NullArgumentException
     *          <code>installationSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>installationSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderInstallationResults(org.osid.installation.InstallationSearchOrder installationSearchOrder) {
	this.installationSearchOrder = installationSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.installation.InstallationSearchOrder getInstallationSearchOrder() {
	return (this.installationSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given installation search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an installation implementing the requested record.
     *
     *  @param installationSearchRecordType an installation search record
     *         type
     *  @return the installation search record
     *  @throws org.osid.NullArgumentException
     *          <code>installationSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(installationSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.installation.records.InstallationSearchRecord getInstallationSearchRecord(org.osid.type.Type installationSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.installation.records.InstallationSearchRecord record : this.records) {
            if (record.implementsRecordType(installationSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(installationSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this installation search. 
     *
     *  @param installationSearchRecord installation search record
     *  @param installationSearchRecordType installation search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addInstallationSearchRecord(org.osid.installation.records.InstallationSearchRecord installationSearchRecord, 
                                           org.osid.type.Type installationSearchRecordType) {

        addRecordType(installationSearchRecordType);
        this.records.add(installationSearchRecord);        
        return;
    }
}
