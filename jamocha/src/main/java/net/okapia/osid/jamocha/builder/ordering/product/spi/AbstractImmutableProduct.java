//
// AbstractImmutableProduct.java
//
//     Wraps a mutable Product to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.ordering.product.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Product</code> to hide modifiers. This
 *  wrapper provides an immutized Product from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying product whose state changes are visible.
 */

public abstract class AbstractImmutableProduct
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.ordering.Product {

    private final org.osid.ordering.Product product;


    /**
     *  Constructs a new <code>AbstractImmutableProduct</code>.
     *
     *  @param product the product to immutablize
     *  @throws org.osid.NullArgumentException <code>product</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableProduct(org.osid.ordering.Product product) {
        super(product);
        this.product = product;
        return;
    }


    /**
     *  Gets the product code. 
     *
     *  @return a product code 
     */

    @OSID @Override
    public String getCode() {
        return (this.product.getCode());
    }


    /**
     *  Gets the price schedule <code> Ids. </code> 
     *
     *  @return a price schedule <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getPriceScheduleIds() {
        return (this.product.getPriceScheduleIds());
    }


    /**
     *  Gets the price schedules. 
     *
     *  @return the price schedules 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleList getPriceSchedules()
        throws org.osid.OperationFailedException {

        return (this.product.getPriceSchedules());
    }


    /**
     *  Tests if an availability is available for this product. 
     *
     *  @return <code> true </code> if an available is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasAvailability() {
        return (this.product.hasAvailability());
    }


    /**
     *  Gets the availability. 
     *
     *  @return the availability 
     *  @throws org.osid.IllegalStateException <code> hasAvailability() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public long getAvailability() {
        return (this.product.getAvailability());
    }


    /**
     *  Gets the product record corresponding to the given <code> Product 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> productRecordType 
     *  </code> may be the <code> Type </code> returned in <code> 
     *  getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(productRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  productRecordType the type of product record to retrieve 
     *  @return the product record 
     *  @throws org.osid.NullArgumentException <code> productRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(productRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ordering.records.ProductRecord getProductRecord(org.osid.type.Type productRecordType)
        throws org.osid.OperationFailedException {

        return (this.product.getProductRecord(productRecordType));
    }
}

