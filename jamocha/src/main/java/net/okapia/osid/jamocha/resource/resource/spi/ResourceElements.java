//
// ResourceElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resource.resource.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ResourceElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the ResourceElement Id.
     *
     *  @return the resource element Id
     */

    public static org.osid.id.Id getResourceEntityId() {
        return (makeEntityId("osid.resource.Resource"));
    }


    /**
     *  Gets the AvatarId element Id.
     *
     *  @return the AvatarId element Id
     */

    public static org.osid.id.Id getAvatarId() {
        return (makeElementId("osid.resource.resource.AvatarId"));
    }


    /**
     *  Gets the Avatar element Id.
     *
     *  @return the Avatar element Id
     */

    public static org.osid.id.Id getAvatar() {
        return (makeElementId("osid.resource.resource.Avatar"));
    }


    /**
     *  Gets the Group element Id.
     *
     *  @return the Group element Id
     */

    public static org.osid.id.Id getGroup() {
        return (makeElementId("osid.resource.resource.Group"));
    }


    /**
     *  Gets the Demographic element Id.
     *
     *  @return the Demographic element Id
     */

    public static org.osid.id.Id getDemographic() {
        return (makeElementId("osid.resource.resource.Demographic"));
    }


    /**
     *  Gets the ContainingGroupId element Id.
     *
     *  @return the ContainingGroupId element Id
     */

    public static org.osid.id.Id getContainingGroupId() {
        return (makeQueryElementId("osid.resource.resource.ContainingGroupId"));
    }


    /**
     *  Gets the ContainingGroup element Id.
     *
     *  @return the ContainingGroup element Id
     */

    public static org.osid.id.Id getContainingGroup() {
        return (makeQueryElementId("osid.resource.resource.ContainingGroup"));
    }


    /**
     *  Gets the AgentId element Id.
     *
     *  @return the AgentId element Id
     */

    public static org.osid.id.Id getAgentId() {
        return (makeQueryElementId("osid.resource.resource.AgentId"));
    }


    /**
     *  Gets the Agent element Id.
     *
     *  @return the Agent element Id
     */

    public static org.osid.id.Id getAgent() {
        return (makeQueryElementId("osid.resource.resource.Agent"));
    }


    /**
     *  Gets the ResourceRelationshipId element Id.
     *
     *  @return the ResourceRelationshipId element Id
     */

    public static org.osid.id.Id getResourceRelationshipId() {
        return (makeQueryElementId("osid.resource.resource.ResourceRelationshipId"));
    }


    /**
     *  Gets the ResourceRelationship element Id.
     *
     *  @return the ResourceRelationship element Id
     */

    public static org.osid.id.Id getResourceRelationship() {
        return (makeQueryElementId("osid.resource.resource.ResourceRelationship"));
    }


    /**
     *  Gets the BinId element Id.
     *
     *  @return the BinId element Id
     */

    public static org.osid.id.Id getBinId() {
        return (makeQueryElementId("osid.resource.resource.BinId"));
    }


    /**
     *  Gets the Bin element Id.
     *
     *  @return the Bin element Id
     */

    public static org.osid.id.Id getBin() {
        return (makeQueryElementId("osid.resource.resource.Bin"));
    }
}
