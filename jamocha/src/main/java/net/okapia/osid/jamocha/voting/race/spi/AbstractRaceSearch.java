//
// AbstractRaceSearch.java
//
//     A template for making a Race Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.race.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing race searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractRaceSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.voting.RaceSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.voting.records.RaceSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.voting.RaceSearchOrder raceSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of races. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  raceIds list of races
     *  @throws org.osid.NullArgumentException
     *          <code>raceIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongRaces(org.osid.id.IdList raceIds) {
        while (raceIds.hasNext()) {
            try {
                this.ids.add(raceIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongRaces</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of race Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getRaceIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  raceSearchOrder race search order 
     *  @throws org.osid.NullArgumentException
     *          <code>raceSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>raceSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderRaceResults(org.osid.voting.RaceSearchOrder raceSearchOrder) {
	this.raceSearchOrder = raceSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.voting.RaceSearchOrder getRaceSearchOrder() {
	return (this.raceSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given race search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a race implementing the requested record.
     *
     *  @param raceSearchRecordType a race search record
     *         type
     *  @return the race search record
     *  @throws org.osid.NullArgumentException
     *          <code>raceSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(raceSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.records.RaceSearchRecord getRaceSearchRecord(org.osid.type.Type raceSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.voting.records.RaceSearchRecord record : this.records) {
            if (record.implementsRecordType(raceSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(raceSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this race search. 
     *
     *  @param raceSearchRecord race search record
     *  @param raceSearchRecordType race search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRaceSearchRecord(org.osid.voting.records.RaceSearchRecord raceSearchRecord, 
                                           org.osid.type.Type raceSearchRecordType) {

        addRecordType(raceSearchRecordType);
        this.records.add(raceSearchRecord);        
        return;
    }
}
