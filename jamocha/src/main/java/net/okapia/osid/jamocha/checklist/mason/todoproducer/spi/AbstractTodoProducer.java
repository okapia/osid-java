//
// AbstractTodoProducer.java
//
//     Defines a TodoProducer.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.checklist.mason.todoproducer.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>TodoProducer</code>.
 */

public abstract class AbstractTodoProducer
    extends net.okapia.osid.jamocha.spi.AbstractOsidRule
    implements org.osid.checklist.mason.TodoProducer {

    private org.osid.calendaring.cycle.CyclicEvent cyclicEvent;
    private long stockLevel = -1;
    private org.osid.inventory.Stock stock;
    private boolean create = true;

    private final java.util.Collection<org.osid.checklist.mason.records.TodoProducerRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Tests if a todo should be created or destroyed. 
     *
     *  @return <code> true </code> if a creation rule, <code> false
     *          </code> if a destruction rule
     */

    @OSID @Override
    public boolean isCreationRule() {
        return (this.create);
    }


    /**
     *  Sets the creation rule.
     *
     *  @param create <code> true </code> if a creation rule, <code>
     *         false </code> if a destruction rule
     */

    protected void setCreationRule(boolean create) {
        this.create = create;
        return;
    }


    /**
     *  Tests if a todo should be produced based on a time cycle. 
     *
     *  @return <code> true </code> if based on a timecycle, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isBasedOnTimeCycle() {
        return (this.cyclicEvent != null);
    }


    /**
     *  Gets the cyclic event <code> Id. </code> 
     *
     *  @return the cyclic event <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isBasedOnTimeCycle() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCyclicEventId() {
        if (!isBasedOnTimeCycle()) {
            throw new org.osid.IllegalStateException("isBasedOnTimeCycle() is false");
        }

        return (this.cyclicEvent.getId());
    }


    /**
     *  Gets the cyclic event. 
     *
     *  @return the cyclic event 
     *  @throws org.osid.IllegalStateException <code> isBasedOnTimeCycle() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEvent getCyclicEvent()
        throws org.osid.OperationFailedException {

        if (!isBasedOnTimeCycle()) {
            throw new org.osid.IllegalStateException("isBasedOnTimeCycle() is false");
        }

        return (this.cyclicEvent);
    }


    /**
     *  Sets the cyclic event.
     *
     *  @param cyclicEvent a cyclic event
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicEvent</code> is <code>null</code>
     */

    protected void setCyclicEvent(org.osid.calendaring.cycle.CyclicEvent cyclicEvent) {
        nullarg(cyclicEvent, "cyclic event");
        this.cyclicEvent = cyclicEvent;
        return;
    }


    /**
     *  Tests if a todo should be produced based on an item level in a stock. 
     *
     *  @return <code> true </code> if based on a stock, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean isBasedOnStock() {
        return ((this.stockLevel > 0) && (this.stock != null));
    }


    /**
     *  Gets the stock level. 
     *
     *  @return the stock level 
     *  @throws org.osid.IllegalStateException <code> isBasedOnStock()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public long getStockLevel() {
        if (!isBasedOnStock()) {
            throw new org.osid.IllegalStateException("isBasedOnStock() is false");
        }
        
        return (this.stockLevel);
    }


    /**
     *  Sets the stock level.
     *
     *  @param level a stock level
     *  @throws org.osid.InvalidArgumentException <code>level</code>
     *          is negative
     */

    protected void setStockLevel(long level) {
        cardinalarg(level, "stock level");
        this.stockLevel = level;
        return;
    }


    /**
     *  Gets the stock <code> Id. </code> 
     *
     *  @return the stock <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isBasedOnStock() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getStockId() {
        if (!isBasedOnStock()) {
            throw new org.osid.IllegalStateException("isBasedOnStock() is false");
        }

        return (this.stock.getId());
    }


    /**
     *  Gets the stock. 
     *
     *  @return the stock 
     *  @throws org.osid.IllegalStateException <code> isBasedOnStock() </code> 
     *          is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.inventory.Stock getStock()
        throws org.osid.OperationFailedException {

        if (!isBasedOnStock()) {
            throw new org.osid.IllegalStateException("isBasedOnStock() is false");
        }

        return (this.stock);
    }


    /**
     *  Sets the stock.
     *
     *  @param stock a stock
     *  @throws org.osid.NullArgumentException <code>stock</code> is
     *          <code>null</code>
     */

    protected void setStock(org.osid.inventory.Stock stock) {
        nullarg(stock, "stock");
        this.stock = stock;
        return;
    }


    /**
     *  Tests if this todo producer supports the given record
     *  <code>Type</code>.
     *
     *  @param  todoProducerRecordType a todo producer record type 
     *  @return <code>true</code> if the todoProducerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>todoProducerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type todoProducerRecordType) {
        for (org.osid.checklist.mason.records.TodoProducerRecord record : this.records) {
            if (record.implementsRecordType(todoProducerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>TodoProducer</code> record <code>Type</code>.
     *
     *  @param  todoProducerRecordType the todo producer record type 
     *  @return the todo producer record 
     *  @throws org.osid.NullArgumentException
     *          <code>todoProducerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(todoProducerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.checklist.mason.records.TodoProducerRecord getTodoProducerRecord(org.osid.type.Type todoProducerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.checklist.mason.records.TodoProducerRecord record : this.records) {
            if (record.implementsRecordType(todoProducerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(todoProducerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this todo producer. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param todoProducerRecord the todo producer record
     *  @param todoProducerRecordType todo producer record type
     *  @throws org.osid.NullArgumentException
     *          <code>todoProducerRecord</code> or
     *          <code>todoProducerRecordType</code> is
     *          <code>null</code>
     */
            
    protected void addTodoProducerRecord(org.osid.checklist.mason.records.TodoProducerRecord todoProducerRecord, 
                                         org.osid.type.Type todoProducerRecordType) {
        
        nullarg(todoProducerRecord, "todo producer record");
        addRecordType(todoProducerRecordType);
        this.records.add(todoProducerRecord);
        
        return;
    }
}
