//
// AbstractActionGroupQueryInspector.java
//
//     A template for making an ActionGroupQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.actiongroup.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for action groups.
 */

public abstract class AbstractActionGroupQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.control.ActionGroupQueryInspector {

    private final java.util.Collection<org.osid.control.records.ActionGroupQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the action <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActionIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the action query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.ActionQueryInspector[] getActionTerms() {
        return (new org.osid.control.ActionQueryInspector[0]);
    }


    /**
     *  Gets the system <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSystemIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the system query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.SystemQueryInspector[] getSystemTerms() {
        return (new org.osid.control.SystemQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given action group query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an action group implementing the requested record.
     *
     *  @param actionGroupRecordType an action group record type
     *  @return the action group query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>actionGroupRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(actionGroupRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.ActionGroupQueryInspectorRecord getActionGroupQueryInspectorRecord(org.osid.type.Type actionGroupRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.ActionGroupQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(actionGroupRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(actionGroupRecordType + " is not supported");
    }


    /**
     *  Adds a record to this action group query. 
     *
     *  @param actionGroupQueryInspectorRecord action group query inspector
     *         record
     *  @param actionGroupRecordType actionGroup record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addActionGroupQueryInspectorRecord(org.osid.control.records.ActionGroupQueryInspectorRecord actionGroupQueryInspectorRecord, 
                                                   org.osid.type.Type actionGroupRecordType) {

        addRecordType(actionGroupRecordType);
        nullarg(actionGroupRecordType, "action group record type");
        this.records.add(actionGroupQueryInspectorRecord);        
        return;
    }
}
