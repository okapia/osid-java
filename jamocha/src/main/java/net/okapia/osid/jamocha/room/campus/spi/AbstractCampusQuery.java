//
// AbstractCampusQuery.java
//
//     A template for making a Campus Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.campus.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for campuses.
 */

public abstract class AbstractCampusQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.room.CampusQuery {

    private final java.util.Collection<org.osid.room.records.CampusQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the room <code> Id </code> for this query to match rooms assigned 
     *  to campuses. 
     *
     *  @param  roomId a room <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> roomId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchRoomId(org.osid.id.Id roomId, boolean match) {
        return;
    }


    /**
     *  Clears the room <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRoomIdTerms() {
        return;
    }


    /**
     *  Tests if a room query is available. 
     *
     *  @return <code> true </code> if a room query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRoomQuery() {
        return (false);
    }


    /**
     *  Gets the query for a room. 
     *
     *  @return the room query 
     *  @throws org.osid.UnimplementedException <code> supportsRoomQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomQuery getRoomQuery() {
        throw new org.osid.UnimplementedException("supportsRoomQuery() is false");
    }


    /**
     *  Matches campuses with any room. 
     *
     *  @param  match <code> true </code> to match campuses with any room, 
     *          <code> false </code> to match campuses with no rooms 
     */

    @OSID @Override
    public void matchAnyRoom(boolean match) {
        return;
    }


    /**
     *  Clears the room terms. 
     */

    @OSID @Override
    public void clearRoomTerms() {
        return;
    }


    /**
     *  Sets the floor <code> Id </code> for this query to match rooms 
     *  assigned to campuses. 
     *
     *  @param  floorId a floor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> floorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchFloorId(org.osid.id.Id floorId, boolean match) {
        return;
    }


    /**
     *  Clears the floor <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearFloorIdTerms() {
        return;
    }


    /**
     *  Tests if a floor query is available. 
     *
     *  @return <code> true </code> if a room query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFloorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a floor. 
     *
     *  @return the floor query 
     *  @throws org.osid.UnimplementedException <code> supportsFloorQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.FloorQuery getFloorQuery() {
        throw new org.osid.UnimplementedException("supportsFloorQuery() is false");
    }


    /**
     *  Matches campuses with any floor. 
     *
     *  @param  match <code> true </code> to match campuses with any floor, 
     *          <code> false </code> to match campuses with no floors 
     */

    @OSID @Override
    public void matchAnyFloor(boolean match) {
        return;
    }


    /**
     *  Clears the floor terms. 
     */

    @OSID @Override
    public void clearFloorTerms() {
        return;
    }


    /**
     *  Sets the building <code> Id </code> for this query to match rooms 
     *  assigned to buildings. 
     *
     *  @param  buildingId a building <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> buildingId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBuildingId(org.osid.id.Id buildingId, boolean match) {
        return;
    }


    /**
     *  Clears the building <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBuildingIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BuildingQuery </code> is available. 
     *
     *  @return <code> true </code> if a building query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBuildingQuery() {
        return (false);
    }


    /**
     *  Gets the query for a building query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the building query 
     *  @throws org.osid.UnimplementedException <code> supportsBuildingQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingQuery getBuildingQuery() {
        throw new org.osid.UnimplementedException("supportsBuildingQuery() is false");
    }


    /**
     *  Matches campuses with any building. 
     *
     *  @param  match <code> true </code> to match campuses with any building, 
     *          <code> false </code> to match campuses with no buildings 
     */

    @OSID @Override
    public void matchAnyBuilding(boolean match) {
        return;
    }


    /**
     *  Clears the building terms. 
     */

    @OSID @Override
    public void clearBuildingTerms() {
        return;
    }


    /**
     *  Sets the campus <code> Id </code> for this query to match campuses 
     *  that have the specified campus as an ancestor. 
     *
     *  @param  campusId a campus <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorCampusId(org.osid.id.Id campusId, boolean match) {
        return;
    }


    /**
     *  Clears the ancestor campus <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorCampusIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CampusQuery </code> is available. 
     *
     *  @return <code> true </code> if a campus query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorCampusQuery() {
        return (false);
    }


    /**
     *  Gets the query for a campus. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the campus query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorCampusQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.CampusQuery getAncestorCampusQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorCampusQuery() is false");
    }


    /**
     *  Matches campuses with any ancestor. 
     *
     *  @param  match <code> true </code> to match campuses with any ancestor, 
     *          <code> false </code> to match root campuses 
     */

    @OSID @Override
    public void matchAnyAncestorCampus(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor campus terms. 
     */

    @OSID @Override
    public void clearAncestorCampusTerms() {
        return;
    }


    /**
     *  Sets the campus <code> Id </code> for this query to match campuses 
     *  that have the specified campus as a descendant. 
     *
     *  @param  campusId a campus <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantCampusId(org.osid.id.Id campusId, boolean match) {
        return;
    }


    /**
     *  Clears the descendant campus <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantCampusIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CampusQuery </code> is available. 
     *
     *  @return <code> true </code> if a campus query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantCampusQuery() {
        return (false);
    }


    /**
     *  Gets the query for a campus. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the campus query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantCampusQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.room.CampusQuery getDescendantCampusQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantCampusQuery() is false");
    }


    /**
     *  Matches campuses with any descendant. 
     *
     *  @param  match <code> true </code> to match campuses with any 
     *          descendant, <code> false </code> to match leaf campuses 
     */

    @OSID @Override
    public void matchAnyDescendantCampus(boolean match) {
        return;
    }


    /**
     *  Clears the descendant campus terms. 
     */

    @OSID @Override
    public void clearDescendantCampusTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given campus query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a campus implementing the requested record.
     *
     *  @param campusRecordType a campus record type
     *  @return the campus query record
     *  @throws org.osid.NullArgumentException
     *          <code>campusRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(campusRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.room.records.CampusQueryRecord getCampusQueryRecord(org.osid.type.Type campusRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.records.CampusQueryRecord record : this.records) {
            if (record.implementsRecordType(campusRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(campusRecordType + " is not supported");
    }


    /**
     *  Adds a record to this campus query. 
     *
     *  @param campusQueryRecord campus query record
     *  @param campusRecordType campus record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCampusQueryRecord(org.osid.room.records.CampusQueryRecord campusQueryRecord, 
                                          org.osid.type.Type campusRecordType) {

        addRecordType(campusRecordType);
        nullarg(campusQueryRecord, "campus query record");
        this.records.add(campusQueryRecord);        
        return;
    }
}
