//
// AbstractConfiguration.java
//
//     Defines a Configuration.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.configuration.configuration.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Configuration</code>.
 */

public abstract class AbstractConfiguration
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalog
    implements org.osid.configuration.Configuration {

    private boolean registry = false;
    private final java.util.Collection<org.osid.configuration.records.ConfigurationRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Tests if this configuration is a parameter registry. A
     *  parameter registry contains parameter definitions with no
     *  values.
     *
     *  @return <code> true </code> if this is a registry, <code>
     *          false </code> otherwise
     */

    @OSID @Override
    public boolean isRegistry() {
        return (this.registry);
    }


    /**
     *  Sets the registry.
     *
     *  @param registry <code> true </code> if this is a registry,
     *          <code> false </code> otherwise
     */

    protected void setRegistry(boolean registry) {
        this.registry = registry;
        return;
    }


    /**
     *  Tests if this configuration supports the given record
     *  <code>Type</code>.
     *
     *  @param  configurationRecordType a configuration record type 
     *  @return <code>true</code> if the configurationRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>configurationRecordType</code> is
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type configurationRecordType) {
        for (org.osid.configuration.records.ConfigurationRecord record : this.records) {
            if (record.implementsRecordType(configurationRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Configuration</code> record <code>Type</code>.
     *
     *  @param  configurationRecordType the configuration record type 
     *  @return the configuration record 
     *  @throws org.osid.NullArgumentException
     *          <code>configurationRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(configurationRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.configuration.records.ConfigurationRecord getConfigurationRecord(org.osid.type.Type configurationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.configuration.records.ConfigurationRecord record : this.records) {
            if (record.implementsRecordType(configurationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(configurationRecordType + " is not supported");
    }


    /**
     *  Adds a record to this configuration. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param configurationRecord the configuration record
     *  @param configurationRecordType configuration record type
     *  @throws org.osid.NullArgumentException
     *          <code>configurationRecord</code> or
     *          <code>configurationRecordType</code> is
     *          <code>null</code>
     */
            
    protected void addConfigurationRecord(org.osid.configuration.records.ConfigurationRecord configurationRecord, 
                                          org.osid.type.Type configurationRecordType) {

        nullarg(configurationRecord, "configuration record");
        addRecordType(configurationRecordType);
        this.records.add(configurationRecord);
        
        return;
    }
}
