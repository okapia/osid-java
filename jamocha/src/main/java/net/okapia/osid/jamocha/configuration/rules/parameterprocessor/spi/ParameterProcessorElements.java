//
// ParameterProcessorElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.configuration.rules.parameterprocessor.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ParameterProcessorElements
    extends net.okapia.osid.jamocha.spi.OsidProcessorElements {


    /**
     *  Gets the ParameterProcessorElement Id.
     *
     *  @return the parameter processor element Id
     */

    public static org.osid.id.Id getParameterProcessorEntityId() {
        return (makeEntityId("osid.configuration.rules.ParameterProcessor"));
    }


    /**
     *  Gets the RuledParameterId element Id.
     *
     *  @return the RuledParameterId element Id
     */

    public static org.osid.id.Id getRuledParameterId() {
        return (makeQueryElementId("osid.configuration.rules.parameterprocessor.RuledParameterId"));
    }


    /**
     *  Gets the RuledParameter element Id.
     *
     *  @return the RuledParameter element Id
     */

    public static org.osid.id.Id getRuledParameter() {
        return (makeQueryElementId("osid.configuration.rules.parameterprocessor.RuledParameter"));
    }


    /**
     *  Gets the ConfigurationId element Id.
     *
     *  @return the ConfigurationId element Id
     */

    public static org.osid.id.Id getConfigurationId() {
        return (makeQueryElementId("osid.configuration.rules.parameterprocessor.ConfigurationId"));
    }


    /**
     *  Gets the Configuration element Id.
     *
     *  @return the Configuration element Id
     */

    public static org.osid.id.Id getConfiguration() {
        return (makeQueryElementId("osid.configuration.rules.parameterprocessor.Configuration"));
    }
}
