//
// AbstractCatalogEnablerLookupSession.java
//
//    A starter implementation framework for providing a CatalogEnabler
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.cataloging.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a CatalogEnabler
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getCatalogEnablers(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractCatalogEnablerLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.cataloging.rules.CatalogEnablerLookupSession {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean federated     = false;
    private org.osid.cataloging.Catalog catalog = new net.okapia.osid.jamocha.nil.cataloging.catalog.UnknownCatalog();
    

    /**
     *  Gets the <code>Catalog/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Catalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCatalogId() {
        return (this.catalog.getId());
    }


    /**
     *  Gets the <code>Catalog</code> associated with this 
     *  session.
     *
     *  @return the <code>Catalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.Catalog getCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.catalog);
    }


    /**
     *  Sets the <code>Catalog</code>.
     *
     *  @param  catalog the catalog for this session
     *  @throws org.osid.NullArgumentException <code>catalog</code>
     *          is <code>null</code>
     */

    protected void setCatalog(org.osid.cataloging.Catalog catalog) {
        nullarg(catalog, "catalog");
        this.catalog = catalog;
        return;
    }


    /**
     *  Tests if this user can perform <code>CatalogEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCatalogEnablers() {
        return (true);
    }


    /**
     *  A complete view of the <code>CatalogEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCatalogEnablerView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>CatalogEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCatalogEnablerView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include catalog enablers in catalogs which are children
     *  of this catalog in the catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCatalogView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this catalog only.
     */

    @OSID @Override
    public void useIsolatedCatalogView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active catalog enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveCatalogEnablerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive catalog enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusCatalogEnablerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>CatalogEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>CatalogEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>CatalogEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, catalog enablers are returned that are currently
     *  active. In any status mode, active and inactive catalog enablers
     *  are returned.
     *
     *  @param  catalogEnablerId <code>Id</code> of the
     *          <code>CatalogEnabler</code>
     *  @return the catalog enabler
     *  @throws org.osid.NotFoundException <code>catalogEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>catalogEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnabler getCatalogEnabler(org.osid.id.Id catalogEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.cataloging.rules.CatalogEnablerList catalogEnablers = getCatalogEnablers()) {
            while (catalogEnablers.hasNext()) {
                org.osid.cataloging.rules.CatalogEnabler catalogEnabler = catalogEnablers.getNextCatalogEnabler();
                if (catalogEnabler.getId().equals(catalogEnablerId)) {
                    return (catalogEnabler);
                }
            }
        } 

        throw new org.osid.NotFoundException(catalogEnablerId + " not found");
    }


    /**
     *  Gets a <code>CatalogEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  catalogEnablers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>CatalogEnablers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, catalog enablers are returned that are currently
     *  active. In any status mode, active and inactive catalog enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getCatalogEnablers()</code>.
     *
     *  @param  catalogEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>CatalogEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
*               found
     *  @throws org.osid.NullArgumentException
     *          <code>catalogEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerList getCatalogEnablersByIds(org.osid.id.IdList catalogEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.cataloging.rules.CatalogEnabler> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = catalogEnablerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getCatalogEnabler(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("catalog enabler " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.cataloging.rules.catalogenabler.LinkedCatalogEnablerList(ret));
    }


    /**
     *  Gets a <code>CatalogEnablerList</code> corresponding to the given
     *  catalog enabler genus <code>Type</code> which does not include
     *  catalog enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  catalog enablers or an error results. Otherwise, the returned list
     *  may contain only those catalog enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, catalog enablers are returned that are currently
     *  active. In any status mode, active and inactive catalog enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getCatalogEnablers()</code>.
     *
     *  @param  catalogEnablerGenusType a catalogEnabler genus type 
     *  @return the returned <code>CatalogEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>catalogEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerList getCatalogEnablersByGenusType(org.osid.type.Type catalogEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.cataloging.rules.catalogenabler.CatalogEnablerGenusFilterList(getCatalogEnablers(), catalogEnablerGenusType));
    }


    /**
     *  Gets a <code>CatalogEnablerList</code> corresponding to the given
     *  catalog enabler genus <code>Type</code> and include any additional
     *  catalog enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  catalog enablers or an error results. Otherwise, the returned list
     *  may contain only those catalog enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, catalog enablers are returned that are currently
     *  active. In any status mode, active and inactive catalog enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCatalogEnablers()</code>.
     *
     *  @param  catalogEnablerGenusType a catalogEnabler genus type 
     *  @return the returned <code>CatalogEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>catalogEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerList getCatalogEnablersByParentGenusType(org.osid.type.Type catalogEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getCatalogEnablersByGenusType(catalogEnablerGenusType));
    }


    /**
     *  Gets a <code>CatalogEnablerList</code> containing the given
     *  catalog enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  catalog enablers or an error results. Otherwise, the returned list
     *  may contain only those catalog enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *

     *  In active mode, catalog enablers are returned that are currently
     *  active. In any status mode, active and inactive catalog enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCatalogEnablers()</code>.
     *
     *  @param  catalogEnablerRecordType a catalogEnabler record type 
     *  @return the returned <code>CatalogEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>catalogEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerList getCatalogEnablersByRecordType(org.osid.type.Type catalogEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.cataloging.rules.catalogenabler.CatalogEnablerRecordFilterList(getCatalogEnablers(), catalogEnablerRecordType));
    }


    /**
     *  Gets a <code>CatalogEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  catalog enablers or an error results. Otherwise, the returned list
     *  may contain only those catalog enablers that are accessible
     *  through this session.
     *  
     *  In active mode, catalog enablers are returned that are currently
     *  active in addition to being effective during the given date
     *  range. In any status mode, active and inactive catalog enablers are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>CatalogEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerList getCatalogEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.cataloging.rules.catalogenabler.TemporalCatalogEnablerFilterList(getCatalogEnablers(), from, to));
    }
        

    /**
     *  Gets a <code>CatalogEnablerList </code> which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  catalog enablers or an error results. Otherwise, the returned list
     *  may contain only those catalog enablers that are accessible
     *  through this session.
     *
     *  In active mode, catalog enablers are returned that are currently
     *  active in addition to being effective during the date
     *  range. In any status mode, active and inactive catalog enablers are
     *  returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>CatalogEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerList getCatalogEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (getCatalogEnablersOnDate(from, to));
    }


    /**
     *  Gets all <code>CatalogEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  catalog enablers or an error results. Otherwise, the returned list
     *  may contain only those catalog enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, catalog enablers are returned that are currently
     *  active. In any status mode, active and inactive catalog enablers
     *  are returned.
     *
     *  @return a list of <code>CatalogEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.cataloging.rules.CatalogEnablerList getCatalogEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the catalog enabler list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of catalog enablers
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.cataloging.rules.CatalogEnablerList filterCatalogEnablersOnViews(org.osid.cataloging.rules.CatalogEnablerList list)
        throws org.osid.OperationFailedException {

        org.osid.cataloging.rules.CatalogEnablerList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.cataloging.rules.catalogenabler.ActiveCatalogEnablerFilterList(ret);
        }

        return (ret);
    }
}
