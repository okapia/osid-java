//
// AbstractQueryJobConstrainerLookupSession.java
//
//    A JobConstrainerQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.resourcing.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A JobConstrainerQuerySession adapter.
 */

public abstract class AbstractAdapterJobConstrainerQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.resourcing.rules.JobConstrainerQuerySession {

    private final org.osid.resourcing.rules.JobConstrainerQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterJobConstrainerQuerySession.
     *
     *  @param session the underlying job constrainer query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterJobConstrainerQuerySession(org.osid.resourcing.rules.JobConstrainerQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeFoundry</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeFoundry Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFoundryId() {
        return (this.session.getFoundryId());
    }


    /**
     *  Gets the {@codeFoundry</code> associated with this 
     *  session.
     *
     *  @return the {@codeFoundry</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Foundry getFoundry()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getFoundry());
    }


    /**
     *  Tests if this user can perform {@codeJobConstrainer</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchJobConstrainers() {
        return (this.session.canSearchJobConstrainers());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include job constrainers in foundries which are children
     *  of this foundry in the foundry hierarchy.
     */

    @OSID @Override
    public void useFederatedFoundryView() {
        this.session.useFederatedFoundryView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this foundry only.
     */
    
    @OSID @Override
    public void useIsolatedFoundryView() {
        this.session.useIsolatedFoundryView();
        return;
    }
    
      
    /**
     *  Gets a job constrainer query. The returned query will not have an
     *  extension query.
     *
     *  @return the job constrainer query 
     */
      
    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerQuery getJobConstrainerQuery() {
        return (this.session.getJobConstrainerQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  jobConstrainerQuery the job constrainer query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code jobConstrainerQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code jobConstrainerQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerList getJobConstrainersByQuery(org.osid.resourcing.rules.JobConstrainerQuery jobConstrainerQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getJobConstrainersByQuery(jobConstrainerQuery));
    }
}
