//
// AbstractTrackingRulesManager.java
//
//     An adapter for a TrackingRulesManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.tracking.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a TrackingRulesManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterTrackingRulesManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.tracking.rules.TrackingRulesManager>
    implements org.osid.tracking.rules.TrackingRulesManager {


    /**
     *  Constructs a new {@code AbstractAdapterTrackingRulesManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterTrackingRulesManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterTrackingRulesManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterTrackingRulesManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up queue constrainer is supported. 
     *
     *  @return <code> true </code> if queue constrainer lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerLookup() {
        return (getAdapteeManager().supportsQueueConstrainerLookup());
    }


    /**
     *  Tests if querying queue constrainer is supported. 
     *
     *  @return <code> true </code> if queue constrainer query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerQuery() {
        return (getAdapteeManager().supportsQueueConstrainerQuery());
    }


    /**
     *  Tests if searching queue constrainer is supported. 
     *
     *  @return <code> true </code> if queue constrainer search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerSearch() {
        return (getAdapteeManager().supportsQueueConstrainerSearch());
    }


    /**
     *  Tests if a queue constrainer administrative service is supported. 
     *
     *  @return <code> true </code> if queue constrainer administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerAdmin() {
        return (getAdapteeManager().supportsQueueConstrainerAdmin());
    }


    /**
     *  Tests if a queue constrainer notification service is supported. 
     *
     *  @return <code> true </code> if queue constrainer notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerNotification() {
        return (getAdapteeManager().supportsQueueConstrainerNotification());
    }


    /**
     *  Tests if a queue constrainer front office lookup service is supported. 
     *
     *  @return <code> true </code> if a queue constrainer front office lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerFrontOffice() {
        return (getAdapteeManager().supportsQueueConstrainerFrontOffice());
    }


    /**
     *  Tests if a queue constrainer front office service is supported. 
     *
     *  @return <code> true </code> if queue constrainer front office 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerFrontOfficeAssignment() {
        return (getAdapteeManager().supportsQueueConstrainerFrontOfficeAssignment());
    }


    /**
     *  Tests if a queue constrainer front office lookup service is supported. 
     *
     *  @return <code> true </code> if a queue constrainer front office 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerSmartFrontOffice() {
        return (getAdapteeManager().supportsQueueConstrainerSmartFrontOffice());
    }


    /**
     *  Tests if a queue constrainer rule lookup service is supported. 
     *
     *  @return <code> true </code> if a queue constrainer rule lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerRuleLookup() {
        return (getAdapteeManager().supportsQueueConstrainerRuleLookup());
    }


    /**
     *  Tests if a queue constrainer rule application service is supported. 
     *
     *  @return <code> true </code> if a queue constrainer rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerRuleApplication() {
        return (getAdapteeManager().supportsQueueConstrainerRuleApplication());
    }


    /**
     *  Tests if looking up queue constrainer enablers is supported. 
     *
     *  @return <code> true </code> if queue constrainer enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerEnablerLookup() {
        return (getAdapteeManager().supportsQueueConstrainerEnablerLookup());
    }


    /**
     *  Tests if querying queue constrainer enablers is supported. 
     *
     *  @return <code> true </code> if queue constrainer enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerEnablerQuery() {
        return (getAdapteeManager().supportsQueueConstrainerEnablerQuery());
    }


    /**
     *  Tests if searching queue constrainer enablers is supported. 
     *
     *  @return <code> true </code> if queue constrainer enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerEnablerSearch() {
        return (getAdapteeManager().supportsQueueConstrainerEnablerSearch());
    }


    /**
     *  Tests if a queue constrainer enabler administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if queue constrainer enabler 
     *          administration is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerEnablerAdmin() {
        return (getAdapteeManager().supportsQueueConstrainerEnablerAdmin());
    }


    /**
     *  Tests if a queue constrainer enabler notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if queue constrainer enabler notification 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerEnablerNotification() {
        return (getAdapteeManager().supportsQueueConstrainerEnablerNotification());
    }


    /**
     *  Tests if a queue constrainer enabler front office lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a queue constrainer enabler front 
     *          office lookup service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerEnablerFrontOffice() {
        return (getAdapteeManager().supportsQueueConstrainerEnablerFrontOffice());
    }


    /**
     *  Tests if a queue constrainer enabler front office service is 
     *  supported. 
     *
     *  @return <code> true </code> if queue constrainer enabler front office 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerEnablerFrontOfficeAssignment() {
        return (getAdapteeManager().supportsQueueConstrainerEnablerFrontOfficeAssignment());
    }


    /**
     *  Tests if a queue constrainer enabler front office lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a queue constrainer enabler front 
     *          office service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerEnablerSmartFrontOffice() {
        return (getAdapteeManager().supportsQueueConstrainerEnablerSmartFrontOffice());
    }


    /**
     *  Tests if a queue constrainer enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a queue constrainer enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerEnablerRuleLookup() {
        return (getAdapteeManager().supportsQueueConstrainerEnablerRuleLookup());
    }


    /**
     *  Tests if a queue constrainer enabler rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if queue constrainer enabler rule 
     *          application service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerEnablerRuleApplication() {
        return (getAdapteeManager().supportsQueueConstrainerEnablerRuleApplication());
    }


    /**
     *  Tests if looking up queue processor is supported. 
     *
     *  @return <code> true </code> if queue processor lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorLookup() {
        return (getAdapteeManager().supportsQueueProcessorLookup());
    }


    /**
     *  Tests if querying queue processor is supported. 
     *
     *  @return <code> true </code> if queue processor query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorQuery() {
        return (getAdapteeManager().supportsQueueProcessorQuery());
    }


    /**
     *  Tests if searching queue processor is supported. 
     *
     *  @return <code> true </code> if queue processor search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorSearch() {
        return (getAdapteeManager().supportsQueueProcessorSearch());
    }


    /**
     *  Tests if a queue processor administrative service is supported. 
     *
     *  @return <code> true </code> if queue processor administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorAdmin() {
        return (getAdapteeManager().supportsQueueProcessorAdmin());
    }


    /**
     *  Tests if a queue processor notification service is supported. 
     *
     *  @return <code> true </code> if queue processor notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorNotification() {
        return (getAdapteeManager().supportsQueueProcessorNotification());
    }


    /**
     *  Tests if a queue processor front office lookup service is supported. 
     *
     *  @return <code> true </code> if a queue processor front office lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorFrontOffice() {
        return (getAdapteeManager().supportsQueueProcessorFrontOffice());
    }


    /**
     *  Tests if a queue processor front office service is supported. 
     *
     *  @return <code> true </code> if queue processor front office assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorFrontOfficeAssignment() {
        return (getAdapteeManager().supportsQueueProcessorFrontOfficeAssignment());
    }


    /**
     *  Tests if a queue processor front office lookup service is supported. 
     *
     *  @return <code> true </code> if a queue processor front office service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorSmartFrontOffice() {
        return (getAdapteeManager().supportsQueueProcessorSmartFrontOffice());
    }


    /**
     *  Tests if a queue processor rule lookup service is supported. 
     *
     *  @return <code> true </code> if a queue processor rule lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorRuleLookup() {
        return (getAdapteeManager().supportsQueueProcessorRuleLookup());
    }


    /**
     *  Tests if a queue processor rule application service is supported. 
     *
     *  @return <code> true </code> if queue processor rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorRuleApplication() {
        return (getAdapteeManager().supportsQueueProcessorRuleApplication());
    }


    /**
     *  Tests if looking up queue processor enablers is supported. 
     *
     *  @return <code> true </code> if queue processor enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorEnablerLookup() {
        return (getAdapteeManager().supportsQueueProcessorEnablerLookup());
    }


    /**
     *  Tests if querying queue processor enablers is supported. 
     *
     *  @return <code> true </code> if queue processor enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorEnablerQuery() {
        return (getAdapteeManager().supportsQueueProcessorEnablerQuery());
    }


    /**
     *  Tests if searching queue processor enablers is supported. 
     *
     *  @return <code> true </code> if queue processor enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorEnablerSearch() {
        return (getAdapteeManager().supportsQueueProcessorEnablerSearch());
    }


    /**
     *  Tests if a queue processor enabler administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if queue processor enabler administration 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorEnablerAdmin() {
        return (getAdapteeManager().supportsQueueProcessorEnablerAdmin());
    }


    /**
     *  Tests if a queue processor enabler notification service is supported. 
     *
     *  @return <code> true </code> if queue processor enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorEnablerNotification() {
        return (getAdapteeManager().supportsQueueProcessorEnablerNotification());
    }


    /**
     *  Tests if a queue processor enabler front office lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a queue processor enabler front office 
     *          lookup service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorEnablerFrontOffice() {
        return (getAdapteeManager().supportsQueueProcessorEnablerFrontOffice());
    }


    /**
     *  Tests if a queue processor enabler front office service is supported. 
     *
     *  @return <code> true </code> if queue processor enabler front office 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorEnablerFrontOfficeAssignment() {
        return (getAdapteeManager().supportsQueueProcessorEnablerFrontOfficeAssignment());
    }


    /**
     *  Tests if a queue processor enabler front office lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a queue processor enabler front office 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorEnablerSmartFrontOffice() {
        return (getAdapteeManager().supportsQueueProcessorEnablerSmartFrontOffice());
    }


    /**
     *  Tests if a queue processor enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if an processor enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorEnablerRuleLookup() {
        return (getAdapteeManager().supportsQueueProcessorEnablerRuleLookup());
    }


    /**
     *  Tests if a queue processor enabler rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if queue processor enabler rule 
     *          application service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorEnablerRuleApplication() {
        return (getAdapteeManager().supportsQueueProcessorEnablerRuleApplication());
    }


    /**
     *  Gets the supported <code> QueueConstrainer </code> record types. 
     *
     *  @return a list containing the supported <code> QueueConstrainer 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getQueueConstrainerRecordTypes() {
        return (getAdapteeManager().getQueueConstrainerRecordTypes());
    }


    /**
     *  Tests if the given <code> QueueConstrainer </code> record type is 
     *  supported. 
     *
     *  @param  queueConstrainerRecordType a <code> Type </code> indicating a 
     *          <code> QueueConstrainer </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          queueConstrainerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerRecordType(org.osid.type.Type queueConstrainerRecordType) {
        return (getAdapteeManager().supportsQueueConstrainerRecordType(queueConstrainerRecordType));
    }


    /**
     *  Gets the supported <code> QueueConstrainer </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> QueueConstrainer 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getQueueConstrainerSearchRecordTypes() {
        return (getAdapteeManager().getQueueConstrainerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> QueueConstrainer </code> search record type 
     *  is supported. 
     *
     *  @param  queueConstrainerSearchRecordType a <code> Type </code> 
     *          indicating a <code> QueueConstrainer </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          queueConstrainerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerSearchRecordType(org.osid.type.Type queueConstrainerSearchRecordType) {
        return (getAdapteeManager().supportsQueueConstrainerSearchRecordType(queueConstrainerSearchRecordType));
    }


    /**
     *  Gets the supported <code> QueueConstrainerEnabler </code> record 
     *  types. 
     *
     *  @return a list containing the supported <code> QueueConstrainerEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getQueueConstrainerEnablerRecordTypes() {
        return (getAdapteeManager().getQueueConstrainerEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> QueueConstrainerEnabler </code> record type 
     *  is supported. 
     *
     *  @param  queueConstrainerEnablerRecordType a <code> Type </code> 
     *          indicating a <code> QueueConstrainerEnabler </code> record 
     *          type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          queueConstrainerEnablerRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerEnablerRecordType(org.osid.type.Type queueConstrainerEnablerRecordType) {
        return (getAdapteeManager().supportsQueueConstrainerEnablerRecordType(queueConstrainerEnablerRecordType));
    }


    /**
     *  Gets the supported <code> QueueConstrainerEnabler </code> search 
     *  record types. 
     *
     *  @return a list containing the supported <code> QueueConstrainerEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getQueueConstrainerEnablerSearchRecordTypes() {
        return (getAdapteeManager().getQueueConstrainerEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> QueueConstrainerEnabler </code> search 
     *  record type is supported. 
     *
     *  @param  queueConstrainerEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> QueueConstrainerEnabler </code> search 
     *          record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          queueConstrainerEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerEnablerSearchRecordType(org.osid.type.Type queueConstrainerEnablerSearchRecordType) {
        return (getAdapteeManager().supportsQueueConstrainerEnablerSearchRecordType(queueConstrainerEnablerSearchRecordType));
    }


    /**
     *  Gets the supported <code> QueueProcessor </code> record types. 
     *
     *  @return a list containing the supported <code> QueueProcessor </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getQueueProcessorRecordTypes() {
        return (getAdapteeManager().getQueueProcessorRecordTypes());
    }


    /**
     *  Tests if the given <code> QueueProcessor </code> record type is 
     *  supported. 
     *
     *  @param  queueProcessorRecordType a <code> Type </code> indicating a 
     *          <code> QueueProcessor </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> queueProcessorRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsQueueProcessorRecordType(org.osid.type.Type queueProcessorRecordType) {
        return (getAdapteeManager().supportsQueueProcessorRecordType(queueProcessorRecordType));
    }


    /**
     *  Gets the supported <code> QueueProcessor </code> search record types. 
     *
     *  @return a list containing the supported <code> QueueProcessor </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getQueueProcessorSearchRecordTypes() {
        return (getAdapteeManager().getQueueProcessorSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> QueueProcessor </code> search record type is 
     *  supported. 
     *
     *  @param  queueProcessorSearchRecordType a <code> Type </code> 
     *          indicating a <code> QueueProcessor </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          queueProcessorSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsQueueProcessorSearchRecordType(org.osid.type.Type queueProcessorSearchRecordType) {
        return (getAdapteeManager().supportsQueueProcessorSearchRecordType(queueProcessorSearchRecordType));
    }


    /**
     *  Gets the supported <code> QueueProcessorEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> QueueProcessorEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getQueueProcessorEnablerRecordTypes() {
        return (getAdapteeManager().getQueueProcessorEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> QueueProcessorEnabler </code> record type is 
     *  supported. 
     *
     *  @param  queueProcessorEnablerRecordType a <code> Type </code> 
     *          indicating a <code> QueueProcessorEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          queueProcessorEnablerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsQueueProcessorEnablerRecordType(org.osid.type.Type queueProcessorEnablerRecordType) {
        return (getAdapteeManager().supportsQueueProcessorEnablerRecordType(queueProcessorEnablerRecordType));
    }


    /**
     *  Gets the supported <code> QueueProcessorEnabler </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> QueueProcessorEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getQueueProcessorEnablerSearchRecordTypes() {
        return (getAdapteeManager().getQueueProcessorEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> QueueProcessorEnabler </code> search record 
     *  type is supported. 
     *
     *  @param  queueProcessorEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> QueueProcessorEnabler </code> search 
     *          record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          queueProcessorEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsQueueProcessorEnablerSearchRecordType(org.osid.type.Type queueProcessorEnablerSearchRecordType) {
        return (getAdapteeManager().supportsQueueProcessorEnablerSearchRecordType(queueProcessorEnablerSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer lookup service. 
     *
     *  @return a <code> QueueConstrainerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerLookupSession getQueueConstrainerLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer lookup service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueConstrainerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerLookupSession getQueueConstrainerLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerLookupSessionForFrontOffice(frontOfficeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer query service. 
     *
     *  @return a <code> QueueConstrainerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerQuerySession getQueueConstrainerQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer query service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueConstrainerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerQuerySession getQueueConstrainerQuerySessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerQuerySessionForFrontOffice(frontOfficeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer search service. 
     *
     *  @return a <code> QueueConstrainerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerSearchSession getQueueConstrainerSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer earch service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueConstrainerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerSearchSession getQueueConstrainerSearchSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerSearchSessionForFrontOffice(frontOfficeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer administration service. 
     *
     *  @return a <code> QueueConstrainerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerAdminSession getQueueConstrainerAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer administration service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueConstrainerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerAdminSession getQueueConstrainerAdminSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerAdminSessionForFrontOffice(frontOfficeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer notification service. 
     *
     *  @param  queueConstrainerReceiver the notification callback 
     *  @return a <code> QueueConstrainerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> queueConstrainerReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerNotificationSession getQueueConstrainerNotificationSession(org.osid.tracking.rules.QueueConstrainerReceiver queueConstrainerReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerNotificationSession(queueConstrainerReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer notification service for the given front office. 
     *
     *  @param  queueConstrainerReceiver the notification callback 
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueConstrainerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no front office found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> queueConstrainerReceiver 
     *          </code> or <code> frontOfficeId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerNotificationSession getQueueConstrainerNotificationSessionForFrontOffice(org.osid.tracking.rules.QueueConstrainerReceiver queueConstrainerReceiver, 
                                                                                                                            org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerNotificationSessionForFrontOffice(queueConstrainerReceiver, frontOfficeId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup queue constrainer/office 
     *  mappings for queue constrainers. 
     *
     *  @return a <code> QueueConstrainerFrontOfficeSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerFrontOffice() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerFrontOfficeSession getQueueConstrainerFrontOfficeSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerFrontOfficeSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning queue 
     *  constrainer to front office. 
     *
     *  @return a <code> QueueConstrainerFrontOfficeAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerFrontOfficeAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerFrontOfficeAssignmentSession getQueueConstrainerFrontOfficeAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerFrontOfficeAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage queue constrainer smart 
     *  front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueConstrainerSmartFrontOfficeSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerSmartFrontOffice() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerSmartFrontOfficeSession getQueueConstrainerSmartFrontOfficeSession(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerSmartFrontOfficeSession(frontOfficeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer mapping lookup service for looking up the rules applied to 
     *  a queue. 
     *
     *  @return a <code> QueueConstrainerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerRuleLookupSession getQueueConstrainerRuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerRuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer mapping lookup service for the given front office for 
     *  looking up rules applied to a queue. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueConstrainerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerRuleLookupSession getQueueConstrainerRuleLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerRuleLookupSessionForFrontOffice(frontOfficeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer assignment service to apply to queues. 
     *
     *  @return a <code> QueueConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerRuleApplicationSession getQueueConstrainerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerRuleApplicationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer assignment service for the given front office to apply to 
     *  queues. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerRuleApplicationSession getQueueConstrainerRuleApplicationSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerRuleApplicationSessionForFrontOffice(frontOfficeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler lookup service. 
     *
     *  @return a <code> QueueConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerLookupSession getQueueConstrainerEnablerLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerEnablerLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler lookup service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerLookupSession getQueueConstrainerEnablerLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerEnablerLookupSessionForFrontOffice(frontOfficeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler query service. 
     *
     *  @return a <code> QueueConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerQuerySession getQueueConstrainerEnablerQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerEnablerQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler query service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerQuerySession getQueueConstrainerEnablerQuerySessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerEnablerQuerySessionForFrontOffice(frontOfficeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler search service. 
     *
     *  @return a <code> QueueConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerSearch() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerSearchSession getQueueConstrainerEnablerSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerEnablerSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enablers earch service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerSearchSession getQueueConstrainerEnablerSearchSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerEnablerSearchSessionForFrontOffice(frontOfficeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler administration service. 
     *
     *  @return a <code> QueueConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerAdminSession getQueueConstrainerEnablerAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerEnablerAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler administration service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerAdminSession getQueueConstrainerEnablerAdminSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerEnablerAdminSessionForFrontOffice(frontOfficeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler notification service. 
     *
     *  @param  queueConstrainerEnablerReceiver the notification callback 
     *  @return a <code> QueueConstrainerEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          queueConstrainerEnablerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerNotification() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerNotificationSession getQueueConstrainerEnablerNotificationSession(org.osid.tracking.rules.QueueConstrainerEnablerReceiver queueConstrainerEnablerReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerEnablerNotificationSession(queueConstrainerEnablerReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler notification service for the given front office. 
     *
     *  @param  queueConstrainerEnablerReceiver the notification callback 
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueConstrainerEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no front office found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          queueConstrainerEnablerReceiver </code> or <code> 
     *          frontOfficeId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerNotification() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerNotificationSession getQueueConstrainerEnablerNotificationSessionForFrontOffice(org.osid.tracking.rules.QueueConstrainerEnablerReceiver queueConstrainerEnablerReceiver, 
                                                                                                                                          org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerEnablerNotificationSessionForFrontOffice(queueConstrainerEnablerReceiver, frontOfficeId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup queue constrainer 
     *  enabler/office mappings for queue constrainer enablers. 
     *
     *  @return a <code> QueueConstrainerEnablerFrontOfficeSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerFrontOffice() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerFrontOfficeSession getQueueConstrainerEnablerFrontOfficeSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerEnablerFrontOfficeSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning queue 
     *  constrainer enablers to front office. 
     *
     *  @return a <code> QueueConstrainerEnablerFrontOfficeAssignmentSession 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerFrontOfficeAssignment() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerFrontOfficeAssignmentSession getQueueConstrainerEnablerFrontOfficeAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerEnablerFrontOfficeAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage queue constrainer 
     *  enabler smart front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueConstrainerEnablerSmartFrontOfficeSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerSmartFrontOffice() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerSmartFrontOfficeSession getQueueConstrainerEnablerSmartFrontOfficeSession(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerEnablerSmartFrontOfficeSession(frontOfficeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler mapping lookup service. 
     *
     *  @return a <code> QueueConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerRuleLookupSession getQueueConstrainerEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerEnablerRuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler mapping lookup service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerRuleLookupSession getQueueConstrainerEnablerRuleLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerEnablerRuleLookupSessionForFrontOffice(frontOfficeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler assignment service. 
     *
     *  @return a <code> QueueConstrainerEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerRuleApplicationSession getQueueConstrainerEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerEnablerRuleApplicationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler assignment service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueConstrainerEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerRuleApplicationSession getQueueConstrainerEnablerRuleApplicationSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerEnablerRuleApplicationSessionForFrontOffice(frontOfficeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor lookup service. 
     *
     *  @return a <code> QueueProcessorLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorLookupSession getQueueProcessorLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor lookup service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueProcessorLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorLookupSession getQueueProcessorLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorLookupSessionForFrontOffice(frontOfficeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor query service. 
     *
     *  @return a <code> QueueProcessorQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorQuerySession getQueueProcessorQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor query service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueProcessorQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorQuerySession getQueueProcessorQuerySessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorQuerySessionForFrontOffice(frontOfficeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor search service. 
     *
     *  @return a <code> QueueProcessorSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorSearchSession getQueueProcessorSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor earch service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueProcessorSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorSearchSession getQueueProcessorSearchSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorSearchSessionForFrontOffice(frontOfficeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor administration service. 
     *
     *  @return a <code> QueueProcessorAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorAdminSession getQueueProcessorAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor administration service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueProcessorAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorAdminSession getQueueProcessorAdminSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorAdminSessionForFrontOffice(frontOfficeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor notification service. 
     *
     *  @param  queueProcessorReceiver the notification callback 
     *  @return a <code> QueueProcessorNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> queueProcessorReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorNotificationSession getQueueProcessorNotificationSession(org.osid.tracking.rules.QueueProcessorReceiver queueProcessorReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorNotificationSession(queueProcessorReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor notification service for the given front office. 
     *
     *  @param  queueProcessorReceiver the notification callback 
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueProcessorNotificationSession </code> 
     *  @throws org.osid.NotFoundException no front office found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> queueProcessorReceiver 
     *          </code> or <code> frontOfficeId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorNotificationSession getQueueProcessorNotificationSessionForFrontOffice(org.osid.tracking.rules.QueueProcessorReceiver queueProcessorReceiver, 
                                                                                                                        org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorNotificationSessionForFrontOffice(queueProcessorReceiver, frontOfficeId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup queue processor/office 
     *  mappings for queue processors. 
     *
     *  @return a <code> QueueProcessorFrontOfficeSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorFrontOffice() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorFrontOfficeSession getQueueProcessorFrontOfficeSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorFrontOfficeSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning queue 
     *  processor to front office. 
     *
     *  @return a <code> QueueProcessorFrontOfficeAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorFrontOfficeAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorFrontOfficeAssignmentSession getQueueProcessorFrontOfficeAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorFrontOfficeAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage queue processor smart 
     *  front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueProcessorSmartFrontOfficeSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorSmartFrontOffice() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorSmartFrontOfficeSession getQueueProcessorSmartFrontOfficeSession(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorSmartFrontOfficeSession(frontOfficeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor mapping lookup service for looking up the rules applied to a 
     *  queue. 
     *
     *  @return a <code> QueueProcessorRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorRuleLookupSession getQueueProcessorRuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorRuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor mapping lookup service for the given front office for 
     *  looking up rules applied to a queue. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueProcessorRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorRuleLookupSession getQueueProcessorRuleLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorRuleLookupSessionForFrontOffice(frontOfficeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor assignment service. 
     *
     *  @return a <code> QueueProcessorRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorRuleApplicationSession getQueueProcessorRuleApplicationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorRuleApplicationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor assignment service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueProcessorRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorRuleApplicationSession getQueueProcessorRuleApplicationSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorRuleApplicationSessionForFrontOffice(frontOfficeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler lookup service. 
     *
     *  @return a <code> QueueProcessorEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerLookupSession getQueueProcessorEnablerLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorEnablerLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler lookup service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueProcessorEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerLookupSession getQueueProcessorEnablerLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorEnablerLookupSessionForFrontOffice(frontOfficeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler query service. 
     *
     *  @return a <code> QueueProcessorEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerQuerySession getQueueProcessorEnablerQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorEnablerQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler query service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueProcessorEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerQuerySession getQueueProcessorEnablerQuerySessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorEnablerQuerySessionForFrontOffice(frontOfficeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler search service. 
     *
     *  @return a <code> QueueProcessorEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerSearchSession getQueueProcessorEnablerSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorEnablerSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enablers earch service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueProcessorEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerSearchSession getQueueProcessorEnablerSearchSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorEnablerSearchSessionForFrontOffice(frontOfficeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler administration service. 
     *
     *  @return a <code> QueueProcessorEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerAdminSession getQueueProcessorEnablerAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorEnablerAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler administration service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueProcessorEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerAdminSession getQueueProcessorEnablerAdminSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorEnablerAdminSessionForFrontOffice(frontOfficeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler notification service. 
     *
     *  @param  queueProcessorEnablerReceiver the notification callback 
     *  @return a <code> QueueProcessorEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          queueProcessorEnablerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerNotificationSession getQueueProcessorEnablerNotificationSession(org.osid.tracking.rules.QueueProcessorEnablerReceiver queueProcessorEnablerReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorEnablerNotificationSession(queueProcessorEnablerReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler notification service for the given front office. 
     *
     *  @param  queueProcessorEnablerReceiver the notification callback 
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueProcessorEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no front office found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          queueProcessorEnablerReceiver </code> or <code> frontOfficeId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerNotificationSession getQueueProcessorEnablerNotificationSessionForFrontOffice(org.osid.tracking.rules.QueueProcessorEnablerReceiver queueProcessorEnablerReceiver, 
                                                                                                                                      org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorEnablerNotificationSessionForFrontOffice(queueProcessorEnablerReceiver, frontOfficeId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup queue processor 
     *  enabler/office mappings for queue processor enablers. 
     *
     *  @return a <code> QueueProcessorEnablerFrontOfficeSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerFrontOffice() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerFrontOfficeSession getQueueProcessorEnablerFrontOfficeSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorEnablerFrontOfficeSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning queue 
     *  processor enablers to front office. 
     *
     *  @return a <code> QueueProcessorEnablerFrontOfficeAssignmentSession 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerFrontOfficeAssignment() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerFrontOfficeAssignmentSession getQueueProcessorEnablerFrontOfficeAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorEnablerFrontOfficeAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage queue processor enabler 
     *  smart front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueProcessorEnablerSmartFrontOfficeSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerSmartFrontOffice() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerSmartFrontOfficeSession getQueueProcessorEnablerSmartFrontOfficeSession(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorEnablerSmartFrontOfficeSession(frontOfficeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler mapping lookup service. 
     *
     *  @return a <code> QueueProcessorEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerRuleLookupSession getQueueProcessorEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorEnablerRuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler mapping lookup service. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueProcessorEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerRuleLookupSession getQueueProcessorEnablerRuleLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorEnablerRuleLookupSessionForFrontOffice(frontOfficeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler assignment service. 
     *
     *  @return a <code> QueueProcessorEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerRuleApplicationSession getQueueProcessorEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorEnablerRuleApplicationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler assignment service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueProcessorEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerRuleApplicationSession getQueueProcessorEnablerRuleApplicationSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorEnablerRuleApplicationSessionForFrontOffice(frontOfficeId));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
