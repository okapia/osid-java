//
// AbstractRecipeSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recipe.recipe.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractRecipeSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.recipe.RecipeSearchResults {

    private org.osid.recipe.RecipeList recipes;
    private final org.osid.recipe.RecipeQueryInspector inspector;
    private final java.util.Collection<org.osid.recipe.records.RecipeSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractRecipeSearchResults.
     *
     *  @param recipes the result set
     *  @param recipeQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>recipes</code>
     *          or <code>recipeQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractRecipeSearchResults(org.osid.recipe.RecipeList recipes,
                                            org.osid.recipe.RecipeQueryInspector recipeQueryInspector) {
        nullarg(recipes, "recipes");
        nullarg(recipeQueryInspector, "recipe query inspectpr");

        this.recipes = recipes;
        this.inspector = recipeQueryInspector;

        return;
    }


    /**
     *  Gets the recipe list resulting from a search.
     *
     *  @return a recipe list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.recipe.RecipeList getRecipes() {
        if (this.recipes == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.recipe.RecipeList recipes = this.recipes;
        this.recipes = null;
	return (recipes);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.recipe.RecipeQueryInspector getRecipeQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  recipe search record <code> Type. </code> This method must
     *  be used to retrieve a recipe implementing the requested
     *  record.
     *
     *  @param recipeSearchRecordType a recipe search 
     *         record type 
     *  @return the recipe search
     *  @throws org.osid.NullArgumentException
     *          <code>recipeSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(recipeSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.recipe.records.RecipeSearchResultsRecord getRecipeSearchResultsRecord(org.osid.type.Type recipeSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.recipe.records.RecipeSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(recipeSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(recipeSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record recipe search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addRecipeRecord(org.osid.recipe.records.RecipeSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "recipe record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
