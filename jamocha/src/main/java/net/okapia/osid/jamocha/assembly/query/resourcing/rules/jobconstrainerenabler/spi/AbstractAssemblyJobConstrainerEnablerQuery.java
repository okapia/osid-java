//
// AbstractAssemblyJobConstrainerEnablerQuery.java
//
//     A JobConstrainerEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.resourcing.rules.jobconstrainerenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A JobConstrainerEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblyJobConstrainerEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.resourcing.rules.JobConstrainerEnablerQuery,
               org.osid.resourcing.rules.JobConstrainerEnablerQueryInspector,
               org.osid.resourcing.rules.JobConstrainerEnablerSearchOrder {

    private final java.util.Collection<org.osid.resourcing.rules.records.JobConstrainerEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resourcing.rules.records.JobConstrainerEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resourcing.rules.records.JobConstrainerEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyJobConstrainerEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyJobConstrainerEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to the job constrainer. 
     *
     *  @param  jobConstrainerId the job constrainer <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> jobConstrainerId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledJobConstrainerId(org.osid.id.Id jobConstrainerId, 
                                           boolean match) {
        getAssembler().addIdTerm(getRuledJobConstrainerIdColumn(), jobConstrainerId, match);
        return;
    }


    /**
     *  Clears the job constrainer <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledJobConstrainerIdTerms() {
        getAssembler().clearTerms(getRuledJobConstrainerIdColumn());
        return;
    }


    /**
     *  Gets the job constrainer <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledJobConstrainerIdTerms() {
        return (getAssembler().getIdTerms(getRuledJobConstrainerIdColumn()));
    }


    /**
     *  Gets the RuledJobConstrainerId column name.
     *
     * @return the column name
     */

    protected String getRuledJobConstrainerIdColumn() {
        return ("ruled_job_constrainer_id");
    }


    /**
     *  Tests if an <code> JobConstrainerQuery </code> is available. 
     *
     *  @return <code> true </code> if a job constrainer query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledJobConstrainerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a job constrainer. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the job constrainer query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledJobConstrainerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerQuery getRuledJobConstrainerQuery() {
        throw new org.osid.UnimplementedException("supportsRuledJobConstrainerQuery() is false");
    }


    /**
     *  Matches enablers mapped to any job constrainer. 
     *
     *  @param  match <code> true </code> for enablers mapped to any job 
     *          constrainer, <code> false </code> to match enablers mapped to 
     *          no job constrainers 
     */

    @OSID @Override
    public void matchAnyRuledJobConstrainer(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledJobConstrainerColumn(), match);
        return;
    }


    /**
     *  Clears the job constrainer query terms. 
     */

    @OSID @Override
    public void clearRuledJobConstrainerTerms() {
        getAssembler().clearTerms(getRuledJobConstrainerColumn());
        return;
    }


    /**
     *  Gets the job constrainer query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerQueryInspector[] getRuledJobConstrainerTerms() {
        return (new org.osid.resourcing.rules.JobConstrainerQueryInspector[0]);
    }


    /**
     *  Gets the RuledJobConstrainer column name.
     *
     * @return the column name
     */

    protected String getRuledJobConstrainerColumn() {
        return ("ruled_job_constrainer");
    }


    /**
     *  Matches enablers mapped to the foundry. 
     *
     *  @param  foundryId the foundry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchFoundryId(org.osid.id.Id foundryId, boolean match) {
        getAssembler().addIdTerm(getFoundryIdColumn(), foundryId, match);
        return;
    }


    /**
     *  Clears the foundry <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearFoundryIdTerms() {
        getAssembler().clearTerms(getFoundryIdColumn());
        return;
    }


    /**
     *  Gets the foundry <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFoundryIdTerms() {
        return (getAssembler().getIdTerms(getFoundryIdColumn()));
    }


    /**
     *  Gets the FoundryId column name.
     *
     * @return the column name
     */

    protected String getFoundryIdColumn() {
        return ("foundry_id");
    }


    /**
     *  Tests if a <code> FoundryQuery </code> is available. 
     *
     *  @return <code> true </code> if a foundry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFoundryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a foundry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the foundry query 
     *  @throws org.osid.UnimplementedException <code> supportsFoundryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQuery getFoundryQuery() {
        throw new org.osid.UnimplementedException("supportsFoundryQuery() is false");
    }


    /**
     *  Clears the foundry query terms. 
     */

    @OSID @Override
    public void clearFoundryTerms() {
        getAssembler().clearTerms(getFoundryColumn());
        return;
    }


    /**
     *  Gets the foundry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQueryInspector[] getFoundryTerms() {
        return (new org.osid.resourcing.FoundryQueryInspector[0]);
    }


    /**
     *  Gets the Foundry column name.
     *
     * @return the column name
     */

    protected String getFoundryColumn() {
        return ("foundry");
    }


    /**
     *  Tests if this jobConstrainerEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  jobConstrainerEnablerRecordType a job constrainer enabler record type 
     *  @return <code>true</code> if the jobConstrainerEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>jobConstrainerEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type jobConstrainerEnablerRecordType) {
        for (org.osid.resourcing.rules.records.JobConstrainerEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(jobConstrainerEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  jobConstrainerEnablerRecordType the job constrainer enabler record type 
     *  @return the job constrainer enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>jobConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(jobConstrainerEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.rules.records.JobConstrainerEnablerQueryRecord getJobConstrainerEnablerQueryRecord(org.osid.type.Type jobConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.rules.records.JobConstrainerEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(jobConstrainerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(jobConstrainerEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  jobConstrainerEnablerRecordType the job constrainer enabler record type 
     *  @return the job constrainer enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>jobConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(jobConstrainerEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.rules.records.JobConstrainerEnablerQueryInspectorRecord getJobConstrainerEnablerQueryInspectorRecord(org.osid.type.Type jobConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.rules.records.JobConstrainerEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(jobConstrainerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(jobConstrainerEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param jobConstrainerEnablerRecordType the job constrainer enabler record type
     *  @return the job constrainer enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>jobConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(jobConstrainerEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.rules.records.JobConstrainerEnablerSearchOrderRecord getJobConstrainerEnablerSearchOrderRecord(org.osid.type.Type jobConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.rules.records.JobConstrainerEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(jobConstrainerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(jobConstrainerEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this job constrainer enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param jobConstrainerEnablerQueryRecord the job constrainer enabler query record
     *  @param jobConstrainerEnablerQueryInspectorRecord the job constrainer enabler query inspector
     *         record
     *  @param jobConstrainerEnablerSearchOrderRecord the job constrainer enabler search order record
     *  @param jobConstrainerEnablerRecordType job constrainer enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>jobConstrainerEnablerQueryRecord</code>,
     *          <code>jobConstrainerEnablerQueryInspectorRecord</code>,
     *          <code>jobConstrainerEnablerSearchOrderRecord</code> or
     *          <code>jobConstrainerEnablerRecordTypejobConstrainerEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addJobConstrainerEnablerRecords(org.osid.resourcing.rules.records.JobConstrainerEnablerQueryRecord jobConstrainerEnablerQueryRecord, 
                                      org.osid.resourcing.rules.records.JobConstrainerEnablerQueryInspectorRecord jobConstrainerEnablerQueryInspectorRecord, 
                                      org.osid.resourcing.rules.records.JobConstrainerEnablerSearchOrderRecord jobConstrainerEnablerSearchOrderRecord, 
                                      org.osid.type.Type jobConstrainerEnablerRecordType) {

        addRecordType(jobConstrainerEnablerRecordType);

        nullarg(jobConstrainerEnablerQueryRecord, "job constrainer enabler query record");
        nullarg(jobConstrainerEnablerQueryInspectorRecord, "job constrainer enabler query inspector record");
        nullarg(jobConstrainerEnablerSearchOrderRecord, "job constrainer enabler search odrer record");

        this.queryRecords.add(jobConstrainerEnablerQueryRecord);
        this.queryInspectorRecords.add(jobConstrainerEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(jobConstrainerEnablerSearchOrderRecord);
        
        return;
    }
}
