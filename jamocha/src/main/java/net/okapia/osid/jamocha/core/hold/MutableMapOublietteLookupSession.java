//
// MutableMapOublietteLookupSession
//
//    Implements an Oubliette lookup service backed by a collection of
//    oubliettes that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.hold;


/**
 *  Implements an Oubliette lookup service backed by a collection of
 *  oubliettes. The oubliettes are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of oubliettes can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapOublietteLookupSession
    extends net.okapia.osid.jamocha.core.hold.spi.AbstractMapOublietteLookupSession
    implements org.osid.hold.OublietteLookupSession {


    /**
     *  Constructs a new {@code MutableMapOublietteLookupSession}
     *  with no oubliettes.
     */

    public MutableMapOublietteLookupSession() {
        return;
    }


    /**
     *  Constructs a new {@code MutableMapOublietteLookupSession} with a
     *  single oubliette.
     *  
     *  @param oubliette an oubliette
     *  @throws org.osid.NullArgumentException {@code oubliette}
     *          is {@code null}
     */

    public MutableMapOublietteLookupSession(org.osid.hold.Oubliette oubliette) {
        putOubliette(oubliette);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapOublietteLookupSession}
     *  using an array of oubliettes.
     *
     *  @param oubliettes an array of oubliettes
     *  @throws org.osid.NullArgumentException {@code oubliettes}
     *          is {@code null}
     */

    public MutableMapOublietteLookupSession(org.osid.hold.Oubliette[] oubliettes) {
        putOubliettes(oubliettes);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapOublietteLookupSession}
     *  using a collection of oubliettes.
     *
     *  @param oubliettes a collection of oubliettes
     *  @throws org.osid.NullArgumentException {@code oubliettes}
     *          is {@code null}
     */

    public MutableMapOublietteLookupSession(java.util.Collection<? extends org.osid.hold.Oubliette> oubliettes) {
        putOubliettes(oubliettes);
        return;
    }

    
    /**
     *  Makes an {@code Oubliette} available in this session.
     *
     *  @param oubliette an oubliette
     *  @throws org.osid.NullArgumentException {@code oubliette{@code  is
     *          {@code null}
     */

    @Override
    public void putOubliette(org.osid.hold.Oubliette oubliette) {
        super.putOubliette(oubliette);
        return;
    }


    /**
     *  Makes an array of oubliettes available in this session.
     *
     *  @param oubliettes an array of oubliettes
     *  @throws org.osid.NullArgumentException {@code oubliettes{@code 
     *          is {@code null}
     */

    @Override
    public void putOubliettes(org.osid.hold.Oubliette[] oubliettes) {
        super.putOubliettes(oubliettes);
        return;
    }


    /**
     *  Makes collection of oubliettes available in this session.
     *
     *  @param oubliettes a collection of oubliettes
     *  @throws org.osid.NullArgumentException {@code oubliettes{@code  is
     *          {@code null}
     */

    @Override
    public void putOubliettes(java.util.Collection<? extends org.osid.hold.Oubliette> oubliettes) {
        super.putOubliettes(oubliettes);
        return;
    }


    /**
     *  Removes an Oubliette from this session.
     *
     *  @param oublietteId the {@code Id} of the oubliette
     *  @throws org.osid.NullArgumentException {@code oublietteId{@code 
     *          is {@code null}
     */

    @Override
    public void removeOubliette(org.osid.id.Id oublietteId) {
        super.removeOubliette(oublietteId);
        return;
    }    
}
