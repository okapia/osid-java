//
// MutableIndexedMapFrontOfficeLookupSession
//
//    Implements a FrontOffice lookup service backed by a collection of
//    frontOffices indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.tracking;


/**
 *  Implements a FrontOffice lookup service backed by a collection of
 *  front offices. The front offices are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some front offices may be compatible
 *  with more types than are indicated through these front office
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of front offices can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapFrontOfficeLookupSession
    extends net.okapia.osid.jamocha.core.tracking.spi.AbstractIndexedMapFrontOfficeLookupSession
    implements org.osid.tracking.FrontOfficeLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapFrontOfficeLookupSession} with no
     *  front offices.
     */

    public MutableIndexedMapFrontOfficeLookupSession() {
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapFrontOfficeLookupSession} with a
     *  single front office.
     *  
     *  @param  frontOffice a single frontOffice
     *  @throws org.osid.NullArgumentException {@code frontOffice}
     *          is {@code null}
     */

    public MutableIndexedMapFrontOfficeLookupSession(org.osid.tracking.FrontOffice frontOffice) {
        putFrontOffice(frontOffice);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapFrontOfficeLookupSession} using an
     *  array of front offices.
     *
     *  @param  frontOffices an array of front offices
     *  @throws org.osid.NullArgumentException {@code frontOffices}
     *          is {@code null}
     */

    public MutableIndexedMapFrontOfficeLookupSession(org.osid.tracking.FrontOffice[] frontOffices) {
        putFrontOffices(frontOffices);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapFrontOfficeLookupSession} using a
     *  collection of front offices.
     *
     *  @param  frontOffices a collection of front offices
     *  @throws org.osid.NullArgumentException {@code frontOffices} is
     *          {@code null}
     */

    public MutableIndexedMapFrontOfficeLookupSession(java.util.Collection<? extends org.osid.tracking.FrontOffice> frontOffices) {
        putFrontOffices(frontOffices);
        return;
    }
    

    /**
     *  Makes a {@code FrontOffice} available in this session.
     *
     *  @param  frontOffice a front office
     *  @throws org.osid.NullArgumentException {@code frontOffice{@code  is
     *          {@code null}
     */

    @Override
    public void putFrontOffice(org.osid.tracking.FrontOffice frontOffice) {
        super.putFrontOffice(frontOffice);
        return;
    }


    /**
     *  Makes an array of front offices available in this session.
     *
     *  @param  frontOffices an array of front offices
     *  @throws org.osid.NullArgumentException {@code frontOffices{@code 
     *          is {@code null}
     */

    @Override
    public void putFrontOffices(org.osid.tracking.FrontOffice[] frontOffices) {
        super.putFrontOffices(frontOffices);
        return;
    }


    /**
     *  Makes collection of front offices available in this session.
     *
     *  @param  frontOffices a collection of front offices
     *  @throws org.osid.NullArgumentException {@code frontOffice{@code  is
     *          {@code null}
     */

    @Override
    public void putFrontOffices(java.util.Collection<? extends org.osid.tracking.FrontOffice> frontOffices) {
        super.putFrontOffices(frontOffices);
        return;
    }


    /**
     *  Removes a FrontOffice from this session.
     *
     *  @param frontOfficeId the {@code Id} of the front office
     *  @throws org.osid.NullArgumentException {@code frontOfficeId{@code  is
     *          {@code null}
     */

    @Override
    public void removeFrontOffice(org.osid.id.Id frontOfficeId) {
        super.removeFrontOffice(frontOfficeId);
        return;
    }    
}
