//
// AbstractRelevancyNotificationSession.java
//
//     A template for making RelevancyNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ontology.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Relevancy} objects. This session is
 *  intended for consumers needing to synchronize their state with
 *  this service without the use of polling. Notifications are
 *  cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Relevancy} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for relevancy entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractRelevancyNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.ontology.RelevancyNotificationSession {

    private boolean federated = false;
    private org.osid.ontology.Ontology ontology = new net.okapia.osid.jamocha.nil.ontology.ontology.UnknownOntology();


    /**
     *  Gets the {@code Ontology/code> {@code Id} associated with this
     *  session.
     *
     *  @return the {@code Ontology Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getOntologyId() {
        return (this.ontology.getId());
    }

    
    /**
     *  Gets the {@code Ontology} associated with this session.
     *
     *  @return the {@code Ontology} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.Ontology getOntology()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.ontology);
    }


    /**
     *  Sets the {@code Ontology}.
     *
     *  @param ontology the ontology for this session
     *  @throws org.osid.NullArgumentException {@code ontology}
     *          is {@code null}
     */

    protected void setOntology(org.osid.ontology.Ontology ontology) {
        nullarg(ontology, "ontology");
        this.ontology = ontology;
        return;
    }


    /**
     *  Tests if this user can register for {@code Relevancy}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForRelevancyNotifications() {
        return (true);
    }


    /**
     *  Reliable notifications are desired. In reliable mode,
     *  notifications are to be acknowledged using <code>
     *  acknowledgeRelevancyNotification() </code>.
     */

    @OSID @Override
    public void reliableRelevancyNotifications() {
        return;
    }


    /**
     *  Unreliable notifications are desired. In unreliable mode,
     *  notifications do not need to be acknowledged.
     */

    @OSID @Override
    public void unreliableRelevancyNotifications() {
        return;
    }


    /**
     *  Acknowledge a relevancy notification.
     *
     *  @param  notificationId the <code> Id </code> of the notification
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void acknowledgeRelevancyNotification(org.osid.id.Id notificationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include relevancies in ontologies which are
     *  children of this ontology in the ontology hierarchy.
     */

    @OSID @Override
    public void useFederatedOntologyView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this ontology only.
     */

    @OSID @Override
    public void useIsolatedOntologyView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return {@codetrue</code> if federated view,
     *          {@codefalse</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new relevancies. {@code
     *  RelevancyReceiver.newRelevancy()} is invoked when a new {@code
     *  Relevancy} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewRelevancies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new relevancies by the given
     *  genus type.  {@code RelevancyReceiver.newRelevancy()} is
     *  invoked when a new relevancy is created.
     *
     *  @param  relevancyGenusType the relevancy genus type 
     *  @throws org.osid.NullArgumentException {@code
     *          relevancyGenusType is null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForNewRelevanciesByGenusType(org.osid.type.Type relevancyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new relevancies for the given
     *  subject {@code Id}. {@code RelevancyReceiver.newRelevancy()}
     *  is invoked when a new {@code Relevancy} is created.
     *
     *  @param  subjectId the {@code Id} of the subject to monitor
     *  @throws org.osid.NullArgumentException {@code subjectId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewRelevanciesForSubject(org.osid.id.Id subjectId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /** 
     *  Register for notifications of new relevancies for the given id
     *  {@code Id}. {@code RelevancyReceiver.newRelevancy()} is
     *  invoked when a new {@code Relevancy} is created.
     *
     *  @param  id the {@code Id} of the id to monitor
     *  @throws org.osid.NullArgumentException {@code id} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewRelevanciesForId(org.osid.id.Id id)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated relevancies. {@code
     *  RelevancyReceiver.changedRelevancy()} is invoked when a
     *  relevancy is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedRelevancies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of changed relevancies of the given
     *  genus type. {@code RelevancyReceiver.changedRelevancy()} is
     *  invoked when a relevancy is changed.
     *
     *  @param  relevancyGenusType the relevancy genus type 
     *  @throws org.osid.NullArgumentException {@code
     *          relevancyGenusType is null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedRelevanciesByGenusType(org.osid.type.Type relevancyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of updated relevancies for the
     *  given subject {@code Id}. {@code
     *  RelevancyReceiver.changedRelevancy()} is invoked when a {@code
     *  Relevancy} in this ontology is changed.
     *
     *  @param  subjectId the {@code Id} of the subject to monitor
     *  @throws org.osid.NullArgumentException {@code subjectId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedRelevanciesForSubject(org.osid.id.Id subjectId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated relevancies for the
     *  given id {@code Id}. {@code
     *  RelevancyReceiver.changedRelevancy()} is invoked when a {@code
     *  Relevancy} in this ontology is changed.
     *
     *  @param  id the {@code Id} of the id to monitor
     *  @throws org.osid.NullArgumentException {@code id} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedRelevanciesForId(org.osid.id.Id id)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated relevancy. {@code
     *  RelevancyReceiver.changedRelevancy()} is invoked when the
     *  specified relevancy is changed.
     *
     *  @param relevancyId the {@code Id} of the {@code Relevancy} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code relevancyId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedRelevancy(org.osid.id.Id relevancyId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted relevancies. {@code
     *  RelevancyReceiver.deletedRelevancy()} is invoked when a
     *  relevancy is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedRelevancies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of deleted relevancies of the given
     *  genus type. {@code RelevancyReceiver.deletedRelevancy()} is
     *  invoked when a relevancy is deleted.
     *
     *  @param  relevancyGenusType the relevancy genus type 
     *  @throws org.osid.NullArgumentException {@code
     *          relevancyGenusType is null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForDeletedRelevanciesByGenusType(org.osid.type.Type relevancyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of deleted relevancies for the
     *  given subject {@code Id}. {@code
     *  RelevancyReceiver.deletedRelevancy()} is invoked when a {@code
     *  Relevancy} is deleted or removed from this ontology.
     *
     *  @param  subjectId the {@code Id} of the subject to monitor
     *  @throws org.osid.NullArgumentException {@code subjectId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */
      
    @OSID @Override
    public void registerForDeletedRelevanciesForSubject(org.osid.id.Id subjectId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of deleted relevancies for the
     *  given id {@code Id}. {@code
     *  RelevancyReceiver.deletedRelevancy()} is invoked when a {@code
     *  Relevancy} is deleted or removed from this ontology.
     *
     *  @param  id the {@code Id} of the id to monitor
     *  @throws org.osid.NullArgumentException {@code id} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForDeletedRelevanciesForId(org.osid.id.Id id)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of a deleted relevancy. {@code
     *  RelevancyReceiver.deletedRelevancy()} is invoked when the
     *  specified relevancy is deleted.
     *
     *  @param relevancyId the {@code Id} of the
     *          {@code Relevancy} to monitor
     *  @throws org.osid.NullArgumentException {@code relevancyId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedRelevancy(org.osid.id.Id relevancyId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
