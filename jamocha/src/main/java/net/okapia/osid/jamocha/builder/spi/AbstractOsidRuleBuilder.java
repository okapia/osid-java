//
// AbstractOsidRuleBuilder.java
//
//     Defines a builder for an OSID Rule.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      rules:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.spi;


/**
 *  Defines the OsidRule builder.
 */

public abstract class AbstractOsidRuleBuilder<T extends AbstractOsidRuleBuilder<? extends T>>
    extends AbstractOsidObjectBuilder<T> {

    private final OsidRuleMiter rule;


    /**
     *  Creates a new <code>AbstractOsidRuleBuilder</code>.
     *
     *  @param rule a rule miter interface
     *  @throws org.osid.NullArgumentException <code>rule</code> is
     *          <code>null</code>
     */

    protected AbstractOsidRuleBuilder(OsidRuleMiter rule) {
        super(rule);
        this.rule = rule;
        return;
    }


    /**
     *  Enables this object.
     */
    
    public T enabled() {
        this.rule.setEnabled(true);
        return (self());
    }


    /**
     *  Disables this object.
     */
    
    public T disabled() {
        this.rule.setDisabled(true);
        return (self());
    }


    /**
     *  Sets the operational flag.
     *
     *  @param operational <code>true</code>if operational,
     *         <code>false</code> if not operational
     */
    
    public T operational(boolean operational) {
        this.rule.setOperational(operational);
        return (self());
    }


    /**
     *  Sets the rule.
     *
     *  @param rule the rule
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>rule</code> is <code>null</code>
     */
    
    public T rule(org.osid.rules.Rule rule) {
        this.rule.setRule(rule);
        return (self());
    }
}
