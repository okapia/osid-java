//
// AbstractIndexedMapOffsetEventEnablerLookupSession.java
//
//    A simple framework for providing an OffsetEventEnabler lookup service
//    backed by a fixed collection of offset event enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an OffsetEventEnabler lookup service backed by a
 *  fixed collection of offset event enablers. The offset event enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some offset event enablers may be compatible
 *  with more types than are indicated through these offset event enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>OffsetEventEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapOffsetEventEnablerLookupSession
    extends AbstractMapOffsetEventEnablerLookupSession
    implements org.osid.calendaring.rules.OffsetEventEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.calendaring.rules.OffsetEventEnabler> offsetEventEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.calendaring.rules.OffsetEventEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.calendaring.rules.OffsetEventEnabler> offsetEventEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.calendaring.rules.OffsetEventEnabler>());


    /**
     *  Makes an <code>OffsetEventEnabler</code> available in this session.
     *
     *  @param  offsetEventEnabler an offset event enabler
     *  @throws org.osid.NullArgumentException <code>offsetEventEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putOffsetEventEnabler(org.osid.calendaring.rules.OffsetEventEnabler offsetEventEnabler) {
        super.putOffsetEventEnabler(offsetEventEnabler);

        this.offsetEventEnablersByGenus.put(offsetEventEnabler.getGenusType(), offsetEventEnabler);
        
        try (org.osid.type.TypeList types = offsetEventEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.offsetEventEnablersByRecord.put(types.getNextType(), offsetEventEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an offset event enabler from this session.
     *
     *  @param offsetEventEnablerId the <code>Id</code> of the offset event enabler
     *  @throws org.osid.NullArgumentException <code>offsetEventEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeOffsetEventEnabler(org.osid.id.Id offsetEventEnablerId) {
        org.osid.calendaring.rules.OffsetEventEnabler offsetEventEnabler;
        try {
            offsetEventEnabler = getOffsetEventEnabler(offsetEventEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.offsetEventEnablersByGenus.remove(offsetEventEnabler.getGenusType());

        try (org.osid.type.TypeList types = offsetEventEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.offsetEventEnablersByRecord.remove(types.getNextType(), offsetEventEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeOffsetEventEnabler(offsetEventEnablerId);
        return;
    }


    /**
     *  Gets an <code>OffsetEventEnablerList</code> corresponding to the given
     *  offset event enabler genus <code>Type</code> which does not include
     *  offset event enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known offset event enablers or an error results. Otherwise,
     *  the returned list may contain only those offset event enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  offsetEventEnablerGenusType an offset event enabler genus type 
     *  @return the returned <code>OffsetEventEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>offsetEventEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerList getOffsetEventEnablersByGenusType(org.osid.type.Type offsetEventEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.rules.offseteventenabler.ArrayOffsetEventEnablerList(this.offsetEventEnablersByGenus.get(offsetEventEnablerGenusType)));
    }


    /**
     *  Gets an <code>OffsetEventEnablerList</code> containing the given
     *  offset event enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known offset event enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  offset event enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  offsetEventEnablerRecordType an offset event enabler record type 
     *  @return the returned <code>offsetEventEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>offsetEventEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerList getOffsetEventEnablersByRecordType(org.osid.type.Type offsetEventEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.rules.offseteventenabler.ArrayOffsetEventEnablerList(this.offsetEventEnablersByRecord.get(offsetEventEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.offsetEventEnablersByGenus.clear();
        this.offsetEventEnablersByRecord.clear();

        super.close();

        return;
    }
}
