//
// AbstractQueryRaceConstrainerLookupSession.java
//
//    An inline adapter that maps a RaceConstrainerLookupSession to
//    a RaceConstrainerQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.voting.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a RaceConstrainerLookupSession to
 *  a RaceConstrainerQuerySession.
 */

public abstract class AbstractQueryRaceConstrainerLookupSession
    extends net.okapia.osid.jamocha.voting.rules.spi.AbstractRaceConstrainerLookupSession
    implements org.osid.voting.rules.RaceConstrainerLookupSession {

    private boolean activeonly    = false;
    private final org.osid.voting.rules.RaceConstrainerQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryRaceConstrainerLookupSession.
     *
     *  @param querySession the underlying race constrainer query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryRaceConstrainerLookupSession(org.osid.voting.rules.RaceConstrainerQuerySession querySession) {
        nullarg(querySession, "race constrainer query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Polls</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Polls Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getPollsId() {
        return (this.session.getPollsId());
    }


    /**
     *  Gets the <code>Polls</code> associated with this 
     *  session.
     *
     *  @return the <code>Polls</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Polls getPolls()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getPolls());
    }


    /**
     *  Tests if this user can perform <code>RaceConstrainer</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRaceConstrainers() {
        return (this.session.canSearchRaceConstrainers());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include race constrainers in pollses which are children
     *  of this polls in the polls hierarchy.
     */

    @OSID @Override
    public void useFederatedPollsView() {
        this.session.useFederatedPollsView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this polls only.
     */

    @OSID @Override
    public void useIsolatedPollsView() {
        this.session.useIsolatedPollsView();
        return;
    }
    

    /**
     *  Only active race constrainers are returned by methods in this
     *  session.
     */
     
    @OSID @Override
    public void useActiveRaceConstrainerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive race constrainers are returned by methods
     *  in this session.
     */
    
    @OSID @Override
    public void useAnyStatusRaceConstrainerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>RaceConstrainer</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>RaceConstrainer</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>RaceConstrainer</code> and retained for compatibility.
     *
     *  In active mode, race constrainers are returned that are
     *  currently active. In any status mode, active and inactive race
     *  constrainers are returned.
     *
     *  @param  raceConstrainerId <code>Id</code> of the
     *          <code>RaceConstrainer</code>
     *  @return the race constrainer
     *  @throws org.osid.NotFoundException <code>raceConstrainerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>raceConstrainerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainer getRaceConstrainer(org.osid.id.Id raceConstrainerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.rules.RaceConstrainerQuery query = getQuery();
        query.matchId(raceConstrainerId, true);
        org.osid.voting.rules.RaceConstrainerList raceConstrainers = this.session.getRaceConstrainersByQuery(query);
        if (raceConstrainers.hasNext()) {
            return (raceConstrainers.getNextRaceConstrainer());
        } 
        
        throw new org.osid.NotFoundException(raceConstrainerId + " not found");
    }


    /**
     *  Gets a <code>RaceConstrainerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  raceConstrainers specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>RaceConstrainers</code> may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  In active mode, race constrainers are returned that are
     *  currently active. In any status mode, active and inactive race
     *  constrainers are returned.
     *
     *  @param  raceConstrainerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>RaceConstrainer</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>raceConstrainerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerList getRaceConstrainersByIds(org.osid.id.IdList raceConstrainerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.rules.RaceConstrainerQuery query = getQuery();

        try (org.osid.id.IdList ids = raceConstrainerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getRaceConstrainersByQuery(query));
    }


    /**
     *  Gets a <code>RaceConstrainerList</code> corresponding to the
     *  given race constrainer genus <code>Type</code> which does not
     *  include race constrainers of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known race
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those race constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, race constrainers are returned that are
     *  currently active. In any status mode, active and inactive race
     *  constrainers are returned.
     *
     *  @param  raceConstrainerGenusType a raceConstrainer genus type 
     *  @return the returned <code>RaceConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>raceConstrainerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerList getRaceConstrainersByGenusType(org.osid.type.Type raceConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.rules.RaceConstrainerQuery query = getQuery();
        query.matchGenusType(raceConstrainerGenusType, true);
        return (this.session.getRaceConstrainersByQuery(query));
    }


    /**
     *  Gets a <code>RaceConstrainerList</code> corresponding to the
     *  given race constrainer genus <code>Type</code> and include any
     *  additional race constrainers with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known race
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those race constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, race constrainers are returned that are
     *  currently active. In any status mode, active and inactive race
     *  constrainers are returned.
     *
     *  @param  raceConstrainerGenusType a raceConstrainer genus type 
     *  @return the returned <code>RaceConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>raceConstrainerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerList getRaceConstrainersByParentGenusType(org.osid.type.Type raceConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.rules.RaceConstrainerQuery query = getQuery();
        query.matchParentGenusType(raceConstrainerGenusType, true);
        return (this.session.getRaceConstrainersByQuery(query));
    }


    /**
     *  Gets a <code>RaceConstrainerList</code> containing the given
     *  race constrainer record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known race
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those race constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, race constrainers are returned that are
     *  currently active. In any status mode, active and inactive race
     *  constrainers are returned.
     *
     *  @param  raceConstrainerRecordType a raceConstrainer record type 
     *  @return the returned <code>RaceConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>raceConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerList getRaceConstrainersByRecordType(org.osid.type.Type raceConstrainerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.rules.RaceConstrainerQuery query = getQuery();
        query.matchRecordType(raceConstrainerRecordType, true);
        return (this.session.getRaceConstrainersByQuery(query));
    }

    
    /**
     *  Gets all <code>RaceConstrainers</code>. 
     *
     *  In plenary mode, the returned list contains all known race
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those race constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, race constrainers are returned that are
     *  currently active. In any status mode, active and inactive race
     *  constrainers are returned.
     *
     *  @return a list of <code>RaceConstrainers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerList getRaceConstrainers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.rules.RaceConstrainerQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getRaceConstrainersByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.voting.rules.RaceConstrainerQuery getQuery() {
        org.osid.voting.rules.RaceConstrainerQuery query = this.session.getRaceConstrainerQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
