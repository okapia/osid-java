//
// DeviceEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.rules.deviceenabler;


/**
 *  A template for implementing a search results.
 */

public final class DeviceEnablerSearchResults
    extends net.okapia.osid.jamocha.control.rules.deviceenabler.spi.AbstractDeviceEnablerSearchResults
    implements org.osid.control.rules.DeviceEnablerSearchResults {


    /**
     *  Constructs a new <code>DeviceEnablerSearchResults.
     *
     *  @param deviceEnablers the result set
     *  @param deviceEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>deviceEnablers</code>
     *          or <code>deviceEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    public DeviceEnablerSearchResults(org.osid.control.rules.DeviceEnablerList deviceEnablers,
                                 org.osid.control.rules.DeviceEnablerQueryInspector deviceEnablerQueryInspector) {
        super(deviceEnablers, deviceEnablerQueryInspector);
        return;
    }
}
