//
// AbstractQueryRaceProcessorEnablerLookupSession.java
//
//    An inline adapter that maps a RaceProcessorEnablerLookupSession to
//    a RaceProcessorEnablerQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.voting.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a RaceProcessorEnablerLookupSession to
 *  a RaceProcessorEnablerQuerySession.
 */

public abstract class AbstractQueryRaceProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.voting.rules.spi.AbstractRaceProcessorEnablerLookupSession
    implements org.osid.voting.rules.RaceProcessorEnablerLookupSession {

    private boolean activeonly    = false;
    private boolean effectiveonly = false;
    private final org.osid.voting.rules.RaceProcessorEnablerQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryRaceProcessorEnablerLookupSession.
     *
     *  @param querySession the underlying race processor enabler query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryRaceProcessorEnablerLookupSession(org.osid.voting.rules.RaceProcessorEnablerQuerySession querySession) {
        nullarg(querySession, "race processor enabler query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Polls</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Polls Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getPollsId() {
        return (this.session.getPollsId());
    }


    /**
     *  Gets the <code>Polls</code> associated with this 
     *  session.
     *
     *  @return the <code>Polls</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Polls getPolls()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getPolls());
    }


    /**
     *  Tests if this user can perform
     *  <code>RaceProcessorEnabler</code> lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRaceProcessorEnablers() {
        return (this.session.canSearchRaceProcessorEnablers());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include race processor enablers in polls which are
     *  children of this polls in the polls hierarchy.
     */

    @OSID @Override
    public void useFederatedPollsView() {
        this.session.useFederatedPollsView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this polls only.
     */

    @OSID @Override
    public void useIsolatedPollsView() {
        this.session.useIsolatedPollsView();
        return;
    }
    

    /**
     *  Only active race processor enablers are returned by methods in
     *  this session.
     */
     
    @OSID @Override
    public void useActiveRaceProcessorEnablerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive race processor enablers are returned by
     *  methods in this session.
     */
    
    @OSID @Override
    public void useAnyStatusRaceProcessorEnablerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>RaceProcessorEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>RaceProcessorEnabler</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>RaceProcessorEnabler</code> and retained for
     *  compatibility.
     *
     *  In active mode, race processor enablers are returned that are
     *  currently active. In any status mode, active and inactive race
     *  processor enablers are returned.
     *
     *  @param  raceProcessorEnablerId <code>Id</code> of the
     *          <code>RaceProcessorEnabler</code>
     *  @return the race processor enabler
     *  @throws org.osid.NotFoundException <code>raceProcessorEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>raceProcessorEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnabler getRaceProcessorEnabler(org.osid.id.Id raceProcessorEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.rules.RaceProcessorEnablerQuery query = getQuery();
        query.matchId(raceProcessorEnablerId, true);
        org.osid.voting.rules.RaceProcessorEnablerList raceProcessorEnablers = this.session.getRaceProcessorEnablersByQuery(query);
        if (raceProcessorEnablers.hasNext()) {
            return (raceProcessorEnablers.getNextRaceProcessorEnabler());
        } 
        
        throw new org.osid.NotFoundException(raceProcessorEnablerId + " not found");
    }


    /**
     *  Gets a <code>RaceProcessorEnablerList</code> corresponding to
     *  the given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  raceProcessorEnablers specified in the <code>Id</code> list,
     *  in the order of the list, including duplicates, or an error
     *  results if an <code>Id</code> in the supplied list is not
     *  found or inaccessible. Otherwise, inaccessible
     *  <code>RaceProcessorEnablers</code> may be omitted from the
     *  list and may present the elements in any order including
     *  returning a unique set.
     *
     *  In active mode, race processor enablers are returned that are
     *  currently active. In any status mode, active and inactive race
     *  processor enablers are returned.
     *
     *  @param raceProcessorEnablerIds the list of <code>Ids</code> to
     *         retrieve
     *  @return the returned <code>RaceProcessorEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerList getRaceProcessorEnablersByIds(org.osid.id.IdList raceProcessorEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.rules.RaceProcessorEnablerQuery query = getQuery();

        try (org.osid.id.IdList ids = raceProcessorEnablerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getRaceProcessorEnablersByQuery(query));
    }


    /**
     *  Gets a <code>RaceProcessorEnablerList</code> corresponding to
     *  the given race processor enabler genus <code>Type</code> which
     *  does not include race processor enablers of types derived from
     *  the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known race
     *  processor enablers or an error results. Otherwise, the
     *  returned list may contain only those race processor enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, race processor enablers are returned that are
     *  currently active. In any status mode, active and inactive race
     *  processor enablers are returned.
     *
     *  @param  raceProcessorEnablerGenusType a raceProcessorEnabler genus type 
     *  @return the returned <code>RaceProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerList getRaceProcessorEnablersByGenusType(org.osid.type.Type raceProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.rules.RaceProcessorEnablerQuery query = getQuery();
        query.matchGenusType(raceProcessorEnablerGenusType, true);
        return (this.session.getRaceProcessorEnablersByQuery(query));
    }


    /**
     *  Gets a <code>RaceProcessorEnablerList</code> corresponding to
     *  the given race processor enabler genus <code>Type</code> and
     *  include any additional race processor enablers with genus
     *  types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known race
     *  processor enablers or an error results. Otherwise, the
     *  returned list may contain only those race processor enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, race processor enablers are returned that are
     *  currently active. In any status mode, active and inactive race
     *  processor enablers are returned.
     *
     *  @param raceProcessorEnablerGenusType a raceProcessorEnabler
     *         genus type
     *  @return the returned <code>RaceProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerList getRaceProcessorEnablersByParentGenusType(org.osid.type.Type raceProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.rules.RaceProcessorEnablerQuery query = getQuery();
        query.matchParentGenusType(raceProcessorEnablerGenusType, true);
        return (this.session.getRaceProcessorEnablersByQuery(query));
    }


    /**
     *  Gets a <code>RaceProcessorEnablerList</code> containing the
     *  given race processor enabler record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known race
     *  processor enablers or an error results. Otherwise, the
     *  returned list may contain only those race processor enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, race processor enablers are returned that are
     *  currently active. In any status mode, active and inactive race
     *  processor enablers are returned.
     *
     *  @param raceProcessorEnablerRecordType a raceProcessorEnabler
     *         record type
     *  @return the returned <code>sorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerList getRaceProcessorEnablersByRecordType(org.osid.type.Type raceProcessorEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.rules.RaceProcessorEnablerQuery query = getQuery();
        query.matchRecordType(raceProcessorEnablerRecordType, true);
        return (this.session.getRaceProcessorEnablersByQuery(query));
    }


    /**
     *  Gets a <code>RaceProcessorEnablerList</code> effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known race
     *  processor enablers or an error results. Otherwise, the
     *  returned list may contain only those race processor enablers
     *  that are accessible through this session.
     *  
     *  In active mode, race processor enablers are returned that are
     *  currently active. In any status mode, active and inactive race
     *  processor enablers are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>RaceProcessorEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerList getRaceProcessorEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.rules.RaceProcessorEnablerQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getRaceProcessorEnablersByQuery(query));
    }
        
    
    /**
     *  Gets all <code>RaceProcessorEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known race
     *  processor enablers or an error results. Otherwise, the
     *  returned list may contain only those race processor enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, race processor enablers are returned that are
     *  currently active. In any status mode, active and inactive race
     *  processor enablers are returned.
     *
     *  @return a list of <code>RaceProcessorEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerList getRaceProcessorEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.rules.RaceProcessorEnablerQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getRaceProcessorEnablersByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.voting.rules.RaceProcessorEnablerQuery getQuery() {
        org.osid.voting.rules.RaceProcessorEnablerQuery query = this.session.getRaceProcessorEnablerQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
