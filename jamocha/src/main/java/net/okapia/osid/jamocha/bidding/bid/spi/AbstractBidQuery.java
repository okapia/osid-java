//
// AbstractBidQuery.java
//
//     A template for making a Bid Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.bid.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for bids.
 */

public abstract class AbstractBidQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.bidding.BidQuery {

    private final java.util.Collection<org.osid.bidding.records.BidQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the auction <code> Id </code> for this query. 
     *
     *  @param  auctionId the auction <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> auctionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAuctionId(org.osid.id.Id auctionId, boolean match) {
        return;
    }


    /**
     *  Clears the auction <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAuctionIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AuctionQuery </code> is available. 
     *
     *  @return <code> true </code> if an auction query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionQuery() {
        return (false);
    }


    /**
     *  Gets the query for an auction. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the auction query 
     *  @throws org.osid.UnimplementedException <code> supportsAuctionQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionQuery getAuctionQuery() {
        throw new org.osid.UnimplementedException("supportsAuctionQuery() is false");
    }


    /**
     *  Clears the auction query terms. 
     */

    @OSID @Override
    public void clearAuctionTerms() {
        return;
    }


    /**
     *  Sets the resource <code> Id </code> for this query. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBidderId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the resource <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearBidderIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBidderQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsBidderQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getBidderQuery() {
        throw new org.osid.UnimplementedException("supportsBidderQuery() is false");
    }


    /**
     *  Clears the resource query terms. 
     */

    @OSID @Override
    public void clearBidderTerms() {
        return;
    }


    /**
     *  Sets the agent <code> Id </code> for this query. 
     *
     *  @param  agentId the agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBiddingAgentId(org.osid.id.Id agentId, boolean match) {
        return;
    }


    /**
     *  Clears the agent <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearBiddingAgentIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBiddingAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agent. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBiddingAgentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getBiddingAgentQuery() {
        throw new org.osid.UnimplementedException("supportsBiddingAgentQuery() is false");
    }


    /**
     *  Clears the agent query terms. 
     */

    @OSID @Override
    public void clearBiddingAgentTerms() {
        return;
    }


    /**
     *  Matches bids with quantities between the given range inclusive. 
     *
     *  @param  start start range 
     *  @param  end end range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     */

    @OSID @Override
    public void matchQuantity(long start, long end, boolean match) {
        return;
    }


    /**
     *  Clears the quantity query terms. 
     */

    @OSID @Override
    public void clearQuantityTerms() {
        return;
    }


    /**
     *  Matches bids with a current bidsbetween the given range inclusive. 
     *
     *  @param  start start range 
     *  @param  end end range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> or <code> 
     *          start.getCurencyType() </code> != <code> end.getCurrencyType() 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> currency </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCurrentBid(org.osid.financials.Currency start, 
                                org.osid.financials.Currency end, 
                                boolean match) {
        return;
    }


    /**
     *  Clears the current bid query terms. 
     */

    @OSID @Override
    public void clearCurrentBidTerms() {
        return;
    }


    /**
     *  Matches bids with a max bid between the given range inclusive. 
     *
     *  @param  start start range 
     *  @param  end end range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> or <code> 
     *          start.getCurencyType() </code> != <code> end.getCurrencyType() 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> currency </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchMaximumBid(org.osid.financials.Currency start, 
                                org.osid.financials.Currency end, 
                                boolean match) {
        return;
    }


    /**
     *  Clears the max bid query terms. 
     */

    @OSID @Override
    public void clearMaximumBidTerms() {
        return;
    }


    /**
     *  Matches winning bids. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchWinner(boolean match) {
        return;
    }


    /**
     *  Clears the winner query terms. 
     */

    @OSID @Override
    public void clearWinnerTerms() {
        return;
    }


    /**
     *  Matches bids with a settlement amount between the given range 
     *  inclusive. 
     *
     *  @param  start start range 
     *  @param  end end range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> or <code> 
     *          start.getCurencyType() </code> != <code> end.getCurrencyType() 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> currency </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSettlementAmount(org.osid.financials.Currency start, 
                                      org.osid.financials.Currency end, 
                                      boolean match) {
        return;
    }


    /**
     *  Clears the settlement amount query terms. 
     */

    @OSID @Override
    public void clearSettlementAmountTerms() {
        return;
    }


    /**
     *  Sets the auction house <code> Id </code> for this query to match bids 
     *  assigned to auction houses. 
     *
     *  @param  auctionHouseId the auction house <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchAuctionHouseId(org.osid.id.Id auctionHouseId, 
                                    boolean match) {
        return;
    }


    /**
     *  Clears the auction house <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAuctionHouseIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> AuctionHouseQuery </code> is available. 
     *
     *  @return <code> true </code> if a auction house query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionHouseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a auction house. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the auction house query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionHouseQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseQuery getAuctionHouseQuery() {
        throw new org.osid.UnimplementedException("supportsAuctionHouseQuery() is false");
    }


    /**
     *  Clears the auction house query terms. 
     */

    @OSID @Override
    public void clearAuctionHouseTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given bid query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a bid implementing the requested record.
     *
     *  @param bidRecordType a bid record type
     *  @return the bid query record
     *  @throws org.osid.NullArgumentException
     *          <code>bidRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(bidRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.records.BidQueryRecord getBidQueryRecord(org.osid.type.Type bidRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.bidding.records.BidQueryRecord record : this.records) {
            if (record.implementsRecordType(bidRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(bidRecordType + " is not supported");
    }


    /**
     *  Adds a record to this bid query. 
     *
     *  @param bidQueryRecord bid query record
     *  @param bidRecordType bid record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBidQueryRecord(org.osid.bidding.records.BidQueryRecord bidQueryRecord, 
                                          org.osid.type.Type bidRecordType) {

        addRecordType(bidRecordType);
        nullarg(bidQueryRecord, "bid query record");
        this.records.add(bidQueryRecord);        
        return;
    }
}
