//
// AbstractBankQueryInspector.java
//
//     A template for making a BankQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.bank.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for banks.
 */

public abstract class AbstractBankQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.assessment.BankQueryInspector {

    private final java.util.Collection<org.osid.assessment.records.BankQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the item <code> Id </code> query terms. 
     *
     *  @return the item <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getItemIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the item query terms. 
     *
     *  @return the item query terms 
     */

    @OSID @Override
    public org.osid.assessment.ItemQueryInspector[] getItemTerms() {
        return (new org.osid.assessment.ItemQueryInspector[0]);
    }


    /**
     *  Gets the assessment <code> Id </code> query terms. 
     *
     *  @return the assessment <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssessmentIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the assessment query terms. 
     *
     *  @return the assessment terms 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentQueryInspector[] getAssessmentTerms() {
        return (new org.osid.assessment.AssessmentQueryInspector[0]);
    }


    /**
     *  Gets the assessment offered <code> Id </code> query terms. 
     *
     *  @return the assessment offered <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssessmentOfferedIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the assessment offered query terms. 
     *
     *  @return the assessment offered terms 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedQueryInspector[] getAssessmentOfferedTerms() {
        return (new org.osid.assessment.AssessmentOfferedQueryInspector[0]);
    }


    /**
     *  Gets the ancestor bank <code> Id </code> query terms. 
     *
     *  @return the ancestor bank <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorBankIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor bank query terms. 
     *
     *  @return the ancestor bank terms 
     */

    @OSID @Override
    public org.osid.assessment.BankQueryInspector[] getAncestorBankTerms() {
        return (new org.osid.assessment.BankQueryInspector[0]);
    }


    /**
     *  Gets the descendant bank <code> Id </code> query terms. 
     *
     *  @return the descendant bank <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantBankIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant bank query terms. 
     *
     *  @return the descendant bank terms 
     */

    @OSID @Override
    public org.osid.assessment.BankQueryInspector[] getDescendantBankTerms() {
        return (new org.osid.assessment.BankQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given bank query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a bank implementing the requested record.
     *
     *  @param bankRecordType a bank record type
     *  @return the bank query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>bankRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(bankRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.BankQueryInspectorRecord getBankQueryInspectorRecord(org.osid.type.Type bankRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.records.BankQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(bankRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(bankRecordType + " is not supported");
    }


    /**
     *  Adds a record to this bank query. 
     *
     *  @param bankQueryInspectorRecord bank query inspector
     *         record
     *  @param bankRecordType bank record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBankQueryInspectorRecord(org.osid.assessment.records.BankQueryInspectorRecord bankQueryInspectorRecord, 
                                                   org.osid.type.Type bankRecordType) {

        addRecordType(bankRecordType);
        nullarg(bankRecordType, "bank record type");
        this.records.add(bankQueryInspectorRecord);        
        return;
    }
}
