//
// AbstractQueryConfigurationLookupSession.java
//
//    An inline adapter that maps a ConfigurationLookupSession to
//    a ConfigurationQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.configuration.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a ConfigurationLookupSession to
 *  a ConfigurationQuerySession.
 */

public abstract class AbstractQueryConfigurationLookupSession
    extends net.okapia.osid.jamocha.configuration.spi.AbstractConfigurationLookupSession
    implements org.osid.configuration.ConfigurationLookupSession {

    private final org.osid.configuration.ConfigurationQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryConfigurationLookupSession.
     *
     *  @param querySession the underlying configuration query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryConfigurationLookupSession(org.osid.configuration.ConfigurationQuerySession querySession) {
        nullarg(querySession, "configuration query session");
        this.session = querySession;
        return;
    }



    /**
     *  Tests if this user can perform <code>Configuration</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupConfigurations() {
        return (this.session.canSearchConfigurations());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }

     
    /**
     *  Gets the <code>Configuration</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Configuration</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Configuration</code> and
     *  retained for compatibility.
     *
     *  @param  configurationId <code>Id</code> of the
     *          <code>Configuration</code>
     *  @return the configuration
     *  @throws org.osid.NotFoundException <code>configurationId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>configurationId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.Configuration getConfiguration(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.configuration.ConfigurationQuery query = getQuery();
        query.matchId(configurationId, true);
        org.osid.configuration.ConfigurationList configurations = this.session.getConfigurationsByQuery(query);
        if (configurations.hasNext()) {
            return (configurations.getNextConfiguration());
        } 
        
        throw new org.osid.NotFoundException(configurationId + " not found");
    }


    /**
     *  Gets a <code>ConfigurationList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  configurations specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Configurations</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  configurationIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Configuration</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>configurationIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationList getConfigurationsByIds(org.osid.id.IdList configurationIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.configuration.ConfigurationQuery query = getQuery();

        try (org.osid.id.IdList ids = configurationIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getConfigurationsByQuery(query));
    }


    /**
     *  Gets a <code>ConfigurationList</code> corresponding to the given
     *  configuration genus <code>Type</code> which does not include
     *  configurations of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  configurations or an error results. Otherwise, the returned list
     *  may contain only those configurations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  configurationGenusType a configuration genus type 
     *  @return the returned <code>Configuration</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>configurationGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationList getConfigurationsByGenusType(org.osid.type.Type configurationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.configuration.ConfigurationQuery query = getQuery();
        query.matchGenusType(configurationGenusType, true);
        return (this.session.getConfigurationsByQuery(query));
    }


    /**
     *  Gets a <code>ConfigurationList</code> corresponding to the given
     *  configuration genus <code>Type</code> and include any additional
     *  configurations with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  configurations or an error results. Otherwise, the returned list
     *  may contain only those configurations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  configurationGenusType a configuration genus type 
     *  @return the returned <code>Configuration</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>configurationGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationList getConfigurationsByParentGenusType(org.osid.type.Type configurationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.configuration.ConfigurationQuery query = getQuery();
        query.matchParentGenusType(configurationGenusType, true);
        return (this.session.getConfigurationsByQuery(query));
    }


    /**
     *  Gets a <code>ConfigurationList</code> containing the given
     *  configuration record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  configurations or an error results. Otherwise, the returned list
     *  may contain only those configurations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  configurationRecordType a configuration record type 
     *  @return the returned <code>Configuration</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>configurationRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationList getConfigurationsByRecordType(org.osid.type.Type configurationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.configuration.ConfigurationQuery query = getQuery();
        query.matchRecordType(configurationRecordType, true);
        return (this.session.getConfigurationsByQuery(query));
    }


    /**
     *  Gets a <code>ConfigurationList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known configurations or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  configurations that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Configuration</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationList getConfigurationsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.configuration.ConfigurationQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getConfigurationsByQuery(query));        
    }

    
    /**
     *  Gets all <code>Configurations</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  configurations or an error results. Otherwise, the returned list
     *  may contain only those configurations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Configurations</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationList getConfigurations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.configuration.ConfigurationQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getConfigurationsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.configuration.ConfigurationQuery getQuery() {
        org.osid.configuration.ConfigurationQuery query = this.session.getConfigurationQuery();
        return (query);
    }
}
