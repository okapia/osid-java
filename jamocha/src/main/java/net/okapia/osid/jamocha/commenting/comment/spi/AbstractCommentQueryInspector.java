//
// AbstractCommentQueryInspector.java
//
//     A template for making a CommentQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.commenting.comment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for comments.
 */

public abstract class AbstractCommentQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQueryInspector
    implements org.osid.commenting.CommentQueryInspector {

    private final java.util.Collection<org.osid.commenting.records.CommentQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the reference <code> Id </code> terms. 
     *
     *  @return the reference <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getReferenceIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the resource <code> Id </code> terms. 
     *
     *  @return the resource <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCommentorIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the resource terms. 
     *
     *  @return the resource terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getCommentorTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the agent <code> Id </code> terms. 
     *
     *  @return the agent <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCommentingAgentIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the agent terms. 
     *
     *  @return the agent terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getCommentingAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Gets the text query terms. 
     *
     *  @return the text query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getTextTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the rating <code> Id </code> terms. 
     *
     *  @return the rating <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRatingIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the rating terms. 
     *
     *  @return the rating terms 
     */

    @OSID @Override
    public org.osid.grading.GradeQueryInspector[] getRatingTerms() {
        return (new org.osid.grading.GradeQueryInspector[0]);
    }


    /**
     *  Gets the book <code> Id </code> terms. 
     *
     *  @return the book <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBookIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the book terms. 
     *
     *  @return the book terms 
     */

    @OSID @Override
    public org.osid.commenting.BookQueryInspector[] getBookTerms() {
        return (new org.osid.commenting.BookQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given comment query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a comment implementing the requested record.
     *
     *  @param commentRecordType a comment record type
     *  @return the comment query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>commentRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(commentRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.commenting.records.CommentQueryInspectorRecord getCommentQueryInspectorRecord(org.osid.type.Type commentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.commenting.records.CommentQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(commentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(commentRecordType + " is not supported");
    }


    /**
     *  Adds a record to this comment query. 
     *
     *  @param commentQueryInspectorRecord comment query inspector
     *         record
     *  @param commentRecordType comment record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCommentQueryInspectorRecord(org.osid.commenting.records.CommentQueryInspectorRecord commentQueryInspectorRecord, 
                                                   org.osid.type.Type commentRecordType) {

        addRecordType(commentRecordType);
        nullarg(commentRecordType, "comment record type");
        this.records.add(commentQueryInspectorRecord);        
        return;
    }
}
