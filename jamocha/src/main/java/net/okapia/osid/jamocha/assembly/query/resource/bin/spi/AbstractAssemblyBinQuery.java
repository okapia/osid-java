//
// AbstractAssemblyBinQuery.java
//
//     A BinQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.resource.bin.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A BinQuery that stores terms.
 */

public abstract class AbstractAssemblyBinQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.resource.BinQuery,
               org.osid.resource.BinQueryInspector,
               org.osid.resource.BinSearchOrder {

    private final java.util.Collection<org.osid.resource.records.BinQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resource.records.BinQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resource.records.BinSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyBinQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyBinQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the resource <code> Id </code> for this query. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResourceId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getResourceIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearResourceIdTerms() {
        getAssembler().clearTerms(getResourceIdColumn());
        return;
    }


    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResourceIdTerms() {
        return (getAssembler().getIdTerms(getResourceIdColumn()));
    }


    /**
     *  Gets the ResourceId column name.
     *
     * @return the column name
     */

    protected String getResourceIdColumn() {
        return ("resource_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResourceQuery() {
        throw new org.osid.UnimplementedException("supportsResourceQuery() is false");
    }


    /**
     *  Matches bins with any resource. 
     *
     *  @param  match <code> true </code> to match bins with any resource, 
     *          <code> false </code> to match bins with no resources 
     */

    @OSID @Override
    public void matchAnyResource(boolean match) {
        getAssembler().addIdWildcardTerm(getResourceColumn(), match);
        return;
    }


    /**
     *  Clears the resource terms. 
     */

    @OSID @Override
    public void clearResourceTerms() {
        getAssembler().clearTerms(getResourceColumn());
        return;
    }


    /**
     *  Gets the resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the Resource column name.
     *
     * @return the column name
     */

    protected String getResourceColumn() {
        return ("resource");
    }


    /**
     *  Sets the bin <code> Id </code> for this query to match bins that have 
     *  the specified bin as an ancestor. 
     *
     *  @param  binid a bin <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchAncestorBinId(org.osid.id.Id binid, boolean match) {
        getAssembler().addIdTerm(getAncestorBinIdColumn(), binid, match);
        return;
    }


    /**
     *  Clears the ancestor bin <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorBinIdTerms() {
        getAssembler().clearTerms(getAncestorBinIdColumn());
        return;
    }


    /**
     *  Gets the ancestor bin <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorBinIdTerms() {
        return (getAssembler().getIdTerms(getAncestorBinIdColumn()));
    }


    /**
     *  Gets the AncestorBinId column name.
     *
     * @return the column name
     */

    protected String getAncestorBinIdColumn() {
        return ("ancestor_bin_id");
    }


    /**
     *  Tests if a <code> BinQuery </code> is available. 
     *
     *  @return <code> true </code> if a bin query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorBinQuery() {
        return (false);
    }


    /**
     *  Gets the query for a bin. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the bin query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorBinQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.BinQuery getAncestorBinQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorBinQuery() is false");
    }


    /**
     *  Matches bins with any ancestor. 
     *
     *  @param  match <code> true </code> to match bins with any ancestor, 
     *          <code> false </code> to match root bins 
     */

    @OSID @Override
    public void matchAnyAncestorBin(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorBinColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor bin terms. 
     */

    @OSID @Override
    public void clearAncestorBinTerms() {
        getAssembler().clearTerms(getAncestorBinColumn());
        return;
    }


    /**
     *  Gets the ancestor bin query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.BinQueryInspector[] getAncestorBinTerms() {
        return (new org.osid.resource.BinQueryInspector[0]);
    }


    /**
     *  Gets the AncestorBin column name.
     *
     * @return the column name
     */

    protected String getAncestorBinColumn() {
        return ("ancestor_bin");
    }


    /**
     *  Sets the bin <code> Id </code> for this query to match bins that have 
     *  the specified bin as a descendant. 
     *
     *  @param  binid a bin <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchDescendantBinId(org.osid.id.Id binid, boolean match) {
        getAssembler().addIdTerm(getDescendantBinIdColumn(), binid, match);
        return;
    }


    /**
     *  Clears the descendant bin <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantBinIdTerms() {
        getAssembler().clearTerms(getDescendantBinIdColumn());
        return;
    }


    /**
     *  Gets the descendant bin <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantBinIdTerms() {
        return (getAssembler().getIdTerms(getDescendantBinIdColumn()));
    }


    /**
     *  Gets the DescendantBinId column name.
     *
     * @return the column name
     */

    protected String getDescendantBinIdColumn() {
        return ("descendant_bin_id");
    }


    /**
     *  Tests if a <code> BinQuery </code> is available. 
     *
     *  @return <code> true </code> if a bin query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantBinQuery() {
        return (false);
    }


    /**
     *  Gets the query for a bin. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the bin query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantBinQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.BinQuery getDescendantBinQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantBinQuery() is false");
    }


    /**
     *  Matches bins with any descendant. 
     *
     *  @param  match <code> true </code> to match bins with any descendant, 
     *          <code> false </code> to match leaf bins 
     */

    @OSID @Override
    public void matchAnyDescendantBin(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantBinColumn(), match);
        return;
    }


    /**
     *  Clears the descendant bin terms. 
     */

    @OSID @Override
    public void clearDescendantBinTerms() {
        getAssembler().clearTerms(getDescendantBinColumn());
        return;
    }


    /**
     *  Gets the descendant bin query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.BinQueryInspector[] getDescendantBinTerms() {
        return (new org.osid.resource.BinQueryInspector[0]);
    }


    /**
     *  Gets the DescendantBin column name.
     *
     * @return the column name
     */

    protected String getDescendantBinColumn() {
        return ("descendant_bin");
    }


    /**
     *  Tests if this bin supports the given record
     *  <code>Type</code>.
     *
     *  @param  binRecordType a bin record type 
     *  @return <code>true</code> if the binRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>binRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type binRecordType) {
        for (org.osid.resource.records.BinQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(binRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  binRecordType the bin record type 
     *  @return the bin query record 
     *  @throws org.osid.NullArgumentException
     *          <code>binRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(binRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resource.records.BinQueryRecord getBinQueryRecord(org.osid.type.Type binRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resource.records.BinQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(binRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(binRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  binRecordType the bin record type 
     *  @return the bin query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>binRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(binRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resource.records.BinQueryInspectorRecord getBinQueryInspectorRecord(org.osid.type.Type binRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resource.records.BinQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(binRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(binRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param binRecordType the bin record type
     *  @return the bin search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>binRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(binRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resource.records.BinSearchOrderRecord getBinSearchOrderRecord(org.osid.type.Type binRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resource.records.BinSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(binRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(binRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this bin. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param binQueryRecord the bin query record
     *  @param binQueryInspectorRecord the bin query inspector
     *         record
     *  @param binSearchOrderRecord the bin search order record
     *  @param binRecordType bin record type
     *  @throws org.osid.NullArgumentException
     *          <code>binQueryRecord</code>,
     *          <code>binQueryInspectorRecord</code>,
     *          <code>binSearchOrderRecord</code> or
     *          <code>binRecordTypebin</code> is
     *          <code>null</code>
     */
            
    protected void addBinRecords(org.osid.resource.records.BinQueryRecord binQueryRecord, 
                                      org.osid.resource.records.BinQueryInspectorRecord binQueryInspectorRecord, 
                                      org.osid.resource.records.BinSearchOrderRecord binSearchOrderRecord, 
                                      org.osid.type.Type binRecordType) {

        addRecordType(binRecordType);

        nullarg(binQueryRecord, "bin query record");
        nullarg(binQueryInspectorRecord, "bin query inspector record");
        nullarg(binSearchOrderRecord, "bin search odrer record");

        this.queryRecords.add(binQueryRecord);
        this.queryInspectorRecords.add(binQueryInspectorRecord);
        this.searchOrderRecords.add(binSearchOrderRecord);
        
        return;
    }
}
