//
// AbstractEngineSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.search.engine.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractEngineSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.search.EngineSearchResults {

    private org.osid.search.EngineList engines;
    private final org.osid.search.EngineQueryInspector inspector;
    private final java.util.Collection<org.osid.search.records.EngineSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractEngineSearchResults.
     *
     *  @param engines the result set
     *  @param engineQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>engines</code>
     *          or <code>engineQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractEngineSearchResults(org.osid.search.EngineList engines,
                                            org.osid.search.EngineQueryInspector engineQueryInspector) {
        nullarg(engines, "engines");
        nullarg(engineQueryInspector, "engine query inspectpr");

        this.engines = engines;
        this.inspector = engineQueryInspector;

        return;
    }


    /**
     *  Gets the engine list resulting from a search.
     *
     *  @return an engine list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.search.EngineList getEngines() {
        if (this.engines == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.search.EngineList engines = this.engines;
        this.engines = null;
	return (engines);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.search.EngineQueryInspector getEngineQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  engine search record <code> Type. </code> This method must
     *  be used to retrieve an engine implementing the requested
     *  record.
     *
     *  @param engineSearchRecordType an engine search 
     *         record type 
     *  @return the engine search
     *  @throws org.osid.NullArgumentException
     *          <code>engineSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(engineSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.search.records.EngineSearchResultsRecord getEngineSearchResultsRecord(org.osid.type.Type engineSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.search.records.EngineSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(engineSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(engineSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record engine search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addEngineRecord(org.osid.search.records.EngineSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "engine record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
