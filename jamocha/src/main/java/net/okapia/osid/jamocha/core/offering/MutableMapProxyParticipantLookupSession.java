//
// MutableMapProxyParticipantLookupSession
//
//    Implements a Participant lookup service backed by a collection of
//    participants that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.offering;


/**
 *  Implements a Participant lookup service backed by a collection of
 *  participants. The participants are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of participants can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyParticipantLookupSession
    extends net.okapia.osid.jamocha.core.offering.spi.AbstractMapParticipantLookupSession
    implements org.osid.offering.ParticipantLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyParticipantLookupSession}
     *  with no participants.
     *
     *  @param catalogue the catalogue
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code catalogue} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyParticipantLookupSession(org.osid.offering.Catalogue catalogue,
                                                  org.osid.proxy.Proxy proxy) {
        setCatalogue(catalogue);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyParticipantLookupSession} with a
     *  single participant.
     *
     *  @param catalogue the catalogue
     *  @param participant a participant
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code catalogue},
     *          {@code participant}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyParticipantLookupSession(org.osid.offering.Catalogue catalogue,
                                                org.osid.offering.Participant participant, org.osid.proxy.Proxy proxy) {
        this(catalogue, proxy);
        putParticipant(participant);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyParticipantLookupSession} using an
     *  array of participants.
     *
     *  @param catalogue the catalogue
     *  @param participants an array of participants
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code catalogue},
     *          {@code participants}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyParticipantLookupSession(org.osid.offering.Catalogue catalogue,
                                                org.osid.offering.Participant[] participants, org.osid.proxy.Proxy proxy) {
        this(catalogue, proxy);
        putParticipants(participants);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyParticipantLookupSession} using a
     *  collection of participants.
     *
     *  @param catalogue the catalogue
     *  @param participants a collection of participants
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code catalogue},
     *          {@code participants}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyParticipantLookupSession(org.osid.offering.Catalogue catalogue,
                                                java.util.Collection<? extends org.osid.offering.Participant> participants,
                                                org.osid.proxy.Proxy proxy) {
   
        this(catalogue, proxy);
        setSessionProxy(proxy);
        putParticipants(participants);
        return;
    }

    
    /**
     *  Makes a {@code Participant} available in this session.
     *
     *  @param participant an participant
     *  @throws org.osid.NullArgumentException {@code participant{@code 
     *          is {@code null}
     */

    @Override
    public void putParticipant(org.osid.offering.Participant participant) {
        super.putParticipant(participant);
        return;
    }


    /**
     *  Makes an array of participants available in this session.
     *
     *  @param participants an array of participants
     *  @throws org.osid.NullArgumentException {@code participants{@code 
     *          is {@code null}
     */

    @Override
    public void putParticipants(org.osid.offering.Participant[] participants) {
        super.putParticipants(participants);
        return;
    }


    /**
     *  Makes collection of participants available in this session.
     *
     *  @param participants
     *  @throws org.osid.NullArgumentException {@code participant{@code 
     *          is {@code null}
     */

    @Override
    public void putParticipants(java.util.Collection<? extends org.osid.offering.Participant> participants) {
        super.putParticipants(participants);
        return;
    }


    /**
     *  Removes a Participant from this session.
     *
     *  @param participantId the {@code Id} of the participant
     *  @throws org.osid.NullArgumentException {@code participantId{@code  is
     *          {@code null}
     */

    @Override
    public void removeParticipant(org.osid.id.Id participantId) {
        super.removeParticipant(participantId);
        return;
    }    
}
