//
// AbstractAssemblyFiscalPeriodQuery.java
//
//     A FiscalPeriodQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.financials.fiscalperiod.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A FiscalPeriodQuery that stores terms.
 */

public abstract class AbstractAssemblyFiscalPeriodQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.financials.FiscalPeriodQuery,
               org.osid.financials.FiscalPeriodQueryInspector,
               org.osid.financials.FiscalPeriodSearchOrder {

    private final java.util.Collection<org.osid.financials.records.FiscalPeriodQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.financials.records.FiscalPeriodQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.financials.records.FiscalPeriodSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyFiscalPeriodQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyFiscalPeriodQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Adds a display label for this query. 
     *
     *  @param  label label string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> label </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> label </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchDisplayLabel(String label, 
                                  org.osid.type.Type stringMatchType, 
                                  boolean match) {
        getAssembler().addStringTerm(getDisplayLabelColumn(), label, stringMatchType, match);
        return;
    }


    /**
     *  Matches a display label that has any value. 
     *
     *  @param  match <code> true </code> to match fiscal periods with any 
     *          display label, <code> false </code> to match fiscal periods 
     *          with no display label 
     */

    @OSID @Override
    public void matchAnyDisplayLabel(boolean match) {
        getAssembler().addStringWildcardTerm(getDisplayLabelColumn(), match);
        return;
    }


    /**
     *  Clears the display label terms. 
     */

    @OSID @Override
    public void clearDisplayLabelTerms() {
        getAssembler().clearTerms(getDisplayLabelColumn());
        return;
    }


    /**
     *  Gets the display label query terms. 
     *
     *  @return the display label terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getDisplayLabelTerms() {
        return (getAssembler().getStringTerms(getDisplayLabelColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by display label. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDisplayLabel(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDisplayLabelColumn(), style);
        return;
    }


    /**
     *  Gets the DisplayLabel column name.
     *
     * @return the column name
     */

    protected String getDisplayLabelColumn() {
        return ("display_label");
    }


    /**
     *  Adds a fiscal year for this query to match periods in the given fiscal 
     *  years inclusive. 
     *
     *  @param  from start of year range 
     *  @param  to end of year range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     */

    @OSID @Override
    public void matchFiscalYear(long from, long to, boolean match) {
        getAssembler().addIntegerRangeTerm(getFiscalYearColumn(), from, to, match);
        return;
    }


    /**
     *  Matches a fiscal year that has any value. 
     *
     *  @param  match <code> true </code> to match fiscal periods with any 
     *          fiscal year, <code> false </code> to match fiscal periods with 
     *          no fiscal year 
     */

    @OSID @Override
    public void matchAnyFiscalYear(boolean match) {
        getAssembler().addIntegerRangeWildcardTerm(getFiscalYearColumn(), match);
        return;
    }


    /**
     *  Clears the fiscal year terms. 
     */

    @OSID @Override
    public void clearFiscalYearTerms() {
        getAssembler().clearTerms(getFiscalYearColumn());
        return;
    }


    /**
     *  Gets the fiscal year query terms. 
     *
     *  @return the fiscal year terms 
     */

    @OSID @Override
    public org.osid.search.terms.IntegerRangeTerm[] getFiscalYearTerms() {
        return (getAssembler().getIntegerRangeTerms(getFiscalYearColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by year. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByFiscalYear(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getFiscalYearColumn(), style);
        return;
    }


    /**
     *  Gets the FiscalYear column name.
     *
     * @return the column name
     */

    protected String getFiscalYearColumn() {
        return ("fiscal_year");
    }


    /**
     *  Matches a start date within the given date range inclusive. 
     *
     *  @param  from start of range 
     *  @param  to end of range 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchStartDate(org.osid.calendaring.DateTime from, 
                               org.osid.calendaring.DateTime to, boolean match) {
        getAssembler().addDateTimeRangeTerm(getStartDateColumn(), from, to, match);
        return;
    }


    /**
     *  Matches a start date that has any value. 
     *
     *  @param  match <code> true </code> to match fiscal periods with any 
     *          start date, <code> false </code> to match fiscal periods with 
     *          no start date 
     */

    @OSID @Override
    public void matchAnyStartDate(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getStartDateColumn(), match);
        return;
    }


    /**
     *  Clears the start date terms. 
     */

    @OSID @Override
    public void clearStartDateTerms() {
        getAssembler().clearTerms(getStartDateColumn());
        return;
    }


    /**
     *  Gets the start date query terms. 
     *
     *  @return the start date query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getStartDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getStartDateColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by fiscal period 
     *  start date. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStartDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getStartDateColumn(), style);
        return;
    }


    /**
     *  Gets the StartDate column name.
     *
     * @return the column name
     */

    protected String getStartDateColumn() {
        return ("start_date");
    }


    /**
     *  Matches an end date within the given date range inclusive. 
     *
     *  @param  from start of range 
     *  @param  to end of range 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchEndDate(org.osid.calendaring.DateTime from, 
                             org.osid.calendaring.DateTime to, boolean match) {
        getAssembler().addDateTimeRangeTerm(getEndDateColumn(), from, to, match);
        return;
    }


    /**
     *  Matches an end date that has any value. 
     *
     *  @param  match <code> true </code> to match fiscal periods with any end 
     *          date, <code> false </code> to match fiscal periods with no end 
     *          date 
     */

    @OSID @Override
    public void matchAnyEndDate(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getEndDateColumn(), match);
        return;
    }


    /**
     *  Clears the end date terms. 
     */

    @OSID @Override
    public void clearEndDateTerms() {
        getAssembler().clearTerms(getEndDateColumn());
        return;
    }


    /**
     *  Gets the end date query terms. 
     *
     *  @return the end date query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getEndDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getEndDateColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by fiscal period 
     *  end date. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByEndDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getEndDateColumn(), style);
        return;
    }


    /**
     *  Gets the EndDate column name.
     *
     * @return the column name
     */

    protected String getEndDateColumn() {
        return ("end_date");
    }


    /**
     *  Matches a fiscal period duratione within the given date range 
     *  inclusive. 
     *
     *  @param  from start of range 
     *  @param  to end of range 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDuration(org.osid.calendaring.Duration from, 
                              org.osid.calendaring.Duration to, boolean match) {
        getAssembler().addDurationRangeTerm(getDurationColumn(), from, to, match);
        return;
    }


    /**
     *  Clears the duration terms. 
     */

    @OSID @Override
    public void clearDurationTerms() {
        getAssembler().clearTerms(getDurationColumn());
        return;
    }


    /**
     *  Gets the duration query terms. 
     *
     *  @return the duration query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getDurationTerms() {
        return (getAssembler().getDurationRangeTerms(getDurationColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by duration. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDuration(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDurationColumn(), style);
        return;
    }


    /**
     *  Gets the Duration column name.
     *
     * @return the column name
     */

    protected String getDurationColumn() {
        return ("duration");
    }


    /**
     *  Matches a budget deadline within the given date range inclusive. 
     *
     *  @param  from start of range 
     *  @param  to end of range 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchBudgetDeadline(org.osid.calendaring.DateTime from, 
                                    org.osid.calendaring.DateTime to, 
                                    boolean match) {
        getAssembler().addDateTimeRangeTerm(getBudgetDeadlineColumn(), from, to, match);
        return;
    }


    /**
     *  Matches a budget deadline that has any value. 
     *
     *  @param  match <code> true </code> to match fiscal periods with any 
     *          budget deadline, <code> false </code> to match fiscal periods 
     *          with no budget deadline 
     */

    @OSID @Override
    public void matchAnyBudgetDeadline(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getBudgetDeadlineColumn(), match);
        return;
    }


    /**
     *  Clears the budget deadline terms. 
     */

    @OSID @Override
    public void clearBudgetDeadlineTerms() {
        getAssembler().clearTerms(getBudgetDeadlineColumn());
        return;
    }


    /**
     *  Gets the budget deadline query terms. 
     *
     *  @return the budget deadline query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getBudgetDeadlineTerms() {
        return (getAssembler().getDateTimeRangeTerms(getBudgetDeadlineColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the budget 
     *  deadline. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBudgetDeadline(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getBudgetDeadlineColumn(), style);
        return;
    }


    /**
     *  Gets the BudgetDeadline column name.
     *
     * @return the column name
     */

    protected String getBudgetDeadlineColumn() {
        return ("budget_deadline");
    }


    /**
     *  Matches a posting deadline within the given date range inclusive. 
     *
     *  @param  from start of range 
     *  @param  to end of range 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchPostingDeadline(org.osid.calendaring.DateTime from, 
                                     org.osid.calendaring.DateTime to, 
                                     boolean match) {
        getAssembler().addDateTimeRangeTerm(getPostingDeadlineColumn(), from, to, match);
        return;
    }


    /**
     *  Matches a posting deadline that has any value. 
     *
     *  @param  match <code> true </code> to match fiscal periods with any 
     *          posting deadline, <code> false </code> to match fiscal periods 
     *          with no posting deadline 
     */

    @OSID @Override
    public void matchAnyPostingDeadline(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getPostingDeadlineColumn(), match);
        return;
    }


    /**
     *  Clears the posting deadline terms. 
     */

    @OSID @Override
    public void clearPostingDeadlineTerms() {
        getAssembler().clearTerms(getPostingDeadlineColumn());
        return;
    }


    /**
     *  Gets the posting deadline query terms. 
     *
     *  @return the posting deadline query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getPostingDeadlineTerms() {
        return (getAssembler().getDateTimeRangeTerms(getPostingDeadlineColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the posting 
     *  deadline. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPostingDeadline(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getPostingDeadlineColumn(), style);
        return;
    }


    /**
     *  Gets the PostingDeadline column name.
     *
     * @return the column name
     */

    protected String getPostingDeadlineColumn() {
        return ("posting_deadline");
    }


    /**
     *  Matches a closing date within the given date range inclusive. 
     *
     *  @param  from start of range 
     *  @param  to end of range 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchClosing(org.osid.calendaring.DateTime from, 
                             org.osid.calendaring.DateTime to, boolean match) {
        getAssembler().addDateTimeRangeTerm(getClosingColumn(), from, to, match);
        return;
    }


    /**
     *  Matches a closing date that has any value. 
     *
     *  @param  match <code> true </code> to match fiscal periods with any 
     *          closing date, <code> false </code> to match fiscal periods 
     *          with no closing date 
     */

    @OSID @Override
    public void matchAnyClosing(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getClosingColumn(), match);
        return;
    }


    /**
     *  Clears the closing terms. 
     */

    @OSID @Override
    public void clearClosingTerms() {
        getAssembler().clearTerms(getClosingColumn());
        return;
    }


    /**
     *  Gets the closing query terms. 
     *
     *  @return the closing query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getClosingTerms() {
        return (getAssembler().getDateTimeRangeTerms(getClosingColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the closing 
     *  date. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByClosing(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getClosingColumn(), style);
        return;
    }


    /**
     *  Gets the Closing column name.
     *
     * @return the column name
     */

    protected String getClosingColumn() {
        return ("closing");
    }


    /**
     *  Sets the business <code> Id </code> for this query to match fiscal 
     *  periods assigned to businesses. 
     *
     *  @param  businessId the business <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBusinessId(org.osid.id.Id businessId, boolean match) {
        getAssembler().addIdTerm(getBusinessIdColumn(), businessId, match);
        return;
    }


    /**
     *  Clears the business <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBusinessIdTerms() {
        getAssembler().clearTerms(getBusinessIdColumn());
        return;
    }


    /**
     *  Gets the business <code> Id </code> query terms. 
     *
     *  @return the business <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBusinessIdTerms() {
        return (getAssembler().getIdTerms(getBusinessIdColumn()));
    }


    /**
     *  Gets the BusinessId column name.
     *
     * @return the column name
     */

    protected String getBusinessIdColumn() {
        return ("business_id");
    }


    /**
     *  Tests if a <code> BusinessQuery </code> is available. 
     *
     *  @return <code> true </code> if a business query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a business. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the business query 
     *  @throws org.osid.UnimplementedException <code> supportsBusinessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.BusinessQuery getBusinessQuery() {
        throw new org.osid.UnimplementedException("supportsBusinessQuery() is false");
    }


    /**
     *  Clears the business terms. 
     */

    @OSID @Override
    public void clearBusinessTerms() {
        getAssembler().clearTerms(getBusinessColumn());
        return;
    }


    /**
     *  Gets the business query terms. 
     *
     *  @return the business query terms 
     */

    @OSID @Override
    public org.osid.financials.BusinessQueryInspector[] getBusinessTerms() {
        return (new org.osid.financials.BusinessQueryInspector[0]);
    }


    /**
     *  Gets the Business column name.
     *
     * @return the column name
     */

    protected String getBusinessColumn() {
        return ("business");
    }


    /**
     *  Tests if this fiscalPeriod supports the given record
     *  <code>Type</code>.
     *
     *  @param  fiscalPeriodRecordType a fiscal period record type 
     *  @return <code>true</code> if the fiscalPeriodRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>fiscalPeriodRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type fiscalPeriodRecordType) {
        for (org.osid.financials.records.FiscalPeriodQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(fiscalPeriodRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  fiscalPeriodRecordType the fiscal period record type 
     *  @return the fiscal period query record 
     *  @throws org.osid.NullArgumentException
     *          <code>fiscalPeriodRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(fiscalPeriodRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.records.FiscalPeriodQueryRecord getFiscalPeriodQueryRecord(org.osid.type.Type fiscalPeriodRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.records.FiscalPeriodQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(fiscalPeriodRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(fiscalPeriodRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  fiscalPeriodRecordType the fiscal period record type 
     *  @return the fiscal period query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>fiscalPeriodRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(fiscalPeriodRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.records.FiscalPeriodQueryInspectorRecord getFiscalPeriodQueryInspectorRecord(org.osid.type.Type fiscalPeriodRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.records.FiscalPeriodQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(fiscalPeriodRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(fiscalPeriodRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param fiscalPeriodRecordType the fiscal period record type
     *  @return the fiscal period search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>fiscalPeriodRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(fiscalPeriodRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.records.FiscalPeriodSearchOrderRecord getFiscalPeriodSearchOrderRecord(org.osid.type.Type fiscalPeriodRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.records.FiscalPeriodSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(fiscalPeriodRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(fiscalPeriodRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this fiscal period. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param fiscalPeriodQueryRecord the fiscal period query record
     *  @param fiscalPeriodQueryInspectorRecord the fiscal period query inspector
     *         record
     *  @param fiscalPeriodSearchOrderRecord the fiscal period search order record
     *  @param fiscalPeriodRecordType fiscal period record type
     *  @throws org.osid.NullArgumentException
     *          <code>fiscalPeriodQueryRecord</code>,
     *          <code>fiscalPeriodQueryInspectorRecord</code>,
     *          <code>fiscalPeriodSearchOrderRecord</code> or
     *          <code>fiscalPeriodRecordTypefiscalPeriod</code> is
     *          <code>null</code>
     */
            
    protected void addFiscalPeriodRecords(org.osid.financials.records.FiscalPeriodQueryRecord fiscalPeriodQueryRecord, 
                                      org.osid.financials.records.FiscalPeriodQueryInspectorRecord fiscalPeriodQueryInspectorRecord, 
                                      org.osid.financials.records.FiscalPeriodSearchOrderRecord fiscalPeriodSearchOrderRecord, 
                                      org.osid.type.Type fiscalPeriodRecordType) {

        addRecordType(fiscalPeriodRecordType);

        nullarg(fiscalPeriodQueryRecord, "fiscal period query record");
        nullarg(fiscalPeriodQueryInspectorRecord, "fiscal period query inspector record");
        nullarg(fiscalPeriodSearchOrderRecord, "fiscal period search odrer record");

        this.queryRecords.add(fiscalPeriodQueryRecord);
        this.queryInspectorRecords.add(fiscalPeriodQueryInspectorRecord);
        this.searchOrderRecords.add(fiscalPeriodSearchOrderRecord);
        
        return;
    }
}
