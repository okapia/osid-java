//
// AbstractEdgeEnablerSearch.java
//
//     A template for making an EdgeEnabler Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.topology.rules.edgeenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing edge enabler searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractEdgeEnablerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.topology.rules.EdgeEnablerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.topology.rules.records.EdgeEnablerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.topology.rules.EdgeEnablerSearchOrder edgeEnablerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of edge enablers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  edgeEnablerIds list of edge enablers
     *  @throws org.osid.NullArgumentException
     *          <code>edgeEnablerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongEdgeEnablers(org.osid.id.IdList edgeEnablerIds) {
        while (edgeEnablerIds.hasNext()) {
            try {
                this.ids.add(edgeEnablerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongEdgeEnablers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of edge enabler Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getEdgeEnablerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  edgeEnablerSearchOrder edge enabler search order 
     *  @throws org.osid.NullArgumentException
     *          <code>edgeEnablerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>edgeEnablerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderEdgeEnablerResults(org.osid.topology.rules.EdgeEnablerSearchOrder edgeEnablerSearchOrder) {
	this.edgeEnablerSearchOrder = edgeEnablerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.topology.rules.EdgeEnablerSearchOrder getEdgeEnablerSearchOrder() {
	return (this.edgeEnablerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given edge enabler search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an edge enabler implementing the requested record.
     *
     *  @param edgeEnablerSearchRecordType an edge enabler search record
     *         type
     *  @return the edge enabler search record
     *  @throws org.osid.NullArgumentException
     *          <code>edgeEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(edgeEnablerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.topology.rules.records.EdgeEnablerSearchRecord getEdgeEnablerSearchRecord(org.osid.type.Type edgeEnablerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.topology.rules.records.EdgeEnablerSearchRecord record : this.records) {
            if (record.implementsRecordType(edgeEnablerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(edgeEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this edge enabler search. 
     *
     *  @param edgeEnablerSearchRecord edge enabler search record
     *  @param edgeEnablerSearchRecordType edgeEnabler search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addEdgeEnablerSearchRecord(org.osid.topology.rules.records.EdgeEnablerSearchRecord edgeEnablerSearchRecord, 
                                           org.osid.type.Type edgeEnablerSearchRecordType) {

        addRecordType(edgeEnablerSearchRecordType);
        this.records.add(edgeEnablerSearchRecord);        
        return;
    }
}
