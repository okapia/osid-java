//
// JournalEntryMiter.java
//
//     Defines a JournalEntry miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.journaling.journalentry;


/**
 *  Defines a <code>JournalEntry</code> miter for use with the builders.
 */

public interface JournalEntryMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.journaling.JournalEntry {


    /**
     *  Sets the branch.
     *
     *  @param branch a branch
     *  @throws org.osid.NullArgumentException <code>branch</code> is
     *          <code>null</code>
     */

    public void setBranch(org.osid.journaling.Branch branch);


    /**
     *  Sets the source id.
     *
     *  @param sourceId a source id
     *  @throws org.osid.NullArgumentException <code>sourceId</code>
     *          is <code>null</code>
     */

    public void setSourceId(org.osid.id.Id sourceId);


    /**
     *  Sets the version id.
     *
     *  @param versionId a version id
     *  @throws org.osid.NullArgumentException <code>versionId</code>
     *          is <code>null</code>
     */

    public void setVersionId(org.osid.id.Id versionId);


    /**
     *  Sets the timestamp.
     *
     *  @param timestamp a timestamp
     *  @throws org.osid.NullArgumentException <code>timestamp</code>
     *          is <code>null</code>
     */

    public void setTimestamp(org.osid.calendaring.DateTime timestamp);


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    public void setResource(org.osid.resource.Resource resource);


    /**
     *  Sets the agent.
     *
     *  @param agent an agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public void setAgent(org.osid.authentication.Agent agent);


    /**
     *  Adds a JournalEntry record.
     *
     *  @param record a journalEntry record
     *  @param recordType the type of journalEntry record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addJournalEntryRecord(org.osid.journaling.records.JournalEntryRecord record, org.osid.type.Type recordType);
}       


