//
// InvariantMapCyclicEventLookupSession
//
//    Implements a CyclicEvent lookup service backed by a fixed collection of
//    cyclicEvents.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.cycle;


/**
 *  Implements a CyclicEvent lookup service backed by a fixed
 *  collection of cyclic events. The cyclic events are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapCyclicEventLookupSession
    extends net.okapia.osid.jamocha.core.calendaring.cycle.spi.AbstractMapCyclicEventLookupSession
    implements org.osid.calendaring.cycle.CyclicEventLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapCyclicEventLookupSession</code> with no
     *  cyclic events.
     *  
     *  @param calendar the calendar
     *  @throws org.osid.NullArgumnetException {@code calendar} is
     *          {@code null}
     */

    public InvariantMapCyclicEventLookupSession(org.osid.calendaring.Calendar calendar) {
        setCalendar(calendar);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapCyclicEventLookupSession</code> with a single
     *  cyclic event.
     *  
     *  @param calendar the calendar
     *  @param cyclicEvent a single cyclic event
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code cyclicEvent} is <code>null</code>
     */

      public InvariantMapCyclicEventLookupSession(org.osid.calendaring.Calendar calendar,
                                               org.osid.calendaring.cycle.CyclicEvent cyclicEvent) {
        this(calendar);
        putCyclicEvent(cyclicEvent);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapCyclicEventLookupSession</code> using an array
     *  of cyclic events.
     *  
     *  @param calendar the calendar
     *  @param cyclicEvents an array of cyclic events
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code cyclicEvents} is <code>null</code>
     */

      public InvariantMapCyclicEventLookupSession(org.osid.calendaring.Calendar calendar,
                                               org.osid.calendaring.cycle.CyclicEvent[] cyclicEvents) {
        this(calendar);
        putCyclicEvents(cyclicEvents);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapCyclicEventLookupSession</code> using a
     *  collection of cyclic events.
     *
     *  @param calendar the calendar
     *  @param cyclicEvents a collection of cyclic events
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code cyclicEvents} is <code>null</code>
     */

      public InvariantMapCyclicEventLookupSession(org.osid.calendaring.Calendar calendar,
                                               java.util.Collection<? extends org.osid.calendaring.cycle.CyclicEvent> cyclicEvents) {
        this(calendar);
        putCyclicEvents(cyclicEvents);
        return;
    }
}
