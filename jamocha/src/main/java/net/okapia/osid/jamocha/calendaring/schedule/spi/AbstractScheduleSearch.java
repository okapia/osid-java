//
// AbstractScheduleSearch.java
//
//     A template for making a Schedule Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.schedule.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing schedule searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractScheduleSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.calendaring.ScheduleSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.calendaring.records.ScheduleSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.calendaring.ScheduleSearchOrder scheduleSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of schedules. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  scheduleIds list of schedules
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongSchedules(org.osid.id.IdList scheduleIds) {
        while (scheduleIds.hasNext()) {
            try {
                this.ids.add(scheduleIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongSchedules</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of schedule Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getScheduleIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  scheduleSearchOrder schedule search order 
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>scheduleSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderScheduleResults(org.osid.calendaring.ScheduleSearchOrder scheduleSearchOrder) {
	this.scheduleSearchOrder = scheduleSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.calendaring.ScheduleSearchOrder getScheduleSearchOrder() {
	return (this.scheduleSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given schedule search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a schedule implementing the requested record.
     *
     *  @param scheduleSearchRecordType a schedule search record
     *         type
     *  @return the schedule search record
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(scheduleSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.ScheduleSearchRecord getScheduleSearchRecord(org.osid.type.Type scheduleSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.calendaring.records.ScheduleSearchRecord record : this.records) {
            if (record.implementsRecordType(scheduleSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(scheduleSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this schedule search. 
     *
     *  @param scheduleSearchRecord schedule search record
     *  @param scheduleSearchRecordType schedule search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addScheduleSearchRecord(org.osid.calendaring.records.ScheduleSearchRecord scheduleSearchRecord, 
                                           org.osid.type.Type scheduleSearchRecordType) {

        addRecordType(scheduleSearchRecordType);
        this.records.add(scheduleSearchRecord);        
        return;
    }
}
