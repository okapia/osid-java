//
// AbstractAssemblyDemographicEnablerQuery.java
//
//     A DemographicEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.resource.demographic.demographicenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A DemographicEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblyDemographicEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.resource.demographic.DemographicEnablerQuery,
               org.osid.resource.demographic.DemographicEnablerQueryInspector,
               org.osid.resource.demographic.DemographicEnablerSearchOrder {

    private final java.util.Collection<org.osid.resource.demographic.records.DemographicEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resource.demographic.records.DemographicEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resource.demographic.records.DemographicEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyDemographicEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyDemographicEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to the demographic. 
     *
     *  @param  demographicId the demographic <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> demographicId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledDemographicId(org.osid.id.Id demographicId, 
                                        boolean match) {
        getAssembler().addIdTerm(getRuledDemographicIdColumn(), demographicId, match);
        return;
    }


    /**
     *  Clears the demographic <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledDemographicIdTerms() {
        getAssembler().clearTerms(getRuledDemographicIdColumn());
        return;
    }


    /**
     *  Gets the demographic <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledDemographicIdTerms() {
        return (getAssembler().getIdTerms(getRuledDemographicIdColumn()));
    }


    /**
     *  Gets the RuledDemographicId column name.
     *
     * @return the column name
     */

    protected String getRuledDemographicIdColumn() {
        return ("ruled_demographic_id");
    }


    /**
     *  Tests if a <code> DemographicQuery </code> is available. 
     *
     *  @return <code> true </code> if a demographic query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledDemographicQuery() {
        return (false);
    }


    /**
     *  Gets the query for a demographic. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the demographic query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledDemographicQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicQuery getRuledDemographicQuery() {
        throw new org.osid.UnimplementedException("supportsRuledDemographicQuery() is false");
    }


    /**
     *  Matches enablers mapped to any demographic. 
     *
     *  @param  match <code> true </code> for enablers mapped to any 
     *          demographic, <code> false </code> to match enablers mapped to 
     *          no demographics 
     */

    @OSID @Override
    public void matchAnyRuledDemographic(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledDemographicColumn(), match);
        return;
    }


    /**
     *  Clears the demographic query terms. 
     */

    @OSID @Override
    public void clearRuledDemographicTerms() {
        getAssembler().clearTerms(getRuledDemographicColumn());
        return;
    }


    /**
     *  Gets the demographic query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicQueryInspector[] getRuledDemographicTerms() {
        return (new org.osid.resource.demographic.DemographicQueryInspector[0]);
    }


    /**
     *  Gets the RuledDemographic column name.
     *
     * @return the column name
     */

    protected String getRuledDemographicColumn() {
        return ("ruled_demographic");
    }


    /**
     *  Matches enablers mapped to the bin. 
     *
     *  @param  binId the bin <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchBinId(org.osid.id.Id binId, boolean match) {
        getAssembler().addIdTerm(getBinIdColumn(), binId, match);
        return;
    }


    /**
     *  Clears the bin <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearBinIdTerms() {
        getAssembler().clearTerms(getBinIdColumn());
        return;
    }


    /**
     *  Gets the bin <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBinIdTerms() {
        return (getAssembler().getIdTerms(getBinIdColumn()));
    }


    /**
     *  Gets the BinId column name.
     *
     * @return the column name
     */

    protected String getBinIdColumn() {
        return ("bin_id");
    }


    /**
     *  Tests if a <code> BinQuery </code> is available. 
     *
     *  @return <code> true </code> if a bin query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBinQuery() {
        return (false);
    }


    /**
     *  Gets the query for a bin. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the bin query 
     *  @throws org.osid.UnimplementedException <code> supportsBinQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.BinQuery getBinQuery() {
        throw new org.osid.UnimplementedException("supportsBinQuery() is false");
    }


    /**
     *  Clears the bin query terms. 
     */

    @OSID @Override
    public void clearBinTerms() {
        getAssembler().clearTerms(getBinColumn());
        return;
    }


    /**
     *  Gets the bin query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.BinQueryInspector[] getBinTerms() {
        return (new org.osid.resource.BinQueryInspector[0]);
    }


    /**
     *  Gets the Bin column name.
     *
     * @return the column name
     */

    protected String getBinColumn() {
        return ("bin");
    }


    /**
     *  Tests if this demographicEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  demographicEnablerRecordType a demographic enabler record type 
     *  @return <code>true</code> if the demographicEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>demographicEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type demographicEnablerRecordType) {
        for (org.osid.resource.demographic.records.DemographicEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(demographicEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  demographicEnablerRecordType the demographic enabler record type 
     *  @return the demographic enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>demographicEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(demographicEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resource.demographic.records.DemographicEnablerQueryRecord getDemographicEnablerQueryRecord(org.osid.type.Type demographicEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resource.demographic.records.DemographicEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(demographicEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(demographicEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  demographicEnablerRecordType the demographic enabler record type 
     *  @return the demographic enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>demographicEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(demographicEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resource.demographic.records.DemographicEnablerQueryInspectorRecord getDemographicEnablerQueryInspectorRecord(org.osid.type.Type demographicEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resource.demographic.records.DemographicEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(demographicEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(demographicEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param demographicEnablerRecordType the demographic enabler record type
     *  @return the demographic enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>demographicEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(demographicEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resource.demographic.records.DemographicEnablerSearchOrderRecord getDemographicEnablerSearchOrderRecord(org.osid.type.Type demographicEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resource.demographic.records.DemographicEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(demographicEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(demographicEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this demographic enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param demographicEnablerQueryRecord the demographic enabler query record
     *  @param demographicEnablerQueryInspectorRecord the demographic enabler query inspector
     *         record
     *  @param demographicEnablerSearchOrderRecord the demographic enabler search order record
     *  @param demographicEnablerRecordType demographic enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>demographicEnablerQueryRecord</code>,
     *          <code>demographicEnablerQueryInspectorRecord</code>,
     *          <code>demographicEnablerSearchOrderRecord</code> or
     *          <code>demographicEnablerRecordTypedemographicEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addDemographicEnablerRecords(org.osid.resource.demographic.records.DemographicEnablerQueryRecord demographicEnablerQueryRecord, 
                                      org.osid.resource.demographic.records.DemographicEnablerQueryInspectorRecord demographicEnablerQueryInspectorRecord, 
                                      org.osid.resource.demographic.records.DemographicEnablerSearchOrderRecord demographicEnablerSearchOrderRecord, 
                                      org.osid.type.Type demographicEnablerRecordType) {

        addRecordType(demographicEnablerRecordType);

        nullarg(demographicEnablerQueryRecord, "demographic enabler query record");
        nullarg(demographicEnablerQueryInspectorRecord, "demographic enabler query inspector record");
        nullarg(demographicEnablerSearchOrderRecord, "demographic enabler search odrer record");

        this.queryRecords.add(demographicEnablerQueryRecord);
        this.queryInspectorRecords.add(demographicEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(demographicEnablerSearchOrderRecord);
        
        return;
    }
}
