//
// AbstractMapTriggerLookupSession
//
//    A simple framework for providing a Trigger lookup service
//    backed by a fixed collection of triggers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Trigger lookup service backed by a
 *  fixed collection of triggers. The triggers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Triggers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapTriggerLookupSession
    extends net.okapia.osid.jamocha.control.spi.AbstractTriggerLookupSession
    implements org.osid.control.TriggerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.control.Trigger> triggers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.control.Trigger>());


    /**
     *  Makes a <code>Trigger</code> available in this session.
     *
     *  @param  trigger a trigger
     *  @throws org.osid.NullArgumentException <code>trigger<code>
     *          is <code>null</code>
     */

    protected void putTrigger(org.osid.control.Trigger trigger) {
        this.triggers.put(trigger.getId(), trigger);
        return;
    }


    /**
     *  Makes an array of triggers available in this session.
     *
     *  @param  triggers an array of triggers
     *  @throws org.osid.NullArgumentException <code>triggers<code>
     *          is <code>null</code>
     */

    protected void putTriggers(org.osid.control.Trigger[] triggers) {
        putTriggers(java.util.Arrays.asList(triggers));
        return;
    }


    /**
     *  Makes a collection of triggers available in this session.
     *
     *  @param  triggers a collection of triggers
     *  @throws org.osid.NullArgumentException <code>triggers<code>
     *          is <code>null</code>
     */

    protected void putTriggers(java.util.Collection<? extends org.osid.control.Trigger> triggers) {
        for (org.osid.control.Trigger trigger : triggers) {
            this.triggers.put(trigger.getId(), trigger);
        }

        return;
    }


    /**
     *  Removes a Trigger from this session.
     *
     *  @param  triggerId the <code>Id</code> of the trigger
     *  @throws org.osid.NullArgumentException <code>triggerId<code> is
     *          <code>null</code>
     */

    protected void removeTrigger(org.osid.id.Id triggerId) {
        this.triggers.remove(triggerId);
        return;
    }


    /**
     *  Gets the <code>Trigger</code> specified by its <code>Id</code>.
     *
     *  @param  triggerId <code>Id</code> of the <code>Trigger</code>
     *  @return the trigger
     *  @throws org.osid.NotFoundException <code>triggerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>triggerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.Trigger getTrigger(org.osid.id.Id triggerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.control.Trigger trigger = this.triggers.get(triggerId);
        if (trigger == null) {
            throw new org.osid.NotFoundException("trigger not found: " + triggerId);
        }

        return (trigger);
    }


    /**
     *  Gets all <code>Triggers</code>. In plenary mode, the returned
     *  list contains all known triggers or an error
     *  results. Otherwise, the returned list may contain only those
     *  triggers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Triggers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.TriggerList getTriggers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.control.trigger.ArrayTriggerList(this.triggers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.triggers.clear();
        super.close();
        return;
    }
}
