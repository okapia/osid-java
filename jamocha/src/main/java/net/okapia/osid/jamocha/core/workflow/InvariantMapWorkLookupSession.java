//
// InvariantMapWorkLookupSession
//
//    Implements a Work lookup service backed by a fixed collection of
//    works.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.workflow;


/**
 *  Implements a Work lookup service backed by a fixed
 *  collection of works. The works are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapWorkLookupSession
    extends net.okapia.osid.jamocha.core.workflow.spi.AbstractMapWorkLookupSession
    implements org.osid.workflow.WorkLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapWorkLookupSession</code> with no
     *  works.
     *  
     *  @param office the office
     *  @throws org.osid.NullArgumnetException {@code office} is
     *          {@code null}
     */

    public InvariantMapWorkLookupSession(org.osid.workflow.Office office) {
        setOffice(office);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapWorkLookupSession</code> with a single
     *  work.
     *  
     *  @param office the office
     *  @param work a single work
     *  @throws org.osid.NullArgumentException {@code office} or
     *          {@code work} is <code>null</code>
     */

      public InvariantMapWorkLookupSession(org.osid.workflow.Office office,
                                               org.osid.workflow.Work work) {
        this(office);
        putWork(work);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapWorkLookupSession</code> using an array
     *  of works.
     *  
     *  @param office the office
     *  @param works an array of works
     *  @throws org.osid.NullArgumentException {@code office} or
     *          {@code works} is <code>null</code>
     */

      public InvariantMapWorkLookupSession(org.osid.workflow.Office office,
                                               org.osid.workflow.Work[] works) {
        this(office);
        putWorks(works);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapWorkLookupSession</code> using a
     *  collection of works.
     *
     *  @param office the office
     *  @param works a collection of works
     *  @throws org.osid.NullArgumentException {@code office} or
     *          {@code works} is <code>null</code>
     */

      public InvariantMapWorkLookupSession(org.osid.workflow.Office office,
                                               java.util.Collection<? extends org.osid.workflow.Work> works) {
        this(office);
        putWorks(works);
        return;
    }
}
