//
// AbstractOrganizationQuery.java
//
//     A template for making an Organization Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.personnel.organization.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for organizations.
 */

public abstract class AbstractOrganizationQuery    
    extends net.okapia.osid.jamocha.spi.AbstractTemporalOsidObjectQuery
    implements org.osid.personnel.OrganizationQuery {

    private final java.util.Collection<org.osid.personnel.records.OrganizationQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches a display label. 
     *
     *  @param  label a display label 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> label </code> is not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> label </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchDisplayLabel(String label, 
                                  org.osid.type.Type stringMatchType, 
                                  boolean match) {
        return;
    }


    /**
     *  Matches organizations with any display label. 
     *
     *  @param  match <code> true </code> to match organizations with any 
     *          display label, <code> false </code> to match organizations 
     *          with no display label 
     */

    @OSID @Override
    public void matchAnyDisplayLabel(boolean match) {
        return;
    }


    /**
     *  Clears all display label terms. 
     */

    @OSID @Override
    public void clearDisplayLabelTerms() {
        return;
    }


    /**
     *  Sets the organization <code> Id </code> for this query to match 
     *  organizations that have the specified organization as an ancestor. 
     *
     *  @param  organizationId an organization <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> organizationId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorOrganizationId(org.osid.id.Id organizationId, 
                                            boolean match) {
        return;
    }


    /**
     *  Clears the ancestor organization <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorOrganizationIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> OrganizationQuery </code> is available. 
     *
     *  @return <code> true </code> if an organization query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorOrganizationQuery() {
        return (false);
    }


    /**
     *  Gets the query for an organization. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the organization query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorOrganizationQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationQuery getAncestorOrganizationQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorOrganizationQuery() is false");
    }


    /**
     *  Matches agencies with any organization ancestor. 
     *
     *  @param  match <code> true </code> to match organizations with any 
     *          ancestor, <code> false </code> to match root organizations 
     */

    @OSID @Override
    public void matchAnyAncestorOrganization(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor organization query terms. 
     */

    @OSID @Override
    public void clearAncestorOrganizationTerms() {
        return;
    }


    /**
     *  Sets the organization <code> Id </code> for this query to match 
     *  organizations that have the specified organization as a descendant. 
     *
     *  @param  organizationId an organization <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> organizationId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantOrganizationId(org.osid.id.Id organizationId, 
                                              boolean match) {
        return;
    }


    /**
     *  Clears the descendant organization <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantOrganizationIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> OrganizationQuery </code> is available. 
     *
     *  @return <code> true </code> if an organization query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantOrganizationQuery() {
        return (false);
    }


    /**
     *  Gets the query for an organization. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the organization query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantOrganizationQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationQuery getDescendantOrganizationQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantOrganizationQuery() is false");
    }


    /**
     *  Matches organizations with any organization descendant. 
     *
     *  @param  match <code> true </code> to match organizations with any 
     *          descendant, <code> false </code> to match leaf organizations 
     */

    @OSID @Override
    public void matchAnyDescendantOrganization(boolean match) {
        return;
    }


    /**
     *  Clears the descendant organization query terms. 
     */

    @OSID @Override
    public void clearDescendantOrganizationTerms() {
        return;
    }


    /**
     *  Sets the realm <code> Id </code> for this query to match organizations 
     *  assigned to realms. 
     *
     *  @param  realmId a realm <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> realmId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRealmId(org.osid.id.Id realmId, boolean match) {
        return;
    }


    /**
     *  Clears all realm <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRealmIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> RealmQuery </code> is available. 
     *
     *  @return <code> true </code> if a realm query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRealmQuery() {
        return (false);
    }


    /**
     *  Gets the query for a realm query. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the realm query 
     *  @throws org.osid.UnimplementedException <code> supportsRealmQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.RealmQuery getRealmQuery() {
        throw new org.osid.UnimplementedException("supportsRealmQuery() is false");
    }


    /**
     *  Clears all realm terms. 
     */

    @OSID @Override
    public void clearRealmTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given organization query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an organization implementing the requested record.
     *
     *  @param organizationRecordType an organization record type
     *  @return the organization query record
     *  @throws org.osid.NullArgumentException
     *          <code>organizationRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(organizationRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.personnel.records.OrganizationQueryRecord getOrganizationQueryRecord(org.osid.type.Type organizationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.personnel.records.OrganizationQueryRecord record : this.records) {
            if (record.implementsRecordType(organizationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(organizationRecordType + " is not supported");
    }


    /**
     *  Adds a record to this organization query. 
     *
     *  @param organizationQueryRecord organization query record
     *  @param organizationRecordType organization record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addOrganizationQueryRecord(org.osid.personnel.records.OrganizationQueryRecord organizationQueryRecord, 
                                          org.osid.type.Type organizationRecordType) {

        addRecordType(organizationRecordType);
        nullarg(organizationQueryRecord, "organization query record");
        this.records.add(organizationQueryRecord);        
        return;
    }
}
