//
// AbstractAdapterObjectiveLookupSession.java
//
//    An Objective lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.learning.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Objective lookup session adapter.
 */

public abstract class AbstractAdapterObjectiveLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.learning.ObjectiveLookupSession {

    private final org.osid.learning.ObjectiveLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterObjectiveLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterObjectiveLookupSession(org.osid.learning.ObjectiveLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code ObjectiveBank/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code ObjectiveBank Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getObjectiveBankId() {
        return (this.session.getObjectiveBankId());
    }


    /**
     *  Gets the {@code ObjectiveBank} associated with this session.
     *
     *  @return the {@code ObjectiveBank} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBank getObjectiveBank()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getObjectiveBank());
    }


    /**
     *  Tests if this user can perform {@code Objective} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupObjectives() {
        return (this.session.canLookupObjectives());
    }


    /**
     *  A complete view of the {@code Objective} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeObjectiveView() {
        this.session.useComparativeObjectiveView();
        return;
    }


    /**
     *  A complete view of the {@code Objective} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryObjectiveView() {
        this.session.usePlenaryObjectiveView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include objectives in objective banks which are children
     *  of this objective bank in the objective bank hierarchy.
     */

    @OSID @Override
    public void useFederatedObjectiveBankView() {
        this.session.useFederatedObjectiveBankView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this objective bank only.
     */

    @OSID @Override
    public void useIsolatedObjectiveBankView() {
        this.session.useIsolatedObjectiveBankView();
        return;
    }
    
     
    /**
     *  Gets the {@code Objective} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Objective} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Objective} and
     *  retained for compatibility.
     *
     *  @param objectiveId {@code Id} of the {@code Objective}
     *  @return the objective
     *  @throws org.osid.NotFoundException {@code objectiveId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code objectiveId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.Objective getObjective(org.osid.id.Id objectiveId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getObjective(objectiveId));
    }


    /**
     *  Gets an {@code ObjectiveList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  objectives specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Objectives} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  objectiveIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Objective} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code objectiveIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveList getObjectivesByIds(org.osid.id.IdList objectiveIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getObjectivesByIds(objectiveIds));
    }


    /**
     *  Gets an {@code ObjectiveList} corresponding to the given
     *  objective genus {@code Type} which does not include
     *  objectives of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  objectives or an error results. Otherwise, the returned list
     *  may contain only those objectives that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  objectiveGenusType an objective genus type 
     *  @return the returned {@code Objective} list
     *  @throws org.osid.NullArgumentException
     *          {@code objectiveGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveList getObjectivesByGenusType(org.osid.type.Type objectiveGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getObjectivesByGenusType(objectiveGenusType));
    }


    /**
     *  Gets an {@code ObjectiveList} corresponding to the given
     *  objective genus {@code Type} and include any additional
     *  objectives with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  objectives or an error results. Otherwise, the returned list
     *  may contain only those objectives that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  objectiveGenusType an objective genus type 
     *  @return the returned {@code Objective} list
     *  @throws org.osid.NullArgumentException
     *          {@code objectiveGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveList getObjectivesByParentGenusType(org.osid.type.Type objectiveGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getObjectivesByParentGenusType(objectiveGenusType));
    }


    /**
     *  Gets an {@code ObjectiveList} containing the given
     *  objective record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  objectives or an error results. Otherwise, the returned list
     *  may contain only those objectives that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  objectiveRecordType an objective record type 
     *  @return the returned {@code Objective} list
     *  @throws org.osid.NullArgumentException
     *          {@code objectiveRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveList getObjectivesByRecordType(org.osid.type.Type objectiveRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getObjectivesByRecordType(objectiveRecordType));
    }


    /**
     *  Gets all {@code Objectives}. 
     *
     *  In plenary mode, the returned list contains all known
     *  objectives or an error results. Otherwise, the returned list
     *  may contain only those objectives that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Objectives} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveList getObjectives()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getObjectives());
    }
}
