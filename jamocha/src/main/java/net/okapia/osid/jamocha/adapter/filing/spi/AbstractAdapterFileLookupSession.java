//
// AbstractAdapterFileLookupSession.java
//
//    A File lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.filing.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A File lookup session adapter.
 */

public abstract class AbstractAdapterFileLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.filing.FileLookupSession {

    private final org.osid.filing.FileLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterFileLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterFileLookupSession(org.osid.filing.FileLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Directory/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Directory Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDirectoryId() {
        return (this.session.getDirectoryId());
    }


    /**
     *  Gets the {@code Directory} associated with this session.
     *
     *  @return the {@code Directory} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.Directory getDirectory()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getDirectory());
    }


    /**
     *  Tests if this user can perform {@code File} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupFiles() {
        return (this.session.canLookupFiles());
    }


    /**
     *  A complete view of the {@code File} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeFileView() {
        this.session.useComparativeFileView();
        return;
    }


    /**
     *  A complete view of the {@code File} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryFileView() {
        this.session.usePlenaryFileView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include files in directories which are children
     *  of this directory in the directory hierarchy.
     */

    @OSID @Override
    public void useFederatedDirectoryView() {
        this.session.useFederatedDirectoryView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this directory only.
     */

    @OSID @Override
    public void useIsolatedDirectoryView() {
        this.session.useIsolatedDirectoryView();
        return;
    }
    
     
    /**
     *  Gets the {@code File} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code File} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code File} and
     *  retained for compatibility.
     *
     *  @param fileId {@code Id} of the {@code File}
     *  @return the file
     *  @throws org.osid.NotFoundException {@code fileId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code fileId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.File getFile(org.osid.id.Id fileId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFile(fileId));
    }


    /**
     *  Gets a {@code FileList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  files specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Files} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  fileIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code File} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code fileIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.FileList getFilesByIds(org.osid.id.IdList fileIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFilesByIds(fileIds));
    }


    /**
     *  Gets a {@code FileList} corresponding to the given
     *  file genus {@code Type} which does not include
     *  files of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  files or an error results. Otherwise, the returned list
     *  may contain only those files that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  fileGenusType a file genus type 
     *  @return the returned {@code File} list
     *  @throws org.osid.NullArgumentException
     *          {@code fileGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.FileList getFilesByGenusType(org.osid.type.Type fileGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFilesByGenusType(fileGenusType));
    }


    /**
     *  Gets a {@code FileList} corresponding to the given
     *  file genus {@code Type} and include any additional
     *  files with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  files or an error results. Otherwise, the returned list
     *  may contain only those files that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  fileGenusType a file genus type 
     *  @return the returned {@code File} list
     *  @throws org.osid.NullArgumentException
     *          {@code fileGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.FileList getFilesByParentGenusType(org.osid.type.Type fileGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFilesByParentGenusType(fileGenusType));
    }


    /**
     *  Gets a {@code FileList} containing the given
     *  file record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  files or an error results. Otherwise, the returned list
     *  may contain only those files that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  fileRecordType a file record type 
     *  @return the returned {@code File} list
     *  @throws org.osid.NullArgumentException
     *          {@code fileRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.FileList getFilesByRecordType(org.osid.type.Type fileRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFilesByRecordType(fileRecordType));
    }


    /**
     *  Gets all {@code Files}. 
     *
     *  In plenary mode, the returned list contains all known
     *  files or an error results. Otherwise, the returned list
     *  may contain only those files that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Files} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.FileList getFiles()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFiles());
    }
}
