//
// AbstractMutableActivityUnit.java
//
//     Defines a mutable ActivityUnit.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.activityunit.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>ActivityUnit</code>.
 */

public abstract class AbstractMutableActivityUnit
    extends net.okapia.osid.jamocha.course.activityunit.spi.AbstractActivityUnit
    implements org.osid.course.ActivityUnit,
               net.okapia.osid.jamocha.builder.course.activityunit.ActivityUnitMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this activity unit. 
     *
     *  @param record activity unit record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addActivityUnitRecord(org.osid.course.records.ActivityUnitRecord record, org.osid.type.Type recordType) {
        super.addActivityUnitRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Enables this activity unit. Enabling an operable overrides any
     *  enabling rule that may exist.
     *  
     *  @param enabled <code>true</code> if enabled,
     *         <code>false<code> otherwise
     */
    
    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        return;
    }


    /**
     *  Disables this activity unit. Disabling an operable overrides
     *  any enabling rule that may exist.
     *
     *  @param disabled <code> true </code> if this object is
     *         disabled, <code> false </code> otherwise
     */
    
    @Override
    public void setDisabled(boolean disabled) {
        super.setDisabled(disabled);
        return;
    }


    /**
     *  Sets the operational flag.
     *
     *  @param operational <code>true</code>if operational,
     *         <code>false</code> if not operational
     */
    
    @Override
    public void setOperational(boolean operational) {
        super.setOperational(operational);
        return;
    }

    
    /**
     *  Sets the display name for this activity unit.
     *
     *  @param displayName the name for this activity unit
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this activity unit.
     *
     *  @param description the description of this activity unit
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException <code>genusType</code>
     *          is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the course.
     *
     *  @param course the course 
     *  @throws org.osid.NullArgumentException <code>course</code> is
     *          <code>null</code>
     */

    @Override
    public void setCourse(org.osid.course.Course course) {
        super.setCourse(course);
        return;
    }


    /**
     *  Sets the total target effort.
     *
     *  @param effort a total target effort
     *  @throws org.osid.NullArgumentException <code>effort</code> is
     *          <code>null</code>
     */

    @Override
    public void setTotalTargetEffort(org.osid.calendaring.Duration effort) {
        super.setTotalTargetEffort(effort);
        return;
    }


    /**
     *  Sets this is a contact activity. 
     *
     *  @param contact <code> true </code> if this is a contact activity, <code> 
     *         false </code> if an independent activity 
     */

    @Override
    public void setContact(boolean contact) {
        super.setContact(contact);
        return;
    }

    
    /**
     *  Sets the total target contact time.
     *
     *  @param contactTime a total target contact time
     *  @throws org.osid.NullArgumentException
     *          <code>contactTime</code> is <code>null</code>
     */

    @Override
    public void setTotalTargetContactTime(org.osid.calendaring.Duration contactTime) {
        super.setTotalTargetContactTime(contactTime);
        return;
    }


    /**
     *  Sets the total target individual effort.
     *
     *  @param effort a total target individual effort
     *  @throws org.osid.NullArgumentException <code>effort</code> is
     *          <code>null</code>
     */

    @Override
    public void setTotalTargetIndividualEffort(org.osid.calendaring.Duration effort) {
        super.setTotalTargetIndividualEffort(effort);
        return;
    }


    /**
     *  Sets the weekly effort.
     *
     *  @param effort a weekly effort
     *  @throws org.osid.NullArgumentException <code>effort</code> is
     *          <code>null</code>
     */

    @Override
    public void setWeeklyEffort(org.osid.calendaring.Duration effort) {
        super.setWeeklyEffort(effort);
        return;
    }


    /**
     *  Sets the weekly contact time.
     *
     *  @param contactTime a weekly contact time
     *  @throws org.osid.NullArgumentException
     *          <code>contactTime</code> is <code>null</code>
     */

    @Override
    public void setWeeklyContactTime(org.osid.calendaring.Duration contactTime) {
        super.setWeeklyContactTime(contactTime);
        return;
    }


    /**
     *  Sets the weekly individual effort.
     *
     *  @param effort a weekly individual effort
     *  @throws org.osid.NullArgumentException <code>effort</code> is
     *          <code>null</code>
     */

    @Override
    public void setWeeklyIndividualEffort(org.osid.calendaring.Duration effort) {
        super.setWeeklyIndividualEffort(effort);
        return;
    }


    /**
     *  Adds a learning objective.
     *
     *  @param objective a learning objective
     *  @throws org.osid.NullArgumentException <code>objective</code>
     *          is <code>null</code>
     */

    @Override
    public void addLearningObjective(org.osid.learning.Objective objective) {
        super.addLearningObjective(objective);
        return;
    }


    /**
     *  Sets all the learning objectives.
     *
     *  @param objectives a collection of learning objectives
     *  @throws org.osid.NullArgumentException <code>objectives</code>
     *          is <code>null</code>
     */

    @Override
    public void setLearningObjectives(java.util.Collection<org.osid.learning.Objective> objectives) {
        super.setLearningObjectives(objectives);
        return;
    }
}

