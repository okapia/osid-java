//
// AbstractEnrollment.java
//
//     Defines an Enrollment.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.program.enrollment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Enrollment</code>.
 */

public abstract class AbstractEnrollment
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.course.program.Enrollment {

    private org.osid.course.program.ProgramOffering programOffering;
    private org.osid.resource.Resource student;

    private final java.util.Collection<org.osid.course.program.records.EnrollmentRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the program offering <code> Id </code> associated with this 
     *  registration. 
     *
     *  @return the program offering <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getProgramOfferingId() {
        return (this.programOffering.getId());
    }


    /**
     *  Gets the program offering associated with this registration.
     *
     *  @return the program offering 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOffering getProgramOffering()
        throws org.osid.OperationFailedException {

        return (this.programOffering);
    }


    /**
     *  Sets the program offering.
     *
     *  @param programOffering a program offering
     *  @throws org.osid.NullArgumentException
     *          <code>programOffering</code> is <code>null</code>
     */

    protected void setProgramOffering(org.osid.course.program.ProgramOffering programOffering) {
        nullarg(programOffering, "prohram offering");
        this.programOffering = programOffering;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the student <code> Resource. </code> 
     *
     *  @return the <code> Resource </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getStudentId() {
        return (this.student.getId());
    }


    /**
     *  Gets the student <code> Resource. </code> 
     *
     *  @return the student 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getStudent()
        throws org.osid.OperationFailedException {

        return (this.student);
    }


    /**
     *  Sets the student.
     *
     *  @param student a student
     *  @throws org.osid.NullArgumentException <code>student</code> is
     *          <code>null</code>
     */

    protected void setStudent(org.osid.resource.Resource student) {
        nullarg(student, "student");
        this.student = student;
        return;
    }


    /**
     *  Tests if this enrollment supports the given record
     *  <code>Type</code>.
     *
     *  @param  enrollmentRecordType an enrollment record type 
     *  @return <code>true</code> if the enrollmentRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>enrollmentRecordType</code> is <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type enrollmentRecordType) {
        for (org.osid.course.program.records.EnrollmentRecord record : this.records) {
            if (record.implementsRecordType(enrollmentRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Enrollment</code> record <code>Type</code>.
     *
     *  @param  enrollmentRecordType the enrollment record type 
     *  @return the enrollment record 
     *  @throws org.osid.NullArgumentException
     *          <code>enrollmentRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(enrollmentRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.program.records.EnrollmentRecord getEnrollmentRecord(org.osid.type.Type enrollmentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.program.records.EnrollmentRecord record : this.records) {
            if (record.implementsRecordType(enrollmentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(enrollmentRecordType + " is not supported");
    }


    /**
     *  Adds a record to this enrollment. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param enrollmentRecord the enrollment record
     *  @param enrollmentRecordType enrollment record type
     *  @throws org.osid.NullArgumentException
     *          <code>enrollmentRecord</code> or
     *          <code>enrollmentRecordTypeenrollment</code> is
     *          <code>null</code>
     */
            
    protected void addEnrollmentRecord(org.osid.course.program.records.EnrollmentRecord enrollmentRecord, 
                                       org.osid.type.Type enrollmentRecordType) {

        nullarg(enrollmentRecord, "enrollment record");
        addRecordType(enrollmentRecordType);
        this.records.add(enrollmentRecord);
        
        return;
    }
}
