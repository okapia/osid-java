//
// AbstractAdapterTimePeriodLookupSession.java
//
//    A TimePeriod lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.calendaring.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A TimePeriod lookup session adapter.
 */

public abstract class AbstractAdapterTimePeriodLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.calendaring.TimePeriodLookupSession {

    private final org.osid.calendaring.TimePeriodLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterTimePeriodLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterTimePeriodLookupSession(org.osid.calendaring.TimePeriodLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Calendar/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Calendar Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.session.getCalendarId());
    }


    /**
     *  Gets the {@code Calendar} associated with this session.
     *
     *  @return the {@code Calendar} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCalendar());
    }


    /**
     *  Tests if this user can perform {@code TimePeriod} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupTimePeriods() {
        return (this.session.canLookupTimePeriods());
    }


    /**
     *  A complete view of the {@code TimePeriod} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeTimePeriodView() {
        this.session.useComparativeTimePeriodView();
        return;
    }


    /**
     *  A complete view of the {@code TimePeriod} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryTimePeriodView() {
        this.session.usePlenaryTimePeriodView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include time periods in calendars which are children
     *  of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        this.session.useFederatedCalendarView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        this.session.useIsolatedCalendarView();
        return;
    }
    
     
    /**
     *  Gets the {@code TimePeriod} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code TimePeriod} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code TimePeriod} and
     *  retained for compatibility.
     *
     *  @param timePeriodId {@code Id} of the {@code TimePeriod}
     *  @return the time period
     *  @throws org.osid.NotFoundException {@code timePeriodId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code timePeriodId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriod getTimePeriod(org.osid.id.Id timePeriodId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTimePeriod(timePeriodId));
    }


    /**
     *  Gets a {@code TimePeriodList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  timePeriods specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code TimePeriods} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  timePeriodIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code TimePeriod} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code timePeriodIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodList getTimePeriodsByIds(org.osid.id.IdList timePeriodIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTimePeriodsByIds(timePeriodIds));
    }


    /**
     *  Gets a {@code TimePeriodList} corresponding to the given
     *  time period genus {@code Type} which does not include
     *  time periods of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  time periods or an error results. Otherwise, the returned list
     *  may contain only those time periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  timePeriodGenusType a timePeriod genus type 
     *  @return the returned {@code TimePeriod} list
     *  @throws org.osid.NullArgumentException
     *          {@code timePeriodGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodList getTimePeriodsByGenusType(org.osid.type.Type timePeriodGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTimePeriodsByGenusType(timePeriodGenusType));
    }


    /**
     *  Gets a {@code TimePeriodList} corresponding to the given
     *  time period genus {@code Type} and include any additional
     *  time periods with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  time periods or an error results. Otherwise, the returned list
     *  may contain only those time periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  timePeriodGenusType a timePeriod genus type 
     *  @return the returned {@code TimePeriod} list
     *  @throws org.osid.NullArgumentException
     *          {@code timePeriodGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodList getTimePeriodsByParentGenusType(org.osid.type.Type timePeriodGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTimePeriodsByParentGenusType(timePeriodGenusType));
    }


    /**
     *  Gets a {@code TimePeriodList} containing the given
     *  time period record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  time periods or an error results. Otherwise, the returned list
     *  may contain only those time periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  timePeriodRecordType a timePeriod record type 
     *  @return the returned {@code TimePeriod} list
     *  @throws org.osid.NullArgumentException
     *          {@code timePeriodRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodList getTimePeriodsByRecordType(org.osid.type.Type timePeriodRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTimePeriodsByRecordType(timePeriodRecordType));
    }


    /**
     *  Gets a {@code TimePeriodList} containing the given {@code
     *  DateTime.} Time periods containing the given date are matched.
     *  In plenary mode, the returned list contains all of the time
     *  periods specified in the {@code Id} list, in the order of the
     *  list, including duplicates, or an error results if an {@code
     *  Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code TimePeriods} may
     *  be omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  datetime a date 
     *  @return the returned {@code TimePeriod} list 
     *  @throws org.osid.NullArgumentException {@code datetime} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodList getTimePeriodsByDate(org.osid.calendaring.DateTime datetime)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTimePeriodsByDate(datetime));
    }


    /**
     *  Gets a {@code TimePeriodList} corresponding to the given
     *  {@code DateTime.} Time periods whose start end end times are
     *  included in the given date range are matched.In plenary mode,
     *  the returned list contains all of the time periods specified
     *  in the {@code Id} list, in the order of the list, including
     *  duplicates, or an error results if an {@code Id} in the
     *  supplied list is not found or inaccessible. Otherwise,
     *  inaccessible {@code TimePeriods } may be omitted from the list
     *  and may present the elements in any order including returning
     *  a unique set.
     *
     *  @param  start start of daterange 
     *  @param  end end of date range 
     *  @return the returned {@code TimePeriod} list 
     *  @throws org.osid.InvalidArgumentException {@code end} is less 
     *          than {@code start} 
     *  @throws org.osid.NullArgumentException {@code start} or {@code 
     *          end} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodList getTimePeriodsInDateRange(org.osid.calendaring.DateTime start, 
                                                                         org.osid.calendaring.DateTime end)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.getTimePeriodsInDateRange(start, end));
    }


    /**
     *  Gets all {@code TimePeriods}. 
     *
     *  In plenary mode, the returned list contains all known
     *  time periods or an error results. Otherwise, the returned list
     *  may contain only those time periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code TimePeriods} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodList getTimePeriods()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTimePeriods());
    }
}
