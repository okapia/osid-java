//
// DeviceEnablerGenusFilterList.java
//
//     Implements a filter for genus types.
//
//
// Tom Coppeto
// Okapia
// 17 December 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.control.rules.deviceenabler;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for genus types.
 */

public final class DeviceEnablerGenusFilterList
    extends net.okapia.osid.jamocha.inline.filter.control.rules.deviceenabler.spi.AbstractDeviceEnablerFilterList
    implements org.osid.control.rules.DeviceEnablerList,
               DeviceEnablerFilter {

    private final java.util.Collection<org.osid.type.Type> genusTypes = new java.util.HashSet<>();


    /**
     *  Creates a new <code>DeviceEnablerGenusFilterList</code> passing the
     *  given genus type.
     *
     *  @param list a <code>DeviceEnablerList</code>
     *  @param type a genus type
     *  @throws org.osid.NullArgumentException <code>list</code> or
     *          <code>type</code> is <code>null</code>
     */

    public DeviceEnablerGenusFilterList(org.osid.control.rules.DeviceEnablerList list, 
                                   org.osid.type.Type type) {
        super(list);

        nullarg(type, "genus type");
        this.genusTypes.add(type);

        return;
    }    


    /**
     *  Creates a new <code>DeviceEnablerFilterList</code> passing the
     *  given collection of genus types.
     *
     *  @param list a <code>DeviceEnablerList</code>
     *  @param types a collection of genus types
     *  @throws org.osid.NullArgumentException <code>list</code> or
     *          <code>types</code> is <code>null</code>
     */
    
    public DeviceEnablerGenusFilterList(org.osid.control.rules.DeviceEnablerList list, 
                                   java.util.Collection<org.osid.type.Type> types) {
        super(list);

        nullarg(types, "genus types");
        this.genusTypes.addAll(types);

        return;
    }    

    
    /**
     *  Filters DeviceEnablers.
     *
     *  @param deviceEnabler the device enabler to filter
     *  @return <code>true</code> if the device enabler passes the filter,
     *          <code>false</code> if the device enabler should be filtered
     */

    @Override
    public boolean pass(org.osid.control.rules.DeviceEnabler deviceEnabler) {
        if (this.genusTypes.contains(deviceEnabler.getGenusType())) {
            return (true);
        } else {
            return (false);
        }
    }
}
